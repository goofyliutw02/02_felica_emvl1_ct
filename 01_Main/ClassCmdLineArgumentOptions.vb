﻿Public Class ClassCmdLineArgumentOptions
    Inherits Object
    ' Private Delegate Sub LogLnDelegate(ByRef strInfo As String)
    Protected Shared LogLn As Form01_Main.LogLnDelegate = New Form01_Main.LogLnDelegate(AddressOf Form01_Main.GetInstance().LogLn)
    '=================[ Basic Units must be initialized before maps objects ]===============
    'Public Shared m_mapPort As Dictionary(Of String, String)
    Private Shared m_nKeyLen As String = 6
    Private Shared m_strKeyIntf As String = "/INTF="
    Public Shared m_strKeyIntf_USBHID As String = "USBHID"
    Public Shared m_strKeyIntf_RS232 As String = "RS232"

    Public Shared m_strKey_PollForToken As String = "/POLLFORTOKENTYPE="
    Public Shared m_strKey_PollForToken_Mifare As String = "MIFARE"

    Private Shared m_strKeyBuad As String = "/BUAD="
    Private Shared m_strKeyBuad_115200 As String = "115200"
    Private Shared m_strKeyBuad_57600 As String = "57600"
    Private Shared m_strKeyBuad_38400 As String = "38400"
    Private Shared m_strKeyBuad_19200 As String = "19200"
    Private Shared m_strKeyBuad_9600 As String = "9600"

    Private Shared m_strKeyPort As String = "/PORT="
    Private Shared m_strKeyPort_Com As String = "COM"

    '2016 Oct 20, goofy_liu@idtechproducts.com.tw
    'Board Level Test  - PISCES
    Private Shared m_strKeyUSBHID_PID As String = "/PID="
    Private Shared m_strKeyUSBHID_VID As String = "/VID="

    Private Shared m_strKeyHelp As String = "/?"

    'ex: /VCOM=COM6
    Private Shared m_strKeyUSBCDC_VCOMPort As String = "/VCOM=" 'USB Virtual COM Port, a.k.a. USB CDC 
    Private Shared m_bInit As Boolean = False
    'TODO --> Auto Detection VCOM Type
    Private Shared m_strUSBCDC_VCOM As String = "" 'ex: /VCOM=COM6, said USB Virtual COM Port = COM6
    Private Shared m_objInstance As ClassCmdLineArgumentOptions = Nothing
    Private Shared m_nConnUSBHIDKeyVIDPID As Integer

    Private m_u16PID As UInt16
    Private m_u16VID As UInt16
    Private m_bIsPollForToken_MifareType As Boolean

    Public Property u16USBHID_PID As UInt16
        Set(value As UInt16)
            m_u16PID = value
        End Set
        Get
            Return m_u16PID
        End Get
    End Property
    Public Property u16USBHID_VID As UInt16
        Set(value As UInt16)
            m_u16VID = value
        End Set
        Get
            Return m_u16VID
        End Get
    End Property
    Public Property strUSBHID_PID As String
        Set(value As String)
            Dim arrayByteData As Byte() = Nothing
            Try
                'Convert from Hex String to Byte Array
                'Ex: "AF92" --> {&H92, &HAF} , 16 bit-Value
                ClassTableListMaster.TLVauleFromString2ByteArray(value, arrayByteData)
                'Use first Value
                ClassTableListMaster.ConvertFromByteArray2UInt16(arrayByteData, m_u16PID)

            Catch ex As Exception
                Throw
            Finally
                If (arrayByteData IsNot Nothing) Then
                    Erase arrayByteData
                End If
            End Try

        End Set
        Get
            Return Convert.ToString(m_u16PID, 16).ToUpper()
        End Get
    End Property
    '
    Public Property strUSBHID_VID As String
        Set(value As String)
            Dim arrayByteData As Byte() = Nothing
            Try
                'Convert from Hex String to Byte Array
                'Ex: "AF92" --> {&H92, &HAF} , 16 bit-Value
                ClassTableListMaster.TLVauleFromString2ByteArray(value, arrayByteData)
                'Use first Value
                ClassTableListMaster.ConvertFromByteArray2UInt16(arrayByteData, m_u16VID)

            Catch ex As Exception
                Throw 'ex
            Finally
                If (arrayByteData IsNot Nothing) Then
                    Erase arrayByteData
                End If
            End Try

        End Set
        Get
            Return Convert.ToString(m_u16VID, 16).ToUpper()
        End Get
    End Property

    Private m_strPartNum As String
    Property strPartNum As String
        Set(value As String)
            m_strPartNum = value
        End Set
        Get
            Return m_strPartNum
        End Get
    End Property

    ReadOnly Property IsConnUSBHIDVisible As Boolean
        Get
            Return True
        End Get
    End Property

    ReadOnly Property IsConnRS232Visible As Boolean
        Get
            Return True
        End Get
    End Property


    Public Shared Function GetInstance() As ClassCmdLineArgumentOptions
        If (m_objInstance Is Nothing) Then
            m_objInstance = New ClassCmdLineArgumentOptions(Form01_Main.m_logRichText)

            '========[Printout Command Line Helps Info ]=============
            ' 
            ' 2018 April 18, remove print help function
            'ClassCmdLineArgumentOptions.mapFuncParserHelp("")
        End If
        Return m_objInstance
    End Function
    '-------------[ Communication Interface : RS232/ USB HID ]--------------
    Private m_nConnIntf As ClassReaderProtocolBasic.ENU_DEV_INTERFACE
    Property intfConnType As ClassReaderProtocolBasic.ENU_DEV_INTERFACE
        Set(value As ClassReaderProtocolBasic.ENU_DEV_INTERFACE)
            m_nConnIntf = value
        End Set
        Get
            Return m_nConnIntf
        End Get
    End Property
    '
    Private m_bIsEngeerModeSupport As Boolean
    Property bIsEngMode As Boolean
        Set(value As Boolean)
            m_bIsEngeerModeSupport = value
        End Set
        Get
            Return m_bIsEngeerModeSupport
        End Get
    End Property

    '---------[ Production ID ]------------
    Private m_nPrdtID As ClassReaderInfo.ENUM_PD_IMG_IDX
    Property nPrdtID As ClassReaderInfo.ENUM_PD_IMG_IDX
        Set(value As ClassReaderInfo.ENUM_PD_IMG_IDX)
            m_nPrdtID = value
        End Set
        Get
            Return m_nPrdtID
        End Get
    End Property
    '-----------[RS232 Port]----------
    '(Optional ByVal strPort As String = "COM1", Optional ByVal nBaudRate As EnumBUAD_RATE = EnumBUAD_RATE.BUADRATE_115200)
    Private m_strPort As String
    Property strPort As String
        Set(value As String)
            m_strPort = value
        End Set
        Get
            Return m_strPort
        End Get
    End Property

    '-----------[ RS232 Buad Rate ]----------
    Private m_nBuadRate As ClassDevRS232.EnumBUAD_RATE
    Property nBuadRate As ClassDevRS232.EnumBUAD_RATE
        Set(value As ClassDevRS232.EnumBUAD_RATE)
            m_nBuadRate = value
        End Set
        Get
            Return m_nBuadRate
        End Get
    End Property
    Public Sub Reset()
        m_nConnIntf = ClassReaderProtocolBasic.ENU_DEV_INTERFACE.INTF_UNKNOWN
        m_nBuadRate = ClassDevRS232.EnumBUAD_RATE._UNKNOWN
        m_strPort = ""
        m_nPrdtID = ClassReaderInfo.ENUM_PD_IMG_IDX.UNKNOWN_MAX
        m_bIsEngeerModeSupport = False
        m_strPartNum = ""

        m_u16VID = &HFFFF
        m_u16PID = &HFFFF
        m_nConnUSBHIDKeyVIDPID = 0

        m_bIsPollForToken_MifareType = False
    End Sub

    Private m_RTB As RichTextBox
    Sub New(rtb As RichTextBox)
        Reset()
        m_RTB = rtb
    End Sub

    Public Sub LogLnHelpCommandLineOption(strMsg As String)
        If (m_RTB Is Nothing) Then
            Return
        End If
        If (m_RTB.IsDisposed) Then
            Return
        End If
        m_RTB.AppendText(vbCrLf + strMsg)
    End Sub

    '------------[ Arguments / Options Key String List, used to translate keyword string to value ]----------
    Public Shared m_mapConnIntf As SortedDictionary(Of String, ClassReaderProtocolBasic.ENU_DEV_INTERFACE) = New SortedDictionary(Of String, ClassReaderProtocolBasic.ENU_DEV_INTERFACE) From {
        {m_strKeyIntf_USBHID, ClassReaderProtocolBasic.ENU_DEV_INTERFACE.INTF_USBHID}
        }

    Public Shared m_mapBuadrate As SortedDictionary(Of String, ClassDevRS232.EnumBUAD_RATE) = New SortedDictionary(Of String, ClassDevRS232.EnumBUAD_RATE) From {
        {m_strKeyBuad_115200, ClassDevRS232.EnumBUAD_RATE._115200},
        {m_strKeyBuad_57600, ClassDevRS232.EnumBUAD_RATE._57600},
        {m_strKeyBuad_38400, ClassDevRS232.EnumBUAD_RATE._38400},
        {m_strKeyBuad_19200, ClassDevRS232.EnumBUAD_RATE._19200},
        {m_strKeyBuad_9600, ClassDevRS232.EnumBUAD_RATE._9600}
        }

    Private Shared m_mapFuncParser As Dictionary(Of String, Action(Of String)) = New Dictionary(Of String, Action(Of String)) From {
        {m_strKeyIntf, AddressOf ClassCmdLineArgumentOptions.mapFuncParserCommInterface},
        {m_strKeyBuad, AddressOf ClassCmdLineArgumentOptions.mapFuncParserCommBuadrate},
        {m_strKeyHelp, AddressOf ClassCmdLineArgumentOptions.mapFuncParserHelp},
        {m_strKeyPort, AddressOf ClassCmdLineArgumentOptions.mapFuncParserCommPort},
        {m_strKeyUSBHID_VID, AddressOf ClassCmdLineArgumentOptions.mapFuncParserUSBHID_VID},
        {m_strKeyUSBHID_PID, AddressOf ClassCmdLineArgumentOptions.mapFuncParserUSBHID_PID},
        {m_strKey_PollForToken, AddressOf ClassCmdLineArgumentOptions.mapFuncParser_PollForToken_MifareType}
}

    Private Shared Sub mapFuncParserCommInterface(strArgs As String)
        If (m_mapConnIntf Is Nothing) Then
            LogLn("[Err] Invalid(Null) Lookup Table : ConnIntf")
            Return
        End If

        If (Not m_mapConnIntf.ContainsKey(strArgs)) Then
            mapFuncParserCommInterface_PrintoutHelp(strArgs)
            Return
        End If

        Dim pInst As ClassCmdLineArgumentOptions = GetInstance()
        pInst.intfConnType = m_mapConnIntf(strArgs)
    End Sub
    '
    '
    Private Shared Sub mapFuncParserCommBuadrate(strArgs As String)
        If (m_mapBuadrate Is Nothing) Then
            LogLn("[Err] Invalid(Null) Lookup Table : m_mapBuadrate")
            Return
        End If

        If (Not m_mapBuadrate.ContainsKey(strArgs)) Then
            mapFuncParserCommBuadrate_PrintoutHelp(strArgs)
            Return
        End If

        Dim pInst As ClassCmdLineArgumentOptions = GetInstance()
        pInst.nBuadRate = m_mapBuadrate(strArgs)
        If (Not pInst.m_strPort.Equals("")) Then
            'Make sure we use RS232 Interface
            pInst.m_nConnIntf = ClassReaderProtocolBasic.ENU_DEV_INTERFACE.INTF_RS232
        End If
    End Sub
    '
    ' Skip the first & the last item comparison
    Private Shared Sub mapFuncParserUSBHID_PID(strArgs As String)
        Dim pInst As ClassCmdLineArgumentOptions = GetInstance()
        strArgs = Trim(strArgs)
        pInst.strUSBHID_PID = strArgs
        m_nConnUSBHIDKeyVIDPID = m_nConnUSBHIDKeyVIDPID Or &H2
        If (m_nConnUSBHIDKeyVIDPID = &H3) Then
            'Force to make it as USB HID Connection Interface
            pInst.intfConnType = ClassReaderProtocolBasic.ENU_DEV_INTERFACE.INTF_USBHID
        End If
    End Sub

    '''////////////////////////////////////////////////////////////////////////////////////////////////////
    ''' \fn Private Shared Sub mapFuncParser_PollForToken_MifareType(strArgs As String) Dim pInst As ClassCmdLineArgumentOptions = GetInstance() strArgs = Trim(strArgs) pInst.strUSBHID_PID = strArgs m_nConnUSBHIDKeyVIDPID = m_nConnUSBHIDKeyVIDPID Or &H2 If (m_nConnUSBHIDKeyVIDPID = &H3) Then 'Force to make it as USB HID Connection Interface pInst.intfConnType = ClassReaderProtocolBasic.ENU_DEV_INTERFACE.INTF_USBHID End If End Sub
    '''
    ''' \brief  Map function parser poll for token mifare type. This is set by command line
    '''         indicator.
    '''
    ''' \author Goofy Liu
    ''' \date   2018/4/16
    '''
    ''' \param  strArgs The arguments of the command line..
    '''////////////////////////////////////////////////////////////////////////////////////////////////////

    Private Shared Sub mapFuncParser_PollForToken_MifareType(strArgs As String)
        Dim pInst As ClassCmdLineArgumentOptions = GetInstance()
        strArgs = Trim(strArgs).ToUpper()

        pInst.bIsPollForToken_MifareType = strArgs.Contains(m_strKey_PollForToken_Mifare)

    End Sub


    '
    ' Skip the first & the last item comparison
    Private Shared Sub mapFuncParserUSBHID_VID(strArgs As String)
        Dim pInst As ClassCmdLineArgumentOptions = GetInstance()
        strArgs = Trim(strArgs)
        pInst.strUSBHID_VID = strArgs
        m_nConnUSBHIDKeyVIDPID = m_nConnUSBHIDKeyVIDPID Or &H1
        If (m_nConnUSBHIDKeyVIDPID = &H3) Then
            pInst.intfConnType = ClassReaderProtocolBasic.ENU_DEV_INTERFACE.INTF_USBHID
        End If
    End Sub
    '
    Public Shared Sub mapFuncParserHelp(strArgs As String)
        Dim clao As ClassCmdLineArgumentOptions = ClassCmdLineArgumentOptions.GetInstance()
        clao.LogLnHelpCommandLineOptionClean()

        'Printout the available Part Number....
        mapFuncParserCommInterface_PrintoutHelp(strArgs)
        mapFuncParserCommInterface_RS232_PrintoutHelp(strArgs)
        mapFuncParserCommBuadrate_PrintoutHelp(strArgs)
        mapFuncParserCommPID_VIDList_PrintoutHelp(strArgs)
    End Sub
    '
    Private Shared Sub mapFuncParserCommPort(strArgs As String)
        Dim nPortID As Integer
        Dim strTmp As String

        'Check is COM#, where # is number 1~99 (Decimal)
        strTmp = strArgs.Substring(0, m_strKeyPort_Com.Length()) 'strArgs.Length() - m_nKeyLen)
        If (Not strTmp.Equals(m_strKeyPort_Com)) Then
            LogLn("[Err] Invalid Argument : " + strArgs + " Prefix = " + strTmp)
            Return
        End If
        strTmp = strArgs.Substring(m_strKeyPort_Com.Length())
        If (strTmp Is Nothing) Then
            LogLn("[Err] Invalid Argument Format :" + strArgs + " Prefix = " + strTmp)
            Return
        End If
        nPortID = Convert.ToInt32(strTmp, 10)
        If ((nPortID < 1) Or (nPortID > 99)) Then
            mapFuncParserCommInterface_RS232_PrintoutHelp(strArgs)
            LogLn("[Err] Invalid Argument : " + strArgs + ", out of range from " + m_strKeyPort_Com + "0 to " + m_strKeyPort_Com + "99")
            Return
        End If

        Dim pInst As ClassCmdLineArgumentOptions = GetInstance()
        pInst.m_strPort = strArgs
        If (pInst.m_nBuadRate <> ClassDevRS232.EnumBUAD_RATE._UNKNOWN) Then
            'Make sure we use RS232 Interface
            pInst.m_nConnIntf = ClassReaderProtocolBasic.ENU_DEV_INTERFACE.INTF_RS232
        End If
    End Sub

    '
    Private Shared Sub mapFuncUSBCDC_VCOM(strArgs As String)
        m_strUSBCDC_VCOM = strArgs.ToUpper()
        '/VCOM=COM6 --> strArgs = COM6
    End Sub
    Shared Property strUSBCDC_VCOM As String
        Get
            Return m_strUSBCDC_VCOM
        End Get
        Set(value As String)
            m_strUSBCDC_VCOM = value
            m_bIsFirstTimeUSBCDC_VCOM = False
        End Set
    End Property
    Private Shared m_bIsFirstTimeUSBCDC_VCOM As Boolean = True
    Shared ReadOnly Property bIsFirstTimeUSBCDC_VCOM As Boolean
        Get
            Return m_bIsFirstTimeUSBCDC_VCOM
        End Get
    End Property
    '
    '========[ New ]========
    Public Sub ParseLineTokens(strDictCmdLine As Dictionary(Of String, Integer), Optional ByVal bStrListAutoErase As Boolean = True, Optional ByVal bFristTokenIsExeName As Boolean = True)
        Dim strKeySeek As String
        Dim strDataSeek As String
        Dim nIdxMax As Integer
        Dim strIterator As String
        Dim nIdxMin As Integer = 0
        Try
            If (strDictCmdLine Is Nothing) Then
                Throw New Exception("[Err] Invalid Command Line Arguments ")
            End If

            If (m_mapFuncParser Is Nothing) Then
                Throw New Exception("[Err] Invalid Command Line Parser Function List ")
            End If

            nIdxMax = strDictCmdLine.Count - 1

            If (bFristTokenIsExeName) Then
                'Remove First One
                strDictCmdLine.Remove(0)
            End If

            For Each itrSeek In strDictCmdLine
                'Normalize to Upper Case before parsing in non-senstive case
                strIterator = itrSeek.Key
                strIterator = strIterator.ToUpper()

                If (strIterator.Equals(m_strKeyHelp)) Then
                    'Special case for "/?" option (argument)
                    strKeySeek = strIterator
                    strDataSeek = ""
                Else

                    'If (strIterator.Length <= m_nKeyLen) Then
                    '    LogLn("[Err] Invalid Arguments(Or No Value) : " + strIterator)
                    '    Continue For
                    'End If
                    strKeySeek = strIterator.Substring(0, itrSeek.Value)
                    strDataSeek = strIterator.Substring(itrSeek.Value, strIterator.Length() - itrSeek.Value)
                End If

                If (m_mapFuncParser.ContainsKey(strKeySeek)) Then
                    m_mapFuncParser(strKeySeek)(strDataSeek)
                Else
                    LogLn("[Err] Invalid Argument prefix : " + strKeySeek)
                End If
            Next


        Catch ex As Exception
            Throw 'ex
        Finally
            If (bStrListAutoErase) Then
                If (strDictCmdLine IsNot Nothing) Then
                    strDictCmdLine.Clear()
                    strDictCmdLine = Nothing
                End If
            End If
        End Try
    End Sub
    '
    ReadOnly Property bIsUSBHID_PID_VID_Available As Boolean
        Get
            Return ((m_u16PID <> &HFFFF) And (m_u16VID <> &HFFFF))
        End Get
    End Property
    '
    Public Sub ParseLineTokens(strCmdLine As String)
        Dim strDictOptions As Dictionary(Of String, Integer) = New Dictionary(Of String, Integer)
        Dim strKeySeek As String
        Dim strDataSeek As String
        Dim strCmdLineUpperCase As String
        Dim sdictTokens As SortedDictionary(Of Integer, Integer) = New SortedDictionary(Of Integer, Integer)

        'sdictTokens2 entry structure
        'Key = Start Pos; Value.Key = Option String.Length; Value.Value = Option Key.Length
        'Ex: "/PTNM=XX12QABC /INTF=USBHID"
        '1st entry : Key("/PTNM=") = 0; Value.key = "/PTNM=XX12QABC ".length = 15; Value.Value = "/PTNM=".length = 6
        '2nd entry : Key("/INTF=") = 15; Value.key = "/INTF=USBHID".length = 12; Value.Value = "/INTF=".length = 6
        Dim sdictTokens2 As SortedDictionary(Of Integer, KeyValuePair(Of Integer, Integer)) = New SortedDictionary(Of Integer, KeyValuePair(Of Integer, Integer))
        Dim nIdxOfKey As Integer
        Dim nOff As Integer = -1
        Dim nKeyLength As Integer = -1
        Dim nIdx As Integer = 0

        Try
            'Ex: /ptnm=Xx12qABC --> /PTNM=XX12QABC
            strCmdLineUpperCase = strCmdLine.ToUpper()

            'Ex: Org Cmd Line = "/PTNM=XX12QABC /INTF=USBHID"
            ' --> nIdxOfKey(/PTNM) = 0
            '--> nIdxOfKey(/INTF) = 16
            For Each itrToken In m_mapFuncParser
                strKeySeek = itrToken.Key
                If (strCmdLineUpperCase.Contains(strKeySeek)) Then
                    nIdxOfKey = strCmdLineUpperCase.IndexOf(strKeySeek)
                    sdictTokens.Add(nIdxOfKey, strKeySeek.Length())
                End If
            Next

            'Compute the arguments length
            'Ex: [ Org]
            '--> nIdxOfKey("/PTNM=") = 0
            '--> nIdxOfKey("/INTF=") = 16
            '[New, length calculation ]
            '--> nIdxOfKey("/PTNM=") = 0, nLen = 16
            '--> nIdxOfKey("/INTF=") = 16, nLen = CmdLine.length - 16 = 28-16 = 12
            If (sdictTokens.Count > 0) Then
                For Each itrTokenDict In sdictTokens
                    If (nOff = -1) Then
                        'First time
                        nOff = itrTokenDict.Key

                    ElseIf (nOff < itrTokenDict.Key) Then
                        'either first time, calculate 
                        sdictTokens2.Add(nOff, New KeyValuePair(Of Integer, Integer)(nKeyLength, itrTokenDict.Key - nOff))
                        nOff = itrTokenDict.Key
                    End If
                    nKeyLength = itrTokenDict.Value
                Next
                ' update the last one token
                sdictTokens2.Add(nOff, New KeyValuePair(Of Integer, Integer)(nKeyLength, strCmdLine.Length - nOff))

                'Create (String, Key Token's length) Pair List
                '[Org] Cmd Line = "/PTNM=XX12QABC /INTF=USBHID"
                '[New String List @ keyword delimiter]
                ' -->[0] = "/PTNM=XX12QABC"
                '--->[1] = "/INTF=USBHID"
                nIdxOfKey = 0
                For Each itrTokenDict In sdictTokens2
                    strDataSeek = strCmdLine.Substring(itrTokenDict.Key, itrTokenDict.Value.Value)
                    'Remove Space/Blank characters from Head(Left) & Trail(Right)
                    strDataSeek = Trim(strDataSeek)

                    'Add  separated Tokens to list of options
                    strDictOptions.Add(strDataSeek, itrTokenDict.Value.Key)
                Next

                ParseLineTokens(strDictOptions, True, False)
            End If

            '=========[ Special Check ]===========
            '---------[USB HID]----------
            If (bIsUSBHID_PID_VID_Available) Then
                If (m_nConnIntf = ClassReaderProtocolBasic.ENU_DEV_INTERFACE.INTF_RS232) Then
                    Throw New Exception("Conflict Arguments: USB HID (PID+VID) is mutually exclusive with RS232 settings")
                End If
                m_nConnIntf = ClassReaderProtocolBasic.ENU_DEV_INTERFACE.INTF_USBHID
            End If

        Catch ex As Exception
            Throw 'ex
        Finally
            If (strDictOptions IsNot Nothing) Then
                strDictOptions.Clear()
                strDictOptions = Nothing
            End If
        End Try
    End Sub

    ReadOnly Property IsConnModeAvailable As Boolean
        Get
            Return (m_nConnIntf <> ClassReaderProtocolBasic.ENU_DEV_INTERFACE.INTF_UNKNOWN)
        End Get
    End Property
    ReadOnly Property IsConnRS232 As Boolean
        Get
            Return (m_nConnIntf = ClassReaderProtocolBasic.ENU_DEV_INTERFACE.INTF_RS232)
        End Get
    End Property


    ReadOnly Property IsConnUSBHID As Boolean
        Get
            Return (m_nConnIntf = ClassReaderProtocolBasic.ENU_DEV_INTERFACE.INTF_USBHID)
        End Get
    End Property

    Public Sub Cleanup()
        If (m_mapConnIntf IsNot Nothing) Then
            m_mapConnIntf.Clear()
        End If
        'If (m_mapPrdtID IsNot Nothing) Then
        '    m_mapPrdtID.Clear()
        'End If
        If (m_mapBuadrate IsNot Nothing) Then
            m_mapBuadrate.Clear()
        End If
        '
        m_objInstance = Nothing
    End Sub

    'Public Shared m_mapConnIntf As Dictionary(Of String, ClassReaderCommander.ENU_DEV_INTERFACE)
    'Public Shared m_mapPrdtID As Dictionary(Of String, ClassReaderInfo.ENUM_PD_IMG_IDX)
    'Public Shared m_mapBuadrate As Dictionary(Of String, ClassDevRS232.EnumBUAD_RATE)
    Public Shared m_strLineDelimiter As String = "============================="
    Public Shared m_strHeadDescription As String = " value is as the following..."
    Private Shared Sub mapFuncParserCommInterface_PrintoutHelp(strArgs As String)
        Dim clao As ClassCmdLineArgumentOptions = ClassCmdLineArgumentOptions.GetInstance()

        LogLn(m_strLineDelimiter)
        LogLn(m_strKeyIntf + " Communication Interface : " + m_strHeadDescription)
        clao.LogLnHelpCommandLineOption(m_strLineDelimiter)
        clao.LogLnHelpCommandLineOption(m_strKeyIntf + " Communication Interface : " + m_strHeadDescription)
        For Each iteratorSeek In m_mapConnIntf
            LogLn(m_strKeyIntf + iteratorSeek.Key)
            clao.LogLnHelpCommandLineOption(m_strKeyIntf + iteratorSeek.Key)
        Next
    End Sub

    Private Shared Sub mapFuncParserCommInterface_RS232_PrintoutHelp(strArgs As String)
        Dim clao As ClassCmdLineArgumentOptions = ClassCmdLineArgumentOptions.GetInstance()
        Dim slist As SortedList(Of String, String) = Form01_Main.GetInstance().GetPortList_RS323()
        '
        LogLn(m_strLineDelimiter)
        LogLn(m_strKeyPort + " RS232 Port : " + m_strHeadDescription)
        clao.LogLnHelpCommandLineOption(m_strLineDelimiter)
        clao.LogLnHelpCommandLineOption(m_strKeyPort + " RS232 Port : " + m_strHeadDescription)
        For Each iteratorSeek In slist
            LogLn(m_strKeyPort + iteratorSeek.Key)
            clao.LogLnHelpCommandLineOption(m_strKeyPort + iteratorSeek.Key)
        Next
        '
        slist.Clear()
    End Sub

    Private Shared Sub mapFuncParserCommBuadrate_PrintoutHelp(strArgs As String)
        Dim clao As ClassCmdLineArgumentOptions = ClassCmdLineArgumentOptions.GetInstance()
        LogLn(m_strLineDelimiter)
        LogLn(m_strKeyBuad + " Baud rate : " + m_strHeadDescription)
        clao.LogLnHelpCommandLineOption(m_strLineDelimiter)
        clao.LogLnHelpCommandLineOption(m_strKeyBuad + " Baud rate : " + m_strHeadDescription)
        For Each iteratorSeek In m_mapBuadrate
            LogLn(m_strKeyBuad + iteratorSeek.Key)
            clao.LogLnHelpCommandLineOption(m_strKeyBuad + iteratorSeek.Key)
        Next
    End Sub


    
    '
    Private Shared Sub mapFuncParserCommPID_VIDList_PrintoutHelp(strArgs As String)
        Dim clao As ClassCmdLineArgumentOptions = ClassCmdLineArgumentOptions.GetInstance()
        Dim strVIP_PID As String = ""

        LogLn(m_strLineDelimiter)
        LogLn("(VID, PID)" + "pair list is as the following...")
        clao.LogLnHelpCommandLineOption(m_strLineDelimiter)
        clao.LogLnHelpCommandLineOption("(VID, PID)" + "pair list is as the following...")

        For Each iteratorSeek In ClassDevUSBHID.m_listUSBHIDDevInfo
            strVIP_PID = Convert.ToString(iteratorSeek.m_u32PID, 16) + "," + Convert.ToString(iteratorSeek.m_u32VID, 16) + vbCrLf + strVIP_PID
        Next
        LogLn("/VID=, /PID=" + strVIP_PID)
        clao.LogLnHelpCommandLineOption("/VID=, /PID=" + strVIP_PID)
    End Sub
    'This function
    'Public Sub ProcessCommandLine()
    '    '------------[1.Communication Interfaces, "/USBHID" or RS232(BuadRate+Port)]------------
    '    '------------[2.Product ID, Current not support yet since after connection TS will retrieve Reader Firmware Info]------------
    '    '------------[3.Buad Rate, RS232 Interface ONLY, ]------------
    '    '------------[4.Port, RS232 Interface ONLY]------------
    '    '------------[5. Mode Selection]------------------------
    'End Sub

    Private Sub LogLnHelpCommandLineOptionClean()
        If (m_RTB Is Nothing) Then
            Return
        End If

        m_RTB.Clear()
    End Sub
    '
    ' 
    Public Property bIsPollForToken_MifareType As Boolean
        Get
            Return m_bIsPollForToken_MifareType
        End Get
        Set(value As Boolean)
            m_bIsPollForToken_MifareType = value
        End Set
    End Property

End Class

