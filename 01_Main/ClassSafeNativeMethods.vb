﻿Imports System.Security
Imports System.Runtime.InteropServices

<SuppressUnmanagedCodeSecurityAttribute()> _
Friend NotInheritable Class SafeNativeMethods

    Public Delegate Function AppDoEvent() As UInt32

    Private Sub New()
    End Sub

    <DllImport("USB_HID_IDTW_32bit.dll", CharSet:=CharSet.Auto, ExactSpelling:=True)> _
    Friend Shared Function getDLLVersion() As String
    End Function

    <DllImport("USB_HID_IDTW_32bit.dll", CharSet:=CharSet.Auto, ExactSpelling:=True)> _
    Friend Shared Sub testfunc()

    End Sub

    <DllImport("USB_HID_IDTW_32bit.dll", CharSet:=CharSet.Auto, ExactSpelling:=True)> _
    Friend Shared Function USBHIDSendAndResponse_u32( _
          u32VID As UInt32, u32PID As UInt32, _
         arrayByteSendData As Byte(), u32SendSize As UInt32, _
         pArrayByteRecvData As Byte(), u32RecvDataBufSize As UInt32, _
         u32TimeoutMS As UInt32, _
         <MarshalAs(UnmanagedType.FunctionPtr)> ByVal pfuncCB As AppDoEvent) _
         As UInt32
    End Function

    <DllImport("USB_HID_IDTW_32bit.dll", CharSet:=CharSet.Auto, ExactSpelling:=True)> _
    Friend Shared Function USBHIDRecvData_u32( _
          u32VID As UInt32, u32PID As UInt32, _
         pArrayByteRecvData As Byte(), u32RecvDataBufSize As UInt32, _
         u32TimeoutMS As UInt32, _
           <MarshalAs(UnmanagedType.FunctionPtr)> pfuncCB As AppDoEvent) _
    As UInt32
    End Function

    <DllImport("USB_HID_IDTW_32bit.dll", CharSet:=CharSet.Auto, ExactSpelling:=True)> _
    Friend Shared Function USBHIDRecvDataIDGCmd_u32( _
          u32VID As UInt32, u32PID As UInt32, _
         pArrayByteRecvData As Byte(), u32RecvDataBufSize As UInt32, _
         u32TimeoutMS As UInt32, _
           <MarshalAs(UnmanagedType.FunctionPtr)> pfuncCB As AppDoEvent) _
         As UInt32
    End Function

    <DllImport("USB_HID_IDTW_32bit.dll", CharSet:=CharSet.Auto, ExactSpelling:=True)> _
    Friend Shared Function USBHIDOpen_u8(u32VID As UInt32, u32PID As UInt32) As Byte
    End Function

    <DllImport("USB_HID_IDTW_32bit.dll", CharSet:=CharSet.Auto, ExactSpelling:=True)> _
    Friend Shared Function USBKB_Wedge_Open_u8(u32VID As UInt32, u32PID As UInt32) As Byte
    End Function


    <DllImport("USB_HID_IDTW_32bit.dll", CharSet:=CharSet.Auto, ExactSpelling:=True)> _
    Friend Shared Sub USBHIDClose()
    End Sub
    '
    <DllImport("user32.dll", CharSet:=CharSet.Auto, ExactSpelling:=True)> _
    Friend Shared Function SendMessageW(ByVal hWnd As IntPtr, ByVal msg As Integer, ByVal _
wParam As Integer, ByVal lParam As Integer) As Integer
    End Function
    '
    <DllImport("user32.dll", CharSet:=CharSet.Auto, ExactSpelling:=True)> _
    Friend Shared Function SetForegroundWindow(ByVal hwnd As IntPtr) As Long
    End Function
    '
    <DllImport("user32.dll", CharSet:=CharSet.Auto, ExactSpelling:=True)> _
    Friend Shared Function FindWindowW( _
    ByVal lpClassName As String, _
    ByVal lpWindowName As String _
    ) As IntPtr
    End Function
    '
    <DllImport("user32.dll", CharSet:=CharSet.Auto, ExactSpelling:=True)> _
    Friend Shared Function FindWindowExW( _
    ByVal hwndParent As IntPtr, _
    ByVal hwndChildAfter As IntPtr, _
    ByVal lpszClass As String, _
    ByVal lpszWindow As String _
    ) As IntPtr
    End Function
    '
    <DllImport("user32.dll", CharSet:=CharSet.Auto, ExactSpelling:=True)> _
    Friend Shared Function PostMessageW(ByVal hwnd As IntPtr, ByVal wMsg As IntPtr, ByVal wParam As IntPtr, ByVal lParam As IntPtr) As IntPtr
    End Function
    '
    <DllImport("user32.dll", CharSet:=CharSet.Auto, ExactSpelling:=True)> _
    Friend Shared Function SendMessageW(ByVal hwnd As IntPtr, ByVal wMsg As Integer, ByVal wParam As Integer, ByVal lParam As String) As Integer
    End Function
    '
    Private Const KeyDown As Integer = &H0
    Private Const KeyUp As Integer = &H2
    '
    <DllImport("user32.dll")> _
    Private Shared Function SendInputW( _
        ByVal nInputs As Integer, _
        ByVal pInputs() As INPUT, _
        ByVal cbSize As Integer) As Integer
    End Function
    <StructLayout(LayoutKind.Explicit)> _
    Private Structure INPUT
        'Field offset 32 bit machine 4
        '64 bit machine 8
        <FieldOffset(0)> _
        Public type As Integer
        <FieldOffset(8)> _
        Public mi As MOUSEINPUT
        <FieldOffset(8)> _
        Public ki As KEYBDINPUT
        <FieldOffset(8)> _
        Public hi As HARDWAREINPUT
    End Structure
    Private Structure MOUSEINPUT
        Public dx As Integer
        Public dy As Integer
        Public mouseData As Integer
        Public dwFlags As Integer
        Public time As Integer
        Public dwExtraInfo As IntPtr
    End Structure
    Private Structure KEYBDINPUT
        Public wVk As Short
        Public wScan As Short
        Public dwFlags As Integer
        Public time As Integer
        Public dwExtraInfo As IntPtr
    End Structure
    Private Structure HARDWAREINPUT
        Public uMsg As Integer
        Public wParamL As Short
        Public wParamH As Short
    End Structure

    '================[ Constant Area ]==================
    Public Const WM_PASTE As UInt32 = &H302
    Public Const WM_KEYDOWN As UInt32 = &H100
    Public Const WM_KEYUP As UInt32 = &H101
    Public Const WM_CLOSE As UInt32 = &H10
    Public Const VK_TAB As UInt32 = &H9
    Public Const VK_ENTER As UInt32 = &HD
    Public Const WM_SETTEXT As UInt32 = &HC
    Public Const WM_GETTEXT As UInt32 = &HD

End Class
