﻿Partial Public Class Form01_Main


    Public Delegate Sub funcCBProgressBar(ByVal nLengthAdd As Integer)
    Private m_refwinobjProgressBar As ProgressBar = Nothing
    ' Private m_refwinobjProgressBarExt As ToolStripProgressBar 
    'Private m_refwinobjProgressBarExtForm03 As ToolStripProgressBar
    '
    Private Sub ProgressBarInit()
        m_refwinobjProgressBar = ProgressBar1
    End Sub
    '
    Private Sub ProgressBarStart_ToolStripGeneric(ByRef tspb As ToolStripProgressBar, ByVal nTotalLength As Integer)
        If (tspb IsNot Nothing) Then
            tspb.Maximum = nTotalLength
            tspb.Minimum = 0
            tspb.Value = 0
        End If
    End Sub
    Public Sub ProgressBarStart(ByVal nTotalLength As Integer)
        m_refwinobjProgressBar.Maximum = nTotalLength
        m_refwinobjProgressBar.Minimum = 0
        m_refwinobjProgressBar.Value = 0
        m_refwinobjProgressBar.Visible = True
        'ProgressBarStart_ToolStripGeneric(m_refwinobjProgressBarExt, nTotalLength)
        'ProgressBarStart_ToolStripGeneric(m_refwinobjProgressBarExtForm03, nTotalLength)
    End Sub

    Private Sub ProgressBarUpdate_GenericToolStrip(ByRef tspb As ToolStripProgressBar, ByVal nLengthAdd As Integer, Optional ByVal bInstanceValue As Boolean = False)
        If (tspb Is Nothing) Then
            Return
        End If
        '
        tspb.Visible = True
        '
        If (bInstanceValue) Then
            tspb.Value = nLengthAdd
        Else
            Dim nValueFinal As Integer
            nValueFinal = tspb.Value + nLengthAdd
            If (nValueFinal > tspb.Maximum) Then
                nValueFinal = tspb.Maximum
            End If
            If (nValueFinal < 0) Then
                nValueFinal = 0
            End If
            tspb.Value = nValueFinal
        End If
    End Sub
    Public Sub ProgressBarUpdate(ByVal nLengthAdd As Integer, Optional ByVal bInstanceValue As Boolean = False)
        'Dim nLengthAddExt As Integer = nLengthAdd
        'Dim bInstanceValueExt As Boolean = bInstanceValue
        '
        If (Not m_refwinobjProgressBar.Visible) Then
            m_refwinobjProgressBar.Visible = True
        End If
        '
        If (bInstanceValue) Then
            m_refwinobjProgressBar.Value = nLengthAdd
        Else
            Dim nValueFinal As Integer
            nValueFinal = m_refwinobjProgressBar.Value + nLengthAdd
            If (nValueFinal > m_refwinobjProgressBar.Maximum) Then
                nValueFinal = m_refwinobjProgressBar.Maximum
            End If
            If (nValueFinal < 0) Then
                nValueFinal = 0
            End If
            m_refwinobjProgressBar.Value = nValueFinal
        End If
        '==================[ Progress Bar In Form04 ]====================
        'ProgressBarUpdate_GenericToolStrip(m_refwinobjProgressBarExt, nLengthAdd, bInstanceValue)
        'ProgressBarUpdate_GenericToolStrip(m_refwinobjProgressBarExtForm03, nLengthAdd, bInstanceValue)
    End Sub

    Public Sub ProgressBarEnd()
        'Reset to zero
        ProgressBarUpdate(0, True)
        '
        m_refwinobjProgressBar.Visible = False

#If 0 Then
        If (m_refwinobjProgressBarExt IsNot Nothing) Then
            m_refwinobjProgressBarExt.Visible = False
        End If
        If (m_refwinobjProgressBarExtForm03 IsNot Nothing) Then
            m_refwinobjProgressBarExtForm03.Visible = False
        End If
#End If
    End Sub
    '
End Class
