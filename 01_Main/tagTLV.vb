﻿'
Public Structure tagTLV
    Public m_strTag As String
    Public m_nLen As Integer
    Public m_strCommentTrail As String
    Public m_arrayTLVal() As Byte
    Public m_lTxtLn As Long
    '-----------------------[ Configurations Group / AID Info Parser  ]-----------------------
#If 1 Then
    Private Shared s_strlstTLVs_Legacy_NEO100_or_AR215_older() As String = New String() { _
        "FFE4", "FFF7", "FFF8", "9F21", "9A", "FFEE1D" _
        , "9C", "9F06", "DF59" _
        , "DF8116", "DF8129", "DF891B", "DFEE23", "DF61" _
        , "DF4B", "FF8106", "DF8115", "FFEE01", "FFEE12", "", "", "", "", "", "", "", "", "", "", "", "", "" _
        }
    Private Shared s_strlstTLVs_NEO200_or_AR300() As String = New String() { _
        "DFEE2D", "DFEE7E", "DFEE37", "9F21", "9A", "FFEE1D" _
        , "9C", "9F06", "DF59" _
        , "DF8116", "DF8129", "DF891B", "DFEE23", "DFEE61" _
        , "DF4B", "FF8106", "DF8115", "FFEE01", "FFEE12", "", "", "", "", "", "", "", "", "", "", "", "", "" _
        }
#End If
    Public Shared TAG_FFE4_or_DFEE2D_GrpID As String = "FFE4"           'Group ID
    Public Shared TAG_FFF7_or_DFEE7E_BurstMode As String = "FFF7"           'UI Mode = &H0 --> ViVO UI; = &H2 --> Visa Wave; = &H3 --> EMEA UI
    Public Shared TAG_FFF8_or_DFEE37_UIScheme As String = "FFF8"           'UI Mode = &H0 --> ViVO UI; = &H2 --> Visa Wave; = &H3 --> EMEA UI
    Public Shared TAG_9F21_Time As String = "9F21"
    Public Shared TAG_9A_Date As String = "9A"
    '---[ FFEE1D, Pre-PAN Settings, NEO Spec. ]---
    '    Byte 1: PrePAN, value scope is [0, 6], 
    'Byte 2: PosPAN, value scope is [0, 4], 
    'Byte 3: MaskAscii, value scope is [20h, 7Eh], 
    'Byte 4: MaskHex, value scope is [0Ah, 0Fh]
    'Byte 5: Expire date output option, 
    '0x30=Mask, 0x31=NotMask, default 0x31
    Public Shared TAG_FFEE1D_PrePAN As String = "FFEE1D"
    Public Shared TAG_DF60_ProductType As String = "DF60"
    '
    Public Enum ENUM_FFF8_UI_SCHEME As Byte
        'Value = 00:ViVOpay User Interface (default) Value = 02:Visa Wave User Interface Value = 03:EMEA User Interface
        x00_VIVOPAY = &H0
        x02_VISA_WAVE = &H2
        x03_EMEA = &H3

        xFF_UNKNOWN = &HFF
    End Enum

    Public Shared TAG_9C_TxnType As String = "9C"                   'Transaction Type
    Public Shared TAG_9F06_TermID_AID As String = "9F06"       ' AID = Terminal ID
    Public Shared TAG_DF59_ViVO_GBCM As String = "DF59"       ' ViVO tag, GR backward Compatibility Mode
    'Public Shared TAG_FFE4 As String = "FFE4"
    'Public Shared TAG_9C As String = "9C"
    'Public Shared TAG_9A As String = "9A"
    'Public Shared TAG_9F21 As String = "9F21"
    'Public Shared TAG_9F06 As String = "9F06"
    '-----------------------[ Transaction Response Tag - Major ]-----------------------
    Public Shared TAG_DF8116 As String = "DF8116"
    Public Shared TAG_DF8129 As String = "DF8129"
    Public Shared TAG_DF891B_PollMode As String = "DF891B"
    Public Shared TAG_DFEE23 As String = "DFEE23"
    Public Shared TAG_DF61_or_DFEE61 As String = "DF61"
    'Public Shared TAG_DFEE61 As String = "DFEE61"
    Public Shared TAG_DF4B As String = "DF4B" 'U 1D5, TTK Version
    Public Shared TAG_FF8106 As String = "FF8106"
    Public Shared TAG_DF8115 As String = "DF8115"
    Public Shared TAG_FFEE01 As String = "FFEE01"
    Public Shared TAG_FFEE12 As String = "FFEE12"
    Public Shared s_strPrintoutTransactionMsgDelimiter As String = " --> "
    Public Shared s_strPrintoutTransactionMsgDelimiterRev As String = " <-- "
    Public Shared s_strPrintoutTransactionMsgNestedTagPrefix As String = "<--------------------------------------- End "
    Public Shared s_strPrintoutTransactionMsgNestedTagPostfix As String = " Nested Tag"
    Public Shared s_strPrintoutTransactionUnknownTag As String = "Unknown Tag in This Payment"

    'Return 0 If  ...
    '1) Value Array Byte = NULL
    '2) value array length = 0
    '3) composed value = 0
    Public ReadOnly Property u32Val As UInteger
        Get
            Dim u32Tmp As UInt32 = 0
            '
            While (True)
                If (m_arrayTLVal Is Nothing) Then
                    Exit While
                End If
                '
                If (m_arrayTLVal.Length = 0) Then
                    Exit While
                End If
                '
                For Each itrByte As Byte In m_arrayTLVal
                    u32Tmp = (u32Tmp << 8) + itrByte
                Next
                '
                Exit While
            End While

            '
            Return u32Tmp
        End Get
    End Property
    '
    'Purpose - Call this init() after the Reader has been identified as 3 Byte TLV supported
    '3-byte TLV ref doc link: 
    'https://atlassian.idtechproducts.com/confluence/pages/viewpage.action?pageId=7766038
    Public Shared Sub Init(Optional ByVal bIs3ByteTLVs As Boolean = False)
        Dim refobj_strlstTLVs_Generic As String()
        Dim nIdx As Integer = 0
        If (bIs3ByteTLVs) Then
            refobj_strlstTLVs_Generic = s_strlstTLVs_NEO200_or_AR300
        Else
            refobj_strlstTLVs_Generic = s_strlstTLVs_Legacy_NEO100_or_AR215_older
        End If

        TAG_FFE4_or_DFEE2D_GrpID = refobj_strlstTLVs_Generic(nIdx)           'FFE4, Group ID
        nIdx = nIdx + 1

        TAG_FFF7_or_DFEE7E_BurstMode = refobj_strlstTLVs_Generic(nIdx) ' = "FFF7"           'UI Mode = &H0 --> ViVO UI; = &H2 --> Visa Wave; = &H3 --> EMEA UI
        nIdx = nIdx + 1

        TAG_FFF8_or_DFEE37_UIScheme = refobj_strlstTLVs_Generic(nIdx) '"FFF8"           'UI Mode = &H0 --> ViVO UI; = &H2 --> Visa Wave; = &H3 --> EMEA UI
        nIdx = nIdx + 1

        TAG_9F21_Time = refobj_strlstTLVs_Generic(nIdx) '"9F21"
        nIdx = nIdx + 1

        TAG_9A_Date = refobj_strlstTLVs_Generic(nIdx) '"9A"
        nIdx = nIdx + 1

        '---[ FFEE1D, Pre-PAN Settings, NEO Spec. ]---
        '    Byte 1: PrePAN, value scope is [0, 6], 
        'Byte 2: PosPAN, value scope is [0, 4], 
        'Byte 3: MaskAscii, value scope is [20h, 7Eh], 
        'Byte 4: MaskHex, value scope is [0Ah, 0Fh]
        'Byte 5: Expire date output option, 
        '0x30=Mask, 0x31=NotMask, default 0x31
        TAG_FFEE1D_PrePAN = refobj_strlstTLVs_Generic(nIdx) '"FFEE1D"
        nIdx = nIdx + 1

        TAG_9C_TxnType = refobj_strlstTLVs_Generic(nIdx) '"9C"                   'Transaction Type
        nIdx = nIdx + 1

        TAG_9F06_TermID_AID = refobj_strlstTLVs_Generic(nIdx) '"9F06"       ' AID = Terminal ID
        nIdx = nIdx + 1

        TAG_DF59_ViVO_GBCM = refobj_strlstTLVs_Generic(nIdx) '"DF59"       ' ViVO tag, GR backward Compatibility Mode
        nIdx = nIdx + 1

        'TAG_FFE4 = refobj_strlstTLVs_Generic(nIdx) '"FFE4"
        'nIdx = nIdx + 1

        'TAG_9C = refobj_strlstTLVs_Generic(nIdx) '"9C"
        'nIdx = nIdx + 1

        'TAG_9A = refobj_strlstTLVs_Generic(nIdx) '"9A"
        'nIdx = nIdx + 1

        'TAG_9F21 = refobj_strlstTLVs_Generic(nIdx) '"9F21"
        'nIdx = nIdx + 1

        'TAG_9F06 = refobj_strlstTLVs_Generic(nIdx) '"9F06"
        'nIdx = nIdx + 1

        '-----------------------[ Transaction Response Tag - Major ]-----------------------
        TAG_DF8116 = refobj_strlstTLVs_Generic(nIdx) '"DF8116"
        nIdx = nIdx + 1

        TAG_DF8129 = refobj_strlstTLVs_Generic(nIdx) '"DF8129"
        nIdx = nIdx + 1

        TAG_DF891B_PollMode = refobj_strlstTLVs_Generic(nIdx) '"DF891B"
        nIdx = nIdx + 1

        TAG_DFEE23 = refobj_strlstTLVs_Generic(nIdx) '"DFEE23"
        nIdx = nIdx + 1

        TAG_DF61_or_DFEE61 = refobj_strlstTLVs_Generic(nIdx) '"DF61"
        nIdx = nIdx + 1

        'TAG_DFEE61 = refobj_strlstTLVs_Generic(nIdx) '"DFEE61"
        'nIdx = nIdx + 1

        TAG_DF4B = refobj_strlstTLVs_Generic(nIdx) '"DF4B" 'U 1D5, TTK Version
        nIdx = nIdx + 1

        TAG_FF8106 = refobj_strlstTLVs_Generic(nIdx) '"FF8106"
        nIdx = nIdx + 1

        TAG_DF8115 = refobj_strlstTLVs_Generic(nIdx) '"DF8115"
        nIdx = nIdx + 1

        TAG_FFEE01 = refobj_strlstTLVs_Generic(nIdx) '"FFEE01"
        nIdx = nIdx + 1

        TAG_FFEE12 = refobj_strlstTLVs_Generic(nIdx) '"FFEE12"
        nIdx = nIdx + 1
    End Sub

    '
    Sub New(ByRef strTag As String, ByRef arrayByteVal() As Byte, Optional ByRef strComment As String = "")
        'Don't care Tag Value
        m_strTag = strTag

        m_strCommentTrail = strComment

        If arrayByteVal Is Nothing Then
            m_arrayTLVal = Nothing
            m_nLen = 0
        Else
            ' Remark Following for Longer (arrayByteVal.Length > nLen) Situation
            'If (nLen <> arrayByteVal.Length) Then
            '    Dim strMsg As String = "[Err:tagTLV:TLVauleFromString2ByteArray(): param invalid] Tag = " + m_strTag + "nLen = " & nLen & ", TLVal = " + arrayByteVal.ToString()
            '    Throw New Exception(strMsg)
            'End If

            If (arrayByteVal.Count > 0) Then
                m_nLen = arrayByteVal.Count
                'm_arrayTLVal = CType(arrayByteVal.Clone(), Byte())
                m_arrayTLVal = New Byte(m_nLen - 1) {}
                Array.Copy(arrayByteVal, m_arrayTLVal, m_nLen)
            Else
                m_nLen = 0
            End If
        End If

        m_lTxtLn = 0
    End Sub

    Sub New(ByRef strTag As String, ByRef byteVal As Byte, Optional ByRef strComment As String = "")
        'Don't care Tag Value
        m_strTag = strTag

        m_strCommentTrail = strComment

        m_arrayTLVal = New Byte(0) {byteVal}
        m_nLen = 1
        '
        m_lTxtLn = 0
    End Sub

    ' For speeding up passing parameter purpose, to use ByRef should reduces the function call time consumption 
    ' INPUT: For Example Format , strVal = "00 A0 08 19 ED"
    Sub New(ByRef strTag As String, ByVal nLen As Integer, ByRef strVal As String, Optional ByRef strComment As String = "")
        m_strTag = strTag
        m_nLen = nLen
        '
        m_strCommentTrail = strComment
        '
        If strVal Is Nothing Then
            m_arrayTLVal = Nothing
            m_nLen = 0
        Else
            ClassTableListMaster.TLVauleFromString2ByteArray(strVal, nLen, m_arrayTLVal)
        End If
        '
        m_lTxtLn = 0
    End Sub
    '
    ' For speeding up passing parameter purpose, to use ByRef should reduces the function call time consumption 
    ' INPUT: For Example Format , strVal = "00 A0 08 19 ED"
    Sub New(ByVal strTag As String, ByVal strVal As String, Optional ByRef strComment As String = "")
        'Normalize for Tag ID
        strTag = Replace(strTag, " ", "")

        If (strVal Is Nothing) Then
            m_nLen = 0
            m_arrayTLVal = Nothing
        Else
            'Normalize First
            strVal = Replace(strVal, " ", "")

            m_strTag = strTag
            m_nLen = strVal.Length \ 2
            '
            ClassTableListMaster.TLVauleFromString2ByteArray(strVal, m_arrayTLVal)

        End If

        m_strCommentTrail = strComment
        '
        '
        m_lTxtLn = 0
    End Sub
    '
    Sub New(ByRef strTag As String, ByVal nLen As Integer, ByRef arrayByteVal() As Byte, Optional ByRef strComment As String = "")
        m_strTag = strTag
        m_nLen = nLen
        m_strCommentTrail = strComment

        If arrayByteVal Is Nothing Then
            m_arrayTLVal = Nothing
            m_nLen = 0
        Else
            ' Remark Following for Longer (arrayByteVal.Length > nLen) Situation
            'If (nLen <> arrayByteVal.Length) Then
            '    Dim strMsg As String = "[Err:tagTLV:TLVauleFromString2ByteArray(): param invalid] Tag = " + m_strTag + "nLen = " & nLen & ", TLVal = " + arrayByteVal.ToString()
            '    Throw New Exception(strMsg)
            'End If

            If (nLen > 0) Then
                'm_arrayTLVal = CType(arrayByteVal.Clone(), Byte())
                m_arrayTLVal = New Byte(nLen - 1) {}
                Array.Copy(arrayByteVal, m_arrayTLVal, nLen)
            Else
                m_arrayTLVal = Nothing
            End If
        End If

        m_lTxtLn = 0
    End Sub

    Sub New(ByVal arrayBytesTag As Byte(), ByVal nLen As Integer, ByVal arrayByteVal() As Byte, Optional ByVal strComment As String = "")
        Dim strTag As String = ""

        ClassTableListMaster.ConvertFromArrayByte2String(arrayBytesTag, strTag, False)

        m_strTag = strTag
        m_nLen = nLen
        m_strCommentTrail = strComment

        If arrayByteVal Is Nothing Then
            m_arrayTLVal = Nothing
            m_nLen = 0
        Else
            ' Remark Following for Longer (arrayByteVal.Length > nLen) Situation
            'If (nLen <> arrayByteVal.Length) Then
            '    Dim strMsg As String = "[Err:tagTLV:TLVauleFromString2ByteArray(): param invalid] Tag = " + m_strTag + "nLen = " & nLen & ", TLVal = " + arrayByteVal.ToString()
            '    Throw New Exception(strMsg)
            'End If

            If (nLen > 0) Then
                'm_arrayTLVal = CType(arrayByteVal.Clone(), Byte())
                m_arrayTLVal = New Byte(nLen - 1) {}
                Array.Copy(arrayByteVal, m_arrayTLVal, nLen)
            Else
                m_arrayTLVal = Nothing
            End If
        End If

        m_lTxtLn = 0
    End Sub

    Property strVal(Optional ByVal bDelimiterSpace As Boolean = False) As String
        Get
            Dim strTmp As String = Nothing
            If (m_nLen < 1) Then
                strTmp = ""
            Else
                ClassTableListMaster.ConvertFromArrayByte2String(m_arrayTLVal, strTmp, bDelimiterSpace)
            End If

            Return strTmp
        End Get
        Set(value As String)
            'Release native
            Erase m_arrayTLVal
            'Update to new one
            ClassTableListMaster.TLVauleFromString2ByteArray(value, m_arrayTLVal)
        End Set
    End Property

    Sub Reset(ByRef strTag As String, ByVal nLen As Integer, ByRef strVal As String, Optional ByRef strComment As String = "")
        Cleanup()
        '
        m_strTag = strTag
        m_nLen = nLen
        m_strCommentTrail = strComment
        ClassTableListMaster.TLVauleFromString2ByteArray(strVal, nLen, m_arrayTLVal)
        m_lTxtLn = 0
    End Sub

    Sub Reset(ByRef strTag As String, ByVal nLen As Integer, ByRef arrayByteVal() As Byte, Optional ByRef strComment As String = "")
        Cleanup()
        '
        m_strTag = strTag
        '
        If m_arrayTLVal IsNot Nothing Then
            If m_arrayTLVal.Length <> nLen Then
                Erase m_arrayTLVal
                m_arrayTLVal = Nothing
            End If
        End If

        '
        If (nLen <> arrayByteVal.Length) Then
            Dim strMsg As String = "[Err:tagTLV:Reset():param invalid] nLen=" & nLen & ", arrayByteVal= " & arrayByteVal.Length
            Throw New Exception(strMsg)
        End If
        '
        m_nLen = nLen
        m_strCommentTrail = strComment
        If (nLen > 0) Then
            m_arrayTLVal = CType(arrayByteVal.Clone(), Byte())
        End If

        m_lTxtLn = 0
    End Sub

    Sub Cleanup()
        If (m_arrayTLVal IsNot Nothing) Then
            Erase m_arrayTLVal
            m_arrayTLVal = Nothing
        End If
    End Sub

    Public Shared Sub Combine(tlv1 As tagTLV, tlv2 As tagTLV, ByVal strTag As String, ByRef o_tlv3 As tagTLV)
        Dim nTlvNewDataLength, nLenTmp As Integer
        Dim arrayByteData As Byte()
        Dim btlv1Available As Boolean = False
        Dim btlv2Available As Boolean = False

        nTlvNewDataLength = 0

        If ((Not tlv1.Equals(Nothing)) And (tlv1.m_arrayTLVal IsNot Nothing)) Then
            nTlvNewDataLength = tlv1.m_nLen + 1 + (tlv1.m_strTag.Length / 2)
            btlv1Available = True
        End If
        '
        If ((Not tlv2.Equals(Nothing)) And (tlv2.m_arrayTLVal IsNot Nothing)) Then
            nTlvNewDataLength = nTlvNewDataLength + (tlv2.m_nLen + 1 + (tlv2.m_strTag.Length / 2))
            btlv2Available = True
        End If
        '
        If (nTlvNewDataLength > 0) Then
            o_tlv3 = New tagTLV(strTag, New Byte(nTlvNewDataLength - 1) {})
            ' Put data together
            ' Initial Destination Position 
            nLenTmp = 0
            If (btlv1Available) Then
                arrayByteData = tlv1.GetArrayByte()
                Array.Copy(arrayByteData, 0, o_tlv3.m_arrayTLVal, nLenTmp, arrayByteData.Count)
                nLenTmp = nLenTmp + arrayByteData.Count
                Erase arrayByteData
            End If
            '
            If (btlv2Available) Then
                arrayByteData = tlv2.GetArrayByte()
                Array.Copy(arrayByteData, 0, o_tlv3.m_arrayTLVal, nLenTmp, arrayByteData.Count)
                nLenTmp = nLenTmp + arrayByteData.Count
                Erase arrayByteData
            End If
        Else
            o_tlv3 = Nothing
        End If
    End Sub

    Public Function GetArrayByte() As Byte()
        Dim arrayByteData As Byte() = Nothing
        Dim arrayByteTag As Byte()
        Dim nCopyBinaryLen As Integer

        If (m_nLen < 1) Then
            Return Nothing
        End If
        '
        Try
            'Get Full Length = Tag Name Byte Length + 1 byte Length + Tag Data Byte Length
            'Ex: "FFEE01" --> Binary Length = 6/2 = 3 bytes
            nCopyBinaryLen = m_strTag.Length / 2
            arrayByteData = New Byte(m_nLen + 1 + nCopyBinaryLen - 1) {}
            'Put Tag Name Converts it to Binary Format and put it into buffer
            ClassTableListMaster.TLVauleFromString2ByteArray(m_strTag, arrayByteData, False)
            'Put Length + Data
            arrayByteData(nCopyBinaryLen) = CType(m_nLen, Byte)
            nCopyBinaryLen = nCopyBinaryLen + 1
            Array.Copy(m_arrayTLVal, 0, arrayByteData, nCopyBinaryLen, m_nLen)

        Catch ex As Exception
            Form01_Main.GetInstance().Log("[Err] tagTLV.GetArrayByte() = " + ex.Message)
            If (arrayByteData IsNot Nothing) Then
                Erase arrayByteData
                arrayByteData = Nothing
            End If
        Finally
            Erase arrayByteTag
        End Try
        '
        Return arrayByteData
    End Function

End Structure

'========================================




'TLV Table Creation Op
Public Structure tagTLVNewOp
    '
    Public Delegate Sub ComposerDelegate(ByRef objTLV As tagTLV, ByRef idgCmdData As tagIDGCmdData)
    Private m_pfuncCbComposer As ComposerDelegate
    Public Delegate Sub Parser2PrintoutDelegate(ByVal refobjTLVOp As tagTLVNewOp, ByRef refobjTLV As tagTLV, ByRef o_tagTxnInfo As ClassTransaction.TAG_TXN_PARSE_RESPONSE_INFO)
    Private m_pfuncCbParser2Printout As Parser2PrintoutDelegate
    '

    ' Dummy Callback function is for dummy purpose
    Private Sub ptrFuncCbDummyComposer(ByRef refobjTLVAttr As tagTLV, ByRef idgCmdData As tagIDGCmdData)
        Throw New Exception("[Warning] tagTLVNewOp.m_pfuncCbDummyComposer (T=" + refobjTLVAttr.m_strTag + ",L=" & refobjTLVAttr.m_nLen & ") is being executed")
    End Sub

    Private Const m_nLogTxtFormatedLineWidthHexIndent As Integer = 28
    Private Sub ptrFuncCbDummyParser2Printout(ByVal tlvValOp As tagTLVNewOp, ByRef refobjTLVAttr As tagTLV, ByRef o_tagTxnInfo As ClassTransaction.TAG_TXN_PARSE_RESPONSE_INFO)
        'Throw New Exception("[Warning] tagTLVNewOp.m_pfuncCbDummyParser2Printout (T=" + refobjTLVAttr.m_strTag + ",L=" & refobjTLVAttr.m_nLen & ") is being executed")
        Dim strMsg = "", strTmp = "", strTmp2 As String = ""
        Dim nIndent As Integer
        ' nAlignmentMode = 0 --> No Data
        '                                  = 1 --> 0 < nLen <= 3
        '                                  = 2 --> 3 < nLen <= 16
        '                                  = 3 --> 16 < nLen
        Dim nAlignmentMode As Integer = 0
        '76 chars per line
        'Const nLogTxtLineWidth As Integer = 73
        Const nLogTxtLineWidthRightRest As Integer = 7
        Const nLogTxtFormatedLineWidth As Integer = 16
        ' 28 = 76 - 48 = nLogTxtLineWidth - (nLogTxtFormatedLineWidth * 3)

        Const strFomratedIndentForLogTxtLen16 As String = "                         "
        'Dim strFomratedIndentForLogTxtLen16Cust As String
        '


        If refobjTLVAttr.m_nLen > 0 Then
            ClassTableListMaster.ConvertFromArrayByte2String(refobjTLVAttr.m_arrayTLVal, strTmp2)
            nAlignmentMode = 1
        Else
            nAlignmentMode = 0
            strTmp2 = "N/A"
        End If

        If (1) Then
            ' Indent + tag + tagTLV.s_strPrintoutTransactionMsgDelimiter + strComment + strDotLine+ " = " + ValueInHexString
            With refobjTLVAttr
                If (nAlignmentMode = 1) Then
                    If (.m_nLen > nLogTxtLineWidthRightRest) Then
                        If (.m_nLen > nLogTxtFormatedLineWidth) Then
                            nAlignmentMode = 4 '3
                        Else
                            nAlignmentMode = 2
                        End If
                    End If
                End If
                strMsg = .m_strTag + tagTLV.s_strPrintoutTransactionMsgDelimiter + tlvValOp.m_strComment
                PktParser2Printout_Common_FormatLine_GetDotsString(strMsg, nIndent, strTmp)
                strMsg = strMsg + strTmp + " = "
                Select Case (nAlignmentMode)
                    Case 0, 1, 2
                        ' Example:  
                        '      FFEE01 --> Comment ........................... = 00 01 02
                        strMsg = strMsg + strTmp2
                        o_tagTxnInfo.Add2PrintoutAbs(strMsg, nIndent)
                        'Case 2
                        ' Example:  
                        '      FFEE01 --> Comment ............................ = 00 01 02 03 04 05 06 07 08 09 0A 0B 0C 0D 0E 0F
                        'o_tagTxnInfo.Add2PrintoutAbs(strMsg + strTmp2, nIndent)
                        'strFomratedIndentForLogTxtLen16Cust = New String(" "c, strMsg.Length)
                        'o_tagTxnInfo.Add2PrintoutAbs(strFomratedIndentForLogTxtLen16Cust + strTmp2, 0)
                    Case 3
                        ' Example:  
                        '      FFEE01 --> Comment ............................ = 
                        '        00 01 02 03 04 05 06 07 08 09 0A 0B 0C 0D 0E 0F
                        '        10 11 12 13 14 15 16 17 18 19 1A 1B 1C 1D 1E 1F
                        '        20 21 22 23 24 25 26 27 28 29 2A 2B 2C 2D 2E 2F
                        '        30 31 32 33 34 35
                        o_tagTxnInfo.Add2PrintoutAbs(strMsg, nIndent)
                        Dim strMsgList() As String = New String() {""}
                        Dim nIdx As Integer
                        ClassTableListMaster.ConvertFromArrayByte2StringListFormatedBlankRight(.m_arrayTLVal, .m_nLen, strMsgList, 16)
                        '
                        For nIdx = 0 To strMsgList.Count - 1
                            o_tagTxnInfo.Add2PrintoutAbs(strFomratedIndentForLogTxtLen16 + strMsgList(nIdx), nIndent)
                        Next
                        '
                        Erase strMsgList
                    Case 4
                        ' Example:  In Wide Main Log WIndow Mode
                        '      FFEE01 --> Comment ............................ = 00 01 02 03 04 05 06 07 08 09 0A 0B 0C 0D 0E 0F
                        '                                                                            10 11 12 13 14 15 16 17 18 19 1A 1B 1C 1D 1E 1F
                        '                                                                            20 21 22 23 24 25 26 27 28 29 2A 2B 2C 2D 2E 2F
                        '                                                                            30 31 32 33 34 35
                        Dim strMsgList() As String = New String() {""}
                        Dim strIndent As String = New String(" "c, strMsg.Length() + nIndent)
                        Dim nIdx As Integer
                        ClassTableListMaster.ConvertFromArrayByte2StringListFormatedBlankRight(.m_arrayTLVal, .m_nLen, strMsgList, 16)
                        o_tagTxnInfo.Add2PrintoutAbs(strMsg + strMsgList(0), nIndent)
                        '
                        For nIdx = 1 To strMsgList.Count - 1
                            o_tagTxnInfo.Add2PrintoutAbs(strIndent + strMsgList(nIdx), 0)
                        Next
                        '
                        Erase strMsgList
                End Select
                'If (.m_nLen > nLogTxtLineWidthRightRest) Then
                '    strMsg = strMsg + strTmp + " = "
                '    o_tagTxnInfo.Add2PrintoutAbs(strMsg, nIndent)

                '    '

                '    '


                'Else
                '    strMsg = strMsg + strTmp + " = " + strTmp2
                '    o_tagTxnInfo.Add2PrintoutAbs(strMsg, nIndent)
                'End If
            End With
        Else
            strMsg = tlvValOp.m_strComment + " ( " + refobjTLVAttr.m_strTag + " )  : " + strTmp
            o_tagTxnInfo.Add2Printout(strMsg, nIndent)
        End If


    End Sub

    Public Shared m_dictPktParser2Printout_MC_DF8116_Byte1_MessageIdentifier As Dictionary(Of Byte, String) = New Dictionary(Of Byte, String) From {
                {&H17, "CARD READ OK"},
                {&H21, "TRY AGAIN"},
                {&H3, "APPROVED"},
                {&H1A, "APPROVED - SIGN"},
                {&H7, "DECLINED"},
                {&H1C, "ERROR - OTHER CARD"},
                {&H1D, "INSERT CARD"},
                {&H20, "SEE PHONE"},
                {&H1B, "AUTHORISING - PLEASE WAIT"},
                {&H1E, "CLEAR DISPLAY"},
                {&HFF, "N/A"}
                }
    Public Shared m_dictPktParser2Printout_MC_DF8116_Byte2_Status As Dictionary(Of Byte, String) = New Dictionary(Of Byte, String) From {
                {&H0, "NOT READY"},
                {&H1, "IDLE"},
                {&H2, "READY TO READ"},
                {&H3, "PROCESSING"},
                {&H4, "CARD READ SUCCESSFULLY"},
                {&H5, "PROCESSING ERROR"},
                {&HFF, "N/A"}
                }
    Public Shared m_dictPktParser2Printout_MC_DF8116_Byte13_ValueQualifier As Dictionary(Of Byte, String) = New Dictionary(Of Byte, String) From {
            {&H0, "NONE"},
            {&H1, "AMOUNT"},
            {&H2, "BALANCE"}
            }
    Public Shared m_strPktParser2Printout_MC_Common001_RFU As String = " ( RFU )"

    Public Shared Sub PktParser2Printout_MasterCard_DF8116_UserInterfaceRequestData(ByVal tlvValOp As tagTLVNewOp, ByRef refobjTLVAttr As tagTLV, ByRef o_tagTxnInfo As ClassTransaction.TAG_TXN_PARSE_RESPONSE_INFO)
        Dim strMsg = "", strTmp = "", strTmp2 As String = ""
        Dim nIdx As Integer = 0
        Dim nIndent As Integer

        'Dim byteVal As Byte
        Dim arrayByteData As Byte() = New Byte(7) {}
        Dim nArrayLen As Integer
        'Dim iteratorTxnPrintout As ClassUIDriver.TXN_RESPONSE_PARSER_2_PRINTOUT
        ' 1. Printout Tag Value
        tlvValOp.ptrFuncCbDummyParser2Printout(tlvValOp, refobjTLVAttr, o_tagTxnInfo)

        '2. Printout Detail Info
        o_tagTxnInfo.AddIndent()

        With refobjTLVAttr
            'Message Identifier, 1 byte
            PktParser2Printout_Common_ByteValueOperation(tlvValOp, refobjTLVAttr, o_tagTxnInfo, .m_arrayTLVal(nIdx), m_dictPktParser2Printout_MC_DF8116_Byte1_MessageIdentifier, "B1 : Message Identifier")
            'strMsg = "Message Identifier : "
            'byteVal = .m_arrayTLVal(nIdx)
            'If Not tagTLVNewOp.m_dictPktParser2Printout_MC_DF8116_MessageIdentifier.ContainsKey(byteVal) Then
            '    strMsg = strMsg + m_strPktParser2Printout_MC_Common001_RFU & byteVal & " )"
            'Else
            '    strMsg = strMsg + m_dictPktParser2Printout_MC_DF8116_MessageIdentifier.Item(byteVal) + " ( " & byteVal & " )"
            'End If
            'o_tagTxnInfo.m_listTxnResponseMsg.Add(New ClassUIDriver.TXN_RESPONSE_PARSER_2_PRINTOUT() With {.m_nLeftBoundaryIndx = o_tagTxnInfo.m_nIndentOp, .m_strMsg = strMsg})
            nIdx = nIdx + 1

            'Status, 1 byte
            PktParser2Printout_Common_ByteValueOperation(tlvValOp, refobjTLVAttr, o_tagTxnInfo, .m_arrayTLVal(nIdx), m_dictPktParser2Printout_MC_DF8116_Byte2_Status, "B2 : Status")
            'strMsg = "Status : "
            'byteVal = .m_arrayTLVal(nIdx)
            'If Not m_dictPktParser2Printout_MC_DF8116_Status.ContainsKey(byteVal) Then
            '    strMsg = strMsg + m_strPktParser2Printout_MC_Common001_RFU & byteVal & " )"
            'Else
            '    strMsg = strMsg + m_dictPktParser2Printout_MC_DF8116_Status.Item(byteVal) + " ( " & byteVal & " )"
            'End If
            'o_tagTxnInfo.m_listTxnResponseMsg.Add(New ClassUIDriver.TXN_RESPONSE_PARSER_2_PRINTOUT() With {.m_nLeftBoundaryIndx = o_tagTxnInfo.m_nIndentOp, .m_strMsg = strMsg})
            nIdx = nIdx + 1

            'Hold Time, 3 byte
            nArrayLen = 3
            strMsg = tagTLV.s_strPrintoutTransactionMsgDelimiter + "B" & (nIdx + 1) & "-" & (nIdx + nArrayLen - 1) & " : Hold Time"
            ClassTableListMaster.ConvertFromArrayByte2String(.m_arrayTLVal, nIdx, nArrayLen, strTmp)
            PktParser2Printout_Common_FormatLine_GetDotsString(strMsg, nIndent, strTmp2)
            strMsg = strMsg + strTmp2 + " = " + strTmp
            o_tagTxnInfo.Add2PrintoutAbs(strMsg, nIndent)
            nIdx = nIdx + nArrayLen

            'Language Preference, 8 bytes
            nArrayLen = 8
            Array.Copy(.m_arrayTLVal, nIdx, arrayByteData, 0, nArrayLen)
            strTmp = ""
            strMsg = "B" & (nIdx + 1) & "-" & (nIdx + nArrayLen - 1) & " : Language Preference"
            PktParser2Printout_Common_Hex2Ascii_Or_BinaryValue(arrayByteData, strTmp, strMsg, o_tagTxnInfo)
            nIdx = nIdx + nArrayLen

            'Value Qualifier, 1 byte,b8-5
            nArrayLen = 1
            PktParser2Printout_Common_ByteValueOperation(tlvValOp, refobjTLVAttr, o_tagTxnInfo, (.m_arrayTLVal(nIdx) >> 4) And &HF, m_dictPktParser2Printout_MC_DF8116_Byte2_Status, "B13b8-5 : Value Qualifier")
            nIdx = nIdx + nArrayLen

            'Value, 6 bytes
            nArrayLen = 6
            Array.Copy(.m_arrayTLVal, nIdx, arrayByteData, 0, nArrayLen)
            strTmp = ""
            strMsg = "B" & (nIdx + 1) & "-" & (nIdx + nArrayLen - 1) & " : Value"
            PktParser2Printout_Common_Hex2Ascii_Or_BinaryValue(arrayByteData, nArrayLen, strTmp, strMsg, o_tagTxnInfo, False)
            nIdx = nIdx + nArrayLen

            'Currency Code, 2 bytes
            nArrayLen = 2
            Array.Copy(.m_arrayTLVal, nIdx, arrayByteData, 0, nArrayLen)
            strTmp = ""
            strMsg = "B" & (nIdx + 1) & "-" & (nIdx + nArrayLen - 1) & " : Currency Code"
            PktParser2Printout_Common_Hex2Ascii_Or_BinaryValue(arrayByteData, nArrayLen, strTmp, strMsg, o_tagTxnInfo, False)
            nIdx = nIdx + nArrayLen

            Erase arrayByteData
        End With
        '
        o_tagTxnInfo.DecIndent()

    End Sub
    '
    Public Shared m_dictPktParser2Printout_TxnResponse_DF811F_SecurityCapability_Byte1 As Dictionary(Of Byte, tagTxnResponseBitValResolveInfo) = New Dictionary(Of Byte, tagTxnResponseBitValResolveInfo) From {
{&H8, New tagTxnResponseBitValResolveInfo(&H80, "SDA")},
{&H7, New tagTxnResponseBitValResolveInfo(&H40, "DDA")},
{&H6, New tagTxnResponseBitValResolveInfo(&H20, "Card Capture")},
{&H5, New tagTxnResponseBitValResolveInfo(&H10, "RFU")},
{&H4, New tagTxnResponseBitValResolveInfo(&H8, "CDA")},
{&H3, New tagTxnResponseBitValResolveInfo(&H4, "RFU")},
{&H2, New tagTxnResponseBitValResolveInfo(&H2, "RFU")},
{&H1, New tagTxnResponseBitValResolveInfo(&H1, "RFU")}
}
    Public Shared Sub PktParser2Printout_DF811F_SecurityCapability(ByVal tlvValOp As tagTLVNewOp, ByRef refobjTLVAttr As tagTLV, ByRef o_tagTxnInfo As ClassTransaction.TAG_TXN_PARSE_RESPONSE_INFO)
        Dim nOffset As Integer = 0
        Dim arrayByteData As Byte() = New Byte(7) {}

        'Dim nIdx, nIdxMax, nTmp As Integer
        ' 1. Printout Tag Value
        tlvValOp.ptrFuncCbDummyParser2Printout(tlvValOp, refobjTLVAttr, o_tagTxnInfo)

        '2. Printout Detail Info
        o_tagTxnInfo.AddIndent()
        PktParser2Printout_Common_ByteBitOperation(tlvValOp, refobjTLVAttr, o_tagTxnInfo, 1, refobjTLVAttr.m_arrayTLVal(0), m_dictPktParser2Printout_TxnResponse_DF811F_SecurityCapability_Byte1)
        o_tagTxnInfo.DecIndent()

    End Sub
    '
    Public Shared m_dictPktParser2Printout_TxnResponse_DF8128_IDSStatus_Byte1 As Dictionary(Of Byte, tagTxnResponseBitValResolveInfo) = New Dictionary(Of Byte, tagTxnResponseBitValResolveInfo) From {
{&H8, New tagTxnResponseBitValResolveInfo(&H80, "Read")},
{&H7, New tagTxnResponseBitValResolveInfo(&H40, "Write")}
}
    Public Shared Sub PktParser2Printout_DF8128_IDSStatus(ByVal tlvValOp As tagTLVNewOp, ByRef refobjTLVAttr As tagTLV, ByRef o_tagTxnInfo As ClassTransaction.TAG_TXN_PARSE_RESPONSE_INFO)
        Dim nOffset As Integer = 0
        Dim arrayByteData As Byte() = New Byte(7) {}
        ' 1. Printout Tag Value
        tlvValOp.ptrFuncCbDummyParser2Printout(tlvValOp, refobjTLVAttr, o_tagTxnInfo)

        '2. Printout Detail Info
        o_tagTxnInfo.AddIndent()
        PktParser2Printout_Common_ByteBitOperation(tlvValOp, refobjTLVAttr, o_tagTxnInfo, 1, refobjTLVAttr.m_arrayTLVal(0), m_dictPktParser2Printout_TxnResponse_DF8128_IDSStatus_Byte1)
        'With refobjTLVAttr
        '    o_tagTxnInfo.Add2Printout("Byte 1")
        '    o_tagTxnInfo.AddIndent()
        '    byteVal = .m_arrayTLVal(0)
        '    'Byte 1,
        '    For Each iteratorBitwiseOp In m_dictPktParser2Printout_TxnResponse_DF8128_IDSStatus_Byte1
        '        '   b8 , Read
        '        '   b7, Write
        '        If (iteratorBitwiseOp.Value.m_byteMaskBit And byteVal) = iteratorBitwiseOp.Value.m_byteMaskBit Then
        '            nIdx = 0
        '        Else
        '            nIdx = 1
        '        End If
        '        o_tagTxnInfo.Add2Printout(".Bit " & iteratorBitwiseOp.Key & " : " + m_dictPktParser2Printout_TxnResponseCommonStringYesNo.Item(nIdx) + " ) <-- " + iteratorBitwiseOp.Value.m_strMsg)
        '    Next
        '    '
        '    o_tagTxnInfo.DecIndent(2)
        'End With
        o_tagTxnInfo.DecIndent()

    End Sub
    '===========================[ DF8129 ]===========================
    Public Shared m_dictPktParser2Printout_MC_DF8129_Byte1_b8b5_Status As Dictionary(Of Byte, String) = New Dictionary(Of Byte, String) From {
           {&H1, "APPROVED"},
           {&H2, "DECLINED"},
           {&H3, "ONLINE REQUEST"},
           {&H4, "END APPLICATION"},
           {&H5, "SELECT NEXT"},
           {&H6, "TRY ANOTHER INTERFACE"},
           {&H7, "TRY AGAIN"},
           {&HF, "N/A"}
           }
    Public Shared m_dictPktParser2Printout_MC_DF8129_Byte2_b8b5_Start As Dictionary(Of Byte, String) = New Dictionary(Of Byte, String) From {
        {&H0, "A"},
        {&H1, "B"},
        {&H2, "C"},
        {&H3, "D"},
        {&HF, "N/A"}
       }
    Public Shared m_dictPktParser2Printout_MC_DF8129_Byte4_b8b5_CVM As Dictionary(Of Byte, String) = New Dictionary(Of Byte, String) From {
        {&H0, "NO CVM"},
        {&H1, "OBTAIN SIGNATURE"},
        {&H2, "ONLINE PIN"},
        {&H3, "CONFIRMATION CODE VERIFIED"},
        {&HF, "N/A"}
}
    Public Shared m_dictPktParser2Printout_MC_DF8129_Byte5_b8b5_UIReqDataRecDiscretionaryPresent As Dictionary(Of Byte, tagTxnResponseBitValResolveInfo) = New Dictionary(Of Byte, tagTxnResponseBitValResolveInfo) From {
        {&H8, New tagTxnResponseBitValResolveInfo(&H80, "UI Request on Outcome Present")},
        {&H7, New tagTxnResponseBitValResolveInfo(&H40, "UI Request on Restart Present")},
        {&H6, New tagTxnResponseBitValResolveInfo(&H20, "Data Record Present")},
        {&H5, New tagTxnResponseBitValResolveInfo(&H10, "Discretionary Data Present")}
}
    '{&H4, New tagTxnResponseBitValResolveInfo(&H8, "Receipt")}

    Public Shared Sub PktParser2Printout_MasterCard_DF8129_OutcomeParameterSet(ByVal tlvValOp As tagTLVNewOp, ByRef refobjTLVAttr As tagTLV, ByRef o_tagTxnInfo As ClassTransaction.TAG_TXN_PARSE_RESPONSE_INFO)
        Dim strMsg = "", strTmp = "", strTmp2 As String = ""
        Dim nOffset As Integer = 0
        Dim byteVal As Byte
        Dim arrayByteData As Byte() = New Byte(7) {}
        'Dim nArrayLen As Integer
        'Dim iteratorBitwiseOp As KeyValuePair(Of Byte, String)
        Dim nIndent As Integer = 3
        Dim nIdx, nTmp As Integer
        ' 1. Printout Tag Value
        tlvValOp.ptrFuncCbDummyParser2Printout(tlvValOp, refobjTLVAttr, o_tagTxnInfo)

        '2. Printout Detail Info
        o_tagTxnInfo.AddIndent()

        With refobjTLVAttr
            'Byte 1, Status, B1,b8-5
            nIdx = 0
            PktParser2Printout_Common_ByteValueOperation(tlvValOp, refobjTLVAttr, o_tagTxnInfo, (.m_arrayTLVal(nIdx) >> 4) And &HF, m_dictPktParser2Printout_MC_DF8129_Byte1_b8b5_Status, "B1b8-5 : Status")
            'byteVal = ((.m_arrayTLVal(nIdx) >> 4) And &HF)
            'o_tagTxnInfo.Add2Printout("Status : " + m_dictPktParser2Printout_MC_DF8129_Byte1_b8b5_Status.Item(byteVal))

            'Byte 2, Start, B2, b8-5
            nIdx = nIdx + 1
            PktParser2Printout_Common_ByteValueOperation(tlvValOp, refobjTLVAttr, o_tagTxnInfo, (.m_arrayTLVal(nIdx) >> 4) And &HF, m_dictPktParser2Printout_MC_DF8129_Byte2_b8b5_Start, "B2b8-5 : Start")
            'byteVal = ((.m_arrayTLVal(nIdx) >> 4) And &HF)
            'o_tagTxnInfo.Add2Printout("Start : " + m_dictPktParser2Printout_MC_DF8129_Byte2_b8b5_Start.Item(byteVal))

            'Byte 3, Online Response Data
            nIdx = nIdx + 1
            byteVal = .m_arrayTLVal(nIdx)
            If (byteVal = &HFF) Then
                strTmp = "N/A ( FF )"
            Else
                'RFU
                strTmp = String.Format("{0,2:X2}", byteVal) + m_strPktParser2Printout_MC_Common001_RFU
            End If
            strMsg = tagTLV.s_strPrintoutTransactionMsgDelimiter + "B3 : Online Response Data"
            PktParser2Printout_Common_FormatLine_GetDotsString(strMsg, nIndent, strTmp2)
            o_tagTxnInfo.Add2PrintoutAbs(strMsg + strTmp2 + " = " + strTmp, nIndent)

            'Byte 4, CVM
            nIdx = nIdx + 1
            byteVal = .m_arrayTLVal(nIdx)
            PktParser2Printout_Common_ByteValueOperation(tlvValOp, refobjTLVAttr, o_tagTxnInfo, byteVal, m_dictPktParser2Printout_MC_DF8129_Byte4_b8b5_CVM, "B4 : CVM")

            'If m_dictPktParser2Printout_MC_DF8129_Byte4_b8b5_CVM.ContainsKey(byteVal) Then
            '    strTmp = m_dictPktParser2Printout_MC_DF8129_Byte4_b8b5_CVM.Item(byteVal)
            'Else
            '    strTmp = String.Format("{0,2:X2}", byteVal)
            '    strTmp = m_strPktParser2Printout_MC_Common001_RFU + strTmp + " )"
            'End If
            'o_tagTxnInfo.Add2Printout("CVM : " + strTmp)
            o_tagTxnInfo.SetOutcomeParameterDF8129SetCVM(byteVal, strTmp)

            'Byte 5, UIRequestDataRecord
            nIdx = nIdx + 1
            byteVal = .m_arrayTLVal(nIdx)
            PktParser2Printout_Common_ByteBitOperation(tlvValOp, refobjTLVAttr, o_tagTxnInfo, 5, byteVal, m_dictPktParser2Printout_MC_DF8129_Byte5_b8b5_UIReqDataRecDiscretionaryPresent)

            'For Receipt case, bit 4
            If ((byteVal And &H8) = &H8) Then
                strTmp = "01 ( YES )"
            Else
                strTmp = "00 ( N/A )"
            End If
            strMsg = tagTLV.s_strPrintoutTransactionMsgDelimiter + "B5b4 : Receipt"
            PktParser2Printout_Common_FormatLine_GetDotsString(strMsg, nIndent, strTmp2)
            o_tagTxnInfo.Add2PrintoutAbs(strMsg + strTmp2 + " = " + strTmp, nIndent)
            '
            o_tagTxnInfo.DF8129_SetOutcomeParameterUIRequestOnOutcome((byteVal And &H80) = &H80)
            o_tagTxnInfo.DF8129_SetOutcomeParameterUIRequestOnRestart((byteVal And &H40) = &H40)
            o_tagTxnInfo.DF8129_SetOutcomeParameterDataRecord((byteVal And &H20) = &H20)
            o_tagTxnInfo.DF8129_SetOutcomeParameterDiscretionaryData((byteVal And &H10) = &H10)
            o_tagTxnInfo.DF8129_SetOutcomeParameterReceiptPresents((byteVal And &H8) = &H8)

            'Byte 6. Alternate Interface Preference
            nIdx = nIdx + 1
            byteVal = .m_arrayTLVal(nIdx)
            If ((byteVal And &HF0) = &HF0) Then
                strTmp = "F0 ( N/A )"
            Else
                strTmp = String.Format("{0,2:X2}", byteVal) + m_strPktParser2Printout_MC_Common001_RFU
            End If
            strMsg = tagTLV.s_strPrintoutTransactionMsgDelimiter + "B6 : Alternate Interface Preference"
            PktParser2Printout_Common_FormatLine_GetDotsString(strMsg, nIndent, strTmp2)
            o_tagTxnInfo.Add2PrintoutAbs(strMsg + strTmp2 + " = " + strTmp, nIndent)

            'Byte 7, Field Off Request, Other values exception &HFF = Hold time in units of 100ms
            nIdx = nIdx + 1
            byteVal = .m_arrayTLVal(nIdx)
            If (byteVal = &HFF) Then
                strTmp = "FF ( N/A )"
            Else
                nTmp = byteVal
                strTmp = byteVal & " * 100ms = " & (nTmp * 100) & "ms Hold time"
            End If
            strMsg = tagTLV.s_strPrintoutTransactionMsgDelimiter + "B7 : Field Off Request"
            PktParser2Printout_Common_FormatLine_GetDotsString(strMsg, nIndent, strTmp2)
            o_tagTxnInfo.Add2PrintoutAbs(strMsg + strTmp2 + " = " + strTmp, nIndent)

            'Byte 8, Removal Timeout
            nIdx = nIdx + 1
            byteVal = .m_arrayTLVal(nIdx)
            strMsg = tagTLV.s_strPrintoutTransactionMsgDelimiter + "B8 : Removal Timeout"
            PktParser2Printout_Common_FormatLine_GetDotsString(strMsg, nIndent, strTmp2)
            o_tagTxnInfo.Add2PrintoutAbs(strMsg + strTmp2 + " = " + String.Format("{0,2:X2}", byteVal), nIndent)
        End With

        '
        o_tagTxnInfo.DecIndent()
    End Sub
    '
    Public Shared Sub PktParser2Printout_MasterCard_DF812D_MessageHoldTime(ByVal tlvValOp As tagTLVNewOp, ByRef refobjTLVAttr As tagTLV, ByRef o_tagTxnInfo As ClassTransaction.TAG_TXN_PARSE_RESPONSE_INFO)
        ' in units of 100ms
        Dim strMsg As String = ""
        'Dim tlvOp2 As tagTLVNewOp
        'Dim tlv As tagTLV
        'Dim iteratorTLV As KeyValuePair(Of String, tagTLV)
        Dim dbMsgHoldTime As Double
        ' 1. Printout Tag Value
        tlvValOp.ptrFuncCbDummyParser2Printout(tlvValOp, refobjTLVAttr, o_tagTxnInfo)

        '2. Printout Detail Info
        o_tagTxnInfo.AddIndent()

        With refobjTLVAttr
            ClassTableListMaster.ConvertFromArrayByte2String(.m_arrayTLVal, strMsg, False)
            dbMsgHoldTime = CType(Convert.ToInt32(strMsg, 10), Double)
            o_tagTxnInfo.Add2Printout("Message Hold Time = " & (dbMsgHoldTime / 10) & " Sec")
        End With

        o_tagTxnInfo.DecIndent()
    End Sub
    '
    Public Shared Sub PktParser2Printout_MasterCard_DF8130_HoldTimeValue(ByVal tlvValOp As tagTLVNewOp, ByRef refobjTLVAttr As tagTLV, ByRef o_tagTxnInfo As ClassTransaction.TAG_TXN_PARSE_RESPONSE_INFO)
        ' in units of 100ms
        'Dim tlvOp2 As tagTLVNewOp
        'Dim tlv As tagTLV
        'Dim iteratorTLV As KeyValuePair(Of String, tagTLV)

        ' 1. Printout Tag Value
        tlvValOp.ptrFuncCbDummyParser2Printout(tlvValOp, refobjTLVAttr, o_tagTxnInfo)

        '2. Printout Detail Info
        o_tagTxnInfo.AddIndent()

        Dim dbHoldTimeVal As Double = CType(refobjTLVAttr.m_arrayTLVal(0), Double)
        o_tagTxnInfo.Add2Printout("Hold Time = " & (dbHoldTimeVal / 10) & " Sec")

        o_tagTxnInfo.DecIndent()
    End Sub
    '
    Public Shared Sub PktParser2Printout_MasterCard_DF8131_PhoneMessageTable(ByVal tlvValOp As tagTLVNewOp, ByRef refobjTLVAttr As tagTLV, ByRef o_tagTxnInfo As ClassTransaction.TAG_TXN_PARSE_RESPONSE_INFO)
        Dim strTmp As String = Nothing
        Dim nIdx As Integer
        Dim tagPMT As ClassTransaction.TAG_PHONE_MSG_TABLE_ELEMENT
        Dim arrayBytePCIIMask As Byte()
        Dim arrayBytePCIIVal As Byte()
        Dim byteMsgId As Byte
        Dim byteStatus As Byte
        ' 1. Printout Tag Value
        tlvValOp.ptrFuncCbDummyParser2Printout(tlvValOp, refobjTLVAttr, o_tagTxnInfo)

        With refobjTLVAttr
            ' 2. Validation Check
            If (.m_nLen Mod 8) <> 0 Then
                Throw New Exception("[Err][Data Length] Not Entry Data Format Each 8 bytes, This Data Length = " & .m_nLen)
            End If

            While (nIdx < .m_nLen)
                'Data Field Length Format
                '1.PCII Mask 3 Bytes b2s
                arrayBytePCIIMask = New Byte(2) {}
                Array.Copy(.m_arrayTLVal, nIdx + 0, arrayBytePCIIMask, 0, 3)
                '2.PCII Value 3 Bytes b2s
                arrayBytePCIIVal = New Byte(2) {}
                Array.Copy(.m_arrayTLVal, nIdx + 3, arrayBytePCIIVal, 0, 3)
                '3.Message Identifier 1 Byte b2s
                byteMsgId = .m_arrayTLVal(nIdx + 6)
                '4.Status 1 Byte, b2s
                byteStatus = .m_arrayTLVal(nIdx + 7)

                tagPMT = New ClassTransaction.TAG_PHONE_MSG_TABLE_ELEMENT With {.m_arrayBytePCIIMask = arrayBytePCIIMask, .m_arrayBytePCIIValue = arrayBytePCIIVal, .m_byteMessageIdentifier = byteMsgId, .m_byteStatus = byteStatus}
                o_tagTxnInfo.m_dictPhoneMessageTable.Add(tagPMT)
                nIdx = nIdx + 8
            End While
        End With

        '2. Printout Detail Info
        With o_tagTxnInfo
            .AddIndent()
            If .m_dictPhoneMessageTable.Count > 0 Then
                nIdx = 0
                For Each iteratorPMT In .m_dictPhoneMessageTable
                    .Add2Printout(" ---[ " & nIdx & " Entry ]---")
                    '
                    .AddIndent()
                    '---------------------------
                    ClassTableListMaster.ConvertFromArrayByte2String(iteratorPMT.m_arrayBytePCIIMask, strTmp)
                    .Add2Printout("PCII Mask = " + strTmp)
                    ClassTableListMaster.ConvertFromArrayByte2String(iteratorPMT.m_arrayBytePCIIValue, strTmp)
                    .Add2Printout("PCII Value = " + strTmp)
                    .Add2Printout("Message Identifier = " + String.Format("{0,2:X2}", iteratorPMT.m_byteMessageIdentifier))
                    .Add2Printout("Status= " + String.Format("{0,2:X2}", iteratorPMT.m_byteStatus))
                    '---------------------------
                    .DecIndent()

                    nIdx = nIdx + 1
                Next
            End If
            '
            .DecIndent()
        End With
    End Sub
    '
    Public Shared Sub PktParser2Printout_Common_SubTagsParser(ByVal tlvValOp As tagTLVNewOp, ByRef refobjTLVAttr As tagTLV, ByRef o_tagTxnInfo As ClassTransaction.TAG_TXN_PARSE_RESPONSE_INFO, ByRef dictTlvsTxnResultSubTagsList As Dictionary(Of String, tagTLV))
        Dim tlvOp2 As tagTLVNewOp
        Dim iteratorTLV As KeyValuePair(Of String, tagTLV)

        ' 1. Printout Tag Value
        tlvValOp.ptrFuncCbDummyParser2Printout(tlvValOp, refobjTLVAttr, o_tagTxnInfo)

        With refobjTLVAttr
            '------------------[ 4. Parse ViVO Proprietary  Tag  ]------------------
            ClassReaderCommanderParser.TLV_ParseFromReaderMultipleTLVs(.m_arrayTLVal, 0, .m_nLen, dictTlvsTxnResultSubTagsList)
        End With

        '2. Printout Detail Info
        o_tagTxnInfo.AddIndent()

        'Printout Sub Tags @ FFEE??
        If dictTlvsTxnResultSubTagsList.Count > 0 Then
            For Each iteratorTLV In dictTlvsTxnResultSubTagsList
                tlvOp2 = ClassReaderCommanderParser.m_dictTLVOp_RefList_Major.Item(iteratorTLV.Key)
                tlvOp2.m_pfuncCbParser2Printout(tlvOp2, iteratorTLV.Value, o_tagTxnInfo)
            Next
        End If

        o_tagTxnInfo.Add2PrintoutAbs(tagTLV.s_strPrintoutTransactionMsgNestedTagPrefix + tlvValOp.m_strTag + tagTLV.s_strPrintoutTransactionMsgNestedTagPostfix, 0)
        o_tagTxnInfo.Add2PrintoutAbs(" ", 0)
        '
        o_tagTxnInfo.DecIndent()

    End Sub
    Public Shared Sub PktParser2Printout_Common_SubTagsParser(ByVal tlvValOp As tagTLVNewOp, ByRef refobjTLVAttr As tagTLV, ByRef o_tagTxnInfo As ClassTransaction.TAG_TXN_PARSE_RESPONSE_INFO, ByRef dictTlvsTxnResultSubTagsList As SortedDictionary(Of String, tagTLV))
        Dim tlvOp2 As tagTLVNewOp
        Dim iteratorTLV As KeyValuePair(Of String, tagTLV)

        ' 1. Printout Tag Value
        tlvValOp.ptrFuncCbDummyParser2Printout(tlvValOp, refobjTLVAttr, o_tagTxnInfo)

        With refobjTLVAttr
            '------------------[ 4. Parse ViVO Proprietary  Tag  ]------------------
            ClassReaderCommanderParser.TLV_ParseFromReaderMultipleTLVs(.m_arrayTLVal, 0, .m_nLen, dictTlvsTxnResultSubTagsList)
        End With

        '2. Printout Detail Info
        o_tagTxnInfo.AddIndent()

        'Printout Sub Tags @ FFEE??
        If dictTlvsTxnResultSubTagsList.Count > 0 Then
            For Each iteratorTLV In dictTlvsTxnResultSubTagsList
                tlvOp2 = ClassReaderCommanderParser.m_dictTLVOp_RefList_Major.Item(iteratorTLV.Key)
                tlvOp2.m_pfuncCbParser2Printout(tlvOp2, iteratorTLV.Value, o_tagTxnInfo)
            Next
        End If

        o_tagTxnInfo.Add2PrintoutAbs(tagTLV.s_strPrintoutTransactionMsgNestedTagPrefix + tlvValOp.m_strTag + tagTLV.s_strPrintoutTransactionMsgNestedTagPostfix, 0)
        o_tagTxnInfo.Add2PrintoutAbs(" ", 0)
        '
        o_tagTxnInfo.DecIndent()

    End Sub

    Public Shared Sub PktParser2Printout_Common_SubTagsParser(ByVal tlvValOp As tagTLVNewOp, ByRef refobjTLVAttr As tagTLV, ByRef o_tagTxnInfo As ClassTransaction.TAG_TXN_PARSE_RESPONSE_INFO, ByRef dictTlvsTxnResultSubTagsList As List(Of tagTLV))
        Dim tlvOp2 As tagTLVNewOp
        Dim tlv As tagTLV
        'Dim iteratorTLV As KeyValuePair(Of String, tagTLV)

        ' 1. Printout Tag Value
        tlvValOp.ptrFuncCbDummyParser2Printout(tlvValOp, refobjTLVAttr, o_tagTxnInfo)

        With refobjTLVAttr
            '------------------[ 4. Parse ViVO Proprietary  Tag "FF8105" ]------------------
            ClassReaderCommanderParser.TLV_ParseFromReaderMultipleTLVs(.m_arrayTLVal, 0, .m_nLen, dictTlvsTxnResultSubTagsList)
        End With

        '2. Printout Detail Info
        o_tagTxnInfo.AddIndent()

        'Printout Sub Tags @ FFEE01
        If dictTlvsTxnResultSubTagsList.Count > 0 Then
            For Each tlv In dictTlvsTxnResultSubTagsList
                tlvOp2 = ClassReaderCommanderParser.m_dictTLVOp_RefList_Major.Item(tlv.m_strTag)
                tlvOp2.m_pfuncCbParser2Printout(tlvOp2, tlv, o_tagTxnInfo)
            Next
        End If

        o_tagTxnInfo.Add2PrintoutAbs(tagTLV.s_strPrintoutTransactionMsgNestedTagPrefix + tlvValOp.m_strTag + tagTLV.s_strPrintoutTransactionMsgNestedTagPostfix, 0)
        o_tagTxnInfo.Add2PrintoutAbs(" ", 0)
        '
        o_tagTxnInfo.DecIndent()

    End Sub

    '
    '===========================[ FF8102 ]===========================
    Public Shared Sub PktParser2Printout_MasterCard_FF8102_TagsToWriteBeforeGenAC(ByVal tlvValOp As tagTLVNewOp, ByRef refobjTLVAttr As tagTLV, ByRef o_tagTxnInfo As ClassTransaction.TAG_TXN_PARSE_RESPONSE_INFO)
        PktParser2Printout_Common_SubTagsParser(tlvValOp, refobjTLVAttr, o_tagTxnInfo, o_tagTxnInfo.m_dictTlvsTxnResult_FF8102)
    End Sub
    '
    '===========================[ FF8103 ]===========================
    Public Shared Sub PktParser2Printout_MasterCard_FF8103_TagsToWriteAfterGenAC(ByVal tlvValOp As tagTLVNewOp, ByRef refobjTLVAttr As tagTLV, ByRef o_tagTxnInfo As ClassTransaction.TAG_TXN_PARSE_RESPONSE_INFO)
        PktParser2Printout_Common_SubTagsParser(tlvValOp, refobjTLVAttr, o_tagTxnInfo, o_tagTxnInfo.m_dictTlvsTxnResult_FF8103)
    End Sub


    ''===========================[ FF8105 ]===========================
    Public Shared Sub PktParser2Printout_MasterCard_FF8105_DataRecord(ByVal tlvValOp As tagTLVNewOp, ByRef refobjTLVAttr As tagTLV, ByRef o_tagTxnInfo As ClassTransaction.TAG_TXN_PARSE_RESPONSE_INFO)
        PktParser2Printout_Common_SubTagsParser(tlvValOp, refobjTLVAttr, o_tagTxnInfo, o_tagTxnInfo.m_dictTlvsTxnResult_FF8105)
    End Sub

    ''===========================[ FF8106 ]===========================
    Public Shared Sub PktParser2Printout_MasterCard_FF8106_DiscretionaryData(ByVal tlvValOp As tagTLVNewOp, ByRef refobjTLVAttr As tagTLV, ByRef o_tagTxnInfo As ClassTransaction.TAG_TXN_PARSE_RESPONSE_INFO)
        PktParser2Printout_Common_SubTagsParser(tlvValOp, refobjTLVAttr, o_tagTxnInfo, o_tagTxnInfo.m_dictTlvsTxnResult_FF8106)
    End Sub

    Public Shared Sub PktParser2Printout_MasterCard_FF8901_UnknownTLV01(ByVal tlvValOp As tagTLVNewOp, ByRef refobjTLVAttr As tagTLV, ByRef o_tagTxnInfo As ClassTransaction.TAG_TXN_PARSE_RESPONSE_INFO)
        Dim dictTLVs As SortedDictionary(Of String, tagTLV) = Nothing
        '
        PktParser2Printout_Common_SubTagsParser(tlvValOp, refobjTLVAttr, o_tagTxnInfo, dictTLVs)
        '
        ClassTableListMaster.CleanupConfigurationsTLVList(dictTLVs)

    End Sub

    Public Shared Sub PktParser2Printout_MasterCard_FF8902_UnknownTLV02(ByVal tlvValOp As tagTLVNewOp, ByRef refobjTLVAttr As tagTLV, ByRef o_tagTxnInfo As ClassTransaction.TAG_TXN_PARSE_RESPONSE_INFO)
        Dim dictTLVs As SortedDictionary(Of String, tagTLV) = Nothing
        '
        PktParser2Printout_Common_SubTagsParser(tlvValOp, refobjTLVAttr, o_tagTxnInfo, dictTLVs)
        '
        ClassTableListMaster.CleanupConfigurationsTLVList(dictTLVs)
    End Sub

    '
    '=============================[ DF8117 ]=============================
    Public Shared m_dictPktParser2Printout_TxnResponse_DF8117_CardDataInputCapability As Dictionary(Of Byte, tagTxnResponseBitValResolveInfo) = New Dictionary(Of Byte, tagTxnResponseBitValResolveInfo) From {
{&H8, New tagTxnResponseBitValResolveInfo(&H80, "Manual Key Entry")},
{&H7, New tagTxnResponseBitValResolveInfo(&H40, "Magnetic Stripe")},
{&H6, New tagTxnResponseBitValResolveInfo(&H20, "Signature (paper)")}
}
    Public Shared Sub PktParser2Printout_DF8117_CVMCapability_CardDataInputCapability(ByVal tlvValOp As tagTLVNewOp, ByRef refobjTLVAttr As tagTLV, ByRef o_tagTxnInfo As ClassTransaction.TAG_TXN_PARSE_RESPONSE_INFO)

        ' 1. Printout Tag Value
        tlvValOp.ptrFuncCbDummyParser2Printout(tlvValOp, refobjTLVAttr, o_tagTxnInfo)

        PktParser2Printout_Common_ByteBitOperation(tlvValOp, refobjTLVAttr, o_tagTxnInfo, 1, refobjTLVAttr.m_arrayTLVal(0), m_dictPktParser2Printout_TxnResponse_DF8117_CardDataInputCapability)
    End Sub
    '
    '=============================[ DF8118 ]=============================
    Public Shared m_dictPktParser2Printout_TxnResponse_DF8118_CVMCapability_CVMRequired_Byte1 As Dictionary(Of Byte, tagTxnResponseBitValResolveInfo) = m_dictPktParser2Printout_TxnResponse_9F33_TerminalCapabilities_Byte2
    '
    Public Shared Sub PktParser2Printout_DF8118_CVMCapability_CVMRequired(ByVal tlvValOp As tagTLVNewOp, ByRef refobjTLVAttr As tagTLV, ByRef o_tagTxnInfo As ClassTransaction.TAG_TXN_PARSE_RESPONSE_INFO)

        ' 1. Printout Tag Value
        tlvValOp.ptrFuncCbDummyParser2Printout(tlvValOp, refobjTLVAttr, o_tagTxnInfo)

        PktParser2Printout_Common_ByteBitOperation(tlvValOp, refobjTLVAttr, o_tagTxnInfo, 1, refobjTLVAttr.m_arrayTLVal(0), m_dictPktParser2Printout_TxnResponse_DF8118_CVMCapability_CVMRequired_Byte1)
    End Sub
    '
    '=============================[ DF8119 ]=============================
    Public Shared m_dictPktParser2Printout_TxnResponse_DF8119_CVMCapability_NoCVMRequired_Byte1 As Dictionary(Of Byte, tagTxnResponseBitValResolveInfo) = m_dictPktParser2Printout_TxnResponse_9F33_TerminalCapabilities_Byte2
    '
    Public Shared Sub PktParser2Printout_DF8119_CVMCapability_NoCVMRequired(ByVal tlvValOp As tagTLVNewOp, ByRef refobjTLVAttr As tagTLV, ByRef o_tagTxnInfo As ClassTransaction.TAG_TXN_PARSE_RESPONSE_INFO)

        ' 1. Printout Tag Value
        tlvValOp.ptrFuncCbDummyParser2Printout(tlvValOp, refobjTLVAttr, o_tagTxnInfo)

        PktParser2Printout_Common_ByteBitOperation(tlvValOp, refobjTLVAttr, o_tagTxnInfo, 1, refobjTLVAttr.m_arrayTLVal(0), m_dictPktParser2Printout_TxnResponse_DF8119_CVMCapability_NoCVMRequired_Byte1)
    End Sub
    '
    '=============================[ Common Routines ]=============================
    '    Public Shared m_dictPktParser2Printout_TxnResponse_9F5D_Master_Byte3 As Dictionary(Of Byte, String) = New Dictionary(Of Byte, String) From {
    '{&H0, "Undefined SDS configuration"},
    '{&H1, "All 10 tags 32 bytes"},
    '{&H2, "All 10 tags 48 bytes"},
    '{&H3, "All 10 tags 64 bytes"},
    '{&H4, "All 10 tags 96 bytes"},
    '{&H5, "All 10 tags 128 bytes"},
    '{&H6, "All 10 tags 160 bytes"},
    '{&H7, "All 10 tags 192 bytes"},
    '{&H8, "All SDS tags 32 bytes except '9F78' which is 64 bytes"}
    '}
    'Ex: PktParser2Printout_Common_ByteValueOperation(tlvValOp,refobjTLVAttr,o_tagTxnInfo, .m_arrayTLVal(nIdx), m_dictPktParser2Printout_TxnResponse_9F5D_Master_Byte3, "Byte 3, SDS Scheme Indicator")
    Public Shared Sub PktParser2Printout_Common_ByteValueOperation(ByVal tlvValOp As tagTLVNewOp, ByRef refobjTLVAttr As tagTLV, ByRef o_tagTxnInfo As ClassTransaction.TAG_TXN_PARSE_RESPONSE_INFO, ByVal byteVal As Byte, ByRef dictPktParser2Printout_Map_ByteValue_String As Dictionary(Of Byte, String), Optional ByVal strByteHeadlineMsg As String = Nothing, Optional ByVal strRFUCustomized As String = Nothing)
        Dim strMsg = "", strTmp = "", strTmp2 As String = ""
        Dim nIndent As Integer = 0

        If dictPktParser2Printout_Map_ByteValue_String.ContainsKey(byteVal) Then
            strTmp2 = dictPktParser2Printout_Map_ByteValue_String.Item(byteVal)
        Else
            If (strRFUCustomized IsNot Nothing) Then
                strTmp2 = strRFUCustomized
            Else
                strTmp2 = "RFU"
            End If
        End If


        If (0) Then
            If (strByteHeadlineMsg IsNot Nothing) Then
                o_tagTxnInfo.Add2PrintoutAbs(strByteHeadlineMsg, nIndent)
            End If
            'o_tagTxnInfo.AddIndent()
            '        o_tagTxnInfo.Add2Printout(strTmp)
            o_tagTxnInfo.Add2Printout("( " + String.Format("{0, 2:X2}", byteVal) + " ) <-- " + strTmp2, nIndent)
            'o_tagTxnInfo.DecIndent()
        Else
            ' Indent + tag + tagTLV.s_strPrintoutTransactionMsgDelimiter + strComment + strDotLine+ " = " + ValueInHexString
            With refobjTLVAttr
                strMsg = tagTLV.s_strPrintoutTransactionMsgDelimiter + strByteHeadlineMsg
                PktParser2Printout_Common_FormatLine_GetDotsString(strMsg, nIndent, strTmp)
                strMsg = strMsg + strTmp + " = " + String.Format("{0, 2:X2}", byteVal) + " ( " + strTmp2 + " )"
            End With
            '
            o_tagTxnInfo.Add2PrintoutAbs(strMsg, nIndent)
        End If
    End Sub
    Public Shared Sub PktParser2Printout_Common_ByteValueOperation_TagDisplay(ByVal tlvValOp As tagTLVNewOp, ByRef refobjTLVAttr As tagTLV, ByRef o_tagTxnInfo As ClassTransaction.TAG_TXN_PARSE_RESPONSE_INFO, ByVal byteVal As Byte, ByRef dictPktParser2Printout_Map_ByteValue_String As Dictionary(Of Byte, String), Optional ByVal strByteHeadlineMsg As String = Nothing, Optional ByVal strRFUCustomized As String = Nothing)
        Dim strMsg = "", strTmp = "", strTmp2 As String = ""
        Dim nIndent As Integer = 0

        If dictPktParser2Printout_Map_ByteValue_String.ContainsKey(byteVal) Then
            strTmp2 = dictPktParser2Printout_Map_ByteValue_String.Item(byteVal)
        Else
            If (strRFUCustomized IsNot Nothing) Then
                strTmp2 = strRFUCustomized
            Else
                strTmp2 = "RFU"
            End If
        End If


        If (0) Then
            If (strByteHeadlineMsg IsNot Nothing) Then
                o_tagTxnInfo.Add2PrintoutAbs(strByteHeadlineMsg, nIndent)
            End If
            'o_tagTxnInfo.AddIndent()
            '        o_tagTxnInfo.Add2Printout(strTmp)
            o_tagTxnInfo.Add2Printout("( " + String.Format("{0, 2:X2}", byteVal) + " ) <-- " + strTmp2, nIndent)
            'o_tagTxnInfo.DecIndent()
        Else
            ' Indent + tag + tagTLV.s_strPrintoutTransactionMsgDelimiter + strComment + strDotLine+ " = " + ValueInHexString
            With refobjTLVAttr
                strMsg = tlvValOp.m_strTag + tagTLV.s_strPrintoutTransactionMsgDelimiter + strByteHeadlineMsg
                PktParser2Printout_Common_FormatLine_GetDotsString(strMsg, nIndent, strTmp)
                strMsg = strMsg + strTmp + " = " + String.Format("{0, 2:X2}", byteVal) + " ( " + strTmp2 + " )"
            End With
            '
            o_tagTxnInfo.Add2PrintoutAbs(strMsg, nIndent)
        End If
    End Sub
    '
    'Ex: 
    '    Dim dictParseResolution9F34_CVM As Dictionary(Of Byte, KeyValuePair(Of String, Dictionary(Of Byte, String))) = New Dictionary(Of Byte, KeyValuePair(Of String, Dictionary(Of Byte, String))) From {
    '        {&H0, New KeyValuePair(Of String, Dictionary(Of Byte, String))("CVM Performed", m_dictPktParser2Printout_TxnResponse_9F34_CardholderVerificationMethod_Result_Performed_Byte0)},
    '        {&H1, New KeyValuePair(Of String, Dictionary(Of Byte, String))("CVM Condition", m_dictPktParser2Printout_TxnResponse_9F34_CardholderVerificationMethod_Result_Condition_Byte1)},
    '        {&H2, New KeyValuePair(Of String, Dictionary(Of Byte, String))("CVM Result", m_dictPktParser2Printout_TxnResponse_9F34_CardholderVerificationMethod_Result_Result_Byte2)}
    '        }
    'PktParser2Printout_Common_ByteValueOperationMultipleLists(tlvValOp, refobjTLVAttr, o_tagTxnInfo, dictParseResolution9F34_CVM, tlvValOp.m_strComment)
    ' or
    'PktParser2Printout_Common_ByteValueOperationMultipleLists(tlvValOp, refobjTLVAttr, o_tagTxnInfo, dictParseResolution9F34_CVM)
    Public Shared Sub PktParser2Printout_Common_ByteValueOperationMultipleLists(ByVal tlvValOp As tagTLVNewOp, ByRef refobjTLVAttr As tagTLV, ByRef o_tagTxnInfo As ClassTransaction.TAG_TXN_PARSE_RESPONSE_INFO, ByRef dictPktParser2Printout_Map_ByteValue_String_MultipleList As Dictionary(Of Byte, KeyValuePair(Of String, Dictionary(Of Byte, String))), Optional ByVal strByteHeadlineMsg As String = Nothing)
        Dim nIdx, nIdxMax As Integer
        '
        If strByteHeadlineMsg IsNot Nothing Then
            o_tagTxnInfo.Add2Printout(strByteHeadlineMsg)
            o_tagTxnInfo.AddIndent()
        End If
        '
        nIdxMax = refobjTLVAttr.m_nLen - 1
        For nIdx = 0 To nIdxMax
            With refobjTLVAttr
                PktParser2Printout_Common_ByteValueOperation(tlvValOp, refobjTLVAttr, o_tagTxnInfo, .m_arrayTLVal(nIdx), dictPktParser2Printout_Map_ByteValue_String_MultipleList.Item(nIdx).Value, "B" & nIdx + 1 & " : " + dictPktParser2Printout_Map_ByteValue_String_MultipleList.Item(nIdx).Key)
            End With
        Next

        If strByteHeadlineMsg IsNot Nothing Then
            o_tagTxnInfo.DecIndent()
        End If
        '
        dictPktParser2Printout_Map_ByteValue_String_MultipleList.Clear()
    End Sub
    '
    '===================================================================================================================
    '    Public Shared m_dictPktParser2Printout_TxnResponse_9F33_TerminalCapabilities_Byte2 As Dictionary(Of Byte, tagTxnResponseBitValResolveInfo) = New Dictionary(Of Byte, tagTxnResponseBitValResolveInfo) From {
    '{&H8, New tagTxnResponseBitValResolveInfo(&H80, "Plaintext PIN for ICC verification")},
    '{&H7, New tagTxnResponseBitValResolveInfo(&H40, "Enciphered PIN for online verification")},
    '{&H6, New tagTxnResponseBitValResolveInfo(&H20, "Signature (paper)")},
    '{&H5, New tagTxnResponseBitValResolveInfo(&H10, "Enciphered PIN for offline verification")},
    '{&H4, New tagTxnResponseBitValResolveInfo(&H8, "No CVM required")}
    '}
    'Dim strTmp As String = Nothing
    'Dim byteVal As Byte
    'Dim byteStrIdx As Byte
    'Dim nIdx, nIdxMax As Byte

    '' 1. Printout Tag Value
    '    tlvValOp.ptrFuncCbDummyParser2Printout(tlvValOp, refobjTLVAttr, o_tagTxnInfo)

    ''2. Printout Detail Info
    '    o_tagTxnInfo.AddIndent()
    ''Byte 1 = Version Number
    '    nIdx = 0
    '    o_tagTxnInfo.Add2Printout("Byte 1 : " + String.Format("{0,2:X2}", refobjTLVAttr.m_arrayTLVal(nIdx)) + " <-- Version Number")
    ''Byte 2 
    '    nIdx = 1
    '    PktParser2Printout_Common_ByteBitOperation(tlvValOp, refobjTLVAttr, o_tagTxnInfo, nIdx + 1, refobjTLVAttr.m_arrayTLVal(nIdx), m_dictPktParser2Printout_TxnResponse_9F33_TerminalCapabilities_Byte2)
    ''
    '    o_tagTxnInfo.DecIndent()
    Public Shared Sub PktParser2Printout_Common_ByteBitOperation(ByVal tlvValOp As tagTLVNewOp, ByRef refobjTLVAttr As tagTLV, ByRef o_tagTxnInfo As ClassTransaction.TAG_TXN_PARSE_RESPONSE_INFO, ByVal byteIdx As Byte, ByVal byteVal As Byte, ByRef dictPktParser2Printout_TxnResponse_Common_ByteBitOperation_Byte As Dictionary(Of Byte, tagTxnResponseBitValResolveInfo))
        Dim strTmp = "", strTmp2 As String = ""
        Dim iteratorBitwiseOp As KeyValuePair(Of Byte, tagTxnResponseBitValResolveInfo)
        Dim nIdx As Integer

        '2. Printout Detail Info
        'o_tagTxnInfo.AddIndent()

        If (0) Then
            'o_tagTxnInfo.Add2Printout("Byte " & byteIdx, 1)
            'o_tagTxnInfo.AddIndent(2)
            ''Byte 1,
            'For Each iteratorBitwiseOp In dictPktParser2Printout_TxnResponse_Common_ByteBitOperation_Byte
            '    If (iteratorBitwiseOp.Value.m_byteMaskBit And byteVal) = iteratorBitwiseOp.Value.m_byteMaskBit Then
            '        nIdx = 0
            '    Else
            '        nIdx = 1
            '    End If
            '    o_tagTxnInfo.Add2Printout(".Bit " & iteratorBitwiseOp.Key & " : " + m_dictPktParser2Printout_TxnResponseCommonStringYesNo.Item(nIdx) + " <-- " + iteratorBitwiseOp.Value.m_strMsg)
            'Next
            'o_tagTxnInfo.DecIndent(2)
        Else
            Dim nIndent As Integer = 3
            'Byte #,
            For Each iteratorBitwiseOp In dictPktParser2Printout_TxnResponse_Common_ByteBitOperation_Byte
                If (iteratorBitwiseOp.Value.m_byteMaskBit And byteVal) = iteratorBitwiseOp.Value.m_byteMaskBit Then
                    nIdx = enumParseTxnResponseYesNo.PARSE_TXN_RESPONSE_YES
                Else
                    nIdx = enumParseTxnResponseYesNo.PARSE_TXN_RESPONSE_NO
                End If
                '
                strTmp = tagTLV.s_strPrintoutTransactionMsgDelimiter + "B" & byteIdx
                ' Calculate strTmp2 String "......"
                strTmp = strTmp + "b" & iteratorBitwiseOp.Key & " : " + iteratorBitwiseOp.Value.m_strMsg
                PktParser2Printout_Common_FormatLine_GetDotsString(strTmp, nIndent, strTmp2)
                ' Make Output String
                strTmp = strTmp + strTmp2 + " = " + String.Format("{0,2:X2}", nIdx) + " ( " + m_dictPktParser2Printout_TxnResponseCommonStringYesNo.Item(nIdx) + " )"
                o_tagTxnInfo.Add2PrintoutAbs(strTmp, nIndent)
            Next
        End If
        '
        'o_tagTxnInfo.DecIndent()
    End Sub
    '
    Public Shared m_dictPktParser2Printout_TxnResponse_DF811B_KernelConfiguration_Byte1 As Dictionary(Of Byte, tagTxnResponseBitValResolveInfo) = New Dictionary(Of Byte, tagTxnResponseBitValResolveInfo) From {
{&H8, New tagTxnResponseBitValResolveInfo(&H80, "Only EMV mode transactions supported")},
{&H7, New tagTxnResponseBitValResolveInfo(&H40, "Only mag-stripe mode transactions supported")},
{&H6, New tagTxnResponseBitValResolveInfo(&H20, "On device cardholder verification supported")}
}
    '
    Public Shared Sub PktParser2Printout_DF811B_KernelConfiguration(ByVal tlvValOp As tagTLVNewOp, ByRef refobjTLVAttr As tagTLV, ByRef o_tagTxnInfo As ClassTransaction.TAG_TXN_PARSE_RESPONSE_INFO)

        ' 1. Printout Tag Value
        tlvValOp.ptrFuncCbDummyParser2Printout(tlvValOp, refobjTLVAttr, o_tagTxnInfo)

        PktParser2Printout_Common_ByteBitOperation(tlvValOp, refobjTLVAttr, o_tagTxnInfo, 1, refobjTLVAttr.m_arrayTLVal(0), m_dictPktParser2Printout_TxnResponse_DF811B_KernelConfiguration_Byte1)
    End Sub
    '
    Public Shared m_dictPktParser2Printout_MC_DF812C_B1b8b5_MagStripeCVMCapabilityNoCVMRequired As Dictionary(Of Byte, String) = New Dictionary(Of Byte, String) From {
        {&H0, "NO CVM"},
        {&H1, "OBTAIN SIGNATURE"},
        {&H2, "ONLINE PIN"},
        {&HF, "N/A"}
        }
    Public Shared Sub PktParser2Printout_MasterCard_DF812C_MagStripeCVMCapabilityNoCVMRequired(ByVal tlvValOp As tagTLVNewOp, ByRef refobjTLVAttr As tagTLV, ByRef o_tagTxnInfo As ClassTransaction.TAG_TXN_PARSE_RESPONSE_INFO)

        ' 1. Printout Tag Value
        'tlvValOp.ptrFuncCbDummyParser2Printout(tlvValOp, refobjTLVAttr, o_tagTxnInfo)

        '2. Printout Detail Info
        'o_tagTxnInfo.AddIndent()
        '        'CVM
        '0:      
        '1:      
        '10:     
        '1111: 
        '        Other values : RFU()
        PktParser2Printout_Common_ByteValueOperation_TagDisplay(tlvValOp, refobjTLVAttr, o_tagTxnInfo, (refobjTLVAttr.m_arrayTLVal(0) >> 4) And &HF, m_dictPktParser2Printout_MC_DF812C_B1b8b5_MagStripeCVMCapabilityNoCVMRequired, "B1b8-5")
        '
        'o_tagTxnInfo.DecIndent()

    End Sub
    '
    Public Shared Sub PktParser2Printout_MasterCard_FFEE01_ViVOtechGroupTag(ByVal tlvValOp As tagTLVNewOp, ByRef refobjTLVAttr As tagTLV, ByRef o_tagTxnInfo As ClassTransaction.TAG_TXN_PARSE_RESPONSE_INFO)
        Dim dictTLV As List(Of tagTLV) = New List(Of tagTLV)
        PktParser2Printout_Common_SubTagsParser(tlvValOp, refobjTLVAttr, o_tagTxnInfo, dictTLV)
        ClassTableListMaster.CleanupConfigurationsTLVList(dictTLV)
    End Sub


    ''===========================[ FFEE04 ]===========================
    Public Shared Sub PktParser2Printout_MasterCard_FFEE04_ViVOtechMC3SignalDataTLV(ByVal tlvValOp As tagTLVNewOp, ByRef refobjTLVAttr As tagTLV, ByRef o_tagTxnInfo As ClassTransaction.TAG_TXN_PARSE_RESPONSE_INFO)
        Dim dictTLV As List(Of tagTLV) = New List(Of tagTLV)
        PktParser2Printout_Common_SubTagsParser(tlvValOp, refobjTLVAttr, o_tagTxnInfo, dictTLV)
        ClassTableListMaster.CleanupConfigurationsTLVList(dictTLV)
    End Sub

    ''===========================[ FFEE05 ]===========================
    Public Shared Sub PktParser2Printout_MasterCard_FFEE05_ViVOtechMC3ProprietaryTag(ByVal tlvValOp As tagTLVNewOp, ByRef refobjTLVAttr As tagTLV, ByRef o_tagTxnInfo As ClassTransaction.TAG_TXN_PARSE_RESPONSE_INFO)
        Dim dictTLV As List(Of tagTLV) = New List(Of tagTLV)
        PktParser2Printout_Common_SubTagsParser(tlvValOp, refobjTLVAttr, o_tagTxnInfo, dictTLV)
        ClassTableListMaster.CleanupConfigurationsTLVList(dictTLV)
    End Sub
    '

    '===========================[ DF8108 ]===========================
    Public Shared m_dictPktParser2Printout_MasterCard_DF8108_DS_AC_Type_B1b8_b7 As Dictionary(Of Byte, String) = m_dictPktParser2Printout_MC_DF8114_AC_Type
    Public Shared Sub PktParser2Printout_MasterCard_DF8108_DS_AC_Type(ByVal tlvValOp As tagTLVNewOp, ByRef refobjTLVAttr As tagTLV, ByRef o_tagTxnInfo As ClassTransaction.TAG_TXN_PARSE_RESPONSE_INFO)
        'Dim strMsg As String
        'Dim nOffset As Integer = 0
        'Dim byteVal As Byte
        'Dim nIdx, nIdxMax As Integer
        ' 1. Printout Tag Value
        'tlvValOp.ptrFuncCbDummyParser2Printout(tlvValOp, refobjTLVAttr, o_tagTxnInfo)

        ''2. Printout Detail Info
        'o_tagTxnInfo.AddIndent()
        With refobjTLVAttr

            PktParser2Printout_Common_ByteValueOperation_TagDisplay(tlvValOp, refobjTLVAttr, o_tagTxnInfo, (.m_arrayTLVal(0) >> 6) And &H3, m_dictPktParser2Printout_MasterCard_DF8108_DS_AC_Type_B1b8_b7, "B1b8-7, AC Type")

        End With
        'o_tagTxnInfo.DecIndent()

    End Sub
    '===========================[ DF810A ]===========================
    Public Shared m_dictPktParser2Printout_MasterCard_DF810A_DS_ODS_InfoForReader As Dictionary(Of Byte, tagTxnResponseBitValResolveInfo) = New Dictionary(Of Byte, tagTxnResponseBitValResolveInfo) From {
     {&H8, New tagTxnResponseBitValResolveInfo(&H80, "Usable for TC")},
      {&H7, New tagTxnResponseBitValResolveInfo(&H40, "Usable for ARQC")},
     {&H6, New tagTxnResponseBitValResolveInfo(&H20, "Usable for AAC")},
     {&H3, New tagTxnResponseBitValResolveInfo(&H4, "Stop if no DS ODS Term")},
     {&H2, New tagTxnResponseBitValResolveInfo(&H2, "Stop if write failed")}
     }
    '    {&H5, New tagTxnResponseBitValResolveInfo(&H10, "RFU")},
    '{&H4, New tagTxnResponseBitValResolveInfo(&H8, "RFU")},
    '{&H1, New tagTxnResponseBitValResolveInfo(&H1, "RFU")},
    Public Shared Sub PktParser2Printout_MasterCard_DF810A_DS_ODS_InfoForReader(ByVal tlvValOp As tagTLVNewOp, ByRef refobjTLVAttr As tagTLV, ByRef o_tagTxnInfo As ClassTransaction.TAG_TXN_PARSE_RESPONSE_INFO)
        Dim byteVal As Byte
        ' 1. Printout Tag Value
        tlvValOp.ptrFuncCbDummyParser2Printout(tlvValOp, refobjTLVAttr, o_tagTxnInfo)

        '2. Printout Detail Info
        o_tagTxnInfo.AddIndent()
        With refobjTLVAttr
            byteVal = .m_arrayTLVal(0)
            PktParser2Printout_Common_ByteBitOperation(tlvValOp, refobjTLVAttr, o_tagTxnInfo, 1, byteVal, m_dictPktParser2Printout_MasterCard_DF810A_DS_ODS_InfoForReader)

        End With
        o_tagTxnInfo.DecIndent()

    End Sub
    '===========================[ DF810B ]===========================
    Public Shared m_dictPktParser2Printout_MasterCard_DF810B_DS_Summary_Status As Dictionary(Of Byte, tagTxnResponseBitValResolveInfo) = New Dictionary(Of Byte, tagTxnResponseBitValResolveInfo) From {
     {&H8, New tagTxnResponseBitValResolveInfo(&H80, "Successful Read")},
      {&H7, New tagTxnResponseBitValResolveInfo(&H40, "Successful Write")}
     }
    Public Shared Sub PktParser2Printout_MasterCard_DF810B_DS_Summary_Status(ByVal tlvValOp As tagTLVNewOp, ByRef refobjTLVAttr As tagTLV, ByRef o_tagTxnInfo As ClassTransaction.TAG_TXN_PARSE_RESPONSE_INFO)
        'Dim strMsg As String
        'Dim nOffset As Integer = 0
        Dim byteVal As Byte
        'Dim iteratorACType As KeyValuePair(Of Byte, String)
        'Dim nIdx, nIdxMax As Integer
        ' 1. Printout Tag Value
        tlvValOp.ptrFuncCbDummyParser2Printout(tlvValOp, refobjTLVAttr, o_tagTxnInfo)

        '2. Printout Detail Info
        o_tagTxnInfo.AddIndent()
        With refobjTLVAttr
            byteVal = .m_arrayTLVal(0)
            PktParser2Printout_Common_ByteBitOperation(tlvValOp, refobjTLVAttr, o_tagTxnInfo, 1, byteVal, m_dictPktParser2Printout_MasterCard_DF810B_DS_Summary_Status)

        End With
        o_tagTxnInfo.DecIndent()

    End Sub
    '===========================[ DF810E ]===========================
    Public Shared m_dictPktParser2Printout_MasterCard_DF810E_Post_Gen_AC_Put_Data_Status As Dictionary(Of Byte, tagTxnResponseBitValResolveInfo) = m_dictPktParser2Printout_MasterCard_DF810F_PreGenACPutDataStatus
    Public Shared Sub PktParser2Printout_MasterCard_DF810E_Post_Gen_AC_Put_Data_Status(ByVal tlvValOp As tagTLVNewOp, ByRef refobjTLVAttr As tagTLV, ByRef o_tagTxnInfo As ClassTransaction.TAG_TXN_PARSE_RESPONSE_INFO)
        'Dim strMsg As String
        'Dim nOffset As Integer = 0
        Dim byteVal As Byte
        'Dim iteratorACType As KeyValuePair(Of Byte, String)
        'Dim nIdx, nIdxMax As Integer
        ' 1. Printout Tag Value
        tlvValOp.ptrFuncCbDummyParser2Printout(tlvValOp, refobjTLVAttr, o_tagTxnInfo)

        '2. Printout Detail Info
        o_tagTxnInfo.AddIndent()


        With refobjTLVAttr
            byteVal = .m_arrayTLVal(0)

            'Byte 1, b8 = CDA signature requested
            PktParser2Printout_Common_ByteBitOperation(tlvValOp, refobjTLVAttr, o_tagTxnInfo, 1, byteVal, m_dictPktParser2Printout_MasterCard_DF810E_Post_Gen_AC_Put_Data_Status)

        End With
        '
        o_tagTxnInfo.DecIndent()

    End Sub


    ''===========================[ DF810F ]===========================
    Public Shared m_dictPktParser2Printout_MasterCard_DF810F_PreGenACPutDataStatus As Dictionary(Of Byte, tagTxnResponseBitValResolveInfo) = New Dictionary(Of Byte, tagTxnResponseBitValResolveInfo) From {
     {&H8, New tagTxnResponseBitValResolveInfo(&H80, "Completed")}
     }
    Public Shared Sub PktParser2Printout_MasterCard_DF810F_PreGenACPutDataStatus(ByVal tlvValOp As tagTLVNewOp, ByRef refobjTLVAttr As tagTLV, ByRef o_tagTxnInfo As ClassTransaction.TAG_TXN_PARSE_RESPONSE_INFO)
        'Dim strMsg As String
        'Dim nOffset As Integer = 0
        Dim byteVal As Byte
        'Dim iteratorACType As KeyValuePair(Of Byte, String)
        'Dim nIdx, nIdxMax As Integer
        ' 1. Printout Tag Value
        tlvValOp.ptrFuncCbDummyParser2Printout(tlvValOp, refobjTLVAttr, o_tagTxnInfo)

        '2. Printout Detail Info
        o_tagTxnInfo.AddIndent()


        With refobjTLVAttr
            byteVal = .m_arrayTLVal(0)

            'Byte 1, b8 = CDA signature requested
            PktParser2Printout_Common_ByteBitOperation(tlvValOp, refobjTLVAttr, o_tagTxnInfo, 1, byteVal, m_dictPktParser2Printout_MasterCard_DF810F_PreGenACPutDataStatus)

        End With
        '
        o_tagTxnInfo.DecIndent()

    End Sub

    ''===========================[ DF8114 ]===========================
    Public Shared m_dictPktParser2Printout_MC_DF8114_AC_Type As Dictionary(Of Byte, String) = New Dictionary(Of Byte, String) From {
           {&H0, "AAC"},
           {&H1, "TC"},
           {&H2, "ARQC"},
           {&H3, "RFU"}
           }
    Public Shared m_dictPktParser2Printout_MC_DF8114_AC_Type_B1b5_CDASignatureRequested As Dictionary(Of Byte, tagTxnResponseBitValResolveInfo) = New Dictionary(Of Byte, tagTxnResponseBitValResolveInfo) From {
     {&H5, New tagTxnResponseBitValResolveInfo(&H10, "CDA signature requested")}
     }
    Public Shared Sub PktParser2Printout_MasterCard_DF8114_ReferenceControlParameter(ByVal tlvValOp As tagTLVNewOp, ByRef refobjTLVAttr As tagTLV, ByRef o_tagTxnInfo As ClassTransaction.TAG_TXN_PARSE_RESPONSE_INFO)
        'Dim strMsg As String
        'Dim nOffset As Integer = 0
        Dim byteVal As Byte
        'Dim iteratorACType As KeyValuePair(Of Byte, String)
        'Dim nIdx, nIdxMax As Integer
        ' 1. Printout Tag Value
        tlvValOp.ptrFuncCbDummyParser2Printout(tlvValOp, refobjTLVAttr, o_tagTxnInfo)

        '2. Printout Detail Info
        'o_tagTxnInfo.AddIndent()


        With refobjTLVAttr
            byteVal = .m_arrayTLVal(0)

            'Byte 1, b8-b7 = AC Type
            PktParser2Printout_Common_ByteValueOperation(tlvValOp, refobjTLVAttr, o_tagTxnInfo, (byteVal >> 6) And &H3, m_dictPktParser2Printout_MC_DF8114_AC_Type, "AC Type (Byte 1, b8-7)")

            'Byte 1, b5 = CDA signature requested
            PktParser2Printout_Common_ByteBitOperation(tlvValOp, refobjTLVAttr, o_tagTxnInfo, 1, byteVal, m_dictPktParser2Printout_MC_DF8114_AC_Type_B1b5_CDASignatureRequested)

        End With
        '
        'o_tagTxnInfo.DecIndent()

    End Sub
    '===========================[ DF8115 ]===========================
    Public Shared m_dictPktParser2Printout_MC_DF8115_L1 As Dictionary(Of Byte, String) = New Dictionary(Of Byte, String) From {
               {&H0, "OK"},
               {&H1, "TIME OUT ERROR"},
               {&H2, "TRANSMISSION ERROR"}
               }
    Public Shared m_dictPktParser2Printout_MC_DF8115_L2 As Dictionary(Of Byte, String) = New Dictionary(Of Byte, String) From {
                {&H0, "OK"},
                {&H1, "CARD DATA MISSING"},
                {&H2, "CAM FAILED"},
                {&H3, "STATUS BYTE"},
                {&H4, "PARSING ERROR"},
                {&H5, "MAX LIMIT EXCEEDED"},
                {&H6, "CARD DATA ERROR"},
                {&H7, "MAGSTRIPE NOT SUPPORTED"},
                {&H8, "NO PPSE"},
                {&H9, "PPSE FAULT"},
                {&HA, "EMPTY CANDIDATE LIST"},
                {&HB, "IDS READ ERROR"},
                {&HC, "IDS WRITE ERROR"},
                {&HD, "IDS DATA ERROR"},
                {&HE, "IDS NO MATCHING AC"},
                {&HF, "TERMINAL DATA ERROR"}
                }
    Public Shared m_dictPktParser2Printout_MC_DF8115_L3 As Dictionary(Of Byte, String) = New Dictionary(Of Byte, String) From {
                {&H0, "OK"},
                {&H1, "TIME OUT"},
                {&H2, "STOP"},
                {&H3, "AMOUNT NOT PRESENT"}
            }
    Public Shared m_dictPktParser2Printout_MC_DF8115_Dummy As Dictionary(Of Byte, String) = New Dictionary(Of Byte, String) From {
                        {&H0, "Dummy"}
        }
    Public Shared Sub PktParser2Printout_MasterCard_DF8115_ErrorIndication(ByVal tlvValOp As tagTLVNewOp, ByRef refobjTLVAttr As tagTLV, ByRef o_tagTxnInfo As ClassTransaction.TAG_TXN_PARSE_RESPONSE_INFO)
        Dim strMsg, strTmp As String
        Dim nOffset As Integer = 0
        Dim byteVal As Byte
        Dim arrayByteData As Byte() = New Byte(7) {}
        'Dim nArrayLen As Integer
        Dim iteratorBytesStringListMap As KeyValuePair(Of String, Dictionary(Of Byte, String))
        Dim dictByteStringListMap As Dictionary(Of Byte, KeyValuePair(Of String, Dictionary(Of Byte, String))) = New Dictionary(Of Byte, KeyValuePair(Of String, Dictionary(Of Byte, String))) From {
            {&H0, New KeyValuePair(Of String, Dictionary(Of Byte, String))("L1", m_dictPktParser2Printout_MC_DF8115_L1)},
            {&H1, New KeyValuePair(Of String, Dictionary(Of Byte, String))("L2", m_dictPktParser2Printout_MC_DF8115_L2)},
            {&H2, New KeyValuePair(Of String, Dictionary(Of Byte, String))("L3", m_dictPktParser2Printout_MC_DF8115_L3)},
            {&H3, New KeyValuePair(Of String, Dictionary(Of Byte, String))("SW12", m_dictPktParser2Printout_MC_DF8115_Dummy)},
            {&H4, New KeyValuePair(Of String, Dictionary(Of Byte, String))("Msg On Error", m_dictPktParser2Printout_MC_DF8116_Byte1_MessageIdentifier)}
            }
        Dim nIdx, nIdxSub, nIdxMax As Integer
        ' 1. Printout Tag Value
        tlvValOp.ptrFuncCbDummyParser2Printout(tlvValOp, refobjTLVAttr, o_tagTxnInfo)

        '2. Printout Detail Info
        o_tagTxnInfo.AddIndent()

        With refobjTLVAttr
            '1. L1: 1 byte, L2: 1 byte, L3: 1 byte
            '2.  SW12: 2 bytes
            '3. Msg On Error: 1 byte
            nIdxMax = refobjTLVAttr.m_nLen - 1 ' SW12: 2 bytes + Msg On Error : 1 bytes + 1 ( 0-based index )
            nIdxSub = 0
            For nIdx = 0 To nIdxMax
                byteVal = .m_arrayTLVal(nIdx)
                iteratorBytesStringListMap = dictByteStringListMap.Item(nIdxSub)
                '
                If iteratorBytesStringListMap.Key.Contains("S") Then
                    'Speical for "Status Word" (i.e. SW) Code
                    Dim u16SW12 As UInt16
                    u16SW12 = byteVal
                    u16SW12 = ((u16SW12 << 8) And &HFF00) + .m_arrayTLVal(nIdx + 1)
                    strTmp = String.Format("{0,4:X4}", u16SW12)
                    strMsg = iteratorBytesStringListMap.Key + " :  " + strTmp
                    nIdx = nIdx + 1
                Else
                    strTmp = String.Format("{0,2:X2}", byteVal)
                    If iteratorBytesStringListMap.Value.ContainsKey(byteVal) Then
                        strMsg = iteratorBytesStringListMap.Key + "( " + strTmp + " )  : " + iteratorBytesStringListMap.Value.Item(byteVal)
                    Else
                        strMsg = iteratorBytesStringListMap.Key + " ( " + strTmp + " )  : < Unknown Code >"
                    End If
                End If
                nIdxSub = nIdxSub + 1
                o_tagTxnInfo.Add2Printout(strMsg)
            Next

        End With

        '
        dictByteStringListMap.Clear()

        '
        o_tagTxnInfo.DecIndent()

    End Sub

    Enum enumParseTxnResponseYesNo As Byte
        PARSE_TXN_RESPONSE_NO = 0
        PARSE_TXN_RESPONSE_YES
    End Enum
    Public Shared m_dictPktParser2Printout_TxnResponseCommonStringYesNo As Dictionary(Of Byte, String) = New Dictionary(Of Byte, String) From {
            {&H0, "No"},
            {&H1, "Yes"}}
    Public Structure tagTxnResponseBitValResolveInfo
        Public m_byteMaskBit As Byte
        Public m_strMsg As String

        Sub New(ByVal byteMaskBit As Byte, ByVal strMsg As String)
            m_byteMaskBit = byteMaskBit
            m_strMsg = strMsg
        End Sub
    End Structure
    '
    '===================================[ 50 ]====================================
    Shared Sub PktParser2Printout_50_ApplicationLabel(ByVal tlvValOp As tagTLVNewOp, ByRef refobjTLVAttr As tagTLV, ByRef o_tagTxnInfo As ClassTransaction.TAG_TXN_PARSE_RESPONSE_INFO)
        Dim strMsg = "", strTmp = "", strTmp2 As String = ""
        Dim nIndent As Integer = 3
        'Dim strFuncName As String = System.Reflection.MethodInfo.GetCurrentMethod().Name
        ClassTableListMaster.ConvertFromHexToAscii(refobjTLVAttr.m_arrayTLVal, strMsg)

        strTmp = tlvValOp.m_strTag + tagTLV.s_strPrintoutTransactionMsgDelimiter + tlvValOp.m_strComment
        PktParser2Printout_Common_FormatLine_GetDotsString(strTmp, nIndent, strTmp2)
        strTmp = strTmp + strTmp2 + " = " + strMsg

        o_tagTxnInfo.Add2PrintoutAbs(strTmp, nIndent)
    End Sub
    '
    '===================================[ 56 ]====================================
    Shared Sub PktParser2Printout_56_Track_1_Data(ByVal tlvValOp As tagTLVNewOp, ByRef refobjTLVAttr As tagTLV, ByRef o_tagTxnInfo As ClassTransaction.TAG_TXN_PARSE_RESPONSE_INFO)
        Dim strTmp = "", strTmp1 As String = ""
        Dim byteVal As Byte
        'Dim byteStrIdx As Byte
        Dim arrayByteData As Byte() = Nothing
        Dim arrayByteTag As Byte() = New Byte(2) {}
        Dim nIdx As Byte = 0
        Dim nSizeRest As Byte
        'Dim iteratorBitParse As KeyValuePair(Of Byte, tagTxnResponseBitValResolveInfo)
        Dim strFuncName As String = System.Reflection.MethodInfo.GetCurrentMethod().Name
        'Dim tlv As tagTLV
        'Dim tlvNewOp As tagTLVNewOp
        Dim nIndent As Integer = 3
        Dim nDataLen As Integer

        Try
            ' 1. Printout Tag Value
            tlvValOp.ptrFuncCbDummyParser2Printout(tlvValOp, refobjTLVAttr, o_tagTxnInfo)

            '2. Printout Detail Info
            o_tagTxnInfo.AddIndent()

            With refobjTLVAttr
                '--------------------------[ Byte 0 , Len = 1 Byte(s), Format Code 1 '42' ]--------------------------
                byteVal = .m_arrayTLVal(nIdx)
                If (byteVal <> &H42) Then
                    o_tagTxnInfo.Add2Printout("[Err] " + strFuncName + "(), No '42' Format Code Found")
                    Return
                End If
                nIdx = nIdx + 1
                '        
                '(M)--------------------------[ Byte 0~20, Len = 0~19 Byte(s), , assume Len = N1 Primary Account Number var up to 19 digits (BCD?) ]--------------------------
                '--------------------------[ Byte N1+1, Len = 1 Byte(s), 'Field Separator 1 '5E' ]--------------------------
                ClassTableListMaster.SubtractByteData(.m_arrayTLVal, nIdx, &H5E, arrayByteData)
                If (arrayByteData IsNot Nothing) Then
                    ClassTableListMaster.ConvertFromHexToAscii(arrayByteData, strTmp)
                    ClassTableListMaster.ConvertFromArrayByte2String(arrayByteData, strTmp1)
                    strTmp = strTmp + "( " + strTmp1 + " )"
                    Erase arrayByteData
                Else
                    strTmp = "N/A"
                End If
                o_tagTxnInfo.Add2PrintoutAbs("Primary Account Number : " + strTmp, nIndent)
                '

                '(M)--------------------------[ Byte N1+2~N1+27, Len = 2~26 Byte(s), Assume Len = N2, Name 2-26 (see ISO/IEC 7813) ]--------------------------
                '--------------------------[ Byte N1+N2+2, Len = 1 Byte(s), Field Separator 1 '5E' ]--------------------------
                ClassTableListMaster.SubtractByteData(.m_arrayTLVal, nIdx, &H5E, arrayByteData)
                If (arrayByteData IsNot Nothing) Then
                    ClassTableListMaster.ConvertFromHexToAscii(arrayByteData, strTmp)
                    ClassTableListMaster.ConvertFromArrayByte2String(arrayByteData, strTmp1)
                    strTmp = strTmp + "( " + strTmp1 + " )"
                Else
                    strTmp = "N/A"
                End If
                o_tagTxnInfo.Add2PrintoutAbs("Name ( " & arrayByteData.Count & " bytes) : " + strTmp, nIndent)

                '
                '(M)--------------------------[ Byte N1+N2+3~N1+N2+6, Len = 4 Byte(s), Expiry Date 4 YYMM ]--------------------------
                nDataLen = 4
                If arrayByteData.Count < nDataLen Then
                    'make sure buffer size is at least 4 bytes or bigger
                    Erase arrayByteData
                    arrayByteData = New Byte(nDataLen - 1) {}
                End If
                Array.Copy(.m_arrayTLVal, nIdx, arrayByteData, 0, nDataLen)
                ClassTableListMaster.ConvertFromHexToAscii(arrayByteData, 0, nDataLen, strTmp)
                ClassTableListMaster.ConvertFromArrayByte2String(arrayByteData, 0, nDataLen, strTmp1)
                strTmp = strTmp + "( " + strTmp1 + " )"
                o_tagTxnInfo.Add2PrintoutAbs("Expiry Date (4 bytes) : " + strTmp, nIndent)
                nIdx = nIdx + nDataLen
                '
                '(M)--------------------------[ Byte N1+N2+7~N1+N2+9 , Len = 3 Byte(s), Service Code 3 digits ]--------------------------
                nDataLen = 3
                Array.Copy(.m_arrayTLVal, nIdx, arrayByteData, 0, nDataLen)
                ClassTableListMaster.ConvertFromHexToAscii(arrayByteData, 0, nDataLen, strTmp)
                ClassTableListMaster.ConvertFromArrayByte2String(arrayByteData, 0, nDataLen, strTmp1)
                strTmp = strTmp + "( " + strTmp1 + " )"
                o_tagTxnInfo.Add2PrintoutAbs("Service Code (3 bytes) : " + strTmp, nIndent)
                nIdx = nIdx + nDataLen
                '
                '--------------------------[ Byte N1+N2+10~N1+N2+10+N3-1, Len =  Vars Byte(s), Assume Len = N3, Discretionary Data var. ans ]--------------------------
                'Check Rest Data is available
                If (nIdx < .m_arrayTLVal.Count) Then
                    nSizeRest = .m_arrayTLVal.Count - nIdx
                    Erase arrayByteData
                    arrayByteData = New Byte(nSizeRest - 1) {}
                    '
                    Array.Copy(.m_arrayTLVal, nIdx, arrayByteData, 0, nSizeRest)

                    'ClassTableListMaster.ConvertFromArrayByte2String(arrayByteData, 0, 3, strTmp1, False)
                    o_tagTxnInfo.AddIndent()
                    ClassTableListMaster.ConvertFromHexToAscii(arrayByteData, 0, nSizeRest, strTmp)
                    ClassTableListMaster.ConvertFromArrayByte2String(arrayByteData, 0, nSizeRest, strTmp1)
                    strTmp = strTmp + "( " + strTmp1 + " )"
                    o_tagTxnInfo.Add2PrintoutAbs("Discretionary Data ( " & nSizeRest & " bytes) : " + strTmp, nIndent)
                    '
                    o_tagTxnInfo.DecIndent()
                    '
                End If
            End With
            '
            o_tagTxnInfo.DecIndent()
        Catch ex As Exception
            If arrayByteData IsNot Nothing Then
                Erase arrayByteData
            End If

            o_tagTxnInfo.Add2Printout("[Err] " + strFuncName + "() = " + ex.Message)
        Finally
            If arrayByteData IsNot Nothing Then
                Erase arrayByteData
            End If
            If arrayByteTag IsNot Nothing Then
                Erase arrayByteTag
            End If
        End Try

    End Sub
    '
    '===================================[ 9F67 ]====================================
    Shared Sub PktParser2Printout_9F67_MC3_NATC_Track2_OR_Amex_PaymentDeviceTypeAndCapabilitiesIndicator(ByVal tlvValOp As tagTLVNewOp, ByRef refobjTLVAttr As tagTLV, ByRef o_tagTxnInfo As ClassTransaction.TAG_TXN_PARSE_RESPONSE_INFO)

        tlvValOp.m_strComment = "Master:NATC (Track2); Amex:Payment Device Type And CapabilitiesIndicator"
        ' 1. Printout Tag Value
        tlvValOp.ptrFuncCbDummyParser2Printout(tlvValOp, refobjTLVAttr, o_tagTxnInfo)

    End Sub
    '
    '===================================[ 9F6B ]====================================
    Shared Sub PktParser2Printout_9F6B_Track_2_Data(ByVal tlvValOp As tagTLVNewOp, ByRef refobjTLVAttr As tagTLV, ByRef o_tagTxnInfo As ClassTransaction.TAG_TXN_PARSE_RESPONSE_INFO)
        'PktParser2Printout_57_Track_2_Equivalent_Data(tlvValOp, refobjTLVAttr, o_tagTxnInfo)
        Dim strMsg = "", strTmp = "", strTmp2 = "", strTmp3 As String = ""
        Dim nIndent As Integer

        ClassTableListMaster.ConvertFromHexToAscii(refobjTLVAttr.m_arrayTLVal, strTmp)
        ClassTableListMaster.ConvertFromArrayByte2String(refobjTLVAttr.m_arrayTLVal, strTmp2)
        With tlvValOp
            strMsg = .m_strTag + tagTLV.s_strPrintoutTransactionMsgDelimiter + .m_strComment
            PktParser2Printout_Common_FormatLine_GetDotsString(strMsg, nIndent, strTmp3)
            strMsg = strMsg + strTmp3 + " = " + strTmp + " (" + strTmp2 + ")"
            o_tagTxnInfo.Add2PrintoutAbs(strMsg, nIndent)
        End With
    End Sub

    '===================================[ 57 ]====================================
    Shared Sub PktParser2Printout_57_Track_2_Equivalent_Data(ByVal tlvValOp As tagTLVNewOp, ByRef refobjTLVAttr As tagTLV, ByRef o_tagTxnInfo As ClassTransaction.TAG_TXN_PARSE_RESPONSE_INFO)
        Dim strTmp = "", strTmp1 As String = ""
        Dim arrayByteData As Byte() = Nothing
        Dim arrayByteTag As Byte() = New Byte(2) {}
        Dim nIdx As Byte = 0
        Dim nLen As Byte = 0
        Dim strFuncName As String = System.Reflection.MethodInfo.GetCurrentMethod().Name

        Dim nIndent As Integer = 2

        Try
            ' 1. Printout Tag Value
            tlvValOp.ptrFuncCbDummyParser2Printout(tlvValOp, refobjTLVAttr, o_tagTxnInfo)

            ' Incorrect parser, don't do advanced breaking down until know it's structure
            Return

            '2. Printout Detail Info
            'o_tagTxnInfo.AddIndent()

            With refobjTLVAttr
                '                
                ' CL 2.3 , Book C-2, A.1.164 , Track 2 Equivalent
                '--------------------------[ Byte 0~N1-1 , Len = n Byte(s), Primary Account Number var. up to 19 nibbles N1]--------------------------
                '--------------------------[ Byte N1, Len = 1 Byte(s), 'Field Separator 1 'D' = &H44]--------------------------
                ClassTableListMaster.SubtractByteData(.m_arrayTLVal, nIdx, &H44, arrayByteData)
                If (arrayByteData IsNot Nothing) Then
                    ClassTableListMaster.ConvertFromHexToAscii(arrayByteData, strTmp)
                    ClassTableListMaster.ConvertFromArrayByte2String(arrayByteData, strTmp1)
                    strTmp = strTmp + "( " + strTmp1 + " )"
                    Erase arrayByteData
                Else
                    strTmp = "N/A"
                End If
                o_tagTxnInfo.Add2PrintoutAbs("Primary Account Number : " + strTmp, nIndent)
                '        
                '(M)--------------------------[ Byte N1+2~N1+3, Len = 2 Byte(s) , Expiration Date (YYMM) 2 n (YYMM) ]--------------------------
                nLen = 3
                If arrayByteData.Count < nLen Then
                    'make sure buffer size is at least 3 bytes or bigger
                    Erase arrayByteData
                    arrayByteData = New Byte(nLen - 1) {}
                End If
                Array.Copy(.m_arrayTLVal, nIdx, arrayByteData, 0, 2)
                ClassTableListMaster.ConvertFromHexToAscii(arrayByteData, 0, nLen, strTmp)
                ClassTableListMaster.ConvertFromArrayByte2String(arrayByteData, 0, nLen, strTmp1)
                strTmp = strTmp + "( " + strTmp1 + " )"
                o_tagTxnInfo.Add2PrintoutAbs("Expiration Date (2 bytes, YYMM) : " + strTmp, nIndent)
                nIdx = nIdx + 2
                '
                '(M)--------------------------[ Byte N1+4~N1+6 , Len = 3 Byte(s), Service Code 3 digits ]--------------------------
                nLen = 3
                Array.Copy(.m_arrayTLVal, nIdx, arrayByteData, 0, nLen)
                ClassTableListMaster.ConvertFromHexToAscii(arrayByteData, 0, nLen, strTmp)
                ClassTableListMaster.ConvertFromArrayByte2String(arrayByteData, 0, nLen, strTmp1)
                strTmp = strTmp + "( " + strTmp1 + " )"
                o_tagTxnInfo.Add2PrintoutAbs("Service Code (3 bytes) : " + strTmp, nIndent)
                nIdx = nIdx + nLen
                '
                '--------------------------[ Byte N1+7~N1+7+Var, Len =  Vars Byte(s), Assume Len = N2, Discretionary Data var. ans ]--------------------------
                'Check Rest Data is available
                If (nIdx < .m_arrayTLVal.Count) Then
                    ClassTableListMaster.SubtractByteData(.m_arrayTLVal, nIdx, &H46, arrayByteData)
                    If (arrayByteData IsNot Nothing) Then
                        'ClassTableListMaster.ConvertForomHexToAscii(arrayByteData, strTmp)
                        ClassTableListMaster.ConvertFromArrayByte2String(arrayByteData, strTmp)
                        Erase arrayByteData
                    Else
                        strTmp = "N/A"
                    End If
                    o_tagTxnInfo.Add2PrintoutAbs("Discretionary Data  : " + strTmp, nIndent)
                    '
                End If
            End With
            '
            'o_tagTxnInfo.DecIndent()
        Catch ex As Exception
            If arrayByteData IsNot Nothing Then
                Erase arrayByteData
            End If

            o_tagTxnInfo.Add2Printout("[Err] " + strFuncName + "() = " + ex.Message)
        Finally
            If arrayByteData IsNot Nothing Then
                Erase arrayByteData
            End If
            If arrayByteTag IsNot Nothing Then
                Erase arrayByteTag
            End If
        End Try

    End Sub
    '
    '===================================[ 61 ]====================================
    Shared Sub PktParser2Printout_61_ApplicationTemplate(ByVal tlvValOp As tagTLVNewOp, ByRef refobjTLVAttr As tagTLV, ByRef o_tagTxnInfo As ClassTransaction.TAG_TXN_PARSE_RESPONSE_INFO)
        PktParser2Printout_Common_SubTagsParser(tlvValOp, refobjTLVAttr, o_tagTxnInfo, o_tagTxnInfo.m_dictTlvsTxnResult_77_ResponseMessageTemplateFormat2)
    End Sub
    '===================================[ 6F ]====================================
    Shared Sub PktParser2Printout_6F_FileControlInformationTemplate(ByVal tlvValOp As tagTLVNewOp, ByRef refobjTLVAttr As tagTLV, ByRef o_tagTxnInfo As ClassTransaction.TAG_TXN_PARSE_RESPONSE_INFO)
        PktParser2Printout_Common_SubTagsParser(tlvValOp, refobjTLVAttr, o_tagTxnInfo, o_tagTxnInfo.m_dictTlvsTxnResult_6F_FileControlInformationTemplate)
    End Sub

    '===================================[ 6F ]====================================
    Shared Sub PktParser2Printout_70_MC3_READRECORDResponseMessageTemplate_Or_Amex_ApplicationElementaryFile_ieAEF_DataTemplate(ByVal tlvValOp As tagTLVNewOp, ByRef refobjTLVAttr As tagTLV, ByRef o_tagTxnInfo As ClassTransaction.TAG_TXN_PARSE_RESPONSE_INFO)

        tlvValOp.m_strComment = "Master:READ RECORD Response Message Template; Amex:Application Elementary File (AEF) Data Template"

        tlvValOp.ptrFuncCbDummyParser2Printout(tlvValOp, refobjTLVAttr, o_tagTxnInfo)
    End Sub

    '===================================[ 77 ]====================================
    Shared Sub PktParser2Printout_77_MC3_WithTagLenght_Or_Amex_WIthoutTagLength_ResponseMessageTemplateFormat2(ByVal tlvValOp As tagTLVNewOp, ByRef refobjTLVAttr As tagTLV, ByRef o_tagTxnInfo As ClassTransaction.TAG_TXN_PARSE_RESPONSE_INFO)
        Select Case Form01_Main.GetInstance().ModeSelectionGet()
            'Case ClassUIDriver.EnumUIDriverEventEnableDisable.UI_ENABLE_COMM_OPEN_PART_NUM_SELECTED_MODE_TEST
            Case Else
                PktParser2Printout_Common_SubTagsParser(tlvValOp, refobjTLVAttr, o_tagTxnInfo, o_tagTxnInfo.m_dictTlvsTxnResult_77_ResponseMessageTemplateFormat2)
                'Case Else
                'Case ClassUIDriver.EnumUIDriverEventEnableDisable.UI_ENABLE_COMM_OPEN_PAYMENT_SELECT_AMERICAN_EXPRESS
                ' tlvValOp.ptrFuncCbDummyParser2Printout(tlvValOp, refobjTLVAttr, o_tagTxnInfo)
        End Select
    End Sub


    '===================================[ 82 ]====================================
    Public Shared m_dictPktParser2Printout_TxnResponse_82_AIP_Byte1 As Dictionary(Of Byte, tagTxnResponseBitValResolveInfo) = New Dictionary(Of Byte, tagTxnResponseBitValResolveInfo) From {
         {&H7, New tagTxnResponseBitValResolveInfo(&H40, "SDA Supported")},
         {&H6, New tagTxnResponseBitValResolveInfo(&H20, "DDA Supported")},
         {&H5, New tagTxnResponseBitValResolveInfo(&H10, "Cardholder verification is supported")},
         {&H4, New tagTxnResponseBitValResolveInfo(&H8, "Terminal risk management is to be performed")},
         {&H3, New tagTxnResponseBitValResolveInfo(&H4, "Issuer Authentication is supported")},
         {&H2, New tagTxnResponseBitValResolveInfo(&H2, "On device cardholder verification is supported")},
         {&H1, New tagTxnResponseBitValResolveInfo(&H1, "CDA Supported")}
         }
    '' 1. Printout Tag Value
    '    tlvValOp.ptrFuncCbDummyParser2Printout(tlvValOp, refobjTLVAttr, o_tagTxnInfo)

    'PktParser2Printout_Common_ByteBitOperation(tlvValOp, refobjTLVAttr, o_tagTxnInfo, 1, refobjTLVAttr.m_arrayTLVal(0), m_dictPktParser2Printout_TxnResponse_DF811B_KernelConfiguration_Byte1)
    Shared Sub PktParser2Printout_82_AIP(ByVal tlvValOp As tagTLVNewOp, ByRef refobjTLVAttr As tagTLV, ByRef o_tagTxnInfo As ClassTransaction.TAG_TXN_PARSE_RESPONSE_INFO)
        Dim strMsg As String = ""
        Dim strTmp As String = ""
        Dim byteVal As Byte
        Dim byteStrIdx As Byte
        Dim nIdx As Byte = 0
        'Dim iteratorBitParse As KeyValuePair(Of Byte, tagTxnResponseBitValResolveInfo)
        Dim nIndent As Integer = 3
        ' 1. Printout Tag Value
        tlvValOp.ptrFuncCbDummyParser2Printout(tlvValOp, refobjTLVAttr, o_tagTxnInfo)

        '2. Printout Detail Info
        o_tagTxnInfo.AddIndent()

        With refobjTLVAttr
            '--------------------------[ Byte 0 ]--------------------------
            PktParser2Printout_Common_ByteBitOperation(tlvValOp, refobjTLVAttr, o_tagTxnInfo, 1, .m_arrayTLVal(0), m_dictPktParser2Printout_TxnResponse_82_AIP_Byte1)
            'byteVal = .m_arrayTLVal(nIdx)
            'o_tagTxnInfo.Add2Printout("Byte " & nIdx + 1)
            'o_tagTxnInfo.AddIndent()
            'For Each iteratorBitParse In m_dictPktParser2Printout_TxnResponse_82_AIP_Byte1
            '    If ((byteVal And iteratorBitParse.Value.m_byteMaskBit) = iteratorBitParse.Value.m_byteMaskBit) Then
            '        byteStrIdx = enumParseTxnResponseYesNo.PARSE_TXN_RESPONSE_YES
            '    Else
            '        byteStrIdx = enumParseTxnResponseYesNo.PARSE_TXN_RESPONSE_NO
            '    End If
            '    o_tagTxnInfo.Add2Printout(".Bit" & (8 - iteratorBitParse.Key) & " : " + m_dictPktParser2Printout_TxnResponseCommonStringYesNo.Item(byteStrIdx) + " <-- " + iteratorBitParse.Value.m_strMsg)
            'Next
            'o_tagTxnInfo.DecIndent()
            '--------------------------[ Byte 1 ]--------------------------
            nIdx = nIdx + 1
            byteVal = .m_arrayTLVal(nIdx)
            If ((byteVal And &H80) = &H80) Then
                byteStrIdx = enumParseTxnResponseYesNo.PARSE_TXN_RESPONSE_YES
            Else
                byteStrIdx = enumParseTxnResponseYesNo.PARSE_TXN_RESPONSE_NO
            End If
            strMsg = tagTLV.s_strPrintoutTransactionMsgDelimiter + "B2b8 : EMV mode is supported"
            PktParser2Printout_Common_FormatLine_GetDotsString(strMsg, nIndent, strTmp)
            strMsg = strMsg + strTmp + " = " + String.Format("{0,2:X2}", byteStrIdx) + " (" + m_dictPktParser2Printout_TxnResponseCommonStringYesNo.Item(byteStrIdx) + ")"
            o_tagTxnInfo.Add2PrintoutAbs(strMsg, nIndent)
        End With
        '
        o_tagTxnInfo.DecIndent()
    End Sub
    '

    '===================================[ 88 ]====================================
    Shared Sub PktParser2Printout_88_Amex_ShortFileIdentifier_SFI(ByVal tlvValOp As tagTLVNewOp, ByRef refobjTLVAttr As tagTLV, ByRef o_tagTxnInfo As ClassTransaction.TAG_TXN_PARSE_RESPONSE_INFO)
        Dim strMsg = "", strTmp = "", strTmp2 As String = ""
        Dim byteVal As Byte = refobjTLVAttr.m_arrayTLVal(0)
        Dim nIndent As Integer

        If (byteVal > 30) Or (byteVal < 1) Then
            'Out of Range Value
            strTmp = tagTLV.s_strPrintoutTransactionUnknownTag
        ElseIf (byteVal > 20) Then
            '21-30: Issuer Specific
            strTmp = "Issuer Specific"
        ElseIf (byteVal > 10) Then
            '11-20: American Express specific
            strTmp = "American Express specific"
        Else
            '1-10: Governed by joint payment systems
            strTmp = "Governed by joint payment systems"
        End If

        strMsg = refobjTLVAttr.m_strTag + tagTLV.s_strPrintoutTransactionMsgDelimiter + tlvValOp.m_strComment
        PktParser2Printout_Common_FormatLine_GetDotsString(strMsg, nIndent, strTmp2)
        o_tagTxnInfo.Add2PrintoutAbs(strMsg + strTmp2 + " = " + String.Format("{0,2:X2} ( ", byteVal) + strTmp + " )", nIndent)
    End Sub
    '
    '===================================[ 89 ]====================================
    Shared Sub PktParser2Printout_89_Amex_AuthorizationCode(ByVal tlvValOp As tagTLVNewOp, ByRef refobjTLVAttr As tagTLV, ByRef o_tagTxnInfo As ClassTransaction.TAG_TXN_PARSE_RESPONSE_INFO)
        PktParser2Printout_Common_Hex2Ascii(tlvValOp, refobjTLVAttr, o_tagTxnInfo)
    End Sub
    '
    '===================================[ 8A ]====================================
    Shared Sub PktParser2Printout_8A_Amex_AuthorizationResponseCode(ByVal tlvValOp As tagTLVNewOp, ByRef refobjTLVAttr As tagTLV, ByRef o_tagTxnInfo As ClassTransaction.TAG_TXN_PARSE_RESPONSE_INFO)
        PktParser2Printout_Common_Hex2Ascii(tlvValOp, refobjTLVAttr, o_tagTxnInfo)
    End Sub
    '
    '===================================[ 8E ]====================================
    Public Shared m_dictPktParser2Printout_TxnResponse_8E_CVMList_CVM_Code_b6_b1 As Dictionary(Of Byte, String) = New Dictionary(Of Byte, String) From {
        {&H0, "Fail CVM processing"},
        {&H1, "Plaintext PIN verification performed by ICC"},
        {&H2, "Enciphered PIN verified online"},
        {&H3, "Plaintext PIN verification performed by ICC and signature (paper)"},
        {&H4, "Enciphered PIN verification performed by ICC"},
        {&H5, "Enciphered PIN verification performed by ICC and signature (paper)"},
        {&H1E, "Signature (paper)"},
        {&H1F, "No CVM required"},
        {&H3F, "This value is not available for use"}
        }
    Shared Sub PktParser2Printout_8E_CVMList(ByVal tlvValOp As tagTLVNewOp, ByRef refobjTLVAttr As tagTLV, ByRef o_tagTxnInfo As ClassTransaction.TAG_TXN_PARSE_RESPONSE_INFO)
        Dim strMsg = "", strTmp = "", strTmp2 As String = ""
        Dim byteVal As Byte
        'Dim byteStrIdx As Byte
        Dim nIdx, nIdxMax As Byte
        Dim strFuncName As String = System.Reflection.MethodInfo.GetCurrentMethod().Name
        Dim nIndent As Integer = 6
        Dim byteCVMCode, byteCVMCoditionCodes As Byte
        '
        If (refobjTLVAttr.m_nLen < 10) Then
            ClassTableListMaster.ConvertFromArrayByte2String(refobjTLVAttr.m_arrayTLVal, strTmp)
            Throw New Exception("[Err][Invalid CVM Data Format] Invalid CVM List Data Element In 10( = 4+4+2) bytes Size, Len = " & refobjTLVAttr.m_nLen & ", Data = " + strTmp)
        End If
        '
        tlvValOp.ptrFuncCbDummyParser2Printout(tlvValOp, refobjTLVAttr, o_tagTxnInfo)

        '2. Printout Detail Info
        o_tagTxnInfo.AddIndent()
        nIdx = 0

        ClassTransaction.TAG_TXN_PARSE_RESPONSE_INFO.CleanupTransactionList(o_tagTxnInfo.m_dictCVMList)

        With refobjTLVAttr
            '3. Parse CVM List, Ref EMV Co V4.3, 10.5, p116.
            '    The CVM List (tag '8E') is a composite data object consisting of the following:
            '3.1. An amount field (4 bytes, binary format), referred to as ‘X’ in Table 40: CVM Condition Codes. ‘X’ is expressed in the Application Currency Code with implicit decimal point. For example, 123 (hexadecimal '7B') represents £1.23 when the currency code is '826'.
            o_tagTxnInfo.m_CVMList_u32Amount1 = ((CType(.m_arrayTLVal(nIdx), UInt32) << 24) And &HFF000000) + ((CType(.m_arrayTLVal(nIdx + 1), UInt32) << 16) And &HFF0000) + ((CType(.m_arrayTLVal(nIdx + 2), UInt32) << 8) And &HFF00) + (CType(.m_arrayTLVal(nIdx + 3), UInt32) And &HFF)
            nIdx = nIdx + 4
            '3.2. A second amount field (4 bytes, binary format), referred to as ‘Y’ in Table 40. ‘Y’ is expressed in Application Currency Code with implicit decimal point. For example, 123 (hexadecimal '7B') represents £1.23 when the currency code is '826'.
            o_tagTxnInfo.m_CVMList_u32Amount2 = ((CType(.m_arrayTLVal(nIdx), UInt32) << 24) And &HFF000000) + ((CType(.m_arrayTLVal(nIdx + 1), UInt32) << 16) And &HFF0000) + ((CType(.m_arrayTLVal(nIdx + 2), UInt32) << 8) And &HFF00) + (CType(.m_arrayTLVal(nIdx + 3), UInt32) And &HFF)
            nIdx = nIdx + 4

            '
            While (nIdx < .m_nLen)
                '3.3. A variable-length list of two-byte data elements called Cardholder Verification Rules (CV Rules). Each CV Rule describes a CVM and the conditions under which that CVM should be applied (see Annex C3).
                byteCVMCode = .m_arrayTLVal(nIdx)
                byteCVMCoditionCodes = .m_arrayTLVal(nIdx + 1)
                nIdx = nIdx + 2
                o_tagTxnInfo.m_dictCVMList.Add(New ClassTransaction.TAG_CVM_LIST_DATA_ELEMENT With {.m_byteCVMCode = byteCVMCode, .m_byteCVMCoditionCodes = byteCVMCoditionCodes})

            End While
        End With


        With o_tagTxnInfo
            '
            strMsg = tagTLV.s_strPrintoutTransactionMsgDelimiter + "B1B4 : Amount 1"
            PktParser2Printout_Common_FormatLine_GetDotsString(strMsg, nIndent, strTmp2)
            strTmp = CType(.m_CVMList_u32Amount1, Double) / 100
            .Add2PrintoutAbs(strMsg + strTmp2 + " = " + strTmp, nIndent)
            '
            strMsg = tagTLV.s_strPrintoutTransactionMsgDelimiter + "B5B8 : Amount 2"
            PktParser2Printout_Common_FormatLine_GetDotsString(strMsg, nIndent, strTmp2)
            strTmp = CType(.m_CVMList_u32Amount2, Double) / 100
            .Add2PrintoutAbs(strMsg + strTmp2 + " = " + strTmp, nIndent)
            '
            '
            strMsg = tagTLV.s_strPrintoutTransactionMsgDelimiter + "2 bytes per CVM List Entry"
            PktParser2Printout_Common_FormatLine_GetDotsString(strMsg, nIndent, strTmp2)
            .Add2PrintoutAbs(strMsg, nIndent)
            '
            nIdxMax = .m_dictCVMList.Count - 1
            For nIdx = 0 To nIdxMax
                byteCVMCode = .m_dictCVMList.Item(nIdx).m_byteCVMCode
                byteCVMCoditionCodes = .m_dictCVMList.Item(nIdx).m_byteCVMCoditionCodes
                '--[ CVM Code, b7 ]--
                strMsg = tagTLV.s_strPrintoutTransactionMsgDelimiter + "(Entry " & nIdx & ") B1b7   : CVM Code 1"
                PktParser2Printout_Common_FormatLine_GetDotsString(strMsg, nIndent, strTmp2)
                byteVal = byteCVMCode And &H40
                If (byteVal = &H40) Then
                    strTmp = "Apply succeeding CV Rule if this CVM is unsuccessful"
                Else
                    strTmp = "Fail cardholder verification if this CVM is unsuccessful"
                End If
                .Add2PrintoutAbs(strMsg + strTmp2 + " = " + String.Format("{0,2:X2} ( ", (byteVal >> 6) And &H1) + strTmp + " )", nIndent)

                '--[ CVM Code, b6~b1 ]--
                strMsg = tagTLV.s_strPrintoutTransactionMsgDelimiter + "(Entry " & nIdx & ") B1b6b1 : CVM Code 2"
                PktParser2Printout_Common_FormatLine_GetDotsString(strMsg, nIndent, strTmp2)
                byteVal = byteCVMCode And &H3F
                If m_dictPktParser2Printout_TxnResponse_8E_CVMList_CVM_Code_b6_b1.ContainsKey(byteVal) Then
                    strTmp = m_dictPktParser2Printout_TxnResponse_8E_CVMList_CVM_Code_b6_b1.Item(byteVal)
                Else
                    If (byteVal And &H20) = &H0 Then
                        strTmp = "Values in the range 000110-011101 reserved for future use by this specification"
                    Else
                        If (byteVal And &H10) = &H0 Then
                            strTmp = "Values in the range 100000-101111 reserved for use by the individual payment systems"
                        Else
                            strTmp = "Values in the range 110000-111110 reserved for use by the issuer"
                        End If
                    End If
                End If
                .Add2PrintoutAbs(strMsg + strTmp2 + " = " + String.Format("{0,2:X2} ( ", byteVal) + strTmp + " )", nIndent)


                ' CVM Condition
                '.Add2Printout("CVM Condition Code:", nIndent)
                strMsg = tagTLV.s_strPrintoutTransactionMsgDelimiter + "(Entry " & nIdx & ") B2     : CVM Condition Code"
                PktParser2Printout_Common_FormatLine_GetDotsString(strMsg, nIndent, strTmp2)
                If m_dictPktParser2Printout_TxnResponse_9F34_CardholderVerificationMethod_Result_Condition_Byte2.ContainsKey(byteCVMCoditionCodes) Then
                    strTmp = m_dictPktParser2Printout_TxnResponse_9F34_CardholderVerificationMethod_Result_Condition_Byte2.Item(byteCVMCoditionCodes)
                Else
                    If (&H9 < byteCVMCoditionCodes) And (&H80 > byteCVMCoditionCodes) Then
                        strTmp = "RFU"
                    Else
                        strTmp = "Reserved for use by individual payment systems"
                    End If
                End If
                .Add2PrintoutAbs(strMsg + strTmp2 + " = " + String.Format("{0,2:X2} ( ", byteCVMCoditionCodes) + strTmp + " )", nIndent)
            Next
        End With
        '
    End Sub
    '
    '===================================[ 90 ]====================================
    Shared Sub PktParser2Printout_90_IssuerPublicKeyCertificate(ByVal tlvValOp As tagTLVNewOp, ByRef refobjTLVAttr As tagTLV, ByRef o_tagTxnInfo As ClassTransaction.TAG_TXN_PARSE_RESPONSE_INFO)
        PktParser2Printout_Common_Hex2Ascii(tlvValOp, refobjTLVAttr, o_tagTxnInfo)
    End Sub
    '
    '===================================[ 91 ]====================================
    Shared Sub PktParser2Printout_91_IssuerAuthenticationData(ByVal tlvValOp As tagTLVNewOp, ByRef refobjTLVAttr As tagTLV, ByRef o_tagTxnInfo As ClassTransaction.TAG_TXN_PARSE_RESPONSE_INFO)
        Dim strTmp As String = Nothing

        ' 1. Printout Tag Value
        tlvValOp.ptrFuncCbDummyParser2Printout(tlvValOp, refobjTLVAttr, o_tagTxnInfo)

        'Amex Expresspay V2.0.1
        '1. First 8 bytes = ARPC
        ClassTableListMaster.ConvertFromArrayByte2String(refobjTLVAttr.m_arrayTLVal, 0, 8, strTmp)
        o_tagTxnInfo.Add2PrintoutAbs("ARPC = " + strTmp, 6 + tagTLV.s_strPrintoutTransactionMsgDelimiter.Length)

        '2.Last 2 bytres = Authorization Response Code
        ClassTableListMaster.ConvertFromArrayByte2String(refobjTLVAttr.m_arrayTLVal, refobjTLVAttr.m_nLen - 2, 2, strTmp)
        o_tagTxnInfo.Add2PrintoutAbs("Authorization Response Code = " + strTmp, 6 + tagTLV.s_strPrintoutTransactionMsgDelimiter.Length)
    End Sub
    '
    '===================================[ 92 ]====================================
    Shared Sub PktParser2Printout_92_IssuerPublicKeyRemainder(ByVal tlvValOp As tagTLVNewOp, ByRef refobjTLVAttr As tagTLV, ByRef o_tagTxnInfo As ClassTransaction.TAG_TXN_PARSE_RESPONSE_INFO)
        PktParser2Printout_Common_Hex2Ascii(tlvValOp, refobjTLVAttr, o_tagTxnInfo)
    End Sub
    '
    '===================================[ 94 ]====================================
    Shared Sub PktParser2Printout_94_ApplicationFileLocator(ByVal tlvValOp As tagTLVNewOp, ByRef refobjTLVAttr As tagTLV, ByRef o_tagTxnInfo As ClassTransaction.TAG_TXN_PARSE_RESPONSE_INFO)
        Dim byteVal As Byte
        Dim arrayByteAFLEntry As Byte()
        Dim nIdx, nIdxMax, nAFLEntryLen As Byte
        Dim listAFLEntries As List(Of Byte()) = New List(Of Byte())
        Dim strAFLEntryData As String = ""
        ' 1. Printout Tag Value
        tlvValOp.ptrFuncCbDummyParser2Printout(tlvValOp, refobjTLVAttr, o_tagTxnInfo)
        'Sourced from EMV CL V2.3 , Book C-2. A.1.15 Application File Locator
        'Tag:    '94'
        'Length: var.; multiple of 4 between 4 and 252
        'Description: Indicates the location (SFI range of records) of the Application
        'Elementary Files associated with a particular AID, and read by
        'the Kernel during a transaction.
        'The Application File Locator is a list of entries of 4 bytes each.
        'Each entry codes an SFI and a range of records as follows:
        '1)The five most significant bits of the first byte indicate the
        'SFI.
        '2)The second byte indicates the first (or only) record number
        'to be read for that SFI.
        '3) The third byte indicates the last record number to be read
        'for that SFI. When the third byte is greater than the second
        'byte, all the records ranging from the record number in the
        'second byte to and including the record number in the third
        'byte must be read for that SFI. When the third byte is equal
        'to the second byte, only the record number coded in the
        'second byte must be read for that SFI.
        '4) The fourth byte indicates the number of records involved in
        'offline data authentication starting with the record number
        'coded in the second byte. The fourth byte may range from
        'zero to the value of the third byte less the value of the
        'second byte plus 1.

        'Set AFL Entry Length in byte
        nAFLEntryLen = 4
        ' 2.Validation Length, it must 4-byte times in byte size
        If ((refobjTLVAttr.m_nLen Mod nAFLEntryLen) <> 0) Then
            Throw New Exception("[Err] PktParser2Printout_95_TVR() = data size is not 4-byte times in size, data len =" & refobjTLVAttr.m_nLen & ", Mod 4 = " & (refobjTLVAttr.m_nLen Mod nAFLEntryLen))
        End If

        '3.Break down data into 4-byte entries list
        With refobjTLVAttr
            nIdxMax = .m_nLen
            nIdx = 0
            While (nIdx < nIdxMax)
                arrayByteAFLEntry = New Byte(nAFLEntryLen - 1) {}
                Array.Copy(.m_arrayTLVal, nIdx, arrayByteAFLEntry, 0, nAFLEntryLen)
                listAFLEntries.Add(arrayByteAFLEntry)
                nIdx = nIdx + nAFLEntryLen
            End While
        End With

        '4. Printout Detail Info
        o_tagTxnInfo.AddIndent()
        '
        nIdx = 0
        For Each arrayByteAFLEntry In listAFLEntries
            'Print out Index at first
            ClassTableListMaster.ConvertFromArrayByte2String(arrayByteAFLEntry, strAFLEntryData)
            o_tagTxnInfo.Add2Printout("Entry " & nIdx & ", Data = " + strAFLEntryData)
            ' Detail Info
            o_tagTxnInfo.AddIndent()
            'B1b8~b4 = SFI
            byteVal = arrayByteAFLEntry(0) >> 3
            o_tagTxnInfo.Add2Printout(String.Format("{0,2:X2}", byteVal) + " = SFI")
            'B2 = First Record
            o_tagTxnInfo.Add2Printout(arrayByteAFLEntry(1) & " = First Record ")
            'B3 = Last Record
            o_tagTxnInfo.Add2Printout(arrayByteAFLEntry(2) & " = Last Record ")
            'B4 = Num of Record involved in Offline Authentication
            o_tagTxnInfo.Add2Printout(arrayByteAFLEntry(3) & " = Num of Record involved in Offline Authentication ")
            o_tagTxnInfo.DecIndent()
            '
            'Finally Free Memory Resource
            Erase arrayByteAFLEntry
        Next
        '
        o_tagTxnInfo.DecIndent()
        '
        listAFLEntries.Clear()
    End Sub
    '
    '===================================[ 95 ]====================================
    Public Shared m_dictPktParser2Printout_TxnResponse_95_TVR_Byte1 As Dictionary(Of Byte, tagTxnResponseBitValResolveInfo) = New Dictionary(Of Byte, tagTxnResponseBitValResolveInfo) From {
     {&H8, New tagTxnResponseBitValResolveInfo(&H80, "Offline data authentication was not performed")},
     {&H7, New tagTxnResponseBitValResolveInfo(&H40, "SDA Failed")},
     {&H6, New tagTxnResponseBitValResolveInfo(&H20, "ICC data missing")},
     {&H5, New tagTxnResponseBitValResolveInfo(&H10, "Card appears on terminal exception file")},
     {&H4, New tagTxnResponseBitValResolveInfo(&H8, "DDA failed")},
     {&H3, New tagTxnResponseBitValResolveInfo(&H4, "CDA failed")}
     }
    Public Shared m_dictPktParser2Printout_TxnResponse_95_TVR_Byte2 As Dictionary(Of Byte, tagTxnResponseBitValResolveInfo) = New Dictionary(Of Byte, tagTxnResponseBitValResolveInfo) From {
 {&H8, New tagTxnResponseBitValResolveInfo(&H80, "ICC and terminal have different application versions")},
 {&H7, New tagTxnResponseBitValResolveInfo(&H40, "Expired application")},
 {&H6, New tagTxnResponseBitValResolveInfo(&H20, "Application not yet effective")},
 {&H5, New tagTxnResponseBitValResolveInfo(&H10, "Requested service not allowed for card product")},
 {&H4, New tagTxnResponseBitValResolveInfo(&H8, "New card")}
 }
    Public Shared m_dictPktParser2Printout_TxnResponse_95_TVR_Byte3 As Dictionary(Of Byte, tagTxnResponseBitValResolveInfo) = New Dictionary(Of Byte, tagTxnResponseBitValResolveInfo) From {
{&H8, New tagTxnResponseBitValResolveInfo(&H80, "Cardholder verification was not successful")},
{&H7, New tagTxnResponseBitValResolveInfo(&H40, "Unrecognised CVM")},
{&H6, New tagTxnResponseBitValResolveInfo(&H20, "PIN Try Limit exceeded")},
{&H5, New tagTxnResponseBitValResolveInfo(&H10, "PIN entry required and PIN pad not present or not working")},
{&H4, New tagTxnResponseBitValResolveInfo(&H8, "PIN entry required, PIN pad present, but PIN was not entered")},
{&H3, New tagTxnResponseBitValResolveInfo(&H4, "Online PIN entered")}
}
    Public Shared m_dictPktParser2Printout_TxnResponse_95_TVR_Byte4 As Dictionary(Of Byte, tagTxnResponseBitValResolveInfo) = New Dictionary(Of Byte, tagTxnResponseBitValResolveInfo) From {
{&H8, New tagTxnResponseBitValResolveInfo(&H80, "Transaction exceeds floor limit")},
{&H7, New tagTxnResponseBitValResolveInfo(&H40, "Lower consecutive offline limit exceeded")},
{&H6, New tagTxnResponseBitValResolveInfo(&H20, "Upper consecutive offline limit exceeded")},
{&H5, New tagTxnResponseBitValResolveInfo(&H10, "Transaction selected randomly for online processing")},
{&H4, New tagTxnResponseBitValResolveInfo(&H8, "Merchant forced transaction online")}
}
    Public Shared m_dictPktParser2Printout_TxnResponse_95_TVR_Byte5 As Dictionary(Of Byte, tagTxnResponseBitValResolveInfo) = New Dictionary(Of Byte, tagTxnResponseBitValResolveInfo) From {
{&H8, New tagTxnResponseBitValResolveInfo(&H80, "Default TDOL used")},
{&H7, New tagTxnResponseBitValResolveInfo(&H40, "Issuer authentication failed")},
{&H6, New tagTxnResponseBitValResolveInfo(&H20, " Script processing failed before final GENERATE AC")},
{&H5, New tagTxnResponseBitValResolveInfo(&H10, "Script processing failed after final GENERATE AC")}
}
    Shared Sub PktParser2Printout_95_TVR(ByVal tlvValOp As tagTLVNewOp, ByRef refobjTLVAttr As tagTLV, ByRef o_tagTxnInfo As ClassTransaction.TAG_TXN_PARSE_RESPONSE_INFO)
        Dim dictParseResolution95TVR As Dictionary(Of Byte, Dictionary(Of Byte, tagTxnResponseBitValResolveInfo)) = New Dictionary(Of Byte, Dictionary(Of Byte, tagTxnResponseBitValResolveInfo)) From {
            {&H0, m_dictPktParser2Printout_TxnResponse_95_TVR_Byte1},
            {&H1, m_dictPktParser2Printout_TxnResponse_95_TVR_Byte2},
            {&H2, m_dictPktParser2Printout_TxnResponse_95_TVR_Byte3},
            {&H3, m_dictPktParser2Printout_TxnResponse_95_TVR_Byte4},
            {&H4, m_dictPktParser2Printout_TxnResponse_95_TVR_Byte5}
            }

        PktParser2Printout_Common_ByteBitOperationMultipleLists(tlvValOp, refobjTLVAttr, o_tagTxnInfo, dictParseResolution95TVR)
    End Sub

    '===================================[ 9A ]====================================
    Shared Sub PktParser2Printout_9A_TransactionDate(ByVal tlvValOp As tagTLVNewOp, ByRef refobjTLVAttr As tagTLV, ByRef o_tagTxnInfo As ClassTransaction.TAG_TXN_PARSE_RESPONSE_INFO)
        Dim strMsg = "", strTmp = "", strTmp2 As String = ""
        Dim nIndent As Integer

        ' 1. Printout Tag Value
        ' tlvValOp.ptrFuncCbDummyParser2Printout(tlvValOp, refobjTLVAttr, o_tagTxnInfo)

        '2. Printout Detail Info
        'o_tagTxnInfo.AddIndent()

        ClassTableListMaster.ConvertFromArrayByte2String(refobjTLVAttr.m_arrayTLVal, strTmp)
        strMsg = tlvValOp.m_strTag + tagTLV.s_strPrintoutTransactionMsgDelimiter + tlvValOp.m_strComment
        PktParser2Printout_Common_FormatLine_GetDotsString(strMsg, nIndent, strTmp2)
        o_tagTxnInfo.Add2PrintoutAbs(strMsg + strTmp2 + " = " + strTmp + " (YYMMDD)", nIndent)
        '
        'o_tagTxnInfo.DecIndent()
    End Sub
    '
    Shared Sub PktParser2Printout_Common_Hex2Ascii(ByVal tlvValOp As tagTLVNewOp, ByRef refobjTLVAttr As tagTLV, ByRef o_tagTxnInfo As ClassTransaction.TAG_TXN_PARSE_RESPONSE_INFO)
        Dim strMsg = "", strTmp = "", strTmp2 As String = ""
        Dim nIndent As Integer

        ' 1. Printout Tag Value
        'tlvValOp.ptrFuncCbDummyParser2Printout(tlvValOp, refobjTLVAttr, o_tagTxnInfo)

        '2. Printout Detail Info
        With refobjTLVAttr
            ClassTableListMaster.ConvertFromHexToAscii(.m_arrayTLVal, 0, .m_nLen, strTmp)
        End With
        strMsg = tlvValOp.m_strTag + tagTLV.s_strPrintoutTransactionMsgDelimiter + tlvValOp.m_strComment
        PktParser2Printout_Common_FormatLine_GetDotsString(strMsg, nIndent, strTmp2)
        o_tagTxnInfo.Add2PrintoutAbs(strMsg + strTmp2 + " = " + strTmp, nIndent)
    End Sub

    Shared Sub PktParser2Printout_Common_Hex2Ascii_Or_BinaryValue(ByVal arrayByteData As Byte(), ByVal strTagPrefix As String, ByVal strComment As String, ByRef o_tagTxnInfo As ClassTransaction.TAG_TXN_PARSE_RESPONSE_INFO, Optional ByVal bStringFormtType2Hex As Boolean = True)
        PktParser2Printout_Common_Hex2Ascii_Or_BinaryValue(arrayByteData, arrayByteData.Length, strTagPrefix, strComment, o_tagTxnInfo, bStringFormtType2Hex)
    End Sub
    '
    Shared Sub PktParser2Printout_Common_Hex2Ascii_Or_BinaryValue(ByVal arrayByteData As Byte(), ByVal nDataLen As Integer, ByVal strTagPrefix As String, ByVal strComment As String, ByRef o_tagTxnInfo As ClassTransaction.TAG_TXN_PARSE_RESPONSE_INFO, Optional ByVal bStringFormtType2Hex As Boolean = True)
        Dim strMsg = "", strTmp = "", strTmp2 As String = ""
        Dim nIndent As Integer = 3

        If (bStringFormtType2Hex) Then
            ClassTableListMaster.ConvertFromHexToAscii(arrayByteData, 0, nDataLen, strTmp)
        Else
            ClassTableListMaster.ConvertFromArrayByte2String(arrayByteData, 0, nDataLen, strTmp)
        End If
        strMsg = strTagPrefix + tagTLV.s_strPrintoutTransactionMsgDelimiter + strComment
        PktParser2Printout_Common_FormatLine_GetDotsString(strMsg, nIndent, strTmp2)
        o_tagTxnInfo.Add2PrintoutAbs(strMsg + strTmp2 + " = " + strTmp, nIndent)
    End Sub

    '
    '===================================[ 5F20 ]====================================
    Shared Sub PktParser2Printout_5F20_CardholderName(ByVal tlvValOp As tagTLVNewOp, ByRef refobjTLVAttr As tagTLV, ByRef o_tagTxnInfo As ClassTransaction.TAG_TXN_PARSE_RESPONSE_INFO)
        PktParser2Printout_Common_Hex2Ascii(tlvValOp, refobjTLVAttr, o_tagTxnInfo)
    End Sub
    '
    '===================================[ 5F24 ]====================================
    Shared Sub PktParser2Printout_5F24_ApplicationExpirationDate(ByVal tlvValOp As tagTLVNewOp, ByRef refobjTLVAttr As tagTLV, ByRef o_tagTxnInfo As ClassTransaction.TAG_TXN_PARSE_RESPONSE_INFO)
        PktParser2Printout_9A_TransactionDate(tlvValOp, refobjTLVAttr, o_tagTxnInfo)
    End Sub
    '    '
    '===================================[ 5F25 ]====================================
    Shared Sub PktParser2Printout_5F25_ApplicationEffectiveDate(ByVal tlvValOp As tagTLVNewOp, ByRef refobjTLVAttr As tagTLV, ByRef o_tagTxnInfo As ClassTransaction.TAG_TXN_PARSE_RESPONSE_INFO)
        PktParser2Printout_9A_TransactionDate(tlvValOp, refobjTLVAttr, o_tagTxnInfo)
    End Sub
    '

    '===================================[ 9F46 ]====================================
    Shared Sub PktParser2Printout_9F46_MC3_ICCPublicKeyCertificate_OR_Amex_ApplicationCDAPublicKeyCertificate(ByVal tlvValOp As tagTLVNewOp, ByRef refobjTLVAttr As tagTLV, ByRef o_tagTxnInfo As ClassTransaction.TAG_TXN_PARSE_RESPONSE_INFO)

        tlvValOp.m_strComment = "Master:ICC Public Key Certificate; Amex:Application CDA Public Key Certificate"
        ' 1. Printout Tag Value
        tlvValOp.ptrFuncCbDummyParser2Printout(tlvValOp, refobjTLVAttr, o_tagTxnInfo)

    End Sub


    '===================================[ 9F47 ]====================================
    Shared Sub PktParser2Printout_9F47_MC3_ICCPublicKeyExponent_OR_Amex_ApplicationCDAPublicKeyExponent(ByVal tlvValOp As tagTLVNewOp, ByRef refobjTLVAttr As tagTLV, ByRef o_tagTxnInfo As ClassTransaction.TAG_TXN_PARSE_RESPONSE_INFO)
        tlvValOp.m_strComment = "Master:ICC Public Key Exponent ; Amex:Application CDA Public Key Exponent "
        ' 1. Printout Tag Value
        tlvValOp.ptrFuncCbDummyParser2Printout(tlvValOp, refobjTLVAttr, o_tagTxnInfo)

    End Sub


    '===================================[ 9F48 ]====================================
    Shared Sub PktParser2Printout_9F48_MC3_ICCPublicKeyRemainder_OR_Amex_ApplicationCDAPublicKeyRemainder(ByVal tlvValOp As tagTLVNewOp, ByRef refobjTLVAttr As tagTLV, ByRef o_tagTxnInfo As ClassTransaction.TAG_TXN_PARSE_RESPONSE_INFO)

        tlvValOp.m_strComment = "Master: ICC Public Key Remainder; Amex: Application CDA Public Key Remainder"
        ' 1. Printout Tag Value
        tlvValOp.ptrFuncCbDummyParser2Printout(tlvValOp, refobjTLVAttr, o_tagTxnInfo)

    End Sub

    Public Shared Sub PktParser2Printout_9F4A_StaticDataAuthenticationTagList_MasterCard_Or_Amex(ByVal tlvValOp As tagTLVNewOp, ByRef refobjTLVAttr As tagTLV, ByRef o_tagTxnInfo As ClassTransaction.TAG_TXN_PARSE_RESPONSE_INFO)
        Dim dictTLV As List(Of tagTLV) = New List(Of tagTLV)
        PktParser2Printout_Common_SubTagsParser(tlvValOp, refobjTLVAttr, o_tagTxnInfo, dictTLV)
        ClassTableListMaster.CleanupConfigurationsTLVList(dictTLV)
    End Sub

    '===================================[ DF2D ]====================================
    Shared Sub PktParser2Printout_DF2D_LanguagePreference(ByVal tlvValOp As tagTLVNewOp, ByRef refobjTLVAttr As tagTLV, ByRef o_tagTxnInfo As ClassTransaction.TAG_TXN_PARSE_RESPONSE_INFO)
        PktParser2Printout_Common_Hex2Ascii(tlvValOp, refobjTLVAttr, o_tagTxnInfo)
    End Sub
    '
    '===================================[ 9F21 ]====================================
    Shared Sub PktParser2Printout_9F21_TransactionTime(ByVal tlvValOp As tagTLVNewOp, ByRef refobjTLVAttr As tagTLV, ByRef o_tagTxnInfo As ClassTransaction.TAG_TXN_PARSE_RESPONSE_INFO)
        Dim strMsg = "", strTmp = "", strTmp2 As String = ""
        Dim nIndent As Integer

        ClassTableListMaster.ConvertFromArrayByte2String(refobjTLVAttr.m_arrayTLVal, strTmp)
        strMsg = tlvValOp.m_strTag + tagTLV.s_strPrintoutTransactionMsgDelimiter + tlvValOp.m_strComment
        PktParser2Printout_Common_FormatLine_GetDotsString(strMsg, nIndent, strTmp2)
        o_tagTxnInfo.Add2PrintoutAbs(strMsg + strTmp2 + " = " + strTmp + " ( HHMMSS )", nIndent)
    End Sub
    '===================================[ 9C ]====================================
    Public Shared m_dictPktParser2Printout_9C_TransactionType As Dictionary(Of Byte, String) = New Dictionary(Of Byte, String) From {
{&H0, "Purchase"},
{&H20, "Refund"}
}
    Shared Sub PktParser2Printout_9C_TransactionType(ByVal tlvValOp As tagTLVNewOp, ByRef refobjTLVAttr As tagTLV, ByRef o_tagTxnInfo As ClassTransaction.TAG_TXN_PARSE_RESPONSE_INFO)
        ' 00 = Purchase
        ' 20 = Refund
        Dim byteVal As Byte = refobjTLVAttr.m_arrayTLVal(0)
        'Dim strMsg As String
        ' 1. Printout Tag Value
        'tlvValOp.ptrFuncCbDummyParser2Printout(tlvValOp, refobjTLVAttr, o_tagTxnInfo)

        '2. Printout Detail Info
        PktParser2Printout_Common_ByteValueOperation_TagDisplay(tlvValOp, refobjTLVAttr, o_tagTxnInfo, refobjTLVAttr.m_arrayTLVal(0), m_dictPktParser2Printout_9C_TransactionType, "B1 : Transaction Type", " Unknown Transaction")

    End Sub
    '
    '===================================[ A5 ]====================================
    Shared Sub PktParser2Printout_A5_FileControlInformationProprietaryTemplate(ByVal tlvValOp As tagTLVNewOp, ByRef refobjTLVAttr As tagTLV, ByRef o_tagTxnInfo As ClassTransaction.TAG_TXN_PARSE_RESPONSE_INFO)
        PktParser2Printout_Common_SubTagsParser(tlvValOp, refobjTLVAttr, o_tagTxnInfo, o_tagTxnInfo.m_dictTlvsTxnResult_A5_FileControlInformationProprietaryTemplate)
    End Sub
    '
    '===================================[ 9F11 ]====================================
    Shared Sub PktParser2Printout_9F08_ApplicationVersionNumber_Card_OR_Amex_ApplicationVersionNumber(ByVal tlvValOp As tagTLVNewOp, ByRef refobjTLVAttr As tagTLV, ByRef o_tagTxnInfo As ClassTransaction.TAG_TXN_PARSE_RESPONSE_INFO)

        Select Case (Form01_Main.GetInstance().ModeSelectionGet())
            'Case ClassUIDriver.EnumUIDriverEventEnableDisable.UI_ENABLE_COMM_OPEN_PART_NUM_SELECTED_MODE_TEST
            '    tlvValOp.m_strComment = "Application Version Number (Card)"
            Case Else
                '            Case ClassUIDriver.EnumUIDriverEventEnableDisable.UI_ENABLE_COMM_OPEN_PAYMENT_SELECT_AMERICAN_EXPRESS
                tlvValOp.m_strComment = "Application Version Number"
                '               tlvValOp.m_strComment = tagTLV.s_strPrintoutTransactionUnknownTag
        End Select

        ' 1. Printout Tag Value
        tlvValOp.ptrFuncCbDummyParser2Printout(tlvValOp, refobjTLVAttr, o_tagTxnInfo)

    End Sub
    '===================================[ 9F11 ]====================================
    Public Shared m_dictPktParser2Printout_TxnResponse_9F07_ApplicationUsageControl_Byte1 As Dictionary(Of Byte, tagTxnResponseBitValResolveInfo) = New Dictionary(Of Byte, tagTxnResponseBitValResolveInfo) From {
{&H8, New tagTxnResponseBitValResolveInfo(&H80, "Valid for domestic cash transactions")},
{&H7, New tagTxnResponseBitValResolveInfo(&H40, "Valid for international cash transactions")},
{&H6, New tagTxnResponseBitValResolveInfo(&H20, "Valid for domestic goods")},
{&H5, New tagTxnResponseBitValResolveInfo(&H10, "Valid for international goods")},
{&H4, New tagTxnResponseBitValResolveInfo(&H8, "Valid for domestic services")},
{&H3, New tagTxnResponseBitValResolveInfo(&H4, "Valid for international services")},
{&H2, New tagTxnResponseBitValResolveInfo(&H2, "Valid at ATMs")},
{&H1, New tagTxnResponseBitValResolveInfo(&H1, "Valid at terminals other than ATMs")}
}
    Public Shared m_dictPktParser2Printout_TxnResponse_9F07_ApplicationUsageControl_Byte2 As Dictionary(Of Byte, tagTxnResponseBitValResolveInfo) = New Dictionary(Of Byte, tagTxnResponseBitValResolveInfo) From {
{&H8, New tagTxnResponseBitValResolveInfo(&H80, "Domestic cashback allowed")},
{&H7, New tagTxnResponseBitValResolveInfo(&H40, "International cashback allowed")}
}

    Shared Sub PktParser2Printout_9F07_ApplicationUsageControl(ByVal tlvValOp As tagTLVNewOp, ByRef refobjTLVAttr As tagTLV, ByRef o_tagTxnInfo As ClassTransaction.TAG_TXN_PARSE_RESPONSE_INFO)
        Dim dictParseResolution_9F07_ApplicationUsageControl As Dictionary(Of Byte, Dictionary(Of Byte, tagTxnResponseBitValResolveInfo)) = New Dictionary(Of Byte, Dictionary(Of Byte, tagTxnResponseBitValResolveInfo)) From {
            {&H0, m_dictPktParser2Printout_TxnResponse_9F07_ApplicationUsageControl_Byte1},
            {&H1, m_dictPktParser2Printout_TxnResponse_9F07_ApplicationUsageControl_Byte2}
            }
        PktParser2Printout_Common_ByteBitOperationMultipleLists(tlvValOp, refobjTLVAttr, o_tagTxnInfo, dictParseResolution_9F07_ApplicationUsageControl)
    End Sub
    '
    '===================================[ 9F0B ]====================================
    Shared Sub PktParser2Printout_9F0B_Amex_CardHolderName_Extended(ByVal tlvValOp As tagTLVNewOp, ByRef refobjTLVAttr As tagTLV, ByRef o_tagTxnInfo As ClassTransaction.TAG_TXN_PARSE_RESPONSE_INFO)
        PktParser2Printout_Common_Hex2Ascii(tlvValOp, refobjTLVAttr, o_tagTxnInfo)
    End Sub

    '
    '===================================[ 9F11 ]====================================
    Public Shared m_dictPktParser2Printout_9F11_IssuerCodeTableIndex As Dictionary(Of Byte, String) = New Dictionary(Of Byte, String) From {
{&H1, "Part 1 of ISO/IEC 8859"},
{&H2, "Part 2 of ISO/IEC 8859"},
{&H3, "Part 3 of ISO/IEC 8859"},
{&H4, "Part 4 of ISO/IEC 8859"},
{&H5, "Part 5 of ISO/IEC 8859"},
{&H6, "Part 6 of ISO/IEC 8859"},
{&H7, "Part 7 of ISO/IEC 8859"},
{&H8, "Part 8 of ISO/IEC 8859"},
{&H9, "Part 9 of ISO/IEC 8859"},
{&HA, "Part 10 of ISO/IEC 8859"}
}
    Shared Sub PktParser2Printout_9F11_IssuerCodeTableIndex(ByVal tlvValOp As tagTLVNewOp, ByRef refobjTLVAttr As tagTLV, ByRef o_tagTxnInfo As ClassTransaction.TAG_TXN_PARSE_RESPONSE_INFO)
        'Dim strMsg As String
        'Dim byteVal As Byte

        ' 1. Printout Tag Value
        'tlvValOp.ptrFuncCbDummyParser2Printout(tlvValOp, refobjTLVAttr, o_tagTxnInfo)

        '2. Printout Detail Info
        PktParser2Printout_Common_ByteValueOperation_TagDisplay(tlvValOp, refobjTLVAttr, o_tagTxnInfo, refobjTLVAttr.m_arrayTLVal(0), m_dictPktParser2Printout_9F11_IssuerCodeTableIndex, "B1 : Issuer Code Table Index", " Unknown Part ?(Valid from Part 01~10) of ISO/IEC 8859")
    End Sub

    '===================================[ 9F12 ]====================================
    Shared Sub PktParser2Printout_9F12_ApplicationPreferredName(ByVal tlvValOp As tagTLVNewOp, ByRef refobjTLVAttr As tagTLV, ByRef o_tagTxnInfo As ClassTransaction.TAG_TXN_PARSE_RESPONSE_INFO)
        PktParser2Printout_Common_Hex2Ascii(tlvValOp, refobjTLVAttr, o_tagTxnInfo)
    End Sub
    '===================================[ 9F16 ]====================================
    Shared Sub PktParser2Printout_9F16_MerchantIdentifier(ByVal tlvValOp As tagTLVNewOp, ByRef refobjTLVAttr As tagTLV, ByRef o_tagTxnInfo As ClassTransaction.TAG_TXN_PARSE_RESPONSE_INFO)
        PktParser2Printout_Common_Hex2Ascii(tlvValOp, refobjTLVAttr, o_tagTxnInfo)
    End Sub
    '===================================[ 9F1C ]====================================
    Shared Sub PktParser2Printout_9F1C_TerminalIdentification(ByVal tlvValOp As tagTLVNewOp, ByRef refobjTLVAttr As tagTLV, ByRef o_tagTxnInfo As ClassTransaction.TAG_TXN_PARSE_RESPONSE_INFO)
        PktParser2Printout_Common_Hex2Ascii(tlvValOp, refobjTLVAttr, o_tagTxnInfo)
    End Sub

    '===================================[ 9F1E ]====================================
    Shared Sub PktParser2Printout_9F1E_InterfaceDeviceSerialNumber(ByVal tlvValOp As tagTLVNewOp, ByRef refobjTLVAttr As tagTLV, ByRef o_tagTxnInfo As ClassTransaction.TAG_TXN_PARSE_RESPONSE_INFO)
        PktParser2Printout_Common_Hex2Ascii(tlvValOp, refobjTLVAttr, o_tagTxnInfo)
    End Sub

    '===================================[ 9F1F ]====================================
    Shared Sub PktParser2Printout_9F20_Track2DiscretionaryData(ByVal tlvValOp As tagTLVNewOp, ByRef refobjTLVAttr As tagTLV, ByRef o_tagTxnInfo As ClassTransaction.TAG_TXN_PARSE_RESPONSE_INFO)
        PktParser2Printout_Common_Hex2Ascii(tlvValOp, refobjTLVAttr, o_tagTxnInfo)

        'Check SS, ES, LRC, For DPAS case, if the Track data has no SS, ES, and LRC from Reader, the Lab Tool must generate them for the operator / SQA
    End Sub


    '===================================[ 9F20 ]====================================
    Shared Sub PktParser2Printout_9F1F_Track1DiscretionaryData(ByVal tlvValOp As tagTLVNewOp, ByRef refobjTLVAttr As tagTLV, ByRef o_tagTxnInfo As ClassTransaction.TAG_TXN_PARSE_RESPONSE_INFO)
        PktParser2Printout_Common_Hex2Ascii(tlvValOp, refobjTLVAttr, o_tagTxnInfo)

        'Check SS, ES, LRC, For DPAS case, if the Track data has no SS, ES, and LRC from Reader, the Lab Tool must generate them for the operator / SQA
    End Sub

    '===================================[ 9F27 ]====================================
    Public Shared m_dictPktParser2Printout_TxnResponse_9F27_MC3_B1b8_b7_Or_Amex_B1b8_b7_CryptogramInformationData As Dictionary(Of Byte, String) = New Dictionary(Of Byte, String) From {
{&H0, "AAC"}, {&H1, "TC"}, {&H2, "AQRC"}, {&H3, "AAR (not supported)"}
}
    Public Shared m_dictPktParser2Printout_TxnResponse_9F27_MC3_B1b3_b1_CryptogramInformationData_ReasonDeviceCode As Dictionary(Of Byte, String) = New Dictionary(Of Byte, String) From {
{&H0, "No information given"}, {&H1, "Service not allowed"}, {&H2, "PIN Try Limit exceeded"}, {&H3, "Issuer authentication"}
}
    Shared Sub PktParser2Printout_9F27_MC3_CryptogramInformationData(ByVal tlvValOp As tagTLVNewOp, ByRef refobjTLVAttr As tagTLV, ByRef o_tagTxnInfo As ClassTransaction.TAG_TXN_PARSE_RESPONSE_INFO)
        Dim byteVal As Byte = refobjTLVAttr.m_arrayTLVal(0)
        Dim byteVal2 As Byte
        Dim strMsg = "", strTmp = "", strTmp2 As String = ""
        Dim nIndent As Integer = 3
        ' 1. Printout Tag Value
        tlvValOp.ptrFuncCbDummyParser2Printout(tlvValOp, refobjTLVAttr, o_tagTxnInfo)

        '2. Printout Detail Info
        o_tagTxnInfo.AddIndent()

        'B1b8-7, Algorithm Selection, AAC(0); TC(1); ARQC(2)
        PktParser2Printout_Common_ByteValueOperation(tlvValOp, refobjTLVAttr, o_tagTxnInfo, (byteVal >> 6) And &H3, m_dictPktParser2Printout_TxnResponse_9F27_MC3_B1b8_b7_Or_Amex_B1b8_b7_CryptogramInformationData, "B1b8-7 : Algorithm Selection, AAC(0); TC(1); ARQC(2)", "Unknown Selection")

        'B1b6-5, Payment System-specific cryptogram
        byteVal2 = (byteVal >> 4) And &H3
        strMsg = tagTLV.s_strPrintoutTransactionMsgDelimiter + "B1b6-5 : Payment System-specific cryptogram"
        PktParser2Printout_Common_FormatLine_GetDotsString(strMsg, nIndent, strTmp2)
        o_tagTxnInfo.Add2PrintoutAbs(strMsg + strTmp2 + " = " + String.Format(" {0,2:X2}", byteVal2), nIndent)

        'B1b4, Advice
        strMsg = tagTLV.s_strPrintoutTransactionMsgDelimiter + "B1b4 : Advice"
        PktParser2Printout_Common_FormatLine_GetDotsString(strMsg, nIndent, strTmp2)
        If ((byteVal And &H8) = &H8) Then
            strTmp = " 01 ( Advice required )"
        Else
            strTmp = " 00 ( No advice required )"
        End If
        o_tagTxnInfo.Add2PrintoutAbs(strMsg + strTmp2 + " = " + strTmp, nIndent)

        'B1b3-1, Reason/advice code
        PktParser2Printout_Common_ByteValueOperation(tlvValOp, refobjTLVAttr, o_tagTxnInfo, byteVal And &H3, m_dictPktParser2Printout_TxnResponse_9F27_MC3_B1b3_b1_CryptogramInformationData_ReasonDeviceCode, "B1b3-1 : Reason/advice code", "RFU Reason/device code")

        o_tagTxnInfo.DecIndent()
    End Sub
    '
    Public Shared m_dictPktParser2Printout_TxnResponse_9F27_Amex_B1b5_OnlineAdviceMandatory_b4_OfflineAdviceMandatory As Dictionary(Of Byte, tagTxnResponseBitValResolveInfo) = New Dictionary(Of Byte, tagTxnResponseBitValResolveInfo) From {
{&H5, New tagTxnResponseBitValResolveInfo(&H10, "Online advice mandatory")},
{&H4, New tagTxnResponseBitValResolveInfo(&H8, "Offline advice mandatory")}
}
    Public Shared m_dictPktParser2Printout_TxnResponse_9F27_Amex_B1b3_b1_CryptogramInformationData_ReasonDeviceCode As Dictionary(Of Byte, String) = New Dictionary(Of Byte, String) From {
{&H0, "No information given"}, {&H1, "Service not allowed"}
}
    Shared Sub PktParser2Printout_9F27_Amex_CryptogramInformationData(ByVal tlvValOp As tagTLVNewOp, ByRef refobjTLVAttr As tagTLV, ByRef o_tagTxnInfo As ClassTransaction.TAG_TXN_PARSE_RESPONSE_INFO)
        Dim byteVal As Byte = refobjTLVAttr.m_arrayTLVal(0)

        Dim nIndent As Integer = 3
        ' 1. Printout Tag Value
        tlvValOp.ptrFuncCbDummyParser2Printout(tlvValOp, refobjTLVAttr, o_tagTxnInfo)

        '2. Printout Detail Info
        o_tagTxnInfo.AddIndent()

        'B1b8-7, Algorithm Selection, AAC(0); TC(1); ARQC(2); AAR(3, not supported)"
        PktParser2Printout_Common_ByteValueOperation(tlvValOp, refobjTLVAttr, o_tagTxnInfo, (byteVal >> 6) And &H3, m_dictPktParser2Printout_TxnResponse_9F27_MC3_B1b8_b7_Or_Amex_B1b8_b7_CryptogramInformationData, "B1b8-7 : Algorithm Selection, AAC(0); TC(1); ARQC(2); AAR(3, not supported)", "Unknown Selection")

        'B1b5; b4
        PktParser2Printout_Common_ByteBitOperation(tlvValOp, refobjTLVAttr, o_tagTxnInfo, 1, byteVal, m_dictPktParser2Printout_TxnResponse_9F27_Amex_B1b5_OnlineAdviceMandatory_b4_OfflineAdviceMandatory)

        'B1b3-1, Reason/advice code
        PktParser2Printout_Common_ByteValueOperation(tlvValOp, refobjTLVAttr, o_tagTxnInfo, byteVal And &H3, m_dictPktParser2Printout_TxnResponse_9F27_MC3_B1b3_b1_CryptogramInformationData_ReasonDeviceCode, "B1b3-1 : Reason/advice code", "RFU Reason/device code")

        o_tagTxnInfo.DecIndent()
    End Sub
    '
    Shared Sub PktParser2Printout_9F27_CryptogramInformationData(ByVal tlvValOp As tagTLVNewOp, ByRef refobjTLVAttr As tagTLV, ByRef o_tagTxnInfo As ClassTransaction.TAG_TXN_PARSE_RESPONSE_INFO)

        tlvValOp.ptrFuncCbDummyParser2Printout(tlvValOp, refobjTLVAttr, o_tagTxnInfo)
        o_tagTxnInfo.Add2PrintoutAbs("------------------------[Master Translation]------------------------", 0)
        PktParser2Printout_9F27_MC3_CryptogramInformationData(tlvValOp, refobjTLVAttr, o_tagTxnInfo)
        o_tagTxnInfo.Add2PrintoutAbs("------------------------[Amex Translation]------------------------", 0)
        PktParser2Printout_9F27_Amex_CryptogramInformationData(tlvValOp, refobjTLVAttr, o_tagTxnInfo)
    End Sub

    '=============================[ 9F38 ]================================
    Public Shared Sub PktParser2Printout_9F38_PDOL_MasterCard_Or_Amex(ByVal tlvValOp As tagTLVNewOp, ByRef refobjTLVAttr As tagTLV, ByRef o_tagTxnInfo As ClassTransaction.TAG_TXN_PARSE_RESPONSE_INFO)
        Dim dictTLV As List(Of tagTLV) = New List(Of tagTLV)
        PktParser2Printout_Common_SubTagsParser(tlvValOp, refobjTLVAttr, o_tagTxnInfo, dictTLV)
        ClassTableListMaster.CleanupConfigurationsTLVList(dictTLV)
    End Sub
    '
    '=============================[ 9F39 ]================================
    Public Shared m_dictPktParser2Printout_TxnResponse__9F39_POSEntryMode_Byte1 As Dictionary(Of Byte, String) = New Dictionary(Of Byte, String) From {
{&H90, "Magnetic Stripe Reader swipe"},
{&H91, "Contactless MSD transaction"},
{&H7, "Contactless EMV"}
}
    Shared Sub PktParser2Printout_9F39_POSEntryMode(ByVal tlvValOp As tagTLVNewOp, ByRef refobjTLVAttr As tagTLV, ByRef o_tagTxnInfo As ClassTransaction.TAG_TXN_PARSE_RESPONSE_INFO)

        PktParser2Printout_Common_ByteValueOperation_TagDisplay(tlvValOp, refobjTLVAttr, o_tagTxnInfo, refobjTLVAttr.m_arrayTLVal(0), m_dictPktParser2Printout_TxnResponse__9F39_POSEntryMode_Byte1, "B1 : POS Entry Mode")

    End Sub


    '=============================[ 9F39 ]================================
    Public Shared m_dictPktParser2Printout_TxnResponse_9F40_AdditionalTerminalCapabilities_Byte1 As Dictionary(Of Byte, tagTxnResponseBitValResolveInfo) = New Dictionary(Of Byte, tagTxnResponseBitValResolveInfo) From {
{&H8, New tagTxnResponseBitValResolveInfo(&H80, "Cash")},
{&H7, New tagTxnResponseBitValResolveInfo(&H40, "Goods")},
{&H6, New tagTxnResponseBitValResolveInfo(&H20, "Services")},
{&H5, New tagTxnResponseBitValResolveInfo(&H10, "Cashback")},
{&H4, New tagTxnResponseBitValResolveInfo(&H8, "Inquiry")},
{&H3, New tagTxnResponseBitValResolveInfo(&H4, "Transfer")},
{&H2, New tagTxnResponseBitValResolveInfo(&H2, "Payment")},
{&H1, New tagTxnResponseBitValResolveInfo(&H1, "Administrative")}
}
    Public Shared m_dictPktParser2Printout_TxnResponse_9F40_AdditionalTerminalCapabilities_Byte2 As Dictionary(Of Byte, tagTxnResponseBitValResolveInfo) = New Dictionary(Of Byte, tagTxnResponseBitValResolveInfo) From {
{&H8, New tagTxnResponseBitValResolveInfo(&H80, "Cash Deposit")}
}
    Public Shared m_dictPktParser2Printout_TxnResponse_9F40_AdditionalTerminalCapabilities_Byte3 As Dictionary(Of Byte, tagTxnResponseBitValResolveInfo) = New Dictionary(Of Byte, tagTxnResponseBitValResolveInfo) From {
{&H8, New tagTxnResponseBitValResolveInfo(&H80, "Numeric Keys")},
{&H7, New tagTxnResponseBitValResolveInfo(&H40, "Alphabetical and special characters keys")},
{&H6, New tagTxnResponseBitValResolveInfo(&H20, "Command keys")},
{&H5, New tagTxnResponseBitValResolveInfo(&H10, "Function keys")}
}
    Shared Sub PktParser2Printout_9F40_AdditionalTerminalCapabilities(ByVal tlvValOp As tagTLVNewOp, ByRef refobjTLVAttr As tagTLV, ByRef o_tagTxnInfo As ClassTransaction.TAG_TXN_PARSE_RESPONSE_INFO)
        Dim dictParseResolution9F40ATC As Dictionary(Of Byte, Dictionary(Of Byte, tagTxnResponseBitValResolveInfo)) = New Dictionary(Of Byte, Dictionary(Of Byte, tagTxnResponseBitValResolveInfo)) From {
                {&H0, m_dictPktParser2Printout_TxnResponse_9F40_AdditionalTerminalCapabilities_Byte1},
                {&H1, m_dictPktParser2Printout_TxnResponse_9F40_AdditionalTerminalCapabilities_Byte2},
                {&H2, m_dictPktParser2Printout_TxnResponse_9F40_AdditionalTerminalCapabilities_Byte3}
                }
        PktParser2Printout_Common_ByteBitOperationMultipleLists(tlvValOp, refobjTLVAttr, o_tagTxnInfo, dictParseResolution9F40ATC)
        ' Clear() done in PktParser2Printout_Common_ByteBitOperationMultipleLists()
    End Sub
    '=============================================================================================
    'Ex :
    '    Public Shared m_dictPktParser2Printout_TxnResponse_9F33_TerminalCapabilities_Byte1 As Dictionary(Of Byte, tagTxnResponseBitValResolveInfo) = New Dictionary(Of Byte, tagTxnResponseBitValResolveInfo) From {
    '{&H8, New tagTxnResponseBitValResolveInfo(&H80, "Manual key entry")},
    '{&H7, New tagTxnResponseBitValResolveInfo(&H40, "Magnetic stripe")},
    '{&H6, New tagTxnResponseBitValResolveInfo(&H20, "IC with contacts")}
    '}
    '    Public Shared m_dictPktParser2Printout_TxnResponse_9F33_TerminalCapabilities_Byte2 As Dictionary(Of Byte, tagTxnResponseBitValResolveInfo) = New Dictionary(Of Byte, tagTxnResponseBitValResolveInfo) From {
    '{&H8, New tagTxnResponseBitValResolveInfo(&H80, "Plaintext PIN for ICC verification")},
    '{&H7, New tagTxnResponseBitValResolveInfo(&H40, "Enciphered PIN for online verification")},
    '{&H6, New tagTxnResponseBitValResolveInfo(&H20, "Signature (paper)")},
    '{&H5, New tagTxnResponseBitValResolveInfo(&H10, "Enciphered PIN for offline verification")},
    '{&H4, New tagTxnResponseBitValResolveInfo(&H8, "No CVM required")}
    '}
    '    Public Shared m_dictPktParser2Printout_TxnResponse_9F33_TerminalCapabilities_Byte3 As Dictionary(Of Byte, tagTxnResponseBitValResolveInfo) = New Dictionary(Of Byte, tagTxnResponseBitValResolveInfo) From {
    '{&H8, New tagTxnResponseBitValResolveInfo(&H80, "SDA")},
    '{&H7, New tagTxnResponseBitValResolveInfo(&H40, "DDA")},
    '{&H6, New tagTxnResponseBitValResolveInfo(&H20, "Card capture")},
    '{&H5, New tagTxnResponseBitValResolveInfo(&H10, "RFU")},
    '{&H4, New tagTxnResponseBitValResolveInfo(&H8, "CDA")}
    '}
    '    Shared Sub PktParser2Printout_9F33_TerminalCapabilities(ByVal tlvValOp As tagTLVNewOp, ByRef refobjTLVAttr As tagTLV, ByRef o_tagTxnInfo As ClassTransaction.TAG_TXN_PARSE_RESPONSE_INFO)

    '        Dim dictParseResolution9F33TC As Dictionary(Of Byte, Dictionary(Of Byte, tagTxnResponseBitValResolveInfo)) = New Dictionary(Of Byte, Dictionary(Of Byte, tagTxnResponseBitValResolveInfo)) From {
    '            {&H0, m_dictPktParser2Printout_TxnResponse_9F33_TerminalCapabilities_Byte1},
    '            {&H1, m_dictPktParser2Printout_TxnResponse_9F33_TerminalCapabilities_Byte2},
    '            {&H2, m_dictPktParser2Printout_TxnResponse_9F33_TerminalCapabilities_Byte3}
    '            }
    '        PktParser2Printout_Common_ByteBitOperationMultipleLists(tlvValOp, refobjTLVAttr, o_tagTxnInfo, dictParseResolution9F33TC)

    '    End Sub
    Shared Sub PktParser2Printout_Common_ByteBitOperationMultipleLists(ByVal tlvValOp As tagTLVNewOp, ByRef refobjTLVAttr As tagTLV, ByRef o_tagTxnInfo As ClassTransaction.TAG_TXN_PARSE_RESPONSE_INFO, ByRef dictMultipleLists As Dictionary(Of Byte, Dictionary(Of Byte, tagTxnResponseBitValResolveInfo)), Optional ByVal nLen As Integer = 0)
        Dim nIdx, nIdxMax As Byte
        'Dim iteratorBitParse As KeyValuePair(Of Byte, tagTxnResponseBitValResolveInfo)
        ' 1. Printout Tag Value
        tlvValOp.ptrFuncCbDummyParser2Printout(tlvValOp, refobjTLVAttr, o_tagTxnInfo)
        'Note: Some Tag's Data The Last 1 or more bytes are RFU, so we use nLen to skip the rest 1 or more bytes
        If (nLen > 0) Then
            If (nLen > refobjTLVAttr.m_nLen) Then
                nLen = refobjTLVAttr.m_nLen
            Else
                'Same as it is
            End If
        Else
            nLen = refobjTLVAttr.m_nLen
        End If
        '
        nIdxMax = nLen - 1

        ' prevent seeking selected table index is out of range
        nIdx = dictMultipleLists.Count - 1
        If (nIdx < nIdxMax) Then
            nIdxMax = nIdx
        End If

        For nIdx = 0 To nIdxMax
            With refobjTLVAttr
                PktParser2Printout_Common_ByteBitOperation(tlvValOp, refobjTLVAttr, o_tagTxnInfo, nIdx + 1, refobjTLVAttr.m_arrayTLVal(nIdx), dictMultipleLists.Item(nIdx))
            End With
        Next
        dictMultipleLists.Clear()
    End Sub
    '
    '=============================[ 9F33 ]================================
    Public Shared m_dictPktParser2Printout_TxnResponse_9F33_TerminalCapabilities_Byte1 As Dictionary(Of Byte, tagTxnResponseBitValResolveInfo) = New Dictionary(Of Byte, tagTxnResponseBitValResolveInfo) From {
{&H8, New tagTxnResponseBitValResolveInfo(&H80, "Manual key entry")},
{&H7, New tagTxnResponseBitValResolveInfo(&H40, "Magnetic stripe")},
{&H6, New tagTxnResponseBitValResolveInfo(&H20, "IC with contacts")}
}
    Public Shared m_dictPktParser2Printout_TxnResponse_9F33_TerminalCapabilities_Byte2 As Dictionary(Of Byte, tagTxnResponseBitValResolveInfo) = New Dictionary(Of Byte, tagTxnResponseBitValResolveInfo) From {
{&H8, New tagTxnResponseBitValResolveInfo(&H80, "Plaintext PIN for ICC verification")},
{&H7, New tagTxnResponseBitValResolveInfo(&H40, "Enciphered PIN for online verification")},
{&H6, New tagTxnResponseBitValResolveInfo(&H20, "Signature (paper)")},
{&H5, New tagTxnResponseBitValResolveInfo(&H10, "Enciphered PIN for offline verification")},
{&H4, New tagTxnResponseBitValResolveInfo(&H8, "No CVM required")}
}
    Public Shared m_dictPktParser2Printout_TxnResponse_9F33_TerminalCapabilities_Byte3 As Dictionary(Of Byte, tagTxnResponseBitValResolveInfo) = New Dictionary(Of Byte, tagTxnResponseBitValResolveInfo) From {
{&H8, New tagTxnResponseBitValResolveInfo(&H80, "SDA")},
{&H7, New tagTxnResponseBitValResolveInfo(&H40, "DDA")},
{&H6, New tagTxnResponseBitValResolveInfo(&H20, "Card capture")},
{&H5, New tagTxnResponseBitValResolveInfo(&H10, "RFU")},
{&H4, New tagTxnResponseBitValResolveInfo(&H8, "CDA")}
}
    Shared Sub PktParser2Printout_9F33_TerminalCapabilities(ByVal tlvValOp As tagTLVNewOp, ByRef refobjTLVAttr As tagTLV, ByRef o_tagTxnInfo As ClassTransaction.TAG_TXN_PARSE_RESPONSE_INFO)

        Dim dictParseResolution9F33TC As Dictionary(Of Byte, Dictionary(Of Byte, tagTxnResponseBitValResolveInfo)) = New Dictionary(Of Byte, Dictionary(Of Byte, tagTxnResponseBitValResolveInfo)) From {
            {&H0, m_dictPktParser2Printout_TxnResponse_9F33_TerminalCapabilities_Byte1},
            {&H1, m_dictPktParser2Printout_TxnResponse_9F33_TerminalCapabilities_Byte2},
            {&H2, m_dictPktParser2Printout_TxnResponse_9F33_TerminalCapabilities_Byte3}
            }
        PktParser2Printout_Common_ByteBitOperationMultipleLists(tlvValOp, refobjTLVAttr, o_tagTxnInfo, dictParseResolution9F33TC)

    End Sub
    '=============================[ 9F34 ]================================
    Public Shared m_dictPktParser2Printout_TxnResponse_9F34_CardholderVerificationMethod_Result_Performed_Byte1 As Dictionary(Of Byte, String) = New Dictionary(Of Byte, String) From {
{&H0, "Plaintext PIN verification performed by ICC"},
{&H3F, "No CVM performed"}
}
    Public Shared m_dictPktParser2Printout_TxnResponse_9F34_CardholderVerificationMethod_Result_Condition_Byte2 As Dictionary(Of Byte, String) = New Dictionary(Of Byte, String) From {
{&H0, "Always"},
{&H1, "If unattended cash"},
{&H2, "If not unattended cash and not manual cash and not purchase with cashback"},
{&H3, "If terminal supports the CVM 20"},
{&H4, "If manual cash"},
{&H5, "If purchase with cashback"},
{&H6, "If transaction is in the application currency 21 and is under X value (see section 10.5 for a discussion of ""X"")"},
{&H7, "If transaction is in the application currency and is over X value"},
{&H8, "If transaction is in the application currency and is under Y value (see section 10.5 for a discussion of ""Y"")"},
{&H9, "If transaction is in the application currency and is over Y value"}
}
    Public Shared m_dictPktParser2Printout_TxnResponse_9F34_CardholderVerificationMethod_Result_Result_Byte3 As Dictionary(Of Byte, String) = New Dictionary(Of Byte, String) From {
{&H0, "Unknown"},
{&H1, "Failed or No CVM Condition was NOT Satisfied or NOT Recognized or NOT Supported"},
{&H2, "Successful"}
}
    Shared Sub PktParser2Printout_9F34_CardholderVerificationMethod_Result(ByVal tlvValOp As tagTLVNewOp, ByRef refobjTLVAttr As tagTLV, ByRef o_tagTxnInfo As ClassTransaction.TAG_TXN_PARSE_RESPONSE_INFO)

        Dim dictParseResolution9F34_CVM As Dictionary(Of Byte, KeyValuePair(Of String, Dictionary(Of Byte, String))) = New Dictionary(Of Byte, KeyValuePair(Of String, Dictionary(Of Byte, String))) From {
            {&H0, New KeyValuePair(Of String, Dictionary(Of Byte, String))("CVM Performed", m_dictPktParser2Printout_TxnResponse_9F34_CardholderVerificationMethod_Result_Performed_Byte1)},
            {&H1, New KeyValuePair(Of String, Dictionary(Of Byte, String))("CVM Condition", m_dictPktParser2Printout_TxnResponse_9F34_CardholderVerificationMethod_Result_Condition_Byte2)},
            {&H2, New KeyValuePair(Of String, Dictionary(Of Byte, String))("CVM Result", m_dictPktParser2Printout_TxnResponse_9F34_CardholderVerificationMethod_Result_Result_Byte3)}
            }
        ' 1. Printout Tag Value
        tlvValOp.ptrFuncCbDummyParser2Printout(tlvValOp, refobjTLVAttr, o_tagTxnInfo)

        '2. Printout Detail Info
        o_tagTxnInfo.AddIndent()

        PktParser2Printout_Common_ByteValueOperationMultipleLists(tlvValOp, refobjTLVAttr, o_tagTxnInfo, dictParseResolution9F34_CVM)
        '
        o_tagTxnInfo.DecIndent()
        '
        'dictParseResolution9F34_CVM.Clear()
    End Sub
    '

    '
    Public Shared m_dictPktParser2Printout_TxnResponse_9F35_TerminalType_Byte1 As Dictionary(Of Byte, String) = New Dictionary(Of Byte, String) From {
{11, "Financial Institution, Attended Online Only"},
{12, "Financial Institution, Attended Offline with Online Capability"},
{13, "Financial Institution, Attended Offline Only"},
{14, "Financial Institution, Unattended Online Only"},
{15, "Financial Institution, Unattended Offline with Online Capability"},
{16, "Financial Institution, Unattended Offline Only"},
{21, "Merchant, Attended Online only"},
{22, "Merchant, Attended Offline with Online Capability"},
{23, "Merchant, Attended Offline Only"},
{24, "Merchant, Unattended Online Only"},
{25, "Merchant, Unattended Offline with Online Capability"},
{26, "Merchant, Unattended Offline Only"},
{34, "Cardholder, Unattended Online Only"},
{35, "Cardholder, Unattended Offline with Online Capability"},
{36, "Cardholder, Unattended Offline Only"}
}
    Shared Sub PktParser2Printout_9F35_TerminalType(ByVal tlvValOp As tagTLVNewOp, ByRef refobjTLVAttr As tagTLV, ByRef o_tagTxnInfo As ClassTransaction.TAG_TXN_PARSE_RESPONSE_INFO)

        PktParser2Printout_Common_ByteValueOperation_TagDisplay(tlvValOp, refobjTLVAttr, o_tagTxnInfo, refobjTLVAttr.m_arrayTLVal(0), m_dictPktParser2Printout_TxnResponse_9F35_TerminalType_Byte1, "B1 : Terminal Type")

        '
    End Sub

    Public Shared m_dictPktParser2Printout_TxnResponse_9F5D_Master_Byte1_b4b1 As Dictionary(Of Byte, String) = New Dictionary(Of Byte, String) From {
{&H0, "DATA STORAGE NOT SUPPORTED"},
{&H1, "VERSION 1"},
{&H2, "VERSION 2"}
}
    'Common Prefix String = "CDA SUPPORTED "
    Public Shared m_dictPktParser2Printout_TxnResponse_9F5D_Master_Byte2b1 As Dictionary(Of Byte, String) = New Dictionary(Of Byte, String) From {
        {0, "AS IN EMV"},
        {1, "OVER TC, ARQC AND AAC"}
        }
    Public Shared m_dictPktParser2Printout_TxnResponse_9F5D_Master_Byte2 As Dictionary(Of Byte, tagTxnResponseBitValResolveInfo) = New Dictionary(Of Byte, tagTxnResponseBitValResolveInfo) From {
{&H3, New tagTxnResponseBitValResolveInfo(&H4, "Support for field off detection")},
{&H2, New tagTxnResponseBitValResolveInfo(&H2, "Support for balance reading")},
{&H1, New tagTxnResponseBitValResolveInfo(&H1, "CDA Indicator")}
}
    Public Shared m_dictPktParser2Printout_TxnResponse_9F5D_Master_Byte3 As Dictionary(Of Byte, String) = New Dictionary(Of Byte, String) From {
{&H0, "Undefined SDS configuration"},
{&H1, "All 10 tags 32 bytes"},
{&H2, "All 10 tags 48 bytes"},
{&H3, "All 10 tags 64 bytes"},
{&H4, "All 10 tags 96 bytes"},
{&H5, "All 10 tags 128 bytes"},
{&H6, "All 10 tags 160 bytes"},
{&H7, "All 10 tags 192 bytes"},
{&H8, "All SDS tags 32 bytes except '9F78' which is 64 bytes"}
}
    Shared Sub PktParser2Printout_9F5D_AvailableOfflineSpendingAmount_Len6_AtVisa_CLBookC3_Or_ApplicationCapabilitiesInformation_Len3_AtMaster_CLBookC2(ByVal tlvValOp As tagTLVNewOp, ByRef refobjTLVAttr As tagTLV, ByRef o_tagTxnInfo As ClassTransaction.TAG_TXN_PARSE_RESPONSE_INFO)
        Dim strTmp = "", strTmp2 As String = ""
        Dim byteVal As Byte
        Dim nIdx, nSel As Byte
        Dim iteratorBitParse As KeyValuePair(Of Byte, tagTxnResponseBitValResolveInfo)
        Dim nIndent As Integer = 3

        Dim strMaster_CDASupported_Prefix As String = "CDA SUPPORTED "
        ' 1. Printout Tag Value
        If refobjTLVAttr.m_nLen = 3 Then
            tlvValOp.m_strComment = "Application Capabilities Information"
        ElseIf refobjTLVAttr.m_nLen = 6 Then
            tlvValOp.m_strComment = "Available Offline Spending Amount, AOSA"
        Else
            tlvValOp.m_strComment = tagTLV.s_strPrintoutTransactionUnknownTag
        End If
        tlvValOp.ptrFuncCbDummyParser2Printout(tlvValOp, refobjTLVAttr, o_tagTxnInfo)

        '2. Printout Detail Info
        o_tagTxnInfo.AddIndent()
        nIdx = 0
        With refobjTLVAttr
            If .m_nLen = 3 Then
                '-------[Master Tag]--------
                'Application Capabilities Information
                'B1 b8~5
                byteVal = .m_arrayTLVal(nIdx)
                PktParser2Printout_Common_ByteValueOperation(tlvValOp, refobjTLVAttr, o_tagTxnInfo, (byteVal >> 4) And &HF, m_dictPktParser2Printout_TxnResponse_9F5D_Master_Byte1_b4b1, "B1b8-5 : ACI Version Number")
                PktParser2Printout_Common_ByteValueOperation(tlvValOp, refobjTLVAttr, o_tagTxnInfo, byteVal And &HF, m_dictPktParser2Printout_TxnResponse_9F5D_Master_Byte1_b4b1, "B1b4-1 : Data Storage Version Number")

                'B2
                nIdx = nIdx + 1
                byteVal = .m_arrayTLVal(nIdx) ' And &H7
                For Each iteratorBitParse In m_dictPktParser2Printout_TxnResponse_9F5D_Master_Byte2
                    If ((iteratorBitParse.Value.m_byteMaskBit And byteVal) = iteratorBitParse.Value.m_byteMaskBit) Then
                        nSel = 1
                    Else
                        nSel = 0
                    End If
                    '
                    strTmp = tagTLV.s_strPrintoutTransactionMsgDelimiter + "B" & (nIdx + 1)
                    ' Calculate strTmp2 String "......"
                    strTmp = strTmp + "b" & iteratorBitParse.Key & " : " + iteratorBitParse.Value.m_strMsg
                    PktParser2Printout_Common_FormatLine_GetDotsString(strTmp, nIndent, strTmp2)

                    If (iteratorBitParse.Key = &H1) Then
                        ' Make Output String
                        strTmp = strTmp + strTmp2 + " = " + String.Format("{0,2:X2}", nIdx) + " ( " + strMaster_CDASupported_Prefix + m_dictPktParser2Printout_TxnResponse_9F5D_Master_Byte2b1.Item(nSel) + " )"
                    Else
                        ' Make Output String
                        strTmp = strTmp + strTmp2 + " = " + String.Format("{0,2:X2}", nSel) + " ( " + m_dictPktParser2Printout_TxnResponseCommonStringYesNo.Item(nSel) + " )"
                    End If
                    o_tagTxnInfo.Add2PrintoutAbs(strTmp, nIndent)
                Next

                'Byte 3
                nIdx = nIdx + 1
                PktParser2Printout_Common_ByteValueOperation(tlvValOp, refobjTLVAttr, o_tagTxnInfo, .m_arrayTLVal(nIdx), m_dictPktParser2Printout_TxnResponse_9F5D_Master_Byte3, "B3 : SDS Scheme Indicator")

            ElseIf .m_nLen = 6 Then
                '-------[Visa Tag]--------
                'Available Offline Spending Amount, AOSA
            Else
                o_tagTxnInfo.DecIndent()
                ClassTableListMaster.ConvertFromArrayByte2String(.m_arrayTLVal, strTmp)
                o_tagTxnInfo.Add2Printout("[Err][Unknown Tag Type] Tag = " + .m_strTag + ",Len= " & .m_nLen & ", Data = " + strTmp)
            End If
        End With

        '
        o_tagTxnInfo.DecIndent()
    End Sub
    '
    '=========================[ 9F5F ]=========================
    Public Shared m_dictPktParser2Printout_TxnResponse_9F5F_DSSlotAvailability_Byte1 As Dictionary(Of Byte, tagTxnResponseBitValResolveInfo) = New Dictionary(Of Byte, tagTxnResponseBitValResolveInfo) From {
{&H8, New tagTxnResponseBitValResolveInfo(&H80, "Permanent slot type")},
{&H7, New tagTxnResponseBitValResolveInfo(&H40, "Volatile slot type")}
}
    Shared Sub PktParser2Printout_9F5F_DSSlotAvailability(ByVal tlvValOp As tagTLVNewOp, ByRef refobjTLVAttr As tagTLV, ByRef o_tagTxnInfo As ClassTransaction.TAG_TXN_PARSE_RESPONSE_INFO)
        Dim nIdx As Integer
        ' 1. Printout Tag Value
        tlvValOp.ptrFuncCbDummyParser2Printout(tlvValOp, refobjTLVAttr, o_tagTxnInfo)

        '2. Printout Detail Info
        o_tagTxnInfo.AddIndent()
        'Byte 1 = Version Number
        nIdx = 0
        PktParser2Printout_Common_ByteBitOperation(tlvValOp, refobjTLVAttr, o_tagTxnInfo, nIdx + 1, refobjTLVAttr.m_arrayTLVal(nIdx), m_dictPktParser2Printout_TxnResponse_9F5F_DSSlotAvailability_Byte1)
        '
        o_tagTxnInfo.DecIndent()

    End Sub
    '
    '=========================[ 9F60  ]=========================
    Shared Sub PktParser2Printout_9F60_MC_CVC3_Track1_Or_Amex_ContactlessCumulativeTotalTransactionAmountLowerLimit(ByVal tlvValOp As tagTLVNewOp, ByRef refobjTLVAttr As tagTLV, ByRef o_tagTxnInfo As ClassTransaction.TAG_TXN_PARSE_RESPONSE_INFO)
        'Dim nIdx As Integer

        tlvValOp.m_strComment = "Master:CVC3 (Track 1) ; Amex:Contactless Cumulative Total Transaction Amount Lower Limit "
        ' 1. Printout Tag Value
        tlvValOp.ptrFuncCbDummyParser2Printout(tlvValOp, refobjTLVAttr, o_tagTxnInfo)

    End Sub
    '

    '=========================[ 9F61  ]=========================
    Shared Sub PktParser2Printout_9F61_MC_CVC3_Track2_Or_Amex_ContactlessCumulativeTotalTransactionAmountUpperLimit(ByVal tlvValOp As tagTLVNewOp, ByRef refobjTLVAttr As tagTLV, ByRef o_tagTxnInfo As ClassTransaction.TAG_TXN_PARSE_RESPONSE_INFO)

        tlvValOp.m_strComment = "Master:CVC3 (Track 2) ; Amex:Contactless Cumulative Total Transaction Amount Upper Limit "
        ' 1. Printout Tag Value
        tlvValOp.ptrFuncCbDummyParser2Printout(tlvValOp, refobjTLVAttr, o_tagTxnInfo)

    End Sub
    '=========================[ 9F63  ]=========================
    Shared Sub PktParser2Printout_9F63_MC_PUNATC_Track1_Or_Amex_ContactlessNonDomesticConsecutiveTransactionCounter(ByVal tlvValOp As tagTLVNewOp, ByRef refobjTLVAttr As tagTLV, ByRef o_tagTxnInfo As ClassTransaction.TAG_TXN_PARSE_RESPONSE_INFO)

        tlvValOp.m_strComment = "Master:PUNATC (Track 1) ; Amex:Contactless Non-Domestic Consecutive Transaction Counter "

        ' 1. Printout Tag Value
        tlvValOp.ptrFuncCbDummyParser2Printout(tlvValOp, refobjTLVAttr, o_tagTxnInfo)

    End Sub
    '
    '=========================[ 9F64  ]=========================
    Shared Sub PktParser2Printout_9F64_MC_NATC_Track1_Or_Amex_SingleTransactionValueUpperLimit(ByVal tlvValOp As tagTLVNewOp, ByRef refobjTLVAttr As tagTLV, ByRef o_tagTxnInfo As ClassTransaction.TAG_TXN_PARSE_RESPONSE_INFO)

        tlvValOp.m_strComment = "Master:NATC (Track 1) ; Amex:Single Transaction Value Upper Limit Dual Currency "
        ' 1. Printout Tag Value
        tlvValOp.ptrFuncCbDummyParser2Printout(tlvValOp, refobjTLVAttr, o_tagTxnInfo)
    End Sub
    '
    '=========================[ 9F65  ]=========================
    Shared Sub PktParser2Printout_9F65_MC_PCVC3_Track3_Or_Amex_SingleTransactionValueUpperLimitDualCurrency(ByVal tlvValOp As tagTLVNewOp, ByRef refobjTLVAttr As tagTLV, ByRef o_tagTxnInfo As ClassTransaction.TAG_TXN_PARSE_RESPONSE_INFO)

        tlvValOp.m_strComment = "Master:PCVC3 (Track 3) ; Amex:Single Transaction Value Upper Limit Dual Currency "
        ' 1. Printout Tag Value
        tlvValOp.ptrFuncCbDummyParser2Printout(tlvValOp, refobjTLVAttr, o_tagTxnInfo)
    End Sub

    '=========================[ 9F66 ]=========================
    Public Shared m_dictPktParser2Printout_TxnResponse_9F66_TTQ_Byte1 As Dictionary(Of Byte, tagTxnResponseBitValResolveInfo) = New Dictionary(Of Byte, tagTxnResponseBitValResolveInfo) From {
{&H8, New tagTxnResponseBitValResolveInfo(&H80, "Mag-stripe mode")},
{&H7, New tagTxnResponseBitValResolveInfo(&H40, "RFU")},
{&H6, New tagTxnResponseBitValResolveInfo(&H20, "EMV mode supported")},
{&H5, New tagTxnResponseBitValResolveInfo(&H10, "EMV contact chip")},
{&H4, New tagTxnResponseBitValResolveInfo(&H8, "Offline-only reader")},
{&H3, New tagTxnResponseBitValResolveInfo(&H4, "Online PIN supported")},
{&H2, New tagTxnResponseBitValResolveInfo(&H2, "Signature supported")},
{&H1, New tagTxnResponseBitValResolveInfo(&H1, "Offline Data,Authentication for Online Authorizations supported")}
}
    Public Shared m_dictPktParser2Printout_TxnResponse_9F66_TTQ_Byte2 As Dictionary(Of Byte, tagTxnResponseBitValResolveInfo) = New Dictionary(Of Byte, tagTxnResponseBitValResolveInfo) From {
{&H8, New tagTxnResponseBitValResolveInfo(&H80, "Online cryptogram required")},
{&H7, New tagTxnResponseBitValResolveInfo(&H40, "CVM required")},
{&H6, New tagTxnResponseBitValResolveInfo(&H20, "(Contact Chip) Offline PIN supported")}
}
    Public Shared m_dictPktParser2Printout_TxnResponse_9F66_TTQ_Byte3 As Dictionary(Of Byte, tagTxnResponseBitValResolveInfo) = New Dictionary(Of Byte, tagTxnResponseBitValResolveInfo) From {
{&H8, New tagTxnResponseBitValResolveInfo(&H80, "Issuer Update Processing supported")},
{&H7, New tagTxnResponseBitValResolveInfo(&H40, "Consumer Device CVM supported")}
}
    Shared Sub PktParser2Printout_9F66_TTQInSpecK3AtVisaEMVCL0203_Or_PUNACTTrack2InSpecK2AtMasterCardEMVCL0203_Or_Amex_SingleTransactionValueLimitCheck_DualCurrencyCheck(ByVal tlvValOp As tagTLVNewOp, ByRef refobjTLVAttr As tagTLV, ByRef o_tagTxnInfo As ClassTransaction.TAG_TXN_PARSE_RESPONSE_INFO)
        Dim bIsVisa As Boolean = True

        tlvValOp.m_strComment = "Master:PUNATC ; Visa:Visa TTQ(Visa ONLY) ; Amex:Single Transaction ValueLimit Check - Dual Currency Check "
        tlvValOp.ptrFuncCbDummyParser2Printout(tlvValOp, refobjTLVAttr, o_tagTxnInfo)

        '
        If (bIsVisa) Then
            Dim dictParseResolution9F66TTQ As Dictionary(Of Byte, Dictionary(Of Byte, tagTxnResponseBitValResolveInfo)) = New Dictionary(Of Byte, Dictionary(Of Byte, tagTxnResponseBitValResolveInfo)) From {
{&H0, m_dictPktParser2Printout_TxnResponse_9F66_TTQ_Byte1},
{&H1, m_dictPktParser2Printout_TxnResponse_9F66_TTQ_Byte2},
{&H2, m_dictPktParser2Printout_TxnResponse_9F66_TTQ_Byte3}
}
            o_tagTxnInfo.Add2PrintoutAbs("------------------------[Visa Translation]------------------------", 0)
            PktParser2Printout_Common_ByteBitOperationMultipleLists(tlvValOp, refobjTLVAttr, o_tagTxnInfo, dictParseResolution9F66TTQ, refobjTLVAttr.m_nLen - 1 - 1)
        End If
        '' 1. Printout Tag Value
        'If refobjTLVAttr.m_nLen = 4 Then

        'Else
        'End If

        ''2. Printout Detail Info
        'If refobjTLVAttr.m_nLen = 4 Then
        '    '-------------------[ 2.1 TLV tag Len = 4 bytes --> TTQ ]-------------------
        '    '[Special Note] Byte 4 (the last  byte) is RFU now 
        'ElseIf refobjTLVAttr.m_nLen = 2 Then
        '    '-------------------[ 2.2 TLV tag Len = 2 bytes --> PUNACT ]-------------------
        '    'Do Nothing , Dump content Done by tlvValOp.m_pfuncCbDummyParser2Printout()
        '    'o_tagTxnInfo.AddIndent()
        '    ' Add your code if it's required
        '    '
        '    'o_tagTxnInfo.DecIndent()
        'End If
        '
    End Sub
    '
    Public Shared m_dictPktParser2Printout_Amex_9F6D_ExpresspayTerminalCapabilities As Dictionary(Of Byte, String) = New Dictionary(Of Byte, String) From {
{&H0, "Expresspay 1.0"},
{&H2, "Expresspay 2.0 - Magstripe Only"},
{&H4, "Expresspay 2.0 - EMV and MagStripe"}
}

    Shared Sub PktParser2Printout_Amex_9F6D_ExpresspayTerminalCapabilities(ByVal tlvValOp As tagTLVNewOp, ByRef refobjTLVAttr As tagTLV, ByRef o_tagTxnInfo As ClassTransaction.TAG_TXN_PARSE_RESPONSE_INFO)
        Dim byteVal, byteVal1 As Byte
        'Amex
        o_tagTxnInfo.Add2PrintoutAbs("------------------------[Amex Translation]------------------------", 0)
        tlvValOp.m_strComment = "Expresspay Terminal Capabilities"
        byteVal = refobjTLVAttr.m_arrayTLVal(0)
        byteVal1 = ((byteVal >> 6) And &H3) Or ((byteVal >> 3) And &H1)
        PktParser2Printout_Common_ByteValueOperation_TagDisplay(tlvValOp, refobjTLVAttr, o_tagTxnInfo, byteVal1, m_dictPktParser2Printout_Amex_9F6D_ExpresspayTerminalCapabilities, "B1b8-7,b4 : Express Terminal Capabilities")
        'Master
        o_tagTxnInfo.Add2PrintoutAbs("------------------------[Master Translation]------------------------", 0)
        tlvValOp.m_strComment = "Mag-Stripe Application Version Number (Reader)"
        ' 1. Printout Tag Value
        tlvValOp.ptrFuncCbDummyParser2Printout(tlvValOp, refobjTLVAttr, o_tagTxnInfo)

    End Sub
    '
    '=========================[ 9F6F  ]=========================
    Public Shared m_dictPktParser2Printout_TxnResponse_9F6F_DSSlotManagementControl_Byte1 As Dictionary(Of Byte, tagTxnResponseBitValResolveInfo) = New Dictionary(Of Byte, tagTxnResponseBitValResolveInfo) From {
{&H8, New tagTxnResponseBitValResolveInfo(&H80, "Permanent slot type")},
{&H7, New tagTxnResponseBitValResolveInfo(&H40, "Volatile slot type")},
{&H6, New tagTxnResponseBitValResolveInfo(&H20, "Low volatility")},
{&H5, New tagTxnResponseBitValResolveInfo(&H10, "Locked slot")},
{&H1, New tagTxnResponseBitValResolveInfo(&H1, "Deactivated slot")}
}
    Shared Sub PktParser2Printout_9F6F_DSSlotManagementControl(ByVal tlvValOp As tagTLVNewOp, ByRef refobjTLVAttr As tagTLV, ByRef o_tagTxnInfo As ClassTransaction.TAG_TXN_PARSE_RESPONSE_INFO)
        Dim nIdx As Integer
        ' 1. Printout Tag Value
        tlvValOp.ptrFuncCbDummyParser2Printout(tlvValOp, refobjTLVAttr, o_tagTxnInfo)

        '2. Printout Detail Info
        o_tagTxnInfo.AddIndent()
        'Byte 1 = Version Number
        nIdx = 0
        PktParser2Printout_Common_ByteBitOperation(tlvValOp, refobjTLVAttr, o_tagTxnInfo, nIdx + 1, refobjTLVAttr.m_arrayTLVal(nIdx), m_dictPktParser2Printout_TxnResponse_9F6F_DSSlotManagementControl_Byte1)
        '
        o_tagTxnInfo.DecIndent()

    End Sub


    '=========================[ 9F7E  ]=========================
    Shared Sub PktParser2Printout_9F7E_MobileSupportIndicator_MC3_MerchantCustomData_Or_Visa_CustomerExclusiveData(ByVal tlvValOp As tagTLVNewOp, ByRef refobjTLVAttr As tagTLV, ByRef o_tagTxnInfo As ClassTransaction.TAG_TXN_PARSE_RESPONSE_INFO)
        'Dim nIdx As Integer
        o_tagTxnInfo.Add2PrintoutAbs("------------------------[Master Translation]------------------------", 0)
        tlvValOp.m_strComment = "Merchant Custom Data"
        tlvValOp.ptrFuncCbDummyParser2Printout(tlvValOp, refobjTLVAttr, o_tagTxnInfo)

        o_tagTxnInfo.Add2PrintoutAbs("------------------------[Visa/ Interac/ Amex Translation]------------------------", 0)
        tlvValOp.m_strComment = "Customer Exclusive Data"
        tlvValOp.ptrFuncCbDummyParser2Printout(tlvValOp, refobjTLVAttr, o_tagTxnInfo)

    End Sub
    '
    '=========================[ 9F7E  ]=========================
    Public Shared m_dictPktParser2Printout_TxnResponse_9F7E_MobileSupportIndicator_Byte1 As Dictionary(Of Byte, tagTxnResponseBitValResolveInfo) = New Dictionary(Of Byte, tagTxnResponseBitValResolveInfo) From {
{&H2, New tagTxnResponseBitValResolveInfo(&H2, "Offline PIN Required")},
{&H1, New tagTxnResponseBitValResolveInfo(&H1, "Mobile supported")}
}
    Shared Sub PktParser2Printout_9F7E_MobileSupportIndicator(ByVal tlvValOp As tagTLVNewOp, ByRef refobjTLVAttr As tagTLV, ByRef o_tagTxnInfo As ClassTransaction.TAG_TXN_PARSE_RESPONSE_INFO)
        Dim nIdx As Integer
        ' 1. Printout Tag Value
        tlvValOp.ptrFuncCbDummyParser2Printout(tlvValOp, refobjTLVAttr, o_tagTxnInfo)

        '2. Printout Detail Info
        o_tagTxnInfo.AddIndent()
        'Byte 1 = Version Number
        nIdx = 0
        PktParser2Printout_Common_ByteBitOperation(tlvValOp, refobjTLVAttr, o_tagTxnInfo, nIdx + 1, refobjTLVAttr.m_arrayTLVal(nIdx), m_dictPktParser2Printout_TxnResponse_9F7E_MobileSupportIndicator_Byte1)
        '
        o_tagTxnInfo.DecIndent()

    End Sub
    '
    '===================================[ BF0C ]====================================
    Shared Sub PktParser2Printout_BF0C_FileControlInformationIssureDiscretionaryData(ByVal tlvValOp As tagTLVNewOp, ByRef refobjTLVAttr As tagTLV, ByRef o_tagTxnInfo As ClassTransaction.TAG_TXN_PARSE_RESPONSE_INFO)
        PktParser2Printout_Common_SubTagsParser(tlvValOp, refobjTLVAttr, o_tagTxnInfo, o_tagTxnInfo.m_dictTlvsTxnResult_BF0C_FileControlInformationIssureDiscretionaryData)
    End Sub
    '=========================[ DF30  ]=========================
    Public Shared m_dictPktParser2Printout_MC_DF30_ViVOProprietaryTag_TrackDataSource As Dictionary(Of Byte, String) = New Dictionary(Of Byte, String) From {
{&H0, "A Contactless Card"},
{&HC, " Swiped MagStripe"}
}
    Shared Sub PktParser2Printout_DF30_ViVOProprietaryTag_TrackDataSource(ByVal tlvValOp As tagTLVNewOp, ByRef refobjTLVAttr As tagTLV, ByRef o_tagTxnInfo As ClassTransaction.TAG_TXN_PARSE_RESPONSE_INFO)

        PktParser2Printout_Common_ByteValueOperation_TagDisplay(tlvValOp, refobjTLVAttr, o_tagTxnInfo, refobjTLVAttr.m_arrayTLVal(0), m_dictPktParser2Printout_MC_DF30_ViVOProprietaryTag_TrackDataSource, "B1 : Track Data Source")

    End Sub
    '
    '=========================[ DF33 ]=========================
    Public Shared m_dictPktParser2Printout_MC_DF33_ViVOProprietaryTag_ReceiptRequirement As Dictionary(Of Byte, String) = New Dictionary(Of Byte, String) From {
   {&H0, "No Receipt Required"},
   {&H1, "Receipt Required"}
   }
    Shared Sub PktParser2Printout_DF33_ViVOProprietaryTag_ReceiptRequirement(ByVal tlvValOp As tagTLVNewOp, ByRef refobjTLVAttr As tagTLV, ByRef o_tagTxnInfo As ClassTransaction.TAG_TXN_PARSE_RESPONSE_INFO)

        PktParser2Printout_Common_ByteValueOperation_TagDisplay(tlvValOp, refobjTLVAttr, o_tagTxnInfo, refobjTLVAttr.m_arrayTLVal(0), m_dictPktParser2Printout_MC_DF33_ViVOProprietaryTag_ReceiptRequirement, , "B1 : Receipt Requirement")
    End Sub

    '=========================[ DF49 ]=========================
    Shared Sub PktParser2Printout_DF49_ViVOProprietaryTag_PrePostPPSE(ByVal tlvValOp As tagTLVNewOp, ByRef refobjTLVAttr As tagTLV, ByRef o_tagTxnInfo As ClassTransaction.TAG_TXN_PARSE_RESPONSE_INFO)

        If (refobjTLVAttr.m_nLen = 9) Then
            tlvValOp.m_strTag = "Pre-PPSE"
        Else
            tlvValOp.m_strTag = "Post-PPSE"
        End If

        ' 1. Printout Tag Value
        tlvValOp.ptrFuncCbDummyParser2Printout(tlvValOp, refobjTLVAttr, o_tagTxnInfo)

    End Sub
    '
    '=========================[ DF4B ]=========================
    Public Shared m_dictPktParser2Printout_TxnResponse_DF4B_POSCardholderInteractionInformation_Byte2 As Dictionary(Of Byte, tagTxnResponseBitValResolveInfo) = New Dictionary(Of Byte, tagTxnResponseBitValResolveInfo) From {
{&H5, New tagTxnResponseBitValResolveInfo(&H10, "Offline PIN verification successful")},
{&H4, New tagTxnResponseBitValResolveInfo(&H8, "Context is conflicting")},
{&H3, New tagTxnResponseBitValResolveInfo(&H4, "Offline change PIN required")},
{&H2, New tagTxnResponseBitValResolveInfo(&H2, "ACK required")},
{&H1, New tagTxnResponseBitValResolveInfo(&H1, "PIN required")}
}
    Shared Sub PktParser2Printout_DF4B_POSCardholderInteractionInformation(ByVal tlvValOp As tagTLVNewOp, ByRef refobjTLVAttr As tagTLV, ByRef o_tagTxnInfo As ClassTransaction.TAG_TXN_PARSE_RESPONSE_INFO)
        Dim strMsg = "", strTmp2 As String = ""
        'Dim byteVal As Byte
        'Dim byteStrIdx As Byte
        Dim nIdx As Byte
        Dim nIndent As Integer

        ' 1. Printout Tag Value
        tlvValOp.ptrFuncCbDummyParser2Printout(tlvValOp, refobjTLVAttr, o_tagTxnInfo)

        '2. Printout Detail Info
        'Byte 1 = Version Number
        nIdx = 0
        strMsg = tagTLV.s_strPrintoutTransactionMsgDelimiter + "B1 : Version Number"
        PktParser2Printout_Common_FormatLine_GetDotsString(strMsg, nIndent, strTmp2)
        o_tagTxnInfo.Add2Printout(strMsg + strTmp2 + String.Format(" = {0,2:X2}", refobjTLVAttr.m_arrayTLVal(nIdx)), nIndent)

        'Byte 2 
        nIdx = 1
        PktParser2Printout_Common_ByteBitOperation(tlvValOp, refobjTLVAttr, o_tagTxnInfo, nIdx + 1, refobjTLVAttr.m_arrayTLVal(nIdx), m_dictPktParser2Printout_TxnResponse_DF4B_POSCardholderInteractionInformation_Byte2)
        '
    End Sub
    '
    '=========================[ DF51 ]=========================
    Shared Sub PktParser2Printout_Amex_DF51_ExpresspayTerminalCapabilities(ByVal tlvValOp As tagTLVNewOp, ByRef refobjTLVAttr As tagTLV, ByRef o_tagTxnInfo As ClassTransaction.TAG_TXN_PARSE_RESPONSE_INFO)
        Dim byteVal, byteVal1 As Byte

        byteVal = refobjTLVAttr.m_arrayTLVal(0)
        byteVal1 = ((byteVal >> 6) And &H3) Or ((byteVal >> 3) And &H1)
        PktParser2Printout_Common_ByteValueOperation_TagDisplay(tlvValOp, refobjTLVAttr, o_tagTxnInfo, byteVal1, m_dictPktParser2Printout_Amex_9F6D_ExpresspayTerminalCapabilities, "B1b8-7,b4 : Express Terminal Capabilities")
    End Sub
    '=========================[ DF52 ]=========================
    Public Shared m_dictPktParser2Printout_MC_DF52_ViVOProprietaryTag_TransactionCVM_Type As Dictionary(Of Byte, String) = New Dictionary(Of Byte, String) From {
       {&H0, "for No CVM"},
       {&H1, "for Signature"},
       {&H2, "for Online PIN"},
       {&H3, "for Mobile CVM / Consumer Device CVM"}
       }
    Shared Sub PktParser2Printout_DF52_ViVOProprietaryTag_TransactionCVM(ByVal tlvValOp As tagTLVNewOp, ByRef refobjTLVAttr As tagTLV, ByRef o_tagTxnInfo As ClassTransaction.TAG_TXN_PARSE_RESPONSE_INFO)
        PktParser2Printout_Common_ByteValueOperation_TagDisplay(tlvValOp, refobjTLVAttr, o_tagTxnInfo, refobjTLVAttr.m_arrayTLVal(0), m_dictPktParser2Printout_MC_DF52_ViVOProprietaryTag_TransactionCVM_Type, "B1 : CVM Mode", "Unknown CVM Code")
    End Sub
    '
    '=========================[ DF5B ]=========================
    Public Shared m_dictPktParser2Printout_TxnResponse_DF5B_ViVOProprietaryTag_TerminalEntryCapability_Byte1 As Dictionary(Of Byte, String) = New Dictionary(Of Byte, String) From {
{&H5, "Reader supports VSDC contact chip"},
{&H8, "Reader does not support VSDC contact chip"}
}
    'Ex: PktParser2Printout_Common_ByteValueOperation(tlvValOp,refobjTLVAttr,o_tagTxnInfo, .m_arrayTLVal(nIdx), m_dictPktParser2Printout_TxnResponse_9F5D_Master_Byte3, "Byte 3, SDS Scheme Indicator")
    Shared Sub PktParser2Printout_DF5B_ViVOProprietaryTag_TerminalEntryCapability(ByVal tlvValOp As tagTLVNewOp, ByRef refobjTLVAttr As tagTLV, ByRef o_tagTxnInfo As ClassTransaction.TAG_TXN_PARSE_RESPONSE_INFO)
        'Dim nIdx As Byte

        PktParser2Printout_Common_ByteValueOperation_TagDisplay(tlvValOp, refobjTLVAttr, o_tagTxnInfo, refobjTLVAttr.m_arrayTLVal(0), m_dictPktParser2Printout_TxnResponse_DF5B_ViVOProprietaryTag_TerminalEntryCapability_Byte1, "B1 : Terminal Entry Capability", "Unknown Value")

    End Sub
    '
    '=========================[ DF62 ]=========================
    Public Shared m_dictPktParser2Printout_TxnResponse_DF62_DS_ODS_Info_Byte1 As Dictionary(Of Byte, tagTxnResponseBitValResolveInfo) = New Dictionary(Of Byte, tagTxnResponseBitValResolveInfo) From {
{&H8, New tagTxnResponseBitValResolveInfo(&H80, "Permanent slot type")},
{&H7, New tagTxnResponseBitValResolveInfo(&H40, "Volatile slot type")},
{&H6, New tagTxnResponseBitValResolveInfo(&H20, "Low volatility")},
{&H4, New tagTxnResponseBitValResolveInfo(&H8, "Decline payment transaction in case of data storage error")}
}
    '{&H5, New tagTxnResponseBitValResolveInfo(&H10, "RFU")},
    'Ex: PktParser2Printout_Common_ByteValueOperation(tlvValOp,refobjTLVAttr,o_tagTxnInfo, .m_arrayTLVal(nIdx), m_dictPktParser2Printout_TxnResponse_9F5D_Master_Byte3, "Byte 3, SDS Scheme Indicator")
    Shared Sub PktParser2Printout_DF62_DS_ODS_Info(ByVal tlvValOp As tagTLVNewOp, ByRef refobjTLVAttr As tagTLV, ByRef o_tagTxnInfo As ClassTransaction.TAG_TXN_PARSE_RESPONSE_INFO)

        tlvValOp.ptrFuncCbDummyParser2Printout(tlvValOp, refobjTLVAttr, o_tagTxnInfo)

        PktParser2Printout_Common_ByteBitOperation(tlvValOp, refobjTLVAttr, o_tagTxnInfo, 1, refobjTLVAttr.m_arrayTLVal(0), m_dictPktParser2Printout_TxnResponse_DF62_DS_ODS_Info_Byte1)

    End Sub
    '
    '=========================[  DF76 ]=========================
    Public Shared m_dictPktParser2Printout_TxnResponse_DF76_TVRBeforeGenAC_Byte1 As Dictionary(Of Byte, tagTxnResponseBitValResolveInfo) = New Dictionary(Of Byte, tagTxnResponseBitValResolveInfo) From {
{&H8, New tagTxnResponseBitValResolveInfo(&H80, "offline data auth was not performed")},
{&H7, New tagTxnResponseBitValResolveInfo(&H40, "SDA failed")},
{&H6, New tagTxnResponseBitValResolveInfo(&H20, "ICC data missing")},
{&H5, New tagTxnResponseBitValResolveInfo(&H10, "Card appears on terminal exception file")},
{&H4, New tagTxnResponseBitValResolveInfo(&H8, "DDA failed")},
{&H3, New tagTxnResponseBitValResolveInfo(&H4, "CDA failed")}
}
    Public Shared m_dictPktParser2Printout_TxnResponse_DF76_TVRBeforeGenAC_Byte2 As Dictionary(Of Byte, tagTxnResponseBitValResolveInfo) = New Dictionary(Of Byte, tagTxnResponseBitValResolveInfo) From {
{&H8, New tagTxnResponseBitValResolveInfo(&H80, "ICC and terminal have different application versions")},
{&H7, New tagTxnResponseBitValResolveInfo(&H40, "expired application")},
{&H6, New tagTxnResponseBitValResolveInfo(&H20, "application not yet effective")},
{&H5, New tagTxnResponseBitValResolveInfo(&H10, "requested service not allowed for card product")},
{&H4, New tagTxnResponseBitValResolveInfo(&H8, "new card")}
}
    Public Shared m_dictPktParser2Printout_TxnResponse_DF76_TVRBeforeGenAC_Byte3 As Dictionary(Of Byte, tagTxnResponseBitValResolveInfo) = New Dictionary(Of Byte, tagTxnResponseBitValResolveInfo) From {
{&H8, New tagTxnResponseBitValResolveInfo(&H80, "cardholder verification was not successful")},
{&H7, New tagTxnResponseBitValResolveInfo(&H40, "unrecognized CVM")},
{&H6, New tagTxnResponseBitValResolveInfo(&H20, "PIN try limit exceeded")},
{&H5, New tagTxnResponseBitValResolveInfo(&H10, "PIN entry required and PIN pad not present or not working")},
{&H4, New tagTxnResponseBitValResolveInfo(&H8, "PIN entry required. PIN pad present, but PIN was not entered")},
{&H3, New tagTxnResponseBitValResolveInfo(&H4, "Online PIN entered")}
}
    Public Shared m_dictPktParser2Printout_TxnResponse_DF76_TVRBeforeGenAC_Byte4 As Dictionary(Of Byte, tagTxnResponseBitValResolveInfo) = New Dictionary(Of Byte, tagTxnResponseBitValResolveInfo) From {
{&H8, New tagTxnResponseBitValResolveInfo(&H80, "transaction exceeds floor limit")},
{&H7, New tagTxnResponseBitValResolveInfo(&H40, "lower consecutive offline limit exceeded")},
{&H6, New tagTxnResponseBitValResolveInfo(&H20, "upper consecutive offline limit exceeded")},
{&H5, New tagTxnResponseBitValResolveInfo(&H10, "Transaction selected randomly for online processing")},
{&H4, New tagTxnResponseBitValResolveInfo(&H8, "merchant forced transaction online")}
}
    Shared Sub PktParser2Printout_DF76_TVRBeforeGenAC(ByVal tlvValOp As tagTLVNewOp, ByRef refobjTLVAttr As tagTLV, ByRef o_tagTxnInfo As ClassTransaction.TAG_TXN_PARSE_RESPONSE_INFO)
        Dim dictParseResolutionDF76TVRBeforeGenAC As Dictionary(Of Byte, Dictionary(Of Byte, tagTxnResponseBitValResolveInfo)) = New Dictionary(Of Byte, Dictionary(Of Byte, tagTxnResponseBitValResolveInfo)) From {
            {&H0, m_dictPktParser2Printout_TxnResponse_DF76_TVRBeforeGenAC_Byte1},
            {&H1, m_dictPktParser2Printout_TxnResponse_DF76_TVRBeforeGenAC_Byte2},
            {&H2, m_dictPktParser2Printout_TxnResponse_DF76_TVRBeforeGenAC_Byte3},
            {&H3, m_dictPktParser2Printout_TxnResponse_DF76_TVRBeforeGenAC_Byte4}
            }
        PktParser2Printout_Common_ByteBitOperationMultipleLists(tlvValOp, refobjTLVAttr, o_tagTxnInfo, dictParseResolutionDF76TVRBeforeGenAC, dictParseResolutionDF76TVRBeforeGenAC.Count)
    End Sub
    '
    '=========================[ FF69  ]=========================
    Shared Sub PktParser2Printout_FF69_ViVOProprietaryTag(ByVal tlvValOp As tagTLVNewOp, ByRef refobjTLVAttr As tagTLV, ByRef o_tagTxnInfo As ClassTransaction.TAG_TXN_PARSE_RESPONSE_INFO)
        Dim strMsg = "", strTmp2 As String = ""
        Dim nIndent As Integer
        ' 1. Printout Tag Value
        If refobjTLVAttr.m_nLen = 0 Then
            strMsg = tlvValOp.m_strTag + tagTLV.s_strPrintoutTransactionMsgDelimiter + tlvValOp.m_strComment
            PktParser2Printout_Common_FormatLine_GetDotsString(strMsg, nIndent, strTmp2)
            o_tagTxnInfo.Add2PrintoutAbs(strMsg + " = N/A", nIndent)
        Else
            tlvValOp.ptrFuncCbDummyParser2Printout(tlvValOp, refobjTLVAttr, o_tagTxnInfo)
        End If

    End Sub

    '
    Public m_strTag As String
    Public m_nLen As Integer
    Public m_nLenMin As Integer
    Public m_bIsUsed As Boolean
    Public m_strComment As String

    '
    Public Sub PktComposer(ByVal tlvVal As tagTLV, ByRef idgCmdData As tagIDGCmdData)
        m_pfuncCbComposer(tlvVal, idgCmdData)
    End Sub
    '
    Public Sub PktParser2Printout(ByVal tlvValOp As tagTLVNewOp, ByRef tlvVal As tagTLV, ByRef o_tagTxnInfo As ClassTransaction.TAG_TXN_PARSE_RESPONSE_INFO)
        m_pfuncCbParser2Printout(tlvValOp, tlvVal, o_tagTxnInfo)
    End Sub
    '
    Sub New(ByRef strTag As String, ByVal nLen As Integer, ByVal nLenMin As Integer, Optional pfuncCbComposer As ComposerDelegate = Nothing, Optional pfuncCbParser As Parser2PrintoutDelegate = Nothing, Optional ByVal strComment As String = "")
        m_strTag = strTag
        m_nLen = nLen
        m_nLenMin = nLenMin
        m_bIsUsed = False
        m_strComment = strComment
        '
        'If strTag = "FF69" Then
        '    Dim i As Integer
        '    i = 0
        'End If
        m_pfuncCbComposer = pfuncCbComposer
        m_pfuncCbParser2Printout = pfuncCbParser
        If (pfuncCbComposer Is Nothing) Then
            m_pfuncCbComposer = New ComposerDelegate(AddressOf ptrFuncCbDummyComposer)
        End If
        If (pfuncCbParser Is Nothing) Then
            m_pfuncCbParser2Printout = New Parser2PrintoutDelegate(AddressOf ptrFuncCbDummyParser2Printout)
        End If

    End Sub

    Sub Cleanup()

    End Sub

    'Note: Alias on " --> " string on both sides(left and right)
    'Example 01:
    ' IN : strTmp = FF1987 --> MC3 Lookup Amount Way
    'OUT : nIndent = 0 = 6 - "FF1987".length=6-6, 
    '           strTmp2 = "......" length = 70 - nIndent - strTmp Length = 
    'Example 02:
    ' IN : strTmp = DF01  --> MC3 Lookup Amount Way
    'OUT : nIndent = 2  = 6 - "DF01".length = 6-4 
    '           strTmp2 = "......" length = 70 - nIndent - strTmp Length = 
    Public Shared m_nPrintoutTransactionMsgPrefixLen As Integer = 70
    Public Shared m_nPrintoutTransactionMsgPrefixLen2 As Integer = 150
    Private Shared Sub PktParser2Printout_Common_FormatLine_GetDotsString(strTmp As String, ByRef nIndent As Integer, ByRef strTmp2 As String)
        Dim nMaxLineWidth As Integer = 50
        Dim nDotLineWidth As Integer
        Dim nIndex As Integer

        ' Get nIndent from " --> "s Left side sub string length 
        nIndex = strTmp.IndexOf(tagTLV.s_strPrintoutTransactionMsgDelimiter)
        If (nIndex < 0) Then
            nIndex = strTmp.IndexOf(tagTLV.s_strPrintoutTransactionMsgDelimiterRev)
            If (nIndex < 0) Then
                nIndent = 0
                strTmp2 = New String("."c, m_nPrintoutTransactionMsgPrefixLen)
                Return
            End If
        End If
        nIndent = 6 - nIndex
        If (nIndent < 0) Then
            nIndent = 0
        End If
        'Get Dot String
        nDotLineWidth = nMaxLineWidth - nIndent - strTmp.Length
        If (nDotLineWidth < 1) Then
            strTmp2 = ""
        Else
            strTmp2 = New String("."c, nDotLineWidth)
        End If
    End Sub

End Structure
'
'TLV Table Creation Operation for CAPK Text Parser & Commands Set Unit
Public Structure tagTLVNewOpCAPK
    '==================[ Delegate Function type ]===================
    Public m_pfuncCbComposer As tagTLVNewOp.ComposerDelegate
    'Public m_funcptrTLVParserCAPK As ClassReaderCommanderParser.TLVParser_TemplateDelegateCAPK
    Public Sub PktComposer(ByVal tlvVal As tagTLV, ByRef idgCmdData As tagIDGCmdData)
        m_pfuncCbComposer(tlvVal, idgCmdData)
    End Sub

    Public m_strTag As String
    Public m_nLen As Integer
    Public m_nLenMin As Integer
    '
    Sub New(ByRef strTag As String, ByVal nLen As Integer, ByVal nLenMin As Integer, Optional ByRef pfuncCbComposer As tagTLVNewOp.ComposerDelegate = Nothing) ', ByRef funcptrTLVParserCAPK As ClassReaderCommanderParser.TLVParser_TemplateDelegateCAPK
        m_strTag = strTag
        m_nLen = nLen
        m_nLenMin = nLenMin
        'm_funcptrTLVParserCAPK = funcptrTLVParserCAPK
        m_pfuncCbComposer = pfuncCbComposer
        If (m_pfuncCbComposer Is Nothing) Then
            m_pfuncCbComposer = New tagTLVNewOp.ComposerDelegate(AddressOf ClassReaderProtocolIDGGR.m_pfuncCbComposerCAPK)
        End If
    End Sub

    Sub Cleanup()

    End Sub
End Structure
'
Public Structure tagTxtFileConfigurationsCmd
    Public m_nCmdType As Integer
    Public m_strCmdType As String

    Sub New(ByVal strCmdType As String, ByVal nCmdType As ClassReaderCommanderParser.EnumCRLCmd)
        m_strCmdType = strCmdType
        m_nCmdType = nCmdType
    End Sub
    Sub New(ByVal strCmdType As String, ByVal nCmdType As ClassReaderCommanderParser.EnumConfigCmd)
        m_strCmdType = strCmdType
        m_nCmdType = nCmdType
    End Sub

End Structure
