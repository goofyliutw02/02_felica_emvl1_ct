﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class Form02_NumOfRoundsInput
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Me.Btn_Cancel = New System.Windows.Forms.Button()
        Me.GroupBox1 = New System.Windows.Forms.GroupBox()
        Me.NumUpDn_Rnd = New System.Windows.Forms.NumericUpDown()
        Me.Btn_Done = New System.Windows.Forms.Button()
        Me.ToolTip1 = New System.Windows.Forms.ToolTip(Me.components)
        Me.GroupBox1.SuspendLayout()
        CType(Me.NumUpDn_Rnd, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'Btn_Cancel
        '
        Me.Btn_Cancel.Location = New System.Drawing.Point(157, 96)
        Me.Btn_Cancel.Name = "Btn_Cancel"
        Me.Btn_Cancel.Size = New System.Drawing.Size(87, 40)
        Me.Btn_Cancel.TabIndex = 0
        Me.Btn_Cancel.Text = "Cancel"
        Me.Btn_Cancel.UseVisualStyleBackColor = True
        '
        'GroupBox1
        '
        Me.GroupBox1.Controls.Add(Me.NumUpDn_Rnd)
        Me.GroupBox1.Font = New System.Drawing.Font("PMingLiU", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(136, Byte))
        Me.GroupBox1.Location = New System.Drawing.Point(4, 12)
        Me.GroupBox1.Name = "GroupBox1"
        Me.GroupBox1.Size = New System.Drawing.Size(240, 71)
        Me.GroupBox1.TabIndex = 1
        Me.GroupBox1.TabStop = False
        Me.GroupBox1.Text = "1~9999 rounds; 0 = Loop Forever"
        '
        'NumUpDn_Rnd
        '
        Me.NumUpDn_Rnd.Location = New System.Drawing.Point(56, 26)
        Me.NumUpDn_Rnd.Maximum = New Decimal(New Integer() {9999, 0, 0, 0})
        Me.NumUpDn_Rnd.Name = "NumUpDn_Rnd"
        Me.NumUpDn_Rnd.Size = New System.Drawing.Size(120, 27)
        Me.NumUpDn_Rnd.TabIndex = 0
        Me.ToolTip1.SetToolTip(Me.NumUpDn_Rnd, "If = 0 --> Forever Loop")
        Me.NumUpDn_Rnd.Value = New Decimal(New Integer() {100, 0, 0, 0})
        '
        'Btn_Done
        '
        Me.Btn_Done.Location = New System.Drawing.Point(4, 96)
        Me.Btn_Done.Name = "Btn_Done"
        Me.Btn_Done.Size = New System.Drawing.Size(87, 40)
        Me.Btn_Done.TabIndex = 0
        Me.Btn_Done.Text = "OK"
        Me.Btn_Done.UseVisualStyleBackColor = True
        '
        'Form02_NumOfRoundsInput
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 12.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(252, 141)
        Me.ControlBox = False
        Me.Controls.Add(Me.GroupBox1)
        Me.Controls.Add(Me.Btn_Done)
        Me.Controls.Add(Me.Btn_Cancel)
        Me.Name = "Form02_NumOfRoundsInput"
        Me.ShowIcon = False
        Me.ShowInTaskbar = False
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent
        Me.Text = "Input Num"
        Me.GroupBox1.ResumeLayout(False)
        CType(Me.NumUpDn_Rnd, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents Btn_Cancel As System.Windows.Forms.Button
    Friend WithEvents GroupBox1 As System.Windows.Forms.GroupBox
    Friend WithEvents NumUpDn_Rnd As System.Windows.Forms.NumericUpDown
    Friend WithEvents Btn_Done As System.Windows.Forms.Button
    Friend WithEvents ToolTip1 As System.Windows.Forms.ToolTip
End Class
