﻿Imports System.Globalization

'=============================================
' 2017 Oct 25 created
' This for RFID / CL Poll For Token Purpose

Partial Public Class Form01_Main

    'if 2C-06 or 2C-07 are Found, we 
    Private m_bIsMifareCmdsFound As Boolean = False

    Structure _TAG_PARSE_INFO_BLK
        'm_listTxRxSimDat
        ' Ex: Input = 2018/4/23 11:29:48.824 [TX] - 56 69 56 4F 74 65 63 68 32 00 2C 07 00 02 31 06 2B 1A 
        ' Output = 5669564F7465636832002C07000231062B1A
        '  Hdr = 5669564F746563683200
        ' Output2 = 2C07000231062B1A, skip Hdr

        'Step 01 - Normalize to upper case
        ' 
        Public Sub Init()
            Cleanup()
        End Sub

        Public Sub Cleanup()
            m_strTx = ""
            m_strRxCmp = ""
        End Sub

        Public m_strTx As String
        Public m_strRxCmp As String


        Public ReadOnly Property bIsRxCmp As Boolean
            Get
                If (m_strRxCmp IsNot Nothing) Then
                    Return  (m_strRxCmp.Length > 0) 
                End If
                Return False
            End Get
        End Property

        'strRxDat, [IN], Data of Rx,
        Function IsRxDatEqualed(strRxDat As String) As Boolean
            Dim strTmp As String
            Dim nIdxOff As Integer
            Dim nRxDatLen As Integer

            If (strRxDat Is Nothing) Then
                Return False
            End If

            If (m_strRxCmp Is Nothing) Then
                Return False
            End If

            If (m_strRxCmp.Length < 1) Then
                Return False
            End If

            'Ex01: 2C0000001C9B
            'CRC = 1C9B
            ' CMD = 2C, V2 Sts = 00,
            ' Length = 0000
            ' 2C00 0000 1C9B
            ' Data = NULL

            'Ex02 :

            nIdxOff = 8
            nRxDatLen = m_strRxCmp.Length - 8 - 4
            If (nRxDatLen < 1) Then
                'No Data Need to check, always TRUE
                Return True
            End If
            strTmp = m_strRxCmp.Substring(nIdxOff, nRxDatLen)

            'Normalize Rx Data
            ' remove " ", Upper Case
            strRxDat = Replace(strRxDat, " ", "")
            strRxDat = strRxDat.ToUpper()

            Return strTmp.Equals(strRxDat)
        End Function

    End Structure

    Enum _PARSE_CMD_TYPE As Integer
        _UNKNOWN = -1
        _TX
        _RX
        '
        _MAX_VAL
    End Enum
    '
    Private Const c_strKeyParse_01_TX As String = "[TX] - "
    Private Const c_strKeyParse_02_RX As String = "[RX] - "
    Private m_strKeyParse_02_V2Hdr_Compressed As String = "5669564F746563683200"

    '''////////////////////////////////////////////////////////////////////////////////////////////////////
    ''' \fn Private Function TxRxSimDat_01_Load_LineParse_01_CmdType(strlstTmp As String, ByRef o_nIdx As Integer) As _PARSE_CMD_TYPE End Function Private Function TxRxSimDat_01_Load_LineParse(strlstTmp As String()) As Boolean Dim nLn As Integer = 0 Dim nLnUpdate As Integer = 0 ' div 10 = 10 % Dim nLnUpdateMax As Integer = strlstTmp.Length \ 10 Dim nIdx As Integer 'nCmdType = -1 , Unknown Command, skipped
    '''
    ''' \brief  Transmit receive simulation dat 01 load line parse 01 command type
    '''
    ''' \author Goofy Liu
    ''' \date   2018/4/26
    '''
    ''' \param          strlstTmp   The strlst temporary.
    ''' \param [out] o_nIdx      Zero-based index of the cmd key in the strlstTmp
    '''
    ''' \return A _PARSE_CMD_TYPE.
    '''////////////////////////////////////////////////////////////////////////////////////////////////////

    Private Function TxRxSimDat_01_Load_LineParse_01_CmdType(strlstTmp As String, ByRef o_nIdx As Integer, ByRef o_CmdLen As Integer) As _PARSE_CMD_TYPE
        Dim nCmdType As _PARSE_CMD_TYPE = _PARSE_CMD_TYPE._UNKNOWN

        o_CmdLen = 0

        While (True)

            o_nIdx = strlstTmp.IndexOf(c_strKeyParse_01_TX)
            If (0 <= o_nIdx) Then
                nCmdType = _PARSE_CMD_TYPE._TX
                o_CmdLen = c_strKeyParse_01_TX.Length
                Exit While
            End If

            o_nIdx = strlstTmp.IndexOf(c_strKeyParse_02_RX)
            If (0 <= o_nIdx) Then
                nCmdType = _PARSE_CMD_TYPE._RX
                o_CmdLen = c_strKeyParse_02_RX.Length
                Exit While
            End If

            '
            Exit While
        End While

        Return nCmdType
    End Function
    '
    Private Sub TxRxSimDat_Init()
        TxRxSimDat_Cleanup()
    End Sub
    '
    Private Sub TxRxSimDat_Cleanup()
        For Each itrTagInfoBlk As _TAG_PARSE_INFO_BLK In m_listTxRxSimDat
            itrTagInfoBlk.Cleanup()
        Next
        '
        m_listTxRxSimDat.Clear()
    End Sub
    '
    Private Function TxRxSimDat_01_Load_LineParse(strlstTmp As String()) As Boolean
        Dim nLn As Integer = 0
        Dim nLnUpdate As Integer = 0
        ' div 10 = 10 %
        Dim nLnUpdateMax As Integer = strlstTmp.Length \ 10
        Dim nIdx As Integer
        'nCmdType = -1 , Unknown Command, skipped
        '         = 0, "[TX] -" , Cmd Tx --> New _TAG_PARSE_INFO_BLK, Init it, Add Tx Part
        '         = 1, "[RX] -" , Cmd Rx --> Add Rx Part for comparing, if No Rx Found , we don't care result.
        Dim nCmdType As _PARSE_CMD_TYPE = _PARSE_CMD_TYPE._UNKNOWN
        Dim nCmdTypeLastUsed As _PARSE_CMD_TYPE = _PARSE_CMD_TYPE._UNKNOWN
        Dim nCmdLen As Integer = 0
        Dim tagInfoTxRx As _TAG_PARSE_INFO_BLK = Nothing

        Me.ProgressBarStart(strlstTmp.Length)

        'm_listTxRxSimDat
        ' Ex: Input = 2018/4/23 11:29:48.824 [TX] - 56 69 56 4F 74 65 63 68 32 00 2C 07 00 02 31 06 2B 1A 
        ' Output = 5669564F7465636832002C07000231062B1A
        '  Hdr = 5669564F746563683200
        ' Output2 = 2C07000231062B1A, skip Hdr
        For Each strItrLn As String In strlstTmp
            'Step 01 - Normalize to upper case
            strItrLn = strItrLn.ToUpper()
            nLn = nLn + 1
            nLnUpdate = nLnUpdate + 1

            If (nLnUpdate >= nLnUpdateMax) Then
                nLnUpdate = 0
                Me.ProgressBarUpdate(nLn, True)
                'Update UI Bar per 10 %
                Form01_Main.AppSleep(20)
            End If

#If 1 Then
            'Step 02 - Find Key Str "[TX..., Trim Off Left + remove space " " and the like
            nCmdType = TxRxSimDat_01_Load_LineParse_01_CmdType(strItrLn, nIdx, nCmdLen)
            If (nCmdType = _PARSE_CMD_TYPE._UNKNOWN) Then
                LogLn_UID("Skipped No Cmd Found Ln (" + nLn.ToString() + ") : " + strItrLn)
                Continue For
            End If
#Else
              nIdx = strItrLn.IndexOf(c_strKeyParse_01_TX)
            nCmdLen =c_strKeyParse_01_TX.Length
            If (0 <= nIdx) Then
                nCmdType = _PARSE_CMD_TYPE._TX
            Else

                LogLn_UID("Skipped No-TX Ln (" + nLn.ToString() + ") : " + strItrLn)
                Continue For
            End If
#End If

            strItrLn = strItrLn.Substring(nIdx + nCmdLen)
            strItrLn = Replace(strItrLn, " ", "")

            'Step 03 - Trim Off Left, include c_strKeyParse_02_V2Hdr_Compressed
            nIdx = strItrLn.IndexOf(m_strKeyParse_02_V2Hdr_Compressed)
            If (-1 = nIdx) Then
                'Not Found
                LogLn_UID("Skipped Non-V2 Tx (" + nLn.ToString() + ") : " + strItrLn)
                Continue For
            End If
            strItrLn = strItrLn.Substring(nIdx + m_strKeyParse_02_V2Hdr_Compressed.Length)

            'Step 04
            Select Case (nCmdType)
                Case _PARSE_CMD_TYPE._TX
                    If ((tagInfoTxRx.bIsRxCmp) Or (tagInfoTxRx.m_strTx IsNot Nothing)) Then
                        m_listTxRxSimDat.Add(tagInfoTxRx)
                    End If
                    tagInfoTxRx = New _TAG_PARSE_INFO_BLK()
                    tagInfoTxRx.Init()
                    tagInfoTxRx.m_strTx = strItrLn
                    'Save Last Used status
                    nCmdTypeLastUsed = nCmdType
                Case _PARSE_CMD_TYPE._RX
                    If (tagInfoTxRx.Equals(Nothing)) Then
                        tagInfoTxRx = New _TAG_PARSE_INFO_BLK()
                        tagInfoTxRx.Init()
                    End If
                    tagInfoTxRx.m_strRxCmp = strItrLn
                    'Save Last Used status
                    nCmdTypeLastUsed = nCmdType
                Case Else
                    'Unknown
            End Select

            'Step XX - Add formal line to output list cmd 

#If 0 Then
            m_listTxRxSimDat.Add(strItrLn)
#End If
        Next
        '
        'Add Last one if possible
        If (nCmdTypeLastUsed <> _PARSE_CMD_TYPE._UNKNOWN) Then
            If ((tagInfoTxRx.bIsRxCmp) Or (tagInfoTxRx.m_strTx IsNot Nothing)) Then
                m_listTxRxSimDat.Add(tagInfoTxRx)
            End If
        End If

        Me.ProgressBarUpdate(nLn)
        Me.ProgressBarEnd()
        '
        LogLn_UID(" Added Ln # = " + m_listTxRxSimDat.Count.ToString())
        '
        Return (m_listTxRxSimDat.Count > 0)
    End Function
End Class
