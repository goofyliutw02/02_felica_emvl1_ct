﻿'==================================================================================
' File Name: ClassReaderProtocolBasic.vb
' Description: The ClassReaderProtocolBasic is as Must-be Inherited Base Class of Reader's Packet Parser & Composer.
' Who use it as Base Class: ClassReaderProtocolBasicIDGAR, ClassReaderProtocolBasicIDGAR, and ClassReaderProtocolBasicIDTECH
' For Security and Cryptography Functions
' 
' Revision:
' -----------------------[ Initial Version ]-----------------------
' 2017 Oct 06, by goofy_liu@idtechproducts.com.tw
' 2018 Jan 10, goofy_liu
' Add VP8800-PISCES Commands
'  D0-29 : Device Reset
'==================================================================================
'
'----------------------[ Importing Stuffs ]----------------------
Imports System.Runtime.Serialization
Imports System.Runtime.Serialization.Formatters.Binary
'Imports DiagTool_HardwareExclusive.ClassReaderProtocolBasicIDGAR
Imports System.IO
Imports System.IO.Ports
Imports System.Threading

'-------------------[ Class Declaration ]-------------------

Partial Public Class ClassReaderProtocolBasic
    '===[ AR 3.0.0, VP8800 ]===
    'Step 01 - Device Reset, D0-29
    Public Sub SystemSecurity_AR300_UFMG_xD029_DeviceReset(Optional ByVal nTimeout As Integer = ClassDevRS232.m_cnTX_TIMEOUT_IN_SEC)
        Dim u16Cmd As UInt16 = &HD029
        Dim strFuncName As String = "SystemSecurity_AR300_UFMG_xD029_DeviceReset"
        Dim bytAryTx As Byte() = Nothing

        If (m_bIDGCmdBusy) Then
            Return
        End If
        m_bIDGCmdBusy = True

        Try
            '----------------------[ Send to reader ]---------------------
            System_Customized_Command(u16Cmd, strFuncName, bytAryTx, Nothing, AddressOf ClassReaderProtocolBasic.PreProcessCbRecv, nTimeout)

            If (bIsRetStatusOK) Then

            Else

            End If

        Catch ext As TimeoutException
            LogLn("[Err][Timeout] [IDG: " + GetIDGCmdString(u16Cmd) + "] " + strFuncName + "() = " + ext.Message)
        Catch ex As Exception
            LogLn("[Err] [IDG:  " + GetIDGCmdString(u16Cmd) + "] " + strFuncName + "() = " + ex.Message)
        Finally
            If (bytAryTx IsNot Nothing) Then
                Erase bytAryTx
            End If
            '
            m_tagIDGCmdDataInfo.Cleanup()
            m_bIDGCmdBusy = False
        End Try
    End Sub

    'Step 02 - Activate Tamper, 04-21
    '2017 Aug 08, goofy_liu
    'VP6300 Vendi-Pro, Beta01
    Public Sub SystemSecurity_Tamper_Disable(Optional ByVal nTimeout As Integer = ClassDevRS232.m_cnTX_TIMEOUT_IN_SEC)
        Dim byteEnable As Byte = &H3
        SystemSecurity_Tamper(byteEnable, Nothing, nTimeout)
    End Sub
    '
    '2017 Aug 08, this is valid for PISCES (AR 300)
    Public Sub SystemSecurity_Tamper_Enable(Optional ByVal nTimeout As Integer = ClassDevRS232.m_cnTX_TIMEOUT_IN_SEC)
        Dim byteEnable As Byte = &H1
        SystemSecurity_Tamper(byteEnable, Nothing, nTimeout)
    End Sub
    '
    Public Sub SystemSecurity_Tamper_StsGetBitmap(ByRef o_aryByteRx As Byte(), Optional ByVal nTimeout As Integer = ClassDevRS232.m_cnTX_TIMEOUT_IN_SEC)
        Dim byteEnable As Byte = &H2
        SystemSecurity_Tamper(byteEnable, o_aryByteRx, nTimeout)
    End Sub
    '
    'Note : Output List = (0,1,2,3...); 0-based index
    'Ex: o_listTamperIndicator = { 1, 3,5 } --> Tamper 1;3;5  are ON
    Public Sub SystemSecurity_Tamper_StsGetBitmap(ByRef o_aryByteRx As Byte(), ByRef o_listTamperIndicator As List(Of Byte), Optional ByVal nTimeout As Integer = ClassDevRS232.m_cnTX_TIMEOUT_IN_SEC)
        '
        'Dim aryByteRx As Byte() = Nothing
        '
        SystemSecurity_Tamper_StsGetBitmap(o_aryByteRx, nTimeout)
        '
        If (o_listTamperIndicator Is Nothing) Then
            o_listTamperIndicator = New List(Of Byte)()
        Else
            o_listTamperIndicator.Clear()
        End If

        If (bIsRetStatusOK) Then
            If (o_aryByteRx IsNot Nothing) Then
                '
                Dim nIndicator As Integer
                Dim bitMask As Byte
                Dim byteVal As Byte
                Dim nIdxBit As Integer
                nIndicator = 0

                'Tamper Bit Sequence, B0b0_b6 = Tamper 0~6, B1b1 = Tamper 9
                'Array.Reverse(aryByteRx)
                For Each byteVal In o_aryByteRx
                    'Bit Sequence, Left to Right bit
                    bitMask = &H1
                    For nIdxBit = 1 To 8
                        If ((byteVal And bitMask) = bitMask) Then
                            o_listTamperIndicator.Add(nIndicator)
                        End If
                        'Advanced indicator & mask
                        nIndicator = nIndicator + 1
                        bitMask = ((bitMask << 1) And (&HFF))
                    Next
                Next
                '
                'Erase o_aryByteRx
            End If
        End If
    End Sub
    '
    Public Overridable Sub SystemSecurity_Tamper(ByVal byteTamperCmd As Byte, ByRef o_aryDatRx As Byte(), Optional ByVal nTimeout As Integer = ClassDevRS232.m_cnTX_TIMEOUT_IN_SEC)
        '04-21 , len = 01, dat = 01: Enable Tamper; dat
        'Using AR @ PISCES platform.

        Dim u16Cmd As UInt16 = &H421
        Dim strFuncName As String = "SystemSecurity_Tamper"
        Dim aryDatTx As Byte() = New Byte() {byteTamperCmd}
        Dim aryDatRx As Byte() = Nothing

        If (m_bIDGCmdBusy) Then
            Return
        End If
        m_bIDGCmdBusy = True

        Try
            '-----------------------------------------------------------------[ Send to reader ]-----------------------------------------------------------------
            System_Customized_Command(u16Cmd, strFuncName, aryDatTx, o_aryDatRx, AddressOf ClassReaderProtocolBasic.PreProcessCbRecv, nTimeout)

            If (bIsRetStatusOK) Then

            Else

            End If

        Catch ext As TimeoutException
            LogLn("[Err][Tx/Rx Timeout] [IDG: " + GetIDGCmdString(u16Cmd) + "] " + strFuncName + "() = " + ext.Message)
        Catch ex As Exception
            LogLn("[Err] [IDG:  " + GetIDGCmdString(u16Cmd) + "] " + strFuncName + "() = " + ex.Message)
        Finally
            m_tagIDGCmdDataInfo.Cleanup()
            m_bIDGCmdBusy = False
            Erase aryDatTx
            '
        End Try

    End Sub
    '
    'This is solution for FWVP5300-83 bug. please see its comment.
    'https://atlassian.idtechproducts.com/jira/browse/FWVP5300-83?focusedCommentId=191162&page=com.atlassian.jira.plugin.system.issuetabpanels:comment-tabpanel#comment-191162
    Private Shared m_s_nDelayLatency_NEO20_Cmd_9007_or_1200_ms As Integer = 1000
    Public Sub SystemSecurity_NEO20_UMFG_x9007_Start(Optional ByVal nTimeout As Integer = ClassDevRS232.m_cnTX_TIMEOUT_IN_SEC)
        Dim u16Cmd As UInt16 = &H9007
        Dim strFuncName As String = "SystemSecurity_NEO20_UMFG_x9007_Start"
        Dim bytAryTx As Byte() = New Byte() {&H0}

        If (m_bIDGCmdBusy) Then
            Return
        End If
        m_bIDGCmdBusy = True

        Try
            '----------------------[ Send to reader ]---------------------
            System_Customized_Command(u16Cmd, strFuncName, bytAryTx, Nothing, AddressOf ClassReaderProtocolBasic.PreProcessCbRecv, nTimeout)

            If (bIsRetStatusOK) Then
                'See FWVP5300-83 bug comment
                Thread.Sleep(m_s_nDelayLatency_NEO20_Cmd_9007_or_1200_ms)
            Else

            End If

        Catch ext As TimeoutException
            LogLn("[Err][Timeout] [IDG: " + GetIDGCmdString(u16Cmd) + "] " + strFuncName + "() = " + ext.Message)
        Catch ex As Exception
            LogLn("[Err] [IDG:  " + GetIDGCmdString(u16Cmd) + "] " + strFuncName + "() = " + ex.Message)
        Finally
            If (bytAryTx IsNot Nothing) Then
                Erase bytAryTx
            End If
            '
            m_tagIDGCmdDataInfo.Cleanup()
            m_bIDGCmdBusy = False
        End Try
    End Sub

    Public Sub SystemSecurity_NEO20_UMFG_x9007_End(Optional ByVal nTimeout As Integer = ClassDevRS232.m_cnTX_TIMEOUT_IN_SEC)
        Dim u16Cmd As UInt16 = &H9007
        Dim strFuncName As String = "SystemSecurity_NEO20_UMFG_x9007_End"
        Dim bytAryTx As Byte() = New Byte() {&H1}

        If (m_bIDGCmdBusy) Then
            Return
        End If
        m_bIDGCmdBusy = True

        Try
            '----------------------[ Send to reader ]---------------------
            System_Customized_Command(u16Cmd, strFuncName, bytAryTx, Nothing, AddressOf ClassReaderProtocolBasic.PreProcessCbRecv, nTimeout)

            If (bIsRetStatusOK) Then
                'See FWVP5300-83 bug comment
                Thread.Sleep(m_s_nDelayLatency_NEO20_Cmd_9007_or_1200_ms)

            Else

            End If

        Catch ext As TimeoutException
            LogLn("[Err][Timeout] [IDG: " + GetIDGCmdString(u16Cmd) + "] " + strFuncName + "() = " + ext.Message)
        Catch ex As Exception
            LogLn("[Err] [IDG:  " + GetIDGCmdString(u16Cmd) + "] " + strFuncName + "() = " + ex.Message)
        Finally
            If (bytAryTx IsNot Nothing) Then
                Erase bytAryTx
            End If
            '
            m_tagIDGCmdDataInfo.Cleanup()
            m_bIDGCmdBusy = False
        End Try
    End Sub

    Sub SystemSecurity_NEO20_UMFG_x9000_DevReset(Optional ByVal nTimeout As Integer = ClassDevRS232.m_cnTX_TIMEOUT_IN_SEC)
        Dim u16Cmd As UInt16 = &H9000
        Dim strFuncName As String = "SystemSecurity_NEO20_UMFG_x9000_DevReset"
        Dim bytAryTx As Byte() = Nothing

        If (m_bIDGCmdBusy) Then
            Return
        End If
        m_bIDGCmdBusy = True

        Try
            '----------------------[ Send to reader ]---------------------
            System_Customized_Command(u16Cmd, strFuncName, bytAryTx, Nothing, AddressOf ClassReaderProtocolBasic.PreProcessCbRecv, nTimeout)

            If (bIsRetStatusOK) Then

            Else

            End If

        Catch ext As TimeoutException
            LogLn("[Err][Timeout] [IDG: " + GetIDGCmdString(u16Cmd) + "] " + strFuncName + "() = " + ext.Message)
        Catch ex As Exception
            LogLn("[Err] [IDG:  " + GetIDGCmdString(u16Cmd) + "] " + strFuncName + "() = " + ex.Message)
        Finally
            If (bytAryTx IsNot Nothing) Then
                Erase bytAryTx
            End If
            '
            m_tagIDGCmdDataInfo.Cleanup()
            m_bIDGCmdBusy = False
        End Try
    End Sub
    '
    '
    Sub SystemSecurity_NEO20_UMFG_x9001_SetRTC(ByVal i_strDate_YYMMDD As String, ByVal i_strTime_HHMMSS As String, Optional ByVal nTimeout As Integer = ClassDevRS232.m_cnTX_TIMEOUT_IN_SEC)
        Dim u16Cmd As UInt16 = &H9001
        Dim strFuncName As String = "SystemSecurity_NEO20_UMFG_x9001_SetRTC"
        Dim aryDatTx As Byte() = Nothing
        Dim aryDatRx As Byte() = Nothing
        Dim aryDatTmp As Byte() = Nothing
        Dim aryDatTmp2 As Byte() = Nothing
        Dim nOffset As Integer = 0

        If (m_bIDGCmdBusy) Then
            Return
        End If
        m_bIDGCmdBusy = True

        Try
            aryDatTx = New Byte(5) {}

            'Ex: Set 16(x10):32(x20):00 s + Year: 20(x14)17(x11) + Date: 9(x09)/30(x1E)
            '[TX] - 56 69 56 4F 74 65 63 68 32 00 90 01 00 06 10 20 14 11 09 1E 97 C1 
            '[RX] - 56 69 56 4F 74 65 63 68 32 00 90 00 00 00 A2 78 

            'Translate to Set Time Format
            'Step 01 - HH:MM

            ClassTableListMaster.TLVauleFromValueString2ByteArray(i_strDate_YYMMDD, aryDatTmp) 'part I
            aryDatTx(4) = aryDatTmp(1)
            aryDatTx(5) = aryDatTmp(2)
            'Step 02 - YYyy

            'Ex: 2017 = 0x14 0x11
            aryDatTx(2) = &H14 '= 20, BCD, YY
            aryDatTx(3) = aryDatTmp(0) ' Ex = 17, BCD, yy
            'Step 03 - MM:DD
            ClassTableListMaster.TLVauleFromValueString2ByteArray(i_strTime_HHMMSS, aryDatTmp2) 'part II
            aryDatTx(0) = aryDatTmp2(0)
            aryDatTx(1) = aryDatTmp2(1)


            Erase aryDatRx
            Erase aryDatTmp
            Erase aryDatTmp2
            '
            aryDatRx = Nothing
            aryDatTmp = Nothing
            aryDatTmp2 = Nothing
            '

            '-----------------------------------------------------------------[ Send to reader ]-----------------------------------------------------------------
            System_Customized_Command(u16Cmd, strFuncName, aryDatTx, aryDatRx, AddressOf ClassReaderProtocolBasic.PreProcessCbRecv, nTimeout)

            If (bIsRetStatusOK) Then

            Else

            End If

        Catch ext As TimeoutException
            LogLn("[Err][Tx/Rx Timeout] [IDG: " + GetIDGCmdString(u16Cmd) + "] " + strFuncName + "() = " + ext.Message)
        Catch ex As Exception
            LogLn("[Err] [IDG:  " + GetIDGCmdString(u16Cmd) + "] " + strFuncName + "() = " + ex.Message)
        Finally
            m_tagIDGCmdDataInfo.Cleanup()
            m_bIDGCmdBusy = False
            Erase aryDatTx
            '
        End Try
    End Sub

    '2017 Oct 31 updated, 
    'Ref Doc: "IDG Supplement for UMFG SMFG RKI TR-31 rev 61.docx"
    'byte1_Peripheral : LCD = 1, RF LED = 2, Flash LED = 3, ICC LED = 4, Buzzer = 5, All peripherals = &HFF
    'byte2_Function: ON = 1, OFF = 0
    Sub SystemSecurity_NEO20_SMFG_x010B_PeripheralControl(byte1_Peripheral As Byte, byte2_Function As Byte, Optional ByVal nTimeout As Integer = ClassDevRS232.m_cnTX_TIMEOUT_IN_SEC)

        Dim u16Cmd As UInt16 = &H10B
        Dim strFuncName As String = "SystemSecurity_NEO20_SMFG_x010B_PeripheralControl"
        Dim bytAryTx As Byte() = New Byte() {byte1_Peripheral, byte2_Function}

        If (m_bIDGCmdBusy) Then
            Return
        End If
        m_bIDGCmdBusy = True

        Try
            '----------------------[ Send to reader ]---------------------
            System_Customized_Command(u16Cmd, strFuncName, bytAryTx, Nothing, AddressOf ClassReaderProtocolBasic.PreProcessCbRecv, nTimeout)

            If (bIsRetStatusOK) Then

            Else

            End If

        Catch ext As TimeoutException
            LogLn("[Err][Timeout] [IDG: " + GetIDGCmdString(u16Cmd) + "] " + strFuncName + "() = " + ext.Message)
        Catch ex As Exception
            LogLn("[Err] [IDG:  " + GetIDGCmdString(u16Cmd) + "] " + strFuncName + "() = " + ex.Message)
        Finally
            If (bytAryTx IsNot Nothing) Then
                Erase bytAryTx
            End If
            '
            m_tagIDGCmdDataInfo.Cleanup()
            m_bIDGCmdBusy = False
        End Try
    End Sub

    '2017 Oct 31 updated, 
    'Ref Doc: "IDG Supplement for UMFG SMFG RKI TR-31 rev 61.docx"
    'byte1_Peripheral : LCD = 1, RF LED = 2, Flash LED = 3, ICC LED = 4, Buzzer = 5, All peripherals = &HFF
    'byte2_Function: ON = 1, OFF = 0
    Sub SystemSecurity_NEO20_UMFG_x9013_PeripheralControl(byte1_Peripheral As Byte, byte2_Function As Byte, Optional ByVal nTimeout As Integer = ClassDevRS232.m_cnTX_TIMEOUT_IN_SEC)

        Dim u16Cmd As UInt16 = &H9013
        Dim strFuncName As String = "SystemSecurity_NEO20_UMFG_x9013_PeripheralControl"
        Dim bytAryTx As Byte() = New Byte() {byte1_Peripheral, byte2_Function}

        If (m_bIDGCmdBusy) Then
            Return
        End If
        m_bIDGCmdBusy = True

        Try
            '----------------------[ Send to reader ]---------------------
            System_Customized_Command(u16Cmd, strFuncName, bytAryTx, Nothing, AddressOf ClassReaderProtocolBasic.PreProcessCbRecv, nTimeout)

            If (bIsRetStatusOK) Then

            Else

            End If

        Catch ext As TimeoutException
            LogLn("[Err][Timeout] [IDG: " + GetIDGCmdString(u16Cmd) + "] " + strFuncName + "() = " + ext.Message)
        Catch ex As Exception
            LogLn("[Err] [IDG:  " + GetIDGCmdString(u16Cmd) + "] " + strFuncName + "() = " + ex.Message)
        Finally
            If (bytAryTx IsNot Nothing) Then
                Erase bytAryTx
            End If
            '
            m_tagIDGCmdDataInfo.Cleanup()
            m_bIDGCmdBusy = False
        End Try
    End Sub
    '
    Public Sub SystemSecurity_NEO20_UMFG_x9007_SecureTaskControl(byte1_0_Start_1_End_Task As Byte, Optional nTimeout As Integer = ClassDevRS232.m_cnTX_TIMEOUT_IN_SEC)

        Dim u16Cmd As UInt16 = &H9007
        Dim strFuncName As String = "SystemSecurity_NEO20_UMFG_x9007_SecureTaskControl"
        Dim bytAryTx As Byte() = New Byte() {byte1_0_Start_1_End_Task}

        If (m_bIDGCmdBusy) Then
            Return
        End If
        m_bIDGCmdBusy = True

        Try
            '----------------------[ Send to reader ]---------------------
            System_Customized_Command(u16Cmd, strFuncName, bytAryTx, Nothing, AddressOf ClassReaderProtocolBasic.PreProcessCbRecv, nTimeout)

            If (bIsRetStatusOK) Then
                'See FWVP5300-83 bug comment
                Thread.Sleep(m_s_nDelayLatency_NEO20_Cmd_9007_or_1200_ms)

            Else

            End If

        Catch ext As TimeoutException
            LogLn("[Err][Timeout] [IDG: " + GetIDGCmdString(u16Cmd) + "] " + strFuncName + "() = " + ext.Message)
        Catch ex As Exception
            LogLn("[Err] [IDG:  " + GetIDGCmdString(u16Cmd) + "] " + strFuncName + "() = " + ex.Message)
        Finally
            If (bytAryTx IsNot Nothing) Then
                Erase bytAryTx
            End If
            '
            m_tagIDGCmdDataInfo.Cleanup()
            m_bIDGCmdBusy = False
        End Try
    End Sub

    '
    Sub SystemSecurity_NEO20_UMFG_x9010_SetSerialNum(i_strSerialNum As String, Optional ByVal i_nTimeoutSec As Integer = ClassDevRS232.m_cnTX_TIMEOUT_IN_SEC)
        Dim u16Cmd As UInt16 = &H9010
        Dim strFuncName As String = "SystemTime_NEO20_UMFG_x9010_SetSerialNum"
        SystemSerialNumberSet_Generic(u16Cmd, strFuncName, i_strSerialNum, i_nTimeoutSec)
    End Sub

    'Output 
    Public Overridable Sub SystemSecurity_Tamper_NEO20_UMFG_x9002_Activate(Optional ByVal i_byteOnOff As Byte = &H0, Optional ByRef o_strRespData As String = Nothing, Optional ByVal i_nTimeout As Integer = ClassDevRS232.m_cnTX_TIMEOUT_IN_SEC_60)
        Dim arrayByteResp As Byte() = Nothing

        If (o_strRespData IsNot Nothing) Then
            arrayByteResp = New Byte() {&H0}
        End If

        SystemSecurity_NEO20_UMFG_x9002_Activate_With_Tamper(i_byteOnOff, arrayByteResp, i_nTimeout)

        o_strRespData = ""
        'Translate to ASCII String
        If (bIsRetStatusOK) Then
            If (arrayByteResp IsNot Nothing) Then
                If (arrayByteResp.Length > 0) Then
                    ClassTableListMaster.ConvertFromHexToAscii(arrayByteResp, o_strRespData)
                End If
            End If
        End If

    End Sub

    '======[ NEO 2.0 - Get Tamper Log Commands ]======
    Public Overridable Sub SystemSecurity_NEO20_UMFG_x9002_Activate_With_Tamper(Optional ByVal i_byteOnOff As Byte = &H0, Optional ByRef o_arrayByteData As Byte() = Nothing, Optional ByVal i_nTimeout As Integer = ClassDevRS232.m_cnTX_TIMEOUT_IN_SEC_60)
        '
        Dim u16Cmd As UInt16 = &H9002
        Dim strFuncName As String = "SystemSecurity_NEO20_UMFG_x9002_Activate_With_Tamper"
        Dim arrayByteSend As Byte() = New Byte() {i_byteOnOff}
        Dim arrayByteRecv As Byte() = o_arrayByteData

        If (m_bIDGCmdBusy) Then
            Return
        End If
        m_bIDGCmdBusy = True

        Try
            'ClassTableListMaster.ConvertValue2ArrayByte(byteModeOn, arrayByteSend)

            System_Customized_Command(u16Cmd, strFuncName, arrayByteSend, arrayByteRecv, AddressOf PreProcessCbRecv, i_nTimeout)

            If (bIsRetStatusOK) Then
                If (o_arrayByteData IsNot Nothing) Then
                    o_arrayByteData = arrayByteRecv
                End If
            Else
                If (o_arrayByteData IsNot Nothing) Then
                    o_arrayByteData = Nothing
                End If
            End If
        Catch ex As Exception

        Finally
            If (arrayByteSend IsNot Nothing) Then
                Erase arrayByteSend
            End If
            If (arrayByteRecv IsNot Nothing) Then
                Erase arrayByteRecv
            End If

            m_bIDGCmdBusy = False
        End Try
    End Sub

    '------------------------------------------------------------
    '2017 Nov 21 added, goofy_liu
    '===[Tamper Log Example, 1-3 pins have been triggered]===
    '20:00:30 [TX] - ViVOtech2<NUL>?=<NUL><ETX><SOH><SOH><NUL>_?
    '20:00:30 [RX] - ViVOtech2<NUL>?<NUL><SOH>7<NUL><SOH><NUL><CR>
    '<NUL>11/20/17,17:32:00,TAMPER:050<LF>
    '11/20/17,19:58:01,TAMPER:064<LF>
    '11/20/17,19:58:01,TAMPER:052<LF>
    '11/20/17,19:58:35,TAMPER:064<LF>
    '11/20/17,19:58:35,TAMPER:052<LF>
    '11/20/17,19:58:36,TAMPER:064<LF>
    '11/20/17,19:58:36,TAMPER:052<LF>
    '11/20/17,19:59:02,TAMPER:064<LF>
    '11/20/17,19:59:18,TAMPER:064<LF>
    '11/20/17,19:59:18,TAMPER:052<LF>
    '11/20/17,19:59:52,TAMPER:064<LF>
    '11/20/17,19:59:52,TAMPER:052<LF>
    '11/20/17,20:00:00,TAMPER:064<LF>
    'Note : Output List = (0,1,2,3...); 0-based index
    'Ex: o_listTamperIndicator = { 1, 3,5 } --> Tamper 1;3;5  are ON
    'Ref Doc:https://atlassian.idtechproducts.com/confluence/download/attachments/29820809/NEOII%20%20secured%20log%20file%20format%20definition.doc?version=1&modificationDate=1511247645719&api=v2
    'Note 01 - This is similar to legacy "Get Tamper Status" function
    ' SystemSecurity_Tamper_StsGetBitmap() 's parameter.
    'Note 02 - Even same tamper triggered but w/ multiple log info

    Public Overridable Sub SystemSecurity_Tamper_NEO20_UMFG_SMFG_xC73D_GetLog(ByRef o_listTamperRdr As List(Of Byte), ByRef o_strTamperLog As String, Optional ByVal nTimeout As Integer = ClassDevRS232.m_cnTX_TIMEOUT_IN_SEC_60)
        Dim u16Cmd As UInt16 = &HC73D
        Dim strFuncName As String = "SystemSecurity_Tamper_NEO20_UMFG_SMFG_xC73D_GetLog"
        Dim aryDatTx As Byte() = New Byte() {&H1, &H1, &H0}
        Dim aryDatRx As Byte() = Nothing

        If (m_bIDGCmdBusy) Then
            Return
        End If
        m_bIDGCmdBusy = True

        Try
            'Init Section
            o_strTamperLog = ""

            '-----------------------------------------------------------------[ Send to reader ]-----------------------------------------------------------------
            System_Customized_Command(u16Cmd, strFuncName, aryDatTx, aryDatRx, AddressOf ClassReaderProtocolBasic.PreProcessCbRecv, nTimeout)

            If (bIsRetStatusOK) Then
                'Translate to ASCII form
                If (aryDatRx IsNot Nothing) Then
                    If (aryDatRx.Length > 0) Then
                        ClassTableListMaster.ConvertFromHexToAscii(aryDatRx, o_strTamperLog)
                        '
                        o_listTamperRdr = SystemSecurity_Tamper_NEO20_UMFG_SMFG_GetLog_01_ParseTamperLogs(o_strTamperLog)
                    End If
                End If
            Else

            End If

        Catch ext As TimeoutException
            LogLn("[Err][Tx/Rx Timeout] [IDG: " + GetIDGCmdString(u16Cmd) + "] " + strFuncName + "() = " + ext.Message)
        Catch ex As Exception
            LogLn("[Err] [IDG:  " + GetIDGCmdString(u16Cmd) + "] " + strFuncName + "() = " + ex.Message)
        Finally
            m_tagIDGCmdDataInfo.Cleanup()
            m_bIDGCmdBusy = False
            Erase aryDatTx
            '
        End Try
    End Sub
    '
    Public Overridable Sub SystemSecurity_Tamper_NEO20_UMFG_SMFG_xC73A_GetDRS(ByRef o_listTamperRdr As List(Of Byte), ByRef o_strTamperLog As String, Optional ByVal nTimeout As Integer = ClassDevRS232.m_cnTX_TIMEOUT_IN_SEC_60)
        'Starndard Steps
        Dim u16Cmd As UInt16 = &HC73A
        Dim strFuncName As String = "SystemSecurity_Tamper_NEO20_UMFG_SMFG_xC73A_GetDRS"
        Dim aryDatTx As Byte() = Nothing

        Dim aryDatRx As Byte() = Nothing
        If (m_bIDGCmdBusy) Then
            Return
        End If
        m_bIDGCmdBusy = True

        Try
            'Init Section
            o_strTamperLog = ""

            '-----------------------------------------------------------------[ Send to reader ]-----------------------------------------------------------------
            System_Customized_Command(u16Cmd, strFuncName, aryDatTx, aryDatRx, AddressOf ClassReaderProtocolBasic.PreProcessCbRecv, nTimeout)

            If (bIsRetStatusOK) Then
                'Translate to ASCII form
                If (aryDatRx IsNot Nothing) Then
                    If (aryDatRx.Length > 0) Then
                        '
                        'ClassTableListMaster.ConvertFromHexToAscii(aryDatRx, o_strTamperLog)
                        o_strTamperLog = SystemSecurity_Tamper_NEO20_UMFG_SMFG_GetLog_02_ConvertFrom_GetDRSxC73A_To_GetLogsxC73D(aryDatRx)
                        'Please refer to C7-3D Get Tamper Log Command Response Steps.
                        o_listTamperRdr = SystemSecurity_Tamper_NEO20_UMFG_SMFG_GetLog_01_ParseTamperLogs(o_strTamperLog)
                    End If
                End If
            Else

            End If

        Catch ext As TimeoutException
            LogLn("[Err][Tx/Rx Timeout] [IDG: " + GetIDGCmdString(u16Cmd) + "] " + strFuncName + "() = " + ext.Message)
        Catch ex As Exception
            LogLn("[Err] [IDG:  " + GetIDGCmdString(u16Cmd) + "] " + strFuncName + "() = " + ex.Message)
        Finally
            m_tagIDGCmdDataInfo.Cleanup()
            m_bIDGCmdBusy = False
            Erase aryDatTx
            '
        End Try
    End Sub
    '
    'Note 01 - Tamper Standard definition in bits.
    ' Generic Toggled Tamper Info --> "TAMPER:052"
    ' Tamper ACK --> "TAMPER:053"
    'B0,
    ' b0 = 0-2; --> 2 --> 2+61 = 63 --> "TAMPER:063"
    ' b1 = 1-3; --> 3 --> 3+61 = 64 --> "TAMPER:064"
    ' b2=  4-6; --> 6 --> 6+61 = 67 --> "TAMPER:067"
    ' b3=  5-7: --> 7 --> 7+61 = 68 --> "TAMPER:068"
    ' b4 = Voltage;                 --> ":056"
    ' b5 = Temperature;             --> ":058"
    ' b6 = Clock;                   --> ":057"
    'B1,
    ' b0 -> TM4 Physical Tamper detected
    ' b1 -> TM4 Voltage Tamper detected
    ' b2 -> TM4 Temperature Tamper detected
    ' b3 -> TM4 Battery Tamper detected
    ' Return: {0,1,2,3,4,200,201}
    '===============
    '2018 Jan 19 updated. 069 ~ 073 ? no definitions
    '
    'Message List = s_strLstTamperPair_VP6300_VendiPro 
    Private Function SystemSecurity_Tamper_NEO20_UMFG_SMFG_GetLog_01_ParseTamperLogs(o_strTamperLog As String) As List(Of Byte)
        Static s_listKeyWordsTamperId As List(Of KeyValuePair(Of String, Byte)) = New List(Of KeyValuePair(Of String, Byte)) From {
            New KeyValuePair(Of String, Byte)(":052", 200), New KeyValuePair(Of String, Byte)(":053", 201) _
            , New KeyValuePair(Of String, Byte)(":054", 0) _
            , New KeyValuePair(Of String, Byte)(":055", 1) _
            , New KeyValuePair(Of String, Byte)(":056", 2) _
            , New KeyValuePair(Of String, Byte)(":057", 3) _
            , New KeyValuePair(Of String, Byte)(":058", 4) _
            , New KeyValuePair(Of String, Byte)(":059", 5) _
            , New KeyValuePair(Of String, Byte)(":060", 6) _
            , New KeyValuePair(Of String, Byte)(":061", 7) _
            , New KeyValuePair(Of String, Byte)(":062", 8) _
            , New KeyValuePair(Of String, Byte)(":063", 7) _
            , New KeyValuePair(Of String, Byte)(":064", 8) _
            , New KeyValuePair(Of String, Byte)(":065", 9) _
            , New KeyValuePair(Of String, Byte)(":066", 10) _
            , New KeyValuePair(Of String, Byte)(":067", 9) _
            , New KeyValuePair(Of String, Byte)(":068", 10) _
            , New KeyValuePair(Of String, Byte)(":069", 11) _
            , New KeyValuePair(Of String, Byte)(":070", 12) _
            , New KeyValuePair(Of String, Byte)(":071", 13) _
            , New KeyValuePair(Of String, Byte)(":072", 14) _
    }

        Dim listTamperSts As List(Of Byte) = New List(Of Byte)()
        For Each itrKeyVal As KeyValuePair(Of String, Byte) In s_listKeyWordsTamperId
            If (o_strTamperLog.Contains(itrKeyVal.Key)) Then
                listTamperSts.Add(itrKeyVal.Value)
                '
            End If
        Next
        '
        Return listTamperSts
    End Function

    '==============[ End of Tamper Commands ]===========
    Protected Overridable Sub SystemSecurity_NEO20_UMFG_x0422_RemovalSensor_Funcs(ByVal byteCmd As Byte, ByRef byteRetSts As Byte, Optional ByVal nTimeoutInSec As Integer = ClassDevRS232.m_cnTX_TIMEOUT_IN_SEC)
        Dim u16Cmd As UInt16 = &H422
        Dim strFuncName As String = "SystemSecurity_NEO20_UMFG_x0422_RemovalSensor_Funcs"
        Dim aryDatTx As Byte() = New Byte() {byteCmd}
        Dim aryDatRx As Byte() = Nothing

        If (m_bIDGCmdBusy) Then
            Return
        End If
        m_bIDGCmdBusy = True

        Try
            '-----------------------------------------------------------------[ Send to reader ]-----------------------------------------------------------------
            System_Customized_Command(u16Cmd, strFuncName, aryDatTx, aryDatRx, AddressOf ClassReaderProtocolBasic.PreProcessCbRecv, nTimeoutInSec)

            If (bIsRetStatusOK) Then
                If (m_tagIDGCmdDataInfo.bIsRawData) Then
                    m_tagIDGCmdDataInfo.ParserDataGetByte(byteRetSts, 0)
                End If
            Else

            End If

        Catch ext As TimeoutException
            LogLn("[Err][Tx/Rx Timeout] [IDG: " + GetIDGCmdString(u16Cmd) + "] " + strFuncName + "() = " + ext.Message)
        Catch ex As Exception
            LogLn("[Err] [IDG:  " + GetIDGCmdString(u16Cmd) + "] " + strFuncName + "() = " + ex.Message)
        Finally
            m_tagIDGCmdDataInfo.Cleanup()
            m_bIDGCmdBusy = False
            Erase aryDatTx
            '
        End Try
    End Sub

    'Ref Doc = NEO 2.0 Rev 129 or later
    ' Cmd = 81-0C, Available Key Slot index.
    'Output - o_BytaryByteKeyInfo_2B_list = {Key Index, Key Block}
    ' Slot 2: RKI-KEK (NSRED and SRED device support, use in Remote Key Injection)
    'Slot 3: MAC DUKPT Key (SRED device support, for future use)
    'Slot 5: Data encryption Key (NSRED and SRED device support, use to encrypt transaction output sensitive data)
    'NEO 20 Response Format + Example
    'Key Index(1B)+Block(1B) = 2 bytes
    '14 00 02 00 0A 00
    'Output = 14 00 02 00 0A 00
    Public Overridable Sub SystemSecurity_NEO20_SMFG_KeyInfo_x810C(ByRef o_BytaryByteKeyInfo_2B_list As Byte(), Optional ByVal nTimeoutInSec As Integer = ClassDevRS232.m_cnTX_TIMEOUT_IN_SEC)
        Dim u16Cmd As UInt16 = &H810C
        Dim strFuncName As String = "SystemSecurity_NEO20_KeyInfo_x810C"
        Dim aryDatTx As Byte() = Nothing
        Dim aryDatRx As Byte() = Nothing

        If (m_bIDGCmdBusy) Then
            Return
        End If
        m_bIDGCmdBusy = True

        Try
            o_BytaryByteKeyInfo_2B_list = Nothing
            '-----------------------------------------------------------------[ Send to reader ]-----------------------------------------------------------------
            System_Customized_Command(u16Cmd, strFuncName, aryDatTx, aryDatRx, AddressOf ClassReaderProtocolBasic.PreProcessCbRecv, nTimeoutInSec)

            If (bIsRetStatusOK) Then
                If (m_tagIDGCmdDataInfo.bIsRawData) Then
                    o_BytaryByteKeyInfo_2B_list = aryDatRx.Clone()
                    'm_tagIDGCmdDataInfo.ParserDataGetByte(byteRetSts, 0)
                End If
            Else

                'o_BytaryByteKeyInfo_2B_list = New Byte() {&H1, &H2, &H2, &H3, &H3, &H5}
            End If

        Catch ext As TimeoutException
            LogLn("[Err][Tx/Rx Timeout] [IDG: " + GetIDGCmdString(u16Cmd) + "] " + strFuncName + "() = " + ext.Message)
        Catch ex As Exception
            LogLn("[Err] [IDG:  " + GetIDGCmdString(u16Cmd) + "] " + strFuncName + "() = " + ex.Message)
        Finally
            m_tagIDGCmdDataInfo.Cleanup()
            m_bIDGCmdBusy = False
            If (aryDatTx IsNot Nothing) Then
                Erase aryDatTx
            End If
            If (aryDatRx IsNot Nothing) Then
                Erase aryDatRx
            End If

            '
        End Try
    End Sub
    '====[ 2017 Nov 23 added, goofy_liu ]=====
    Public Sub SystemSecurity_NEO20_SMFG_x1200_Start_PCI_Task(Optional nTimeoutInSec As Integer = ClassDevRS232.m_cnTX_TIMEOUT_IN_SEC)
        SystemSecurity_NEO20_SMFG_x1200_PCI_Task_Generic(&H0, nTimeoutInSec)
    End Sub

    Public Sub SystemSecurity_NEO20_SMFG_x1200_End_PCI_Task(Optional nTimeoutInSec As Integer = ClassDevRS232.m_cnTX_TIMEOUT_IN_SEC)
        SystemSecurity_NEO20_SMFG_x1200_PCI_Task_Generic(&H1, nTimeoutInSec)
    End Sub

    'Param byteMode = 0x00 : Start PCI Command Task
    '               = 0x01 : End PCI Command Task
    Private Sub SystemSecurity_NEO20_SMFG_x1200_PCI_Task_Generic(ByVal byteMode As Byte, Optional nTimeoutInSec As Integer = ClassDevRS232.m_cnTX_TIMEOUT_IN_SEC)
        Dim u16Cmd As UInt16 = &H1200
        Dim strFuncName As String = "SystemSecurity_NEO20_SMFG_x1200_PCI_Task_Generic"
        Dim aryDatTx As Byte() = New Byte() {byteMode}
        Dim aryDatRx As Byte() = Nothing

        If (m_bIDGCmdBusy) Then
            Return
        End If
        m_bIDGCmdBusy = True

        Try

            '-----------------------------------------------------------------[ Send to reader ]-----------------------------------------------------------------
            System_Customized_Command(u16Cmd, strFuncName, aryDatTx, aryDatRx, AddressOf ClassReaderProtocolBasic.PreProcessCbRecv, nTimeoutInSec)

            If (bIsRetStatusOK) Then
                'See FWVP5300-83 bug comment
                Thread.Sleep(m_s_nDelayLatency_NEO20_Cmd_9007_or_1200_ms)
            Else

            End If

        Catch ext As TimeoutException
            LogLn("[Err][Tx/Rx Timeout] [IDG: " + GetIDGCmdString(u16Cmd) + "] " + strFuncName + "() = " + ext.Message)
        Catch ex As Exception
            LogLn("[Err] [IDG:  " + GetIDGCmdString(u16Cmd) + "] " + strFuncName + "() = " + ex.Message)
        Finally
            m_tagIDGCmdDataInfo.Cleanup()
            m_bIDGCmdBusy = False
            Erase aryDatTx
            '
        End Try
    End Sub

    'Public Sub SystemSecurity_NEO20_SMFG_x9100_Master_Reset_with_PKI_WebRequest(ByVal i_strNEO20_SMFG_DevNonce As String, ByVal i_strSerNum As String, ByRef o_strMsgErr As String, Optional nTimeoutInSec As Integer = ClassDevRS232.m_cnTX_TIMEOUT_IN_SEC)
    '    Dim u16Cmd As UInt16 = &H9100
    '    Dim strFuncName As String = "SystemSecurity_NEO20_SMFG_x9100_Master_Reset"
    '    Dim aryDatTx As Byte() = Nothing
    '    Dim aryDatRx As Byte() = Nothing


    '    'PKI Web Service to Get Sign1 Device(Dev Nonce, Cmd, SubCmd, Serial Num)
    '    Dim strTmp As String = ""
    '    Dim nPkiCode As RETURN_CODE = RETURN_CODE.EMV_APP_NO_SUPPORT_OR_FW_DATA_ERROR
    '    Dim aryByteTx_RSA_Sign1_Device As Byte() = Nothing
    '    Dim aryByteTx_RSA_Sign1_Device_Tmp As Byte() = Nothing
    '    Dim nTmpLen As Integer
    '    Dim aryDataCmdMain As Byte() = New Byte() {&H91}
    '    Dim aryDataCmdSub As Byte() = New Byte() {&H0}



    '    If (m_bIDGCmdBusy) Then
    '        Return
    '    End If
    '    m_bIDGCmdBusy = True

    '    Try
    '        'To make sure response code <> OK if PKI Web Service Communication Failed. For caller validation.
    '        m_tagIDGCmdDataInfo.Reinit(u16Cmd, m_arraybyteSendBuf, Me, , m_arraybyteSendBufUnknown)

    '        If (i_strNEO20_SMFG_DevNonce Is Nothing) Then
    '            Throw New Exception("Invalid Device Nonce...")
    '        End If
    '        'Init Section, Prepare
    '        ClassTableListMaster.TLVauleFromString2ByteArray(i_strNEO20_SMFG_DevNonce, aryByteTx_RSA_Sign1_Device)
    '        If (aryByteTx_RSA_Sign1_Device Is Nothing) Then
    '            Throw New Exception("Invalid Device Nonce Value")
    '        End If

    '        '-----------------------------------------------------------------[ Send to reader ]-----------------------------------------------------------------
    '        'Step 1/3 : Get Sign Hash from PKI Server
    '        nPkiCode = API.pki_SignMFGCommand(aryByteTx_RSA_Sign1_Device, aryDataCmdMain, aryDataCmdSub, i_strSerNum, Nothing, strTmp)
    '        'Response strTmp = <RSA - Sign1 Device from PKI Web Service>
    '        If (RETURN_CODE.RETURN_CODE_DO_SUCCESS <> nPkiCode) Then
    '            o_strMsgErr = "Invalid response. PKI Web response = " + nPkiCode.ToString()
    '            Throw New Exception(o_strMsgErr)
    '        End If

    '        'Step 2/3 : Prepare Master Reset Tx Data 
    '        ClassTableListMaster.TLVauleFromString2ByteArray(strTmp, aryByteTx_RSA_Sign1_Device_Tmp)
    '        If (aryByteTx_RSA_Sign1_Device_Tmp Is Nothing) Then
    '            o_strMsgErr = "Invalid RSA Sign1 of the Device Data. Data = " + strTmp
    '            Throw New Exception(o_strMsgErr)
    '        End If

    '        nTmpLen = aryByteTx_RSA_Sign1_Device_Tmp.Length
    '        'Tx Data = <Len 2 Bytes><RSA - Sign1 Device from PKI Web Service>
    '        aryByteTx_RSA_Sign1_Device = New Byte(nTmpLen + 1) {}
    '        Array.Copy(aryByteTx_RSA_Sign1_Device_Tmp, 0, aryByteTx_RSA_Sign1_Device, 2, nTmpLen)
    '        aryByteTx_RSA_Sign1_Device(0) = CType(nTmpLen \ 256, Byte)
    '        aryByteTx_RSA_Sign1_Device(1) = CType(nTmpLen Mod 256, Byte)
    '        aryDatTx = aryByteTx_RSA_Sign1_Device
    '        'Step 3/3 : Master Reset reader
    '        System_Customized_Command(u16Cmd, strFuncName, aryDatTx, aryDatRx, AddressOf ClassReaderProtocolBasic.PreProcessCbRecv, nTimeoutInSec)

    '        If (bIsRetStatusOK) Then

    '        Else

    '        End If

    '    Catch ext As TimeoutException
    '        LogLn("[Err][Tx/Rx Timeout] [IDG: " + GetIDGCmdString(u16Cmd) + "] " + strFuncName + "() = " + ext.Message)
    '    Catch ex As Exception
    '        LogLn("[Err] [IDG:  " + GetIDGCmdString(u16Cmd) + "] " + strFuncName + "() = " + ex.Message)
    '    Finally
    '        m_tagIDGCmdDataInfo.Cleanup()
    '        m_bIDGCmdBusy = False

    '        If (aryByteTx_RSA_Sign1_Device Is Nothing) Then
    '            Erase aryByteTx_RSA_Sign1_Device
    '        End If
    '        If (aryByteTx_RSA_Sign1_Device_Tmp Is Nothing) Then
    '            Erase aryByteTx_RSA_Sign1_Device_Tmp
    '        End If
    '        If (aryDataCmdMain Is Nothing) Then
    '            Erase aryDataCmdMain
    '        End If
    '        If (aryDataCmdSub Is Nothing) Then
    '            Erase aryDataCmdSub
    '        End If


    '        '
    '    End Try
    'End Sub
    'byteMode = &H0 ==> Device Nonce Get
    Public Sub SystemSecurity_NEO20_SMFG_x9108_DevNonce_Get(ByRef o_strNEO20_SMFG_DevNonce As String, Optional ByVal byteMode As Byte = &H0, Optional nTimeoutInSec As Integer = ClassDevRS232.m_cnTX_TIMEOUT_IN_SEC)
        Dim u16Cmd As UInt16 = &H9108
        Dim strFuncName As String = "SystemSecurity_NEO20_SMFG_x9108_DevNonce_Get"
        Dim aryDatTx As Byte() = New Byte() {byteMode}
        Dim aryDatRx As Byte() = Nothing

        If (m_bIDGCmdBusy) Then
            Return
        End If
        m_bIDGCmdBusy = True

        Try
            'Init Section, Prepare
            o_strNEO20_SMFG_DevNonce = ""
            '-----------------------------------------------------------------[ Send to reader ]-----------------------------------------------------------------

            System_Customized_Command(u16Cmd, strFuncName, aryDatTx, aryDatRx, AddressOf ClassReaderProtocolBasic.PreProcessCbRecv, nTimeoutInSec)

            If (bIsRetStatusOK) Then
                'Translate to ASCII form
                If (aryDatRx IsNot Nothing) Then
                    If (aryDatRx.Length > 0) Then
                        ClassTableListMaster.ConvertFromArrayByte2String(aryDatRx, o_strNEO20_SMFG_DevNonce, False)
                    End If
                End If
            Else

            End If

        Catch ext As TimeoutException
            LogLn("[Err][Tx/Rx Timeout] [IDG: " + GetIDGCmdString(u16Cmd) + "] " + strFuncName + "() = " + ext.Message)
        Catch ex As Exception
            LogLn("[Err] [IDG:  " + GetIDGCmdString(u16Cmd) + "] " + strFuncName + "() = " + ex.Message)
        Finally
            m_tagIDGCmdDataInfo.Cleanup()
            m_bIDGCmdBusy = False
            Erase aryDatTx
            '
        End Try
    End Sub

    Public Sub SystemSecurity_NEO20_UMFG_x9003_SignedCert_Injection(ByVal i_itrStrCert As String, Optional nTimeoutInSec As Integer = ClassDevRS232.m_cnTX_TIMEOUT_IN_SEC)
        Dim u16Cmd As UInt16 = &H9003
        Dim strFuncName As String = "SystemSecurity_NEO20_UMFG_x9003_SignedCert_Injection"
        Dim aryDatTx As Byte() = Nothing
        Dim aryDatRx As Byte() = Nothing

        If (m_bIDGCmdBusy) Then
            Return
        End If
        m_bIDGCmdBusy = True

        Try
            'Init Section
            ClassTableListMaster.TLVauleFromString2ByteArray(i_itrStrCert, aryDatTx)

            '-----------------------------------------------------------------[ Send to reader ]-----------------------------------------------------------------
            System_Customized_Command(u16Cmd, strFuncName, aryDatTx, aryDatRx, AddressOf ClassReaderProtocolBasic.PreProcessCbRecv, nTimeoutInSec)

            If (bIsRetStatusOK) Then

            Else

            End If

        Catch ext As TimeoutException
            LogLn("[Err][Tx/Rx Timeout] [IDG: " + GetIDGCmdString(u16Cmd) + "] " + strFuncName + "() = " + ext.Message)
        Catch ex As Exception
            LogLn("[Err] [IDG:  " + GetIDGCmdString(u16Cmd) + "] " + strFuncName + "() = " + ex.Message)
        Finally
            m_tagIDGCmdDataInfo.Cleanup()
            m_bIDGCmdBusy = False
            Erase aryDatTx
            '
        End Try
    End Sub

    Sub SystemSecurity_NEO20_UMFG_x9004_ValidateCertTree(Optional nTimeoutInSec As Integer = ClassDevRS232.m_cnTX_TIMEOUT_IN_SEC)
        Dim u16Cmd As UInt16 = &H9004
        Dim strFuncName As String = "SystemSecurity_NEO20_UMFG_x9004_ValidateCertTree"
        Dim aryDatTx As Byte() = Nothing
        Dim aryDatRx As Byte() = Nothing

        If (m_bIDGCmdBusy) Then
            Return
        End If
        m_bIDGCmdBusy = True

        Try
            '-----------------------------------------------------------------[ Send to reader ]-----------------------------------------------------------------
            System_Customized_Command(u16Cmd, strFuncName, aryDatTx, aryDatRx, AddressOf ClassReaderProtocolBasic.PreProcessCbRecv, nTimeoutInSec)

            If (bIsRetStatusOK) Then

            Else

            End If

        Catch ext As TimeoutException
            LogLn("[Err][Tx/Rx Timeout] [IDG: " + GetIDGCmdString(u16Cmd) + "] " + strFuncName + "() = " + ext.Message)
        Catch ex As Exception
            LogLn("[Err] [IDG:  " + GetIDGCmdString(u16Cmd) + "] " + strFuncName + "() = " + ex.Message)
        Finally
            m_tagIDGCmdDataInfo.Cleanup()
            m_bIDGCmdBusy = False
            Erase aryDatTx
            '
        End Try
    End Sub

    Sub SystemSecurity_NEO20_UMFG_x9005_Lock_To_SMFG(Optional nTimeoutInSec As Integer = ClassDevRS232.m_cnTX_TIMEOUT_IN_SEC)
        Dim u16Cmd As UInt16 = &H9005
        Dim strFuncName As String = "SystemSecurity_NEO20_UMFG_x9005_Lock_To_SMFG"
        Dim aryDatTx As Byte() = Nothing
        Dim aryDatRx As Byte() = Nothing

        If (m_bIDGCmdBusy) Then
            Return
        End If
        m_bIDGCmdBusy = True

        Try
            '-----------------------------------------------------------------[ Send to reader ]-----------------------------------------------------------------
            System_Customized_Command(u16Cmd, strFuncName, aryDatTx, aryDatRx, AddressOf ClassReaderProtocolBasic.PreProcessCbRecv, nTimeoutInSec)

            If (bIsRetStatusOK) Then

            Else

            End If

        Catch ext As TimeoutException
            LogLn("[Err][Tx/Rx Timeout] [IDG: " + GetIDGCmdString(u16Cmd) + "] " + strFuncName + "() = " + ext.Message)
        Catch ex As Exception
            LogLn("[Err] [IDG:  " + GetIDGCmdString(u16Cmd) + "] " + strFuncName + "() = " + ex.Message)
        Finally
            m_tagIDGCmdDataInfo.Cleanup()
            m_bIDGCmdBusy = False
            Erase aryDatTx
            '
        End Try
    End Sub

    '2017 Dec 08, goofy_liu
    'Return True = Close --> Sts Code = &H0 (No Armed) or &H2 (Armed)
    'Return False = Open --> Sts Code = &H1
    Public Function SystemSecurity_NEO20_UMFG_x0422_RemovalSensorSts_Get(ByRef o_strRetMsg As String, Optional ByVal nTimeoutInSec As Integer = ClassDevRS232.m_cnTX_TIMEOUT_IN_SEC) As Boolean
        Return SystemSecurity_NEO20_UMFG_x0422_RemovalSensorSts_Get(&H2, o_strRetMsg, nTimeoutInSec)
    End Function

    'True = Pushed, Close   --> Sts Code = &H0
    'False = Released, Open --> Sts Code = &H1
    Public Function SystemSecurity_NEO20_UMFG_x0422_RemovalSensorSts_Get_Deprecated_VP6300(ByRef o_strRetMsg As String, Optional ByVal nTimeoutInSec As Integer = ClassDevRS232.m_cnTX_TIMEOUT_IN_SEC) As Boolean
        Return SystemSecurity_NEO20_UMFG_x0422_RemovalSensorSts_Get(&H0, o_strRetMsg, nTimeoutInSec)
    End Function


    '2017 Dec 08, goofy_liu
    'Kevin Vo Removal Sensor Functions
    '[IN] io_byteRetSts
    'a) &H00 = Wanna Check Close Status, if returned value is matched --> return True
    'b) &H01 = Wanna Check Open Status, if returned value is matched --> return False

    '[OUT] io_byteRetSts
    ' &H00 = Not Armed
    ' &H01 = Armed & Triggered (Open)
    '2018 May 28 , goofy_liu
    ' &H02 = Armed but No triggered (Pressed)

    'Return True / False : Please see [IN] io_byteRetSts notes.
    'Always excute command in the sequence : a) --> b)
    Public Function SystemSecurity_NEO20_UMFG_x0422_RemovalSensorSts_Get(ByRef io_byteRetSts As Byte, ByRef o_strRetMsg As String, Optional ByVal nTimeoutInSec As Integer = ClassDevRS232.m_cnTX_TIMEOUT_IN_SEC) As Boolean
        Dim byteRetStsWanted As Byte = io_byteRetSts

        'Change Value to Error Status
        io_byteRetSts = (&HF - byteRetStsWanted)

        While (True)

            'Init, Enable Removal Sensor
            'a) 01 = Enable , byteRetSts = Don't care value
            SystemSecurity_NEO20_UMFG_x0422_RemovalSensor_Funcs(&H1, io_byteRetSts, nTimeoutInSec)
            If (Not bIsRetStatusOK) Then
                o_strRetMsg = "Err, Enable Tamper "
                Exit While
            End If

            Form01_Main.AppSleep(2000)

            'b) 02 = Get Status, byteRetSts = Returned Removal Sensor Status 
            SystemSecurity_NEO20_UMFG_x0422_RemovalSensor_Funcs(&H2, io_byteRetSts, nTimeoutInSec)
            If (Not bIsRetStatusOK) Then
                o_strRetMsg = "Warning, Get Removal Sensor Status Failed"
                'Exit Sub
            Else
                o_strRetMsg = "Get Status Okay, Sts = " + io_byteRetSts.ToString("X")
            End If

            Form01_Main.AppSleep(2000)
            '

            '
            Exit While
        End While
        '
        Return (byteRetStsWanted = io_byteRetSts)
    End Function

    'Ref Doc : https://atlassian.idtechproducts.com/confluence/download/attachments/50531261/FW%20VP5300%20Transarmor%20Root%20and%20CA%20loading%20Info.msg?version=1&modificationDate=1529566973333&api=v2
    '
    Public Overridable Sub SystemSecurity_NEO20_SMFG_Certs_TransArmor_CACert_Get_xC754(ByVal i_byte_TACert_Indicator_Rootx00_Intermx01_Keyx02 As Byte, ByRef o_strTACert_ASCII As String, Optional ByVal nTimeoutInSec As Integer = ClassDevRS232.m_cnRX_TIMEOUT_IN_SEC)
        Dim u16Cmd As UInt16 = &HC754
        Dim strFuncName As String = "SystemSecurity_NEO20_SMFG_Certs_TransArmor_CACert_Get_xC754"
        Dim aryDatTx As Byte() = Nothing
        Dim aryDatRx As Byte() = Nothing

        If (m_bIDGCmdBusy) Then
            Return
        End If
        m_bIDGCmdBusy = True


        Try
            'Init
            o_strTACert_ASCII = ""
            aryDatTx = New Byte() {i_byte_TACert_Indicator_Rootx00_Intermx01_Keyx02}
            '-----------------------------------------------------------------[ Send to reader ]-----------------------------------------------------------------
            System_Customized_Command(u16Cmd, strFuncName, aryDatTx, aryDatRx, AddressOf ClassReaderProtocolBasic.PreProcessCbRecv, nTimeoutInSec)

            If (bIsRetStatusOK) Then
                If (aryDatRx IsNot Nothing) Then
                    'Translate to ASCII
                    ClassTableListMaster.ConvertFromHexToAscii(aryDatRx, o_strTACert_ASCII, False)
                End If
            Else

            End If

        Catch ext As TimeoutException
            LogLn("[Err][Tx/Rx Timeout] [IDG: " + GetIDGCmdString(u16Cmd) + "] " + strFuncName + "() = " + ext.Message)
        Catch ex As Exception
            LogLn("[Err] [IDG:  " + GetIDGCmdString(u16Cmd) + "] " + strFuncName + "() = " + ex.Message)
        Finally
            m_tagIDGCmdDataInfo.Cleanup()
            m_bIDGCmdBusy = False
            If (aryDatTx IsNot Nothing) Then
                Erase aryDatTx
            End If
            If (aryDatRx IsNot Nothing) Then
                Erase aryDatRx
            End If
            '
        End Try
    End Sub

    '
    ' [Output]
    ' o_strInstalledCertsNum : if size = 1 & (0) = &HFF --> N/A
    ' o_strSerNum : Serial Number
    Public Overridable Sub SystemSecurity_NEO20_SMFG_Certs_Get_x810E(ByRef o_strSerNum As String, ByRef o_strInstalledCertsNum As String, Optional ByRef io_aryByteInstalledCerts As Byte() = Nothing, Optional ByVal nTimeoutInSec As Integer = ClassDevRS232.m_cnRX_TIMEOUT_IN_SEC)
        Dim u16Cmd As UInt16 = &H810E
        Dim strFuncName As String = "SystemSecurity_NEO20_SMFG_Certs_Get_x810E"
        Dim aryDatTx As Byte() = Nothing
        Dim aryDatRx As Byte() = Nothing
        Dim nSNLen As Integer = 0
        Dim nCertNumsInstalled As Integer = 0

        If (m_bIDGCmdBusy) Then
            Return
        End If
        m_bIDGCmdBusy = True

        Try
            'o_nSerNumLen = 0
            o_strSerNum = "N/A"
            o_strInstalledCertsNum = 0
            'o_strInstalledCertsNum = "N/A"
            '-----------------------------------------------------------------[ Send to reader ]-----------------------------------------------------------------
            System_Customized_Command(u16Cmd, strFuncName, aryDatTx, aryDatRx, AddressOf ClassReaderProtocolBasic.PreProcessCbRecv, nTimeoutInSec)

            If (o_strSerNum IsNot Nothing) Then
                o_strSerNum = "N/A"
            End If

            If (bIsRetStatusOK) Then
                '2017 Dec 29, goofy_liu, NEO 2.0
                'Verified in VP5300 0.00.000.0038.S
                '[RX] - 56 69 56 4F 74 65 63 68 32 00 81 00 00 21 0F 35 33 30 5A 30 30 30 30 30 35 00 00 00 00 00 0F 00 0A 0B 0C 0D 0E 14 15 16 17 18 19 1A 1B 1C 1D A2 C5 
                '1st, 0F, 1Byte = Serial Num Length
                'S/N = 35 33 30 5A 30 30 30 30 30 35 = "530Z000005"
                '2nd, 0F 00, 2 Bytes = 15 certs num
                'installed certs ' index = 0A~0E, 14~1D
                'customer cert, index = 0F
                If (m_tagIDGCmdDataInfo.bIsRawData) Then
                    Dim aryByteRx2 As Byte() = Nothing
                    Dim nOffset As Integer = 0
                    'm_tagIDGCmdDataInfo.ParserDataGetArrayRawData(aryByteRx)
                    '1st byte = Serial Num Length
                    nSNLen = aryDatRx(0)
                    nCertNumsInstalled = aryDatRx(nSNLen + 1)
                    If (nCertNumsInstalled > (aryDatRx.Length - (nSNLen + 1))) Then
                        Throw New Exception("Warning, Installed Certs Num = " + nCertNumsInstalled.ToString() + ", Out of Range")
                    End If
                    'S/N
                    nOffset = 1
                    If (nSNLen > 0) Then
                        aryByteRx2 = New Byte(nSNLen - 1) {}
                        Array.Copy(aryDatRx, nOffset, aryByteRx2, 0, nSNLen)
                        ClassTableListMaster.ConvertFromHexToAscii(aryByteRx2, o_strSerNum, True)
                        '
                        If (o_strSerNum IsNot Nothing) Then
                            ClassTableListMaster.ConvertFromHexToAscii(aryByteRx2, o_strSerNum, True)
                        End If
                        '
                        Erase aryByteRx2
                        nOffset = nOffset + nSNLen
                    End If
                    '
                    '                    [Additional Info - NEO 2.0, Certificates, 81-0E]
                    'Tx :  56 69 56 4F 74 65 63 68 32 00 81 0E 00 00 D4 6A.
                    'RX:  56 69 56 4F 74 65 63 68 32 00 81 00 00 1C 0A 36 33 30 5A 30 30 30 30 30 31 10 00 0A 0B 0C 0D 0E 14 15 16 17 18 19 1A 1B 1C 1D E7 4E 
                    '==============================
                    '0A： Length of SN  in Hex 
                    '36 33 30 5A 30 30 30 30 30 31  :  SN (630Z000006 in ASCII)
                    '10 : number of certificates in Hex
                    '00 0A 0B 0C 0D 0E 14 15 16 17 18 19 1A 1B 1C 1D : certificate KeyID(Hex)  list.
                    'certificate Key ID definition:
                    '#define ROOT_CA                                  0x00
                    '#define DEVICE_CA                               0x0A
                    '#define SECURE_CONTENT_CA         0x0B
                    '#define TMS_CA                                    0x0C
                    '#define DATA_KEY_KDH_CA               0x0D
                    '#define PIN_KEY_KDH_CA                  0x0E
                    '#define DEVICE_CERTIFICATE            0x14
                    '#define FW_AUTH_CERTIFICATE        0x15
                    '#define TMS_SEVER_CERTIFICATE      0x18
                    '#define PIN_KDH_AUTH_CERTIFICATE   0x1A
                    '#define PIN_KDH_ENC_CERTIFICATE    0x1D
                    '#define MFG_COMMANDS_CERT          0x16
                    '#define SECURE_MESSAGE_CERT         0x17
                    '#define APPLICATION_AUTH_CERT      0x1B
                    '#define DATA_SKDH_AUTH_CERT         0x19
                    '#define DATA_SKDH_ENC_CERT            0x1C
                    '#define DATA_NSKDH_AUTH_CERT      0x1E
                    '#define DATA_NSKDH_ENC_CERT         0x1F

                    'UInt16 byte length.
                    nOffset = nOffset + 1
                    If (nCertNumsInstalled > 0) Then
                        aryByteRx2 = New Byte(nCertNumsInstalled - 1) {}
                        Array.Copy(aryDatRx, nOffset, aryByteRx2, 0, nCertNumsInstalled)
                        'Copy to output if being required
                        If (io_aryByteInstalledCerts IsNot Nothing) Then
                            Erase io_aryByteInstalledCerts
                        End If
                        '-----------------
                        io_aryByteInstalledCerts = New Byte(nCertNumsInstalled - 1) {}
                        Array.Copy(aryByteRx2, 0, io_aryByteInstalledCerts, 0, nCertNumsInstalled)
                        '
                        ClassTableListMaster.ConvertFromArrayByte2String(aryByteRx2, o_strInstalledCertsNum, True)
                        nOffset = nOffset + aryByteRx2.Length
                        '
                        Erase aryByteRx2
                    Else
                        'No Installed Cert Info. Set to 0xFF as N/A status
                        If (io_aryByteInstalledCerts IsNot Nothing) Then
                            Erase io_aryByteInstalledCerts
                        End If
                        '-----------------
                        io_aryByteInstalledCerts = New Byte(0) {}
                        io_aryByteInstalledCerts(0) = &HFF
                    End If
                    '
                End If
            End If

        Catch ext As TimeoutException
            LogLn("[Err][Tx/Rx Timeout] [IDG: " + GetIDGCmdString(u16Cmd) + "] " + strFuncName + "() = " + ext.Message)
        Catch ex As Exception
            LogLn("[Err] [IDG:  " + GetIDGCmdString(u16Cmd) + "] " + strFuncName + "() = " + ex.Message)
        Finally
            m_tagIDGCmdDataInfo.Cleanup()
            m_bIDGCmdBusy = False
            If (aryDatTx IsNot Nothing) Then
                Erase aryDatTx
            End If
            If (aryDatRx IsNot Nothing) Then
                Erase aryDatRx
            End If

            '
        End Try

    End Sub
    '
    'Example
    ' i_strKeySlot = "02 00"
    ' i_strByteIndicator = "00", default
    ' o_strTmp = "01 0A EF FF 98 76 54 32 10 00 00 01", Len = 0x0C
    Public Overridable Sub SystemSecurity_NEO20_SMFG_KSN_Get_x810B(ByVal i_strKeySlot As String, Optional ByVal i_strByteIndicator As String = "00", Optional ByRef o_strTmp As String = Nothing, Optional ByVal nTimeoutInSec As Integer = ClassDevRS232.m_cnRX_TIMEOUT_IN_SEC)
        Dim u16Cmd As UInt16 = &H810B
        Dim strFuncName As String = "SystemSecurity_NEO20_SMFG_KSN_Get_x810B"
        Dim aryDatTx As Byte() = Nothing
        Dim aryDatRx As Byte() = Nothing
        Dim aryDatTmp1 As Byte() = Nothing
        Dim aryDatTmp2 As Byte() = Nothing

        If (m_bIDGCmdBusy) Then
            Return
        End If
        m_bIDGCmdBusy = True

        Try
            If (o_strTmp IsNot Nothing) Then
                o_strTmp = "N/A"
            End If

            If (i_strKeySlot Is Nothing) Then
                i_strKeySlot = "02 00"
            End If
            If (i_strByteIndicator Is Nothing) Then
                i_strByteIndicator = "00"
            End If
            ClassTableListMaster.TLVauleFromString2ByteArray(i_strKeySlot, aryDatTmp1)
            ClassTableListMaster.TLVauleFromString2ByteArray(i_strByteIndicator, aryDatTmp2)
            aryDatTx = New Byte(aryDatTmp1.Length + aryDatTmp2.Length - 1) {}
            Array.Copy(aryDatTmp1, 0, aryDatTx, 0, aryDatTmp1.Length)
            Array.Copy(aryDatTmp2, 0, aryDatTx, aryDatTmp1.Length, aryDatTmp2.Length)
            '-----------------------------------------------------------------[ Send to reader ]-----------------------------------------------------------------
            System_Customized_Command(u16Cmd, strFuncName, aryDatTx, aryDatRx, AddressOf ClassReaderProtocolBasic.PreProcessCbRecv, nTimeoutInSec)

            If (bIsRetStatusOK) Then
                'If (m_tagIDGCmdDataInfo.bIsRawData) Then
                '    Dim aryByteRx2 As Byte() = Nothing
                '    m_tagIDGCmdDataInfo.ParserDataGetArrayRawData(aryByteRx2)
                '    'Ex: 56 69 56 4F 74 65 63 68 32 00 81 00 00 01 FF 6E 62 => "FF"

                '    Erase aryByteRx2
                'End If
                If (o_strTmp IsNot Nothing) Then
                    ClassTableListMaster.ConvertFromArrayByte2String(aryDatRx, o_strTmp, True)
                End If
            Else

                'o_BytaryByteKeyInfo_2B_list = New Byte() {&H1, &H2, &H2, &H3, &H3, &H5}
            End If

        Catch ext As TimeoutException
            LogLn("[Err][Tx/Rx Timeout] [IDG: " + GetIDGCmdString(u16Cmd) + "] " + strFuncName + "() = " + ext.Message)
        Catch ex As Exception
            LogLn("[Err] [IDG:  " + GetIDGCmdString(u16Cmd) + "] " + strFuncName + "() = " + ex.Message)
        Finally
            m_tagIDGCmdDataInfo.Cleanup()
            m_bIDGCmdBusy = False
            If (aryDatTx IsNot Nothing) Then
                Erase aryDatTx
            End If
            If (aryDatRx IsNot Nothing) Then
                Erase aryDatRx
            End If

            '
        End Try

    End Sub

    'Rx Resp / Out : byte 00 = MSR Card Type, 01 = Track Status
    Public Sub SystemSecurity_NEO20_SMFG_SwipeMSR_x2C20(ByRef o_byte00_MsrCardType As Byte, ByRef o_byte01_MsrCardTrackStatus As Byte, Optional ByVal nTimeoutInSec As Integer = ClassDevRS232.m_cnRX_TIMEOUT_IN_SEC)
        '
        Dim u16Cmd As UInt16 = &H2C20
        Dim strFuncName As String = "SystemSecurity_NEO20_SMFG_SwipeMSR_x2C20"
        Dim aryDatTx As Byte() = Nothing
        Dim aryDatRx As Byte() = Nothing
        Dim aryDatTmp1 As Byte() = Nothing
        Dim aryDatTmp2 As Byte() = Nothing

        If (m_bIDGCmdBusy) Then
            Return
        End If
        m_bIDGCmdBusy = True

        Try
            'Tx Data = Null
            '-----------------------------------------------------------------[ Send to reader ]-----------------------------------------------------------------
            System_Customized_Command(u16Cmd, strFuncName, aryDatTx, aryDatRx, AddressOf ClassReaderProtocolBasic.PreProcessCbRecv, nTimeoutInSec)

            If (bIsRetStatusOK) Then

                'Check Byte Len = 2
                If (aryDatRx Is Nothing) Then
                    Throw New Exception("No Resp Data")
                End If
                If (aryDatRx.Length < 2) Then
                    Throw New Exception("Resp Data < 2 bytes")
                End If

                'Byte 00
                '80: ISO 7813/ISO 4909/ABA format
                '81: AAMVA Format
                '83: Other
                o_byte00_MsrCardType = aryDatRx(0)

                'Byte 01
                o_byte01_MsrCardTrackStatus = aryDatRx(1)
            Else

            End If

        Catch ext As TimeoutException
            LogLn("[Err][Tx/Rx Timeout] [IDG: " + GetIDGCmdString(u16Cmd) + "] " + strFuncName + "() = " + ext.Message)
        Catch ex As Exception
            LogLn("[Err] [IDG:  " + GetIDGCmdString(u16Cmd) + "] " + strFuncName + "() = " + ex.Message)
        Finally
            m_tagIDGCmdDataInfo.Cleanup()
            m_bIDGCmdBusy = False
            If (aryDatTx IsNot Nothing) Then
                Erase aryDatTx
            End If
            If (aryDatRx IsNot Nothing) Then
                Erase aryDatRx
            End If

            '
        End Try
    End Sub

    '2018 July 27, goofy_liu updated
    'Table - 1000
    '==[ Input, byte array style ]==
    'Note: Msg Idx = Idx of s_strLstTamperPair_VP6300_VendiPro.Reader
    '<Src Id><Val Len><Vals>
    'Ex: 10 01 00 ==> 0x10 = Battery, Val Len = 1 byte, Value = 0x00
    '
    '00 01 00 = K81 Self check Error : Msg Idx = 14; Tamper Log = "TAMPER:902"
    '01 01 00 = TM4 Check Value Error : Msg Idx = 12; Tamper Log = "TAMPER:900"
    '02 01 FF = NA(Reserved 1), 0xFF = RFU : Msg Idx = N/A, X
    '10 01 00 = Battery : Msg Idx = 11; Tamper Log = "TAMPER:069", special, only 0x00 allowed as *good* status
    '11 01 00 = Tamper Switch
    '           Bit 0– Tamper Switch 0 (0- Normal, 1-Error) : Msg Idx = 7; Tamper Log = "TAMPER:061"
    '           Bit 1– Tamper Switch 1 (0- Normal, 1-Error) : Msg Idx = 8; Tamper Log = "TAMPER:062"
    '           Bit 2– Tamper Switch 2 (0- Normal, 1-Error) : Msg Idx = 7; Tamper Log = "TAMPER:063"
    '           Bit 3– Tamper Switch 3 (0- Normal, 1-Error) : Msg Idx = 8; Tamper Log = "TAMPER:064"
    '           Bit 4– Tamper Switch 4 (0- Normal, 1-Error) : Msg Idx = 9; Tamper Log = "TAMPER:065"
    '           Bit 5– Tamper Switch 5 (0- Normal, 1-Error) : Msg Idx = 10; Tamper Log = "TAMPER:066"
    '           Bit 6 – Tamper Switch 6 (0- Normal,1-Error) : Msg Idx = 9; Tamper Log = "TAMPER:067"
    '           Bit 7 – Tamper Switch 7 (0- Normal, 1-Error) : Msg Idx = 10; Tamper Log = "TAMPER:068"
    '           0xFE - not check
    '12 01 00 = Temperature : Msg Idx = 4; Tamper Log = "TAMPER:058"
    '13 01 00 = Voltage : Msg Idx = 2; Tamper Log = "TAMPER:056"
    '14 01 00 = TM4 removal sensor : Msg Idx = 13; Tamper Log = "TAMPER:901"
    '15 01 00 = K81 RTC TOF : Msg Idx = 0; Tamper Log = "TAMPER:054"
    '16 01 00 = K81 clock CTF : Msg Idx = 3 ; Tamper Log = "TAMPER:057"
    '1F 01 FF = Other(Reserved2) : Msg Idx = N/A, X; 
    '
    '==[ Output, N/A ]==
    '==[ Return, "TAMPER:50" ]==
    Private Function SystemSecurity_Tamper_NEO20_UMFG_SMFG_GetLog_02_ConvertFrom_GetDRSxC73A_To_GetLogsxC73D(aryDatRx As Byte()) As String
        Dim strTamperLogs As String = ""
        Static s_strTamperPrefix As String = "DRS_TAMPER:"
        Dim strTmp As String
        Dim nIdxMax As Integer
        Dim nIdx As Integer
        Dim byteSrcId As Byte
        Dim byteSrcSts As Byte
        Dim byteTmprSwitchMask As Byte
        Dim strSrcIdx_StsVal As String = ""

#If 1 Then
        'Ignored list
        ' Src Idx = 0x01 - TM4 Check Value Error = "070"
        ' Src Idx = 0x14 - TM4 Removal Sensor Error = "071"
        Static s_lst01_SrcId2TamperLogMsg As SortedDictionary(Of Byte, String) = New SortedDictionary(Of Byte, String) From {
          {&H0, "072"}, {&H2, "RFU"}, {&H10, "069"}, {&H11, "x01_SubLst02"} _
       , {&H12, "058"}, {&H13, "056"}, {&H15, "054"}, {&H16, "057"} _
       , {&H1F, "RFU"} _
            }
#Else
        '// 2018 Sept 17, V02
               Static s_lst01_SrcId2TamperLogMsg As SortedDictionary(Of Byte, String) = New SortedDictionary(Of Byte, String) From {
          {&H0, "072"}, {&H1, "070"}, {&H2, "RFU"}, {&H10, "069"}, {&H11, "x01_SubLst02"} _
       , {&H12, "058"}, {&H13, "056"}, {&H14, "071"}, {&H15, "054"}, {&H16, "057"} _
       , {&H1F, "RFU"} _
            }
#End If
        '==[ x01_SubLst02 ]==
        'lst02 is ONLY for Tamper Switch Bitmask Check.
        '0xFE must be validat first.
        Static s_lst02_SrcId_x11_TamperSwitch2TamperLogMsg As Dictionary(Of Byte, String) = New Dictionary(Of Byte, String) From {
            {&H1, "061"}, {&H2, "062"}, {&H4, "063"}, {&H8, "064"} _
            , {&H10, "065"}, {&H20, "066"}, {&H40, "067"}, {&H80, "068"} _
            }
        '
        While (True)
            'Step 01 - Validation aryDatRx
            If (aryDatRx Is Nothing) Then
                strTamperLogs = "Err001 = Resp N/A"
                Exit While
            End If
            nIdxMax = aryDatRx.Length
            'Invalid Size
            If ((nIdxMax Mod 3) > 0) Then
                strTamperLogs = "Err001 = Resp Length Invalid, Not 3-byte multiple"
                Exit While
            End If
            nIdxMax = nIdxMax - 1

            'Step 02 - Normalize, 3 bytes as one set, Information Collection
            For nIdx = 0 To nIdxMax Step 3
                '02.A - Check Src Data ,Status Byte , offset = 2, 
                byteSrcId = aryDatRx(nIdx)
                byteSrcSts = aryDatRx(nIdx + 2)

                '----
                '===[ Special Note 01 ]===
                'Noted by Kevin Cheng @ EE Leader.RD.TW
                ' Battery Tamper, Src Id = 0x10
                ' but Sts = 0xFE, 0xFF ==> should consider it as tampered case/condition.
                '
                '----
                'if sts = 0x00, NO Tampered; 
                'sts = 0x01, Tampered. 
                'sts = 0xFF, RFU
                'sts = 0xFE, not check
                '----
                '
                strSrcIdx_StsVal = byteSrcId.ToString("X02") + "-" + byteSrcSts.ToString("X02")
                If (byteSrcSts = &HFE) Or (byteSrcSts = &HFF) Or (byteSrcSts = &H0) Then
                    'Uncheck Or RFU mode Or No Tampered
                    If (byteSrcSts = &HFE) Then
                        LogLn("[Waring] Not Checked, SrcId-StsVal = " + strSrcIdx_StsVal)
                    End If

                    '===[ Special Note 01 ]===
                    'Adding Tampered Log if (byteSrcSts <> &H0) And (byteSrcId = &H10)
                    If Not ((byteSrcSts <> &H0) And (byteSrcId = &H10)) Then
                        Continue For
                    End If

                End If
                '02.B - If Tampered, Check Source Id, offset = 0
                If (Not s_lst01_SrcId2TamperLogMsg.ContainsKey(byteSrcId)) Then
                    LogLn("[Warning] Tampered but Invalid " + s_strTamperPrefix + ",  SrcId-StsVal = " + strSrcIdx_StsVal)
                    Continue For
                End If
#If 0 Then
                If (&H14 = byteSrcId) Then
                    LogLn("[Information] Removal Tamper Found but ignore it since it's deactivated .  SrcId-StsVal = " + strSrcIdx_StsVal)
                    Continue For
                End If
#End If
                'offset = 1 = Src Data Len
                '02.C - If Is Tamper Switch Byte Sourc (Idx = &H11) = > Output Msg
                If (s_lst01_SrcId2TamperLogMsg(byteSrcId).Contains("SubLst02")) Then
                    For Each itrKeyVal As KeyValuePair(Of Byte, String) In s_lst02_SrcId_x11_TamperSwitch2TamperLogMsg
                        byteTmprSwitchMask = itrKeyVal.Key
                        If ((byteSrcSts And byteTmprSwitchMask) = byteTmprSwitchMask) Then
                            'Add to Logs Info, ex: TAMPER:062
                            strTamperLogs = strTamperLogs + s_strTamperPrefix + itrKeyVal.Value + vbCrLf
                        End If
                    Next
                Else
                    'Add to Logs Info, ex: TAMPER:054
                    strTamperLogs = strTamperLogs + s_strTamperPrefix + s_lst01_SrcId2TamperLogMsg(byteSrcId) + ":SrcId-StsVal = " + strSrcIdx_StsVal + vbCrLf
                End If
            Next


            'Step 03 - Information Collection Done
            If (strTamperLogs.Length < 1) Then
                strTamperLogs = strTamperLogs + s_strTamperPrefix + "050"
            End If

            'Finally Done
            Exit While
        End While

        Return strTamperLogs
    End Function

End Class
