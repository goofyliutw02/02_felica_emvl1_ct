﻿Imports System.Text
Imports Main.tagConfigCmdUnitPollmode

Public Class ClassTableListMaster
    '
    '=====================[ Configurations Text File ]=====================
    'AID List, since AID is unique --> Use Dictionary(Map@STL CPP language) structure to manage them
    'Public m_listAIDList As List(Of tagConfigCmdUnitAID)
    'Public m_dictAIDList As Dictionary(Of String, tagConfigCmdUnitAID)
    'Delete AID Info
    'Public m_dictAIDListDel As Dictionary(Of String, tagTLV)

    'Group List, since Group ID is unique --> Use Dictionary structure to manage them
    'Public m_dictGroupList As Dictionary(Of Integer, tagConfigCmdUnitGroup)

    'Comment List,  since Text line # is unique --> Us Dictionary structure to manage them
    'Public m_dictConfigCmdCommentList As Dictionary(Of Long, String)

    'All In One Command / Comment List
    'Public m_dictConfigCmdLineMap As Dictionary(Of Long, tagConfigCmdUnitLineMap)

    '=====================[ CRL Cmd Text File ]=====================
    'CRL Commands 'ADD' & 'ADD ALL' List, Key = RID+Index + Serial No (Line Content without Comment, Value = Formated CRL Info)
    'Public m_dictCRLCmdAddList As Dictionary(Of String, tagCRLCmdUnit)
    'CRL 'DEL' & 'DEL ALL' Command
    'Public m_dictCRLCmdDelList As Dictionary(Of String, tagCRLCmdUnit)
    'CRL File Comment List, Key = Line No, Value = Current Line Text
    'Public m_dictCRLCmdLineMap As Dictionary(Of Long, tagConfigCmdUnitLineMap)

    '======================[ CAPK Text File ]======================
    'Public m_dictCAPKCmdAddList As Dictionary(Of String, ClassCAPKUnit)
    'Public m_dictCAPKCmdDelList As Dictionary(Of String, ClassCAPKDelUnit)
    'Public m_dictCAPKCmdLineMap As Dictionary(Of Long, String)

    '=======================================================================================================================
    Sub New()
        '
        Dim iteratorA2H As KeyValuePair(Of Byte, String)
        For Each iteratorA2H In m_dictHex2ASCII
            m_dictASCII2Hex.Add(iteratorA2H.Value, iteratorA2H.Key)
        Next
        'Cleanup()
        'InitConfigurations()

        ''
        ''CleanupCRL()
        'InitCRL()
        ''
        ''CAPK
        'InitCAPK()
    End Sub
    '
   

    Public Sub Cleanup()
        'CleanupConfigurations()
        'CleanupCAPK()
        'CleanupCRL()
        '
        m_dictHex2ASCII.Clear()
        m_dictASCII2Hex.Clear()
        '
        m_s_objInstance = Nothing
    End Sub
    '==========================================
    'Example 1
    '[Input] strID = "A0 00 00 09 1F 05", nAIDLastByte = &H08
    '[Output] strID = "A0 00 00 09 1F 08"
    'Example 2
    '[Input] strID = "A0 00 00 09 1F 0E", nAIDLastByte = &H02
    '[Output] strID = "A0 00 00 09 1F 02"
    Private Sub AIDNewFormatLastByteChange(ByRef strAID As String, ByVal nAIDLastByte As Byte)
        Dim strAIDTokens() As String = Split(strAID)
        Dim nSeek As Integer
        Dim nSeekMax As Integer = strAIDTokens.Count - 2
        '
        strAID = ""
        For nSeek = 0 To nSeekMax
            strAID = strAID + strAIDTokens(nSeek) + " "
        Next
        'Add last one
        strAID = String.Format("{0}{1,2:X2}", strAID, nAIDLastByte)

    End Sub

    '==========================================
    'Example 1, No Blank
    '[Input]  strVal = "A0000E57927FD2365A"
    '[Output] o_arraybyteTLVal = {&HA0, &H00, &H0E, &H57, &H92, &H7F, &HD2, &H36, &H5A}
    'Example 2, Space Delimiter Exists = bSpaceDelimiterInStrVal = True
    '[Input]  strVal = "A0 00 0E 57 92 7F D2 36 5A"
    '[Output] o_arraybyteTLVal = {&HA0, &H00, &H0E, &H57, &H92, &H7F, &HD2, &H36, &H5A}
    Public Shared Sub TLVauleFromString2ByteArray(ByVal strVal As String, ByRef o_arraybyteTLVal() As Byte, Optional ByVal bNewBuffer As Boolean = True)
        TLVauleFromString2ByteArray(strVal, o_arraybyteTLVal, 0, bNewBuffer)
    End Sub

    'IN : "AB9C45" = {&HAB,&H9C,&H45}
    Public Shared Sub TLVauleFromString2ByteArray(ByVal strVal As String, ByRef o_arraybyteTLVal() As Byte, ByVal nPosDest As Integer, ByVal bNewBuffer As Boolean)
        Dim u32ChkLengthEven As UInt32
        Dim nLengthNew As Integer
        'Dim nCountForBlank As Integer
        Dim i, j As Integer
        Dim arrayCharVal() As Char
        Static Dim arrayCharHex As Dictionary(Of Char, Byte) = New Dictionary(Of Char, Byte) From {
            {"0"c, 0}, {"1"c, 1}, {"2"c, 2}, {"3"c, 3}, {"4"c, 4},
            {"5"c, 5}, {"6"c, 6}, {"7"c, 7}, {"8"c, 8}, {"9"c, 9},
            {"A"c, 10}, {"B"c, 11}, {"C"c, 12}, {"D"c, 13}, {"E"c, 14},
            {"F"c, 15}}

        If strVal Is Nothing Then
            Throw New Exception("Invalid Parameter strVal = Nothing ")
        End If
        'Remove Space
        strVal = Replace(strVal, " ", "")
        'Check Length
        u32ChkLengthEven = CType(strVal.Count, UInt32)
        If (u32ChkLengthEven And &H1) = &H1 Then
            'Throw New Exception("Invalid Parameter strVal = Length not EVEN ")
            strVal = "0" + strVal
        End If

        arrayCharVal = strVal.ToUpper().ToCharArray()


        ' Create new return on demand
        nLengthNew = (strVal.Count >> 1) - 1
        If bNewBuffer Then
            If o_arraybyteTLVal IsNot Nothing Then
                Erase o_arraybyteTLVal
            End If
            o_arraybyteTLVal = New Byte(nLengthNew) {}
        End If
        'Starting Position Boundary Check
        If (nPosDest >= o_arraybyteTLVal.Count) Then
            Throw New Exception("Start Position (" + nPosDest + ") of Destination Buffer >= Buffer's Size (" + o_arraybyteTLVal.Count + ") is Invalid")
        End If
        ' Copy data to return buffer
        j = 0
        For i = nPosDest To nLengthNew
            Dim bVal As Byte
            bVal = (arrayCharHex.Item(arrayCharVal(j)) << 4) + arrayCharHex.Item(arrayCharVal(j + 1))
            j = j + 2
            o_arraybyteTLVal(i) = bVal
        Next
    End Sub
    '
    Public Shared Sub TLVauleFromString2Byte(ByVal strVal As String, ByRef o_arraybyteTLVal As Byte)
        'Dim byteChkLengthEven As Byte
        'Dim nLengthNew As Integer
        'Dim nCountForBlank As Integer
        'Dim j As Integer
        Dim arrayCharVal() As Char
        Static Dim arrayCharHex As Dictionary(Of Char, Byte) = New Dictionary(Of Char, Byte) From {
            {"0"c, 0}, {"1"c, 1}, {"2"c, 2}, {"3"c, 3}, {"4"c, 4},
            {"5"c, 5}, {"6"c, 6}, {"7"c, 7}, {"8"c, 8}, {"9"c, 9},
            {"A"c, 10}, {"B"c, 11}, {"C"c, 12}, {"D"c, 13}, {"E"c, 14},
            {"F"c, 15}}

        If strVal Is Nothing Then
            Throw New Exception("Invalid Parameter strVal = Nothing ")
        End If
        'Remove Sapce
        strVal = Replace(strVal, " ", "")
        arrayCharVal = strVal.ToUpper().ToCharArray()

        'Check Length
        If (strVal.Count <> 2) Then
            'byteChkLengthEven = CType(strVal.Count, Byte)
            'If (byteChkLengthEven And &H1) = &H1 Then
            Throw New Exception("Invalid Parameter strVal = Length not EVEN ")
        End If
        '
        o_arraybyteTLVal = (arrayCharHex.Item(arrayCharVal(0)) << 4) + arrayCharHex.Item(arrayCharVal(1))
    End Sub
    '
    '==========================================
    'Example 1
    '[Input] nLen = 9, strVal = "A0 00 0E 57 92 7F D2 36 5A"
    '[Output] o_arraybyteTLVal = {&HA0, &H00, &H0E, &H57, &H92, &H7F, &HD2, &H36, &H5A}
    Public Shared Sub TLVauleFromString2ByteArray(ByVal strVal As String, ByVal nLen As Integer, ByRef o_arraybyteTLVal() As Byte)
        Dim strSplit() As String = Split(strVal)
        Dim strSeek As String
        Dim nIdx As Integer

        'Special Case, TLV Len = 0, No TLV data = ""
        'Since Spilt("") comes with length = 1
        If (nLen = 0) And (strVal = "") Then
            Throw New Exception("[Err] tagTLV:TLVauleFromString2ByteArray() = Invalid TLV New Params")
        ElseIf (nLen <> strSplit.Length) Then
            Dim strMsg As String = "[Err:tagTLV:TLVauleFromString2ByteArray():param invalid] TLValue = " + strVal + "nLen = " & nLen & ", TLVal = " + strVal
            Throw New Exception(strMsg)
        End If

        '===========================================================================
        If o_arraybyteTLVal IsNot Nothing Then
            Erase o_arraybyteTLVal
            o_arraybyteTLVal = Nothing
        End If
        '
        ' If strSplit.Length > 0 Then
        If nLen > 0 Then
            o_arraybyteTLVal = New Byte(strSplit.Length - 1) {}
            ' Start to fill byte value one by one
            nIdx = 0
            For Each strSeek In strSplit
                o_arraybyteTLVal.SetValue(Convert.ToByte(strSeek, 16), nIdx)
                nIdx = nIdx + 1
            Next
        End If
        '================================================================================
        'If m_arrayTLVal IsNot Nothing Then
        '    Erase m_arrayTLVal
        '    m_arrayTLVal = Nothing
        'End If
        ''
        '' If strSplit.Length > 0 Then
        'If nLen > 0 Then
        '    m_arrayTLVal = New Byte(strSplit.Length - 1) {}
        '    ' Start to fill byte value one by one
        '    nIdx = 0
        '    For Each strSeek In strSplit
        '        m_arrayTLVal.SetValue(Convert.ToByte(strSeek, 16), nIdx)
        '        nIdx = nIdx + 1
        '    Next
        'End If
    End Sub
    '
    Public Shared Sub TLVauleFromString2ByteArray(ByVal strVal As String, ByRef o_arraybyteTLVal() As Byte)
        Dim nLen As Integer
        Dim strSplit As String() = New String() {}
        Dim nIdx, nIdxResult As Integer

        If (strVal.Equals("")) Then
            Throw New Exception("strVal Can't be NULL String")
        End If
        'Normalize
        strVal = ClassTableListMaster.StrNormalized(strVal)

        nLen = strVal.Length()
        If ((nLen Mod 1) > 0) Then
            Throw New Exception("strVal's Char Number must be EVEN")
        End If

        '===========================================================================
        If o_arraybyteTLVal IsNot Nothing Then
            Erase o_arraybyteTLVal
            o_arraybyteTLVal = Nothing
        End If
        '
        ' If strSplit.Length > 0 Then
        If nLen > 0 Then
            o_arraybyteTLVal = New Byte(nLen \ 2 - 1) {}
            ' Start to fill byte value one by one
            nIdxResult = 0
            nIdx = 0
            While (nIdx < nLen)
                o_arraybyteTLVal.SetValue(Convert.ToByte(CType(strVal.Chars(nIdx), String) + CType(strVal.Chars(nIdx + 1), String), 16), nIdxResult)
                nIdx = nIdx + 2
                nIdxResult = nIdxResult + 1
            End While
        End If
    End Sub

    '==========================================
    'Example 1
    '[Input] i_arraybAID = { &HA0, &H00, &H00, &H09, &H1F, &H05}
    '[Output] o_strAID = "A0 00 00 09 1F 05"
    Public Shared Sub ConvertFromArrayByte2String(ByVal i_arrayByte() As Byte, ByRef o_strByteList As String, Optional ByVal bDelimiterSpace As Boolean = True)
        ConvertFromArrayByte2String(i_arrayByte, i_arrayByte.Count, o_strByteList, bDelimiterSpace)
    End Sub

    Public Shared Sub ConvertFromArrayByte2String(ByVal iByte As Byte, ByRef o_strByte As String)
        Dim i_arrayByte As Byte() = New Byte(0) {iByte}
        Try
            ConvertFromArrayByte2String(i_arrayByte, 1, o_strByte, False)
        Catch ex As Exception

        Finally
            Erase i_arrayByte
        End Try

    End Sub

    Public Shared Sub ConvertFromArrayByte2String(ByVal i_arrayByte() As Byte, ByVal nArrayByteLen As Integer, ByRef o_strByteList As String, Optional ByVal bDelimiterSpace As Boolean = True)
        ConvertFromArrayByte2String(i_arrayByte, 0, nArrayByteLen, o_strByteList, bDelimiterSpace)
    End Sub

    Public Shared Sub ConvertFromArrayByte2String(ByVal i_arrayByte() As Byte, ByRef nPosStart As Integer, ByVal nArrayByteLenAfterPosStart As Integer, ByRef o_strByteList As String, Optional ByVal bDelimiterSpace As Boolean = True)
        Dim nIdx As Integer
        Dim nIdxMax As Integer = nPosStart + nArrayByteLenAfterPosStart - 1

        If (nArrayByteLenAfterPosStart < 1) Then
            Throw New Exception("[Err] ConvertFromArrayByte2String() = Bad i_arrayByte() Length = " & nArrayByteLenAfterPosStart)
        End If
        If (nPosStart < 0) Then
            Throw New Exception("[Err] ConvertFromArrayByte2String() = Bad Param, 'nPosStart' ( " & nPosStart & " ) < 0 index is out of range")
        End If
        If (i_arrayByte.Count < (nIdxMax + 1)) Then
            Throw New Exception("[Err] ConvertFromArrayByte2String() = Over Range Indexing,  seeking range from " & nPosStart & " To " & nIdxMax & ", But i_arrayByte range is 0 To " & (i_arrayByte.Count - 1))
        End If
        'Give first one with prefix blank " "
        o_strByteList = String.Format("{0,2:X2}", i_arrayByte(nPosStart))
        'Continue to remains
        If nArrayByteLenAfterPosStart > 1 Then
            For nIdx = nPosStart + 1 To nIdxMax
                If (bDelimiterSpace) Then
                    o_strByteList = o_strByteList + String.Format(" {0,2:X2}", i_arrayByte(nIdx))
                Else
                    o_strByteList = o_strByteList + String.Format("{0,2:X2}", i_arrayByte(nIdx))
                End If
            Next
        End If
    End Sub

    '==========================================
    'Example 1
    '[Input] i_arraybAID = { &HA0, &H00, &H00, &H09, &H1F, &H05}
    '[Output] o_strAID(0) = "A0 00 00 "
    '                    o_strAID(1) = "09 1F 05"
    Public Shared Sub ConvertFromArrayByte2StringListFormated(ByVal i_arrayByte() As Byte, ByRef o_strByteStrListFormated() As String, ByVal nLineByteNum As Integer)
        ConvertFromArrayByte2StringListFormated(i_arrayByte, i_arrayByte.Count - 1, o_strByteStrListFormated, nLineByteNum)
    End Sub

    Public Shared Sub ConvertFromArrayByte2StringListFormated(ByVal i_arrayByte() As Byte, ByVal n_DataLen As Integer, ByRef o_strByteStrListFormated() As String, ByVal nLineByteNum As Integer)
        ConvertFromArrayByte2StringListFormated(i_arrayByte, 0, n_DataLen, o_strByteStrListFormated, nLineByteNum)
    End Sub
    Public Shared Sub ConvertFromArrayByte2StringListFormated(ByVal i_arrayByte() As Byte, ByVal nPosStart As Integer, ByVal n_DataLen As Integer, ByRef o_strByteStrListFormated() As String, ByVal nLineByteNum As Integer)
        Dim nIdx As Integer
        Dim nIdxMax As Integer = n_DataLen
        Dim o_strByteList As String = ""
        Dim nLineByteNumCount As Integer
        Dim myList As New List(Of String)()


        'Start to loop it
        nLineByteNumCount = 0
        For nIdx = 0 To nIdxMax - 1
            o_strByteList = o_strByteList + String.Format(" {0,2:X2}", i_arrayByte(nIdx))

            nLineByteNumCount = nLineByteNumCount + 1
            '
            If nLineByteNum = nLineByteNumCount Then
                myList.Add(o_strByteList)
                'o_strByteStrListFormated(o_strByteStrListFormated.Count) = New String(o_strByteList.ToCharArray())
                nLineByteNumCount = 0
                o_strByteList = ""
            End If
        Next

        ' Put the remains data in the last line
        If nLineByteNumCount > 0 Then
            myList.Add(o_strByteList.ToCharArray())
            nLineByteNumCount = 0
            o_strByteList = ""
        End If

        'Output to string list
        o_strByteStrListFormated = myList.ToArray()
        myList.Clear()

    End Sub

    Public Shared Sub ConvertFromArrayByte2StringListFormatedBlankRight(ByVal i_arrayByte() As Byte, ByVal n_DataLen As Integer, ByRef o_strByteStrListFormated() As String, ByVal nLineByteNum As Integer)
        Dim nIdx As Integer
        Dim nIdxMax As Integer = n_DataLen
        Dim o_strByteList As String = ""
        Dim nLineByteNumCount As Integer
        Dim myList As New List(Of String)()


        'Start to loop it
        nLineByteNumCount = 0
        For nIdx = 0 To nIdxMax - 1
            o_strByteList = o_strByteList + String.Format("{0,2:X2} ", i_arrayByte(nIdx))

            nLineByteNumCount = nLineByteNumCount + 1
            '
            If nLineByteNum = nLineByteNumCount Then
                myList.Add(o_strByteList)
                'o_strByteStrListFormated(o_strByteStrListFormated.Count) = New String(o_strByteList.ToCharArray())
                nLineByteNumCount = 0
                o_strByteList = ""
            End If
        Next

        ' Put the remains data in the last line
        If nLineByteNumCount > 0 Then
            myList.Add(o_strByteList.ToCharArray())
            nLineByteNumCount = 0
            o_strByteList = ""
        End If

        'Output to string list
        o_strByteStrListFormated = myList.ToArray()
        myList.Clear()

    End Sub
  
    '
    Shared Sub CleanupConfigurationsTLVList(ByRef o_dictTLV As Dictionary(Of String, tagTLV))
        Dim iteratorKeyVal As KeyValuePair(Of String, tagTLV)
        'TLV Value Clean up
        If o_dictTLV IsNot Nothing Then
            If o_dictTLV.Count > 0 Then
                For Each iteratorKeyVal In o_dictTLV
                    iteratorKeyVal.Value.Cleanup()
                Next
                o_dictTLV.Clear()
            End If
        End If
    End Sub
    Shared Sub CleanupConfigurationsTLVList(ByRef o_dictTLV As SortedDictionary(Of String, tagTLV))
        Dim iteratorKeyVal As KeyValuePair(Of String, tagTLV)
        'TLV Value Clean up
        If o_dictTLV IsNot Nothing Then
            If o_dictTLV.Count > 0 Then
                For Each iteratorKeyVal In o_dictTLV
                    iteratorKeyVal.Value.Cleanup()
                Next
                o_dictTLV.Clear()
            End If
        End If
    End Sub
    Shared Sub CleanupConfigurationsTLVList(ByRef o_dictTLV As SortedList(Of String, tagTLV))
        Dim iteratorKeyVal As KeyValuePair(Of String, tagTLV)
        'TLV Value Clean up
        If o_dictTLV IsNot Nothing Then
            If o_dictTLV.Count > 0 Then
                For Each iteratorKeyVal In o_dictTLV
                    iteratorKeyVal.Value.Cleanup()
                Next
                o_dictTLV.Clear()
            End If
        End If
    End Sub
    '
    Shared Sub CleanupConfigurationsTLVList(ByRef o_dictTLV As List(Of tagTLV))
        Dim tlv As tagTLV
        'TLV Value Clean up
        If o_dictTLV IsNot Nothing Then
            If o_dictTLV.Count > 0 Then
                For Each tlv In o_dictTLV
                    tlv.Cleanup()
                Next
                o_dictTLV.Clear()
            End If
        End If
    End Sub

    '================[  CA Public Key Commands Tables being Loaded From CA Public Key File or Reader Device ]======================
#If 0 Then
     ReadOnly Property bIsDataAvailableConfigurations As Boolean
        Get
            Return ((m_dictAIDList.Count > 0) Or (m_dictAIDListDel.Count > 0) Or (m_dictGroupList.Count > 0))
        End Get
    End Property
    '

    ReadOnly Property bIsDataAvailableCAPK As Boolean
        Get
            Return ((m_dictCAPKCmdAddList.Count > 0) Or (m_dictCAPKCmdDelList.Count > 0))
        End Get
    End Property

      '
    Public Shared Sub CleanupConfigurationsAIDList(ByRef dictAIDList As SortedDictionary(Of String, tagConfigCmdUnitAID))
        ' AID List Cleanup
        If dictAIDList IsNot Nothing Then
            If dictAIDList.Count > 0 Then
                Dim iteratorAIDListKeyValue As KeyValuePair(Of String, tagConfigCmdUnitAID)
                For Each iteratorAIDListKeyValue In dictAIDList
                    iteratorAIDListKeyValue.Value.Cleanup()
                Next
                dictAIDList.Clear()
            End If
        End If
    End Sub


    Public Sub InitCAPK()
        If m_dictCAPKCmdAddList Is Nothing Then
            m_dictCAPKCmdAddList = New Dictionary(Of String, ClassCAPKUnit)
        End If
        '
        If m_dictCAPKCmdDelList Is Nothing Then
            m_dictCAPKCmdDelList = New Dictionary(Of String, ClassCAPKDelUnit)
        End If
        '
        If m_dictCAPKCmdLineMap Is Nothing Then
            m_dictCAPKCmdLineMap = New Dictionary(Of Long, String)
        End If
    End Sub
    '
    Public Sub CleanupCAPK()
        Dim iteratorCAPKAdd As KeyValuePair(Of String, ClassCAPKUnit)
        Dim iteratorCAPKDel As KeyValuePair(Of String, ClassCAPKDelUnit)
        Dim iteratorCAPKLineMap As KeyValuePair(Of Long, String)

        For Each iteratorCAPKAdd In m_dictCAPKCmdAddList
            iteratorCAPKAdd.Value.Cleanup()
        Next
        m_dictCAPKCmdAddList.Clear()
        '
        For Each iteratorCAPKDel In m_dictCAPKCmdDelList
            iteratorCAPKDel.Value.Cleanup()
        Next
        m_dictCAPKCmdDelList.Clear()

        For Each iteratorCAPKLineMap In m_dictCAPKCmdLineMap
            Dim strTmp As String = iteratorCAPKLineMap.Value
        Next
        m_dictCAPKCmdLineMap.Clear()
    End Sub
    '
    '================[ CRL  Commands Tables being Loaded From CA Public Key File or Reader Device ]======================
    Public Sub InitCRL()
        If m_dictCRLCmdAddList Is Nothing Then
            m_dictCRLCmdAddList = New Dictionary(Of String, tagCRLCmdUnit)
        End If
        '
        If m_dictCRLCmdDelList Is Nothing Then
            m_dictCRLCmdDelList = New Dictionary(Of String, tagCRLCmdUnit)
        End If
        '
        'If m_dictCRLCmdLineMap Is Nothing Then
        '    m_dictCRLCmdLineMap = New Dictionary(Of Long, tagConfigCmdUnitLineMap)
        'End If
    End Sub
    '
    Public Sub CleanupCRL()
        '========================[CRL Commands Information ]===================
        'CRL Delete All Command
        If m_dictCRLCmdAddList IsNot Nothing Then
            If m_dictCRLCmdAddList.Count > 0 Then
                Dim iteratorCRLKeyValue As KeyValuePair(Of String, tagCRLCmdUnit)
                For Each iteratorCRLKeyValue In m_dictCRLCmdAddList
                    iteratorCRLKeyValue.Value.Cleanup()
                Next
                m_dictCRLCmdAddList.Clear()
            End If
        End If
        'CRL Delete / Delete All Command
        If m_dictCRLCmdDelList IsNot Nothing Then
            If m_dictCRLCmdDelList.Count > 0 Then
                Dim iteratorCRLKeyValue As KeyValuePair(Of String, tagCRLCmdUnit)
                For Each iteratorCRLKeyValue In m_dictCRLCmdDelList
                    iteratorCRLKeyValue.Value.Cleanup()
                Next
                m_dictCRLCmdDelList.Clear()
            End If
        End If
        '
        'If m_dictCRLCmdLineMap IsNot Nothing Then
        '    If m_dictCRLCmdLineMap.Count > 0 Then
        '        Dim iteratorCRLCmdLineMap As KeyValuePair(Of Long, tagConfigCmdUnitLineMap)
        '        For Each iteratorCRLCmdLineMap In m_dictConfigCmdLineMap
        '            iteratorCRLCmdLineMap.Value.Cleanup()
        '        Next
        '        m_dictCRLCmdLineMap.Clear()

        '    End If
        'End If

    End Sub

    '
    Public Sub InitConfigurations()
        ' Table / List / Dictionary New Initialization
        'm_listAIDList = New List(Of tagConfigCmdUnitAID)
        If m_dictAIDList Is Nothing Then
            m_dictAIDList = New Dictionary(Of String, tagConfigCmdUnitAID)
        End If
        If m_dictGroupList Is Nothing Then
            m_dictGroupList = New Dictionary(Of Integer, tagConfigCmdUnitGroup)
        End If
        If m_dictConfigCmdCommentList Is Nothing Then
            m_dictConfigCmdCommentList = New Dictionary(Of Long, String) ' (Line #, Text Line)
        End If
        If m_dictConfigCmdLineMap Is Nothing Then
            m_dictConfigCmdLineMap = New Dictionary(Of Long, tagConfigCmdUnitLineMap)
        End If
        If m_dictAIDListDel Is Nothing Then
            m_dictAIDListDel = New Dictionary(Of String, tagTLV)
        End If
        '
    End Sub
    '
    Public Shared Sub CleanupConfigurationsGroupList(ByRef dictGroupList As Dictionary(Of Integer, tagConfigCmdUnitGroup))
        ' Group List Cleanup
        If dictGroupList IsNot Nothing Then
            If dictGroupList.Count > 0 Then
                Dim iteratorGroupListKeyValue As KeyValuePair(Of Integer, tagConfigCmdUnitGroup)
                For Each iteratorGroupListKeyValue In dictGroupList
                    iteratorGroupListKeyValue.Value.Cleanup()
                Next
                dictGroupList.Clear()
            End If
        End If
    End Sub
    '
    Public Shared Sub CleanupConfigurationsAIDList(ByRef dictAIDList As Dictionary(Of String, tagConfigCmdUnitAID))
        ' AID List Cleanup
        If dictAIDList IsNot Nothing Then
            If dictAIDList.Count > 0 Then
                Dim iteratorAIDListKeyValue As KeyValuePair(Of String, tagConfigCmdUnitAID)
                For Each iteratorAIDListKeyValue In dictAIDList
                    iteratorAIDListKeyValue.Value.Cleanup()
                Next
                dictAIDList.Clear()
            End If
        End If
    End Sub
    '
    Public Sub CleanupConfigurations()
        '===================[Configurations Commands : AID / GROUP List Add/Delete Information ]===================
        ' AID List Cleanup
        CleanupConfigurationsAIDList(m_dictAIDList)

        '
        If m_dictAIDListDel IsNot Nothing Then
            m_dictAIDListDel.Clear()
        End If

        CleanupConfigurationsGroupList(m_dictGroupList)


        ' Comment Line List Cleanup
        If m_dictConfigCmdCommentList IsNot Nothing Then
            If m_dictConfigCmdCommentList.Count > 0 Then
                'Dim iteratorCommentListKeyValue As KeyValuePair(Of Integer, String)
                'For Each iteratorCommentListKeyValue In m_dictConfigCmdCommentList
                'iteratorCommentListKeyValue.Value.Cleanup()
                'Next
                m_dictConfigCmdCommentList.Clear()
            End If
        End If

        'Command / Text Line Map
        If m_dictConfigCmdLineMap IsNot Nothing Then
            If m_dictConfigCmdLineMap.Count > 0 Then
                m_dictConfigCmdLineMap.Clear()
            End If
        End If

    End Sub


    Shared Sub CleanupCRLLists(ByRef o_dictCRLCmdAddList As Dictionary(Of String, tagCRLCmdUnit))
        Dim iteratorCRL As KeyValuePair(Of String, tagCRLCmdUnit)

        For Each iteratorCRL In o_dictCRLCmdAddList
            iteratorCRL.Value.Cleanup()
        Next

        o_dictCRLCmdAddList.Clear()

    End Sub
    Shared Sub CleanupCRLLists(ByRef o_dictCRLCmdAddList As SortedDictionary(Of String, tagCRLCmdUnit))
        Dim iteratorCRL As KeyValuePair(Of String, tagCRLCmdUnit)

        For Each iteratorCRL In o_dictCRLCmdAddList
            iteratorCRL.Value.Cleanup()
        Next

        o_dictCRLCmdAddList.Clear()

    End Sub


    Sub CleanupCRLSN100001()
        Dim iteratorCRL As KeyValuePair(Of String, tagCRLCmdUnit)
        Dim tagCRLList As tagCRLCmdUnit
        Dim tagCRLSingle As tagCRLCmdUnitSingle
        Static Dim strSerialNum As String = "10 00 01"

        For Each iteratorCRL In m_dictCRLCmdAddList
            tagCRLList = iteratorCRL.Value
            With tagCRLList
                ' Remove From Serial Num List
                If .m_dictCRLCmdList.ContainsKey(strSerialNum) Then
                    tagCRLSingle = .m_dictCRLCmdList.Item(strSerialNum)
                    tagCRLSingle.Cleanup()
                    .m_dictCRLCmdList.Remove(strSerialNum)
                End If
                'Remove From RID+Index List
                If (.m_dictCRLCmdList.Count = 0) Then
                    m_dictCRLCmdAddList.Remove(tagCRLList.m_strRIDIndex)
                End If
            End With
        Next

    End Sub

    Sub GetCRLSN100001(ByRef o_dictCRLSN100001 As Dictionary(Of String, tagCRLCmdUnitSingle))
        Dim iteratorCRL As KeyValuePair(Of String, tagCRLCmdUnit)
        Dim tagCRLList As tagCRLCmdUnit
        Static Dim strSerialNum As String = "10 00 01"

        For Each iteratorCRL In m_dictCRLCmdAddList
            tagCRLList = iteratorCRL.Value
            With tagCRLList
                If .m_dictCRLCmdList.ContainsKey(strSerialNum) Then
                    If Not o_dictCRLSN100001.ContainsKey(.m_strRIDIndex) Then
                        o_dictCRLSN100001.Add(.m_strRIDIndex, .m_dictCRLCmdList.Item(strSerialNum))
                    End If
                End If
            End With
        Next

    End Sub
    Sub GetCRLSN100001(ByRef o_dictCRLSN100001 As SortedDictionary(Of String, tagCRLCmdUnitSingle))
        Dim iteratorCRL As KeyValuePair(Of String, tagCRLCmdUnit)
        Dim tagCRLList As tagCRLCmdUnit
        Static Dim strSerialNum As String = "10 00 01"

        For Each iteratorCRL In m_dictCRLCmdAddList
            tagCRLList = iteratorCRL.Value
            With tagCRLList
                If .m_dictCRLCmdList.ContainsKey(strSerialNum) Then
                    If Not o_dictCRLSN100001.ContainsKey(.m_strRIDIndex) Then
                        o_dictCRLSN100001.Add(.m_strRIDIndex, .m_dictCRLCmdList.Item(strSerialNum))
                    End If
                End If
            End With
        Next

    End Sub
#End If
    Public Shared m_dictHex2ASCII As Dictionary(Of Byte, String) = New Dictionary(Of Byte, String) From {
    {0, "[00h"}, {1, "[01h"}, {2, "[02h"}, {3, "[03h"}, {4, "[04h"}, {5, "[05h"}, {6, "[06h"}, {7, "[07h"}, {8, "[08h"}, {9, "[09h"},
    {10, "[0Ah"}, {11, "[0Bh"}, {12, "[0Ch"}, {13, "[0Dh"}, {14, "[0Eh"}, {15, "[0Fh"}, {16, "[10h"}, {17, "[11h"}, {18, "[12h"}, {19, "[13h"},
    {20, "[14h"}, {21, "[15h"}, {22, "[16h"}, {23, "[17h"}, {24, "[18h"}, {25, "[19h"}, {26, "[1Ah"}, {27, "[1Bh"}, {28, "[1Ch"}, {29, "[1Dh"},
    {30, "[1Eh"}, {31, "[1Fh"}, {32, " "}, {33, "!"}, {34, """"}, {35, "#"}, {36, "$"}, {37, "%"}, {38, "&"}, {39, "'"},
    {40, "("}, {41, ")"}, {42, "*"}, {43, "+"}, {44, ","}, {45, "-"}, {46, "."}, {47, "/"}, {48, "0"}, {49, "1"}, _
 _
    {50, "2"}, {51, "3"}, {52, "4"}, {53, "5"}, {54, "6"}, {55, "7"}, {56, "8"}, {57, "9"}, {58, ":"}, {59, ";"},
    {60, "<"}, {61, "="}, {62, ">"}, {63, "?"}, {64, "@"}, {65, "A"}, {66, "B"}, {67, "C"}, {68, "D"}, {69, "E"},
    {70, "F"}, {71, "G"}, {72, "H"}, {73, "I"}, {74, "J"}, {75, "K"}, {76, "L"}, {77, "M"}, {78, "N"}, {79, "O"},
    {80, "P"}, {81, "Q"}, {82, "R"}, {83, "S"}, {84, "T"}, {85, "U"}, {86, "V"}, {87, "W"}, {88, "X"}, {89, "Y"},
    {90, "Z"}, {91, "["}, {92, "\"}, {93, "]"}, {94, "^"}, {95, "_"}, {96, Chr(96)}, {97, "a"}, {98, "b"}, {99, "c"}, _
 _
    {100, "d"}, {101, "e"}, {102, "f"}, {103, "g"}, {104, "h"}, {105, "i"}, {106, "j"}, {107, "k"}, {108, "l"}, {109, "m"},
    {110, "n"}, {111, "o"}, {112, "p"}, {113, "q"}, {114, "r"}, {115, "s"}, {116, "t"}, {117, "u"}, {118, "v"}, {119, "w"},
    {120, "x"}, {121, "y"}, {122, "z"}, {123, "{"}, {124, "|"}, {125, "}"}, {126, "~"}, {127, "[7Fh"}, {128, "[80h"}, {129, "[81h"},
    {130, "[82h"}, {131, "[83h"}, {132, "[84h"}, {133, "[85h"}, {134, "[86h"}, {135, "[87h"}, {136, "[88h"}, {137, "[89h"}, {138, "[8Ah"}, {139, "[8Bh"},
    {140, "[8Ch"}, {141, "[8Dh"}, {142, "[8Eh"}, {143, "[8Fh"}, {144, "[90h"}, {145, "[91h"}, {146, "[92h"}, {147, "[93h"}, {148, "[94h"}, {149, "[95h"}, _
 _
    {150, "[96h"}, {151, "[97h"}, {152, "[98h"}, {153, "[99h"}, {154, "[9Ah"}, {155, "[9Bh"}, {156, "[9Ch"}, {157, "[9Dh"}, {158, "[9Eh"}, {159, "[9Fh"},
    {160, "[A0h"}, {161, "[A1h"}, {162, "[A2h"}, {163, "[A3h"}, {164, "[A4h"}, {165, "[A5h"}, {166, "[A6h"}, {167, "[A7h"}, {168, "[A8h"}, {169, "[A9h"},
    {170, "[AAh"}, {171, "[ABh"}, {172, "[ACh"}, {173, "[ADh"}, {174, "[AEh"}, {175, "[AFh"}, {176, "[B0h"}, {177, "[B1h"}, {178, "[B2h"}, {179, "[B3h"},
    {180, "[B4h"}, {181, "[B5h"}, {182, "[B6h"}, {183, "[B7h"}, {184, "[B8h"}, {185, "[B9h"}, {186, "[BAh"}, {187, "[BBh"}, {188, "[BCh"}, {189, "[BDh"},
    {190, "[BEh"}, {191, "[BFh"}, {192, "[C0h"}, {193, "[C1h"}, {194, "[C2h"}, {195, "[C3h"}, {196, "[C4h"}, {197, "[C5h"}, {198, "[C6h"}, {199, "[C7h"}, _
 _
     {200, "[C8h"}, {201, "[C9h"}, {202, "[CAh"}, {203, "[CBh"}, {204, "[CCh"}, {205, "[CDh"}, {206, "[CEh"}, {207, "[CFh"}, {208, "[D0h"}, {209, "[D1h"},
    {210, "[D2h"}, {211, "[D3h"}, {212, "[D4h"}, {213, "[D5h"}, {214, "[D6h"}, {215, "[D7h"}, {216, "[D8h"}, {217, "[D9h"}, {218, "[DAh"}, {219, "[DBh"},
    {220, "[DCh"}, {221, "[DDh"}, {222, "[DEh"}, {223, "[DFh"}, {224, "[E0h"}, {225, "[E1h"}, {226, "[E2h"}, {227, "[E3h"}, {228, "[E4h"}, {229, "[E5h"},
    {230, "[E6h"}, {231, "[E7h"}, {232, "[E8h"}, {233, "[E9h"}, {234, "[EAh"}, {235, "[EBh"}, {236, "[ECh"}, {237, "[EDh"}, {238, "[EEh"}, {239, "[EFh"},
    {240, "[F0h"}, {241, "[F1h"}, {242, "[F2h"}, {243, "[F3h"}, {244, "[F4h"}, {245, "[F5h"}, {246, "[F6h"}, {247, "[F7h"}, {248, "[F8h"}, {249, "[F9h"}, _
 _
    {250, "[FAh"}, {251, "[FBh"}, {252, "[FCh"}, {253, "[FDh"}, {254, "[FEh"}, {255, "[FFh"}
    }
    Public Shared m_dictASCII2Hex As Dictionary(Of String, Byte) = New Dictionary(Of String, Byte)
    'Note: Non Readable ASCII value will be translate as [##h, 
    'For Example: {&HD9, &HA} ==> "[D9h[0Ah"

    'For Byte Array Example:
    'INPUT = {&H30, &H31, &H32, &H33, &H00, &HFF}
    'OUTPUT = "0123[h[FFh"
    Shared Sub ConvertFromHexToAscii(arrayByteData As Byte(), ByRef o_strTmp As String, Optional bStringTypeOut As Boolean = False)
        ConvertFromHexToAscii(arrayByteData, 0, arrayByteData.Count, o_strTmp, bStringTypeOut)
    End Sub
    Shared Sub ConvertFromHexToAscii(arrayByteData As Byte(), nPosStart As Integer, nDataLen As Integer, ByRef o_strTmp As String, Optional bStringTypeNullEnd As Boolean = False)
        Dim nIdx, nIdxMax As Integer
        '
        o_strTmp = ""
        nIdxMax = nPosStart + nDataLen - 1
        For nIdx = nPosStart To nIdxMax

            If ((bStringTypeNullEnd) And (arrayByteData(nIdx) = &H0)) Then
                'String ONLY
                Exit For
            End If
            o_strTmp = o_strTmp + m_dictHex2ASCII.Item(arrayByteData(nIdx))
        Next
    End Sub

    'Input = "A09F", null byte() o_arrayByteTmp 
    'Output =  o_arrayByteTmp = New Byte() {&H41, &H30,&H39,&H46}
    Shared Sub ConvertFromAscii2HexByte(strVal As String, ByRef o_arrayByteTmp As Byte())
        ' Dim arrayByteData As Byte() = System.Text.Encoding.ASCII.GetBytes(strData)
        ConvertFromAscii2HexByte(strVal, o_arrayByteTmp, 0)
    End Sub

    Shared Sub ConvertFromAscii2HexByte(strValSrc As String, ByRef o_arrayByteTmpDest As Byte(), ByVal nPosStartDest As Integer, Optional ByVal bNewBuffer As Boolean = True)
        Dim nIdxSrc, nIdx, nIdxMax As Integer
        Dim arrayCharSrc As Char() = strValSrc.ToCharArray()
        '
        nIdxMax = nPosStartDest + strValSrc.Count - 1
        ' New Buffer if necessary
        If (bNewBuffer) Then
            If o_arrayByteTmpDest IsNot Nothing Then
                Erase o_arrayByteTmpDest
            End If
            o_arrayByteTmpDest = New Byte(nIdxMax) {}
        End If

        ' Start Position Boundary Check
        If (nPosStartDest >= o_arrayByteTmpDest.Count) Then
            Throw New Exception("[Err]ConvertFromAscii2HexByte() = Out of Range,Destination Start Position (i.e. nPosStart (" + nPosStartDest + ")) >= Output Buffer Size (size=" + o_arrayByteTmpDest.Count + ")")
        End If

        ' Copy Data New
        nIdxSrc = 0
        For nIdx = nPosStartDest To nIdxMax
            o_arrayByteTmpDest(nIdx) = Convert.ToByte(arrayCharSrc(nIdxSrc))
            nIdxSrc = nIdxSrc + 1
        Next
        '
    End Sub
    '

    '================================================================================
    Public Shared Sub ByteExchange(ByRef b1 As Byte, ByRef b2 As Byte)
        Dim bTmp As Byte

        bTmp = b1
        b1 = b2
        b2 = bTmp

    End Sub
    Shared Sub ConvertFromByteArray2UInt32(arrayByte As Byte(), ByRef o_u32Val As UInt32, Optional ByVal bBigEndian As Boolean = True)
        ConvertFromByteArray2UInt32(arrayByte, 0, o_u32Val, bBigEndian)
    End Sub
    Shared Sub ConvertFromByteArray2UInt32(arrayByte As Byte(), nPosOff As Integer, ByRef o_u32Val As UInt32, Optional ByVal bBigEndian As Boolean = True)
        Dim byte1, byte2, byte3, byte4 As Byte
        If (arrayByte Is Nothing) Then
            Throw New Exception("ConvertFromByteArray2UInt32() : Array Data is NULL")
        End If
        If (arrayByte.Count < 4) Then
            Throw New Exception("ConvertFromByteArray2UInt32() : Array Size is smaller than 2")
        End If
        'Default is Big Endian
        byte1 = arrayByte(nPosOff)
        nPosOff = nPosOff + 1
        byte2 = arrayByte(nPosOff)
        nPosOff = nPosOff + 1
        byte3 = arrayByte(nPosOff)
        nPosOff = nPosOff + 1
        byte4 = arrayByte(nPosOff)
        nPosOff = nPosOff + 1

        'If Not B.E.
        If Not bBigEndian Then
            ByteExchange(byte1, byte4)
            ByteExchange(byte2, byte3)
        End If

        o_u32Val = (((CType(byte1, UInt32) And &HFF) << 24) And &HFF000000) + (((CType(byte2, UInt32) And &HFF) << 16) And &HFF0000) + (((CType(byte3, UInt32) And &HFF) << 8) And &HFF00) + (CType(byte4, UInt32) And &HFF)
    End Sub
    Shared Sub ConvertFromByteArray2UInt16(arrayByte As Byte(), ByRef o_u16Val As UInt16, Optional ByVal bBigEndian As Boolean = True)
        ConvertFromByteArray2UInt16(arrayByte, 0, o_u16Val, bBigEndian)
    End Sub
    Shared Sub ConvertFromByteArray2UInt16(arrayByte As Byte(), nPosOff As Integer, ByRef o_u16Val As UInt16, Optional ByVal bBigEndian As Boolean = True)
        Dim byte1, byte2 As Byte

        If (arrayByte Is Nothing) Then
            Throw New Exception("ConvertFromByteArray2UInt32() : Array Data is NULL")
        End If
        If (arrayByte.Count < 2) Then
            Throw New Exception("ConvertFromByteArray2UInt16() : Array Size is smaller than 2")
        End If
        'Default is Big Endian
        byte1 = arrayByte(nPosOff)
        nPosOff = nPosOff + 1
        byte2 = arrayByte(nPosOff)
        nPosOff = nPosOff + 1

        'If Not B.E.
        If Not bBigEndian Then
            ByteExchange(byte1, byte2)
        End If

        o_u16Val = (((CType(byte1, UInt32) And &HFF) << 8) And &HFF00) + (CType(byte2, UInt32) And &HFF)
    End Sub
    '
    Shared Sub ConvertValue2ArrayByte(ByVal bData As Boolean, ByRef o_arrayByte As Byte())
        Dim byteVal As Byte
        If (bData) Then
            byteVal = &H1
        Else
            byteVal = &H0
        End If
        '
        o_arrayByte = New Byte() {byteVal}
    End Sub

    Shared Sub ConvertFromArrayByte2StringsList(arrayByteData As Byte(), ByRef o_strFwVersionList As List(Of String), Optional ByVal bListClear As Boolean = False)
        Dim strTmp As String = ""
        Dim strTmpLst As String() = Nothing
        Dim nIdx, nIdxMax As Integer
        Try
            'Cleanup List
            If (o_strFwVersionList Is Nothing) Then
                o_strFwVersionList = New List(Of String)
            Else
                If (bListClear) Then
                    o_strFwVersionList.Clear()
                End If
            End If
            ' Find vbCr
            ClassTableListMaster.ConvertFromHexToAsciiOnlyReadable(arrayByteData, 0, arrayByteData.Count, strTmp)
            '
            If (strTmp.Contains(vbCrLf)) Then
                strTmpLst = Split(strTmp, vbCrLf)
                '
                nIdxMax = strTmpLst.Count - 1
                For nIdx = 0 To nIdxMax
                    strTmp = strTmpLst(nIdx)
                    strTmp = Replace(strTmp, ",,", "")
                    o_strFwVersionList.Add(strTmp)
                Next
            Else
                o_strFwVersionList.Add(strTmp)
            End If
        Catch ex As Exception
            Throw 'ex
        Finally
            If strTmpLst IsNot Nothing Then
                Erase strTmpLst
            End If
        End Try
        '
    End Sub

    Private Shared Sub ConvertFromHexToAsciiOnlyReadable(arrayByteData As Byte(), nPosStart As Integer, nLen As Integer, ByRef o_strTmp As String)
        Dim nIdx, nIdxMax As Integer

        'nIdxMax = arrayByteData.Count - 1
        nIdxMax = nPosStart + nLen - 1
        For nIdx = nPosStart To nIdxMax
            o_strTmp = o_strTmp + Chr(arrayByteData(nIdx))
        Next
    End Sub

    Shared Sub SubtractByteData(ByVal arrayTLVal As Byte(), ByRef o_nPosStart As Byte, ByVal byteDelimiter As Byte, ByRef o_arrayByteData As Byte())
        Dim nIdx, nIdxMax, nLen As Integer

        If (o_arrayByteData IsNot Nothing) Then
            Erase o_arrayByteData
        End If
        nLen = 0
        nIdxMax = arrayTLVal.Count - 1
        If (nIdx > nIdxMax) Then
            Throw New Exception("[Err] SubByteData() = Out of Range, Start Pos Index ( " & nIdx & " ) > The Max Index ( " & nIdxMax & " )")
        End If
        '
        For nIdx = o_nPosStart To nIdxMax
            If arrayTLVal(nIdx) = byteDelimiter Then
                'If data presents, then duplicate it
                If (nLen > 0) Then
                    o_arrayByteData = New Byte(nLen - 1) {}
                    Array.Copy(arrayTLVal, o_nPosStart, o_arrayByteData, 0, nLen)
                End If
                ' Move to next data position, +1 = byteDelimiter size
                o_nPosStart = nIdx + 1
                Exit Sub
            End If
            '
            nLen = nLen + 1
        Next
        '
        Throw New Exception("[Err] SubByteData() = No Delimiter ( " + String.Format("{0,2:X2}", byteDelimiter) + " ) Found")
    End Sub

    Public Shared Sub TLVListsAddFirstList2SecondList(dictTLVs As Dictionary(Of String, tagTLV), ByRef o_dictTLVsTxnSend As SortedDictionary(Of String, tagTLV))
        Dim Log As Form01_Main.LogLnDelegate = New Form01_Main.LogLnDelegate(AddressOf Form01_Main.GetInstance().LogLn)

        If (dictTLVs Is Nothing) Then
            Throw New Exception("[Err] TLVListsAddFirstList2SecondList() = 1st parameter failed")
        End If
        If (o_dictTLVsTxnSend Is Nothing) Then
            Throw New Exception("[Err] TLVListsAddFirstList2SecondList() = 2nd parameter failed")
        End If
        '
        For Each iteratorIn In dictTLVs
            If (o_dictTLVsTxnSend.ContainsKey(iteratorIn.Key)) Then
                Form01_Main.GetInstance().Log("[Warning] tagTLV.TLValueAdd() = Source TLV Dict's tag = " + iteratorIn.Key + " Exists in Dest TLV Dict")
            Else
                o_dictTLVsTxnSend.Add(iteratorIn.Key, iteratorIn.Value)
            End If
        Next
    End Sub
    '
    '
    Shared Sub TrimFromPosOfStrArray(TestArray As String(), nPos As Integer, ByRef strIDHex As String)
        Dim sb As StringBuilder = New StringBuilder
        Dim nIdx As Integer

        For nIdx = nPos To TestArray.Count - 2
            sb.Append(TestArray(nIdx) + " ")
        Next
        sb.Append(TestArray(TestArray.Count - 1))
        strIDHex = sb.ToString()

    End Sub

    Private Shared m_s_objInstance As ClassTableListMaster
    Public Shared Function GetInstance() As ClassTableListMaster
        If (m_s_objInstance Is Nothing) Then
            m_s_objInstance = New ClassTableListMaster
        End If

        Return m_s_objInstance
    End Function

    Shared Sub ConvertFromArrayByte2UInt32(arrayByteData As Byte(), ByRef o_u32ErrStatusCode As UInt32, Optional bIsBigEndian As Boolean = True)
        Dim nIdx, nIdxMax As Integer
        Dim nIdxMin, nIdxOff As Integer

        'Only to retrieve first 4-byte data from arrayByteData()
        o_u32ErrStatusCode = 0
        If (arrayByteData.Count < 4) Then
            Throw New Exception("arrayByteData size is not enough, current (" & arrayByteData.Count & ") < 4")
        End If

        If (bIsBigEndian) Then
            nIdxMin = 0
            nIdxMax = 3
            nIdxOff = 1
        Else
            nIdxMin = 3
            nIdxMax = 0
            nIdxOff = -1
        End If
        For nIdx = nIdxMin To nIdxMax Step nIdxOff
            o_u32ErrStatusCode = (o_u32ErrStatusCode << 8) Or arrayByteData(nIdx)
        Next
    End Sub

    '
    Shared Function StrNormalized(strTxt As String) As String
        Dim strTmpNormalized As String

        strTmpNormalized = strTxt

        If (Not strTmpNormalized.Equals("")) Then
            strTmpNormalized = Replace(strTmpNormalized, " ", "")
            strTmpNormalized = Replace(strTmpNormalized, vbCr, "")
            strTmpNormalized = Replace(strTmpNormalized, vbLf, "")
        End If

        Return strTmpNormalized
    End Function
    '
    'Return : -1 = Invalid Arg 'arrayByteSearchIn' or Not Found
    '         -2 = Invalid Arg 'arrayByteSearchFor'
    '         -3 = In's Len < For's Len
    '         >= 0, Found
    Public Shared Function ArrayByteSubPatternIndexOf(arrayByteSearchIn As Byte(), nStrInPos As Integer, arrayByteSearchFor As Byte(), nStrForPos As Integer, Optional nStrInLen As Integer = -1) As Integer
        Dim nIdx, nStartIdx, nLenIn, nLenInMax, nLenFor As Integer
        Dim bFound As Boolean

        If arrayByteSearchIn.Equals(Nothing) Then
            Return -1
        End If
        If arrayByteSearchFor.Equals(Nothing) Then
            Return -2
        End If

        If (nStrInLen = -1) Then
            nLenIn = arrayByteSearchIn.Length - nStrInPos 'Make sure start offset is shifted
        Else
            nLenIn = nStrInLen
        End If
        nLenFor = arrayByteSearchFor.Length

        'If (nLenIn > nLenFor) Then
        '    Return -3
        'End If
        If (nLenIn < nLenFor) Then
            Return -3
        End If

        nLenIn = nLenIn - nLenFor + 1 'Skip Last Check
        nLenInMax = (nLenIn + nStrInPos) - 1

        For nStartIdx = nStrInPos To nLenInMax
            bFound = True
            For nIdx = nStrForPos To nLenFor - 1
                If (arrayByteSearchIn(nStartIdx + nIdx) <> arrayByteSearchFor(nIdx)) Then
                    bFound = False
                    nIdx = nLenFor - 1
                End If
            Next
            If (bFound) Then
                Exit For
            End If
        Next

        If (Not bFound) Then
            'Not Found, set error length
            nStartIdx = -1
        End If

        Return nStartIdx
    End Function

    'Ex: [IN] strTmpSrc = "00112233445", chDelimiter = " "
    '      [Return]             = "00 11 22 33 44 5"
    Shared Function TLVauleFromString2DelimiterString(strTmpSrc As String, chDelimiter As String) As String
        Dim strTmpResult As String = ""
        Dim nCount As Integer = 0
        '
        For Each itrChr In strTmpSrc
            strTmpResult = strTmpResult + itrChr
            nCount = nCount + 1
            If (nCount = 2) Then
                nCount = 0
                strTmpResult = strTmpResult + chDelimiter
            End If
        Next
        '
        Return strTmpResult
    End Function

    '[OUT] = Modified TLV List if Done
    '[Ret] = tagTLV : Done, Found and Removed
    '          = Nothing : Not Found
    ' [Exception] = Invalid Params
    Shared Function TLVListRemoveOne(ByRef io_sdictTLVsGlobalVars01 As SortedDictionary(Of String, tagTLV), ByVal strTLVName As String) As tagTLV
        'Dim bResult As Boolean = False
        Dim tagTlv As tagTLV = Nothing
        Try
            If (io_sdictTLVsGlobalVars01 Is Nothing) Then
                Throw New Exception("Input TLV List Invalid")
            End If
            If (io_sdictTLVsGlobalVars01.Count < 1) Then
                Throw New Exception("Input TLV List Empty")
            End If
            If (Not io_sdictTLVsGlobalVars01.ContainsKey(strTLVName)) Then
                Exit Try
            End If
            tagTlv = io_sdictTLVsGlobalVars01.Item(strTLVName)
            io_sdictTLVsGlobalVars01.Remove(strTLVName)
            'bResult = True
        Catch ex As Exception
            Throw
        Finally
            '
        End Try

        Return tagTlv
    End Function
    ' Description:
    '  Compare 2 TLVs List with each other exception TLV : 9A, 9F21
    '[Ret] = 1 : Equaled
    '               0 : Mismatched, some TLV(s) has different values
    '[Exception] Invalid Param
    Shared Function TLVListCompareForGlobalVars(sdictTLVsGlobalVars01 As SortedDictionary(Of String, tagTLV), sdictTLVsGlobalVars02 As SortedDictionary(Of String, tagTLV)) As Integer
        Dim bResult As Boolean = False
        Dim nRet As Integer = -1
        Dim itrTLV02 = Nothing, tagTlv01_DF891B = Nothing, tagTlv01_9A = Nothing, tagTlv01_9F21 = Nothing, tagTlv02_DF891B = Nothing, tagTlv02_9A = Nothing, tagTlv02_9F21 As tagTLV = Nothing
        Dim itrTLV01 As KeyValuePair(Of String, tagTLV)
        Dim nCount As Integer = 0
        Dim nCountForNothingVal As Integer = 0
        Dim bChkLoopFailed As Boolean = False
        Try
            '0.Validation
            If (sdictTLVsGlobalVars01 Is Nothing) Then
                Throw New Exception("01 param is Invalid")
            End If
            If (sdictTLVsGlobalVars02 Is Nothing) Then
                Throw New Exception("02  param is Invalid")
            End If
            If (sdictTLVsGlobalVars01.Count < 1) Then
                Throw New Exception("01 param's size is Empty")
            End If
            If (sdictTLVsGlobalVars02.Count < 1) Then
                Throw New Exception("02 param's size is Empty")
            End If
            '
            nRet = 0
            '1.Remove 9A, 9F21 First, from each list
            tagTlv01_9A = TLVListRemoveOne(sdictTLVsGlobalVars01, tagTLV.TAG_9A_Date)
            tagTlv01_9F21 = TLVListRemoveOne(sdictTLVsGlobalVars01, tagTLV.TAG_9F21_Time)

            tagTlv02_9A = TLVListRemoveOne(sdictTLVsGlobalVars02, tagTLV.TAG_9A_Date)
            tagTlv02_9F21 = TLVListRemoveOne(sdictTLVsGlobalVars02, tagTLV.TAG_9F21_Time)

#If 0 Then
            tagTlv01_DF891B = TLVListRemoveOne(sdictTLVsGlobalVars01, tagTLV.TAG_DF891B_PollMode)
            tagTlv02_DF891B = TLVListRemoveOne(sdictTLVsGlobalVars02, tagTLV.TAG_DF891B_PollMode)
#End If

            '2.Check Size
            If (sdictTLVsGlobalVars01.Count <> sdictTLVsGlobalVars02.Count) Then
                Exit Try
            End If

            '3.Check Each TLV
            For Each itrTLV01 In sdictTLVsGlobalVars01
                'Reset Vars
                nCountForNothingVal = 0

                '3.1 Found One
                If (Not sdictTLVsGlobalVars02.ContainsKey(itrTLV01.Key)) Then
                    Dim i As Integer ' For Debug Purpose ONLY
                    i = 0
                    Exit Try
                End If
                '3.2 Check Value
                itrTLV02 = sdictTLVsGlobalVars02.Item(itrTLV01.Key)

                'Special Case, m_arrayTLVal = Nothing
                If (itrTLV01.Value.m_arrayTLVal Is Nothing) Then
                    nCountForNothingVal = nCountForNothingVal + 1
                End If
                If (itrTLV02.m_arrayTLVal Is Nothing) Then
                    nCountForNothingVal = nCountForNothingVal + 1
                End If

                Select Case (nCountForNothingVal)
                    Case 2
                        ' Both are Nothing ==> Same value --> Continue.
                    Case 0 ' Both Values are available. Check to each other.
                        If (Not itrTLV01.Value.m_arrayTLVal.SequenceEqual(itrTLV02.m_arrayTLVal)) Then
                            bChkLoopFailed = True
                            Exit For
                        End If
                    Case Else ' One of values is Nothing --> Failed
                        bChkLoopFailed = True
                        Exit For
                End Select

                '
                nCount = nCount + 1
            Next
            'Failed Check
            If (bChkLoopFailed) Then
                Exit Try
            End If
            '
            nRet = 1
        Catch ex As Exception
            Throw
        Finally
            'Restore TLV back it if possible
            If (Not tagTlv01_9A.Equals(Nothing)) Then
                sdictTLVsGlobalVars01.Add(tagTLV.TAG_9A_Date, tagTlv01_9A)
            End If
            If (Not tagTlv01_9F21.Equals(Nothing)) Then
                sdictTLVsGlobalVars01.Add(tagTLV.TAG_9F21_Time, tagTlv01_9F21)
            End If
            If (Not tagTlv02_9A.Equals(Nothing)) Then
                sdictTLVsGlobalVars02.Add(tagTLV.TAG_9A_Date, tagTlv02_9A)
            End If
            If (Not tagTlv02_9F21.Equals(Nothing)) Then
                sdictTLVsGlobalVars02.Add(tagTLV.TAG_9F21_Time, tagTlv02_9F21)
            End If
            '
#If 0 Then
            If (Not tagTlv01_DF891B.Equals(Nothing)) Then
                sdictTLVsGlobalVars01.Add(tagTLV.TAG_DF891B_PollMode, tagTlv01_DF891B)
            End If
            If (Not tagTlv02_DF891B.Equals(Nothing)) Then
                sdictTLVsGlobalVars02.Add(tagTLV.TAG_DF891B_PollMode, tagTlv02_DF891B)
            End If
#End If
            '
        End Try

        Return nRet
    End Function

    Public Shared Function ConvertFromAscii2HexString(i_str As String) As String
        Dim o_str As String = ""
        Dim strTmp As String
        '
        For Each c As Char In i_str
            strTmp = Convert.ToString(Convert.ToInt32(c), 16)
            If (strTmp.Length < 2) Then
                strTmp = "0" + strTmp
            End If
            o_str &= strTmp
        Next
        '
        Return o_str.ToUpper()
    End Function

    Shared Sub TLVListCombine(sdictGlobTLVs As SortedDictionary(Of String, tagTLV), ByRef io_sdictGlobalTLVsChk_Step03 As SortedDictionary(Of String, tagTLV))
        Dim tlv As tagTLV
        For Each itr In sdictGlobTLVs
            If (io_sdictGlobalTLVsChk_Step03.ContainsKey(itr.Key)) Then
                'Update the presented TLV value
                tlv = io_sdictGlobalTLVsChk_Step03.Item(itr.Key)
                tlv.strVal = itr.Value.strVal
            Else
                'Add to it
                tlv = New tagTLV(itr.Key, itr.Value.strVal)
                io_sdictGlobalTLVsChk_Step03.Add(itr.Key, tlv)
            End If
        Next
    End Sub

    '2017 Feb 06, 
    'ref URL : http://stackoverflow.com/questions/627742/what-is-the-fastest-way-to-compare-two-byte-arrays
    Shared Function ArrayByteCompare(first As Byte(), Second As Byte()) As Boolean
        If (first Is Second) Then
            Return True
        End If

        If (first Is Nothing OrElse Second Is Nothing) Then
            Return False
        End If
        If (first.Length <> Second.Length) Then
            Return False
        End If

        For i As Integer = 0 To first.Length - 1
            If (first(i) <> Second(i)) Then
                Return False
            End If
        Next i
        Return True
    End Function

    'Ex01: [IN] "170131", [OUT]= {&H11,&H1,&H1E}
    'Ex02: [IN] "1211217", [OUT]= {&H1,&H15,&HC,&H11}
    Shared Sub TLVauleFromValueString2ByteArray(i_strDate_YYMMDD As String, ByRef o_aryDatTmp As Byte())

        Dim nIdx As Integer
        Dim nIdxMax As Integer
        Dim strTmp As String
        If (o_aryDatTmp IsNot Nothing) Then
            Erase o_aryDatTmp
        End If
        o_aryDatTmp = Nothing

        If (i_strDate_YYMMDD.Length < 1) Then
            Return
        End If

        If ((i_strDate_YYMMDD.Length Mod 2) > 0) Then
            'Add "0" to high digit if it
            i_strDate_YYMMDD = "0" + i_strDate_YYMMDD
        End If
        Try
            '"170131" --> Split --> {"17","01","31"}
            nIdxMax = i_strDate_YYMMDD.Length - 1
            o_aryDatTmp = New Byte((nIdxMax + 1) \ 2 - 1) {}
            For nIdx = 0 To nIdxMax
                strTmp = i_strDate_YYMMDD.Substring(nIdx, 2)
                o_aryDatTmp(nIdx \ 2) = Convert.ToByte(strTmp)
                '
                nIdx = nIdx + 1
            Next
        Catch ex As Exception
            Throw
        Finally
        End Try
    End Sub
End Class
