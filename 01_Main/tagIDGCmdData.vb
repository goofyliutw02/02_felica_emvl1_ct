﻿'==================================================================================
' File Name: tagIDGCmdData.vb
' Description: The Structure tagIDGCmdData is as IDG Command Parameter Control Block Type
' Who use it : 
' Revision:
' 2014 Sept 25 Updated and isolation to a single VB file
' -----------------------[ Initial Version ]-----------------------
' 2014 May 30, by goofy_liu@idtechproducts.com.tw
'==================================================================================


Public Structure tagIDGCmdData
    'Response, Remember to reset it to default = &HFF as non-used status 
    'This member is checked to be
    Public m_nResponse As Integer
    Public m_byteV1FrameType As ClassReaderProtocolBasic.EnumProtocolV1FrameType
    Public m_byteV1FrameDataLen As Byte
    Public m_bFrameDataFollowed As Boolean

    Private m_bInterruptWait As Boolean

    '
    Public m_refarraybyteData() As Byte
    Public m_refarraybyteDataExtraUnknown() As Byte

    Public m_nProtocol As Integer

    'For Command Set
    Public m_u16CmdSet As UInt16

    'For Group ID
    Public m_nGroupID As Integer
    Public m_nTransactionType As Integer

    ' For AID

    'For CRL

    'For CAPK

    'For Communication MISC
    Public m_nDataRecvCmdRespLenInBytes As Integer
    Public m_nDataRecvOff As Integer
    'Public m_nDataSendOff As Integer
    Public m_nDataSizeRecv As Integer
    Public m_nDataSizeSend As Integer
    Public m_nDataSizeRecvRaw As Integer
    Public m_bIsCRCChkDone As Boolean

    Private m_refobjIDGCmder As ClassReaderProtocolBasic
    Private m_refobjTxRxer As ClassRdrCommNoPrtcl
    Public m_byteCmdTx As Byte
    Public m_byteCmdRx As Byte

    Enum EnumDEFAULTVAL As Integer
        RESPONSE = &HFF
    End Enum
    Public Shared m_nPktHeader As Integer = 14
    Public Shared m_nPktHeaderV1 As Integer = 10
    Public Shared m_nPktTrail As Integer = 2

    '=======[ Note ]========
    'For Audio Jack <---> RS232(COM) Communication
    ' Preamble data from Reader "55 55 ... 55 66" (32 bytes) are filtered frmo Rx Packet
    ' m_nDataRecvOffSkipBytes is the length of the preamble data
    Public m_nDataRecvOffSkipBytes As Integer
    Public m_nDataRecvOffSkipBytes2_PrefixGarbage As Integer
    Public m_nDataSizeRecvExtraUnknownOff As Integer

    ReadOnly Property bIsV1ResponseOK As Boolean
        Get
            If (m_byteV1FrameDataLen > 0) Then
                'There is a followed Data/Special Frame after ACK Frame, we don't get correct response code until Data/Special Frame is received
                Return False
            End If
            Return (m_nResponse = ClassReaderProtocolBasic.EnumSTATUSCODE1.x00OK)
        End Get
    End Property
    ReadOnly Property bIsV1ResponseFailNAK As Boolean
        Get
            Return (m_nResponse = ClassReaderProtocolBasic.EnumSTATUSCODE1.x07Failed)
        End Get
    End Property
    ReadOnly Property bIsResponseOK As Boolean
        Get
            If (ClassReaderProtocolBasic.m_dictProtocolV1FrameTypeResponse.ContainsKey(m_byteV1FrameType)) Then
                Return bIsV1ResponseOK
            End If
            Return (m_nResponse = ClassReaderProtocolBasic.EnumSTATUSCODE2.x00OK)
        End Get
    End Property
    ReadOnly Property bIsResponseTimeout As Boolean
        Get
            Return (m_nResponse = ClassReaderProtocolBasic.EnumSTATUSCODE2.x08Timeout)
        End Get
    End Property
    ReadOnly Property bIsResponseFailNAK As Boolean
        Get

            If (ClassReaderProtocolBasic.m_dictProtocolV1FrameTypeResponse.ContainsKey(m_byteV1FrameType)) Then
                Return bIsV1ResponseFailNAK
            End If
            Return (m_nResponse = ClassReaderProtocolBasic.EnumSTATUSCODE2.x0AFailedNACK)
        End Get
    End Property
    ReadOnly Property bIsResponseNotAllowed As Boolean
        Get
            Return (m_nResponse = ClassReaderProtocolBasic.EnumSTATUSCODE2.x0BCommandNotAllowed)
        End Get
    End Property
    ReadOnly Property byteGetResponseCode As Byte
        Get
            Dim byteIdx As Byte = CType(m_nResponse, Byte)

            Return byteIdx
        End Get
    End Property
    ReadOnly Property strGetResponseMsg As String
        Get
            Dim byteIdx As Byte = CType(m_nResponse, Byte)

            If Not ClassReaderProtocolBasic.m_dictStatusCode2.ContainsKey(byteIdx) Then
                Return "N/A, RC = " & byteIdx
            End If

            Return ClassReaderProtocolBasic.m_dictStatusCode2.Item(byteIdx)
        End Get
    End Property

    '===============[ Interrupt Handling ]===============
    Enum EnumInterruptFSM As Integer
        COMM_INTERRUPT_FSM__MIN = -1

        COMM_INTERRUPT_FSM_IDLE = 0
        COMM_INTERRUPT_FSM_ACT
        COMM_INTERRUPT_FSM_STOPPING_IDG
        COMM_INTERRUPT_FSM_STOPPING_TIMEOUT
        COMM_INTERRUPT_FSM_STOPPED

        COMM_INTERRUPT_FSM__MAX
    End Enum
    Private Shared strMsgInterruptFSM As String() = New String() {
        "N/A, MIN",
        "IDLE",
        "ACT",
         "STOPPING_IDG",
        "STOPPING_TIMEOUT",
        "STOPPED",
        "N/A, MAX"
        }
    Private m_nCommInterruptFSM As EnumInterruptFSM
    ReadOnly Property CallByIDGCmdTimeOutChk_IsCommAct As Boolean
        Get
            Return (m_nCommInterruptFSM = EnumInterruptFSM.COMM_INTERRUPT_FSM_ACT)
        End Get
    End Property
    ReadOnly Property CallByIDGCmdTimeOutChk_IsCommIdle As Boolean
        Get
            Return (m_nCommInterruptFSM = EnumInterruptFSM.COMM_INTERRUPT_FSM_IDLE)
        End Get
    End Property
    ReadOnly Property CallByIDGCmdTimeOutChk_IsCommIDGCancelStop As Boolean
        Get
            Return (m_nCommInterruptFSM = EnumInterruptFSM.COMM_INTERRUPT_FSM_STOPPING_IDG)
        End Get
    End Property
    ReadOnly Property CallByIDGCmdTimeOutChk_IsCommTimeOutCancelStop As Boolean
        Get
            Return (m_nCommInterruptFSM = EnumInterruptFSM.COMM_INTERRUPT_FSM_STOPPING_TIMEOUT)
        End Get
    End Property
    ReadOnly Property CallByIDGCmdTimeOutChk_IsCommStopped As Boolean
        Get
            Return (m_nCommInterruptFSM = EnumInterruptFSM.COMM_INTERRUPT_FSM_STOPPED)
        End Get
    End Property
    ReadOnly Property CallByIDGCmdTimeOutChk_GetStrCommFSM As String
        Get
            Return strMsgInterruptFSM(m_nCommInterruptFSM)
        End Get
    End Property
    '
    Public Shared m_s_nV2HdrPos As Integer = -1

    ReadOnly Property bIsResponseFail_x45_NoSerialNumber As Boolean
        Get
            Return (m_nResponse = ClassReaderProtocolBasic.EnumSTATUSCODE2.x45_NEO20_9002_EnableTamperFailed_NoSerialNumber)
        End Get
    End Property

    Property bIsTxRxCmdOk As Boolean

    '
    'Note 01 - This property will decide 'm_nDataRecvOffSkipBytes' value
    Private Function bIsVHdrReady_Generic(ByVal nHeaderLen As ClassReaderProtocolBasic.EnumIDGCmdOffset, ByVal arrayBytesVHeader As Byte()) As Boolean
        Dim bVerifiedPass As Boolean = False
        Dim nStartOff As Integer = 0
        Dim nIdxCmp, nIdxCmpMax As Integer
        Dim nIdxStd As Integer
        Dim nStartOffMax As Integer
        '
        '
        m_s_nV2HdrPos = -1
        '
        nStartOffMax = m_nDataRecvOff - nHeaderLen - 1
#If 0 Then
            nIdxStd = ClassTableListMaster.ArrayByteSubPatternIndexOf(m_refarraybyteData, m_nDataRecvOffSkipBytes, arrayBytesV2Header, -1)
            bVerifiedPass = (nIdxStd > -1)

            If (bVerifiedPass) Then
                m_nDataRecvOffSkipBytes = nIdxStd
            Else
                m_nDataRecvOffSkipBytes = nStartOffMax
            End If
#Else
        'For nStartOff = m_nDataRecvOffSkipBytes To nStartOffMax
        For nStartOff = m_nDataRecvOffSkipBytes2_PrefixGarbage To nStartOffMax
            '
            nIdxCmpMax = nStartOff + nHeaderLen - 1
            nIdxStd = 0
            For nIdxCmp = nStartOff To nIdxCmpMax
                '
                If (m_refarraybyteData(nIdxCmp) <> arrayBytesVHeader(nIdxStd)) Then
                    Exit For
                End If
                '
                nIdxStd = nIdxStd + 1
            Next
            'Paired Length is good or not
            If (nIdxStd = nHeaderLen) Then
                bVerifiedPass = True
                'm_nDataRecvOffSkipBytes = nStartOff
                'Remember Found V2 Header Start Address for later use
                m_s_nV2HdrPos = nStartOff
                Exit For
            End If
        Next 'For nStartOff...

        'm_nDataRecvOffSkipBytes = nStartOff
        '
        'Erase arrayBytesV2Header
        If (bVerifiedPass) Then
            m_nDataRecvOffSkipBytes = m_s_nV2HdrPos
        Else
            m_nDataRecvOffSkipBytes = nStartOffMax
        End If
        '
#End If
        '
        Return bVerifiedPass
    End Function
    '
    Private ReadOnly Property bIsV2HdrReady As Boolean
        Get

            Dim nHeaderLen As Integer = ClassReaderProtocolBasic.EnumIDGCmdOffset.Protocol2_OffSet_CmdMain_Len_1B
            Dim bVerifiedPass As Boolean = False
            Dim arrayBytesV2Header As Byte() = New Byte() {&H56, &H69, &H56, &H4F, &H74, &H65, &H63, &H68, &H32, &H0}

            '
            If (m_nDataRecvOff < nHeaderLen) Then
                Return False
            End If


            bVerifiedPass = bIsVHdrReady_Generic(nHeaderLen, arrayBytesV2Header) 'bVerifiedPass
            'Free
            Erase arrayBytesV2Header

            Return bVerifiedPass
        End Get
    End Property
    '
    Private ReadOnly Property bIsV3HdrReady As Boolean
        Get

            Dim nHeaderLen As Integer = ClassReaderProtocolBasic.EnumIDGCmdOffset.Protocol3_OffSet_CmdMain_Len_1B
            Dim bVerifiedPass As Boolean = False
            Dim arrayBytesV3Header As Byte() = New Byte() {&H56, &H69, &H56, &H4F, &H70, &H61, &H79, &H56, &H33, &H0}
            '
            If (m_nDataRecvOff < nHeaderLen) Then
                Return False
            End If
            '
            bVerifiedPass = bIsVHdrReady_Generic(nHeaderLen, arrayBytesV3Header) 'bVerifiedPass
            'Free
            Erase arrayBytesV3Header

            Return bVerifiedPass
        End Get
    End Property
    '
    ReadOnly Property bIsV3HdrReadyRawSizeReady As Boolean
        Get
            Dim nReadyCnt As Integer = 0

            '1. Header Ready : Find V2 Header "ViVOPay2"+"\0"
            If (bIsV3HdrReady) Then
                nReadyCnt = nReadyCnt + 1

                '2. Raw Size Ready: 
                If m_nDataSizeRecv = 0 Then
                    If (m_nDataRecvOff > (m_nDataRecvOffSkipBytes + ClassReaderProtocolBasic.EnumIDGCmdOffset.Protocol3_OffSet_Rx_DataLen_LSB_Len_1B)) Then

                        m_nDataSizeRecvRaw = ((CType(m_refarraybyteData(m_nDataRecvOffSkipBytes + ClassReaderProtocolBasic.EnumIDGCmdOffset.Protocol3_OffSet_Rx_DataLen_MSB_Len_1B), UInt16) << 8) And &HFF00) + CType(m_refarraybyteData(m_nDataRecvOffSkipBytes + ClassReaderProtocolBasic.EnumIDGCmdOffset.Protocol3_OffSet_Rx_DataLen_LSB_Len_1B), UInt16)
                        '2017 Sept 25, ViVOpayV3<00><Cmd,Sub-Cmd><Sts Code><LenH,LenLow><*Raw Data*><CRC,2Bs>
                        ' +1 for Sub-Cmd byte, Added in V3 protocol.
                        m_nDataSizeRecv = tagIDGCmdData.m_nPktHeader + m_nDataSizeRecvRaw + tagIDGCmdData.m_nPktTrail + 1
                        '2017 July 27, goofy_liu
                        'One more check, Recv Data Length must equal or be longer than Off+Pkt Size
                        'If (m_nDataRecvOff >= (m_nDataSizeRecv + m_nDataRecvOffSkipBytes)) Then
                        nReadyCnt = nReadyCnt + 1
                        'End If
                    End If
                Else
                    '2017 July 27, goofy_liu
                    'One more check, Recv Data Length must equal or be longer than Off+Pkt Size
                    'If (m_nDataRecvOff >= (m_nDataSizeRecv + m_nDataRecvOffSkipBytes)) Then
                    nReadyCnt = nReadyCnt + 1
                    'End If
                End If
            End If

            Return (nReadyCnt >= 2)
        End Get
    End Property

    '2017 Sept 21, to Support ViVOpayV3 Commands.
    Private Shared m_s_nVivoProctlType As ClassReaderInfo.ENU_PROTCL_VER = ClassReaderInfo.ENU_PROTCL_VER._V2
    '

    ReadOnly Property bIsVHdrReadyRawSizeReady As Boolean
        Get
            bIsVHdrReadyRawSizeReady_TempSave()

            If (bIsV2HdrReadyRawSizeReady) Then
                m_s_nVivoProctlType = ClassReaderInfo.ENU_PROTCL_VER._V2
                Return True
            End If
            '
            bIsVHdrReadyRawSizeReady_TempRestore()
            '
            If (bIsV3HdrReadyRawSizeReady) Then
                m_s_nVivoProctlType = ClassReaderInfo.ENU_PROTCL_VER._V3
                Return True
            End If
            '
            Return False
        End Get
    End Property
    '
    ReadOnly Property bIsV2HdrReadyRawSizeReady As Boolean
        Get
            Dim nReadyCnt As Integer = 0

            '1. Header Ready : Find V2 Header "ViVOPay2"+"\0"
            If (bIsV2HdrReady) Then
                nReadyCnt = nReadyCnt + 1

                '2. Raw Size Ready: 
                If m_nDataSizeRecv = 0 Then
                    If (m_nDataRecvOff > (m_nDataRecvOffSkipBytes + ClassReaderProtocolBasic.EnumIDGCmdOffset.Protocol2_OffSet_Rx_DataLen_LSB_Len_1B)) Then

                        m_nDataSizeRecvRaw = ((CType(m_refarraybyteData(m_nDataRecvOffSkipBytes + ClassReaderProtocolBasic.EnumIDGCmdOffset.Protocol2_OffSet_Rx_DataLen_MSB_Len_1B), UInt16) << 8) And &HFF00) + CType(m_refarraybyteData(m_nDataRecvOffSkipBytes + ClassReaderProtocolBasic.EnumIDGCmdOffset.Protocol2_OffSet_Rx_DataLen_LSB_Len_1B), UInt16)
                        '
                        m_nDataSizeRecv = tagIDGCmdData.m_nPktHeader + m_nDataSizeRecvRaw + tagIDGCmdData.m_nPktTrail
                        '2017 July 27, goofy_liu
                        'One more check, Recv Data Length must equal or be longer than Off+Pkt Size
                        'If (m_nDataRecvOff >= (m_nDataSizeRecv + m_nDataRecvOffSkipBytes)) Then
                        nReadyCnt = nReadyCnt + 1
                        'End If
                    End If
                Else
                    '2017 July 27, goofy_liu
                    'One more check, Recv Data Length must equal or be longer than Off+Pkt Size
                    'If (m_nDataRecvOff >= (m_nDataSizeRecv + m_nDataRecvOffSkipBytes)) Then
                    nReadyCnt = nReadyCnt + 1
                    'End If
                End If
            End If

            Return (nReadyCnt >= 2)
        End Get
    End Property

    WriteOnly Property byteSetResponseCode As Byte
        Set(value As Byte)
            Me.m_nResponse = value
        End Set
    End Property

    'Return Ex: "18-01"
    ReadOnly Property strCmdNEOFormat As String
        Get
            Dim strMsgCmdSub As String = String.Format("{0,2:X2}-{1,2:X2}", (m_u16CmdSet >> 8) And (&HFF), (m_u16CmdSet) And (&HFF))

            Return strMsgCmdSub
        End Get
    End Property

    Public WriteOnly Property SetVivoProctlType As ClassReaderInfo.ENU_PROTCL_VER
        Set(valueType As ClassReaderInfo.ENU_PROTCL_VER)
            m_s_nVivoProctlType = valueType
        End Set
    End Property


    Public ReadOnly Property bIsV2Proctl As Boolean
        Get
            Return m_s_nVivoProctlType = ClassReaderInfo.ENU_PROTCL_VER._V2
        End Get
    End Property

    Public ReadOnly Property bIsV3Proctl As Boolean
        Get
            Return m_s_nVivoProctlType = ClassReaderInfo.ENU_PROTCL_VER._V3
        End Get
    End Property

    Public ReadOnly Property bIsV1Proctl As Boolean
        Get
            Return m_s_nVivoProctlType = ClassReaderInfo.ENU_PROTCL_VER._V1
        End Get
    End Property

    Public Sub CallByIDGCmdTimeOutChk_SetTimeoutEvent()
        If (m_nCommInterruptFSM = EnumInterruptFSM.COMM_INTERRUPT_FSM_ACT) Then
            m_nCommInterruptFSM = EnumInterruptFSM.COMM_INTERRUPT_FSM_STOPPING_TIMEOUT
        End If
    End Sub
    Public Sub CallByIDGCmdTimeOutChk_SetTimeoutEventIDGCancelStop()
        If (m_nCommInterruptFSM = EnumInterruptFSM.COMM_INTERRUPT_FSM_ACT) Then
            m_nCommInterruptFSM = EnumInterruptFSM.COMM_INTERRUPT_FSM_STOPPING_IDG
            '[Special Note] The following statements will halt the program in the single thread architecture
            'Don't Call DoWaitStop()
            'While (CallByIDGCmdTimeOutChk_IsCommActCancelStop)
            '    CallByIDGCmdTimeOutChk_DoWaitStop()
            'End While
        End If

        '2016 Feb 19, goofy_liu@idtechproducts.com.tw
        'Trigger Interruption <--- Consistency in bInterruptedByExternal
        m_bInterruptWait = True
    End Sub

    'Used for Check Inconsistency of Tx & Rx
    ReadOnly Property bIsTxRxCmdEqualed As Boolean
        Get
            Return (m_byteCmdTx = m_byteCmdRx)
        End Get
    End Property

    Public Sub Reinit_Common_MemValsOnly()
        m_nDataRecvOff = 0

        '===================================================
        ' Case to use m_nDataRecvOffSkipBytes field is as the following....
        'Specially while in 02-01 do transaction in Vend Type Reader,
        'MSR swiping incorrectly causes Vendi Machine to printout messages via communicating ports.
        'case 1: Messages are "02 01 6B 4C" at the beginning of buffer after MSR bad swiping.
        '--> m_nDataRecvOffSkipBytes =4
        'case 2: "02 01 6B 4C 02 01 6B 4C" --> m_nDataRecvOffSkipBytes = 8
        ' and so on.
        m_nDataRecvOffSkipBytes = 0
        m_nDataRecvOffSkipBytes2_PrefixGarbage = 0

        'm_nDataSendOff = 0
        m_nDataSizeSend = 0
        m_nDataSizeRecv = 0
        'm_refarraybyteData = refarraybyteData
        'm_refarraybyteDataExtraUnknown = refarrayByteDataExtraUnknown
        m_nDataSizeRecvExtraUnknownOff = 0

        m_nResponse = EnumDEFAULTVAL.RESPONSE ' ClassReaderCommanderIDGAR.STATUSCODE2.x00OK
        'm_u16CmdSet = u16CmdSet
        m_bIsCRCChkDone = False
        'Interrupt FSM
        m_nCommInterruptFSM = EnumInterruptFSM.COMM_INTERRUPT_FSM_IDLE
        m_bInterruptWait = False

        'For Protocol V1 Type
        m_byteV1FrameType = ClassReaderProtocolBasic.EnumProtocolV1FrameType._X
        m_byteV1FrameDataLen = 0
    End Sub
    '
    Private Sub Reinit_Common(ByVal u16CmdSet As UInt16, ByVal refarraybyteData() As Byte, Optional ByVal bFrameDataFollowed As Boolean = False, Optional ByVal refarrayByteDataExtraUnknown() As Byte = Nothing)
        Dim bCarePreRestData As Boolean = False
        m_byteCmdTx = ((u16CmdSet >> 8) And &HFF)
        m_byteCmdRx = &HFF

#If 0 Then
        '2017 July 27.
        'In re usable case of tagIDGCmdData... we need more take care of rest buffer in previous use
        'If there is rest / unknown data, should move to ahead of buffer
        If (m_nDataRecvOff > 0) Then
            If ((m_nDataRecvOffSkipBytes < m_nDataRecvOff)) Then
                If (m_refarraybyteData IsNot Nothing) Then
                    bCarePreRestData = True
                    Array.Copy(m_refarraybyteData, m_nDataRecvOffSkipBytes, m_refarraybyteData, 0, m_nDataRecvOff - m_nDataRecvOffSkipBytes)
                    m_nDataRecvOff = m_nDataRecvOff - m_nDataRecvOffSkipBytes
                End If
            End If
        End If
        If (Not bCarePreRestData) Then
            m_nDataRecvOff = 0
        End If
#Else
        m_nDataRecvOff = 0
#End If


        '===================================================
        ' Case to use m_nDataRecvOffSkipBytes field is as the following....
        'Specially while in 02-01 do transaction in Vend Type Reader,
        'MSR swiping incorrectly causes Vendi Machine to printout messages via communicating ports.
        'case 1: Messages are "02 01 6B 4C" at the beginning of buffer after MSR bad swiping.
        '--> m_nDataRecvOffSkipBytes =4
        'case 2: "02 01 6B 4C 02 01 6B 4C" --> m_nDataRecvOffSkipBytes = 8
        ' and so on.
        m_nDataRecvOffSkipBytes = 0
        m_nDataRecvOffSkipBytes2_PrefixGarbage = 0

        'm_nDataSendOff = 0
        m_nDataSizeSend = 0
        m_nDataSizeRecv = 0
        m_refarraybyteData = refarraybyteData
        m_refarraybyteDataExtraUnknown = refarrayByteDataExtraUnknown
        m_nDataSizeRecvExtraUnknownOff = 0

        m_nResponse = EnumDEFAULTVAL.RESPONSE ' ClassReaderCommanderIDGAR.STATUSCODE2.x00OK
        m_u16CmdSet = u16CmdSet
        m_bIsCRCChkDone = False
        'Interrupt FSM
        m_nCommInterruptFSM = EnumInterruptFSM.COMM_INTERRUPT_FSM_IDLE
        m_bInterruptWait = False

        'For Protocol V1 Type
        m_byteV1FrameType = ClassReaderProtocolBasic.EnumProtocolV1FrameType._X
        m_byteV1FrameDataLen = 0
        m_bFrameDataFollowed = bFrameDataFollowed
        '--------------------------[ ViVOtech IDG Protocol Stuffs ]--------------------------
        'GR Protocol 1 = 13 bytes
        'Header = "ViVOtech\0"

        'GR Protocol 2 = 14 bytes
        'Header = "ViVOtech2\0"
        'm_nPktHeader = 14

        'AR , GR are same in Trail Part = CRC Check , 2 bytes
        'm_nPktTrail = 2
    End Sub
    '
    Public Sub Reinit(ByVal u16CmdSet As UInt16, ByRef refarraybyteData() As Byte, ByRef refobjTxRxer As ClassRdrCommNoPrtcl, Optional ByVal bFrameDataFollowed As Boolean = False, Optional ByVal refarrayByteDataExtraUnknown() As Byte = Nothing)
        '
        m_refobjIDGCmder = Nothing
        m_refobjTxRxer = m_refobjTxRxer
        '
        Reinit_Common(u16CmdSet, refarraybyteData, bFrameDataFollowed, refarrayByteDataExtraUnknown)
    End Sub

    Public Sub Reinit(ByVal u16CmdSet As UInt16, ByRef refarraybyteData() As Byte, ByRef refobjIDGCmder As ClassReaderProtocolBasic, Optional ByVal bFrameDataFollowed As Boolean = False, Optional ByVal refarrayByteDataExtraUnknown() As Byte = Nothing)
        Cleanup()
        '
        m_refobjIDGCmder = refobjIDGCmder
        m_refobjTxRxer = Nothing
        '
        Reinit_Common(u16CmdSet, refarraybyteData, bFrameDataFollowed, refarrayByteDataExtraUnknown)

    End Sub
    '
    Public Sub ReinitForV1DataFrameFollowedMode(Optional ByVal nOffSetMoveFrom As Integer = 0, Optional ByVal nOffsetMoveTo As Integer = 0, Optional ByVal nOffsetMoveLen As Integer = 0)
        'Cleanup()
        m_nDataSizeRecv = 0
        'm_nDataSendOff = 0
        'm_nDataSizeSend = 0
        If ((nOffsetMoveLen > 0) And (nOffSetMoveFrom > nOffsetMoveTo)) Then
            Array.Copy(m_refarraybyteData, nOffSetMoveFrom, m_refarraybyteData, nOffsetMoveTo, nOffsetMoveLen)
            m_nDataRecvOff = nOffsetMoveLen
        Else
            m_nDataRecvOff = 0
        End If

        'm_refarraybyteData = refarraybyteData
        'm_nResponse = EnumDEFAULTVAL.RESPONSE ' ClassReaderCommanderIDGAR.STATUSCODE2.x00OK
        'm_u16CmdSet = u16CmdSet
        m_bIsCRCChkDone = False
        'Interrupt FSM
        m_nCommInterruptFSM = EnumInterruptFSM.COMM_INTERRUPT_FSM_IDLE
        m_bInterruptWait = False

        'For Protocol V1 Type
        m_byteV1FrameType = &H0
        'm_byteV1FrameDataLen = 0
        m_bFrameDataFollowed = False
    End Sub
    '
    Sub New(ByVal u16CmdSet As UInt16, ByRef refarraybyteData() As Byte, ByRef refobjIDGCmder As ClassReaderProtocolBasic)

        Reinit(u16CmdSet, refarraybyteData, refobjIDGCmder)

    End Sub
    '
    Public ReadOnly Property bIsIDGCmderBusy As Boolean
        Get
            If (m_refobjIDGCmder Is Nothing) Then
                Return False
            End If
            Return m_refobjIDGCmder.bIsBusy
        End Get
    End Property

    ' 2017 Feb 03, goofy_liu@idtechproducts.com.tw
    ' For Tx / Rx ONLY, No Protocol allowed
    Public ReadOnly Property bIsTxRxBusy As Boolean
        Get
            If (m_refobjTxRxer Is Nothing) Then
                Return False
            End If
            Return m_refobjTxRxer.bIsTxBusy Or m_refobjTxRxer.bIsRxBusy
        End Get
    End Property

    '
    Public Sub DefaultResponse()
        m_nResponse = EnumDEFAULTVAL.RESPONSE
    End Sub

    '===========[ Properties ]===========
    WriteOnly Property byteCmd As Byte
        Set(value As Byte)
            Dim nLen As Integer = tagIDGCmdData.m_nPktHeader + 1
            Dim nCmdMainOff As Integer = ClassReaderProtocolBasic.EnumIDGCmdOffset.Protocol2_OffSet_CmdMain_Len_1B
            Select Case (m_s_nVivoProctlType)
                Case ClassReaderInfo.ENU_PROTCL_VER._V3
                    nCmdMainOff = ClassReaderProtocolBasic.EnumIDGCmdOffset.Protocol3_OffSet_CmdMain_Len_1B
            End Select

            If m_refarraybyteData Is Nothing Then
                Throw New Exception("[Err] Empty Byte Buffer")
            End If
            '
            If (m_refarraybyteData.Count < nLen) Then
                Throw New Exception("[Err] Buffer size(" & m_refarraybyteData.Count & " is smaller than " + nLen.ToString())
            End If
            '
            m_refarraybyteData(nCmdMainOff) = value
        End Set
    End Property

    WriteOnly Property byteCmdSub As Byte
        Set(value As Byte)
            Dim nLen As Integer = tagIDGCmdData.m_nPktHeader + 1
            Dim nCmdSubOff As Integer = ClassReaderProtocolBasic.EnumIDGCmdOffset.Protocol2_OffSet_CmdSub_Len_1B
            Select Case (m_s_nVivoProctlType)
                Case ClassReaderInfo.ENU_PROTCL_VER._V3
                    nCmdSubOff = ClassReaderProtocolBasic.EnumIDGCmdOffset.Protocol3_OffSet_CmdSub_Len_1B
            End Select


            If m_refarraybyteData Is Nothing Then
                Throw New Exception("[Err] Empty Byte Buffer")
            End If
            '
            If (m_refarraybyteData.Count < nLen) Then
                Throw New Exception("[Err] Buffer size(" & m_refarraybyteData.Count & " is smaller than " + nLen.ToString())
            End If
            '
            m_refarraybyteData(nCmdSubOff) = value
        End Set
    End Property

    Public Sub WaitCommandLoopInterrupt()
        m_bInterruptWait = True
        m_nDataSizeRecv = 0
        m_nResponse = ClassReaderProtocolBasic.EnumSTATUSCODE2.x08Timeout
    End Sub

    ReadOnly Property bIsRecvDataLenReady As Boolean
        Get
            Return (m_nDataSizeRecv > 0)
        End Get
    End Property


    'To be Used In RS232 Recv Thread (System Thread), Not Main Thread
    ReadOnly Property bIsNotResponse As Boolean
        Get
            '
            If (m_bInterruptWait) Then
                Return False
            End If
            '
            Return (m_nResponse = tagIDGCmdData.EnumDEFAULTVAL.RESPONSE)
        End Get
    End Property

    'Called by PreProcessCbRecv . In Main Thread.
    ReadOnly Property bIsRecvDone As Boolean
        Get
            If m_nDataSizeRecv = 0 Then
                Return False
            End If
            '
            If (m_bInterruptWait) Then
                Return True
            End If
            '
            If (Not bIsVHdrReadyRawSizeReady) Then
                Return False
            End If
            '
            Return (((m_nDataRecvOff - m_nDataRecvOffSkipBytes) >= m_nDataSizeRecv)) ' And m_bIsCRCChkDone)
        End Get
    End Property
    ReadOnly Property bIsV1RecvDone As Boolean
        Get
            If m_nDataSizeRecv = 0 Then
                Return False
            End If
            '
            If (m_bInterruptWait) Then
                Return True
            End If
            '
            Return (m_nDataRecvOff >= m_nDataSizeRecv) ' And m_bIsCRCChkDone)
        End Get
    End Property

    ReadOnly Property bIsInterruptedByExternal As Boolean
        Get
            Return m_bInterruptWait
        End Get
    End Property
    WriteOnly Property setInterruptByExternal As Boolean
        Set(value As Boolean)
            m_bInterruptWait = value
        End Set
    End Property

    Public Sub Cleanup()

        '==================[ Special Note of Received Buffer ]=====================
        'This member var points to a huge buffer up 4096 bytes to be as received one,
        'Don't release it here.
        'If (m_refarraybyteData IsNot Nothing) Then
        '    Erase m_refarraybyteData
        '    m_refarraybyteData = Nothing
        'End If

    End Sub


    Public Sub ComposerAddData(ByVal u16DataLen As UInt16, Optional ByVal bBigEndian As Boolean = True)
        Dim byte1, byte2 As Byte
        '
        byte1 = CType(((u16DataLen >> 8) And &HFF), Byte)
        byte2 = CType((u16DataLen And &HFF), Byte)

        If (Not bBigEndian) Then
            ByteExchange(byte1, byte2)
        End If

        m_refarraybyteData(m_nPktHeader + m_nDataSizeSend) = byte1
        m_refarraybyteData(m_nPktHeader + m_nDataSizeSend + 1) = byte2
        '
        m_nDataSizeSend = m_nDataSizeSend + 2
    End Sub
    '
    Public Sub ComposerAddData(ByVal u32DataLen As UInt32, Optional ByVal bBigEndian As Boolean = True)
        Dim byte1, byte2, byte3, byte4 As Byte
        '
        byte1 = CType(((u32DataLen >> 24) And &HFF), Byte)
        byte2 = CType(((u32DataLen >> 16) And &HFF), Byte)
        byte3 = CType(((u32DataLen >> 8) And &HFF), Byte)
        byte4 = CType((u32DataLen And &HFF), Byte)

        If (Not bBigEndian) Then
            ByteExchange(byte1, byte4)
            ByteExchange(byte2, byte3)
        End If

        m_refarraybyteData(m_nPktHeader + m_nDataSizeSend) = byte1
        m_refarraybyteData(m_nPktHeader + m_nDataSizeSend + 1) = byte2
        m_refarraybyteData(m_nPktHeader + m_nDataSizeSend + 2) = byte3
        m_refarraybyteData(m_nPktHeader + m_nDataSizeSend + 3) = byte4
        '
        m_nDataSizeSend = m_nDataSizeSend + 4
    End Sub

    Public Sub ComposerAddData(ByVal arrayByteData As Byte(), ByVal nOffsetStart As Integer, ByVal nDataLen As Integer)
        Dim nSrcRest As Integer

        If (nDataLen = 0) Then
            nDataLen = arrayByteData.Length
            nDataLen = nDataLen - nOffsetStart
        End If

        nSrcRest = m_refarraybyteData.Count - m_nDataSizeSend
        If (nDataLen > nSrcRest) Then
            Throw New Exception("ComposerAddData() : Buffer over flow ==> Src Data = " + nDataLen.ToString() + "> Dest Rest Size = " + nSrcRest.ToString())
        End If

        Array.Copy(arrayByteData, nOffsetStart, m_refarraybyteData, m_nPktHeader + m_nDataSizeSend, nDataLen)
        '
        m_nDataSizeSend = m_nDataSizeSend + nDataLen
    End Sub

    Public Sub ComposerAddData(ByVal arrayByteData As Byte(), ByVal nDataLen As Integer)
        ComposerAddData(arrayByteData, 0, nDataLen)
    End Sub
    Public Sub ComposerAddData(ByVal arrayByteData As Byte())
        ComposerAddData(arrayByteData, arrayByteData.Length)
    End Sub

    Public Sub ComposerAddData(ByVal byteData As Byte)
        m_refarraybyteData(m_nPktHeader + m_nDataSizeSend) = byteData
        '
        m_nDataSizeSend = m_nDataSizeSend + 1
    End Sub

    Public Sub ComposerAddData(ByVal bData As Boolean)
        Dim byteVal As Byte
        If (bData) Then
            byteVal = &H1
        Else
            byteVal = &H0
        End If
        '
        m_refarraybyteData(m_nPktHeader + m_nDataSizeSend) = byteVal
        '
        m_nDataSizeSend = m_nDataSizeSend + 1
    End Sub

    Public Sub ComposerAddDataUnicode(ByVal strData As String)
        Dim arrayByteData As Byte() = System.Text.Encoding.Unicode.GetBytes(strData)
        '
        ComposerAddData(arrayByteData)
        '
        Erase arrayByteData
    End Sub
    '
    Public Sub ComposerAddData(ByVal strData As String, Optional ByVal bNullEndReq As Boolean = True)
        Dim arrayByteData As Byte() = System.Text.Encoding.ASCII.GetBytes(strData)
        Dim byteVal As Byte = &H0
        '
        ComposerAddData(arrayByteData)
        If (bNullEndReq) Then
            ComposerAddData(byteVal)
        End If
        '
        Erase arrayByteData
    End Sub

    '
    Public Shared Sub ByteExchange(ByRef b1 As Byte, ByRef b2 As Byte)
        Dim bTmp As Byte

        bTmp = b1
        b1 = b2
        b2 = bTmp

    End Sub
    '
    Public Sub ParserDataGet(ByRef o_u32IntData As UInt32, Optional ByVal bBigEndian As Boolean = True)
        ParserDataGet(o_u32IntData, 0, bBigEndian)
    End Sub

    '
    Public Sub ParserDataGet(ByRef o_u32IntData As UInt32, ByRef io_nPosOff As Integer, Optional ByVal bBigEndian As Boolean = True)
        Dim byte1, byte2, byte3, byte4 As Byte
        'Dim nOffset As Integer = m_nDataRecvOffSkipBytes + m_nPktHeader
        Dim nOffset As Integer = m_nDataRecvOffSkipBytes - m_nDataSizeRecvRaw - m_nPktTrail
        'Default is Big Endian
        byte1 = m_refarraybyteData(nOffset + io_nPosOff)
        io_nPosOff = io_nPosOff + 1
        byte2 = m_refarraybyteData(nOffset + io_nPosOff)
        io_nPosOff = io_nPosOff + 1
        byte3 = m_refarraybyteData(nOffset + io_nPosOff)
        io_nPosOff = io_nPosOff + 1
        byte4 = m_refarraybyteData(nOffset + io_nPosOff)
        io_nPosOff = io_nPosOff + 1

        'If Not B.E.
        If Not bBigEndian Then
            ByteExchange(byte1, byte4)
            ByteExchange(byte2, byte3)
        End If

        o_u32IntData = (((CType(byte1, UInt32) And &HFF) << 24) And &HFF000000) + (((CType(byte2, UInt32) And &HFF) << 16) And &HFF0000) + (((CType(byte3, UInt32) And &HFF) << 8) And &HFF00) + (CType(byte4, UInt32) And &HFF)
    End Sub
    '
    Public Sub ParserDataGet(ByRef u16IntData As UInt16, Optional ByVal bBigEndian As Boolean = True)
        ParserDataGet(u16IntData, 0, bBigEndian)
    End Sub
    '
    Public Sub ParserDataGet(ByRef u16IntData As UInt16, ByRef nPosOff As Integer, Optional ByVal bBigEndian As Boolean = True)
        Dim byte1, byte2 As Byte
        'Dim nOffset As Integer = m_nDataRecvOffSkipBytes + m_nPktHeader
        Dim nOffset As Integer = m_nDataRecvOffSkipBytes - m_nDataSizeRecvRaw - m_nPktTrail

        'Default is Big Endian
        byte1 = m_refarraybyteData(nOffset + nPosOff)
        nPosOff = nPosOff + 1
        byte2 = m_refarraybyteData(nOffset + nPosOff)
        nPosOff = nPosOff + 1


        'If Not B.E.
        If Not bBigEndian Then
            ByteExchange(byte1, byte2)
        End If

        u16IntData = (((CType(byte1, UInt16) And &HFF) << 8) And &HFF00) + (CType(byte2, UInt16) And &HFF)
    End Sub

    '<Note> This sub routine destroys o_arrayByteData memory space then create new byte buffer space which has RAW Data only 
    Sub ParserDataGetArrayRawData(ByRef o_arrayByteData As Byte())
        Dim nRawDataSize As Integer
        'nRawDataSize = m_nDataSizeRecv - tagIDGCmdData.m_nPktHeader - tagIDGCmdData.m_nPktTrail
        nRawDataSize = m_nDataSizeRecvRaw

        If (m_byteV1FrameType <> ClassReaderProtocolBasic.EnumProtocolV1FrameType._X) Then
            'Protocol V1
            ParserDataGetArrayV1(0, nRawDataSize, o_arrayByteData)
            Return
        End If

        'Select Case (m_s_nVivoProctlType)
        '    Case ClassReaderInfo.ENU_PROTCL_VER._V3
        '    Case ClassReaderInfo.ENU_PROTCL_VER._V2
        'End Select
        'Protocol V2, also V3
        ParserDataGetArray(0, nRawDataSize, o_arrayByteData)

    End Sub

    ReadOnly Property bIsRawData As Boolean
        Get
            'Dim nRawDataSize As Integer
            'nRawDataSize = m_nDataSizeRecv - tagIDGCmdData.m_nPktHeader - tagIDGCmdData.m_nPktTrail
            'Return (nRawDataSize > 0)
            Return (m_nDataSizeRecvRaw > 0)
        End Get
    End Property
    '<Note> This sub routine destroys o_arrayByteData memory space then create new byte buffer space
    Sub ParserDataGetArray(ByRef io_nPosOff As Integer, ByVal nDataReadSize As Integer, ByRef o_arrayByteData As Byte())

        '--------------------------------[ Parameter Validation ]--------------------------------
        If (io_nPosOff < 0) Then
            Throw New Exception("[Property][2nd Param Err] ParserDataGetArray(), nPosOff  = " & io_nPosOff & " < 0")
        End If
        If (nDataReadSize < 1) Then
            'Throw New Exception("[Property][3rd Param Err] ParserDataGetArray(), nDataReadSize  = " & nDataReadSize & " < 1")
            'In many IDG commands there is no raw data, so just return directly
            If (o_arrayByteData IsNot Nothing) Then
                Erase o_arrayByteData
            End If
            o_arrayByteData = Nothing
            Return
        End If

        '
        If (o_arrayByteData IsNot Nothing) Then
            Erase o_arrayByteData
        End If
        o_arrayByteData = New Byte(nDataReadSize - 1) {}

        '
        '--------------------------------[ Get Data Array ]--------------------------------
        Array.Copy(m_refarraybyteData, m_nDataRecvOffSkipBytes - m_nDataSizeRecvRaw - m_nPktTrail, o_arrayByteData, 0, nDataReadSize)
        'Array.Copy(m_refarraybyteData, m_nDataRecvOffSkipBytes + m_nPktHeader + nPosOff, o_arrayByteData, 0, nDataReadSize) ' old
        io_nPosOff = io_nPosOff + nDataReadSize

    End Sub
    '
    Sub ParserDataGetArrayV1(ByRef nPosOff As Integer, ByRef nDataReadSize As Integer, ByRef o_arrayByteData As Byte())

        '--------------------------------[ Parameter Validation ]--------------------------------
        If (nPosOff < 0) Then
            Throw New Exception("[Property][2nd Param Err] ParserDataGetArray(), nPosOff  = " & nPosOff & " < 0")
        End If
        If (nDataReadSize < 1) Then
            'Throw New Exception("[Property][3rd Param Err] ParserDataGetArray(), nDataReadSize  = " & nDataReadSize & " < 1")
            'In many IDG commands there is no raw data, so just return directly
            If (o_arrayByteData IsNot Nothing) Then
                Erase o_arrayByteData
            End If
            o_arrayByteData = Nothing
            Return
        End If

        '
        If (o_arrayByteData IsNot Nothing) Then
            Erase o_arrayByteData
        End If
        o_arrayByteData = New Byte(nDataReadSize - 1) {}

        '
        '--------------------------------[ Get Data Array ]--------------------------------
        ' removed by 2017 July 27
        'Array.Copy(m_refarraybyteData, m_nDataRecvOffSkipBytes + m_nPktHeaderV1 + m_nDataRecvCmdRespLenInBytes, o_arrayByteData, 0, nDataReadSize)
        Array.Copy(m_refarraybyteData, m_nDataRecvOffSkipBytes - m_nDataSizeRecvRaw - m_nPktTrail, o_arrayByteData, 0, nDataReadSize)
        nPosOff = nPosOff + nDataReadSize

    End Sub

    Sub ParserDataGetByte(ByRef o_ByteData As Byte, ByRef nPosOff As Integer)

        '--------------------------------[ Parameter Validation ]--------------------------------
        If (nPosOff < 0) Then
            Throw New Exception("[Property][2nd Param Err] ParserDataGetByte(), nPosOff  = " & nPosOff & " < 0")
        End If

        '--------------------------------[ Get Data Byte ]--------------------------------
        'o_ByteData = m_refarraybyteData(m_nDataRecvOffSkipBytes + m_nPktHeader + nPosOff) ' removed by 2017 July 27
        o_ByteData = m_refarraybyteData(m_nDataRecvOffSkipBytes - m_nDataSizeRecvRaw - m_nPktTrail + nPosOff)
        nPosOff = nPosOff + 1

    End Sub

    Sub ComposerAddTLV(strTag As String, nLen As Integer, strVal As String)
        Dim arrayByteTmp As Byte() = Nothing
        'Tag
        ClassTableListMaster.TLVauleFromString2ByteArray(strTag, arrayByteTmp)
        ComposerAddData(arrayByteTmp)
        '
        ComposerAddData(CType(nLen, Byte))
        '
        Erase arrayByteTmp

        If (nLen > 0) Then
            ClassTableListMaster.TLVauleFromString2ByteArray(strVal, arrayByteTmp)
            ComposerAddData(arrayByteTmp)
            '
            Erase arrayByteTmp
        Else
            Dim i As Integer
            i = 1
        End If
    End Sub

    Sub ComposerAddTLV(strTag As String, nLen As Integer, arrayByteVal As Byte())
        Dim arrayByteTmp As Byte() = Nothing
        'Tag
        ClassTableListMaster.TLVauleFromString2ByteArray(strTag, arrayByteTmp)
        ComposerAddData(arrayByteTmp)
        '
        ComposerAddData(CType(nLen, Byte))
        '
        Erase arrayByteTmp

        If (nLen > 0) Then
            ComposerAddData(arrayByteVal)
        Else
            Dim i As Integer
            i = 1
        End If
    End Sub

    Sub ComposerAddTLVs(ByRef io_sdictTLVs As SortedDictionary(Of String, tagTLV))
        Dim tlv As tagTLV
        For Each tlvSeek In io_sdictTLVs
            tlv = tlvSeek.Value
            ComposerAddTLV(tlv)
        Next
    End Sub

    Public Sub ComposerAddTLV(tlv As tagTLV)
        ComposerAddTLV(tlv.m_strTag, tlv.m_nLen, tlv.m_arrayTLVal)
    End Sub

    Sub CommInterruptFSMAct()
        m_nCommInterruptFSM = EnumInterruptFSM.COMM_INTERRUPT_FSM_ACT
    End Sub
    Sub CommInterruptFSMIdle()
        m_nCommInterruptFSM = EnumInterruptFSM.COMM_INTERRUPT_FSM_IDLE
    End Sub

    Public Sub SetDebugMode_RecvDone(Optional ByVal refArrayByteData As Byte() = Nothing)
        Dim nVivostsOff As ClassReaderProtocolBasic.EnumIDGCmdOffset = ClassReaderProtocolBasic.EnumIDGCmdOffset.Protocol2_OffSet_Rx_RetStatusCode_LSB_Len_1B
        '
        If (m_s_nVivoProctlType = ClassReaderInfo.ENU_PROTCL_VER._V3) Then
            nVivostsOff = ClassReaderProtocolBasic.EnumIDGCmdOffset.Protocol3_OffSet_Rx_RetStatusCode_LSB_Len_1B
        End If
        '
        If (refArrayByteData IsNot Nothing) Then
            Array.Copy(refArrayByteData, 0, m_refarraybyteData, 0, refArrayByteData.Count)
            m_nDataRecvOff = refArrayByteData.Count
        Else
            m_nDataRecvOff = m_refarraybyteData.Count
        End If

        '
        m_nResponse = m_refarraybyteData(nVivostsOff)
        m_nDataSizeRecv = m_nDataRecvOff
        '
    End Sub


    Sub ParserDataGet(ByRef o_strSerialNum As String)

        o_strSerialNum = ""
        'TODO
        'ClassTableListMaster.ConvertFromHexToAscii(m_refarraybyteData, m_nDataRecvOffSkipBytes +  m_nPktHeader, m_nDataSizeRecvRaw, o_strSerialNum, True) ' removed by 2017 July 27
        ClassTableListMaster.ConvertFromHexToAscii(m_refarraybyteData, m_nDataRecvOffSkipBytes - m_nDataSizeRecvRaw - m_nPktTrail, m_nDataSizeRecvRaw, o_strSerialNum, True)
    End Sub

    Public Function Truncate1stPkt() As Boolean
        Dim bRet As Boolean = False
        'Dim nLen1stPktPos As Integer = m_nDataRecvOffSkipBytes + m_nDataSizeRecv ' removed by 2017 July 27
        Dim nLen1stPktPos As Integer = m_nDataRecvOffSkipBytes
        Dim pMF As Form01_Main = Form01_Main.GetInstance()
        Dim strRespErr As String = ""

        While (True)
            If (nLen1stPktPos > m_nDataRecvOff) Then
                'Invalid size
                Exit While
            End If

            '1.Remove 1st Packet (Also include Audio Jack Preamble ) And Move remain data to head of buffer
            If (nLen1stPktPos > 0) Then
                pMF.LogThreadSafe("******[Err Resp]*******")
                pMF.LogThreadSafe("[Err] Invalid Response Pkts")
                ClassTableListMaster.ConvertFromArrayByte2String(m_refarraybyteData, 0, nLen1stPktPos, strRespErr, True)
                pMF.LogThreadSafe("Resp = " + strRespErr)
                pMF.LogThreadSafe("******[End of Err Resp]*******")

                Array.Copy(m_refarraybyteData, nLen1stPktPos, m_refarraybyteData, 0, m_nDataRecvOff - nLen1stPktPos)

                m_nDataRecvOff = m_nDataRecvOff - nLen1stPktPos
            Else
                m_nDataRecvOff = 0
            End If
            '2. Change length variable

            m_nDataSizeRecv = 0
            m_nDataRecvOffSkipBytes = 0
            m_nResponse = EnumDEFAULTVAL.RESPONSE
            m_bIsCRCChkDone = False
            '
            bRet = True
            '
            Exit While
        End While
        '
        Return bRet
    End Function

#Const _DEBUG = 1
    Public Sub RxBufMoveUnknownDataOut(Optional ByVal nLenExtraByteMove As Integer = 0)
        Dim bShowUnknownBeforeBufRenew As Boolean = True
        Dim nRemainsBuf As Integer
        Dim strUnknown As String = ""
        '1.Unknown Data size check
        If (nLenExtraByteMove < 1) Then
            Return
        End If


        '2.Save Unknown Data to  Unknown Collector Buffer.
        If (m_refarraybyteDataExtraUnknown IsNot Nothing) Then
            'Check Rest Buff of Extra Unknown Buffer
            '2.1.If Not enough, then reset offset of Extra Unknown Buffer.
            If ((m_refarraybyteDataExtraUnknown.Count() - m_nDataSizeRecvExtraUnknownOff) < nLenExtraByteMove) Then
                If (bShowUnknownBeforeBufRenew) Then
#If _DEBUG Then
                    Dim o_strByteStrListFormated() As String = New String() {}
                    ClassTableListMaster.ConvertFromArrayByte2StringListFormated(m_refarraybyteDataExtraUnknown, 0, m_nDataSizeRecvExtraUnknownOff, o_strByteStrListFormated, 16)
                    '
                    For Each strLn As String In o_strByteStrListFormated
                        Form01_Main.GetInstance().DumpingRecvHexStr(strLn)
                    Next
                    '
                    Erase o_strByteStrListFormated
#End If
                End If
                ' Reset Offset
                m_nDataSizeRecvExtraUnknownOff = 0

            End If

            '2.2 Move Unknown data from Rx Buffer to Rx Unknown Buffer & Add Anchor Offset
            Array.Copy(m_refarraybyteData, m_nDataRecvOffSkipBytes, m_refarraybyteDataExtraUnknown, m_nDataSizeRecvExtraUnknownOff, nLenExtraByteMove)
            ' Update Unknown buffer offset
            m_nDataSizeRecvExtraUnknownOff = m_nDataSizeRecvExtraUnknownOff + nLenExtraByteMove

        End If

        'We dumping log here
        ClassTableListMaster.ConvertFromHexToAscii(m_refarraybyteData, m_nDataRecvOffSkipBytes, nLenExtraByteMove, strUnknown)
        Form01_Main.GetInstance().DumpingRecvAsciiStr(strUnknown)

        'Calculate rest data size after garbage. If > 0 then we pack it.
        nRemainsBuf = m_nDataRecvOff - nLenExtraByteMove - m_nDataRecvOffSkipBytes
        If (nRemainsBuf > 0) Then
            '3.Move Found Data Ahead to start [SkippedOffset] in Rx Buffer.
            Array.Copy(m_refarraybyteData, m_nDataRecvOff - nRemainsBuf, m_refarraybyteData, m_nDataRecvOffSkipBytes, nRemainsBuf)
            'Update Last Rx Offset Indicator
            m_nDataRecvOff = m_nDataRecvOffSkipBytes + nRemainsBuf
        Else
            'Update Last Rx Offset Indicator
            m_nDataRecvOff = m_nDataRecvOffSkipBytes
        End If

    End Sub

    '[Issue] [Unresolved] [Special Note]
    '2017 July 27, goofy_liu
    'Note: This subroutine is for non-sync fileds (m_nDataRecvOffSkipBytes, m_nDataRecvOffSkipBytes2_PrefixGarbage )
    '01.After Rx Done, Source code statement near Ln 3957, as the following
    'ClassReaderProtocolBasic.IsOkWhileCRCCheckFrameResponse(refIDGCmdData)
    'Advanced RecvOffSkipBytes to the pos after the found packet
    '-->.m_nDataRecvOffSkipBytes = nCRCPos + 2
    '-->.m_nDataRecvOffSkipBytes2_PrefixGarbage = .m_nDataRecvOffSkipBytes
    '02.after 01, m_nDataRecvOffSkipBytes becomes smaller than m_nDataRecvOffSkipBytes2_PrefixGarbage
    'Ex: 
    'correct --------------------
    ' m_nDataRecvOffSkipBytes2 = &H2F
    'm_nDataRecvOffSkipBytes2_PrefixGarbage = &H2F
    ' Invalid result after return to upper / caller -
    ' m_nDataRecvOffSkipBytes2 = &H24
    'm_nDataRecvOffSkipBytes2_PrefixGarbage = &H2F
    '
    '03. After few days investigation still no conclusion (2017 July 25~27)
    'but possible some multi-thread synchronization protection problem.
    'still not found

    Sub ParserDataUpdateBeforeRetrieve()
        If (m_nDataRecvOffSkipBytes < m_nDataRecvOffSkipBytes2_PrefixGarbage) Then
            m_nDataRecvOffSkipBytes = m_nDataRecvOffSkipBytes2_PrefixGarbage
        End If
    End Sub

    Private m_nDataSizeRecv2 As Integer
    Private m_nDataRecvOffSkipBytes2 As Integer
    '2017 Sept 25, used for after V2 resp format retrying --> V3 Retrying..
    Private Sub bIsVHdrReadyRawSizeReady_TempSave()
        m_nDataSizeRecv2 = m_nDataSizeRecv
        m_nDataRecvOffSkipBytes2 = m_nDataRecvOffSkipBytes
    End Sub

    Private Sub bIsVHdrReadyRawSizeReady_TempRestore()
        m_nDataSizeRecv = m_nDataSizeRecv2
        m_nDataRecvOffSkipBytes = m_nDataRecvOffSkipBytes2
    End Sub

    
End Structure