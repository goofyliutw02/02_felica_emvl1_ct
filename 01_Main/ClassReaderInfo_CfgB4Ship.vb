﻿' This class is used to save the Reader's Type
'======================[ Check List for Other Reader Migration ]======================
'1.Auto Poll Mode
'2.Burst Mode
'3.UI Scheme
'4.Button Configuration
'5.Other for later add-in
'6.This is ID TECH Reader Info Database.
' -----------[ List of Reader ]--------------
'    AR - Vend III(2013), PISCES(2016)
'    GR - KIOSK II (2012)
'    NEO (inherited from GR) - Vendi(2015), Unipay III (2016), Unipay 1.5 (2016, from Old Version),  VP4880 Serials (2016,aka Goose Run),
' Special Note:
' 001 - This is Configuration functions(properties) list before Shipment
'======================[ End of Check List for Other Reader Migration ]======================
Imports System
'Imports MainRoot.ClassDataFilesCmpLog
'


Partial Public Class ClassReaderInfo

    ' ---------------[Updated List]--------------
    '2016 Dec 09, CTCFG_ITEM011_TESTITEM_EXCLUDE_001_MSR, Vendi-C, No MSR Test
    '2016 Dec 07, CTCFG_ITEM010_ENCRPT_3_PAN_PRE added, UniPay 1.5 TTK
    Enum ENU_CUST_CFG_KEYTYPE
        CTCFG_ITEM001_TDES_AES = &H0
        CTCFG_ITEM002_ENCRPT_ENABLE_EMV
        CTCFG_ITEM002_ENCRPT_ENABLE_2_MSR
        CTCFG_ITEM003_GLOB_TLVS_POLL_MOD_VALUE
        CTCFG_ITEM004_GLOB_TLVS_BURST_MODE_ENABLE
        CTCFG_ITEM005_GLOB_TLVS_BURST_MODE
        CTCFG_ITEM006_GLOB_TLVS_UI_SCHEME
        CTCFG_ITEM007_COM_BAUDRATE
        ' Beware the Conflicts to RS232 & USBHID Functions Setting 
        CTCFG_ITEM008_UIFORM02_04COMM_USBHID_VISIBLE
        CTCFG_ITEM009_UIFORM02_04COMM_RS232_VISIBLE
        CTCFG_ITEM010_ENCRPT_3_PAN_PRE
        CTCFG_ITEM011_TESTITEM_EXCLUDE_001_MSR
        CTCFG_ITEM012_GLOB_TLVS_CUSTOM
        CTCFG_ITEM013_BLE_RDR_ENZYTEK_FRIENDLY_NAME
        CTCFG_ITEM014_TESTITEM_EXCLUDE_002_VENDI_KEYRIGHT
        ' [Note]
        ' Conflicts to RS232 & USBHID UI Visible Setting 
        CTCFG_ITEM015_TESTITEM_EXCLUDE_001_USBHID
        CTCFG_ITEM016_TESTITEM_EXCLUDE_002_RS232
        CTCFG_ITEM017_TESTITEM_EXCLUDE_003_ETHERNET
        '
        CTCFG_ITEMMAX


    End Enum
    '
    ' Property GetProductName As String
    '    Get

    '    End Get
    ' Set is used while Customized functions
    '   Set(value As Byte)
    '        m_TagXMLPartNumCustCfgParams.SetItemEnable(ENU_CUST_CFG_KEYTYPE.CTCFG_ITEM001_TDES_AES, value)
    '   End Set
    'End Property

    '============[ Features of Shipping Configurations ]===========
    'property to check allowed setting
    Property byteIsShippingCfg001_EncAlgo As Byte
        Get
            Return m_TagXMLPartNumCustCfgParams.GetItemEnable(ENU_CUST_CFG_KEYTYPE.CTCFG_ITEM001_TDES_AES)
        End Get
        Set(value As Byte)
            m_TagXMLPartNumCustCfgParams.SetItemEnable(ENU_CUST_CFG_KEYTYPE.CTCFG_ITEM001_TDES_AES, value)
        End Set
    End Property
    Property byteIsShippingCfg002_EncEnable_1_EMV As Byte
        Get
            Return m_TagXMLPartNumCustCfgParams.GetItemEnable(ENU_CUST_CFG_KEYTYPE.CTCFG_ITEM002_ENCRPT_ENABLE_EMV)
        End Get
        Set(value As Byte)
            m_TagXMLPartNumCustCfgParams.SetItemEnable(ENU_CUST_CFG_KEYTYPE.CTCFG_ITEM002_ENCRPT_ENABLE_EMV, value)
        End Set
    End Property
    '----
    Property byteIsShippingCfg002_EncEnable_2_MSR As Byte
        Get
            Return m_TagXMLPartNumCustCfgParams.GetItemEnable(ENU_CUST_CFG_KEYTYPE.CTCFG_ITEM002_ENCRPT_ENABLE_2_MSR)
        End Get
        Set(value As Byte)
            m_TagXMLPartNumCustCfgParams.SetItemEnable(ENU_CUST_CFG_KEYTYPE.CTCFG_ITEM002_ENCRPT_ENABLE_2_MSR, value)
        End Set
    End Property

    '2016 Dec 06, goofy_liu@idtechproducts.com.tw
    'Pre-PAN Setting, UniPay 1.5 TTK ONLY.
    Property byteIsShippingCfg010_EncEnable_3_MSR_PANPre As Byte
        Get
            Return m_TagXMLPartNumCustCfgParams.GetItemEnable(ENU_CUST_CFG_KEYTYPE.CTCFG_ITEM010_ENCRPT_3_PAN_PRE)
        End Get
        Set(value As Byte)
            m_TagXMLPartNumCustCfgParams.SetItemEnable(ENU_CUST_CFG_KEYTYPE.CTCFG_ITEM010_ENCRPT_3_PAN_PRE, value)
        End Set
    End Property


    '
    Property byteIsShippingCfg003_GlobTLVs_PollMode As Byte
        Get
            Return m_TagXMLPartNumCustCfgParams.GetItemEnable(ENU_CUST_CFG_KEYTYPE.CTCFG_ITEM003_GLOB_TLVS_POLL_MOD_VALUE)
        End Get
        Set(value As Byte)
            m_TagXMLPartNumCustCfgParams.SetItemEnable(ENU_CUST_CFG_KEYTYPE.CTCFG_ITEM003_GLOB_TLVS_POLL_MOD_VALUE, value)
        End Set
    End Property
    Property byteIsShippingCfg005_GlobTLVs_BurstMode As Byte
        Get
            Return m_TagXMLPartNumCustCfgParams.GetItemEnable(ENU_CUST_CFG_KEYTYPE.CTCFG_ITEM005_GLOB_TLVS_BURST_MODE)
        End Get
        Set(value As Byte)
            m_TagXMLPartNumCustCfgParams.SetItemEnable(ENU_CUST_CFG_KEYTYPE.CTCFG_ITEM005_GLOB_TLVS_BURST_MODE, value)
        End Set
    End Property
    Property byteIsShippingCfg006_GlobTLVs_UIScheme As Byte
        Get
            Return m_TagXMLPartNumCustCfgParams.GetItemEnable(ENU_CUST_CFG_KEYTYPE.CTCFG_ITEM006_GLOB_TLVS_UI_SCHEME)
        End Get
        Set(value As Byte)
            m_TagXMLPartNumCustCfgParams.SetItemEnable(ENU_CUST_CFG_KEYTYPE.CTCFG_ITEM006_GLOB_TLVS_UI_SCHEME, value)
        End Set
    End Property
    Property byteIsShippingCfg012_GlobTLVs_Customize As Byte
        Get
            Return m_TagXMLPartNumCustCfgParams.GetItemEnable(ENU_CUST_CFG_KEYTYPE.CTCFG_ITEM012_GLOB_TLVS_CUSTOM)
        End Get
        Set(value As Byte)
            m_TagXMLPartNumCustCfgParams.SetItemEnable(ENU_CUST_CFG_KEYTYPE.CTCFG_ITEM012_GLOB_TLVS_CUSTOM, value)
        End Set
    End Property

    Property byteIsShippingCfg007_COMBaudrate As Byte
        Get
            Return m_TagXMLPartNumCustCfgParams.GetItemEnable(ENU_CUST_CFG_KEYTYPE.CTCFG_ITEM007_COM_BAUDRATE)
        End Get
        Set(value As Byte)
            m_TagXMLPartNumCustCfgParams.SetItemEnable(ENU_CUST_CFG_KEYTYPE.CTCFG_ITEM007_COM_BAUDRATE, value)
        End Set
    End Property

    Property byteIsShippingCfg008_UIForm02_usbhid_visible As Byte
        Get
            Return m_TagXMLPartNumCustCfgParams.GetItemEnable(ENU_CUST_CFG_KEYTYPE.CTCFG_ITEM008_UIFORM02_04COMM_USBHID_VISIBLE)
        End Get
        Set(value As Byte)
            m_TagXMLPartNumCustCfgParams.SetItemEnable(ENU_CUST_CFG_KEYTYPE.CTCFG_ITEM008_UIFORM02_04COMM_USBHID_VISIBLE, value)
        End Set
    End Property

    '2016 Dec 09, goofy_liu@idtechproducts.com.tw
    'Pre-PAN Setting, Vendi-C ONLY.
    Property byteIsShippingCfg011_Form04_TITEM_Exclude_001_MSR As Byte
        Get
            Return m_TagXMLPartNumCustCfgParams.GetItemEnable(ENU_CUST_CFG_KEYTYPE.CTCFG_ITEM011_TESTITEM_EXCLUDE_001_MSR)
        End Get
        Set(value As Byte)
            m_TagXMLPartNumCustCfgParams.SetItemEnable(ENU_CUST_CFG_KEYTYPE.CTCFG_ITEM011_TESTITEM_EXCLUDE_001_MSR, value)
        End Set
    End Property

    Property byteIsShippingCfg013_ExtraSettings_BLE_Friendly_Name As Byte
        Get
            Return m_TagXMLPartNumCustCfgParams.GetItemEnable(ENU_CUST_CFG_KEYTYPE.CTCFG_ITEM013_BLE_RDR_ENZYTEK_FRIENDLY_NAME)
        End Get
        Set(value As Byte)
            m_TagXMLPartNumCustCfgParams.SetItemEnable(ENU_CUST_CFG_KEYTYPE.CTCFG_ITEM013_BLE_RDR_ENZYTEK_FRIENDLY_NAME, value)
        End Set
    End Property
    '
    '2017 May 17, See ECN-007212, goofy_liu@idtechproducts.com.tw
    'Some Vendi, Vendi-C P/N has been modified for this
    '================================================
    'IDVV-110101 (only the left key) Vendi-C
    'IDVV-110101N (only the left key) Vendi-C
    'IDVV-110101P (only the left key) Vendi-C
    'IDVV-120101
    'IDVV-120101-US (only the left key) (please also confirm if this P/N should be OBS)
    'IDVV-120101A-US (only the left key)
    'IDVV-120101N
    'IDVV-120101P
    Property byteIsShippingCfg014_Form04_TITEM_Exclude_002_Vendi_KeyPad_RightKey As Byte
        Get
            Return m_TagXMLPartNumCustCfgParams.GetItemEnable(ENU_CUST_CFG_KEYTYPE.CTCFG_ITEM014_TESTITEM_EXCLUDE_002_VENDI_KEYRIGHT)
        End Get
        Set(value As Byte)
            m_TagXMLPartNumCustCfgParams.SetItemEnable(ENU_CUST_CFG_KEYTYPE.CTCFG_ITEM014_TESTITEM_EXCLUDE_002_VENDI_KEYRIGHT, value)
        End Set
    End Property


    Property byteIsShippingCfg015_Form04_TITEM_Exclude_003_Intf_USBHID As Byte
        Get
            Return m_TagXMLPartNumCustCfgParams.GetItemEnable(ENU_CUST_CFG_KEYTYPE.CTCFG_ITEM015_TESTITEM_EXCLUDE_001_USBHID)
        End Get
        Set(value As Byte)
            m_TagXMLPartNumCustCfgParams.SetItemEnable(ENU_CUST_CFG_KEYTYPE.CTCFG_ITEM015_TESTITEM_EXCLUDE_001_USBHID, value)
        End Set
    End Property
    Property byteIsShippingCfg016_Form04_TITEM_Exclude_004_Intf_RS232 As Byte
        Get
            Return m_TagXMLPartNumCustCfgParams.GetItemEnable(ENU_CUST_CFG_KEYTYPE.CTCFG_ITEM016_TESTITEM_EXCLUDE_002_RS232)
        End Get
        Set(value As Byte)
            m_TagXMLPartNumCustCfgParams.SetItemEnable(ENU_CUST_CFG_KEYTYPE.CTCFG_ITEM016_TESTITEM_EXCLUDE_002_RS232, value)
        End Set
    End Property
    Property byteIsShippingCfg017_Form04_TITEM_Exclude_005_Intf_Ethernet As Byte
        Get
            Return m_TagXMLPartNumCustCfgParams.GetItemEnable(ENU_CUST_CFG_KEYTYPE.CTCFG_ITEM017_TESTITEM_EXCLUDE_003_ETHERNET)
        End Get
        Set(value As Byte)
            m_TagXMLPartNumCustCfgParams.SetItemEnable(ENU_CUST_CFG_KEYTYPE.CTCFG_ITEM017_TESTITEM_EXCLUDE_003_ETHERNET, value)
        End Set
    End Property
    '========================
    Property byteValShippingCfg001_EncAlgo As Byte
        Get
            Return m_TagXMLPartNumCustCfgParams.m_Item001_byteEncryption_Algorithm
        End Get
        Set(value As Byte)
            m_TagXMLPartNumCustCfgParams.m_Item001_byteEncryption_Algorithm = value

        End Set
    End Property
    '
    Property byteValShippingCfg002_EncEnable_1_EMV As Byte
        Get
            Return m_TagXMLPartNumCustCfgParams.m_Item002_byteEncryption_Enable_1_EMV
        End Get
        Set(value As Byte)
            m_TagXMLPartNumCustCfgParams.m_Item002_byteEncryption_Enable_1_EMV = value

        End Set
    End Property
    '
    Property byteValShippingCfg002_EncEnable_2_MSR As Byte
        Get
            Return m_TagXMLPartNumCustCfgParams.m_Item002_byteEncryption_Enable_2_MSR
        End Get
        Set(value As Byte)
            m_TagXMLPartNumCustCfgParams.m_Item002_byteEncryption_Enable_2_MSR = value
        End Set
    End Property
    '
    Property byteValShippingCfg003_PollMode As Byte
        Get
            Return m_TagXMLPartNumCustCfgParams.m_Item003_byteGlobalTLVsVars_PollMode
        End Get
        Set(value As Byte)
            m_TagXMLPartNumCustCfgParams.m_Item003_byteGlobalTLVsVars_PollMode = value
        End Set
    End Property
    Property byteValShippingCfg005_BurstMode As Byte
        Get
            Return m_TagXMLPartNumCustCfgParams.m_Item005_byteGlobalTLVsVars_BurstMode
        End Get
        Set(value As Byte)
            m_TagXMLPartNumCustCfgParams.m_Item005_byteGlobalTLVsVars_BurstMode = value
        End Set
    End Property
    Property byteValShippingCfg006_UIScheme As Byte
        Get
            Return m_TagXMLPartNumCustCfgParams.m_Item006_byteGlobalTLVsVars_UIScheme
        End Get
        Set(value As Byte)
            m_TagXMLPartNumCustCfgParams.m_Item006_byteGlobalTLVsVars_UIScheme = value
        End Set
    End Property

    Property byteValShippingCfg007_CommIntf_COMBaudrate As Byte
        Get
            Return m_TagXMLPartNumCustCfgParams.m_Item007_byteGlobalTLVsVars_COMBaudrate
        End Get
        Set(value As Byte)
            m_TagXMLPartNumCustCfgParams.m_Item007_byteGlobalTLVsVars_COMBaudrate = value
        End Set
    End Property

    Property byteVal008_UIItemVisible_Form02_04comm_usbhid_visible As Byte
        Get
            Return m_TagXMLPartNumCustCfgParams.m_Item008_byteUI_Form02_04Comm_usbhide_visible
        End Get
        Set(value As Byte)
            m_TagXMLPartNumCustCfgParams.m_Item008_byteUI_Form02_04Comm_usbhide_visible = value
        End Set
    End Property

    Property byteVal009_UIItemVisible_Form02_04comm_rs232_visible As Byte
        Get
            Return m_TagXMLPartNumCustCfgParams.m_Item009_byteUI_Form02_04Comm_rs232_visible
        End Get
        Set(value As Byte)
            m_TagXMLPartNumCustCfgParams.m_Item009_byteUI_Form02_04Comm_rs232_visible = value
        End Set
    End Property

    Property byteValShippingCfg010_Enc_3_PrePAN As Byte
        Get
            Return m_TagXMLPartNumCustCfgParams.m_Item010_byteEncryption_3_PANPre
        End Get
        Set(value As Byte)
            m_TagXMLPartNumCustCfgParams.m_Item010_byteEncryption_3_PANPre = value
        End Set
    End Property

    Property byteValShippingCfg011_Form04_TITEM_EXCLUDE_001_MSR As Byte
        Get
            Return m_TagXMLPartNumCustCfgParams.m_Item011_byteTestItem_Exclude_001_MSR
        End Get
        Set(value As Byte)
            m_TagXMLPartNumCustCfgParams.m_Item011_byteTestItem_Exclude_001_MSR = value
        End Set
    End Property

    '
    '    <!--[Item 012]-->
    '	<!--This section provides 04-00 Set Global TLVs Service-->
    '	<!--Don't put V2/V3 Header & CRC portions, Data = TLVs -->
    '	<cust_data set="0">1_5f3601029f1a0208409f3501219f33036028c89f4005f000f0a0019f1e    '085465726d696e616c9f150212349f160f3030303030303030303030303030309f1c0838373635343332319f4e2231303732312057616c6b65722053742e20437970726573732c204341202c5553412edf260101df1008656e667265737a68df110100df270100dfee150101dfee160100dfee170107dfee180180dfee1e08d0dc20d0c41e1400dfee1f0180dfee1b083030303135313030dfee20013cdfee21010adfee2203323c3cdfef4b03120000</cust_data>
    Property byteValShippingCfg012_GlobalVars_CustTLVs As Byte
        Get
            Return m_TagXMLPartNumCustCfgParams.m_Item012_byteGlobalTLVsVars_Custom
        End Get
        Set(value As Byte)
            m_TagXMLPartNumCustCfgParams.m_Item012_byteGlobalTLVsVars_Custom = value
        End Set
    End Property
    Property strValShippingCfg012_GlobalVars_CustTLVs As String
        Get
            Return m_TagXMLPartNumCustCfgParams.m_Item012_strGlobalTLVsVars_Custom
        End Get
        Set(value As String)
            m_TagXMLPartNumCustCfgParams.m_Item012_strGlobalTLVsVars_Custom = value
        End Set
    End Property
    '
    '
    '    <extra_settings>
    '        <!--[Item 013]-->
    '        <!--[Blue tooth's Friendly Name Settings]-->
    '        <!--Len = 1~19 char(s), No Null char-->
    '        <!--Reader: BTPay Mini(VP3300)-->
    '        <!-- Prefix value alway = "1_"-->
    '        <!--default = "IDTECH-VP3300"-->
    '        <ble_frdly_name set="1">1_IDTECH-BTPay Mini</ble_frdly_name>
    '    </extra_settings>
    '</custconfig>
    Property byteValShippingCfg013_ExtraSettings_BLE_Friendly_Name As Byte
        Get
            Return m_TagXMLPartNumCustCfgParams.m_Item013_byteExtSetting_BLE_FriendlyName_Enzytek
        End Get
        Set(value As Byte)
            m_TagXMLPartNumCustCfgParams.m_Item013_byteExtSetting_BLE_FriendlyName_Enzytek = value
        End Set
    End Property
    Property strValShippingCfg013_ExtraSettings_BLE_Friendly_Name As String
        Get
            Return m_TagXMLPartNumCustCfgParams.m_Item013_strExtSetting_BLE_FriendlyName_Enzytek
        End Get
        Set(value As String)
            m_TagXMLPartNumCustCfgParams.m_Item013_strExtSetting_BLE_FriendlyName_Enzytek = value
        End Set
    End Property


    '<ui_form04_testitem_exclude>
    '             <!--[Item 014]-->
    '             <!--[Test Item 002 - KeyRight_Vendi, Exclude]-->
    '             <!--1_EXCLUDE = disable test; 0_INCLUDE = enable test-->
    '             <titem_002_Vendi_KeyRight set="0">0_INCLUDE</titem_002_Vendi_KeyRight>
    '</ui_form04_testitem_exclude>
    Property byteValShippingCfg014_Form04_TITEM_EXCLUDE_002_Vendi_KeyPad_RightKey As Byte
        Get
            Return m_TagXMLPartNumCustCfgParams.m_Item014_byteTestItem_Exclude_002_Vendi_KeyRight
        End Get
        Set(value As Byte)
            m_TagXMLPartNumCustCfgParams.m_Item014_byteTestItem_Exclude_002_Vendi_KeyRight = value
        End Set
    End Property


    Property byteValShippingCfg015_Form04_TITEM_EXCLUDE_003_Intf_USBHID As Byte
        Get
            Return m_TagXMLPartNumCustCfgParams.m_Item015_byteTestItem_Exclude_001_USBHID
        End Get
        Set(value As Byte)
            m_TagXMLPartNumCustCfgParams.m_Item015_byteTestItem_Exclude_001_USBHID = value
        End Set
    End Property

    Property byteValShippingCfg016_Form04_TITEM_EXCLUDE_004_Intf_RS232 As Byte
        Get
            Return m_TagXMLPartNumCustCfgParams.m_Item016_byteTestItem_Exclude_002_RS232
        End Get
        Set(value As Byte)
            m_TagXMLPartNumCustCfgParams.m_Item016_byteTestItem_Exclude_002_RS232 = value
        End Set
    End Property

    Property byteValShippingCfg017_Form04_TITEM_EXCLUDE_005_Intf_Ethernet As Byte
        Get
            Return m_TagXMLPartNumCustCfgParams.m_Item017_byteTestItem_Exclude_003_ETHERNET
        End Get
        Set(value As Byte)
            m_TagXMLPartNumCustCfgParams.m_Item017_byteTestItem_Exclude_003_ETHERNET = value
        End Set
    End Property
    '============[ End of Features of Customer Configurations Before Shipping ]===========
End Class
