﻿'==================================================================================
' File Name: ClassReaderProtocolBasic.vb
' Description: The ClassReaderProtocolBasic is as Must-be Inherited Base Class of Reader's Packet Parser & Composer.
' Who use it as Base Class: ClassReaderProtocolBasicIDGAR, ClassReaderProtocolBasicIDGAR, and ClassReaderProtocolBasicIDTECH
' For Network Functions
' 
' Revision:
' -----------------------[ Initial Version ]-----------------------
' 2017 Oct 06, by goofy_liu@idtechproducts.com.tw

'==================================================================================
'
'----------------------[ Importing Stuffs ]----------------------
Imports System.Runtime.Serialization
Imports System.Runtime.Serialization.Formatters.Binary
'Imports DiagTool_HardwareExclusive.ClassReaderProtocolBasicIDGAR
Imports System.IO
Imports System.IO.Ports
Imports System.Threading

'-------------------[ Class Declaration ]-------------------

Partial Public Class ClassReaderProtocolBasic
    '
    'Ref Doc: https://atlassian.idtechproducts.com/confluence/pages/viewpage.action?pageId=30486021
    'Based on NEO IDG Supplement U3 & U1.5 v1.28
    'Project: BTPay Mini, 2016~2017
    'Bluetooth Module Vendor : Enzytek,886-3-5736708
    Overridable Sub System_Bluetooth_GetAddr(ByRef o_strBLEAddr As String, Optional ByVal nTimeout As Integer = ClassDevRS232.m_cnTX_TIMEOUT_IN_SEC)
        '2017 Mar 1, goofy_liu@idtechproductsc.om.tw
        Dim u16Cmd As UInt16 = &H3010
        Dim strFuncName As String = "System_Bluetooth_GetAddr"

        If (m_bIDGCmdBusy) Then
            Return
        End If
        m_bIDGCmdBusy = True

        Try
            '----------[Note, BTPay Mini]----------
            '2017 Mar 02
            'Enzytek BLE Module & MUC communication has slow response.
            'Tx/Rx response time is around up to 200 ms.
            'So here we have to delay 100ms before Tx and delay 100ms after Rx.
            'Point (A)
            Thread.Sleep(100)

            o_strBLEAddr = "N/A"
            '----------------------[ Send to reader ]---------------------
            System_Customized_Command(u16Cmd, strFuncName, Nothing, Nothing, AddressOf ClassReaderProtocolBasic.PreProcessCbRecv, nTimeout)

            If (bIsRetStatusOK) Then
                If (m_tagIDGCmdDataInfo.bIsRawData) Then
                    Dim aryByteRx As Byte() = Nothing
                    m_tagIDGCmdDataInfo.ParserDataGetArrayRawData(aryByteRx)
                    ClassTableListMaster.ConvertFromHexToAscii(aryByteRx, o_strBLEAddr, False)
                    o_strBLEAddr = o_strBLEAddr.ToUpper()
                    Erase aryByteRx
                End If
            Else

            End If

        Catch ext As TimeoutException
            LogLn("[Err][Timeout] [IDG: " + GetIDGCmdString(u16Cmd) + "] " + strFuncName + "() = " + ext.Message)
        Catch ex As Exception
            LogLn("[Err] [IDG:  " + GetIDGCmdString(u16Cmd) + "] " + strFuncName + "() = " + ex.Message)
        Finally
            '----------[Note, BTPay Mini]----------
            'Enzytek BLE Module & MUC communication has slow response.
            'Tx/Rx response time is around up to 200 ms.
            'So here we have to delay 100ms before Tx and delay 100ms after Rx.
            'Point (B)
            Thread.Sleep(100)

            m_tagIDGCmdDataInfo.Cleanup()
            m_bIDGCmdBusy = False
        End Try

    End Sub
    '
    Public Overridable Sub System_Bluetooth_ModuleFrmwInfoGet(ByRef o_strFwVersionList As List(Of String), Optional ByVal nTimeout As Integer = ClassDevRS232.m_cnTX_TIMEOUT_IN_SEC)
        Dim u16Cmd As UInt16 = &H3013
        Dim strFuncName As String = "System_Bluetooth_ModuleFrmwInfoGet"
        Dim arrayByteSend As Byte() = Nothing
        Dim arrayByteRecv As Byte() = Nothing

        If (m_bIDGCmdBusy) Then
            Return
        End If
        m_bIDGCmdBusy = True

        '
        Try

            System_Customized_Command(u16Cmd, strFuncName, arrayByteSend, arrayByteRecv, AddressOf ClassReaderProtocolBasic.PreProcessCbRecv, nTimeout)

            'Its' response code = 0 => OK
            If (bIsRetStatusOK) Then
                Dim arrayByteData As Byte() = Nothing


                m_tagIDGCmdDataInfo.ParserDataGetArrayRawData(arrayByteData)
                If (arrayByteData IsNot Nothing) Then

                    ClassTableListMaster.ConvertFromArrayByte2StringsList(arrayByteData, o_strFwVersionList)

                    ' Free Resource
                    Erase arrayByteData
                Else
                    o_strFwVersionList.Clear()
                End If
            Else
                LogLn("[Err] " + strFuncName + "() = Response Code (" & m_tagIDGCmdDataInfo.m_nResponse & ") --> " + m_dictStatusCode2.Item(m_tagIDGCmdDataInfo.m_nResponse))
            End If

        Catch ext As TimeoutException
            LogLn("[Err] [Timeout][IDG: " + GetIDGCmdString(u16Cmd) + "] " + strFuncName + "() = " + ext.Message)
        Catch ex As Exception
            LogLn("[Err][IDG: " + GetIDGCmdString(u16Cmd) + "] " + strFuncName + "() = " + ex.Message)
        Finally
            m_bIDGCmdBusy = False
            If (arrayByteSend IsNot Nothing) Then
                Erase arrayByteSend
            End If
            If (arrayByteRecv IsNot Nothing) Then
                Erase arrayByteRecv
            End If
        End Try
        '

    End Sub
    '

    '
    Overridable Sub System_Bluetooth_GetFriendlyName(ByRef o_strBLE_FrdlyName As String, Optional ByVal nTimeout As Integer = ClassDevRS232.m_cnTX_TIMEOUT_IN_SEC)
        '2017 Mar 1, goofy_liu@idtechproductsc.om.tw
        Dim u16Cmd As UInt16 = &H3011
        Dim strFuncName As String = "System_Bluetooth_GetFriendlyName"

        If (m_bIDGCmdBusy) Then
            Return
        End If
        m_bIDGCmdBusy = True

        Try
            o_strBLE_FrdlyName = "N/A"

            '----------[Note, BTPay Mini]----------
            '2017 Mar 02
            'Enzytek BLE Module & MUC communication has slow response.
            'Tx/Rx response time is around up to 200 ms.
            'So here we have to delay 100ms before Tx and delay 100ms after Rx.
            'Point (A)
            Thread.Sleep(100)

            '--------[ Send to reader ]--------
            System_Customized_Command(u16Cmd, strFuncName, Nothing, Nothing, AddressOf ClassReaderProtocolBasic.PreProcessCbRecv, nTimeout)

            If (bIsRetStatusOK) Then
                If (m_tagIDGCmdDataInfo.bIsRawData) Then
                    Dim aryByteRx As Byte() = Nothing
                    m_tagIDGCmdDataInfo.ParserDataGetArrayRawData(aryByteRx)
                    'Ex:49 44 54 45 43 48 2D 42 54 70 61 79 20 6D 69 6E 69 20 20 --> "IDTECH-BTpay mini  "
                    ClassTableListMaster.ConvertFromHexToAscii(aryByteRx, o_strBLE_FrdlyName, True)
                End If
            Else

            End If

        Catch ext As TimeoutException
            LogLn("[Err][Timeout] [IDG: " + GetIDGCmdString(u16Cmd) + "] " + strFuncName + "() = " + ext.Message)
        Catch ex As Exception
            LogLn("[Err] [IDG:  " + GetIDGCmdString(u16Cmd) + "] " + strFuncName + "() = " + ex.Message)
        Finally
            '----------[Note, BTPay Mini]----------
            '2017 Mar 02
            'Enzytek BLE Module & MUC communication has slow response.
            'Tx/Rx response time is around up to 200 ms.
            'So here we have to delay 100ms before Tx and delay 100ms after Rx.
            'Point (B)
            Thread.Sleep(100)
            m_tagIDGCmdDataInfo.Cleanup()
            m_bIDGCmdBusy = False
        End Try

    End Sub
    '
    'Note: String Length <= 19 chars
    'Note: No Null End Required.
    'Ref Doc: Same Cmd 30-10
    'Input Ex:"IDTECH-BTpay mini  " --> 49 44 54 45 43 48 2D 42 54 70 61 79 20 6D 69 6E 69 20 20
    Overridable Sub System_Bluetooth_SetFriendlyName(i_strBLE_FrdlyName As String, Optional ByVal nTimeout As Integer = ClassDevRS232.m_cnTX_TIMEOUT_IN_SEC)
        '2017 Mar 1, goofy_liu@idtechproductsc.om.tw
        Dim u16Cmd As UInt16 = &H3012
        Dim strFuncName As String = "System_Bluetooth_SetFriendlyName"
        Dim bytAryTx As Byte() = Nothing

        If (m_bIDGCmdBusy) Then
            Return
        End If
        m_bIDGCmdBusy = True

        Try
            '----------[Note, BTPay Mini]----------
            '2017 Mar 02
            'Enzytek BLE Module & MUC communication has slow response.
            'Tx/Rx response time is around up to 200 ms.
            'So here we have to delay 100ms before Tx and delay 100ms after Rx.
            'Point (A)
            Thread.Sleep(100)

            If (i_strBLE_FrdlyName.Length > 0) Then

                ClassTableListMaster.ConvertFromAscii2HexByte(i_strBLE_FrdlyName, bytAryTx)


                'Add customized data
                'm_tagIDGCmdDataInfo.ComposerAddData(i_strBLE_FrdlyName, False)
            End If

            '----------------------[ Send to reader ]---------------------
            System_Customized_Command(u16Cmd, strFuncName, bytAryTx, Nothing, AddressOf ClassReaderProtocolBasic.PreProcessCbRecv, nTimeout)

            If (bIsRetStatusOK) Then

            Else

            End If

        Catch ext As TimeoutException
            LogLn("[Err][Timeout] [IDG: " + GetIDGCmdString(u16Cmd) + "] " + strFuncName + "() = " + ext.Message)
        Catch ex As Exception
            LogLn("[Err] [IDG:  " + GetIDGCmdString(u16Cmd) + "] " + strFuncName + "() = " + ex.Message)
        Finally
            '----------[Note, BTPay Mini]----------
            '2017 Mar 02
            'Enzytek BLE Module & MUC communication has slow response.
            'Tx/Rx response time is around up to 200 ms.
            'So here we have to delay 100ms before Tx and delay 100ms after Rx.
            'Point (B)
            Thread.Sleep(100)
            If (bytAryTx IsNot Nothing) Then
                Erase bytAryTx
            End If
            m_tagIDGCmdDataInfo.Cleanup()
            m_bIDGCmdBusy = False
        End Try

    End Sub
    '
    '
    ' Ex 01 - Set ,Static IP = "192.168.1.1", Netmask = "255.255.255.0"
    Public Overridable Function SystemInternal_Ethernal_SetIPLayerMode(Optional ByVal chFlag As Char = "1"c, Optional ByVal strIPAddr As String = "192.168.1.1", Optional ByVal strIPAddrMask As String = "255.255.255.0", Optional ByVal strIPAddrGateway As String = "", Optional ByVal strIPAddrDNS As String = "", Optional ByVal nTimeout As Integer = ClassDevRS232.m_cnTX_TIMEOUT_IN_SEC) As Boolean
        '
        Dim u16Cmd As UInt16 = &HD113
        Dim strFuncName As String = "SystemInternal_Ethernal_SetStaticIP"
        Dim bRet As Boolean = False
        Dim nLen, nTmp As Integer

        If (m_bIDGCmdBusy) Then
            Return False
        End If
        m_bIDGCmdBusy = True

        Dim arrayByteCmdData As Byte() = Nothing
        Dim aryByteTxDataList As SortedDictionary(Of Integer, Byte()) = New SortedDictionary(Of Integer, Byte())
        Dim aryByteTmp As Byte()

        Try
            '56 69 56 4F 74 65 63 68 32 00 D1 13 00 13 31 00 31 39 32 2E 31 36 38 2E 39 39 2E 36 35 00 00 00 00 05 C3
            'chFlag = "0"c --> DHCP
            '         = "1"c --> Static
            '01 - chFlag, String,NULL End
            nLen = 0
            aryByteTmp = System.Text.Encoding.ASCII.GetBytes(chFlag)
            aryByteTxDataList.Add(nLen, aryByteTmp)

            '02 - IPAddr, String,NULL End
            nLen = nLen + 1
            aryByteTmp = System.Text.Encoding.ASCII.GetBytes(strIPAddr)
            aryByteTxDataList.Add(nLen, aryByteTmp)

            '03 - Netmask,String,NULL End
            nLen = nLen + 1
            aryByteTmp = System.Text.Encoding.ASCII.GetBytes(strIPAddrMask)
            aryByteTxDataList.Add(nLen, aryByteTmp)

            '04 - Gateway,String,NULL End
            nLen = nLen + 1
            aryByteTmp = System.Text.Encoding.ASCII.GetBytes(strIPAddrGateway)
            aryByteTxDataList.Add(nLen, aryByteTmp)

            '05 - DNS,String,NULL End
            nLen = nLen + 1
            aryByteTmp = System.Text.Encoding.ASCII.GetBytes(strIPAddrDNS)
            aryByteTxDataList.Add(nLen, aryByteTmp)

            '06 - Compute Total Length
            nLen = 0
            For Each itrByAry As KeyValuePair(Of Integer, Byte()) In aryByteTxDataList
                nLen = nLen + 1 + itrByAry.Value.Length 'Remember add NULL End , 1 byte
            Next

            '07 - Layout Tx Data in Order, Flag(ASCII),NULL,IPAddr(ASCII),NULL,Netmask(ASCII),NULL,Gateway(ASCII),NULL,DNS(ASCII),NULL
            arrayByteCmdData = New Byte(nLen - 1) {}
            nLen = 0 'Reset to offset = 0
            For Each itrByAry As KeyValuePair(Of Integer, Byte()) In aryByteTxDataList
                nTmp = itrByAry.Value.Length
                If (nTmp > 0) Then
                    System.Array.Copy(itrByAry.Value, 0, arrayByteCmdData, nLen, nTmp)
                End If
                '
                nLen = nLen + nTmp
                arrayByteCmdData(nLen) = &H0 'NULL End of String
                nLen = nLen + 1
            Next

            '-----------------------------------------------------------------[ Send to reader ]-----------------------------------------------------------------
            System_Customized_Command(u16Cmd, strFuncName, arrayByteCmdData, Nothing, AddressOf ClassReaderProtocolBasic.PreProcessCbRecv, nTimeout)

            If (bIsRetStatusOK) Then

            Else

            End If
            bRet = bIsRetStatusOK

        Catch ext As TimeoutException
            LogLn("[Err][Timeout] [IDG: " + GetIDGCmdString(u16Cmd) + "] " + strFuncName + "() = " + ext.Message)
        Catch ex As Exception
            LogLn("[Err] [IDG:  " + GetIDGCmdString(u16Cmd) + "] " + strFuncName + "() = " + ex.Message)
        Finally
            m_tagIDGCmdDataInfo.Cleanup()
            m_bIDGCmdBusy = False

            If (arrayByteCmdData IsNot Nothing) Then
                Erase arrayByteCmdData
            End If

            If (aryByteTxDataList IsNot Nothing) Then
                For Each itrByAry As KeyValuePair(Of Integer, Byte()) In aryByteTxDataList
                    aryByteTmp = itrByAry.Value
                    Erase aryByteTmp
                Next
                aryByteTxDataList.Clear()
            End If

        End Try
        '
        Return bRet
    End Function
    '
End Class
