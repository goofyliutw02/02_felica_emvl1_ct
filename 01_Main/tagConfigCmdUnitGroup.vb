﻿Public Structure tagConfigCmdUnitGroup

    Enum EnumValueRange As Integer
        LINE_MAX = 65535
    End Enum
    Public m_tagConfigCmd As tagConfigCmdUnit
    Public m_dictTLV As SortedDictionary(Of String, tagTLV)
    Public m_nGroupID As Integer
    Public m_nGroupIDOld As Integer
    Public m_nTransactionType As Integer

    Public m_strUIDriverTabName As String
    '---------------
    Sub New(ByVal nConfigCmd As ClassReaderCommanderParser.EnumConfigCmd)
        'm_tagConfigCmd = Nothing
        ''
        '' Set to integer max value
        'm_nGroupID = ValueRange.LINE_MAX
        'm_nTransactionType = ValueRange.LINE_MAX
        ''
        'm_strUIDriverTabName = ""
        Cleanup()
        m_tagConfigCmd.m_nConfigCmd = nConfigCmd

        If (m_dictTLV Is Nothing) Then
            m_dictTLV = New SortedDictionary(Of String, tagTLV)
        End If

    End Sub
    '---------------
    Sub Cleanup()
        'Dim iteratorKeyVal As KeyValuePair(Of String, tagTLV)
        m_tagConfigCmd = Nothing
        ''TLV Value Clean up
        'If m_dictTLV IsNot Nothing Then
        '    If m_dictTLV.Count > 0 Then
        '        For Each iteratorKeyVal In m_dictTLV
        '            iteratorKeyVal.Value.Cleanup()
        '        Next
        '        m_dictTLV.Clear()
        '    End If
        'End If
        ClassTableListMaster.CleanupConfigurationsTLVList(m_dictTLV)
        ' Set to integer max value
        m_nGroupID = EnumValueRange.LINE_MAX
        m_nTransactionType = EnumValueRange.LINE_MAX
        '
        m_strUIDriverTabName = ""
    End Sub

    Public Function TLVGet(tagName As String, ByRef o_tlvTag As tagTLV) As Boolean
        If (m_dictTLV Is Nothing) Then
            Return False
        End If

        If (m_dictTLV.Count < 1) Then
            Return False
        End If

        If (Not m_dictTLV.ContainsKey(tagName)) Then
            Return False
        End If
        '
        o_tlvTag = m_dictTLV.Item(tagName)
        '
        Return True
    End Function

    Public Function TLVSet(tlv As tagTLV) As Boolean
        If (m_dictTLV Is Nothing) Then
            Return False
        End If

        If (m_dictTLV.ContainsKey(tlv.m_strTag)) Then
            m_dictTLV.Remove(tlv.m_strTag)
        End If
        '
        m_dictTLV.Add(tlv.m_strTag, tlv)
        '
        Return True
    End Function

End Structure

Public Structure tagConfigCmdUnitPollmode
    Public m_tagConfigCmd As tagConfigCmdUnit
    '
    Public m_bModeOn As Byte
    '---------------
    Sub New(ByVal nConfigCmd As ClassReaderCommanderParser.EnumConfigCmd)
        m_tagConfigCmd = Nothing
        '
        m_tagConfigCmd.m_nConfigCmd = nConfigCmd
        m_bModeOn = &H0
    End Sub
    '---------------
    Sub Cleanup()
        m_tagConfigCmd = Nothing
        m_bModeOn = &H0

    End Sub

    Public Structure tagConfigCmdUnitLineMap

        Enum EnumValueRange As Integer
            LINE_MAX = 65535
        End Enum
        'Key(Index) of This Map, assigned by line number of txt file
        Public m_lTxtLine As Long
        'To know it belongs to AID List / Group Dictionary / Comment / PollMode
        Public m_tagConfigCmdType As Integer
        'Position in the Group / List
        Public m_nPosOfListGroup As Integer

        'Line Text
        Public m_strLineText As String

        Public Sub Cleanup()
            'Key(Index) of This Map, assigned by line number of txt file
            m_lTxtLine = EnumValueRange.LINE_MAX
            'To know it belongs to AID List / Group Dictionary / Comment / PollMode
            m_tagConfigCmdType = ClassReaderCommanderParser.EnumCRLCmd.CRLCMD_UNKNOWN
            'Position in the Group / List
            m_nPosOfListGroup = 0
        End Sub

    End Structure
End Structure
