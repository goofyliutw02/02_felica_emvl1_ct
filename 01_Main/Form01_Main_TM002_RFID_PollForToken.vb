﻿Imports System.Globalization

'=============================================
' 2017 Oct 25 created
' This for RFID / CL Poll For Token Purpose

Partial Public Class Form01_Main
    '
    Private m_refwinformMainForm As Form01_Main
    Private m_bActive As Boolean
    Private m_bIsBtnStopClicked As Boolean

    '
    Private Function TestTM002_Init() As Boolean
        Dim bRet As Boolean = False
        '
        m_bActive = False
        m_refwinformMainForm = Me
        'Interrupt Flag
        m_bIsBtnStopClicked = False

        'If (-1 = Rnd_01_Cmbox_Sel_Rnd.SelectedIndex) Then
        '    Rnd_01_Cmbox_Sel_Rnd.SelectedIndex = 0
        'End If
        'm_nNumOfRounds = 100

        ''
        Return bRet
    End Function

    Private Function TestTM002_Cleanup() As Boolean
        Dim bRet As Boolean = False
        '
        '
        Return bRet
    End Function
    '
    '========================[ Production Mode Common Functions ]========================
    Public Sub TestTM_Common_PassthroughMode_And_ThrowErrException(Optional ByVal bMode As Boolean = True, Optional ByVal bThrowErrException As Boolean = True)
        'm_refobjRdrCmder.SystemSetPassthroughMode(bMode)
        'If (m_refobjRdrCmder.bIsRetStatusOK) Then
        '    If (bMode) Then
        '        m_refobjUIDriver.SystemSelectionPicShowPassThrough(ClassUIDriver.ENUM_PIC_LIGHT_INDICATOR_SELECT.ON_MODE)
        '    Else
        '        m_refobjUIDriver.SystemSelectionPicShowPassThrough(ClassUIDriver.ENUM_PIC_LIGHT_INDICATOR_SELECT.OFF_MODE)
        '    End If
        'End If

        Dim strSt As String = "[Unknown]"
        '
        'Set Timeout value down to 1 sec since some beta version FW is not allowed to set it
        'if we use default timeout (10 seconds) that will cause many waiting
        m_refObjRdrCmder.SystemSetPassthroughMode(bMode, 1)
        If (m_refObjRdrCmder.bIsRetStatusOK) Then
            'If (bMode) Then
            '    m_refobjUIDriver.SystemSelectionPicShowPassThrough(ClassUIDriver.ENUM_PIC_LIGHT_INDICATOR_SELECT.ON_MODE)
            '    strSt = "ON"
            'Else
            '    m_refobjUIDriver.SystemSelectionPicShowPassThrough(ClassUIDriver.ENUM_PIC_LIGHT_INDICATOR_SELECT.OFF_MODE)
            '    strSt = "OFF"
            'End If
        End If

        If (bThrowErrException) Then
            'TestTM_Common_IDGCmd_ErrorTimeout_OR_InterruptException("Set Passthrough Mode " + strSt)
        End If
    End Sub
    '===================[ RFID Antenna Test ]===================

    Public Structure _TAG_PRINT_INFO
        Private AppendText As Form01_Main.LogLnDelegate
        Private LogLn As Form01_Main.LogLnDelegate

        Private m_nCharsMaxPerLine As Integer 'Total Num Rounds Per line
        'Private m_nCharsOkPerLine As Integer ' Num Of True/Passed Rounds per line
        Private m_listCharsInCurrentLine As List(Of Boolean)
        Private m_arrayListCharsPerLine As List(Of List(Of Boolean))

        Private m_nTotalRndsCnt As Integer
        Private m_nTotoalOk As Integer
        Private m_nLineOk As Integer
        Private m_nLineCnt As Integer

        Public Sub Init()
            m_listCharsInCurrentLine = New List(Of Boolean)
            m_arrayListCharsPerLine = New List(Of List(Of Boolean))

            AppendText = New Form01_Main.LogLnDelegate(AddressOf Form01_Main.AppendText)
            LogLn = New Form01_Main.LogLnDelegate(AddressOf Form01_Main.LogLn)
            '
            m_nCharsMaxPerLine = 50
            m_nTotalRndsCnt = 0
            m_nTotoalOk = 0
            m_nLineOk = 0
            m_nLineCnt = 0

            m_nLineBlockInterCnt = 0
            m_nLineBlockInterMax = 5
            m_s_nLineMaxToClear = 5000 ' 5k lines then reset
        End Sub
        Public Sub Cleanup()
            If (m_listCharsInCurrentLine IsNot Nothing) Then
                For Each bVar As Boolean In m_listCharsInCurrentLine

                Next
                '
                m_listCharsInCurrentLine.Clear()
            End If
            If (m_arrayListCharsPerLine IsNot Nothing) Then
                For Each listVar As List(Of Boolean) In m_arrayListCharsPerLine
                    listVar.Clear()
                Next
                m_arrayListCharsPerLine.Clear()
            End If
        End Sub
        Private Sub CleanupCurrentLine(Optional ByVal bAddToArrayList As Boolean = True)
            If (bAddToArrayList) Then
                m_arrayListCharsPerLine.Add(m_listCharsInCurrentLine)
                m_listCharsInCurrentLine = New List(Of Boolean)
            Else
                m_listCharsInCurrentLine.Clear()
            End If
        End Sub

        Private m_nLineBlockInterMax As Integer
        Private m_nLineBlockInterCnt As Integer
        Public Shared m_s_nLineMaxToClear As Integer
        Public Sub PrintBoolean(ByVal bResult As Boolean)
            Dim strMsg As String
            Dim bIsAllDone As Boolean = False
            

            If (bResult) Then
                'If Sts Ok, Add Pass Counter
                m_nLineOk = m_nLineOk + 1
                m_nTotoalOk = m_nTotoalOk + 1
            End If

            m_listCharsInCurrentLine.Add(bResult)
            If (bResult) Then
                strMsg = "O"
            Else
                strMsg = "."
            End If

            m_nTotalRndsCnt = m_nTotalRndsCnt + 1
            'Add Block Delimiter Space " " if necessary
            m_nLineBlockInterCnt = m_nLineBlockInterCnt + 1
            If (m_nLineBlockInterCnt >= m_nLineBlockInterMax) Then
                m_nLineBlockInterCnt = 0
                strMsg = strMsg + " "
            End If

            If ((m_listCharsInCurrentLine.Count >= m_nCharsMaxPerLine) Or bIsAllDone) Then
                Dim strTail As String
                Dim dblDividend As Double = m_nLineOk
                Dim dblDivisor As Double = m_nCharsMaxPerLine
                '--------[ Log Window ]--------
                '1.Tx / Rx Log Message
                '2.Error / Warning Message
                'OOXXO OOXXX XOXOX OXOXO OOXXO OOXXX XOXOX OXOXO OOXXO OOXXX [  25/  50]  50%
                'OOXXO OOXXX XOXOX OXOXO OOXXO OOXXX XOXOX OXOXO OOXXO OOXXX [  25/  50]  50%
                'OOXXO OOXXX XOXOX OXOXO OOXXO OOXXX XOXOX OXOXO OOXXO OOXXX [  25/  50]  50%
                'OOXXO OOXXX XOXOX OXOXO OOXXO OOXXX XOXOX OXOXO OOXXO OOXXX [  25/  50]  50%
                'OOXXO OOXXX XOXOX OXOXO OOXXO OOXXX XOXOX OXOXO OOXXO OOXXX [  25/  50]  50%
                'OOXXO OOXXX XOXOX OXOXO OOXXO OOXXX XOXOX OXOXO OOXXO OOXXX [  25/  50]  50%
                'OOXXO OOXXX XOXOX OXOXO OOXXO OOXXX XOXOX OXOXO OOXXO OOXXX [  25/  50]  50%
                'OOXXO OOXXX XOXOX OXOXO OOXXO OOXXX XOXOX OXOXO OOXXO OOXXX [  25/  50]  50%
                'OOXXO OOXXX XOXOX OXOXO OOXXO OOXXX XOXOX OXOXO OOXXO OOXXX [  25/  50]  50%
                'OOXXO OOXXX XOXOX OXOXO OOXXO OOXXX XOXOX OXOXO OOXXO OOXXX [  25/  50]  50%
                'OOXXO OOXXX XOXOX OXOXO OOXXO OOXXX XOXOX OXOXO OOXXO OOXXX [  25/  50]  50%
                'OOXXO OOXXX XOXOX OXOXO OOXXO OOXXX XOXOX OXOXO OOXXO OOXXX [  25/  50]  50%
                'OOXXO OOXXX XOXOX OXOXO OOXXO OOXXX XOXOX OXOXO OOXXO OOXXX [  25/  50]  50%
                'OOXXO OOXXX XOXOX OXOXO OOXXO OOXXX XOXOX OXOXO OOXXO OOXXX [  25/  50]  50%
                '[Summary] [ 350/ 700] ...  50.00%
                'Append Line Trail: Summary Info per line, ex: " [  25/  50]  50%"
                strTail = String.Format("[ {0}/ {1}] {2}%", m_nLineOk.ToString(), m_nCharsMaxPerLine.ToString(), ((dblDividend / dblDivisor) * 100.0).ToString("F", CultureInfo.InvariantCulture)) + vbCrLf
                strMsg = strMsg + strTail
                'Put Current List into the Array List Pool
                m_arrayListCharsPerLine.Add(m_listCharsInCurrentLine)
                'Create new List for next line
                m_listCharsInCurrentLine = New List(Of Boolean)
                '
                'Reset Line Vars
                m_nLineOk = 0
            End If
            '
            AppendText(strMsg)
        End Sub

        Sub PrintoutSummary()
            Dim nLineCnt As Integer = m_listCharsInCurrentLine.Count
            Dim strMsg As String
            Dim strTail As String
            Dim dblDividend As Double = m_nLineOk
            Dim dblDivisor As Double = m_nCharsMaxPerLine
            'Print Line Rest if possible
            If (nLineCnt > 0) Then
                
                Dim nFillChar As Integer = m_nCharsMaxPerLine - nLineCnt
                'Put Current List into the Array List Pool
                m_arrayListCharsPerLine.Add(m_listCharsInCurrentLine)
                dblDividend = m_nLineOk
                dblDivisor = m_nCharsMaxPerLine
                '
                strMsg = New String(" ", nFillChar + (nFillChar \ m_nLineBlockInterMax))
                '
                If ((nFillChar Mod m_nLineBlockInterMax) > 0) Then
                    strMsg = strMsg + " "
                End If

                strTail = String.Format("[ {0}/ {1}] {2}%", m_nLineOk.ToString(), m_nCharsMaxPerLine.ToString(), ((dblDividend / dblDivisor) * 100.0).ToString("F", CultureInfo.InvariantCulture)) + vbCrLf
                strMsg = strMsg + strTail
                AppendText(strMsg)
            End If

            'Printout Summary
            dblDividend = m_nTotoalOk
            dblDivisor = m_nTotalRndsCnt

            strMsg = String.Format("[ Summary ][ {0}/ {1}] ... {2}%", m_nTotoalOk.ToString(), m_nTotalRndsCnt.ToString(), ((dblDividend / dblDivisor) * 100.0).ToString("F", CultureInfo.InvariantCulture)) + vbCrLf

            AppendText(strMsg)
        End Sub

    End Structure ' _TAG_PRINT_INFO

    'Return
    ' False = Failed to run
    ' True = Okay & Run End
    Public Function TestTM002_RFIDAntenna(Optional ByVal nRunRounds As Integer = 100) As Boolean
        Dim bRun As Boolean
        Dim pCmdLine As ClassCmdLineArgumentOptions = ClassCmdLineArgumentOptions.GetInstance()

        If (-1 = nRunRounds) Then
            Return False
        End If

        Try
            If (pCmdLine.bIsPollForToken_MifareType) Then
                '
                'bRun = TestTM002_RFIDAntenna_EnhancedPassthrougMode_Mifare(nRunRounds)
                If (m_nTimeLimitSec < 0) Then
                    'Rounds Mode. -1 --> 1 rounds; -100 --> 100 rounds
                    bRun = TestTM002_RFIDAntenna_EnhancedPassthrougMode_RoundsMode(0 - m_nTimeLimitSec)
                Else
                    ' Time Limted Mode
                    bRun = TestTM002_RFIDAntenna_EnhancedPassthrougMode_Mifare_TimeWaitSecs(m_nTimeLimitSec)
                End If

            Else
                bRun = TestTM002_RFIDAntenna_EnhancedPassthrougMode(nRunRounds)
            End If



        Catch ex As Exception
            LogLn("[Err] " + ex.Message)
        Finally

        End Try
        '
        Return True
    End Function
    '
#Const _CFG_NEED_DELAY = 0

    Public Function TestTM002_RFIDAntenna_EnhancedPassthrougMode(Optional ByVal nRoundsNum As Integer = 100) As Boolean
        Dim nRetry As Integer
        Dim nRetryMax As Integer = nRoundsNum
        Dim colorResult As System.Drawing.Color = Color.Red
        Dim strFuncName As String = "TestTM002_RFIDAntenna_EnhancedPassthrougMode()"
        Dim byteCardType As Byte
        Dim arrayByteData As Byte() = Nothing
        Dim strReader As String = ""
        Dim strReaderFromFile As String = ""
        Dim strStd As String = ""
        Dim nTimeoutSec As Integer = 10
        'Dim strSave As String
        Dim bIsNewAdd As Boolean = False
        Dim bRet As Boolean = True
        Dim nTimeoutInMS As Integer = TestTM002_RFIDAntenna_EnhancedPassthrougMode_GetTxRxTimeout() '50
        Dim tagResultInfo As _TAG_PRINT_INFO = New _TAG_PRINT_INFO
        Dim nRndCnt As Integer = 0
        Dim bForeverLoopUntilHitBtnStop As Boolean
        'Dim bRun As Boolean
        Dim bRdrRespSts As Boolean
        Dim byteCardTypePredefine As Byte = 0
        'Dim nTickMax As Integer
        'Dim nTickCur As Integer

        If (m_bActive) Then
            LogLn("[Warning] " + strFuncName + " Re-entrying Rejected Since It's Running...")
            Return bRet
        End If

        m_bActive = True
        '
        If (-1 = nRoundsNum) Then
            Return False
        End If

        bForeverLoopUntilHitBtnStop = (nRetryMax = 0)
        m_bIsBtnStopClicked = False

        Try
            m_refwinformMainForm.IDGCmdTimeoutSet(, False)
            'Do Cleanup & Initial stuffs
            If (m_bIsHWTestOnly) Then
                LogLn(Form01_Main.GetTimeStampYear2Millisec())
            End If
            '------------------------[Start Processing]----------------------

            TestTM_Common_PassthroughMode_And_ThrowErrException()

            If (bForeverLoopUntilHitBtnStop) Then
                nRetryMax = 1
            End If

            tagResultInfo.Init()


            For nRetry = 1 To nRetryMax
#If _CFG_NEED_DELAY Then
                'Save Max Timeout for Tx/Rx delay check
                nTickMax = My.Computer.Clock.TickCount + nTimeoutInMS
#End If

                'PICC Poll Activate
                'm_refObjRdrCmder.GRExclusive_PT_PollInterfacePICC_Activate(byteCardType, arrayByteData)
#If 0 Then
                byteCardTypePredefine = TestTM002_RFIDAntenna_EnhancedPassthrougMode_GetSelection()

                m_refObjRdrCmder.SystemEmvL1Cmd_PT_PollForToken_TimeoutInMS_V02(byteCardTypePredefine, byteCardType, arrayByteData, nTimeoutInMS)
#Else
                m_refObjRdrCmder.SystemEmvL1Cmd_PT_PollForToken_TimeoutInMS(byteCardType, arrayByteData, nTimeoutInMS)
#End If


                

#If _CFG_NEED_DELAY Then
                nTickCur = My.Computer.Clock.TickCount

                'Delay time upto 200 if possible
                If (nTickMax >= nTickCur) Then
                    AppSleep(nTickMax - nTickCur)
                End If
#End If
                'TestTM08_ICC_IDGCmd_ErrorException("ICC Polling Test")
                bRdrRespSts = m_refObjRdrCmder.bIsRetStatusOK
                'Print out result
                tagResultInfo.PrintBoolean(bRdrRespSts)

#If 0 Then
                If (m_refObjRdrCmder.bIsRetStatusOK) Then
                    Dim strList As String() = Nothing
                    '
                    'Show Card Type
                    strReader = m_refObjRdrCmder.propGetCC_CL_CardType(byteCardType)
                    If (arrayByteData IsNot Nothing) Then
                        Dim strTmp As String = ""

                        ClassTableListMaster.ConvertFromArrayByte2String(arrayByteData, strTmp, True)
                        strReader = strReader + "| UID/SerNum =" + strTmp
                    Else
                        Throw New Exception("No UID/SerNum, Card Type  (" + byteCardType.ToString() + ")")
                    End If

                    '
                    If (strReader.Contains("N/A")) Then
                        'Not Supported Card or Value
                        Throw New Exception("[Err] Invalid Card Type = " + strReader + " (" + byteCardType.ToString() + ")")
                    End If
                    '
                    Erase strList

                    colorResult = Color.Lime
                    bRet = True
                Else
                    colorResult = Color.Red
                    strReader = "FAILED" 'm_pRefLang.GetMsgLangTranslation(ClassServiceLanguages._WTD_STRID.COMMON_FAILED) 
                End If
#End If
                'For Ever Loop, Always reset nRndCnt
                If (bForeverLoopUntilHitBtnStop) Then
                    nRetry = 0
                End If

                'Stopped by User Input
                If (m_bIsBtnStopClicked) Then
                    Exit For
                End If

                'We may do more delay here if Response Okay 
                '(Tx/Rx period is smaller than 200ms) !!

            Next
            '
            'Printout Summary Info
            tagResultInfo.PrintoutSummary()

        Catch ex As Exception
            LogLn("[Err] " + strFuncName + "() = " + ex.Message)
        Finally
            m_bActive = False


            'Finally Clean it up
            'm_refobjUIDriver.UIDriverEnableDisable(m_refwinformMainForm.ModeSelectionGet())
            Me.UIEnable(False, 2)

            ' Hide Wait Window
            m_refwinformMainForm.WaitMsgHide()
#If 0 Then
            If (bIsNewAdd) Then
                If (m_pFormStdData Is Nothing) Then
                    m_pFormStdData = FormStdDataInform.GetInstance()
                End If
                'Show Confirm Dialog box after Wait Msg Window to prevent Main Form Hidden
                m_pFormStdData.SetStandardData = strReader
                m_pFormStdData.ShowDialog()
            End If
#End If
            'Show Buttons Color
            'winobjBtn.BackColor = colorResult

            If (Not m_bIsHWTestOnly) Then
                'LogLn("=================[ End " + winobjBtn.Text + " Test  ]=================")
                LogLn(GetTimeStampYear2Millisec())
                'm_refwinformMainForm.LogSaveToFileAndCleanup(True, (winobjBtn.BackColor = Color.Lime))
            End If

            '
            If (arrayByteData IsNot Nothing) Then
                Erase arrayByteData
            End If
            '--------------------[ End of UI/Flags Process ]--------------------

            TestTM_Common_PassthroughMode_And_ThrowErrException(True, False)
            'PICC Deactivate
            'If (Not m_refwinformMainForm.bIsEngMode_TM002_RFID_No_De_Activate) Then
            '2017 Sept 27, goofy_liu , Anderson
            'we discussed and pending using 2C-0B, and instead it w/ 28-01 to turn off Antenna
            'Note : True = No Buzzer
            m_refObjRdrCmder.SystemEmvL1Cmd_CL_PT_PollInterfacePICC_DeActivate(byteCardType, arrayByteData, True)

            TestTM_Common_PassthroughMode_And_ThrowErrException(False, False)
            '
            tagResultInfo.Cleanup()
        End Try
        '
        Return bRet
    End Function
    '

    '''////////////////////////////////////////////////////////////////////////////////////////////////////
    ''' \fn Public Function TestTM002_RFIDAntenna_EnhancedPassthrougMode_Mifare(Optional ByVal nRoundsNum As Integer = 100) As Boolean Dim nRetry As Integer Dim nRetryMax As Integer = nRoundsNum Dim colorResult As System.Drawing.Color = Color.Red Dim strFuncName As String = "TestTM002_RFIDAntenna_EnhancedPassthrougMode_Mifare()" Dim byteCardType As Byte Dim arrayByteData As Byte() = Nothing Dim strReader As String = "" Dim strReaderFromFile As String = "" Dim strStd As String = "" Dim nTimeoutSec As Integer = 10
    '''
    ''' \brief  Tests time 002 rfid antenna enhanced passthroug mode - Contactless Mifare Card
    '''
    ''' \author Goofy Liu
    ''' \date   2018/4/16
    '''
    ''' \param  nRoundsNum  (Optional) The rounds number.
    '''
    ''' \return True if the test passes, false if the test fails.
    '''////////////////////////////////////////////////////////////////////////////////////////////////////

    Public Function TestTM002_RFIDAntenna_EnhancedPassthrougMode_Mifare(Optional ByVal nRoundsNum As Integer = 100) As Boolean
        Dim nRetry As Integer
        Dim nRetryMax As Integer = nRoundsNum
        Dim colorResult As System.Drawing.Color = Color.Red
        Dim strFuncName As String = "TestTM002_RFIDAntenna_EnhancedPassthrougMode_Mifare()"
        Dim byteCardType As Byte
        Dim arrayByteData As Byte() = Nothing
        Dim strReader As String = ""
        Dim strReaderFromFile As String = ""
        Dim strStd As String = ""
        Dim strTmp As String = ""
        Dim nTimeoutSec As Integer = 10
        'Dim strSave As String
        Dim bIsNewAdd As Boolean = False
        Dim bRet As Boolean = True
        Dim nTimeoutInMS As Integer = TestTM002_RFIDAntenna_EnhancedPassthrougMode_GetTxRxTimeout() '50
        Dim tagResultInfo As _TAG_PRINT_INFO = New _TAG_PRINT_INFO
        Dim nRndCnt As Integer = 0
        Dim bForeverLoopUntilHitBtnStop As Boolean
        'Dim bRun As Boolean
        Dim bRdrRespSts As Boolean
        Dim byteCardTypePredefine As Byte = 0
        Dim nTickMax As Integer = 3
        Dim nTickCur As Integer
        ' 

        If (m_bActive) Then
            LogLn("[Warning] " + strFuncName + " Re-entrying Rejected Since It's Running...")
            Return bRet
        End If

        m_bActive = True
        '
        If (-1 = nRoundsNum) Then
            Return False
        End If

        bForeverLoopUntilHitBtnStop = (nRetryMax = 0)
        m_bIsBtnStopClicked = False

        Try
            m_refwinformMainForm.IDGCmdTimeoutSet(, False)
            'Do Cleanup & Initial stuffs
            'If (m_bIsHWTestOnly) Then
            LogLn(Form01_Main.GetTimeStampYear2Millisec())
            'End If
            '------------------------[Start Processing]----------------------

            TestTM_Common_PassthroughMode_And_ThrowErrException()

            If (bForeverLoopUntilHitBtnStop) Then
                nRetryMax = 1
            End If

            tagResultInfo.Init()


            For nRetry = 1 To nRetryMax

                nTickCur = 0
                'PICC Poll Activate
                'm_refObjRdrCmder.GRExclusive_PT_PollInterfacePICC_Activate(byteCardType, arrayByteData)
                'byteCardTypePredefine = TestTM002_RFIDAntenna_EnhancedPassthrougMode_GetSelection()


                While (True)
                    'Step 01 - Poll For Token
                    ' 
                    'm_refObjRdrCmder.SystemEmvL1Cmd_PT_PollForToken_TimeoutInMS_V02(byteCardTypePredefine, byteCardType, arrayByteData, nTimeoutInMS)
                    m_refObjRdrCmder.SystemEmvL1Cmd_PT_PollForToken_TimeoutInMS(byteCardType, arrayByteData, nTimeoutInMS)

                    If (m_refObjRdrCmder.bIsRetStatusOK) Then
#If 0 Then
                    ClassTableListMaster.ConvertFromArrayByte2String(arrayByteData, strTmp)
#End If
                        nTickCur = nTickCur + 1
                    Else
                        Log_UID("N/A")
                        Log_MifareBlkRec("N/A")
                        Exit While
                    End If

                    'Step 02 - Mifare Auth , 2C-06
                    m_refObjRdrCmder.SystemEmvL1Cmd_PT_Mifare_Authentication()
                    If (m_refObjRdrCmder.bIsRetStatusOK) Then
                        ClassTableListMaster.ConvertFromArrayByte2String(arrayByteData, strTmp)
                        Log_UID("Card UID : " + strTmp)
                        nTickCur = nTickCur + 1
                    Else
                        Log_UID("N/A")
                        Log_MifareBlkRec("N/A")
                        Exit While
                    End If

                    'Step 03 - Mifare Read Block, 2C-07
                    If (arrayByteData IsNot Nothing) Then
                        Erase arrayByteData
                    End If
                    m_refObjRdrCmder.SystemEmvL1Cmd_PT_Mifare_ReadBlocks(arrayByteData)
                    If (m_refObjRdrCmder.bIsRetStatusOK) Then
#If 1 Then
                        ClassTableListMaster.ConvertFromArrayByte2String(arrayByteData, strTmp)
                        Log_MifareBlkRec("Mifare-ReadBlock : " + strTmp)
#End If
                        nTickCur = nTickCur + 1
                    Else
                        Log_MifareBlkRec("N/A")
                    End If
                    '
                    Exit While
                End While


                bRdrRespSts = (nTickCur = nTickMax)
                'Print out result
                tagResultInfo.PrintBoolean(bRdrRespSts)

                'For Ever Loop, Always reset nRndCnt
                If (bForeverLoopUntilHitBtnStop) Then
                    nRetry = 0
                End If

                'Stopped by User Input
                If (m_bIsBtnStopClicked) Then
                    Exit For
                End If

                'We may do more delay here if Response Okay 
                '(Tx/Rx period is smaller than 200ms) !!

            Next
            '
            'Printout Summary Info
            tagResultInfo.PrintoutSummary()

        Catch ex As Exception
            LogLn("[Err] " + strFuncName + "() = " + ex.Message)
        Finally
            m_bActive = False


            'Finally Clean it up
            'm_refobjUIDriver.UIDriverEnableDisable(m_refwinformMainForm.ModeSelectionGet())
            Me.UIEnable(False, 2)

            ' Hide Wait Window
            m_refwinformMainForm.WaitMsgHide()
#If 0 Then
            If (bIsNewAdd) Then
                If (m_pFormStdData Is Nothing) Then
                    m_pFormStdData = FormStdDataInform.GetInstance()
                End If
                'Show Confirm Dialog box after Wait Msg Window to prevent Main Form Hidden
                m_pFormStdData.SetStandardData = strReader
                m_pFormStdData.ShowDialog()
            End If
#End If
            'Show Buttons Color
            'winobjBtn.BackColor = colorResult

            'If (Not m_bIsHWTestOnly) Then
            'LogLn("=================[ End " + winobjBtn.Text + " Test  ]=================")
            LogLn(GetTimeStampYear2Millisec())
            'm_refwinformMainForm.LogSaveToFileAndCleanup(True, (winobjBtn.BackColor = Color.Lime))
            'End If

            '
            If (arrayByteData IsNot Nothing) Then
                Erase arrayByteData
            End If
            '--------------------[ End of UI/Flags Process ]--------------------

            'PICC Deactivate
            'If (Not m_refwinformMainForm.bIsEngMode_TM002_RFID_No_De_Activate) Then
            '2017 Sept 27, goofy_liu , Anderson
            'we discussed and pending using 2C-0B, and instead it w/ 28-01 to turn off Antenna
            'Note : True = No Buzzer
            m_refObjRdrCmder.SystemEmvL1Cmd_CL_PT_PollInterfacePICC_DeActivate(byteCardType, arrayByteData, True)

            TestTM_Common_PassthroughMode_And_ThrowErrException(False, False)
            '
            tagResultInfo.Cleanup()
        End Try
        '
        Return bRet
    End Function
    '
    Const c_strCmd_2C02_PollForToken As String = "2C02"
    Const c_strCmd_2C06_MifareAuth As String = "2C06"
    Const c_strCmd_2C07_MifareReadRec As String = "2C07"
    ', Optional ByVal nTimeLimitSec As Integer = 60
    Public Function TestTM002_RFIDAntenna_EnhancedPassthrougMode_Mifare_TimeWaitSecs_TxRx_Customized(ByRef byteCardType As Byte) As Boolean
        Dim bRet As Boolean = False
        Dim nTimeoutInMS As Integer = TestTM002_RFIDAntenna_EnhancedPassthrougMode_GetTxRxTimeout()
        Dim nTickCur As Integer = 0
        Dim nTickMax As Integer = 0
        Dim arrayByteData As Byte() = Nothing
        Dim arrayByteDataRx As Byte() = Nothing
        Dim strCmd As String
        Dim strTxDat As String
        Dim strRxDat As String
        Dim nIdxOff As Integer = 0
        Dim nTxDatLen As Integer = 0
        Dim bPollCardFound As Boolean = False
        Dim bMifareAuthOkay As Boolean = False
        Dim itrCmdTxDat As String
        'If RS232 interface, we use large Timeout Tx/Rx Latency, +10 seconds
        'If (bIsConnRS232) Then
        '    nTimeoutInMS = nTimeoutInMS + 10000
        'End If
        '
        Try
            'Tier 1 = Rounds Control
            For nIdx As Integer = 1 To 1

                For Each itrtagCmdTxDat As _TAG_PARSE_INFO_BLK In m_listTxRxSimDat
                    'Always Reset it
                    nIdxOff = 0
                    itrCmdTxDat = itrtagCmdTxDat.m_strTx
                    ' Get Cmd, 4 hex chars, idx = 0
                    strCmd = itrCmdTxDat.Substring(nIdxOff, 4)
                    nIdxOff = nIdxOff + 8

#If 1 Then
                    'Mifare - Poll For Token Check before Tx, or next Tx
                    If (Not bPollCardFound) Then
                        'Check condition here. If Poll Card No Found but
                        ' performing 2C-06, 2C-07 Mifare Operation
                        ' --> Invalid Operations
                        ' --> End this commands round
                        If (strCmd.Equals(c_strCmd_2C06_MifareAuth)) Then
                            Log_MifareBlkRec("No Card Found --> Stop Mifare Auth")
                            Exit For
                        End If
                        If (strCmd.Equals(c_strCmd_2C07_MifareReadRec)) Then
                            Log_MifareBlkRec("No Card Found --> Stop Mifare Record Read")
                            Exit For
                        End If
                    Else
                        If (Not bMifareAuthOkay) Then
                            If (strCmd.Equals(c_strCmd_2C07_MifareReadRec)) Then
                                Log_MifareBlkRec("No Auth --> Stop Mifare Record Read")
                                'Not Exit since might have more
                            End If
                        End If
                    End If
#End If
                    ' Get Tx Data, if any, idx = 0+8, len = all - 8 - 4
                    ' Note :  Tx  Tail CRC, last 4 hex chars.
                    nTxDatLen = itrCmdTxDat.Length - 8 - 4
                    If (nTxDatLen > 0) Then
                        strTxDat = itrCmdTxDat.Substring(nIdxOff, nTxDatLen)
                    Else
                        strTxDat = ""
                    End If

                    nTickMax = nTickMax + 1
                    '
                    m_refObjRdrCmder.System_Customized_TxRx_MS(strCmd, strTxDat, strRxDat, nTimeoutInMS)

                    If (m_refObjRdrCmder.bIsRetStatusOK) Then
                        nTickCur = nTickCur + 1
                    End If

#If 1 Then
                    'Mifare - Poll For Token Check after Rx

                    'If 2C-02, Poll For Token --> Display UID
                    If (strCmd.Equals(c_strCmd_2C02_PollForToken)) Then
                        If (m_refObjRdrCmder.bIsRetStatusOK) Then
                            'Print out some information in specific commands.
                            Log_UID(strRxDat)
                            bPollCardFound = True
                            Log_UID("Card UID : " + strRxDat)
                        Else
                            'Do Nothing & Jump out For Loop, No more Mifare Read Going since no card found
                            Log_UID("[Err]Card UI : N/A")
                        End If
                        Continue For
                    End If

                    ' If 2C-06, Authen Block --> N/A
                    If (strCmd.Equals(c_strCmd_2C06_MifareAuth)) Then
                        If (m_refObjRdrCmder.bIsRetStatusOK) Then
                            'Print out some information in specific commands.
                            Log_MifareBlkRec("Mifare Auth PASS")
                            bMifareAuthOkay = True
                        Else
                            'Do Nothing & Jump out For Loop, No more Mifare Read Going since no card found
                            Log_MifareBlkRec("[Err] Mifare Auth")
                            bMifareAuthOkay = False
                        End If
                        Continue For
                    End If

                    ' If 2C-07, Read Block --> Display Block Info
                    If (strCmd.Equals(c_strCmd_2C07_MifareReadRec)) Then
                        If (m_refObjRdrCmder.bIsRetStatusOK) Then
                            'Print out some information in specific commands.
                            Log_MifareBlkRec("Mifare-ReadBlock : " + strRxDat)
                        Else
                            'Do Nothing & Jump out For Loop, No more Mifare Read Going since no card found
                            Log_MifareBlkRec("[Err] Mifare Read Rec")
                        End If
                    End If
#End If
                    'Check Rx Exists ---> Compare it as result.

                    If (itrtagCmdTxDat.bIsRxCmp) Then
                        nTickMax = nTickMax + 1
                        If (itrtagCmdTxDat.IsRxDatEqualed(strRxDat)) Then
                            nTickCur = nTickCur + 1
                        End If
                    End If
                Next
            Next
        Catch ex As Exception
            LogLn(ex.Message)
        Finally
            bRet = (nTickCur = nTickMax)
            '
            If (arrayByteData IsNot Nothing) Then
                Erase arrayByteData
            End If
            If (arrayByteDataRx IsNot Nothing) Then
                Erase arrayByteDataRx
            End If

        End Try
        Return bRet
    End Function

    ', Optional ByVal nTimeLimitSec As Integer = 60
    Public Function TestTM002_RFIDAntenna_EnhancedPassthrougMode_Mifare_TimeWaitSecs_TxRx_Default(ByRef byteCardType As Byte) As Boolean
        Dim bRet As Boolean = False
        Dim nTimeoutInMS As Integer = TestTM002_RFIDAntenna_EnhancedPassthrougMode_GetTxRxTimeout()
        Dim nTickCur As Integer = 0
        Dim nTickMax As Integer = 3
        Dim strTmp As String
        Dim arrayByteData As Byte() = Nothing
        '
        For nIdx As Integer = 1 To 1
            'Step 01 - Poll For Token
            'm_refObjRdrCmder.SystemEmvL1Cmd_PT_PollForToken_TimeoutInMS_V02(byteCardTypePredefine, byteCardType, arrayByteData, nTimeoutInMS)
            m_refObjRdrCmder.SystemEmvL1Cmd_PT_PollForToken_TimeoutInMS(byteCardType, arrayByteData, nTimeoutInMS)

            If (m_refObjRdrCmder.bIsRetStatusOK) Then
#If 0 Then
                    ClassTableListMaster.ConvertFromArrayByte2String(arrayByteData, strTmp)
#End If
                nTickCur = nTickCur + 1
            Else
                Log_UID("N/A")
                Log_MifareBlkRec("N/A")
                Exit For
            End If

            'Step 02 - Mifare Auth , 2C-06
            m_refObjRdrCmder.SystemEmvL1Cmd_PT_Mifare_Authentication()
            If (m_refObjRdrCmder.bIsRetStatusOK) Then
                ClassTableListMaster.ConvertFromArrayByte2String(arrayByteData, strTmp)
                Log_UID("Card UID : " + strTmp)
                nTickCur = nTickCur + 1
            Else
                Log_UID("N/A")
                Log_MifareBlkRec("N/A")
                Exit For
            End If

            'Step 03 - Mifare Read Block, 2C-07
            If (arrayByteData IsNot Nothing) Then
                Erase arrayByteData
            End If
            m_refObjRdrCmder.SystemEmvL1Cmd_PT_Mifare_ReadBlocks(arrayByteData)
            If (m_refObjRdrCmder.bIsRetStatusOK) Then
                ClassTableListMaster.ConvertFromArrayByte2String(arrayByteData, strTmp)
                Log_MifareBlkRec("Mifare-ReadBlock : " + strTmp)
                nTickCur = nTickCur + 1
            Else
                Log_MifareBlkRec("N/A")
            End If
            '
        Next

        bRet = (nTickCur = nTickMax)
        Return bRet
    End Function
    ' 
    Public Function TestTM002_RFIDAntenna_EnhancedPassthrougMode_RoundsMode(Optional ByVal nRoundsMax As Integer = 1) As Boolean
        Dim colorResult As System.Drawing.Color = Color.Red
        Dim strFuncName As String = "TestTM002_RFIDAntenna_EnhancedPassthrougMode_RoundsMode()"
        Dim nRounds As Integer = 1
        Dim byteCardType As Byte
        Dim arrayByteData As Byte() = Nothing
        Dim strReader As String = ""
        Dim strReaderFromFile As String = ""
        Dim strStd As String = ""
        Dim strTmp As String = ""
        Dim nTimeoutSec As Integer = 10
        'Dim strSave As String
        Dim bIsNewAdd As Boolean = False
        Dim bRet As Boolean = True
        Dim nTimeoutInMS As Integer = TestTM002_RFIDAntenna_EnhancedPassthrougMode_GetTxRxTimeout() '50
        Dim tagResultInfo As _TAG_PRINT_INFO = New _TAG_PRINT_INFO
        Dim nRndCnt As Integer = 0
        Dim bForeverLoopUntilHitBtnStop As Boolean
        'Dim bRun As Boolean
        Dim bRdrRespSts As Boolean
        Dim byteCardTypePredefine As Byte = 0
        'Dim nTickMax As Integer = 3
        'Dim nTickCur As Integer
        ' 

        If (m_bActive) Then
            LogLn("[Warning] " + strFuncName + " Re-entrying Rejected Since It's Running...")
            Return bRet
        End If

        m_bActive = True

        m_bIsBtnStopClicked = False

        Try
            m_refwinformMainForm.IDGCmdTimeoutSet(, False)

            LogLn(Form01_Main.GetTimeStampYear2Millisec())

            '------------------------[Start Processing]----------------------
            If ((Not m_refObjRdrCmder.bIsConnectedUSBHID_KBWedge) And (Not m_refObjRdrCmder.bIsConnectedUSBHID)) Then
                '2018 May 17, goofy_liu, for Chingwei USB-KB case.
                'Don't do PT setting in USB-KB mode
                TestTM_Common_PassthroughMode_And_ThrowErrException()
            End If

            tagResultInfo.Init()

            For nRounds = 1 To nRoundsMax

                If (bIsTxRxSimDatMode) Then
                    ', nTimeLimitSec
                    bRdrRespSts = TestTM002_RFIDAntenna_EnhancedPassthrougMode_Mifare_TimeWaitSecs_TxRx_Customized(byteCardType)
                Else
                    ', nTimeLimitSec
                    bRdrRespSts = TestTM002_RFIDAntenna_EnhancedPassthrougMode_Mifare_TimeWaitSecs_TxRx_Default(byteCardType)
                End If

                'Print out result
                tagResultInfo.PrintBoolean(bRdrRespSts)

                'For Ever Loop, Always reset nRndCnt
                If (bForeverLoopUntilHitBtnStop) Then
                    Exit For
                End If

                'Stopped by User Input
                If (m_bIsBtnStopClicked) Then
                    Exit For
                End If
                '
            Next
            '
            'Printout Summary Info
            tagResultInfo.PrintoutSummary()

        Catch ex As Exception
            LogLn("[Err] " + strFuncName + "() = " + ex.Message)
        Finally
            m_bActive = False


            Me.UIEnable(False, 2)

            ' Hide Wait Window
            m_refwinformMainForm.WaitMsgHide()


            LogLn(GetTimeStampYear2Millisec())

            '
            If (arrayByteData IsNot Nothing) Then
                Erase arrayByteData
            End If
            '--------------------[ End of UI/Flags Process ]--------------------

            If ((Not m_refObjRdrCmder.bIsConnectedUSBHID_KBWedge) And (Not m_refObjRdrCmder.bIsConnectedUSBHID)) Then
                '2018 May 17, goofy_liu
                'Since there is USB-KB mode application then we skip this.

                ' Must Enable Pass-through mode before Deactivate PICC
                TestTM_Common_PassthroughMode_And_ThrowErrException(True, False)
                'PICC Deactivate
                'If (Not m_refwinformMainForm.bIsEngMode_TM002_RFID_No_De_Activate) Then
                '2017 Sept 27, goofy_liu , Anderson
                'we discussed and pending using 2C-0B, and instead it w/ 28-01 to turn off Antenna
                'Note : True = No Buzzer
                m_refObjRdrCmder.SystemEmvL1Cmd_CL_PT_PollInterfacePICC_DeActivate(byteCardType, arrayByteData, True)

                TestTM_Common_PassthroughMode_And_ThrowErrException(False, False)
            End If

            '
            tagResultInfo.Cleanup()
        End Try
        '
        Return bRet
    End Function

    ' 
    Public Function TestTM002_RFIDAntenna_EnhancedPassthrougMode_Mifare_TimeWaitSecs(Optional ByVal nTimeLimitSec As Integer = 60) As Boolean
        Dim nTimeLimtTicks As Integer
        Dim nTimeLimitTicksMax As Integer = nTimeLimitSec * 1000
        Dim colorResult As System.Drawing.Color = Color.Red
        Dim strFuncName As String = "TestTM002_RFIDAntenna_EnhancedPassthrougMode_Mifare_TimeWaitSecs()"
        Dim byteCardType As Byte
        Dim arrayByteData As Byte() = Nothing
        Dim strReader As String = ""
        Dim strReaderFromFile As String = ""
        Dim strStd As String = ""
        Dim strTmp As String = ""
        Dim nTimeoutSec As Integer = 10
        'Dim strSave As String
        Dim bIsNewAdd As Boolean = False
        Dim bRet As Boolean = True
        Dim nTimeoutInMS As Integer = TestTM002_RFIDAntenna_EnhancedPassthrougMode_GetTxRxTimeout() '50
        Dim tagResultInfo As _TAG_PRINT_INFO = New _TAG_PRINT_INFO
        Dim nRndCnt As Integer = 0
        Dim bForeverLoopUntilHitBtnStop As Boolean
        'Dim bRun As Boolean
        Dim bRdrRespSts As Boolean
        Dim byteCardTypePredefine As Byte = 0
        Dim nTickMax As Integer = 3
        Dim nTickCur As Integer
        ' 

        If (m_bActive) Then
            LogLn("[Warning] " + strFuncName + " Re-entrying Rejected Since It's Running...")
            Return bRet
        End If

        m_bActive = True
        '
        If (-1 = nTimeLimitSec) Then
            Return False
        End If

        'bForeverLoopUntilHitBtnStop = (nTimeLimitSecMax = 0)
        m_bIsBtnStopClicked = False

        Try
            m_refwinformMainForm.IDGCmdTimeoutSet(, False)
            'Do Cleanup & Initial stuffs
            'If (m_bIsHWTestOnly) Then
            LogLn(Form01_Main.GetTimeStampYear2Millisec())
            'End If
            '------------------------[Start Processing]----------------------

            TestTM_Common_PassthroughMode_And_ThrowErrException()

            'If (bForeverLoopUntilHitBtnStop) Then
            '    nTimeLimitSecMax = 1
            'End If

            tagResultInfo.Init()

            nTimeLimitTicksMax = nTimeLimitTicksMax + My.Computer.Clock.TickCount

            While ((nTimeLimitTicksMax > My.Computer.Clock.TickCount))

                nTickCur = 0

#If 1 Then
                If (bIsTxRxSimDatMode) Then
                    bRdrRespSts = TestTM002_RFIDAntenna_EnhancedPassthrougMode_Mifare_TimeWaitSecs_TxRx_Customized(byteCardType)
                Else
                    bRdrRespSts = TestTM002_RFIDAntenna_EnhancedPassthrougMode_Mifare_TimeWaitSecs_TxRx_Default(byteCardType)
                End If

#Else
                For nIdx As Integer = 1 To 1
                    'Step 01 - Poll For Token
                    'm_refObjRdrCmder.SystemEmvL1Cmd_PT_PollForToken_TimeoutInMS_V02(byteCardTypePredefine, byteCardType, arrayByteData, nTimeoutInMS)
                    m_refObjRdrCmder.SystemEmvL1Cmd_PT_PollForToken_TimeoutInMS(byteCardType, arrayByteData, nTimeoutInMS)

                    If (m_refObjRdrCmder.bIsRetStatusOK) Then
#If 0 Then
                    ClassTableListMaster.ConvertFromArrayByte2String(arrayByteData, strTmp)
#End If
                        nTickCur = nTickCur + 1
                    Else
                        Log_UID("N/A")
                        Log_MifareBlkRec("N/A")
                        Exit For
                    End If

                    'Step 02 - Mifare Auth , 2C-06
                    m_refObjRdrCmder.SystemEmvL1Cmd_PT_Mifare_Authentication()
                    If (m_refObjRdrCmder.bIsRetStatusOK) Then
                        ClassTableListMaster.ConvertFromArrayByte2String(arrayByteData, strTmp)
                        Log_UID("Card UID : " + strTmp)
                        nTickCur = nTickCur + 1
                    Else
                        Log_UID("N/A")
                        Log_MifareBlkRec("N/A")
                        Exit For
                    End If

                    'Step 03 - Mifare Read Block, 2C-07
                    If (arrayByteData IsNot Nothing) Then
                        Erase arrayByteData
                    End If
                    m_refObjRdrCmder.SystemEmvL1Cmd_PT_Mifare_ReadBlocks(arrayByteData)
                    If (m_refObjRdrCmder.bIsRetStatusOK) Then
                        ClassTableListMaster.ConvertFromArrayByte2String(arrayByteData, strTmp)
                        Log_MifareBlkRec("Mifare-ReadBlock : " + strTmp)
                        nTickCur = nTickCur + 1
                    Else
                        Log_MifareBlkRec("N/A")
                    End If
                    '
                Next
                '
                bRdrRespSts = (nTickCur = nTickMax)
#End If
              
                'Print out result
                tagResultInfo.PrintBoolean(bRdrRespSts)

                'For Ever Loop, Always reset nRndCnt
                If (bForeverLoopUntilHitBtnStop) Then
                    nTimeLimtTicks = 0
                End If

                'Stopped by User Input
                If (m_bIsBtnStopClicked) Then
                    Exit While
                End If
                '
                'Form01_Main.AppSleep(100)
            End While
            '
            'Printout Summary Info
            tagResultInfo.PrintoutSummary()

        Catch ex As Exception
            LogLn("[Err] " + strFuncName + "() = " + ex.Message)
        Finally
            m_bActive = False


            Me.UIEnable(False, 2)

            ' Hide Wait Window
            m_refwinformMainForm.WaitMsgHide()


            LogLn(GetTimeStampYear2Millisec())

            '
            If (arrayByteData IsNot Nothing) Then
                Erase arrayByteData
            End If
            '--------------------[ End of UI/Flags Process ]--------------------
            ' Must Enable Pass-through mode before Deactivate PICC
            TestTM_Common_PassthroughMode_And_ThrowErrException(True, False)
            'PICC Deactivate
            'If (Not m_refwinformMainForm.bIsEngMode_TM002_RFID_No_De_Activate) Then
            '2017 Sept 27, goofy_liu , Anderson
            'we discussed and pending using 2C-0B, and instead it w/ 28-01 to turn off Antenna
            'Note : True = No Buzzer
            m_refObjRdrCmder.SystemEmvL1Cmd_CL_PT_PollInterfacePICC_DeActivate(byteCardType, arrayByteData, True)

            TestTM_Common_PassthroughMode_And_ThrowErrException(False, False)
            '
            tagResultInfo.Cleanup()
        End Try
        '
        Return bRet
    End Function
    '
#If 0 Then
    Public Sub TestTM002_RFIDAntenna_ActivateTransaction(Optional winobjBtn As System.Windows.Forms.Button = Nothing, Optional ByVal bDoCompare As Boolean = False)
        Dim colorResult As System.Drawing.Color = Color.Red
        Dim strFuncName As String = "TestTM002_RFIDAntenna_ActivateTransaction()"
        'Dim byteCardType As Byte
        Dim arrayByteData As Byte() = Nothing
        Dim strReader As String = ""
        Dim strStd As String = ""
        'Dim strSave As String
        Dim nTimeout As Integer = 10

        If (m_bActive) Then
            LogLn("[Warning] " + strFuncName + " Re-entrying Rejected Since It's Running...")
            Return
        End If

        m_bActive = True
        '
        winobjBtn.BackColor = Color.LightSeaGreen
        '
        Try
            m_refwinformMainForm.IDGCmdTimeoutSet(nTimeout, False)
            '

            'Do Cleanup & Initial stuffs
            m_refobjUIDriver.UIDriverEnableDisable(ClassUIDriver.EnumUIDriverEventEnableDisable.UI_ENABLE_TRANSACTION_START)
            'm_refobjTransaction.Cleanup()
            LogLn(ClassTestScenarioBasic.GetTimeStampYear2Millisec())
            LogLn("=================[ Start " + winobjBtn.Text + " Test  ]=================")



            '------------------------[Start Processing]----------------------

            LogLn(">>>>>>>>>>>> Present Card <<<<<<<<<<<< ")
            'Display Text onto Reader's LCD
            If (m_objReaderInfo.bIsLCDSupported) Then
                TestTM_Common_UI_LCDClear(nTimeout)
                'm_refObjRdrCmder.SystemLCDClear(nTimeout)
                TestTM_Common_IDGCmd_ErrorTimeout_OR_InterruptException(ClassTestScenarioBasic.m_sstrExcptMsgLCDClear)
                'Turn On LCD Custom Display Mode
                'm_objRdrCmderIDGAR.SystemLCDCustomDisplayModeSetOn()
                TestTM_Common_UI_LCDCustomDisplayMode_And_ThrowErrExceptinOccured_Deprecated(True, False)
                TestTM_Common_IDGCmd_ErrorTimeout_OR_InterruptException("Set LCD Custom Display Mode On")

                TestTM_Common_UI_LCDTextDisplay("Activate", True, True, nTimeout)
                TestTM_Common_UI_LCDTextDisplay("Transaction", False, False, nTimeout)
            End If

            'Show Wait Window
            m_refwinformMainForm.WaitMsgShow(True, ClassServiceLanguages._WTD_STRID.TM002_PC, False, 0)
            m_refwinformMainForm.LogInfoMain(ClassServiceLanguages._WTD_STRID.TM002_PCPN, Color.Lime)
            'm_refwinformMainForm.WaitMsgShow(True, "Do Transaction")
            'm_refwinformMainForm.LogInfoMain("Present Card (PN = 80005203-001)", Color.Lime)

            ' Activate Transaction
            m_refobjTransaction.TxnActivate()


            '------------------------[Parse Result]----------------------
            Dim txnResult As ClassTransaction.TAG_TXN_PARSE_RESPONSE_INFO = m_refobjTransaction.GetInfoTransactionResponse
            strStd = m_pRefLang.GetMsgLangTranslation(COMMON_NA) ' "N/A, No compared data available"
            If (txnResult.bIsProtocolRetStatusOK) Then
                strReader = m_pRefLang.GetMsgLangTranslation(TM002_TXN_PASSED) '"PASSED, Transaction OK"
                colorResult = Color.Lime
            Else
                If (txnResult.bIsProtocolRetStatusFaileWithTransactionStatusOnlineAuthReq) Then
                    strReader = m_pRefLang.GetMsgLangTranslation(TM002_TXN_PASSED) ' "PASSED, Transaction OK"
                    colorResult = Color.Lime
                Else
                    If (m_refwinformMainForm.bIsWaitMsgEnd) Then
                        strReader = m_pRefLang.GetMsgLangTranslation(TM002_TXN_FAILED_INT_BY_USER) '"FAILED , Transaction Failed since it's interrupted by user "
                        m_refobjTransaction.TxnCancel()
                    Else
                        strReader = m_pRefLang.GetMsgLangTranslation(TM002_TXN_FAILED_SINCE_) + txnResult.strGetTransactionResult '"FAILED , Transaction Failed since "
                    End If
                    colorResult = Color.Red
                End If
            End If

            '------------------------[End Processing]----------------------
        Catch ex As Exception
            LogLn("[Err] " + strFuncName + "() = " + ex.Message)
            nTimeout = 1
        Finally
            '
            m_bActive = False

            If (bDoCompare) Then
                m_refobjUIDriver.PrintoutQCModeWindowCmpResult(strStd, strReader)
            Else
                m_refwinformMainForm.LogInfoMain(strReader, colorResult)
            End If

            'Finally Clean it up
            m_refobjUIDriver.UIDriverEnableDisable(m_refwinformMainForm.ModeSelectionGet())

            ' Hide Wait Window
            m_refwinformMainForm.WaitMsgHide()


            'Show Buttons Color
            winobjBtn.BackColor = colorResult

            LogLn("=================[ End " + winobjBtn.Text + " Test  ]=================")
            LogLn(ClassTestScenarioBasic.GetTimeStampYear2Millisec())
            m_refwinformMainForm.LogSaveToFileAndCleanup(True, (winobjBtn.BackColor = Color.Lime))
            '
            If (arrayByteData IsNot Nothing) Then
                Erase arrayByteData
            End If

            '----------------------------[ End of UI / Flags Process ]----------------------------

            If (m_objReaderInfo.bIsLCDSupported) Then
                'Restore LCD Custom Display = Off
                'm_objRdrCmderIDGAR.SystemLCDCustomDisplayModeSetOff()
                TestTM_Common_UI_LCDCustomDisplayMode_And_ThrowErrExceptinOccured_Deprecated(False, False)
                TestTM_Common_UI_LCDClear(nTimeout)
                'm_refObjRdrCmder.SystemLCDClear(nTimeout)
            End If

        End Try
    End Sub
#End If
    '
    '2C-02 new document
    'https://atlassian.idtechproducts.com/confluence/download/attachments/44076848/2C-02_VP3300usb_20171108.docx?version=1&modificationDate=1510195024426&api=v2
    Private Function TestTM002_RFIDAntenna_EnhancedPassthrougMode_GetSelection() As Byte
        Dim strCardType As String 'Ex: "A+ B+ C", "A+ C", "B"
        Dim byteCardTypeRet As Byte = &H0
        If (-1 = cbox_SeekCardType.SelectedIndex) Then
            cbox_SeekCardType.SelectedIndex = 0
        End If
        strCardType = CType(cbox_SeekCardType.SelectedItem, String)
        '
        If (strCardType.Contains("A")) Then
            byteCardTypeRet = byteCardTypeRet Or &H1
        End If
        If (strCardType.Contains("B")) Then
            byteCardTypeRet = byteCardTypeRet Or &H2
        End If
        If (strCardType.Contains("C")) Then
            byteCardTypeRet = byteCardTypeRet Or &H4
        End If
        '
        Return byteCardTypeRet
    End Function

    Private Function TestTM002_RFIDAntenna_EnhancedPassthrougMode_GetTxRxTimeout() As Integer
        Dim nTimeoutTxRx As Integer = 10

        Return NumUpDwn_Timeout_TxRx.Value
    End Function

End Class
