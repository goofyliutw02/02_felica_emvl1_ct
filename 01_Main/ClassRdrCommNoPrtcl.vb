﻿'=====================================================
' File Name: ClassRdrCommNoPrtcl.vb
' Description: This is communication class for RS232 and USB-HID Interfaces wo/ Protocol.
'
' Revision:
'---[ Initial Version ]---
' 2017 Feb 03, by goofy_liu@idtechproducts.com.tw
'====================================================
'
'----------------------[ Importing Stuffs ]----------------------
Imports System.Runtime.Serialization
Imports System.Runtime.Serialization.Formatters.Binary
'Imports DiagTool_HardwareExclusive.ClassReaderProtocolBasicIDGAR
Imports System.IO
Imports System.IO.Ports
Imports System.Threading

'-------------------[ Class Declaration ]-------------------
Public Class ClassRdrCommNoPrtcl
    '
    Inherits ClassReaderProtocolBasic
    Implements IDisposable

#Region "IDisposable Support"
    Private disposedValue As Boolean ' To detect redundant calls

    ReadOnly Property bIsTxBusy As Boolean
        Get
            Return m_bIsTxBusy
        End Get
    End Property

    ReadOnly Property bIsRxBusy As Boolean
        Get
            Return m_bIsRxBusy
        End Get
    End Property


    ReadOnly Property strCOMPort As String
        Get
            Return m_devRS232.strCOMPort
        End Get
    End Property

#End Region

    '
    '---[All major stuffs placeholder]---
#If 0 Then
    Protected m_refdictTlvNewOpConfigurations As Dictionary(Of String, tagTLVNewOp)
    Protected m_refdictTlvNewOpCAPK As Dictionary(Of String, tagTLVNewOpCAPK)
    Protected m_nIDGCommanderType As ClassReaderInfo.ENU_FW_PLATFORM_TYPE

     Public Shared s_strProtocolV2Header As String = "ViVOtech2"
    Public Shared s_arrayByteV2Header As Byte() = New Byte() {&H56, &H69, &H56, &H4F, &H74, &H65, &H63, &H68, &H32}
    '======================[ Error Code , Message ]==========================

    Enum EnumPROTOCOLTYPE As Byte
        PROTOCOL1 = 0 ' GR Protocol Type 1
        PROTOCOL2 ' GR Protocol Type 2
    End Enum
#End If

    '=============[ Public Shared Objects Section ]=====================

    Protected m_bIsRetOK As Boolean 'True = Rx Conditions(AT Least one of Header and Tail Patterns) matched
    Protected m_bIsRxOK As Boolean ' True = Data In; False = No Data In after T.O.
    Protected m_bIsTxBusy As Boolean
    Protected m_bIsRxBusy As Boolean
    'Protected m_tagIDGCmdDataInfo As tagIDGCmdData
    '
    Private Shared m_s_pInst As ClassRdrCommNoPrtcl = Nothing
    Public Shared Function GetInstance(ByRef refwinformMain As Form01_Main, ByRef refobjCTLB As ClassTableListMaster) As ClassRdrCommNoPrtcl
        '
        If (m_s_pInst Is Nothing) Then
            m_s_pInst = New ClassRdrCommNoPrtcl(refwinformMain, refobjCTLB)
        Else
            'Since this may be free in Cleanup() of instance.
            'We re-allocate memory here !
            If (m_s_pInst.m_arraybyteSendBuf Is Nothing) Then
                m_s_pInst.m_arraybyteSendBuf = New Byte(ClassDevRS232.m_cnRX_BUF_MAX - 1) {}
            End If

        End If
        '
        Return m_s_pInst
    End Function
    '
    'New (Constructor) can't override...
    Private Sub New(ByRef refwinformMain As Form01_Main, ByRef refobjCTLB As ClassTableListMaster)
        MyBase.New(refwinformMain, refobjCTLB)
        '
        m_devRS232 = New ClassDevRS232(refwinformMain)
        m_devUSBHID = New ClassDevUSBHID(refwinformMain)
        '
        m_refwinformMain = Form01_Main.GetInstance()
        '
        'Create Logging Stuffs
        LogLnThreadSafe = New Form01_Main.LogLnDelegate(AddressOf m_refwinformMain.LogThreadSafe)
        DumpingRecv = New Form01_Main.LogLnDelegate(AddressOf m_refwinformMain.DumpingRecvAsciiStr)
        DumpingRecvHexStr = New Form01_Main.LogLnDelegate(AddressOf m_refwinformMain.DumpingRecvHexStr)

        CalledByPreProcessCbRecv_InterruptByExternalStatusUpdate = New Form01_Main.CalledByPreProcessCbRecv_IBESU(AddressOf m_refwinformMain.CalledByPreProcessCbRecv_InterruptByExternalStatusUpdate)

        LogLn = New Form01_Main.LogLnDelegate(AddressOf m_refwinformMain.LogLn)
        '
        m_arraybyteSendBuf = New Byte(ClassDevRS232.m_cnRX_BUF_MAX - 1) {}
        m_refobjCTLM = refobjCTLB

        Init()
    End Sub


    '
    '---[Rx Procedure for RS232/COM]---
#If 1 Then
    Private Shared Sub PreProcessCbRecv_NormalMode(ByRef devSerialPort As SerialPort, ByRef refIDGCmdData As tagIDGCmdData, ByVal bRS232CommOK As Boolean)
        Dim nDataLen As Integer
        Dim nDataLen2 As Integer

        Try

            'LogLnThreadSafe("PPCB:001")
            nDataLen = devSerialPort.BytesToRead 'find number of bytes in buf
            'Prevent from garbage call
            If (nDataLen = 0) Then
                Return
            End If
            'LogLnThreadSafe("PPCB:002")

            ' Boundary Checking to prevent from buffer overflow
            If (nDataLen > (refIDGCmdData.m_refarraybyteData.Count - refIDGCmdData.m_nDataRecvOff)) Then
                nDataLen = refIDGCmdData.m_refarraybyteData.Count - refIDGCmdData.m_nDataRecvOff
            End If

            'No More Space to Rx Data
            If (nDataLen = 0) Then
                Return
            End If
            '
            nDataLen2 = devSerialPort.Read(refIDGCmdData.m_refarraybyteData, refIDGCmdData.m_nDataRecvOff, nDataLen)

            'Compute Read Data Accumulation Length
            refIDGCmdData.m_nDataRecvOff = refIDGCmdData.m_nDataRecvOff + nDataLen2
            'LogLnThreadSafe("PPCB:004")

            'Check Interrupt Command present
            CalledByPreProcessCbRecv_InterruptByExternalStatusUpdate(refIDGCmdData)

        Catch ex As Exception
            LogLnThreadSafe("[Rx - Err] " + ex.Message)
        End Try
    End Sub
    '
    Private Shared Sub PreProcessCbRecv_DumpingMode(ByRef devSerialPort As SerialPort, ByRef refIDGCmdData As tagIDGCmdData, ByVal bRS232CommOK As Boolean)
        Dim nDataLen As Integer
        Dim nDataLen2 As Integer
        Dim strDumpTmp As String = ""
        '
        Try
            nDataLen = devSerialPort.BytesToRead 'find number of bytes in buf
            'Prevent from garbage call
            If (nDataLen = 0) Then
                Return
            End If
            ' Boundary Checking to prevent from buffer overflow
            If (nDataLen > refIDGCmdData.m_refarraybyteData.Count) Then
                nDataLen = refIDGCmdData.m_refarraybyteData.Count
            End If

            nDataLen2 = devSerialPort.Read(refIDGCmdData.m_refarraybyteData, 0, nDataLen)
            '
            'Dumping It Now
            ClassTableListMaster.ConvertFromArrayByte2String(refIDGCmdData.m_refarraybyteData, 0, nDataLen2, strDumpTmp)
            '2016 April 24(Mon) - Remove Auto Poll+Burst Mode Dumpping Message to haste TS
            'LogLnThreadSafe("[Hex Data]=" + strDumpTmp)
            DumpingRecvHexStr(strDumpTmp)

            ClassTableListMaster.ConvertFromHexToAscii(refIDGCmdData.m_refarraybyteData, 0, nDataLen2, strDumpTmp)

            DumpingRecv(strDumpTmp)
        Catch ex As Exception
            LogLnThreadSafe("[Rx Dumping Mode ] " + ex.Message)
        End Try
    End Sub

    '========================================[]==========================================
    'IDG CMD:_2900
    Private Shared Sub CommonCbRecv(ByRef devSerialPort As SerialPort, ByRef o_IDGCmdData As tagIDGCmdData, ByVal bRS232CommOK As Boolean)
        Try
            ' Common Process to receive data 
            PreProcessCbRecv(devSerialPort, o_IDGCmdData, bRS232CommOK)
            '
        Catch ex As Exception
            LogLnThreadSafe("[Err] SystemGetFirmwareVersionCbRecv() = " + ex.Message)
        End Try
    End Sub

#End If
    '
#If 0 Then


    '
    Public Sub System_Customized_Command(ByVal arrayByteSendData As Byte(), ByRef o_arrayByteRecv As Byte(), callbackExternalRecvProcedure As ClassDevRS232.DelegatecallbackExternalRecvProcedure, Optional ByVal nTimeout As Integer = ClassDevRS232.m_cnTX_TIMEOUT_IN_SEC, Optional _
                                         bWait As Boolean = False)
        Try
            m_bIsRxOK = False

            m_tagIDGCmdDataInfo.Reinit(&H0, m_arraybyteSendBuf, Nothing)

            'Add Send Data
            If (arrayByteSendData IsNot Nothing) Then
                m_tagIDGCmdDataInfo.ComposerAddData(arrayByteSendData)
            End If

            '---[ Tx to reader ]---
            '[X] V2 Protocol Header -
            'Compose 1)Protocol Header 2) Command Set 3) Length 4) Command Frame CRC Data
            'PktComposerCommandFrameHeaderCmdSetDataLengthCRCData(m_tagIDGCmdDataInfo, bCRCMSB)

            '----------------[ Debug Section ]----------------
            If (m_bDebug) Then
                Form01_Main.LogLnDumpData(m_tagIDGCmdDataInfo.m_refarraybyteData.Skip(m_tagIDGCmdDataInfo.m_nDataRecvOffSkipBytes).ToArray(), m_tagIDGCmdDataInfo.m_nDataSizeRecv)
            End If

            '----------------[ End Debug Section ]----------------
            'Waiting For Response in SetConfigurationsGroupSingleCbRecv()
            If (callbackExternalRecvProcedure Is Nothing) Then
                callbackExternalRecvProcedure = AddressOf ClassRdrCommNoPrtcl.PreProcessCbRecv
            End If

            'bWait = False ==> Tx ONLY, 
            bWait = False
            'Don't care platform here since no protocol in use
            m_refdevConn.SendCommandFrameAndWaitForResponseFrame(callbackExternalRecvProcedure, m_tagIDGCmdDataInfo, ClassReaderInfo.ENU_FW_PLATFORM_TYPE.UNKNOWN, bWait, nTimeout * 1000)

            'Its' response code = 0 => OK
            If (m_tagIDGCmdDataInfo.bIsResponseOK) Then
                m_bIsRxOK = True
            End If

            m_tagIDGCmdDataInfo.ParserDataGetArrayRawData(o_arrayByteRecv)

        Catch ext As TimeoutException
            LogLn("[Err][Timeout] " + ext.Message)
        Catch ex As Exception
            LogLn("[Err] " + ex.Message)
            Throw 'ex
        Finally
            m_bIsRxBusy()
        End Try
    End Sub
#End If
    '
    '(a) @strHdr = "" --> No Header String Check
    '(b) @strTail = "" --> No Tail String Check
    '(c) If (b) Not exist --> Rx Process will Rx & wait until timeout
    '(d) If (a)+(b) --> [OUT] o_strHexRx will be the payload string value between (a) & (b)
    Public Sub System_Customized_Command_DirectSend_HdrTail_StrHex(strHexTx As String, strHexHdr As String, strHexTail As String, ByRef o_strHexRx As String, Optional ByVal nTimeout As Integer = ClassDevRS232.m_cnTX_TIMEOUT_IN_SEC)
        Dim arrayByteSendData As Byte() = Nothing
        Dim arrayByteHdr As Byte() = Nothing
        Dim arrayByteTail As Byte() = Nothing
        Dim o_arrayByteRecv As Byte() = New Byte(ClassDevRS232.m_cnRX_BUF_MAX - 1) {}
        Dim nRxLen As UInt32 = ClassDevRS232.m_cnRX_BUF_MAX
        Try
            'Init Value
            o_strHexRx = ""

            'Tx Hex Data 2 Binary Array
            If (Not strHexTx.Equals("")) Then
                ClassTableListMaster.TLVauleFromString2ByteArray(strHexTx, arrayByteSendData)
            End If
            If (Not strHexHdr.Equals("")) Then
                ClassTableListMaster.TLVauleFromString2ByteArray(strHexHdr, arrayByteHdr)
            End If
            If (Not strHexTail.Equals("")) Then
                ClassTableListMaster.TLVauleFromString2ByteArray(strHexTail, arrayByteTail)
            End If

            'Send it
            System_Customized_Command_DirectSend_RxChk_HdrTail_ArrayByte(arrayByteSendData, arrayByteHdr, arrayByteTail, o_arrayByteRecv, nRxLen, nTimeout)

            If (bIsRetStatusOK) Then

            End If
            'Rx Data
            If (nRxLen > 0) Then
                ClassTableListMaster.ConvertFromArrayByte2String(o_arrayByteRecv, 0, nRxLen, o_strHexRx, False)
                'ClassTableListMaster.ConvertFromHexToAscii(o_arrayByteRecv, 0, nRxLen, o_strHexRx, False)
            Else
                'No Data available
                o_strHexRx = ""
            End If

        Catch ex As Exception
            LogLn("Err : " + ex.Message)
        Finally
            If (arrayByteSendData IsNot Nothing) Then
                Erase arrayByteSendData
            End If
            If (o_arrayByteRecv IsNot Nothing) Then
                Erase o_arrayByteRecv
            End If
        End Try
    End Sub
    '
    Public Sub System_Customized_Command_DirectSend_HdrTail_StrASCII(strHexTx As String, strHexHdr As String, strHexTail As String, ByRef o_strASCIIRx As String, Optional ByVal nTimeout As Integer = ClassDevRS232.m_cnTX_TIMEOUT_IN_SEC)
        Dim arrayByteSendData As Byte() = Nothing
        Dim arrayByteHdr As Byte() = Nothing
        Dim arrayByteTail As Byte() = Nothing
        Dim o_arrayByteRecv As Byte() = New Byte(ClassDevRS232.m_cnRX_BUF_MAX - 1) {}
        Dim nRxLen As UInt32 = ClassDevRS232.m_cnRX_BUF_MAX
        Try
            'Init Value
            o_strASCIIRx = ""

            'Tx Hex Data 2 Binary Array
            If (Not strHexTx.Equals("")) Then
                ClassTableListMaster.TLVauleFromString2ByteArray(strHexTx, arrayByteSendData)
            End If
            If (Not strHexHdr.Equals("")) Then
                ClassTableListMaster.TLVauleFromString2ByteArray(strHexHdr, arrayByteHdr)
            End If
            If (Not strHexTail.Equals("")) Then
                ClassTableListMaster.TLVauleFromString2ByteArray(strHexTail, arrayByteTail)
            End If

            'Send it
            System_Customized_Command_DirectSend_RxChk_HdrTail_ArrayByte(arrayByteSendData, arrayByteHdr, arrayByteTail, o_arrayByteRecv, nRxLen, nTimeout)

            If (bIsRetStatusOK) Then

            End If
            'Rx Data
            If (nRxLen > 0) Then
                'ClassTableListMaster.ConvertFromArrayByte2String(o_arrayByteRecv, 0, nRxLen, o_strASCIIRx, False)
                ClassTableListMaster.ConvertFromHexToAscii(o_arrayByteRecv, 0, nRxLen, o_strASCIIRx, False)
            Else
                'No Data available
                o_strASCIIRx = ""
            End If

        Catch ex As Exception
            LogLn("Err : " + ex.Message)
        Finally
            If (arrayByteSendData IsNot Nothing) Then
                Erase arrayByteSendData
            End If
            If (o_arrayByteRecv IsNot Nothing) Then
                Erase o_arrayByteRecv
            End If
        End Try
    End Sub
    '
    Public Sub System_Customized_Command_DirectSend_StrHex(strHexTx As String, ByRef o_strHexRx As String, Optional ByVal nTimeout As Integer = ClassDevRS232.m_cnTX_TIMEOUT_IN_SEC)
        Dim arrayByteSendData As Byte() = Nothing
        Dim o_arrayByteRecv As Byte() = New Byte(ClassDevRS232.m_cnRX_BUF_MAX - 1) {}
        Dim nRxLen As UInt32 = ClassDevRS232.m_cnRX_BUF_MAX
        Try
            'Init Value
            o_strHexRx = ""

            'Tx Hex Data 2 Binary Array
            If (Not strHexTx.Equals("")) Then
                ClassTableListMaster.TLVauleFromString2ByteArray(strHexTx, arrayByteSendData)
            End If

            'Send it
            System_Customized_Command_DirectSend(arrayByteSendData, o_arrayByteRecv, nRxLen, nTimeout)

            'Rx Data
            If (nRxLen > 0) Then
                ClassTableListMaster.ConvertFromArrayByte2String(o_arrayByteRecv, o_strHexRx, False)
            End If

        Catch ex As Exception
            LogLn("Err : " + ex.Message)
        Finally
            If (arrayByteSendData IsNot Nothing) Then
                Erase arrayByteSendData
            End If
            If (o_arrayByteRecv IsNot Nothing) Then
                Erase o_arrayByteRecv
            End If
        End Try
    End Sub
    '
    Public Overloads Sub System_Customized_Command_DirectSend(ByVal arrayByteSendData As Byte(), ByRef o_arrayByteRecv As Byte(), ByRef o_u32RxLen As UInt32, Optional ByVal nTimeout As Integer = ClassDevRS232.m_cnTX_TIMEOUT_IN_SEC)
        Dim bWait As Boolean = True 'Always to Wait
        Try
            m_bIsRxOK = False

            '---[ Debug - Printout Tx Data ]---
            If (m_bDebug) Then
                Form01_Main.LogLnDumpData(arrayByteSendData, arrayByteSendData.Length)
            End If

            'If (o_arrayByteRecv IsNot Nothing) Then
            '    u32RxLen = o_arrayByteRecv.Length
            'End If
            m_refdevConn.SendCommandFrameAndWaitForResponseFrame_Pure(AddressOf ClassRdrCommNoPrtcl.PreProcessCbRecv, arrayByteSendData, o_arrayByteRecv, o_u32RxLen, bWait, nTimeout * 1000)

            'Its' response code = 0 => OK
            If (o_u32RxLen > 0) Then
                m_bIsRxOK = True
            End If

        Catch ext As TimeoutException
            LogLn("[Err][Timeout] DirectSend " + ext.Message)
        Catch ex As Exception
            LogLn("[Err]  DirectSend " + ex.Message)
            Throw 'ex
        Finally

        End Try
    End Sub
    '
    '
    Public Sub System_Customized_Command_DirectSend_RxChk_HdrTail_ArrayByte(ByVal arrayByteSendData As Byte(), ByVal arrayByteHdr As Byte(), ByVal arrayByteTail As Byte(), ByRef o_arrayByteRecv As Byte(), ByRef o_u32RxLen As UInt32, Optional ByVal nTimeout As Integer = ClassDevRS232.m_cnTX_TIMEOUT_IN_SEC)
        Dim bWait As Boolean = True 'Always to Wait
        'Get Timeout Stamp Point
        Dim dwTO As Integer = nTimeout * 1000
        Dim dwTO2 As Integer
        Dim dwTPoint As Integer
        Dim bLoop As Boolean = True
        Dim nStgLoop As Integer = 0
        Dim nRxLen As Integer
        Dim nRxLenMax As Integer = 100
        Dim nRxLenFinal As Integer = 0
        Dim arrayByteRxLocal As Byte() = New Byte(nRxLenMax - 1) {}
        Dim arrayByteRxFinal As Byte() = m_arraybyteSendBuf
        Dim nOffset As Integer = 0
        'Dim nOffset2 As Integer 'This is used for prefix offset during Tail Check Stage 2/3

        Dim o_nRxLen As Integer = CType(o_u32RxLen, Integer)

        Dim nOffsetHdrChk As Integer = -1
        Dim nOffsetTailChk As Integer = -1
        Dim nRxRetry As Integer = 0
        Dim nRxRetryMax As Integer = 3
        Dim nPassCnt As Integer = 0
        Dim nPassCntMax As Integer = 0
        '
        Try
            m_bIsRetOK = False
            m_bIsRxOK = False
            nRxLen = arrayByteRxFinal.Length
            If (nRxLen > 1023) Then
                nRxLen = 1024
            End If

            'Make sure all Rx Buffer is clear
            Array.Clear(arrayByteRxFinal, 0, nRxLen)


            '---[ Debug - Printout Tx Data ]---
            If (m_bDebug) Then
                Form01_Main.LogLnDumpData(arrayByteSendData, arrayByteSendData.Length)
            End If

            'If (o_arrayByteRecv IsNot Nothing) Then
            '    u32RxLen = o_arrayByteRecv.Length
            'End If
            dwTPoint = Environment.TickCount + dwTO
            'Init Rx Time out value
            dwTO2 = dwTO

            '01 - Tx First
            nRxLen = 0


            '---[ Special Tx ONLY Application Note, from Goofy, 2017 Feb 06 ]---
            'Here I ONLY need Tx wo/ Rx --> Give Non-null Rx Buffer + nRxLen=0 --> Just performs Tx ONLY in RS232 Interface
            'Note 01 - Since we don't use Serial Rx Callback Handler, so we let AddressOf "Nothing" instead of "ClassRdrCommNoPrtcl.PreProcessCbRecv"
            m_refdevConn.SendCommandFrameAndWaitForResponseFrame_Pure2(Nothing, arrayByteSendData, arrayByteRxLocal, nRxLen, bWait, dwTO)

            '02 - Rx Loop until Timeout or Header(*) + Tail(*) Found
            'Decide Start Stage Check : 0 = Init / Skip; 1 = Header Check; 2 = Tail Check; 3 = No Tail Check; 4 = collect final result; 4 = Done & return immediately
            If (arrayByteHdr Is Nothing) Then
                nOffsetHdrChk = 0 'Set to start position of Header
                nRxRetry = 0 ' Reset Retry Counter before falling to Stage 2 / 3
                'nOffset2 = 0
                If (arrayByteTail Is Nothing) Then
                    nStgLoop = 3
                    dwTO2 = Environment.TickCount + 5000
                Else
                    nStgLoop = 2
                End If
            Else
                nStgLoop = 1
            End If
            nPassCntMax = nPassCntMax + 1 '*Add one more check

            '03 - Check stage 
            'System.Linq .Enumerable .Concat 
            While (bLoop)
                'Step 01 : Stage Procedure
                Select Case (nStgLoop)
                    Case 0
                        'Unknown status
                        m_refdevConn.bIsStatusTimeout = True
                        dwTPoint = Environment.TickCount
                        '---------------------
                    Case 1
                        nRxLen = arrayByteRxLocal.Length
                        If (o_u32RxLen < (nOffset + nRxLen)) Then
                            'Buffer Size Exceeded --> Reduce it
                            nRxLen = o_u32RxLen - nOffset
                            If (nRxLen = 0) Then
                                'Buffer Full, stop Rx, No Header Found
                                nStgLoop = 4
                                Continue While
                            End If
                        End If
                        'Since we don't use SerialPort Rx Callback Handler,then we let AddressOf "Nothing" instead of AddressOf "ClassRdrCommNoPrtcl.PreProcessCbRecv"
                        m_refdevConn.SendCommandFrameAndWaitForResponseFrame_Pure2(Nothing, Nothing, arrayByteRxLocal, nRxLen, bWait, dwTO2)

                        'Check Header If needed
                        If (nRxLen > 0) Then
                            'Combine to Full Data First
                            Array.Copy(arrayByteRxLocal, 0, arrayByteRxFinal, nOffset, nRxLen)
                            nOffset = nOffset + nRxLen
                            nOffsetHdrChk = ClassTableListMaster.ArrayByteSubPatternIndexOf(arrayByteRxFinal, 0, arrayByteHdr, 0)
                            If (nOffsetHdrChk >= 0) Then
                                'Skip Header part.
                                nRxRetry = 0 ' Reset Retry Counter before falling to Stage 2 / 3
                                'Skip Header part.
                                'Make Final Header Offset = Header[0]+Header's Len
                                nOffsetHdrChk = nOffsetHdrChk + arrayByteHdr.Length

                                nPassCnt = nPassCnt + 1 '#Pass this stage

                                nPassCntMax = nPassCntMax + 1 ' *Add one more check for Tail Pattern
                                If (arrayByteTail Is Nothing) Then

                                    nStgLoop = 3
                                    dwTO2 = Environment.TickCount + 5000

                                Else
                                    nStgLoop = 2
                                    'Try to find tail pattern here to skip Stage 2
                                    nOffsetTailChk = ClassTableListMaster.ArrayByteSubPatternIndexOf(arrayByteRxFinal, nOffsetHdrChk, arrayByteTail, 0)
                                    If (nOffsetTailChk >= 0) Then
                                        nPassCnt = nPassCnt + 1 '#Pass this stage
                                        nStgLoop = 4
                                        Continue While
                                    End If
                                End If

                                Continue While
                            End If

                        End If
                        '---------------------

                    Case 2
                        'Check Tail If needed
                        nRxLen = arrayByteRxLocal.Length
                        If (o_u32RxLen < (nOffset + nRxLen)) Then
                            'Buffer Size Exceeded --> Reduce it
                            nRxLen = o_u32RxLen - nOffset
                            If (nRxLen = 0) Then
                                'Buffer Full, stop Rx, No Tail Found
                                'still not found, then we give
                                nOffsetTailChk = nOffset
                                nStgLoop = 4
                                Continue While
                            End If
                        End If
                        'Since we don't use SerialPort Rx Callback Handler,then we let AddressOf "Nothing" instead of AddressOf "ClassRdrCommNoPrtcl.PreProcessCbRecv"
                        m_refdevConn.SendCommandFrameAndWaitForResponseFrame_Pure2(Nothing, Nothing, arrayByteRxLocal, nRxLen, bWait, dwTO2)

                        If (nRxLen > 0) Then
                            'Combine to Full Data First
                            Array.Copy(arrayByteRxLocal, 0, arrayByteRxFinal, nOffset, nRxLen)
                            nOffset = nOffset + nRxLen
                        End If

                        'Try to find tail pattern
                        nOffsetTailChk = ClassTableListMaster.ArrayByteSubPatternIndexOf(arrayByteRxFinal, nOffsetHdrChk, arrayByteTail, 0)
                        If (nOffsetTailChk >= 0) Then
                            nPassCnt = nPassCnt + 1 '#Pass this stage
                            nStgLoop = 4
                            Continue While
                        End If
                        '---------------------

                    Case 3
                        'Rx Time out value is up to 500 ms
                        If (dwTO2 > Environment.TickCount) Then
                            'No Tail Check, Rxing until timeout or retrying times >= 3.
                            nRxLen = arrayByteRxLocal.Length
                            If (o_u32RxLen < (nOffset + nRxLen)) Then
                                'Buffer Size Exceeded --> Reduce it
                                nRxLen = o_u32RxLen - nOffset
                                If (nRxLen = 0) Then
                                    'Buffer Full, stop Rx, Move to stage 04
                                    nPassCnt = nPassCnt + 1 '#Pass this stage
                                    nStgLoop = 4
                                    Continue While
                                End If
                            End If
                            'Since we don't use SerialPort Rx Callback Handler,then we let AddressOf "Nothing" instead of AddressOf "ClassRdrCommNoPrtcl.PreProcessCbRecv"
                            m_refdevConn.SendCommandFrameAndWaitForResponseFrame_Pure2(Nothing, Nothing, arrayByteRxLocal, nRxLen, bWait, 100) 'using 100 ms instead of dwTO2 - Environment.TickCount)
                            If (nRxLen > 0) Then
                                nRxRetry = 0 ' Reset it
                                'Combine to Full Data First
                                Array.Copy(arrayByteRxLocal, 0, arrayByteRxFinal, nOffset, nRxLen)
                                nOffset = nOffset + nRxLen
                                'Set Tail Offset Check Point = Last Position+1
                                nOffsetTailChk = nOffset
                            Else
                                'No More Rx Data, trying ...
                                nRxRetry = nRxRetry + 1
                                If (nRxRetry >= nRxRetryMax) Then
                                    'bLoop = False
                                    nStgLoop = 4
                                    nPassCnt = nPassCnt + 1 '#Pass this stage
                                End If
                            End If
                        Else
                            '500 ms Timeout --> End & Move to data stage.
                            nStgLoop = 4
                        End If
                        '---------------------
                    Case 4
                        'Collect
                        If (nOffset > 0) Then
                            'Rx Length 
                            Dim nLenChk As Integer = 0
                            If (nOffsetHdrChk <= -1) Then
                                nLenChk += 1
                            End If
                            If (nOffsetTailChk <= -1) Then
                                nLenChk += 2
                            End If
                            '
                            Select Case (nLenChk)
                                Case 1 'No Hdr , Yes Tail
                                    nRxLenFinal = nOffsetTailChk
                                    nOffsetHdrChk = 0
                                Case 2 'Yes Hdr, No Tail
                                    nRxLenFinal = nOffset - nOffsetHdrChk
                                Case 3 'No Hdr, No Tail
                                    nRxLenFinal = nOffset
                                    nOffsetHdrChk = 0
                                Case Else '= Case 0
                                    nRxLenFinal = nOffsetTailChk - nOffsetHdrChk
                            End Select
                            '--------------------------------------
                            'Boundary Check, Can't Exceed Output Buffer Size
                            If (o_u32RxLen < nRxLenFinal) Then
                                nRxLenFinal = o_u32RxLen
                            End If

                            'Overlay Check of Header & Tail Pattern
                            If (nRxLenFinal < 0) Then
                                nRxLenFinal = nOffset
                                Array.Copy(arrayByteRxFinal, 0, o_arrayByteRecv, 0, nRxLenFinal)
                                o_u32RxLen = nRxLenFinal
                                'Overlay data between Header & Tail String Pattern !!
                                Throw New Exception("Header & Tail Overlay!!")
                            End If

                            If (nRxLenFinal > 0) Then
                                'Copy Data to Output Buffer & Length Value
                                Array.Copy(arrayByteRxFinal, nOffsetHdrChk, o_arrayByteRecv, 0, nRxLenFinal)
                            Else
                                'Possible No Data between Header& Tail String Pattern
                            End If

                            o_u32RxLen = nRxLenFinal
                        Else
                            o_u32RxLen = 0
                        End If
                        ' End of Step 04
                        bLoop = False
                End Select

                'Step 04 : Timeout Check
                If (m_refdevConn.bIsStatusTimeout) Then
                    If (nStgLoop <> 3) Then
                        'Time out case is excluded from No Tail Check Status
                        bLoop = False
                    End If

                    ' Still Try to strip off RX data if data IN Found when timeout
                    If (nOffset > 0) Then
                        Array.Copy(arrayByteRxFinal, 0, o_arrayByteRecv, 0, nOffset)
                        o_u32RxLen = nOffset
                    Else
                        o_u32RxLen = 0
                    End If
                Else
                    'Calculate rest timeout
                    dwTO2 = dwTPoint - Environment.TickCount
                    If (dwTO2 < 0) Then
                        'If time exceeded , No more check
                        dwTO2 = 0
                        m_refdevConn.bIsStatusTimeout = True
                        bLoop = False
                        '
                        ' Still Try to strip off RX data if data IN Found when timeout
                        If (nOffset > 0) Then
                            Array.Copy(arrayByteRxFinal, 0, o_arrayByteRecv, 0, nOffset)
                            o_u32RxLen = nOffset
                        Else
                            o_u32RxLen = 0
                        End If
                    End If
                End If

            End While

            'Its' response code = 0 => OK
            If (o_u32RxLen > 0) Then
                m_bIsRxOK = True
            End If

        Catch ext As TimeoutException
            LogLn("[Err][Timeout] DirectSend " + ext.Message)
        Catch ex As Exception
            LogLn("[Err]  DirectSend " + ex.Message)
            Throw 'ex
        Finally
            If (arrayByteRxLocal IsNot Nothing) Then
                Erase arrayByteRxLocal
            End If
            If (nPassCnt > 0) Then
                m_bIsRetOK = (nPassCnt = nPassCntMax)
            End If
        End Try
    End Sub
    '
    ReadOnly Property bIsTimeout As Boolean
        Get
            If (m_refdevConn Is Nothing) Then
                Return False
            End If
            '
            Return m_refdevConn.bIsStatusTimeout
        End Get
    End Property
    '

    'Note : Check Return Value & bIsRetStatusOK and the like....
    Public Sub Enzytek_ATCmd_ATChk(ByRef o_strHexRx As String, Optional ByVal nTOInSec As Integer = 1)
        Dim strHexTx As String = ClassTableListMaster.ConvertFromAscii2HexString("AT" + vbCr)
        Dim strHexRxHdr As String = ""
        Dim strHexRxTail As String = ClassTableListMaster.ConvertFromAscii2HexString(vbCr + "OK" + vbCr)

        System_Customized_Command_DirectSend_HdrTail_StrHex(strHexTx, strHexRxHdr, strHexRxTail, o_strHexRx, nTOInSec)
    End Sub

    '2017 Feb 23, goofy_liu@idtechproducts.com.tw, Project --[ BTPay Mini ]--
    'Note 01 - If bMainWait = True , pRefMF.WaitMsgShow(True, TM017_BLE_02_ATCMD_SPSG_DISCONNECT, False, 0) must be Performed before invoking this function. and will call pRefMF.WaitMsgHide() to end waiting dialog box
    Public Function Enzytek_ATCmd_DisconntPIO11(ByRef o_strHexRx As String, Optional ByVal bMainWait As Boolean = False) As Boolean
        Dim bFound As Boolean = False
        '
        Dim pRefMF As Form01_Main = Form01_Main.GetInstance()
        'Manually Disable
        Dim strHexRx As String = ""
        Dim strHexTx As String = ""
        Dim strHexRxHdr As String = ""
        Dim strHexRxTail As String = ""
        Dim strHexRx2 As String = ""
        Dim strHexRxBLEDownChk01 As String = "BLE DOWN"
        Dim strHexRxBLEDownChk02 As String = "SCAN_START"
        Dim strHexRxBLEDownChk03 As String = "Enzytek SPS_C"
        '
        'pRefMF.WaitMsgShow(True, ClassServiceLanguages._WTD_STRID.TM017_BLE_02_ATCMD_SPSG_DISCONNECT, False, 0)
        '
        bFound = False
        While (Not bFound)
            strHexRx = ""
            System_Customized_Command_DirectSend_HdrTail_StrHex(strHexTx, strHexRxHdr, strHexRxTail, strHexRx, 1)

            If (bMainWait) Then
                'Perform Waiting Dialog Check, this must be care
                If (pRefMF.bIsWaitMsgCanceled Or pRefMF.bIsWaitMsgEnd) Then
                    Exit While
                End If
            End If


            strHexRx2 += strHexRx

            If (strHexRx2.Length > 0) Then
                'a - Exit if Rx w/ "BLE DOWN"
                If (strHexRx2.IndexOf(strHexRxBLEDownChk01) >= 0) Then
                    bFound = True
                End If

                'b - Exit if Rx w/ "SCAN_START" (Being caused from shorten Enzytek SPS's PIO11 pin will feedback this string)
                If (strHexRx2.IndexOf(strHexRxBLEDownChk02) >= 0) Then
                    bFound = True
                End If

                'c - Exit if Rx w/ "Enzytek SPS_C", being caused by re-power on Enzytek SPS's will feedback this string)
                If (strHexRx2.IndexOf(strHexRxBLEDownChk03) >= 0) Then
                    bFound = True
                End If
            End If
            '
        End While 'Loop Lv 01

        'Save Return Value
        o_strHexRx = strHexRx2 'Move Rx Data to correct place
        '
        pRefMF.WaitMsgHide()

        Return bFound
    End Function

    '----------------------------------------------------------
    'Disconnect Command in Data Passthrough mode "^^^<CR>"
    '11:30:06 [TX] - ^^^<CR>
    '11:30:06 [RX] - BLE DOWN <CR><LF>
    Public Sub Enzytek_ATCmd_Disconnect_DataMode(ByRef o_strHexRx As String, Optional ByVal bSkipTailChk As Boolean = True, Optional ByVal nTimeoutSec As Integer = 1)
        Dim strHexTx As String = ClassTableListMaster.ConvertFromAscii2HexString("^^^" + vbCr)
        Dim strHexRxHdr As String = ""
        Dim strHexRxTail As String = ClassTableListMaster.ConvertFromAscii2HexString("BLE DOWN " + vbCr) + "0A"
        If (bSkipTailChk) Then
            strHexRxTail = ""
        End If

        System_Customized_Command_DirectSend_HdrTail_StrHex(strHexTx, "", strHexRxTail, o_strHexRx, nTimeoutSec)

    End Sub

    'AT Ex03 - Tx=AT+BLD001C97154B0A<CR>; Rx= <CR>OK<CR>BLE UP 
    Function Enzytek_ATCmd_Connecting(strBLEAddr As String, ByRef o_strHexRx As String, Optional ByVal bMainWaitChk As Boolean = False) As Boolean
        Dim bFound As Boolean = False
        Dim nChkCnt As Integer
        Dim pRefMF As Form01_Main = Form01_Main.GetInstance()

        Dim strHexTx As String = ClassTableListMaster.ConvertFromAscii2HexString("AT+BLD" + strBLEAddr + vbCr)
        Dim strHexRxHdr As String = ClassTableListMaster.ConvertFromAscii2HexString(vbCr + "OK" + vbCr)
        Dim strHexRx As String = ""
        Dim strHexRx2 As String = ""
        'This may happen if BLE Not ready
        Dim strHexRxBLEDownChk01 As String = ClassTableListMaster.ConvertFromAscii2HexString("BLE UP " + strBLEAddr) + "0D0A" '<CR><LF>="0D0A" in hex string
        Dim strHexRxBLEDownChk02 As String = ClassTableListMaster.ConvertFromAscii2HexString("Cancel" + vbCr)
        bFound = False
        While (Not bFound)
            strHexRx = ""
            System_Customized_Command_DirectSend_HdrTail_StrHex(strHexTx, strHexRxHdr, "", strHexRx, 1)
            strHexTx = ""
            '
            If (bMainWaitChk) Then
                If (pRefMF.bIsWaitMsgCanceled Or pRefMF.bIsWaitMsgEnd) Then
                    Exit While
                End If
            End If


            strHexRx2 += strHexRx
            nChkCnt = 0
            If (strHexRx2.Length > 0) Then
                'b - Exit if Rx w/ "<CR>OK<CR>BLE UP 001C97154B0A<CR><LF>"
                If (strHexRx2.IndexOf(strHexRxBLEDownChk01) >= 0) Then
                    nChkCnt = 1
                End If

                'c - Exit if Rx w/ "<CR>OK<CR>Cancel<CR>", being caused by No BLE Dev Found.
                If (strHexRx2.IndexOf(strHexRxBLEDownChk02) >= 0) Then
                    nChkCnt = 2 'Not Found, try again
                    LogLn("[Note] Not Found, Press [Wake up] Button and Trying again...")
                End If
            End If
            If (nChkCnt = 1) Then
                bFound = True
            Else
                Thread.Sleep(300)
            End If
            '
        End While 'Loop Lv 01

        Return bFound
    End Function
    '
    '
    Public Function Enzytek_ATCmd_BLE_DUT_Scanning_Or_Get_1st_Found_Addr(strBLEAddr As String, strHexRx As String, Optional ByVal bIsMainWaint As Boolean = False) As Boolean
        Dim bFound As Boolean = False
        Dim pRefMF As Form01_Main = Form01_Main.GetInstance()
        '2017 Feb 23, goofy_liu@idtechproducts.com.tw
        '-------[Special Note]-------
        'nMode = 0 : Found Mode, where input strBLEAddr.Length = 12, if found then return True, otherwise return False.
        'nMode = 1 : 1st Get Mode,where input strBLEAddr.Length = 0, if 1st Blue tooth Device found then return True, otherwise return False.
        Dim nMode As Integer = 0
        '-------[End of Special Note]-------------

        Dim nChkCnt As Integer
        Dim strHexRx2 As String = ""
        Dim strHexTx As String = ClassTableListMaster.ConvertFromAscii2HexString("AT+BLQ" + vbCr)
        Dim strHexRxHdr As String = "SCAN_START" ' + vbCr)
        Dim strHexRxTail As String = strBLEAddr + ",Enzytek SPS_S" '+ vbCr)

        If (strBLEAddr.Length = 0) Then
            nMode = 1
        End If

        While (Not bFound)
            strHexRx = ""
            System_Customized_Command_DirectSend_HdrTail_StrASCII(strHexTx, "", "", strHexRx, 1)
            strHexTx = ""

            If (bIsMainWaint) Then
                If (pRefMF.bIsWaitMsgCanceled Or pRefMF.bIsWaitMsgEnd) Then
                    Exit While
                End If
            End If
            '
            strHexRx2 += strHexRx
            nChkCnt = 0
            If (strHexRx2.Length > 0) Then
                'a - Exit if Rx w/ "SCAN_START" (Being caused from shorten Enzytek SPS's PIO11 pin will feedback this string)
                If (strHexRx2.IndexOf(strHexRxHdr) >= 0) Then
                    nChkCnt += 1
                End If
                '
                'b - Exit if Rx w/ BLE Address+",Enzytek SPS_C", being caused by re-power on Enzytek SPS's will feedback this string)
                If (strHexRx2.IndexOf(strHexRxTail) >= 0) Then
                    nChkCnt += 1
                End If


            End If
            If (nChkCnt = 2) Then
                If (nMode = 1) Then
                    'nMode = 1, Get 1st Blue tooth Address Mode
                    If (strHexRx.Length >= (12 + strHexRxTail.Length)) Then
                        strBLEAddr = strHexRx.Substring(strHexRx.Length - strHexRxTail.Length, 12)
                    End If
                End If
                bFound = True
            End If
            '
        End While 'Loop Lv 01
        '
        Return bFound
    End Function

    Public Sub Enzytek_ATCmd_Disconnt_CmdMode(ByRef o_strHexRx As String, Optional ByVal bSkipTailChk As Boolean = True, Optional ByVal nTimeoutSec As Integer = 1)

        Dim strHexTx As String = ClassTableListMaster.ConvertFromAscii2HexString("ATH" + vbCr)
        Dim strHexRxTail As String = ClassTableListMaster.ConvertFromAscii2HexString(vbCr + "OK" + vbCr)
        If (bSkipTailChk) Then
            strHexRxTail = ""
        End If
        '
        System_Customized_Command_DirectSend_HdrTail_StrHex(strHexTx, "", strHexRxTail, o_strHexRx, nTimeoutSec)
    End Sub

    '2017 Mar 03, goofy_liu@idtechproducts.com.tw
    'Discard In Data first since lots of scaning data in if possible.
    '[Note] We dont wrap Discard Buff function into Tx/Rx Commands to allow more flexible control from upper/caller
    Public Sub DiscardRxBuff()
        m_devRS232.RxBuffDiscard()
    End Sub


End Class
'

