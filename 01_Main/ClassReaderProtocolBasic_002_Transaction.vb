﻿'==================================================================================
' File Name: ClassReaderProtocolBasic.vb
' Description: The ClassReaderProtocolBasic is as Must-be Inherited Base Class of Reader's Packet Parser & Composer.
' Who use it as Base Class: ClassReaderProtocolBasicIDGAR, ClassReaderProtocolBasicIDGAR, and ClassReaderProtocolBasicIDTECH
' For Activate Transaction Functions
' 
' Revision:
' -----------------------[ Initial Version ]-----------------------
' 2017 Oct 06, by goofy_liu@idtechproducts.com.tw

'==================================================================================
'
'----------------------[ Importing Stuffs ]----------------------
Imports System.Runtime.Serialization
Imports System.Runtime.Serialization.Formatters.Binary
'Imports DiagTool_HardwareExclusive.ClassReaderProtocolBasicIDGAR
Imports System.IO
Imports System.IO.Ports
Imports System.Threading

'-------------------[ Class Declaration ]-------------------

Partial Public Class ClassReaderProtocolBasic
    '
    '
    Public Overridable Sub TransactionActivate(ByVal dictTxnSendTLVs As Dictionary(Of String, tagTLV), ByRef o_tagInfoTxnResponse As ClassTransaction.TAG_TXN_PARSE_RESPONSE_INFO, Optional ByVal nTimeoutSec As Integer = ClassDevRS232.m_cnTX_TIMEOUT_IN_SEC)
        Try

        Catch ex As Exception
            LogLn("[Err] TransactionActivate() = " + ex.Message)
        Finally

        End Try
    End Sub
    Public Overridable Sub TransactionActivate(ByVal dictTxnSendTLVs As SortedDictionary(Of String, tagTLV), ByRef o_tagInfoTxnResponse As ClassTransaction.TAG_TXN_PARSE_RESPONSE_INFO, Optional ByVal nTimeoutSec As Integer = ClassDevRS232.m_cnTX_TIMEOUT_IN_SEC)
        Try

            Dim tmpDictTxnSendTLVs As Dictionary(Of String, tagTLV) = New Dictionary(Of String, tagTLV)
            If (dictTxnSendTLVs IsNot Nothing) Then
                If (dictTxnSendTLVs.Count > 0) Then
                    For Each iteratorTLV In dictTxnSendTLVs
                        tmpDictTxnSendTLVs.Add(iteratorTLV.Key, iteratorTLV.Value)
                    Next
                End If
            End If
            '
            TransactionActivate(tmpDictTxnSendTLVs, o_tagInfoTxnResponse, nTimeoutSec)
            '
            tmpDictTxnSendTLVs.Clear()
            '
        Catch ex As Exception
            LogLn("[Err] TransactionActivate() = " + ex.Message)
        Finally

        End Try
    End Sub

    '----------------------------[IDG Cmd: 02-20 ]----------------------------
    Public Overridable Sub TransactionActivateEnhancement(ByVal bytePollCardType As ClassReaderProtocolBasic.ENHANCE_CARD_TYPE, ByVal byteUIFlag As Byte, ByVal byteMode As Byte, ByVal dictTxnSendTLVs As Dictionary(Of String, tagTLV), ByRef o_tagInfoTxnResponse As ClassTransaction.TAG_TXN_PARSE_RESPONSE_INFO, Optional ByVal nTimeoutSec As Integer = ClassDevRS232.m_cnTX_TIMEOUT_IN_SEC)
        Try

        Catch ex As Exception
            LogLn("[Err] TransactionActivateEnhancement() = " + ex.Message)
        Finally

        End Try
    End Sub
    '
    Public Overridable Sub TransactionActivateEnhancement_SecureMode(ByVal bytePollCardType As ClassReaderProtocolBasic.ENHANCE_CARD_TYPE, ByVal byteUIFlag As Byte, ByVal byteMode As Byte, ByVal dictTxnSendTLVs As Dictionary(Of String, tagTLV), ByRef o_tagInfoTxnResponse As ClassTransaction.TAG_TXN_PARSE_RESPONSE_INFO, Optional ByVal nTimeoutSec As Integer = ClassDevRS232.m_cnTX_TIMEOUT_IN_SEC)
        Try

        Catch ex As Exception
            LogLn("[Err] TransactionActivateEnhancement() = " + ex.Message)
        Finally

        End Try
    End Sub
    '
    '
    Public Sub TransactionActivateEnhancement(ByVal bytePollCardType As ClassReaderProtocolBasic.ENHANCE_CARD_TYPE, ByVal byteUIFlag As Byte, ByVal byteMode As Byte, ByVal dictTxnSendTLVs As SortedDictionary(Of String, tagTLV), ByRef o_tagInfoTxnResponse As ClassTransaction.TAG_TXN_PARSE_RESPONSE_INFO, Optional ByVal nTimeoutSec As Integer = ClassDevRS232.m_cnTX_TIMEOUT_IN_SEC)
        Try

            Dim tmpDictTxnSendTLVs As Dictionary(Of String, tagTLV) = New Dictionary(Of String, tagTLV)
            For Each iteratorTLV In dictTxnSendTLVs
                tmpDictTxnSendTLVs.Add(iteratorTLV.Key, iteratorTLV.Value)
            Next
            '
            TransactionActivateEnhancement(bytePollCardType, byteUIFlag, byteMode, tmpDictTxnSendTLVs, o_tagInfoTxnResponse, nTimeoutSec)
            '
            tmpDictTxnSendTLVs.Clear()
            '
        Catch ex As Exception
            LogLn("[Err] TransactionActivateEnhancement() = " + ex.Message)
        Finally

        End Try
    End Sub
    '
    '2017 Mar 20, In PISCES (PCI Reader),
    Public Sub TransactionActivateEnhancement_SecureMode(ByVal bytePollCardType As ClassReaderProtocolBasic.ENHANCE_CARD_TYPE, ByVal byteUIFlag As Byte, ByVal byteMode As Byte, ByVal dictTxnSendTLVs As SortedDictionary(Of String, tagTLV), ByRef o_tagInfoTxnResponse As ClassTransaction.TAG_TXN_PARSE_RESPONSE_INFO, Optional ByVal nTimeoutSec As Integer = ClassDevRS232.m_cnTX_TIMEOUT_IN_SEC)
        Try

            Dim tmpDictTxnSendTLVs As Dictionary(Of String, tagTLV) = New Dictionary(Of String, tagTLV)
            For Each iteratorTLV In dictTxnSendTLVs
                tmpDictTxnSendTLVs.Add(iteratorTLV.Key, iteratorTLV.Value)
            Next

            If (nTimeoutSec < 0) Then
                nTimeoutSec = ClassDevRS232.m_cnTX_TIMEOUT_IN_SEC
            End If
            '
            TransactionActivateEnhancement_SecureMode(bytePollCardType, byteUIFlag, byteMode, tmpDictTxnSendTLVs, o_tagInfoTxnResponse, nTimeoutSec)
            '
            tmpDictTxnSendTLVs.Clear()
            '
        Catch ex As Exception
            LogLn("[Err] TransactionActivateEnhancement() = " + ex.Message)
        Finally

        End Try
    End Sub

    Public Overridable Sub TransactionGetResult()
        Try

        Catch ex As Exception
            LogLn("[Err] () = " + ex.Message)
        Finally

        End Try
    End Sub

    Public Overridable Sub TransactionCancel(Optional ByVal nTimeoutSec As Integer = ClassDevRS232.m_cnTX_TIMEOUT_IN_SEC)
        Try

        Catch ex As Exception
            LogLn("[Err] () = " + ex.Message)
        Finally

        End Try
    End Sub

    Public Overridable Sub TransactionStop()
        Try

        Catch ex As Exception
            LogLn("[Err] () = " + ex.Message)
        Finally

        End Try
    End Sub

    Public Overridable Sub TransactionMChip30TornTransactionLogClean(Optional ByVal nTimeout As Integer = ClassDevRS232.m_cnTX_TIMEOUT_IN_SEC)
        Try

        Catch ex As Exception
            LogLn("[Err] () = " + ex.Message)
        Finally

        End Try
    End Sub

    Public Overridable Sub TransactionMChip30TornTransactionLogReset(Optional ByVal nTimeout As Integer = ClassDevRS232.m_cnTX_TIMEOUT_IN_SEC)
        Try

        Catch ex As Exception
            LogLn("[Err] () = " + ex.Message)
        Finally

        End Try
    End Sub

    Public Overridable Sub TransactionUpdateBalance(ByVal byteApprovedDeclined As Byte, ByVal arrayByteAuthCode As Byte(), ByVal dictTLVs As Dictionary(Of String, tagTLV), ByRef o_tagInfoTxnResponse As ClassTransaction.TAG_TXN_PARSE_RESPONSE_INFO, Optional ByVal nTimeoutSec As Integer = ClassDevRS232.m_cnTX_TIMEOUT_IN_SEC)
        Try

        Catch ex As Exception
            LogLn("[Err] TransactionUpdateBalance() = " + ex.Message)
        Finally

        End Try
    End Sub
End Class
