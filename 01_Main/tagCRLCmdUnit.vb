﻿Imports System.Text


Public Structure tagCRLCmdUnit
    Public m_nCmd As ClassReaderCommanderParser.EnumCRLCmd
    Public m_strRIDIndex As String
    ' Key = Serial No in String Type
    Public m_dictCRLCmdList As Dictionary(Of String, tagCRLCmdUnitSingle)

    Sub New(ByVal nCmd As ClassReaderCommanderParser.EnumCRLCmd)
        m_nCmd = nCmd
        m_dictCRLCmdList = New Dictionary(Of String, tagCRLCmdUnitSingle)
        m_strRIDIndex = ""
    End Sub
    '
    Public Sub Cleanup()
        If m_dictCRLCmdList IsNot Nothing Then
            Dim iteratorCRLCmdSingle As KeyValuePair(Of String, tagCRLCmdUnitSingle)
            ' Cleanup CRL Cmd List
            For Each iteratorCRLCmdSingle In m_dictCRLCmdList
                iteratorCRLCmdSingle.Value.Cleanup()
            Next
            m_dictCRLCmdList.Clear()
        End If
        '
        m_strRIDIndex = ""
    End Sub
End Structure


'=========================[CRL Cmd]=========================
Public Structure tagCRLCmdUnitSingle
    Public m_strRID As String
    Public m_strIndex As String
    Public m_strSerialNo As String
    Public m_strCommentTrail As String
    '
    Sub New(ByVal strCRLCmd As String, ByVal strCommentTrail As String)
        Dim sb As StringBuilder = New StringBuilder
        Dim strCRLCmdTokens() As String = Split(strCRLCmd)
        '
        If strCRLCmdTokens.Count = 9 Then
            ' RID : 5 bytes
            For nIdx = 0 To 3
                sb.Append(strCRLCmdTokens(nIdx) + " ")
            Next
            sb.Append(strCRLCmdTokens(4))
            m_strRID = sb.ToString()

            'Index: 1 byte
            m_strIndex = strCRLCmdTokens(5)

            'SerialNo : 3 bytes
            'sb.Clear() '.Net Framework V4.5
            sb.Length = 0 '.Net Framework V3.5
            For nIdx = 6 To 7
                sb.Append(strCRLCmdTokens(nIdx) + " ")
            Next
            sb.Append(strCRLCmdTokens(8))
            m_strSerialNo = sb.ToString()
        Else
            m_strRID = ""
            m_strIndex = ""
            m_strSerialNo = ""
        End If
        '
        m_strCommentTrail = strCommentTrail
    End Sub
    '
    Public Property strIndex As String
        Set(strIndexIn As String)
            m_strIndex = strIndexIn
        End Set
        Get
            Return m_strIndex
        End Get
    End Property
    Public Property byteIndex As Byte
        Set(byteIndexIn As Byte)
            m_strIndex = String.Format("{0,2:X2}", byteIndexIn)
        End Set
        Get
            Dim byteIndexOut As Byte
            ClassTableListMaster.TLVauleFromString2Byte(m_strIndex, byteIndexOut)
            Return byteIndexOut
        End Get
    End Property
    '
    Public Property strSerialNo As String
        Set(strSerialNoIn As String)
            m_strSerialNo = strSerialNoIn
        End Set
        Get
            Return m_strSerialNo
        End Get
    End Property
    Public Property byteSerialNo As Byte()
        Set(byteSerialNoIn As Byte())
            ClassTableListMaster.ConvertFromArrayByte2String(byteSerialNoIn, m_strSerialNo)
        End Set
        Get
            Dim byteSerialNoOut As Byte() = Nothing
            ClassTableListMaster.TLVauleFromString2ByteArray(m_strSerialNo, byteSerialNoOut)
            Return byteSerialNoOut
        End Get
    End Property
    '
    Public Property strRID As String
        Set(strRIDIn As String)
            m_strRID = strRIDIn
        End Set
        Get
            Return m_strRID
        End Get
    End Property

    Public Property byteRID As Byte()
        Set(byteRIDIn As Byte())
            ClassTableListMaster.ConvertFromArrayByte2String(byteRIDIn, m_strRID)
        End Set
        Get
            Dim byteRIDOut As Byte() = Nothing
            ClassTableListMaster.TLVauleFromString2ByteArray(m_strRID, byteRIDOut)
            Return byteRIDOut
        End Get
    End Property

    Property strCommentTrail As String
        Set(strCommTrail As String)
            m_strCommentTrail = strCommTrail
        End Set
        Get
            Return m_strCommentTrail
        End Get
    End Property

    ReadOnly Property strRIDIndex As String
        Get
            Return m_strRID + " " + m_strIndex
        End Get
    End Property

    '
    Public Sub Cleanup()
        m_strRID = ""
        m_strIndex = ""
        m_strSerialNo = ""
        m_strCommentTrail = ""
    End Sub
End Structure

