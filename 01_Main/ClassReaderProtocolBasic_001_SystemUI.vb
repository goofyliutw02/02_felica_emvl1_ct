﻿'==================================================================================
' File Name: ClassReaderProtocolBasic.vb
' Description: The ClassReaderProtocolBasic is as Must-be Inherited Base Class of Reader's Packet Parser & Composer.
' Who use it as Base Class: ClassReaderProtocolBasicIDGAR, ClassReaderProtocolBasicIDGAR, and ClassReaderProtocolBasicIDTECH
' For System UI Control Functions

' Revision:
' -----------------------[ Initial Version ]-----------------------
' 2017 Oct 06, by goofy_liu@idtechproducts.com.tw

'==================================================================================
'
'----------------------[ Importing Stuffs ]----------------------
Imports System.Runtime.Serialization
Imports System.Runtime.Serialization.Formatters.Binary
'Imports DiagTool_HardwareExclusive.ClassReaderProtocolBasicIDGAR
Imports System.IO
Imports System.IO.Ports
Imports System.Threading

'-------------------[ Class Declaration ]-------------------

Partial Public Class ClassReaderProtocolBasic
    '
    '======================================[ IDG CMD:_0B-01, Buzzer short beep, data len = 1 (control byte) ]======================================
    Public Overridable Sub SystemUI_ButtonsConfigSet(arrayBtnConfig As Byte(), Optional ByVal nTimeout As Integer = ClassDevRS232.m_cnTX_TIMEOUT_IN_SEC)
        Dim strFuncName As String = "SystemUI_ButtonsConfigSet()"
        LogLn("Not Implement" + strFuncName)
    End Sub
    Public Overridable Sub SystemUI_ButtonsConfigGet(ByRef o_arrayBtnConfig As Byte(), Optional ByVal nTimeout As Integer = ClassDevRS232.m_cnTX_TIMEOUT_IN_SEC)
        Dim strFuncName As String = "SystemUI_ButtonsConfigGet()"
        LogLn("Not Implement" + strFuncName)
    End Sub

    ' AR and GR are used same Commands in Pass through mode
    Public Overridable Sub SystemUI_PT_BuzzerLongBeep(Optional ByVal byteLongDurationMS As ClassReaderProtocolBasic.ENUM_LB_TYPE = ENUM_LB_TYPE.MS200, Optional ByVal nTimeout As Integer = ClassDevRS232.m_cnTX_TIMEOUT_IN_SEC)
        '
        '        If Sub-Command is Short Beeps
        'Num Beeps = 01h One Short Beep 
        '= 02h Two Short Beeps 
        '= 03h Three Short Beeps 
        '= 04h Four Short Beeps
        'If Sub-Command is Long Beep
        'Duration = 00h 200 ms
        ' = 01h 400 ms 
        '= 02h 600 ms 
        '= 03h 800 ms
        '
        Dim nTOMax As Integer = Environment.TickCount()
        Select Case (byteLongDurationMS)
            Case ENUM_LB_TYPE.MS200
                nTOMax = nTOMax + 200
            Case ENUM_LB_TYPE.MS400
                nTOMax = nTOMax + 400
            Case ENUM_LB_TYPE.MS600
                nTOMax = nTOMax + 600
            Case ENUM_LB_TYPE.MS800
                nTOMax = nTOMax + 800
            Case Else
                nTOMax = nTOMax + 800
        End Select

        SystemUI_PT_BuzzerSetCommon(ENUM_BUZZER_TYPE.LONG_BEEP, byteLongDurationMS, nTimeout)

        If (bIsRetStatusOK) Then
            'If OKAY, non-blocking response here, we have to wait until sound playing
            'done , otherwise it is possible to cause buzzer driver re-entrying...
            While (nTOMax > Environment.TickCount())
                Thread.Sleep(100)
                Application.DoEvents()
            End While
        End If
    End Sub
    Public Overridable Sub SystemUI_PT_BuzzerShortBeep(Optional ByVal byteShortNumOfBeeps As ClassReaderProtocolBasic.ENUM_SB_TYPE = ENUM_SB_TYPE.NUM1, Optional ByVal nTimeout As Integer = ClassDevRS232.m_cnTX_TIMEOUT_IN_SEC)
        '
        '        If Sub-Command is Short Beeps
        'Num Beeps = 01h One Short Beep 
        '= 02h Two Short Beeps 
        '= 03h Three Short Beeps 
        '= 04h Four Short Beeps
        'If Sub-Command is Long Beep
        'Duration = 00h 200 ms
        ' = 01h 400 ms 
        '= 02h 600 ms 
        '= 03h 800 ms
        '
        '===========[ Special Notes ]===========
        ' 2016 Sept 23 Updated, 
        'goofy_liu@ idtechproducts.com.tw
        ' Golden Sample = UniPay III
        ' FW Info:
        'P/N =ID-80149003-001
        '===========================
        '[ProcessorType] = ARM Cortex-M4/ K21 Family
        '===========================
        '[Hardware Info] = HW,VPUnipay3
        '        K21F Rev9
        '===========================
        '[FWInfo] = VP4880 NEO v1.01.022C
        'UnipayIII-BL-V3.00.010
        '---------------------------------------------------------------
        ' TS146, V1.03.005-C04
        '---------------------------------------------------------------
        'nTOMax = nTOMax + D.T. (Delay Time, Depending on Num Of Beeps)
        'Please Don't Change D.T. until you know how to guarantee Enough Duration
        ' while non-blocking  sound playing not being interrupted.

        Dim nTOMax As Integer = Environment.TickCount()
        Select Case (byteShortNumOfBeeps)
            Case ENUM_SB_TYPE.NUM1
                nTOMax = nTOMax + 300
            Case ENUM_SB_TYPE.NUM2
                nTOMax = nTOMax + 500
            Case ENUM_SB_TYPE.NUM3
                nTOMax = nTOMax + 700
            Case ENUM_SB_TYPE.NUM4
                nTOMax = nTOMax + 1000
        End Select
        '
        SystemUI_PT_BuzzerSetCommon(ENUM_BUZZER_TYPE.SHORT_BEEP, byteShortNumOfBeeps, nTimeout)
        '
        If (bIsRetStatusOK) Then
            'If OKAY, non-blocking response here, we have to wait until sound playing
            'done , otherwise it is possible to cause buzzer driver re-entrying...
            While (nTOMax > Environment.TickCount())
                Thread.Sleep(100)
                Application.DoEvents()
            End While
        End If
    End Sub
    '
    '======================================[ IDG CMD:_F0-F6/F7, Blue LED Loops Enable/Disable]======================================
    Public Overridable Sub NEO20_Exclusive_Beta01_SystemUI_LEDBlueSet_Cycle(Optional bLEDAllON As Boolean = True, Optional ByVal nTimeout As Integer = ClassDevRS232.m_cnTX_TIMEOUT_IN_SEC)
        Dim u16Cmd As UInt16 = &HF0F7
        Dim strFuncName As String = "NEO20_Exclusive_Beta01_SystemUI_LEDBlueSet"
        Dim arrayByteSend As Byte() = Nothing
        Dim arrayByteRecv As Byte() = Nothing

        If (m_bIDGCmdBusy) Then
            Return
        End If
        m_bIDGCmdBusy = True

        If (Not bLEDAllON) Then
            u16Cmd = &HF0F6
        End If
        Try
            'ClassTableListMaster.ConvertValue2ArrayByte(byteModeOn, arrayByteSend)

            System_Customized_Command(u16Cmd, strFuncName, arrayByteSend, arrayByteRecv, AddressOf PreProcessCbRecv, nTimeout)

            If (bIsRetStatusOK) Then

            Else

            End If
        Catch ex As Exception

        Finally
            If (arrayByteSend IsNot Nothing) Then
                Erase arrayByteSend
            End If
            If (arrayByteRecv IsNot Nothing) Then
                Erase arrayByteRecv
            End If

            m_bIDGCmdBusy = False
        End Try
    End Sub
    '
    '
    Public Overridable Sub SystemUI_LEDBlueSet(Optional ByVal bEnable As Boolean = True, Optional ByVal bPatternSel As Boolean = False, Optional ByVal nTimeout As Integer = ClassDevRS232.m_cnTX_TIMEOUT_IN_SEC)
        'A.Blue LED Pattern Left (ON),Mid(OFF),Right(ON), MSR_Yellow(OFF) = (1,0,1,0), delay 1 sec = 100 millisec = 100 x 10
        'B.Blue LED Pattern Left (OFF),Mid(ON),Right(OFF), MSR_Yellow(OFF) = (0,1,0,0), delay 1 sec = 100 millisec
        'C.Blue LED Pattern Left (OFF),Mid(OFF),Right(OFF), MSR_Yellow(OFF) = (0,1,0,0), delay 1 sec = 100 millisec
        ' A + B + C total time = 3 seconds 
        Dim arrayBytePatternSend As Byte() = New Byte() { _
            &H0, &HA0, &H0, 10, _
             &H40, &H0, 10, _
            &H0, &H0, 10}

        '----------[ 2015 Nov 18 updated ]---------
        '300 milli-sec / once 
        'Dim arrayBytePatternSend As Byte() = New Byte() { _
        '    &H0, _
        '    &H0, &H0, 10, _
        '    &HE0, &H0, 10, _
        '    &H0, &H0, 10}
        Dim byteSubCmd As Byte
        Dim strFuncName As String = "SystemUI_LEDBlueSet"
        If (bEnable) Then
            byteSubCmd = &HF7
            strFuncName = strFuncName + " (ON) "
            If (Not bPatternSel) Then
                'Use Pattern 1
            Else
                'Use Pattern 2
            End If
        Else
            ' This will cause CRAN Version to disable Reader
            '****< Vendi's FW-based Product will cause this problem. >****
            Throw New Exception("[Warning] Blue LED OFF : CRAN Version to Disable Reader")
            byteSubCmd = &HF6
            strFuncName = strFuncName + " (OFF) "
        End If

        Try
            SystemUI_LEDSetCommon(strFuncName, byteSubCmd, arrayBytePatternSend, nTimeout)
        Catch ex As Exception
            Throw 'ex
        Finally
            Erase arrayBytePatternSend
        End Try
    End Sub
    '

    '
    Public Overridable Sub SystemUI_LEDTriColorRGBSet(Optional ByVal bPatternSel As LED_STATE = LED_STATE.Lx00_PISCES_ALL_OFF, Optional ByVal nTimeout As Integer = ClassDevRS232.m_cnTX_TIMEOUT_IN_SEC)
        '----------[ 2016 Aug 01 ]---------
        'Product: PISCES
        Dim byteSubCmd As Byte = &HE0
        Dim strFuncName As String = "SystemUI_LEDTriColorRGBSet"
        Dim arrayBytePatternSend As Byte()
        '
        arrayBytePatternSend = New Byte() {CType(bPatternSel, Byte)}
        '
        Try
            SystemUI_LEDSetCommon(strFuncName, byteSubCmd, arrayBytePatternSend, nTimeout)
        Catch ex As Exception
            Throw 'ex
        Finally
            Erase arrayBytePatternSend
        End Try
    End Sub

    ' bPatternSel = &H0 (OFF), &H1 (ON)
    Public Overridable Sub SystemUI_LED_ICC_RingLight(Optional ByVal bPatternSel As LED_STATE = LED_STATE.Lx00_OFF, Optional ByVal nTimeout As Integer = ClassDevRS232.m_cnTX_TIMEOUT_IN_SEC)
        '----------[ 2016 Aug 01 ]---------
        'Product: PISCES, ICC RingLED
        Dim byteSubCmd As Byte = &HE1
        Dim strFuncName As String = "SystemUI_LED_ICC_RingLight"
        Dim arrayBytePatternSend As Byte()
        '
        arrayBytePatternSend = New Byte() {CType(bPatternSel, Byte)}
        '
        Try
            SystemUI_LEDSetCommon(strFuncName, byteSubCmd, arrayBytePatternSend, nTimeout)
        Catch ex As Exception
            Throw 'ex
        Finally
            Erase arrayBytePatternSend
        End Try
    End Sub
    '
    ' bPatternSel = &H0 (OFF), &H1 (ON)
    Public Overridable Sub SystemUI_LED_Keypad_BKLight(Optional ByVal bPatternSel As LED_STATE = LED_STATE.Lx00_OFF, Optional ByVal nTimeout As Integer = ClassDevRS232.m_cnTX_TIMEOUT_IN_SEC)
        '----------[ 2016 Aug 01 ]---------
        'Product: PISCES, Keypad Backlight
        Dim byteSubCmd As Byte = &HE2
        Dim strFuncName As String = "SystemUI_LED_Keypad_BKLight"
        Dim arrayBytePatternSend As Byte()
        '
        arrayBytePatternSend = New Byte() {CType(bPatternSel, Byte)}
        '
        Try
            SystemUI_LEDSetCommon(strFuncName, byteSubCmd, arrayBytePatternSend, nTimeout)
        Catch ex As Exception
            Throw 'ex
        Finally
            Erase arrayBytePatternSend
        End Try
    End Sub
    '

    Public Overridable Sub SystemUI_NPT_BuzzerOnce(Optional ByVal nTimeout As Integer = ClassDevRS232.m_cnTX_TIMEOUT_IN_SEC)
        Dim strFuncName As String = "SystemUI_BuzzerOnceNoPassthrough()"
        LogLn("Not Implement" + strFuncName)
    End Sub
    '
    Public Overridable Sub SystemUI_PT_BuzzerSetCommon(ByVal bCmdShortLoopBeep As ENUM_BUZZER_TYPE, Optional ByVal byteShortNumOfBeeps_LongDurationMS As Byte = &H1, Optional ByVal nTimeout As Integer = ClassDevRS232.m_cnTX_TIMEOUT_IN_SEC)
        '
        Dim u16Cmd As UInt16 = CUShort(&HB00 + bCmdShortLoopBeep)
        Dim strFuncName As String = "SystemUI_BuzzerSetCommon"

        Dim arrayByteSend As Byte() = New Byte() {byteShortNumOfBeeps_LongDurationMS}
        Dim arrayByteRecv As Byte() = Nothing

        If (m_bIDGCmdBusy) Then
            Return
        End If
        m_bIDGCmdBusy = True

        Try
            System_Customized_Command(u16Cmd, strFuncName, arrayByteSend, arrayByteRecv, AddressOf ClassReaderProtocolBasic.PreProcessCbRecv, nTimeout, True)

            If (bIsRetStatusOK) Then
                'm_bIsRetStatusOK = True
            Else
                'm_bIsRetStatusOK = False
            End If

        Catch ext As TimeoutException
            LogLn("[Err][Timeout] [IDG: " + GetIDGCmdString(u16Cmd) + "] " + strFuncName + "() = " + ext.Message)
        Catch ex As Exception
            LogLn("[Err] [IDG:  " + GetIDGCmdString(u16Cmd) + "] " + strFuncName + "() = " + ex.Message)
        Finally
            m_tagIDGCmdDataInfo.Cleanup()
            m_bIDGCmdBusy = False
            If (arrayByteSend IsNot Nothing) Then
                Erase arrayByteSend
            End If
            If (arrayByteRecv IsNot Nothing) Then
                Erase arrayByteRecv
            End If
        End Try

    End Sub

    Public Enum LED_NUM As Byte
        Lx0 = &H0
        Lx1 '= &H1
        Lx2 '= &H2
        Lx3 '= &H3
        Lx4 '= &H4
        Lx5 '= &H5
        Lx6 '= &H6
        Lx7 '= &H7
        Lx8 '= &H8

        Lx10_TRICOLOR = &H10 '= &H10, UnipayIII Exclusive (Mobile Reader)

        LxFF_ALL = &HFF
    End Enum

    Public Enum LED_STATE As Byte
        Lx00_OFF = &H0 '
        Lx01_ON '=&H1

        Lx02_TRICOLOR_GREEN '=&H2, UnipayIII Exclusive (Mobile Reader)
        Lx03_TRICOLOR_RED '=&H3, UnipayIII Exclusive (Mobile Reader)
        Lx04_TRICOLOR_AMBER '=&H4, UnipayIII Exclusive (Mobile Reader)
        '
        Lx00_PISCES_ALL_OFF = &H0
        Lx01_PISCES_RED '= &H1
        Lx02_PISCES_GREEN
        Lx03_PISCES_R_G_x
        Lx04_PISCES_BLUE
        Lx05_PISCES_R_x_B
        Lx06_PISCES_x_G_B
        Lx07_PISCES_R_G_B
    End Enum
    '
    Public Overridable Sub SystemUI_LEDSet(enumLEDNum As LED_NUM, enumLEDState As LED_STATE, Optional ByVal nTimeout As Integer = ClassDevRS232.m_cnTX_TIMEOUT_IN_SEC)
        '
        Dim u16Cmd As UInt16 = &H102
        Dim strFuncName As String = "SystemUI_LEDSet"

        If (m_bIDGCmdBusy) Then
            Return
        End If
        m_bIDGCmdBusy = True

        Dim arrayByteCmdData As Byte() = New Byte() {&HFF, &H0, enumLEDNum, enumLEDState}
        Try
            '-----------------------------------------------------------------[ Send to reader ]-----------------------------------------------------------------
            System_Customized_Command(u16Cmd, strFuncName, arrayByteCmdData, Nothing, AddressOf ClassReaderProtocolBasic.PreProcessCbRecv, nTimeout)

            If (bIsRetStatusOK) Then

            Else

            End If

        Catch ext As TimeoutException
            LogLn("[Err][Timeout] [IDG: " + GetIDGCmdString(u16Cmd) + "] " + strFuncName + "() = " + ext.Message)
        Catch ex As Exception
            LogLn("[Err] [IDG:  " + GetIDGCmdString(u16Cmd) + "] " + strFuncName + "() = " + ex.Message)
        Finally
            m_tagIDGCmdDataInfo.Cleanup()
            m_bIDGCmdBusy = False

            Erase arrayByteCmdData
        End Try
    End Sub
    '
    Public Overridable Sub System_NEO20_UI_LEDSet_TriColor(ByVal byte01_LED As Byte, ByVal byte02_OnOff As Byte, Optional ByVal nTimeout As Integer = ClassDevRS232.m_cnTX_TIMEOUT_IN_SEC)
        '
        Dim u16Cmd As UInt16 = &HF802
        Dim strFuncName As String = "System_NEO20_UI_LEDSet_TriColor"

        If (m_bIDGCmdBusy) Then
            Return
        End If
        m_bIDGCmdBusy = True

        Dim arrayByteCmdData As Byte() = New Byte() {byte01_LED, byte02_OnOff}
        Try
            '-----------------------------------------------------------------[ Send to reader ]-----------------------------------------------------------------
            System_Customized_Command(u16Cmd, strFuncName, arrayByteCmdData, Nothing, AddressOf ClassReaderProtocolBasic.PreProcessCbRecv, nTimeout)

            If (bIsRetStatusOK) Then

            Else

            End If

        Catch ext As TimeoutException
            LogLn("[Err][Timeout] [IDG: " + GetIDGCmdString(u16Cmd) + "] " + strFuncName + "() = " + ext.Message)
        Catch ex As Exception
            LogLn("[Err] [IDG:  " + GetIDGCmdString(u16Cmd) + "] " + strFuncName + "() = " + ex.Message)
        Finally
            m_tagIDGCmdDataInfo.Cleanup()
            m_bIDGCmdBusy = False

            Erase arrayByteCmdData
        End Try
    End Sub
    '
    Public Overridable Sub SystemUI_LEDSet_PassthroughOnly(enumLEDNum As LED_NUM, enumLEDState As LED_STATE, Optional ByVal nTimeout As Integer = ClassDevRS232.m_cnTX_TIMEOUT_IN_SEC)
        '======================================[ Note Of Use ]======================================
        'This Passthrough Cmd is able to control POWER LED but you have to return the LED Control to EMV UI Controller
        'by configuration of  UI Scheme Tag
        Dim u16Cmd As UInt16 = &HA02
        Dim strFuncName As String = "SystemUI_LEDSet_PassthroughOnly"

        If (m_bIDGCmdBusy) Then
            Return
        End If
        m_bIDGCmdBusy = True

        Dim arrayByteCmdData As Byte() = New Byte() {enumLEDNum, enumLEDState}
        Try
            '-----------------------------------------------------------------[ Send to reader ]-----------------------------------------------------------------
            System_Customized_Command(u16Cmd, strFuncName, arrayByteCmdData, Nothing, AddressOf ClassReaderProtocolBasic.PreProcessCbRecv, nTimeout)

            If (bIsRetStatusOK) Then

            Else

            End If

        Catch ext As TimeoutException
            LogLn("[Err][Timeout] [IDG: " + GetIDGCmdString(u16Cmd) + "] " + strFuncName + "() = " + ext.Message)
        Catch ex As Exception
            LogLn("[Err] [IDG:  " + GetIDGCmdString(u16Cmd) + "] " + strFuncName + "() = " + ex.Message)
        Finally
            m_tagIDGCmdDataInfo.Cleanup()
            m_bIDGCmdBusy = False

            Erase arrayByteCmdData
        End Try
    End Sub


    '
    Public Overridable Sub SystemUI_LEDBlueYellowSet(Optional ByVal bEnable As Boolean = True, Optional ByVal bPatternSel As Boolean = False, Optional ByVal nTimeout As Integer = ClassDevRS232.m_cnTX_TIMEOUT_IN_SEC)
        'A.Blue LED Pattern Left (ON),Mid(OFF),Right(ON), MSR_Yellow(OFF) = (1,0,1,0), delay 1 sec = 100 millisec = 100 x 10
        'B.Blue LED Pattern Left (OFF),Mid(ON),Right(OFF), MSR_Yellow(OFF) = (0,1,0,0), delay 1 sec = 100 millisec
        'C.Blue LED Pattern Left (OFF),Mid(OFF),Right(OFF), MSR_Yellow(OFF) = (0,1,0,0), delay 1 sec = 100 millisec
        ' A + B + C total time = 3 seconds 
        'Dim arrayBytePatternSend As Byte() = New Byte() { _
        '    &H0, &HA0, &H0, 10, _
        '     &H40, &H0, 10, _
        '    &H0, &H0, 10}

        '----------[ 2015 Nov 18 updated ]---------
        '300 milli-sec / once 
        Dim arrayBytePatternSend As Byte() = New Byte() { _
            &H0, _
            &H10, &H0, 30, _
            &HE0, &H0, 30, _
            &HF0, &H0, 30}
        Dim byteSubCmd As Byte
        Dim strFuncName As String = "SystemUI_LEDBlueSet"
        If (bEnable) Then
            byteSubCmd = &HF7
            strFuncName = strFuncName + " (ON) "
            If (Not bPatternSel) Then
                'Use Pattern 1
            Else
                'Use Pattern 2
            End If
        Else
            ' This will cause CRAN Version to disable Reader
            '****< Vendi's FW-based Product will cause this problem. >****
            Throw New Exception("[Warning] Blue LED OFF : CRAN Version to Disable Reader")
            byteSubCmd = &HF6
            strFuncName = strFuncName + " (OFF) "
        End If

        Try
            SystemUI_LEDSetCommon(strFuncName, byteSubCmd, arrayBytePatternSend, nTimeout)
        Catch ex As Exception
            Throw 'ex
        Finally
            Erase arrayBytePatternSend
        End Try
    End Sub
    'Public Overridable Sub SystemUI_LEDBlueSet(Optional ByVal bEnable As Boolean = True, Optional ByVal bPatternSel As Boolean = False, Optional ByVal nTimeout As Integer = ClassDevRS232.m_cnTX_TIMEOUT_IN_SEC)


    '    Dim byteSubCmd As Byte
    '    Dim strFuncName As String = "SystemUI_LEDBlueSet"
    '    If (bEnable) Then
    '        byteSubCmd = &HF7
    '        strFuncName = strFuncName + " (ON) "
    '    Else
    '        byteSubCmd = &HF6
    '        strFuncName = strFuncName + " (OFF) "
    '    End If

    '    Try
    '        SystemUI_LEDSetCommon(strFuncName, byteSubCmd, nTimeout)
    '    Catch ex As Exception
    '        Throw 'ex
    '    Finally

    '    End Try
    'End Sub

    Public Overridable Sub SystemUI_LEDMSRSet(Optional ByVal bEnable As Boolean = True, Optional ByVal nTimeout As Integer = ClassDevRS232.m_cnTX_TIMEOUT_IN_SEC)
        Dim byteSubCmd As Byte
        Dim strFuncName As String = "SystemUI_LEDMSRSet"
        If (bEnable) Then
            byteSubCmd = &HFB
            strFuncName = strFuncName + " (ON) "
        Else
            byteSubCmd = &HFA
            strFuncName = strFuncName + " (OFF) "
        End If

        SystemUI_LEDSetCommon(strFuncName, byteSubCmd, nTimeout)
    End Sub

    Private Sub SystemUI_LEDSetCommon(strFuncName As String, ByVal byteSubCmd As Byte, Optional ByVal nTimeout As Integer = ClassDevRS232.m_cnTX_TIMEOUT_IN_SEC)
        '
        Dim u16Cmd As UInt16 = &HF000
        'Dim strFuncName As String = strFuncName

        If (m_bIDGCmdBusy) Then
            Return
        End If
        m_bIDGCmdBusy = True

        u16Cmd = u16Cmd + byteSubCmd

        Try
            System_Customized_Command(u16Cmd, strFuncName, Nothing, Nothing, AddressOf ClassReaderProtocolBasic.PreProcessCbRecv, nTimeout)

            If (bIsRetStatusOK) Then
                'm_bIsRetStatusOK = True
            Else
                'm_bIsRetStatusOK = False
            End If


        Catch ext As TimeoutException
            LogLn("[Err][Timeout] [IDG: " + GetIDGCmdString(u16Cmd) + "] " + strFuncName + "() = " + ext.Message)
        Catch ex As Exception
            LogLn("[Err] [IDG:  " + GetIDGCmdString(u16Cmd) + "] " + strFuncName + "() = " + ex.Message)
        Finally
            m_tagIDGCmdDataInfo.Cleanup()
            m_bIDGCmdBusy = False
        End Try
    End Sub
    '
    Public Sub SystemUI_LEDSetCommon(strFuncName As String, ByVal byteSubCmd As Byte, ByVal arrayByteSend As Byte(), Optional ByVal nTimeout As Integer = ClassDevRS232.m_cnTX_TIMEOUT_IN_SEC)
        '
        Dim u16Cmd As UInt16 = &HF000
        'Dim strFuncName As String = strFuncName

        If (m_bIDGCmdBusy) Then
            Return
        End If
        m_bIDGCmdBusy = True
        '
        u16Cmd = u16Cmd + byteSubCmd
        '
        Try
            System_Customized_Command(u16Cmd, strFuncName, arrayByteSend, Nothing, AddressOf ClassReaderProtocolBasic.PreProcessCbRecv, nTimeout)

            If (bIsRetStatusOK) Then
                'm_bIsRetStatusOK = True
            Else
                'm_bIsRetStatusOK = False
            End If
        Catch ext As TimeoutException
            LogLn("[Err][Timeout] [IDG: " + GetIDGCmdString(u16Cmd) + "] " + strFuncName + "() = " + ext.Message)
        Catch ex As Exception
            LogLn("[Err] [IDG:  " + GetIDGCmdString(u16Cmd) + "] " + strFuncName + "() = " + ex.Message)
        Finally
            m_tagIDGCmdDataInfo.Cleanup()
            m_bIDGCmdBusy = False
        End Try
    End Sub
    '
    '
    Public Overridable Sub SystemUI_EventInputGet(ByRef tagEvtInfo As TAG_GET_INPUT_EVETN, Optional ByVal nTimeout As Integer = ClassDevRS232.m_cnTX_TIMEOUT_IN_SEC)
        '

    End Sub

    Public Overridable Sub SystemUI_EventQueueClean(Optional ByVal nTimeout As Integer = ClassDevRS232.m_cnTX_TIMEOUT_IN_SEC)
        Try

        Catch ex As Exception
            LogLn("[Err] SystemUI_EventQueueClean () = " + ex.Message)
        Finally

        End Try
    End Sub


    Public Overridable Sub SystemUI_LED_TriColor_NEO20_Exclusive(p1 As Byte, p2 As Byte, p3 As Byte, Optional ByVal nTimeout As Integer = ClassDevRS232.m_cnRX_TIMEOUT_IN_SEC)
        Throw New NotImplementedException
    End Sub

    Public Overridable Sub SystemLCDContrastSet(u16Contrast As UInt16, Optional ByVal nTimeout As Integer = ClassDevRS232.m_cnTX_TIMEOUT_IN_SEC)
        Try

        Catch ex As Exception
            LogLn("[Err] () = " + ex.Message)
        Finally

        End Try
    End Sub
    '
    Public Overridable Sub SystemLCDContrastGet(ByRef o_u16Contrast As UInt16, Optional ByVal nTimeout As Integer = ClassDevRS232.m_cnTX_TIMEOUT_IN_SEC)
        Try

        Catch ex As Exception
            LogLn("[Err] () = " + ex.Message)
        Finally

        End Try
    End Sub
    '
    Public Overridable Sub SystemLCDBackLightSet(u32BackLight As UInt32, Optional ByVal nTimeout As Integer = ClassDevRS232.m_cnTX_TIMEOUT_IN_SEC)
        Try

        Catch ex As Exception
            LogLn("[Err] () = " + ex.Message)
        Finally

        End Try
    End Sub

    Public Overridable Sub SystemGraphic_ChkBoxDisplay(ByVal nXPos As Integer, ByVal nYPos As Integer, ByVal nW As Integer, ByVal nH As Integer, ByVal nSts_x0_x1 As Byte, ByRef o_strGraphicID As String, Optional ByVal nTimeout As Integer = ClassDevRS232.m_cnTX_TIMEOUT_IN_SEC)
        Throw New NotImplementedException("SystemGraphic_ChkBoxDisplay")
    End Sub
    '
    '
    Public Overridable Sub SystemLCDCustomDisplayModeSetOn(Optional ByVal nTimeout As Integer = ClassDevRS232.m_cnTX_TIMEOUT_IN_SEC)
        SystemLCDCustomDisplayModeSetSelect()
    End Sub

    Public Overridable Sub SystemLCDCustomDisplayModeSetOff(Optional ByVal nTimeout As Integer = ClassDevRS232.m_cnTX_TIMEOUT_IN_SEC)
        SystemLCDCustomDisplayModeSetSelect(False)
    End Sub
    '
    Public Overridable Sub SystemLCDDisplayText(ByRef txtInfoString1 As String, Optional ByVal nTimeout As Integer = ClassDevRS232.m_cnTX_TIMEOUT_IN_SEC)
        Dim txnEnhInfo As ClassTransaction.TAG_IDG0220_IDG8303_ENH_TXN_ACTIVATE

        Try
            Select Case (m_nIDGCommanderType)
                Case ClassReaderInfo.ENU_FW_PLATFORM_TYPE.GR, ClassReaderInfo.ENU_FW_PLATFORM_TYPE.NEO_IDG, ClassReaderInfo.ENU_FW_PLATFORM_TYPE.NEO20_IDG
                    SystemLCDDisplayText(txtInfoString1, 1, nTimeout)
                Case ClassReaderInfo.ENU_FW_PLATFORM_TYPE.AR
                    txnEnhInfo = New ClassTransaction.TAG_IDG0220_IDG8303_ENH_TXN_ACTIVATE(0, 16, 0, 0, 1, 1, 1, txtInfoString1, 0)
                    SystemLCDDisplayText(txnEnhInfo, nTimeout)
                    'Case ClassReaderInfo.ENU_FW_PLATFORM_TYPE.NEO_IDG
                    '    SystemLCDDisplayText(txtInfoString1, 1, nTimeout)
                Case Else
                    Throw New Exception("Unknown Command Type = " + m_nIDGCommanderType.ToString())
            End Select

        Catch ex As Exception
            LogLn("[Err] SystemLCDDisplayText () = " + ex.Message)
        Finally

        End Try
    End Sub
    '
    '
    Public Overridable Sub SystemLCDDisplayImage(nPosX As Integer, nPosY As Integer, byteFlag As Byte, strRemoteImageFilePath As String, Optional ByVal nTimeout As Integer = ClassDevRS232.m_cnTX_TIMEOUT_IN_SEC)
        Try

        Catch ex As Exception
            LogLn("[Err] () = " + ex.Message)
        Finally

        End Try
    End Sub
    '
    Public Overridable Sub SystemLCDDisplayText(ByVal strText As String, ByVal nLine As Integer, ByVal nTimeout As Integer)
        Try

        Catch ex As Exception
            LogLn("[Err] () = " + ex.Message)
        Finally

        End Try
    End Sub
    '
    Public Overridable Sub SystemLCDDisplayText(ByRef txtInfoString1 As ClassTransaction.TAG_IDG0220_IDG8303_ENH_TXN_ACTIVATE, Optional ByVal nTimeout As Integer = ClassDevRS232.m_cnTX_TIMEOUT_IN_SEC)
        Try

        Catch ex As Exception
            LogLn("[Err] () = " + ex.Message)
        Finally

        End Try
    End Sub
    '
    '
    Protected Overridable Sub SystemLCDCustomDisplayModeSetSelect(Optional byteModeOn As Boolean = True, Optional ByVal nTimeout As Integer = ClassDevRS232.m_cnTX_TIMEOUT_IN_SEC)
        Dim u16Cmd As UInt16 = &H8308
        If (Not byteModeOn) Then
            u16Cmd = &H8309
        End If
        '
        Try

        Catch ex As Exception
            LogLn("[Err] () = " + ex.Message)
        Finally

        End Try
    End Sub

End Class
