﻿'==================================================================================
' File Name: ClassReaderProtocolBasic.vb
' Description: The ClassReaderProtocolBasic is as Must-be Inherited Base Class of Reader's Packet Parser & Composer.
' Who use it as Base Class: ClassReaderProtocolBasicIDGAR, ClassReaderProtocolBasicIDGAR, and ClassReaderProtocolBasicIDTECH
' For EMV L2 Functions
' 
' Revision:
' -----------------------[ Initial Version ]-----------------------
' 2017 Oct 06, by goofy_liu@idtechproducts.com.tw

'==================================================================================
'
'----------------------[ Importing Stuffs ]----------------------
Imports System.Runtime.Serialization
Imports System.Runtime.Serialization.Formatters.Binary
'Imports DiagTool_HardwareExclusive.ClassReaderProtocolBasicIDGAR
Imports System.IO
Imports System.IO.Ports
Imports System.Threading

'-------------------[ Class Declaration ]-------------------

Partial Public Class ClassReaderProtocolBasic

    Public Shared m_dictRFErrCodeRetryTransactionAllowedListForTransactionErr20h As List(Of EnumSTATECODE_RF) = New List(Of EnumSTATECODE_RF) From {
       EnumSTATECODE_RF.x01PPSE_ErrorOccurredDuringPPSECommand,
   EnumSTATECODE_RF.x02SELECT_ErrorOccurredDuringSELECTCommand,
   EnumSTATECODE_RF.x03GPO_ErrorOccurredDuringGETPROCESSINGOPTIONSCommand,
   EnumSTATECODE_RF.x04READRECORD_ErrorOccurredDuringREADRECORDCommand,
   EnumSTATECODE_RF.x12GETDATABalance_ErrorOccurredDuringGETDATACommandToRetrieveTheBalance,
   EnumSTATECODE_RF.x11GETDATATicketingProf_ErrorOccurredDuringGETDATACommandToRetrieveTheTicketingProfile,
   EnumSTATECODE_RF.x10GETDATATicket_ErrorOccurredDuringGETDATACommandToRetrieveTheTicket,
   EnumSTATECODE_RF.x20PUTDATATicket_ErrorOccurredDuringPUTDATACommandToRetrieveTheTicket,
   EnumSTATECODE_RF.x05GENAC_ErrorOccurredDuringGENACCommand
       }

    Public Shared m_dictRFErrCodeRetryTransactionAllowedListForTransactionErr30h As List(Of EnumSTATECODE_RF) = New List(Of EnumSTATECODE_RF) From {
    EnumSTATECODE_RF.x01PPSE_ErrorOccurredDuringPPSECommand,
    EnumSTATECODE_RF.x02SELECT_ErrorOccurredDuringSELECTCommand,
    EnumSTATECODE_RF.x03GPO_ErrorOccurredDuringGETPROCESSINGOPTIONSCommand,
    EnumSTATECODE_RF.x04READRECORD_ErrorOccurredDuringREADRECORDCommand,
    EnumSTATECODE_RF.x12GETDATABalance_ErrorOccurredDuringGETDATACommandToRetrieveTheBalance,
    EnumSTATECODE_RF.x11GETDATATicketingProf_ErrorOccurredDuringGETDATACommandToRetrieveTheTicketingProfile,
    EnumSTATECODE_RF.x10GETDATATicket_ErrorOccurredDuringGETDATACommandToRetrieveTheTicket,
    EnumSTATECODE_RF.x20PUTDATATicket_ErrorOccurredDuringPUTDATACommandToRetrieveTheTicket,
    EnumSTATECODE_RF.x05GENAC_ErrorOccurredDuringGENACCommand
    }
    
    '-------------------------------------------------------------------------------------------------
    Public Shared m_dictRFStateCodes As Dictionary(Of Byte, String) = New Dictionary(Of Byte, String) From {
        {EnumSTATECODE_RF.x00None_RFStateCodeNotAvailable, "None - RF State Code Not Available"},
        {EnumSTATECODE_RF.x01PPSE_ErrorOccurredDuringPPSECommand, "PPSE - Error Occurred During PPSE Command"},
        {EnumSTATECODE_RF.x02SELECT_ErrorOccurredDuringSELECTCommand, "SELECT - Error Occurred During SELECT Command"},
        {EnumSTATECODE_RF.x03GPO_ErrorOccurredDuringGETPROCESSINGOPTIONSCommand, "GPO - Error Occurred During GET PROCESSING OPTIONS Command"},
        {EnumSTATECODE_RF.x04READRECORD_ErrorOccurredDuringREADRECORDCommand, "READ RECORD - Error Occurred During READ RECORD Command"},
        {EnumSTATECODE_RF.x05GENAC_ErrorOccurredDuringGENACCommand, "GEN AC - Error Occurred During GEN AC Command"},
        {EnumSTATECODE_RF.x06CCC_ErrorOccurredDuringCCCCommand, "CCC - Error Occurred During CCC Command"},
        {EnumSTATECODE_RF.x07IA_ErrorOccurredDuringIACommand, "IA - Error Occurred During IA Command"},
        {EnumSTATECODE_RF.x08SDA_ErrorOccurredDuringSDAProcessing, "SDA - ErrorOccurred During SDA Processing"},
        {EnumSTATECODE_RF.x09DDA_ErrorOccurredDuringDDAProcessing, "DDA - Error Occurred During DDA Processing"},
        {EnumSTATECODE_RF.x0ACDA_ErrorOccurredDuringCDAProcessing, "CDA - Error Occurred During CDA Processing"},
        {EnumSTATECODE_RF.x0BTAA_ErrorOccurredDuringTAAProcessing, "TAA - Error Occurred During TAA Processing"},
        {EnumSTATECODE_RF.x0CUPDATERECORD_ErrorOccurredDuringUPDATERECORDCommand, "UPDATE RECORD - Error Occurred During UPDATE RECORD Command"},
        {EnumSTATECODE_RF.x10GETDATATicket_ErrorOccurredDuringGETDATACommandToRetrieveTheTicket, "GET DATA Ticket - Error Occurred During GET DATA Command To Retrieve The Ticket"},
        {EnumSTATECODE_RF.x11GETDATATicketingProf_ErrorOccurredDuringGETDATACommandToRetrieveTheTicketingProfile, "GET DATA Ticketing Prof - Error Occurred During GET DATA Command To Retrieve The Ticketing Profile"},
        {EnumSTATECODE_RF.x12GETDATABalance_ErrorOccurredDuringGETDATACommandToRetrieveTheBalance, "GET DATA Balance - Error Occurred During GET DATA Command To Retrieve The Balance"},
        {EnumSTATECODE_RF.x13GETDATAAll_ErrorOccurredDuringGETDATACommandToRetrieveAllData, "GET DATA All - Error Occurred During GET DATA Command To Retrieve All Data"},
        {EnumSTATECODE_RF.x20PUTDATATicket_ErrorOccurredDuringPUTDATACommandToRetrieveTheTicket, "PUT DATA Ticket - Error Occurred During PUT DATA Command To Retrieve The Ticket"}
        }

    Public Shared Function GetTransactionRFErrCodeMsg(ByVal byteVal As Byte, ByRef strMsg As String) As Boolean

        If Not m_dictRFStateCodes.ContainsKey(byteVal) Then
            strMsg = "N/A"
            Return False
        End If
        '
        strMsg = m_dictRFStateCodes.Item(byteVal)
        Return True
    End Function
    '-------------------------------------------------------------------------------------------------
    Public Shared m_dictTransactionErrorCodes As Dictionary(Of Byte, String) = New Dictionary(Of Byte, String) From {
{EnumTXN_ERRCODE.x00NoError, "No Error"},
{EnumTXN_ERRCODE.x01OutOfSequence, "Out Of Sequence"},
{EnumTXN_ERRCODE.x02GoToContactInterface, "Go To Contact Interface"},
{EnumTXN_ERRCODE.x03TransactionAmountIsZero, "Transaction Amount Is Zero"},
{EnumTXN_ERRCODE.x20CardReturnedErrorStatus, "Card Returned Error Status"},
{EnumTXN_ERRCODE.x21CollisionError, "Collision Error"},
{EnumTXN_ERRCODE.x22AmountOverMaximumLimit, "Amount Over Maximum Limit"},
{EnumTXN_ERRCODE.x23RequestOnlineAuthorization, "Request On-line Authorization"},
{EnumTXN_ERRCODE.x25CardBlocked, "Card Blocked"},
{EnumTXN_ERRCODE.x26CardExpired, "Card Expired"},
{EnumTXN_ERRCODE.x27UnsupportedCard, "Unsupported Card"},
{EnumTXN_ERRCODE.x30CardDidNotRespond, "Card Did Not Respond"},
{EnumTXN_ERRCODE.x42CardGeneratedAAC, "Card Generated AAC"},
{EnumTXN_ERRCODE.x43CardGeneratedARQC, "Card Generated ARQC"},
{EnumTXN_ERRCODE.x44SDAorDDAFailedSinceNotSupportedByCard, "SDA/DDA Failed Since Not Supported By Card"},
{EnumTXN_ERRCODE.x50SDAorDDAorCDDAFailed_CAPublicKey, "SDA/DDA/CDDA Failed - CA Public Key "},
{EnumTXN_ERRCODE.x51SDAorDDAorCDDAFailed_IssuerPublicKey, "SDA/DDA/CDDA Failed - Issuer Public Key"},
{EnumTXN_ERRCODE.x52SDAFailed_SSAD, "SDA Failed - SSAD"},
{EnumTXN_ERRCODE.x53DDAorCDDAFailed_ICCPublicKey, "DDAorCDDAFailed - ICCPublicKey"},
{EnumTXN_ERRCODE.x54DDAorCDDAFailed_DynamicSignatureVerification, "DDA/CDDA Failed - Dynamic Signature Verification"},
{EnumTXN_ERRCODE.x55ProcessionRestrictionsFailed, "Procession Restrictions Failed"},
{EnumTXN_ERRCODE.x56TerminalRiskManagementFailed_TRM, "Terminal Risk Management Failed - TRM"},
{EnumTXN_ERRCODE.x57CardholderVerificationFailed, "Card holder Verification Failed"},
{EnumTXN_ERRCODE.x58TerminalActionAnalysisFailed_TAA, "Terminal Action Analysis Failed - TAA"},
{EnumTXN_ERRCODE.x61SDMemoryError, "SD Memory Error"}
}

    Public Shared Function GetTransactionErrCodeMsg(ByVal byteValTxnRC As Byte, ByRef strMsg As String, Optional ByVal byteValTxnRFRC As Byte = &HFF) As Boolean
        Dim strRFErr As String = "N/A"
        If Not m_dictTransactionErrorCodes.ContainsKey(byteValTxnRC) Then
            strMsg = "N/A"
            Return False
        End If
        '
        If (byteValTxnRC = &H20) Or (byteValTxnRC = &H30) Then
            If (byteValTxnRFRC <> &HFF) Then
                GetTransactionRFErrCodeMsg(byteValTxnRFRC, strRFErr)
                strMsg = m_dictTransactionErrorCodes.Item(byteValTxnRC) + ", RF Err Code = " + strRFErr
                Return True
            End If
        End If

        strMsg = m_dictTransactionErrorCodes.Item(byteValTxnRC)
        Return True
    End Function

    Public Sub System_Reader_UIScheme_Set_Common(ByVal strFuncName As String, ByVal u8UIScheme As tagTLV.ENUM_FFF8_UI_SCHEME, Optional ByVal nTimeout As Integer = ClassDevRS232.m_cnTX_TIMEOUT_IN_SEC)
        Dim u16Cmd As UInt16 = &H400

        Dim arrayByteSend As Byte() = New Byte() {&HFF, &HF8, &H1, u8UIScheme}
        Dim arrayByteRecv As Byte() = Nothing

        If (m_bIDGCmdBusy) Then
            Return
        End If
        m_bIDGCmdBusy = True
        Try
            'ClassTableListMaster.ConvertValue2ArrayByte(byteModeOn, arrayByteSend)

            System_Customized_Command(u16Cmd, strFuncName, arrayByteSend, arrayByteRecv, AddressOf PreProcessCbRecv, nTimeout)

            If (bIsRetStatusOK) Then

            Else

            End If
        Catch ex As Exception
            Throw 'ex
        Finally
            m_bIDGCmdBusy = False
            If (arrayByteSend IsNot Nothing) Then
                Erase arrayByteSend
            End If
            If (arrayByteRecv IsNot Nothing) Then
                Erase arrayByteRecv
            End If
        End Try
    End Sub

    Public Sub System_Reader_UIScheme_Set_EMEA(Optional ByVal nTimeout As Integer = ClassDevRS232.m_cnTX_TIMEOUT_IN_SEC)

        Dim strFuncName As String = "System_ReaderUI_EMEA"

        System_Reader_UIScheme_Set_Common(strFuncName, tagTLV.ENUM_FFF8_UI_SCHEME.x03_EMEA, nTimeout)

    End Sub

    Public Sub System_Reader_UIScheme_Set_VisaWave(Optional ByVal nTimeout As Integer = ClassDevRS232.m_cnTX_TIMEOUT_IN_SEC)

        Dim strFuncName As String = "System_Reader_UIScheme_Set_VisaWave"

        System_Reader_UIScheme_Set_Common(strFuncName, tagTLV.ENUM_FFF8_UI_SCHEME.x02_VISA_WAVE, nTimeout)

    End Sub

    Public Sub System_Reader_UIScheme_Set_ViVOPay(Optional ByVal nTimeout As Integer = ClassDevRS232.m_cnTX_TIMEOUT_IN_SEC)

        Dim strFuncName As String = "System_Reader_UIScheme_Set_ViVOPay"

        System_Reader_UIScheme_Set_Common(strFuncName, tagTLV.ENUM_FFF8_UI_SCHEME.x00_VIVOPAY, nTimeout)

    End Sub
    '
    '
    '=======================[ APIs / SDKs  Section ]=======================
    Public Overridable Sub SetConfigurations(ByRef dictTLV As Dictionary(Of String, tagTLV), Optional ByVal nTimeout As Integer = ClassDevRS232.m_cnTX_TIMEOUT_IN_SEC)

    End Sub
    '
    Public Overridable Sub SetConfigurations(ByVal o_arrayByteTLVData As Byte(), Optional ByVal nTimeout As Integer = ClassDevRS232.m_cnTX_TIMEOUT_IN_SEC)

    End Sub
    '

    Public Overridable Sub SetConfigurationsGroupSingleIndexed(ByRef configGroup As tagConfigCmdUnitGroup, Optional ByVal nTimeout As Integer = ClassDevRS232.m_cnTX_TIMEOUT_IN_SEC)
        SetConfigurationsGroupSingle(&H403, configGroup)
    End Sub

    Public Overridable Sub SetConfigurationsGroupSingleDefault(ByRef configGroup As tagConfigCmdUnitGroup, Optional ByVal nTimeout As Integer = ClassDevRS232.m_cnTX_TIMEOUT_IN_SEC)
        SetConfigurationsGroupSingle(&H400, configGroup)
    End Sub



    Public Overridable Sub SetConfigurationsGroupSingle(ByRef configGroup As tagConfigCmdUnitGroup, Optional ByVal nTimeout As Integer = ClassDevRS232.m_cnTX_TIMEOUT_IN_SEC)
        If (configGroup.m_nGroupID = 0) Then
            SetConfigurationsGroupSingle(&H400, configGroup, nTimeout)
        Else
            SetConfigurationsGroupSingle(&H403, configGroup, nTimeout)
        End If
    End Sub

    'IDG CMD:_0403
    'Note: bSendIt, default = TRUE.
    '          For Mass Group Data Operations, the invoker may need to compose all/multiple group info until send them out (to reader), similar to batch job case
    '          In the case of above, the invoker should set bSendIt = False to prevent the sending  step triggered while invoking.

    Protected Sub SetConfigurationsGroupSingle(ByVal u16Cmd As UInt16, ByRef configGroup As tagConfigCmdUnitGroup, Optional ByVal nTimeout As Integer = ClassDevRS232.m_cnTX_TIMEOUT_IN_SEC)
        'Dim u16Cmd As UInt16 = 
        Dim strFuncName As String = ""
        Dim iteratorTLV As KeyValuePair(Of String, tagTLV)
        Dim dictTLV As SortedDictionary(Of String, tagTLV) = Nothing
        Dim tlvOp As tagTLVNewOp
        '
        m_bIsRetStatusOK = False
        If (m_bIDGCmdBusy) Then
            Return
        End If
        m_bIDGCmdBusy = True
        Try
            m_tagIDGCmdDataInfo.Reinit(u16Cmd, m_arraybyteSendBuf, Me, , m_arraybyteSendBufUnknown)

            'Create Group Major Information
            m_tagIDGCmdDataInfo.m_nGroupID = configGroup.m_nGroupID
            m_tagIDGCmdDataInfo.m_nTransactionType = configGroup.m_nTransactionType
            'm_tagIDGCmdDataInfo.m_refarraybyteData = m_arraybyteSendBuf

            'Clone the dictionary for operation
            dictTLV = ClassReaderProtocolBasic.CloneDict(configGroup.m_dictTLV) ' configGroup.m_dictTLV

            'if Group 0(i.e. Default Group), to use Set Configuration Command (04-00) @ GR is better
            If configGroup.m_nGroupID <> 0 Then
                'Put Group ID("FFE4") At First for Mandatory of IDG GR Cmd 04-03 Spec
                If Not dictTLV.ContainsKey(tagTLV.TAG_FFE4_or_DFEE2D_GrpID) Then
                    Throw New Exception("Group ID (T=" + tagTLV.TAG_FFE4_or_DFEE2D_GrpID + ", V = " & configGroup.m_nGroupID & ") Is Unavailable !!")
                End If
                '
                'Composer The Packet with Group ID Info
                tlvOp =
ClassReaderCommanderParser.m_dictTLVOp_RefList_Major.Item(tagTLV.TAG_FFE4_or_DFEE2D_GrpID)
                tlvOp.PktComposer(dictTLV.Item(tagTLV.TAG_FFE4_or_DFEE2D_GrpID), m_tagIDGCmdDataInfo)
                dictTLV.Remove(tagTLV.TAG_FFE4_or_DFEE2D_GrpID)
            Else
                LogLn("This is default Group (ID = 0)")
            End If

            'Compose Rest TLVs
            'ComposeRestTLVsConfigurations(dictTLV, m_tagIDGCmdDataInfo)
            m_tagIDGCmdDataInfo.ComposerAddTLVs(dictTLV)

            '-----------------------------------------------------------------[ Send to reader ]-----------------------------------------------------------------
            'Compose 1)Protocol Header 2) Command Set 3) Length 4) Command Frame CRC Data
            PktComposerCommandFrameHeaderCmdSetDataLengthCRCData(m_tagIDGCmdDataInfo)

            '----------------[ Debug Section ]----------------
            If (m_bDebug) Then
                'Note: After PktComposerCommandFrameHeaderCmdSetDataLengthCRCData(), m_tagIDGCmdDataInfo.m_nDataSizeSend includes m_nPktHeader + m_nPktTrail
                'm_refwinformMain.LogLnDumpData(m_tagIDGCmdDataInfo.m_refarraybyteData, tagIDGCmdData.m_nPktHeader + m_tagIDGCmdDataInfo.m_nDataSizeSend + tagIDGCmdData.m_nPktTrail)
                Form01_Main.LogLnDumpData(m_tagIDGCmdDataInfo.m_refarraybyteData.Skip(m_tagIDGCmdDataInfo.m_nDataRecvOffSkipBytes).ToArray(), m_tagIDGCmdDataInfo.m_nDataSizeRecv)
            End If

            '----------------[ End Debug Section ]----------------
            'Waiting For Response in SetConfigurationsGroupSingleCbRecv()
            m_refdevConn.SendCommandFrameAndWaitForResponseFrame(AddressOf ClassReaderProtocolBasic.PreProcessCbRecv, m_tagIDGCmdDataInfo, m_nIDGCommanderType, True, nTimeout * 1000)

            'Its' response code = 0 => OK
            If (m_tagIDGCmdDataInfo.bIsResponseOK) Then
                m_bIsRetStatusOK = True
            Else
                m_bIsRetStatusOK = False
            End If
        Catch ext As TimeoutException
            LogLn("[Err][Timeout] [IDG: " + GetIDGCmdString(u16Cmd) + "] " + strFuncName + "() = " + ext.Message + ",Group ID= " & configGroup.m_nGroupID)
        Catch ex As Exception
            LogLn("[Err] [IDG:  " + GetIDGCmdString(u16Cmd) + "] " + strFuncName + "() = " + ex.Message)
        Finally
            If dictTLV IsNot Nothing Then
                For Each iteratorTLV In dictTLV
                    iteratorTLV.Value.Cleanup()
                Next
                'Clean up stuffs
                dictTLV.Clear()
            End If
            '
            m_bIDGCmdBusy = False
        End Try

    End Sub

    Public Overridable Sub SetConfigurationsGroupMass()
        Try

        Catch ex As Exception
            LogLn("[Err] () = " + ex.Message)
        Finally

        End Try
    End Sub

    Public Overridable Sub GetConfigurationsGroupMass(ByRef o_configGroupList As Dictionary(Of Integer, tagConfigCmdUnitGroup), Optional ByVal nTimeout As Integer = ClassDevRS232.m_cnTX_TIMEOUT_IN_SEC)
        Try

        Catch ex As Exception
            LogLn("[Err] () = " + ex.Message)
        Finally

        End Try
    End Sub

    Public Overridable Sub SetCRLMass()
        Try

        Catch ex As Exception
            LogLn("[Err] () = " + ex.Message)
        Finally

        End Try
    End Sub

    Public Overridable Sub GetCAPublicKeyListIDMass(ByVal arrayByteRID() As Byte, ByRef o_RIDKeyList() As Byte, Optional ByVal nTimeout As Integer = ClassDevRS232.m_cnTX_TIMEOUT_IN_SEC)
        Try

        Catch ex As Exception
            LogLn("[Err] () = " + ex.Message)
        Finally

        End Try
    End Sub

    Public Overridable Sub GetCAPublicKeyListRIDMass()
        Try

        Catch ex As Exception
            LogLn("[Err] () = " + ex.Message)
        Finally

        End Try
    End Sub
    '
    Public Overridable Sub SetAllCommandsFromTables(ByRef refobjCTLM As ClassTableListMaster, ByVal bPollmodeMode As Byte, Optional ByVal nTimeout As Integer = ClassDevRS232.m_cnTX_TIMEOUT_IN_SEC, Optional ByVal funcInProgressUpdate As Action(Of Integer) = Nothing)

    End Sub
    '
    Public Overridable Sub SetCAPublicKeyMass()
        Try

        Catch ex As Exception
            LogLn("[Err] () = " + ex.Message)
        Finally

        End Try
    End Sub


    Public Overridable Sub GetCAPublicKeyIDMass()
        Try

        Catch ex As Exception
            LogLn("[Err] () = " + ex.Message)
        Finally

        End Try
    End Sub

    Public Overridable Sub SetConfigurationsAIDMass()
        Try

        Catch ex As Exception
            LogLn("[Err] () = " + ex.Message)
        Finally

        End Try
    End Sub

    Public Overridable Sub GetConfigurationsAIDMass(ByRef o_AIDList As Dictionary(Of String, tagConfigCmdUnitAID), Optional ByVal nTimeout As Integer = ClassDevRS232.m_cnTX_TIMEOUT_IN_SEC)
        Try

        Catch ex As Exception
            LogLn("[Err] () = " + ex.Message)
        Finally

        End Try
    End Sub

    'Note: This Get All AID Information may contain 2 or more same TLVs in response.
    'Ref Spec Document Version : NEO V1.00 Rev 77, 2016 Mar 16
    Public Overridable Sub GetConfigurationsAIDMass(ByRef o_AIDList As List(Of tagTLV), Optional ByVal nTimeout As Integer = ClassDevRS232.m_cnTX_TIMEOUT_IN_SEC)

        Dim u16Cmd As UInt16 = &H305
        Dim strFuncName As String = "GetConfigurationsAIDMass, List of TagTLV"

        m_bIsRetStatusOK = False
        If (m_bIDGCmdBusy) Then
            Return
        End If
        m_bIDGCmdBusy = True
        Try
            '
            m_bIsRetStatusOK = False
            '
            m_tagIDGCmdDataInfo.Reinit(u16Cmd, m_arraybyteSendBuf, Me, , m_arraybyteSendBufUnknown)
            '-----------------------------------------------------------------[ Send to reader ]-----------------------------------------------------------------
            ''Compose The IDG Packet 03-05, Not Command Frame Data, No Response Frame Data
            'arrayByteData = m_tagIDGCmdDataInfo.m_refarraybyteData
            ''Composer The Packet with Group ID Info
            'tlvOp = m_refdicttlvConfigurationsTLVOp.Item(tagTLV.TAG_FFE4)
            ''Make a TLV with Group ID = 01
            'tlvTmp = New tagTLV(tagTLV.TAG_FFE4, 1, "01")
            'tlvOp.PktComposer(tlvTmp, m_tagIDGCmdDataInfo)

            'Compose 1)Protocol Header 2) Command Set 3) Length 4) Command Frame CRC Data
            PktComposerCommandFrameHeaderCmdSetDataLengthCRCData(m_tagIDGCmdDataInfo)
            'tlvTmp.Cleanup()

            '----------------[ Debug Section ]----------------
            If (m_bDebug) Then
                'Note: After PktComposerCommandFrameHeaderCmdSetDataLengthCRCData(), m_tagIDGCmdDataInfo.m_nDataSizeSend includes m_nPktHeader + m_nPktTrail
                'Form01_Main .LogLnDumpData(m_tagIDGCmdDataInfo.m_refarraybyteData, tagIDGCmdData.m_nPktHeader + m_tagIDGCmdDataInfo.m_nDataSizeSend + tagIDGCmdData.m_nPktTrail)
                Form01_Main.LogLnDumpData(m_tagIDGCmdDataInfo.m_refarraybyteData, m_tagIDGCmdDataInfo.m_nDataSizeSend)
            End If
            '----------------[ End Debug Section ]----------------

            'Waiting For Response in GetConfigurationsGroupSingleCbRecv()
            m_refdevConn.SendCommandFrameAndWaitForResponseFrame(AddressOf ClassReaderProtocolBasic.PreProcessCbRecv, m_tagIDGCmdDataInfo, m_nIDGCommanderType, True, nTimeout * 1000)

            'Its' response code = 0 => OK
            If (m_tagIDGCmdDataInfo.bIsResponseOK) Then
                Dim byteArrayData As Byte() = Nothing

                If m_tagIDGCmdDataInfo.bIsRawData Then
                    m_bIsRetStatusOK = True

                    m_tagIDGCmdDataInfo.ParserDataGetArrayRawData(byteArrayData)

                    'Form01_Main .LogLnDumpData(byteArrayData, byteArrayData.Count )
                    ClassReaderCommanderParser.TLV_ParseFromReaderMultipleTLVs(byteArrayData, o_AIDList)

                    Erase byteArrayData
                End If
            Else
                m_bIsRetStatusOK = False
            End If
        Catch ext As TimeoutException
            LogLn("[Err][Timeout] [IDG: " + GetIDGCmdString(u16Cmd) + "] " + strFuncName + "() = " + ext.Message + ",AID List Count= " & o_AIDList.Count)
        Catch ex As Exception
            LogLn("[Err] [IDG:  " + GetIDGCmdString(u16Cmd) + "] " + strFuncName + "() = " + ex.Message)
        Finally
            m_bIDGCmdBusy = False
        End Try
        '
    End Sub

    Public Overridable Sub GetCAPublicKeyIDSingle()
        Try

        Catch ex As Exception
            LogLn("[Err] () = " + ex.Message)
        Finally

        End Try
    End Sub
    '
    ''==========[03-02]==========
    '
    Public Sub GetConfigurationsGroupDefault(ByRef o_dictTLVs As Dictionary(Of String, tagTLV), Optional ByVal nTimeout As Integer = ClassDevRS232.m_cnTX_TIMEOUT_IN_SEC)
        Dim sdictTLV As SortedDictionary(Of String, tagTLV) = Nothing

        Try
            GetConfigurationsGroupDefault(sdictTLV, nTimeout)

            If (sdictTLV IsNot Nothing) Then
                If (sdictTLV.Count > 0) Then
                    If (o_dictTLVs Is Nothing) Then
                        o_dictTLVs = New Dictionary(Of String, tagTLV)
                    Else
                        ClassTableListMaster.CleanupConfigurationsTLVList(o_dictTLVs)
                    End If
                    ' Copy to destination dictionary
                    For Each iteraTLV In sdictTLV
                        o_dictTLVs.Add(iteraTLV.Key, iteraTLV.Value)
                    Next
                End If
            End If
        Catch ex As Exception
            LogLn("[Err] GetConfigurationsGroupDefault () = " + ex.Message)
        Finally
            If (sdictTLV IsNot Nothing) Then
                ClassTableListMaster.CleanupConfigurationsTLVList(sdictTLV)
            End If
        End Try
    End Sub
    '--------------------------------------[ IDG CMD:_0302 ]--------------------------------------
    'Private Shared Sub GetConfigurationsGroupDefaultCbRecv(ByRef devSerialPort As SerialPort, ByRef refConfigGroupData As tagIDGCmdData, ByVal bRS232CommOK As Boolean)
    '    Try
    '        ' Common Process to receive data 
    '        PreProcessCbRecv(devSerialPort, refConfigGroupData, bRS232CommOK)
    '        '
    '    Catch ex As Exception
    '        LogLnThreadSafe("[Err] GetConfigurationsGroupDefaultCbRecv() = " + ex.Message)
    '    End Try
    'End Sub

    'IDG CMD:_0302
    'IN = configGroup = Nothing
    'OUT = Wanted Group ID
    '
    Public Overridable Sub GetConfigurationsGroupDefault(ByRef o_sdictTLVs As SortedDictionary(Of String, tagTLV), Optional ByVal nTimeout As Integer = ClassDevRS232.m_cnTX_TIMEOUT_IN_SEC)
        Dim arrayByteDataSend() As Byte = Nothing
        Dim arrayByteDataRecv() As Byte = Nothing
        Dim u16Cmd As UInt16 = &H302
        Dim strFuncName As String = "GetConfigurationsGroupDefault"

        If (m_bIDGCmdBusy) Then
            Return
        End If
        m_bIDGCmdBusy = True

        Try
            System_Customized_Command(u16Cmd, strFuncName, arrayByteDataSend, arrayByteDataRecv, Nothing, nTimeout)
            '
            If (bIsRetStatusOK) Then
                'Parser the Group Info
                ClassReaderCommanderParser.Config_ParseFromReaderGroupInfo(m_tagIDGCmdDataInfo, o_sdictTLVs, True)
            Else
                LogLn("[IDG Cmd][Err] V2 Response Status Failed =" + m_tagIDGCmdDataInfo.strGetResponseMsg)
            End If
        Catch ex As Exception
            Dim strGrpID As String = ""
            If (o_sdictTLVs.ContainsKey(tagTLV.TAG_FFE4_or_DFEE2D_GrpID)) Then
                Dim tlv As tagTLV = o_sdictTLVs.Item(tagTLV.TAG_FFE4_or_DFEE2D_GrpID)

                ClassTableListMaster.ConvertFromArrayByte2String(tlv.m_arrayTLVal, strGrpID)
            Else
                strGrpID = "Unknow Group ID, tlv Count = " + o_sdictTLVs.Count.ToString()
            End If
            LogLn("[Err] " + strFuncName + " () = " + ex.Message + ",Group ID= " + strGrpID)
        Finally
            m_bIDGCmdBusy = False
            If (arrayByteDataSend IsNot Nothing) Then
                Erase arrayByteDataSend
            End If
            If (arrayByteDataRecv IsNot Nothing) Then
                Erase arrayByteDataRecv
            End If
        End Try
    End Sub
    '
    Public Overridable Sub GetConfigurationsGroupDefault(ByRef o_arrayByteTLVs As Byte(), Optional ByVal nTimeout As Integer = ClassDevRS232.m_cnTX_TIMEOUT_IN_SEC)
        Dim arrayByteDataSend() As Byte = Nothing
        Dim arrayByteDataRecv() As Byte = Nothing
        Dim u16Cmd As UInt16 = &H302
        Dim strFuncName As String = "GetConfigurationsGroupDefault"

        If (m_bIDGCmdBusy) Then
            Return
        End If
        m_bIDGCmdBusy = True

        Try
            System_Customized_Command(u16Cmd, strFuncName, arrayByteDataSend, o_arrayByteTLVs, Nothing, nTimeout)

            If (bIsRetStatusOK) Then
                'Parser the Group Info
            Else
                LogLn("[IDG Cmd][Err] V2 Response Status Failed =" + m_tagIDGCmdDataInfo.strGetResponseMsg)
            End If
        Catch ex As Exception
            LogLn("[Err] " + strFuncName + " () = " + ex.Message)
        Finally
            m_bIDGCmdBusy = False
            If (arrayByteDataSend IsNot Nothing) Then
                Erase arrayByteDataSend
            End If
            If (arrayByteDataRecv IsNot Nothing) Then
                Erase arrayByteDataRecv
            End If
        End Try
    End Sub
    '
    Public Overridable Sub GetConfigurationsGroupDefault(ByRef o_strTLVsInHexNoCrLfSpace As String, Optional ByVal nTimeout As Integer = ClassDevRS232.m_cnTX_TIMEOUT_IN_SEC)
        Dim arrayByteDataRecv() As Byte = Nothing
        GetConfigurationsGroupDefault(arrayByteDataRecv, nTimeout)
        If (bIsRetStatusOK) Then
            '
            If (arrayByteDataRecv IsNot Nothing) Then
                ClassTableListMaster.ConvertFromArrayByte2String(arrayByteDataRecv, o_strTLVsInHexNoCrLfSpace, False)
                o_strTLVsInHexNoCrLfSpace = o_strTLVsInHexNoCrLfSpace.ToUpper()
                o_strTLVsInHexNoCrLfSpace = Replace(o_strTLVsInHexNoCrLfSpace, vbCr, "")
                o_strTLVsInHexNoCrLfSpace = Replace(o_strTLVsInHexNoCrLfSpace, vbLf, "")
            End If
        Else
            LogLn("[IDG Cmd][Err] V2 Response Status Failed =" + m_tagIDGCmdDataInfo.strGetResponseMsg)
        End If

    End Sub
    '
    Public Overridable Sub GetConfigurationsGroupDefault(ByRef o_configGroup As tagConfigCmdUnitGroup, Optional ByVal nTimeout As Integer = ClassDevRS232.m_cnTX_TIMEOUT_IN_SEC)
        Dim iteratorTLV As KeyValuePair(Of String, tagTLV)
        Dim dictTLV As Dictionary(Of String, tagTLV) = Nothing
        'Dim tlvOp As tagTLVNewOp
        'Dim tlvTmp As tagTLV
        'Dim nIdx, nIdxEnd As Integer
        'Dim arrayByteData() As Byte
        Dim u16Cmd As UInt16 = &H302
        Dim strFuncName As String = "GetConfigurationsGroupDefault -- Group Info Output"

        '
        m_bIsRetStatusOK = False
        If (m_bIDGCmdBusy) Then
            Return
        End If
        m_bIDGCmdBusy = True
        Try
            m_tagIDGCmdDataInfo.Reinit(u16Cmd, m_arraybyteSendBuf, Me, , m_arraybyteSendBufUnknown)
            '-----------------------------------------------------------------[ Send to reader ]-----------------------------------------------------------------
            'tlvOp.PktComposer(tlvTmp, m_tagIDGCmdDataInfo)

            'Compose 1)Protocol Header 2) Command Set 3) Length 4) Command Frame CRC Data
            PktComposerCommandFrameHeaderCmdSetDataLengthCRCData(m_tagIDGCmdDataInfo)
            'tlvTmp.Cleanup()

            '----------------[ Debug Section ]----------------
            If (m_bDebug) Then
                'Note: After PktComposerCommandFrameHeaderCmdSetDataLengthCRCData(), m_tagIDGCmdDataInfo.m_nDataSizeSend includes m_nPktHeader + m_nPktTrail
                'Form01_Main .LogLnDumpData(m_tagIDGCmdDataInfo.m_refarraybyteData, tagIDGCmdData.m_nPktHeader + m_tagIDGCmdDataInfo.m_nDataSizeSend + tagIDGCmdData.m_nPktTrail)
                Form01_Main.LogLnDumpData(m_tagIDGCmdDataInfo.m_refarraybyteData, m_tagIDGCmdDataInfo.m_nDataSizeSend)
            End If
            '----------------[ End Debug Section ]----------------

            'Waiting For Response in GetConfigurationsGroupSingleCbRecv()
            m_refdevConn.SendCommandFrameAndWaitForResponseFrame(AddressOf ClassReaderProtocolBasic.PreProcessCbRecv, m_tagIDGCmdDataInfo, m_nIDGCommanderType, True, nTimeout * 1000)

            'Its' response code = 0 => OK
            If (m_tagIDGCmdDataInfo.bIsResponseOK) Then
                m_bIsRetStatusOK = True

                'Parser the Group Info
                ClassReaderCommanderParser.Config_ParseFromReaderGroupInfo(m_tagIDGCmdDataInfo, o_configGroup)
            Else
                m_bIsRetStatusOK = False
            End If
        Catch ext As TimeoutException
            LogLn("[Err][Timeout] [IDG: " + GetIDGCmdString(u16Cmd) + "] " + strFuncName + "() = " + ext.Message + ",Group ID= " & o_configGroup.m_nGroupID)
        Catch ex As Exception
            LogLn("[Err] [IDG:  " + GetIDGCmdString(u16Cmd) + "] " + strFuncName + "() = " + ex.Message)
        Finally
            If dictTLV IsNot Nothing Then
                For Each iteratorTLV In dictTLV
                    iteratorTLV.Value.Cleanup()
                Next
                'Clean up stuffs
                dictTLV.Clear()
            End If
            m_bIDGCmdBusy = False
        End Try
        '
    End Sub

    '
    Public Overridable Sub GetCashTransactionReaderRiskParameters(ByRef o_dictTLV As SortedDictionary(Of String, tagTLV), Optional ByVal nTimeout As Integer = ClassDevRS232.m_cnTX_TIMEOUT_IN_SEC)
        Try

        Catch ex As Exception
            LogLn("[Err] GetCashTransactionReaderRiskParameters () = " + ex.Message)
        Finally

        End Try
    End Sub

    Public Overridable Sub GetCashbackTransactionReaderRiskParameters(ByRef o_dictTLV As SortedDictionary(Of String, tagTLV), Optional ByVal nTimeout As Integer = ClassDevRS232.m_cnTX_TIMEOUT_IN_SEC)
        Try

        Catch ex As Exception
            LogLn("[Err] GetCashbackTransactionReaderRiskParameters () = " + ex.Message)
        Finally

        End Try
    End Sub

    Public Overridable Sub GetConfigurationsAIDSingle()
        Try

        Catch ex As Exception
            LogLn("[Err] () = " + ex.Message)
        Finally

        End Try
    End Sub

    Public Overridable Sub GetConfigurationsGroupSingle(ByVal nGroupIdx As Integer, ByRef configGroup As tagConfigCmdUnitGroup, Optional ByVal nTimeout As Integer = ClassDevRS232.m_cnTX_TIMEOUT_IN_SEC)
        Try

        Catch ex As Exception
            LogLn("[Err] () = " + ex.Message)
        Finally

        End Try
    End Sub

    Public Overridable Sub GetCRLMass(ByRef o_dictCRLCmdAddList As Dictionary(Of String, tagCRLCmdUnit), Optional ByVal nTimeout As Integer = ClassDevRS232.m_cnTX_TIMEOUT_IN_SEC)
        Try

        Catch ex As Exception
            LogLn("[Err] () = " + ex.Message)
        Finally

        End Try
    End Sub

    Public Overridable Sub GetCRLSingle(ByRef tagCRLSingle As tagCRLCmdUnitSingle, Optional ByVal nTimeout As Integer = ClassDevRS232.m_cnTX_TIMEOUT_IN_SEC)
        Try

        Catch ex As Exception
            LogLn("[Err] () = " + ex.Message)
        Finally

        End Try
    End Sub

    'Public Overridable Sub SetCAPublicKeySingle(ByRef tagCAPKInfo As ClassCAPKUnit, Optional ByVal nTimeout As Integer = ClassDevRS232.m_cnTX_TIMEOUT_IN_SEC)
    '    Try

    '    Catch ex As Exception
    '        LogLn("[Err] () = " + ex.Message)
    '    Finally

    '    End Try
    'End Sub

    Public Overridable Sub SetConfigurationsAIDSingle(ByRef tagAIDInfo As tagConfigCmdUnitAID, Optional ByVal nTimeout As Integer = ClassDevRS232.m_cnTX_TIMEOUT_IN_SEC)
        Try

        Catch ex As Exception
            LogLn("[Err] SetConfigurationsAIDSingle () = " + ex.Message)
        Finally

        End Try
    End Sub

    Public Overridable Sub SetCashTransactionReaderRiskParameters(ByVal dictTLV As SortedDictionary(Of String, tagTLV), Optional ByVal nTimeout As Integer = ClassDevRS232.m_cnTX_TIMEOUT_IN_SEC)
        Try

        Catch ex As Exception
            LogLn("[Err] SetCashTransactionReaderRiskParameters () = " + ex.Message)
        Finally

        End Try
    End Sub
    Public Overridable Sub SetCashbackTransactionReaderRiskParameters(ByVal dictTLV As SortedDictionary(Of String, tagTLV), Optional ByVal nTimeout As Integer = ClassDevRS232.m_cnTX_TIMEOUT_IN_SEC)
        Try

        Catch ex As Exception
            LogLn("[Err] SetCashbackTransactionReaderRiskParameters () = " + ex.Message)
        Finally

        End Try
    End Sub



    Public Overridable Sub SetCRLSingle(ByRef tagCRLSingle As tagCRLCmdUnitSingle, Optional ByVal nTimeout As Integer = ClassDevRS232.m_cnTX_TIMEOUT_IN_SEC)
        Try

        Catch ex As Exception
            LogLn("[Err] () = " + ex.Message)
        Finally

        End Try
    End Sub

    Public Overridable Sub DelConfigurationsAIDSingle(ByRef tlv As tagTLV, Optional ByVal nTimeout As Integer = ClassDevRS232.m_cnTX_TIMEOUT_IN_SEC)
        Try

        Catch ex As Exception
            LogLn("[Err] () = " + ex.Message)
        Finally

        End Try
    End Sub

    Public Overridable Sub DelConfigurationsAIDMass(ByRef dictTLV As Dictionary(Of String, tagTLV), Optional ByVal nTimeout As Integer = ClassDevRS232.m_cnTX_TIMEOUT_IN_SEC)
        Try

        Catch ex As Exception
            LogLn("[Err] () = " + ex.Message)
        Finally

        End Try
    End Sub

    Public Overridable Sub DelConfigurationsGroupMass()
        Try

        Catch ex As Exception
            LogLn("[Err] () = " + ex.Message)
        Finally

        End Try
    End Sub

    Public Overridable Sub DelConfigurationsGroupSingle()
        Try

        Catch ex As Exception
            LogLn("[Err] () = " + ex.Message)
        Finally

        End Try
    End Sub

    Public Overridable Sub DelCRLSingle(ByRef tagCRLSingle As tagCRLCmdUnitSingle, Optional ByVal nTimeout As Integer = ClassDevRS232.m_cnTX_TIMEOUT_IN_SEC)
        Try

        Catch ex As Exception
            LogLn("[Err] () = " + ex.Message)
        Finally

        End Try
    End Sub

    Public Overridable Sub DelCRLMass(Optional ByVal nTimeout As Integer = ClassDevRS232.m_cnTX_TIMEOUT_IN_SEC)
        Try

        Catch ex As Exception
            LogLn("[Err] () = " + ex.Message)
        Finally

        End Try
    End Sub

    '
    Public Overridable Sub GetCAPublicKeyRIDMass()
        Try

        Catch ex As Exception
            LogLn("[Err] () = " + ex.Message)
        Finally

        End Try
    End Sub

    'For Example
    'INPUT : strRID = "A0 00 00 00 04"
    'OUTPUT:   "00" --> (R
    '                  "02"
    '                   "0A"
    'Public Overridable Sub GetCAPublicKeyRIDSingle(ByVal arrayByteRID() As Byte, ByVal byteKeyIndex As Byte, ByRef O_tagCAPKInfo As ClassCAPKUnit, Optional ByVal nTimeout As Integer = ClassDevRS232.m_cnTX_TIMEOUT_IN_SEC)
    '    Try

    '    Catch ex As Exception
    '        LogLn("[Err] () = " + ex.Message)
    '    Finally

    '    End Try
    'End Sub

    'IDG CMD : D0-04
    Public Overridable Sub DelCAPublicKeyRIDSingle(ByVal arrayByteRID() As Byte, ByVal byteKeyIndex As Byte, Optional ByVal nTimeout As Integer = ClassDevRS232.m_cnTX_TIMEOUT_IN_SEC)
        Try

        Catch ex As Exception
            LogLn("[Err] () = " + ex.Message)
        Finally

        End Try
    End Sub

    Public Overridable Sub DelCAPublicKeyRIDsAll(Optional ByVal nTimeout As Integer = ClassDevRS232.m_cnTX_TIMEOUT_IN_SEC)
        Try

        Catch ex As Exception
            LogLn("[Err] DelCAPublicKeyRIDsAll() = " + ex.Message)
        Finally

        End Try
    End Sub

    Public Overridable Sub GetCAPublicKeyGetAllRIDs(ByRef o_RIDList As List(Of Byte()), Optional ByVal nTimeout As Integer = ClassDevRS232.m_cnTX_TIMEOUT_IN_SEC)
        Try

        Catch ex As Exception
            LogLn("[Err] GetCAPublicKeyGetAllRIDs () = " + ex.Message)
        Finally

        End Try
    End Sub

    Public Overridable Sub GetCAPublicKeyHashSingle()
        Try

        Catch ex As Exception
            LogLn("[Err] () = " + ex.Message)
        Finally

        End Try
    End Sub

    '
    Enum TAG_BURST_MODE As Byte
        _x00_OFF = &H0
        _x01_ON
        _x02_AUTOEXIT
    End Enum
    Public Overridable Sub SetConfigurationBurstMode(byteBurstMode As TAG_BURST_MODE, Optional ByVal nTimeout As Integer = ClassDevRS232.m_cnTX_TIMEOUT_IN_SEC)
        Dim arrayByteDataSend As Byte()
        Dim arrayByteDataRecv As Byte() = Nothing
        Dim strFuncName As String = "SetConfigurationBurstMode"
        Dim u16Cmd As UInt16 = &H400

        If (m_bIDGCmdBusy) Then
            Return
        End If
        m_bIDGCmdBusy = True
        Try
            'If (bBurstMode) Then
            '    arrayByteDataSend = New Byte() {&HFF, &HF7, &H1, &H1}
            'Else
            '    arrayByteDataSend = New Byte() {&HFF, &HF7, &H1, &H0}
            'End If
            arrayByteDataSend = New Byte() {&HFF, &HF7, &H1, CType(byteBurstMode, Byte)}

            System_Customized_Command(u16Cmd, strFuncName, arrayByteDataSend, arrayByteDataRecv, Nothing, nTimeout)

            If (bIsRetStatusOK) Then

            End If
        Catch ex As Exception
            Throw 'ex
        Finally
            m_bIDGCmdBusy = False
        End Try
    End Sub

End Class
