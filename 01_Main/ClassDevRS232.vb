﻿'==================================================================================
' File Name: ClassDevRS232.vb
' Description: The ClassDevRS232 is as Non-inherit Class of RS232 object
' Who use it as component: ClassReaderCommander
' Similar Class: ClassDevUsbHID
'
' Revision:
' -----------------------[ Initial Version ]-----------------------
' 2014 May 29, by goofy_liu@idtechproducts.com.tw
'==================================================================================
Imports System
Imports System.IO.Ports
Imports System.Threading
Imports Main.ClassReaderProtocolBasic

''
'Public Class Exception
'    Inherits Exception

'    Sub New(strMsg As String)
'        ' TODO: Complete member initialization 
'        MyBase.New(strMsg)
'    End Sub

'    '


'End Class
'
Public Class ClassDevRS232
    Inherits ClassDev

    Implements IDisposable

    '
    '=================[Private Variables Area]===================
    Private m_devobjSerialPort As SerialPort
    '
    '=================[Public Variables Area]===================
    'Baud Rate Auto Means Do Nothing
    Enum EnumBUAD_RATE As Integer
        _UNKNOWN = 0
        _9600 = 9600
        _19200 = 19200
        _38400 = 38400
        _57600 = 57600
        _115200 = 115200
    End Enum
    '
    Public Const m_cstrClassName As String = "ClassDevRS232"
    'For PISCES Case, we need Rx Buffer up to 16K~20K for Signature Rx PNG Max Data Accommodation
    'Use 20K as Safety Buffer Size.
    Public Const m_cnRX_BUF_MAX As Integer = 20480 '=20K; 4096 ' RX BUF SIZE = 4K
    Public Const m_cnTX_BUF_MAX As Integer = 4096 ' RX BUF SIZE = 4K
    Public Const m_cnRX_TIMEOUT As Integer = 10000 ' RX Timeout 10 secs
    Public Const m_cnTX_TIMEOUT As Integer = 10000 ' TX Timeout 10 secs
    Public Const m_cnRX_TIMEOUT_IN_SEC As Integer = 10 ' RX Timeout 10 secs
    Public Const m_cnTX_TIMEOUT_IN_SEC As Integer = 10 ' TX Timeout 10 secs
    Public Const m_cnTX_TIMEOUT_IN_SEC_60 As Integer = 60 ' TX Timeout 10 secs

    '
    Public Const m_c_strCOM As String = "COM"
    Public Const m_c_strBaudrate As String = "Baudrate"

    '
    '==================[Constructor / Destructor Area ]=====================
    Sub New(refwinformMain As Form01_Main)
        MyBase.New(refwinformMain)

        '------------------[ Device RS232 Stuffs ]------------------
        m_devobjSerialPort = New SerialPort()
        'Add RS232 Events' Handler
        AddHandler m_devobjSerialPort.DataReceived, AddressOf Recv
        AddHandler m_devobjSerialPort.ErrorReceived, AddressOf RecvErr
        'AddHandler m_devobjSerialPort.PinChanged, AddressOf PinChg
        'Increase up TX/RX Buffer Size
        m_devobjSerialPort.ReadBufferSize = m_cnRX_BUF_MAX
        m_devobjSerialPort.WriteBufferSize = m_cnTX_BUF_MAX
        'Set TX/RX Timeout
        m_devobjSerialPort.ReadTimeout = m_cnRX_TIMEOUT
        m_devobjSerialPort.WriteTimeout = m_cnTX_TIMEOUT

    End Sub

    ReadOnly Property strCOMPort As String
        Get
            Return m_devobjSerialPort.PortName
        End Get
    End Property
    '
    '==================[*Debugging* Code = Public Sub / Func Area ]=====================

    '==================[Public Sub / Func Area ]=====================
    '
    Public Overrides Sub Cleanup()
        MyBase.Cleanup()
        '-------[ Close Device RS232 Serial Port ]-------
        If m_devobjSerialPort.IsOpen Then
            m_devobjSerialPort.Close()
            'm_devobjSerialPort.Dispose() ' Do this in IDispose Interface
        End If


    End Sub
    '
    Public Sub Config(Optional ByVal strPort As String = "COM1", Optional ByVal nBaudRate As EnumBUAD_RATE = EnumBUAD_RATE._115200)
        Try
            '----------------------------------------------------
            'Config RS232 Port Name & Selection of Buad-rate
            With m_devobjSerialPort
                .PortName = strPort
                .BaudRate = nBaudRate
                .DataBits = 8
                .Parity = Parity.None
                .StopBits = StopBits.One
                .Handshake = Handshake.None
            End With

        Catch ex As Exception
            LogLn("[Err] " + m_cstrClassName + " Config() = " + ex.Message)
        End Try

    End Sub


    Public Overrides Sub Open()
        Try
            m_devobjSerialPort.Open()

        Catch ex As Exception
            LogLn("[Err] " + m_cstrClassName + " Open() = " + ex.Message)
        End Try
    End Sub

    Public Overrides Sub Close()
        Try
            m_devobjSerialPort.Close()
        Catch ex As Exception
            LogLn("[Err] " + m_cstrClassName + " Close() = " + ex.Message)
        End Try
    End Sub

    ReadOnly Property bIsOpen As Boolean
        Get
            Return m_devobjSerialPort.IsOpen()
        End Get
    End Property

    '
    Private Sub SendDebugModeRecv(ByRef arraybyte As Byte(), ByVal nDataSize As Integer)
        Try
            Dim nRemain As Integer = nDataSize
            'Dim nPacketSize As Integer
            'Dim nDataLen As Integer = arraybyte.Count
            m_devobjSerialPort.DiscardOutBuffer()
            m_devobjSerialPort.DiscardInBuffer()

            ''Set RS232 Write Timeout

            ''Tx Dat
            'While (nRemain > 0)
            '    If (m_cnTX_BUF_MAX < nRemain) Then
            '        nPacketSize = m_cnTX_BUF_MAX
            '    Else
            '        nPacketSize = nRemain
            '    End If
            '    m_devobjSerialPort.Write(arraybyte, nDataSize - nRemain, nPacketSize)
            '    nRemain = nRemain - nPacketSize

            '    ' Wait Till All Data Are Send Out
            '    While m_devobjSerialPort.BytesToWrite > 0
            '        'Wait
            '        Thread.Sleep(10)
            '    End While
            'End While
        Catch ex As Exception
            LogLn("[Err] " + m_cstrClassName + " Send() = " + ex.Message)
        End Try
    End Sub

    '==========================[ Stress Test ]==========================
    ' For Poll For Toke, Get ATR, ICC Power Down
    '2016 April 12, goofy_liu@idtechproducts.com.tw
    Private Shared s_dictNEOIDGResp As SortedDictionary(Of UInt16, Byte()) = New SortedDictionary(Of UShort, Byte()) From {{&H2C12, {&H55, &H55, &H55, &H55, &H55, &H55, &H55, &H55, &H55, &H55, &H55, &H55, &H55, &H55, &H55, &H55,
 &H66, &H56, &H69, &H56, &H4F, &H74, &H65, &H63, &H68, &H32, &H0, &H2C, &H0, &H0, &H13, &H3B,
 &H6F, &H0, &H0, &H80, &H31, &HE0, &H6B, &H4, &H31, &H5, &H2, &HAB, &H55, &H55, &H55, &H55,
 &H55, &H55, &HF8, &HD9}},
 {&H2C18, {&H55, &H55, &H55, &H55, &H55, &H55, &H55, &H55, &H55, &H55, &H55, &H55, &H55, &H55, &H55, &H55,
 &H66, &H56, &H69, &H56, &H4F, &H74, &H65, &H63, &H68, &H32, &H0, &H2C, &H0, &H0, &H0, &H1C,
 &H9B}},
{&H2C02, {&H55, &H55, &H55, &H55, &H55, &H55, &H55, &H55, &H55, &H55, &H55, &H55, &H55, &H55, &H55, &H55,
 &H66, &H56, &H69, &H56, &H4F, &H74, &H65, &H63, &H68, &H32, &H0, &H2C, &H0, &H0, &H8, &H1,
 &H5, &H86, &HE, &HF0, &H7D, &H0, &H0, &H7F, &H58}}
}

    Public Shared Function SendCommandAndResponseStressTest(ByRef arraybyteTx As Byte(), ByVal u32DataSizeTx As UInt32, ByRef o_arraybyteRx As Byte(), ByRef io_u32DataSizeRx As UInt32) As Boolean
        Dim bProcess As Boolean = False
        Dim wCmd As UInt16 = (CType(arraybyteTx(ClassReaderProtocolBasic.EnumIDGCmdOffset.Protocol1_OffSet_CmdMain_Len_1B), UInt16) << 8) Or arraybyteTx(ClassReaderProtocolBasic.EnumIDGCmdOffset.Protocol1_OffSet_CmdSub_Len_1B)
        'Dim pForm02 As Form02_StartupSelection = Form02_StartupSelection.GetInstance()

        '
        io_u32DataSizeRx = 0
        '
        'If (Not pForm02.bIsStressTest) Then
        'Return False
        'End If

        If (Not (s_dictNEOIDGResp Is Nothing)) Then
            'Case 02 - Poll For Token, Get ATR
            If (s_dictNEOIDGResp.ContainsKey(wCmd)) Then
                Dim aryByte As Byte() = s_dictNEOIDGResp(wCmd)
                o_arraybyteRx = aryByte.Clone()
                bProcess = True
            End If
        End If

        If (bProcess) Then
            io_u32DataSizeRx = o_arraybyteRx.Count
        End If
        '
        Return bProcess
    End Function
    '==========================[ End of Stress Test ]=========================='
    '
    Public Overrides Sub SendCommandAndResponse(ByRef arraybyteTx As Byte(), ByVal u32DataSizeTx As UInt32, ByRef o_arraybyteRx As Byte(), ByRef io_u32DataSizeRx As UInt32, ByVal u32TimeoutMS As UInt32, Optional ByVal bIsDiscardedTxRx As Boolean = True)
        Dim nRemain As Integer = u32DataSizeTx
        Dim nPacketSize As Integer
        Dim u32TimeoutAcct As UInt32
        Dim u32TimeCurrent As UInt32
        Dim u32RxLenIfNew As UInt32 = 1024

        Try
            If (bIsDiscardedTxRx) Then
                m_devobjSerialPort.DiscardOutBuffer()
                m_devobjSerialPort.DiscardInBuffer()
            End If


            m_bTimeout = False
            'Tx Dat
            While (nRemain > 0)
                If (m_cnTX_BUF_MAX < nRemain) Then
                    nPacketSize = m_cnTX_BUF_MAX
                Else
                    nPacketSize = nRemain
                End If
                m_devobjSerialPort.Write(arraybyteTx, u32DataSizeTx - nRemain, nPacketSize)
                nRemain = nRemain - nPacketSize

                ' Wait Till All Data Are Send Out
                While m_devobjSerialPort.BytesToWrite > 0
                    'Wait
                    Thread.Sleep(10)
                    '
                    If m_callbackExternalRecvProcedure IsNot Nothing Then
                        'Call IDG Command Packet Recv Packet Handler
                        m_callbackExternalRecvProcedure(m_devobjSerialPort, m_callbackParametersTagIDGCmdData, True) 'callback function
                    End If

                    Application.DoEvents()
                End While
            End While

            ' Waiting Data In, Rx
            u32TimeoutAcct = 0
            While ((m_devobjSerialPort.BytesToRead = 0) And (u32TimeoutAcct < u32TimeoutMS))
                u32TimeCurrent = Environment.TickCount
                'Wait
                Thread.Sleep(100)
                '
                If m_callbackExternalRecvProcedure IsNot Nothing Then
                    'Call IDG Command Packet Recv Packet Handler
                    m_callbackExternalRecvProcedure(m_devobjSerialPort, m_callbackParametersTagIDGCmdData, True) 'callback function
                End If
                '
                Application.DoEvents()
                '
                u32TimeoutAcct = u32TimeoutAcct + (Environment.TickCount - u32TimeCurrent)
            End While


            '------------------[ Read Data ]---------------------
            'If (u32TimeoutAcct < u32TimeoutMS) Then

            'u32TimeoutAcct = 0
            If (o_arraybyteRx Is Nothing) Then
                'o_arraybyteRx = New Byte(u32RxLenIfNew - 1) {}
                'io_u32DataSizeRx = u32RxLenIfNew
                'Using Default Buffer, Can't be Release by Caller
                o_arraybyteRx = m_arrayByteBufferRxDirect
                io_u32DataSizeRx = m_arrayByteBufferRxDirect.Length
            End If
            nRemain = io_u32DataSizeRx
            While ((m_devobjSerialPort.BytesToRead > 0) And (u32TimeoutAcct < u32TimeoutMS) And (nRemain > 0))
                u32TimeCurrent = Environment.TickCount
                'Recv Buffer Size Boundary Check
                If (m_cnTX_BUF_MAX < nRemain) Then
                    nPacketSize = m_cnTX_BUF_MAX
                Else
                    nPacketSize = nRemain
                End If
                'Set Remain Data to Rx In Data Size if must
                If (m_devobjSerialPort.BytesToRead < nPacketSize) Then
                    nPacketSize = m_devobjSerialPort.BytesToRead
                End If
                '
                m_devobjSerialPort.Read(o_arraybyteRx, io_u32DataSizeRx - nRemain, nPacketSize)
                nRemain = nRemain - nPacketSize
                '
                If m_callbackExternalRecvProcedure IsNot Nothing Then
                    'Call IDG Command Packet Recv Packet Handler
                    m_callbackExternalRecvProcedure(m_devobjSerialPort, m_callbackParametersTagIDGCmdData, True) 'callback function
                End If
                '
                Application.DoEvents()
                '
                u32TimeoutAcct = u32TimeoutAcct + (Environment.TickCount - u32TimeCurrent)
            End While

            '-------------[ Get Rx Data Size ]--------------
            io_u32DataSizeRx = io_u32DataSizeRx - nRemain

            If (u32TimeoutAcct >= u32TimeoutMS) Then
                m_bTimeout = True
            End If
            'End If
        Catch ex As Exception
            LogLn("[Err] " + m_cstrClassName + " Send() = " + ex.Message)
        End Try
    End Sub

    'Tx Data ONLY here
    Public Overrides Sub SendCommandAndResponse(ByRef arraybyte As Byte(), ByVal u32DataSize As UInt32, ByVal u32TimeoutMS As UInt32)
        Try
            Dim nRemain As Integer = u32DataSize
            Dim nPacketSize As Integer
            'Dim nDataLen As Integer = arraybyte.Count
            m_devobjSerialPort.DiscardOutBuffer()
            m_devobjSerialPort.DiscardInBuffer()

            'Set RS232 Write Timeout

            'Tx Dat
            While (nRemain > 0)
                If (m_cnTX_BUF_MAX < nRemain) Then
                    nPacketSize = m_cnTX_BUF_MAX
                Else
                    nPacketSize = nRemain
                End If
                m_devobjSerialPort.Write(arraybyte, u32DataSize - nRemain, nPacketSize)
                nRemain = nRemain - nPacketSize

                ' Wait Till All Data Are Send Out
                While m_devobjSerialPort.BytesToWrite > 0
                    'Wait
                    Thread.Sleep(10)
                End While
            End While
        Catch ex As Exception
            LogLn("[Err] " + m_cstrClassName + " Send() = " + ex.Message)
        End Try
    End Sub
    '
    Public Overrides Sub Recv(ByVal sender As Object, ByVal e As SerialDataReceivedEventArgs)
        Dim devobjRS232 As SerialPort = CType(sender, SerialPort)

        'Handles serial port data received events
        'UpdateFormDelegate1 = New UpdateFormDelegate(AddressOf UpdateDisplay)
        'nDataLen = SerialPort1.BytesToRead 'find number of bytes in buf
        'comBuffer = New Byte(nDataLen - 1) {} 're dimension storage buffer
        'SerialPort1.Read(comBuffer, 0, nDataLen) 'read data from the buffer
        If m_callbackExternalRecvProcedure IsNot Nothing Then
            'Call IDG Command Packet Recv Packet Handler
            m_callbackExternalRecvProcedure(m_devobjSerialPort, m_callbackParametersTagIDGCmdData, True) 'callback function

            'Sync tagIDGCmdInfo Parameter (i.e. = m_callbackParameters)
            If m_callbackExternalRecvProcedureSyncParameter IsNot Nothing Then
                m_callbackExternalRecvProcedureSyncParameter(m_callbackParametersTagIDGCmdData)
            End If

        End If

    End Sub

    Public Shared Sub EnumeratePorts(ByRef strlstRS232Port As String())
        Try
            strlstRS232Port = SerialPort.GetPortNames()

        Catch ex As Exception
            LogLnThreadSafe("[Err] " + m_cstrClassName + " EnumeratePorts() = " + ex.Message)
        End Try
    End Sub

    Private Sub RecvErr(sender As Object, e As SerialErrorReceivedEventArgs)
        Try
            Const cstrRecvErrMsgHeader As String = "[RS232 Recv Err]"
            Select Case e.EventType
                Case SerialError.Frame
                    LogLnThreadSafe(cstrRecvErrMsgHeader + " Hardware Detects A Frame Error")
                Case SerialError.Overrun
                    LogLnThreadSafe(cstrRecvErrMsgHeader + " A Char-Buffer Overrun has occurred. The Next Char is lost")
                Case SerialError.RXOver
                    LogLnThreadSafe(cstrRecvErrMsgHeader + " An input buffer overflow has occurred")
                Case SerialError.RXParity
                    LogLnThreadSafe(cstrRecvErrMsgHeader + " The hardware detected a parity error")
                Case SerialError.TXFull
                    LogLnThreadSafe(cstrRecvErrMsgHeader + " The Application Tried to transmit a char, but the output buffer was full")
                Case Else
                    LogLnThreadSafe(cstrRecvErrMsgHeader + " Unknown RecvErr Event = " & e.EventType)
            End Select
            '
            If m_callbackExternalRecvProcedure IsNot Nothing Then
                m_callbackExternalRecvProcedure(m_devobjSerialPort, m_callbackParametersTagIDGCmdData, False) 'callback function
            End If
        Catch ex As Exception
            LogLnThreadSafe("[Err] " + m_cstrClassName + " RecvErr() = " + ex.Message)
        End Try
    End Sub

    Private Sub PinChg(sender As Object, e As SerialPinChangedEventArgs)
        Try
            Const cstrEvtPinChg As String = "[RS232 Ping Change]"
            Select Case e.EventType
                Case SerialPinChange.Break
                    LogLnThreadSafe(cstrEvtPinChg + " A break was detected on INPUT ")
                Case SerialPinChange.CDChanged
                    LogLnThreadSafe(cstrEvtPinChg + " The CDC signal has changed")
                Case SerialPinChange.CtsChanged
                    LogLnThreadSafe(cstrEvtPinChg + " The CTS signal has changed")
                Case SerialPinChange.DsrChanged
                    LogLnThreadSafe(cstrEvtPinChg + " The DSR signal has changed")
                Case SerialPinChange.Ring
                    LogLnThreadSafe(cstrEvtPinChg + " A Ring Indicator was detected")
                Case Else
                    LogLnThreadSafe(cstrEvtPinChg + " Unknown Ping Change Event = " & e.EventType)
            End Select

        Catch ex As Exception
            LogLnThreadSafe("[Err] " + m_cstrClassName + " RecvErr() = " + ex.Message)
        End Try
    End Sub
   
    'Private m_bTimeout As Boolean
    'ReadOnly Property bIsStatusTimeout As Boolean
    '    Get
    '        Return m_bTimeout
    '    End Get
    'End Property

    Private Sub SendCommandFrameAndWaitForResponseFrameNormal(ByRef callbackExternalRecvProcedure As DelegatecallbackExternalRecvProcedure, ByRef refIDGCmdData As tagIDGCmdData, Optional ByVal bWait As Boolean = True, Optional ByVal u32TimeoutMS As UInt32 = ClassDevRS232.m_cnTX_TIMEOUT)
        'Dim bDebug As Boolean = True
        Try
            Dim nTicksCountDelay As Integer = 0
            Dim nTicksCountTO As Integer
            Dim nTicksCountStartSuitableDeplay As Integer

            m_bTimeout = False
            '
            Dim strMsgCmdSub As String = refIDGCmdData.strCmdNEOFormat
            Dim strWaitStatus As String
            If bWait Then
                strWaitStatus = "Wait Response"
            Else
                strWaitStatus = "Exit Without Wait"
            End If

            ' Set to Command Activate Mode
            refIDGCmdData.CommInterruptFSMAct()


            'Config Receiver Call Back Function & Reset Response Value to default(non-use state)
            ConfigCallbackRecv(callbackExternalRecvProcedure, refIDGCmdData)
            '
            refIDGCmdData.DefaultResponse()

            If (m_bLogTurnOnOff) Then
                LogLn("<<<<<<<<<< Tx Dat , " + Form01_Main.GetTimeStampYear2Millisec() + " >>>>>>>>>>")
                Form01_Main.LogLnDumpData(refIDGCmdData.m_refarraybyteData, refIDGCmdData.m_nDataSizeSend)
                LogLn(" ")
            End If

            '=======================[ Cleanup RS232 In/Out Buffer Before Send/Recv Process ]=======================
            'm_devobjSerialPort.DiscardInBuffer(), SendCommandAndResponse() calls these paired functions
            'm_devobjSerialPort.DiscardOutBuffer()

#If 1 Then
            '--------------------[ Stress Test Check ]-------------------
            ' 2016 April 12, goofy_liu@idtechproducts.com.tw
            With refIDGCmdData
                If (m_bIsStressTest) Then
                    'Return
                    ClassDevRS232.SendCommandAndResponseStressTest(.m_refarraybyteData, .m_nDataSizeSend, .m_refarraybyteData, .m_nDataSizeRecv)
                    '-------------[ Fill Up some Rx Information ]--------------
                    ClassReaderProtocolBasic.PreProcessCbRecv_NormalMode_StressTest(refIDGCmdData)
                Else
                    SendCommandAndResponse(.m_refarraybyteData, .m_nDataSizeSend, u32TimeoutMS)
                End If
            End With
            '--------------------[ End of Stress Test Check ]-------------------
#Else
            SendCommandAndResponse(refIDGCmdData.m_refarraybyteData, refIDGCmdData.m_nDataSizeSend, u32TimeoutMS)
#End If


            'LogLn("SendCommandFrameAndWaitForResponseFrame() 4")
            '----------------------[ Waiting for Response Frame ]----------------------
            If bWait Then
                'Get Start System Ticks
                nTicksCountTO = Environment.TickCount + u32TimeoutMS
                nTicksCountStartSuitableDeplay = nTicksCountTO
                '====================[Receiving While Loop]====================

                '
                While Not IsResponseFrameReady(refIDGCmdData)

                    m_pMF.CalledByPreProcessCbRecv_InterruptByExternalStatusUpdate(refIDGCmdData)

                    '================================================================================
                    'Make Other Windows Control Events Available
                    Application.DoEvents()

                    If (refIDGCmdData.CallByIDGCmdTimeOutChk_IsCommIDGCancelStop) Then
                        ' This is triggered by "Transaction Stop" Button
                        Exit While
                    End If

                    If (refIDGCmdData.CallByIDGCmdTimeOutChk_IsCommTimeOutCancelStop) Then
                        ' This is triggered by Transaction Timer
                        m_bTimeout = True
                        Exit While
                    End If
#If 1 Then
                    If (nTicksCountTO <= Environment.TickCount) Then
                        '2018 April 25, set for timeout internal
                        refIDGCmdData.CallByIDGCmdTimeOutChk_SetTimeoutEvent()
                        m_bTimeout = True
                        Exit While
                    End If
#End If
                    If (refIDGCmdData.bIsInterruptedByExternal) Then
                        ' This is triggered by User to Press "Cancel" Button
                        Exit While
                    End If
                End While
                '====================[End of Receiving While Loop]====================

                If (refIDGCmdData.bIsInterruptedByExternal) Then
                    'User press a Cancel Button to stop IDG Commands
                    Throw New Exception("[Warning] Cancel Button Pressed to Stop RS232 Receiving Handler")
                End If
                '
                If refIDGCmdData.CallByIDGCmdTimeOutChk_IsCommIDGCancelStop Then
                    Throw New Exception("[Warning] Stopping RS232 Recv Handler")
                End If
                '
                If m_bTimeout Then
                    strMsgCmdSub = String.Format("[Err][Timeout] Cmd Frame, Cmd ={0}", strMsgCmdSub)
                    Throw New TimeoutException(strMsgCmdSub)
                End If
                '
                If (Not m_bIsEngMode_HWTest) Then
                    If ((Not m_bLogTurnOnOff) Or (Not refIDGCmdData.bIsResponseOK)) Then
                        LogLn(strMsgCmdSub + " << RxSts = " + refIDGCmdData.strGetResponseMsg + "( " & refIDGCmdData.byteGetResponseCode & " )")
                    End If
                End If
                '
                If (m_bLogTurnOnOff) Or (Not refIDGCmdData.bIsResponseOK) Then
                    LogLn(">>>>>>>>>> Rx Dat, " + Form01_Main.GetTimeStampYear2Millisec() + " <<<<<<<<<<")
                    With refIDGCmdData
                        Form01_Main.LogLnDumpData(.m_refarraybyteData.Skip(.m_nDataRecvOffSkipBytes - .m_nDataSizeRecv).ToArray(), .m_nDataSizeRecv)
                    End With
                    LogLn(" ")
                End If
            Else
                ' Do Nothing
                refIDGCmdData.byteSetResponseCode = ClassReaderProtocolBasic.EnumSTATUSCODE2.x00OK
            End If
        Catch ex As Exception
            Throw 'ex
        Finally
            If ((refIDGCmdData.CallByIDGCmdTimeOutChk_IsCommIDGCancelStop) Or (refIDGCmdData.CallByIDGCmdTimeOutChk_IsCommTimeOutCancelStop)) Then
                refIDGCmdData.CallByIDGCmdTimeOutChk_SetTimeoutEvent()
            Else
                If ((Not refIDGCmdData.CallByIDGCmdTimeOutChk_IsCommIdle) And (m_bLogTurnOnOff)) Then
                    'LogLn("[Dev RS232][Warning] Not in Stopping Status or No instance stop Receiving Procedure, must have some exception, FSM = " & refIDGCmdData.CallByIDGCmdTimeOutChk_GetStrCommFSM) ' 2016 July 06, remove this msg
                End If
            End If
            'Start Time Out Mechanism in Main Form
            m_refwinformMain.IDGCmdTimeOutRealTimeResetAndStart(u32TimeoutMS)
            refIDGCmdData.CommInterruptFSMIdle()

            '2017 July 27, correction skipped offset
            refIDGCmdData.ParserDataUpdateBeforeRetrieve()
        End Try
    End Sub
    '

    Private Sub SendCommandFrameAndWaitForResponseFrameNormal_Pure(ByRef callbackExternalRecvProcedure As DelegatecallbackExternalRecvProcedure, ByVal arrayByteTx As Byte(), ByRef o_arrayByteRx As Byte(), ByRef o_u32RxLen As UInt32, Optional ByVal bWait As Boolean = True, Optional ByVal u32TimeoutMS As UInt32 = ClassDevRS232.m_cnTX_TIMEOUT)
        'Dim bDebug As Boolean = True
        Try
            Dim nTicksCountDelay As Integer = 0
            'Dim nTicksCountStart As Integer
            'Dim nTicksCountStartSuitableDeplay As Integer
            Dim u32TxLen As UInt32
            m_bTimeout = False
            '
            Dim strMsg As String = "Pure Tx/Rx, No NEO/IDG Protocol"
            Dim strWaitStatus As String
            If bWait Then
                strWaitStatus = "Wait Response"
            Else
                strWaitStatus = "Exit Without Wait"
                'Set Timeout value to ZERO if no wait
                u32TimeoutMS = 0
            End If
            '
            If (m_bLogTurnOnOff) Then
                LogLn("<<<<<<<<<< Tx Dat >>>>>>>>>>")
                If (arrayByteTx IsNot Nothing) Then
                    Form01_Main.LogLnDumpData(arrayByteTx, arrayByteTx.Length)
                Else
                    LogLn("Tx Data = Nothing")
                End If
                LogLn(" ")
            End If

            '=======================[ Cleanup RS232 In/Out Buffer Before Send/Recv Process ]=======================
            'm_devobjSerialPort.DiscardInBuffer(), SendCommandAndResponse() calls these paired functions
            'm_devobjSerialPort.DiscardOutBuffer()
            If (arrayByteTx IsNot Nothing) Then
                u32TxLen = arrayByteTx.Length
            Else
                u32TxLen = 0
            End If

            If (o_arrayByteRx IsNot Nothing) Then
                o_u32RxLen = o_arrayByteRx.Length
            Else
                o_u32RxLen = 0
            End If
            '
            'Config Receiver Call Back Function & Reset Response Value to default(non-use state)
            ConfigCallbackRecv(callbackExternalRecvProcedure, Nothing)
            '
            SendCommandAndResponse(arrayByteTx, u32TxLen, o_arrayByteRx, o_u32RxLen, u32TimeoutMS)
            If (o_u32RxLen < 1) Then
                'LogLn("Rx Data = Nothing") 'Debug Purpose
            ElseIf (m_bLogTurnOnOff) Or (Not (o_arrayByteRx(ClassReaderProtocolBasic.EnumIDGCmdOffset.Protocol2_OffSet_Rx_RetStatusCode_LSB_Len_1B) = ClassReaderProtocolBasic.EnumSTATUSCODE1.x00OK)) Then
                LogLn(">>>>>>>>>> Rx Data <<<<<<<<<<")
                Form01_Main.LogLnDumpData(o_arrayByteRx, o_u32RxLen)
                LogLn(" ")
            End If
            '
        Catch ex As Exception
            Throw 'ex
        Finally

        End Try
    End Sub
    '
    'Note 01 - No Discard Tx/Rx Data
    'Note 02 - If o_arrayByteRx is not nothing, o_u32RxLen won't be set to length of o_arrayByteRx
    Private Sub SendCommandFrameAndWaitForResponseFrameNormal_Pure2(ByRef callbackExternalRecvProcedure As DelegatecallbackExternalRecvProcedure, ByVal arrayByteTx As Byte(), ByRef o_arrayByteRx As Byte(), ByRef o_u32RxLen As UInt32, Optional ByVal bWait As Boolean = True, Optional ByVal u32TimeoutMS As UInt32 = ClassDevRS232.m_cnTX_TIMEOUT)
        'Dim bDebug As Boolean = True
        Try
            Dim nTicksCountDelay As Integer = 0
            'Dim nTicksCountStart As Integer
            'Dim nTicksCountStartSuitableDeplay As Integer
            Dim u32TxLen As UInt32
            m_bTimeout = False
            '
            Dim strMsg As String = "Pure Tx/Rx, No NEO/IDG Protocol"
            Dim strWaitStatus As String
            If bWait Then
                strWaitStatus = "Wait Response"
            Else
                strWaitStatus = "Exit Without Wait"
                'Set Timeout value to ZERO if no wait
                u32TimeoutMS = 0
            End If
            '
            If (m_bLogTurnOnOff) Then
                LogLn("<<<<<<<<<< Tx Dat >>>>>>>>>>")
                If (arrayByteTx IsNot Nothing) Then
                    Form01_Main.LogLnDumpData(arrayByteTx, arrayByteTx.Length)
                Else
                    LogLn("Tx Data = Nothing")
                End If
                LogLn(" ")
            End If

            '=======================[ Cleanup RS232 In/Out Buffer Before Send/Recv Process ]=======================
            'm_devobjSerialPort.DiscardInBuffer(), SendCommandAndResponse() calls these paired functions
            'm_devobjSerialPort.DiscardOutBuffer()

            If (arrayByteTx IsNot Nothing) Then
                u32TxLen = arrayByteTx.Length
            Else
                u32TxLen = 0
            End If
            'If (o_arrayByteRx IsNot Nothing) Then
            '    o_u32RxLen = o_arrayByteRx.Length
            'Else
            '    o_u32RxLen = 0
            'End If
            '
            'Config Receiver Call Back Function & Reset Response Value to default(non-use state)
            ConfigCallbackRecv(callbackExternalRecvProcedure, Nothing)
            '
            SendCommandAndResponse(arrayByteTx, u32TxLen, o_arrayByteRx, o_u32RxLen, u32TimeoutMS, False)
            If (o_u32RxLen < 1) Then
                'LogLn("Rx Data = Nothing") 'Debug Purpose
            ElseIf (m_bLogTurnOnOff) Then
                LogLn(">>>>>>>>>> Rx Data <<<<<<<<<<")
                Form01_Main.LogLnDumpData(o_arrayByteRx, o_u32RxLen)
                LogLn(" ")
            End If
            '
        Catch ex As Exception
            Throw 'ex
        Finally

        End Try
    End Sub

    '-------------------------------------------------------------[]-------------------------------------------------------------
    '<Note> RS232 Timeout Parameter is in millisecond
    Public Overrides Sub SendCommandFrameAndWaitForResponseFrame(ByRef callbackExternalRecvProcedure As DelegatecallbackExternalRecvProcedure, ByRef refIDGCmdData As tagIDGCmdData, ByVal nPlatform As ClassReaderInfo.ENU_FW_PLATFORM_TYPE, Optional ByVal bWait As Boolean = True, Optional ByVal u32TimeoutInMilliSeconds As UInt32 = ClassDevRS232.m_cnTX_TIMEOUT)

        Try
            Dim strMsg As String = String.Format("{0,4:X4}", refIDGCmdData.m_u16CmdSet)
            If (m_bLogTurnOnOff) Then
                Select Case (nPlatform)
                    Case ClassReaderInfo.ENU_FW_PLATFORM_TYPE.AR
                        strMsg = strMsg + ", " + ClassReaderProtocolBasic.GetCommandDescription(refIDGCmdData.m_u16CmdSet)
                        '
                        LogLn("IDG Cmd [ " + strMsg + " ]")
                    Case ClassReaderInfo.ENU_FW_PLATFORM_TYPE.GR, ClassReaderInfo.ENU_FW_PLATFORM_TYPE.NEO_IDG
                        strMsg = strMsg + ", " + ClassReaderProtocolIDGGR.GetCommandDescription(refIDGCmdData.m_u16CmdSet)
                        '
                        LogLn("IDG Cmd [ " + strMsg + " ]")
                    Case Else

                End Select
#If 0 Then


                If (nPlatform = ClassReaderInfo.ENU_FW_PLATFORM_TYPE.AR) Then
                    strMsg = strMsg + ", " + ClassReaderProtocolBasic.GetCommandDescription(refIDGCmdData.m_u16CmdSet)
                    'ElseIf (nPlatform = ClassReaderInfo.ENU_FW_PLATFORM_TYPE.GR) Then
                Else
                    strMsg = strMsg + ", " + ClassReaderProtocolIDGGR.GetCommandDescription(refIDGCmdData.m_u16CmdSet)

                End If
#End If

            End If
            '
            If m_bDebugRecvMode Then
                SendCommandFrameAndWaitForResponseFrameDebugRecvMode(callbackExternalRecvProcedure, refIDGCmdData, bWait, u32TimeoutInMilliSeconds)
            Else
                'Start Time Out Mechanism in Main Form
                m_refwinformMain.IDGCmdTimeOutRealTimeResetAndStart(u32TimeoutInMilliSeconds)

                SendCommandFrameAndWaitForResponseFrameNormal(callbackExternalRecvProcedure, refIDGCmdData, bWait, u32TimeoutInMilliSeconds)
            End If
            '
            '
        Catch ex As Exception
            Throw 'ex
        Finally
            'm_refwinformMain.IDGCmdTimeOutRealTimeStop()
        End Try

    End Sub
    '
    Public Overrides Sub SendCommandFrameAndWaitForResponseFrame_Pure(ByRef callbackExternalRecvProcedure As DelegatecallbackExternalRecvProcedure, ByVal arrayDataTx As Byte(), ByRef o_arrayDataRx As Byte(), ByRef o_u32RxLen As UInt32, Optional ByVal bWait As Boolean = True, Optional ByVal u32TimeoutInMilliSeconds As UInt32 = ClassDevRS232.m_cnTX_TIMEOUT)
        Try
            If m_bDebugRecvMode Then
                SendCommandFrameAndWaitForResponseFrameDebugRecvMode_Pure(callbackExternalRecvProcedure, arrayDataTx, o_arrayDataRx, o_u32RxLen, bWait, u32TimeoutInMilliSeconds)
            Else
                'Start Time Out Mechanism in Main Form
                'm_refwinformMain.IDGCmdTimeOutRealTimeResetAndStart(u32TimeoutInMilliSeconds)
                SendCommandFrameAndWaitForResponseFrameNormal_Pure(callbackExternalRecvProcedure, arrayDataTx, o_arrayDataRx, o_u32RxLen, bWait, u32TimeoutInMilliSeconds)
            End If
        Catch ex As Exception
            Throw 'ex
        Finally
            'm_refwinformMain.IDGCmdTimeOutRealTimeStop()
        End Try
    End Sub
    '
    '
    Public Overrides Sub SendCommandFrameAndWaitForResponseFrame_Pure2(ByRef callbackExternalRecvProcedure As DelegatecallbackExternalRecvProcedure, ByVal arrayDataTx As Byte(), ByRef o_arrayDataRx As Byte(), ByRef o_u32RxLen As UInt32, Optional ByVal bWait As Boolean = True, Optional ByVal u32TimeoutInMilliSeconds As UInt32 = ClassDevRS232.m_cnTX_TIMEOUT)
        Try
            If m_bDebugRecvMode Then
                SendCommandFrameAndWaitForResponseFrameDebugRecvMode_Pure(callbackExternalRecvProcedure, arrayDataTx, o_arrayDataRx, o_u32RxLen, bWait, u32TimeoutInMilliSeconds)
            Else
                'Start Time Out Mechanism in Main Form
                'm_refwinformMain.IDGCmdTimeOutRealTimeResetAndStart(u32TimeoutInMilliSeconds)
                SendCommandFrameAndWaitForResponseFrameNormal_Pure2(callbackExternalRecvProcedure, arrayDataTx, o_arrayDataRx, o_u32RxLen, bWait, u32TimeoutInMilliSeconds)
            End If
        Catch ex As Exception
            Throw 'ex
        Finally
            'm_refwinformMain.IDGCmdTimeOutRealTimeStop()
        End Try
    End Sub
    '

    Sub SetCallbackRecvParameterSync(ByRef cbExtRecvSyncParam As DelegatecallbackExternalRecvParameterSynctagIDGCmdData)
        m_callbackExternalRecvProcedureSyncParameter = cbExtRecvSyncParam
    End Sub
    '
    'This might through Exception,
    Public Shared Function BuadRateFormatChk(strVal As String) As Integer
        Dim nVal As Integer
        Dim bOk As Boolean = False
        '
        Try
            nVal = Convert.ToInt32(strVal)
            bOk = True
        Catch ex As Exception
            'Do  some error handling if possible
        Finally
            If (Not bOk) Then
                nVal = EnumBUAD_RATE._UNKNOWN
            End If
        End Try
        '
        Return nVal
    End Function
    '
    Public Shared Function ComPortFormatChk(strVal As String) As Boolean
        Dim nVal As Integer
        Dim bOk As Boolean = False
        Dim strTmp As String
        '
        If (strVal.Equals("")) Then
            Return False
        End If
        strVal = strVal.ToUpper()

        Try
            'Subtract first 3 chars string "COM"
            strTmp = strVal.Substring(0, 3)
            If (strTmp.Equals(m_c_strCOM)) Then
                'Check Value of COM port #

                strTmp = Replace(strVal, m_c_strCOM, "")
                nVal = Convert.ToInt32(strTmp)
                '
                bOk = True
            End If
        Catch ex As Exception
            'Do  some error handling if possible
        Finally
            If (Not bOk) Then
                'Invalid Format
            End If
        End Try
        '
        Return bOk
    End Function

#Region "IDisposable Support"
    Private disposedValue As Boolean ' To detect redundant calls

    ' IDisposable
    Protected Overridable Sub Dispose(disposing As Boolean)
        If Not Me.disposedValue Then
            If disposing Then
                ' TODO: dispose managed state (managed objects).
                If (m_devobjSerialPort IsNot Nothing) Then
                    m_devobjSerialPort.Dispose()
                    m_devobjSerialPort = Nothing
                End If
            End If
        End If
        Me.disposedValue = True
    End Sub

    ' TODO: override Finalize() only if Dispose(ByVal disposing As Boolean) above has code to free unmanaged resources.
    'Protected Overrides Sub Finalize()
    '    ' Do not change this code.  Put cleanup code in Dispose(ByVal disposing As Boolean) above.
    '    Dispose(False)
    '    MyBase.Finalize()
    'End Sub

    ' This code added by Visual Basic to correctly implement the disposable pattern.
    Public Sub Dispose() Implements IDisposable.Dispose
        ' Do not change this code.  Put cleanup code in Dispose(disposing As Boolean) above.
        Dispose(True)
        GC.SuppressFinalize(Me)
    End Sub
#End Region

    Sub RxBuffDiscard()
        m_devobjSerialPort.DiscardInBuffer()
    End Sub

End Class
