﻿' This class is used to save the Reader's Type
'======================[ Check List for Other Reader Migration ]======================
'1.Auto Poll Mode
'2.Burst Mode
'3.UI Scheme
'4.Button Configuration
'5.Other for later add-in
'6.This is ID TECH Reader Info Database.
' -----------[ List of Reader ]--------------
'    AR - Vend III(2013), PISCES(2016)
'    GR - KIOSK II (2012)
'    NEO (inherited from GR) - Vendi(2015), Unipay III (2016), Unipay 1.5 (2016, from Old Version),  VP4880 Serials (2016,aka Goose Run),
'======================[ End of Check List for Other Reader Migration ]======================
Imports System
'Imports MainRoot.ClassDataFilesCmpLog
'
Public Class ClassReaderInfo
    ' =====[ Revision / History ]========
    Enum ENUM_PD_IMG_IDX As Integer
        AR_HG4_VENDIII = 10000 '2013
        AR_HG4_VENDIII_AR_2_1_5 '2016
        AR_HG3_VP4800
        AR_HG2_VP8100
        AR_HG1_VP8800
        AR_HG0_VP8600
        AR_PISCES_V0 ' 2016
        AR_PISCES ' 2016

        GR_EC3_VP5000 = 10100
        GR_EB7_VP5000
        GR_EC7_KIOSKI
        GR_EC8_KIOSKII
        GR_ECA_KIOSKII

        NEO_VENDI_V0 = 10200
        NEO_VENDI_NEW '2016, New BL Architecture from K21 Shanghai Traditional's
        NEO_VP3300_AUDIO_JACK '2016, = Old VENDI_3_In_1 cases
        NEO_UNIPAY_III_iBase '2015, = Old VENDI_3_In_1 cases
        'NEO_UNIPAY_III '2016, = Old VENDI_3_In_1 cases '2017, rename UniPay III --> VP3300 Audio Jack
        'NEO_VP3300_AUDIO_JACK 
        NEO_UNIPAY_1D5 ' 2016, Unipay 1.5
        NEO_K_III '2016, Shanghai Team
        NEO_VP3300USB ' PICC+ICC+MSR, 2016, Inherited from Unipay III,  USB ONLY, a.k.a. Goose Run
        NEO_VP3300USB_E ' PICC+ICC, 2016, Inherited from Unipay III,  USB ONLY, a.k.a. Goose Run
        NEO_VP3300USB_C ' PICC Only, 2016, Inherited from Unipay III,  USB ONLY, a.k.a. Goose Run
        NEO_VP3300Bluetooth '2017, 2017 June 19 => rename NEO_BTPAY_MINI --> NEO_VP3300
        NEO_UNIPAY_1D5_TTK '2017, Inherited from Unipay 1D5 w/ Numerous Customized EMV L2 Functions, No A/J, No Battery.

        NEO_20_VP6300_VENDI_PRO = 10300 '2017, Vendi-Pro, Foxtail, VP6300
        NEO_20_VP5300_SPTP_II '2017, Spectrum Pro II, VP5300

        UNKNOWN_MAX
    End Enum

    '---------------------------------------
    Enum ENU_FW_PLATFORM_TYPE
        GR
        AR
        FW
        NEO_IDG
        NEO20_IDG

        UNKNOWN
    End Enum

    Enum ENU_PROTCL_VER As Integer
        _UNKNOWN = 0
        _V1
        _V2
        _V3

        _MAX
    End Enum
    '
    'Public m_TagXMLPartNumCustCfgParams As ClassDataFilesCmpLog.TagXMLPartNumCustCfgParams
    '
    Private m_listPlatformGBCMDefOn As List(Of ENUM_PD_IMG_IDX)
    Public m_dictPlatformNameStr2EnuVal As SortedDictionary(Of String, ENU_FW_PLATFORM_TYPE)
    Public m_dictBootloaderVerStr2Platform As New SortedDictionary(Of String, KeyValuePair(Of ENUM_PD_IMG_IDX, ENU_FW_PLATFORM_TYPE))
    Private m_dictProductTree As SortedDictionary(Of String, SortedDictionary(Of String, Integer))
    Private m_dictProductPicList As Dictionary(Of ENUM_PD_IMG_IDX, System.Drawing.Image)
    Private m_strFWVersionInfo As String
    Private m_PD_IMG As ENUM_PD_IMG_IDX
    Private m_nFwPlatform As ENU_FW_PLATFORM_TYPE
    Private m_bIsGBCMDefault As Boolean
    Private m_pRefMF As Form01_Main

    Private m_byteUIScheme As tagTLV.ENUM_FFF8_UI_SCHEME

    Private Shared m_strKeyPrdt_Vendi As String = "VENDI"
    Private Shared m_strKeyPrdt_VendIII As String = "VENDIII"
    Private Shared m_strKeyPrdt_KIOSKII As String = "KIOSKII"
    Private Shared m_strKeyPrdt_UnipayIII As String = "UnipayIII"
    Private Shared m_strKeyPrdt_VP3300AudioJack As String = "VP3300 Audio Jack" 'Rename UniPay III --> VP3300 Audio Jack
    Private Shared m_strKeyPrdt_UnipayI_V As String = "UnipayI_V" 'Unipay 1.5
    Private Shared m_strKeyPrdt_VP3300USB As String = "HW,VP3300 USB" '<--New, Old-->"HW,VP4880", last update 2017 July 25
    Private Shared m_strKeyPrdt_VP3300USB_E As String = "HW,VP3300 USB-E"  '<--New, Old-->"HW,VP4880E" , last update 2017 July 25
    Private Shared m_strKeyPrdt_VP3300USB_C As String = "HW,VP3300 USB-C" '<--New, Old-->"HW,VP4880C", last update 2017 July 25
    'Private Shared m_strKeyPrdt_BTPayMini As String = "HW,VPBTPay Mini", deprecated, change to VP3300
    Private Shared m_strKeyPrdt_VP3300Bluetooth As String = "HW,VP3300 Bluetooth"
    Private Shared m_strKeyPrdt_VP6300 As String = "VP6300"
    Private Shared m_strKeyPrdt_VP5300 As String = "VP5300"

    Public Shared m_mapPrdtID As SortedDictionary(Of String, ClassReaderInfo.ENUM_PD_IMG_IDX) = New SortedDictionary(Of String, ClassReaderInfo.ENUM_PD_IMG_IDX) From {
    {m_strKeyPrdt_Vendi, ClassReaderInfo.ENUM_PD_IMG_IDX.NEO_VENDI_V0},
    {m_strKeyPrdt_VendIII, ClassReaderInfo.ENUM_PD_IMG_IDX.AR_HG4_VENDIII},
    {m_strKeyPrdt_KIOSKII, ClassReaderInfo.ENUM_PD_IMG_IDX.GR_ECA_KIOSKII},
    {m_strKeyPrdt_UnipayIII, ClassReaderInfo.ENUM_PD_IMG_IDX.NEO_VP3300_AUDIO_JACK},
    {m_strKeyPrdt_VP3300AudioJack, ClassReaderInfo.ENUM_PD_IMG_IDX.NEO_VP3300_AUDIO_JACK},
    {m_strKeyPrdt_UnipayI_V, ClassReaderInfo.ENUM_PD_IMG_IDX.NEO_UNIPAY_1D5},
    {m_strKeyPrdt_VP3300USB, ClassReaderInfo.ENUM_PD_IMG_IDX.NEO_VP3300USB},
    {m_strKeyPrdt_VP3300USB_E, ClassReaderInfo.ENUM_PD_IMG_IDX.NEO_VP3300USB_E},
    {m_strKeyPrdt_VP3300USB_C, ClassReaderInfo.ENUM_PD_IMG_IDX.NEO_VP3300USB_C},
    {m_strKeyPrdt_VP3300Bluetooth, ClassReaderInfo.ENUM_PD_IMG_IDX.NEO_VP3300Bluetooth},
    {m_strKeyPrdt_VP5300, ClassReaderInfo.ENUM_PD_IMG_IDX.NEO_20_VP5300_SPTP_II},
    {m_strKeyPrdt_VP6300, ClassReaderInfo.ENUM_PD_IMG_IDX.NEO_20_VP6300_VENDI_PRO}
    }
    '-----------------[ Reference Objects, Don't Release in the Cleanup() Routine ]-------------------
    Private m_refobjRdrCmder As ClassReaderProtocolBasic
    'Private m_refobjTestScenario As ClassTestScenarioBasic
    Private m_bIsLCDSupported As Boolean
    Private m_bIsLCDContrastSupported As Boolean
    Private m_bIsDateTimeSupported As Boolean

    '=====[ Note- RTC No Battery Model Test Scenario ]=====
    'case a>Reader has No Battery for RTC. Ex: VP4880
    'In QC Mode, test RTC in such a scenario. a.k.a. = Production Mode
    '1.Set RTC 2.Do most of other test items 3.before S/N Injection(Verification), Check RTC again

    'case b>Reader has Battery for RTC. Ex: VP3300 Audio Jack, Bluetooth, VP6300, Vendi, VP5300, UniPay 1.5
    'In QC Mode, test RTC in such a scenario.
    '1.Perform most of other test items 2.before S/N Injection(Verification), Check RTC again
    'Diff betw case a & b = Since case a is NO Battery Case, RTC can't last after Power OFF (USB Source).
    'TS need to regulate RTC value again before check start in QC Mode.
    Private m_bIsDateTimeSupported_RTCNoBattery As Boolean 'RTC Test ONLY available in TM, Not in QC
    '=====[ End of Note- RTC No Battery Model ]=====

    Private m_bIsButtonsCfgSupported As Boolean
    Private m_bIsBuzzerSupported As Boolean
    Private m_bIsLEDsGreenSupport As Boolean
    Private m_bIsLEDsBlueSupport As Boolean
    Private m_bIsLEDsMSRSupport As Boolean
    Private m_arrayByteTestButtonsConfigEnableAll As Byte()
    Private m_arrayByteTestButtonsConfigDefault As Byte()
    Private m_refsdictTLVsDefault As SortedDictionary(Of String, tagTLV)
    Private m_bIsDefBurstMode As ClassReaderProtocolBasic.TAG_BURST_MODE
    Private Shared m_pInstance As ClassReaderInfo = Nothing
    Private m_bIsDevMode As Boolean
    Private m_bIsSREDSupported As Boolean
    Private m_tblTSName As SortedDictionary(Of ENUM_PD_IMG_IDX, String)
    Private m_strTrack1 As String
    Private m_bMSRTrackChkAndParse As Boolean
    Private m_strTrack2 As String
    Private m_strTrack3 As String
    Private m_bMSRTrackChkAndParseDone As Boolean
    Private m_bIsLEDsTriColorSupport As Boolean
    Private m_bIsAdminKeyInjection As Boolean
    Private m_Rect_Signature As Rectangle
    Private m_bIsMSRTk1 As Boolean
    Private m_bIsMSRTk2 As Boolean
    Private m_bIsMSRTk3 As Boolean
    Private m_bIsKeyPadSupported As Boolean
    Private m_bIsTouchScreenSupported As Boolean
    Private m_bIsSDCardSupported As Boolean

    Public ReadOnly Property s_strTrack1 As String
        Get
            Return m_strTrack1
        End Get
    End Property

    Public ReadOnly Property s_strTrack2 As String
        Get
            Return m_strTrack2
        End Get
    End Property

    Public ReadOnly Property s_strTrack3 As String
        Get
            Return m_strTrack3
        End Get
    End Property

    ReadOnly Property bIsBuzzerSupport As Boolean
        Get
            Select Case m_PD_IMG
                Case ENUM_PD_IMG_IDX.NEO_UNIPAY_1D5, ENUM_PD_IMG_IDX.NEO_UNIPAY_1D5_TTK
                    Return False
                Case Else
                    Return True
            End Select
        End Get
    End Property
    '
    ReadOnly Property GetTSName As String
        Get
            If Not m_tblTSName.ContainsKey(m_PD_IMG) Then
                Return "TS Unknown (" + m_PD_IMG.ToString() + ")"
            End If

            Return m_tblTSName(m_PD_IMG)
        End Get
    End Property

    ReadOnly Property GetPDImage As ClassReaderInfo.ENUM_PD_IMG_IDX
        Get
            Return m_PD_IMG
        End Get
    End Property

    ReadOnly Property GetProductId As ENUM_PD_IMG_IDX
        Get
            Return m_PD_IMG
        End Get
    End Property
    ReadOnly Property GetProductName As String
        Get
            If (m_mapPrdtID.ContainsValue(m_PD_IMG)) Then
                For Each ItrSeek In m_mapPrdtID
                    If (ItrSeek.Value = m_PD_IMG) Then
                        Return ItrSeek.Key
                    End If
                Next 'For Loop
            End If 'If

            Return "Unknown Product Image ID ( = " + m_PD_IMG.ToString() + ")"
        End Get
    End Property

    ReadOnly Property bIsIntfRS232 As Boolean
        Get
            Dim bRet As Boolean = True 'Normally Mobile Reader don't has RS232 Interface, Audio Jack --> RS232 via Convert Board

            'If (byteIsShippingCfg016_Form04_TITEM_Exclude_004_Intf_RS232) Then
            '    If (&H1 = byteValShippingCfg016_Form04_TITEM_EXCLUDE_004_Intf_RS232) Then
            '        Return False
            '    End If
            'End If

            Select Case (m_PD_IMG)
                Case ENUM_PD_IMG_IDX.NEO_UNIPAY_III_iBase, ENUM_PD_IMG_IDX.NEO_VP3300USB, ENUM_PD_IMG_IDX.NEO_VP3300USB_C, ENUM_PD_IMG_IDX.NEO_VP3300USB_E, ENUM_PD_IMG_IDX.NEO_UNIPAY_1D5_TTK, ENUM_PD_IMG_IDX.NEO_VP3300Bluetooth
                    bRet = False
            End Select
            '
            'Return (bRet And (Not byteIsShippingCfg016_Form04_TITEM_Exclude_004_Intf_RS232))
            Return bRet
        End Get
    End Property

    '
    ReadOnly Property bIsIntfUSB_VCOMSupported As Boolean
        Get
            Dim bRet As Boolean = False
            Select Case (m_PD_IMG)
                Case ENUM_PD_IMG_IDX.AR_HG4_VENDIII, ENUM_PD_IMG_IDX.AR_HG4_VENDIII_AR_2_1_5
                    Return bRet
            End Select
            '
            Return bRet
        End Get
    End Property

    ReadOnly Property bIsIntfUSBHID As Boolean
        Get
            Dim bRet As Boolean = True

            'If (byteIsShippingCfg015_Form04_TITEM_Exclude_003_Intf_USBHID) Then
            '    If (&H1 = byteValShippingCfg015_Form04_TITEM_EXCLUDE_003_Intf_USBHID) Then
            '        Return False
            '    End If
            'End If

            'Select Case (m_PD_IMG)
            '    Case ENUM_PD_IMG_IDX.AR_HG4_VENDIII _
            '        , ENUM_PD_IMG_IDX.NEO_VENDI_V0 _
            '        , ENUM_PD_IMG_IDX.NEO_VENDI_NEW _
            '        , ENUM_PD_IMG_IDX.NEO_VP3300_AUDIO_JACK _
            '        , ENUM_PD_IMG_IDX.NEO_VP3300USB _
            '        , ENUM_PD_IMG_IDX.NEO_VP3300USB_C _
            '        , ENUM_PD_IMG_IDX.NEO_VP3300USB_E _
            '        , ENUM_PD_IMG_IDX.GR_EC7_KIOSKI _
            '        , ENUM_PD_IMG_IDX.GR_EC8_KIOSKII _
            '        , ENUM_PD_IMG_IDX.NEO_UNIPAY_1D5 _
            '        , ENUM_PD_IMG_IDX.NEO_UNIPAY_1D5_TTK _
            '        , ENUM_PD_IMG_IDX.AR_HG4_VENDIII_AR_2_1_5 _
            '        , ENUM_PD_IMG_IDX.AR_PISCES _
            '        , ENUM_PD_IMG_IDX.AR_PISCES_V0 _
            '        , ENUM_PD_IMG_IDX.NEO_K_III _
            '        , ENUM_PD_IMG_IDX.NEO_VP3300Bluetooth _
            '        , ENUM_PD_IMG_IDX.NEO_20_VP6300_VENDI_PRO _
            '        , ENUM_PD_IMG_IDX.NEO_20_VP5300_SPTP_II
            '        Return True
            'End Select

            Return bRet
        End Get
    End Property

    ReadOnly Property bIsIntfMSR As Boolean
        Get
            'If (byteIsShippingCfg011_Form04_TITEM_Exclude_001_MSR) Then
            '    'Exclude = 0 --> Enable MSR
            '    Return (byteValShippingCfg011_Form04_TITEM_EXCLUDE_001_MSR = 0)

            'End If
            Return (bIsMSRAvailableTrack1 Or bIsMSRAvailableTrack2 Or bIsMSRAvailableTrack3)
        End Get
    End Property

    ReadOnly Property bIsMSRParseDone As Boolean
        Get
            Return m_bMSRTrackChkAndParseDone
        End Get
    End Property
    '
    '
    ReadOnly Property GetMSRStr(Optional ByVal bNeedCrLf As Boolean = True) As String
        Get
            Dim strDelimiter As String = ""
            Dim strPrintout As String = ""
            If (bNeedCrLf) Then
                strDelimiter = vbCrLf
            End If

            If (Not m_strTrack1.Equals("")) Then
                strPrintout = strPrintout + "Track 1 = " + m_strTrack1 + strDelimiter
            End If
            If (Not m_strTrack2.Equals("")) Then
                strPrintout = strPrintout + "Track 2 = " + m_strTrack2 + strDelimiter
            End If
            If (Not m_strTrack3.Equals("")) Then
                strPrintout = strPrintout + "Track 3 = " + m_strTrack3 + strDelimiter
            End If
            Return strPrintout
        End Get
    End Property

    ReadOnly Property GetPlatformType As ENU_FW_PLATFORM_TYPE
        Get
            Return m_nFwPlatform
        End Get
    End Property

    ReadOnly Property bIsLEDTriColorSupport As Boolean
        Get
            Return m_bIsLEDsTriColorSupport
        End Get
    End Property

    'ReadOnly Property bIsKeyInjectionSupport As Boolean
    '    Get
    '        Select Case m_PD_IMG
    '            Case ENUM_PD_IMG_IDX.NEO_VENDI, ENUM_PD_IMG_IDX.NEO_VENDI_NEW
    '                Return True
    '            Case ENUM_PD_IMG_IDX.NEO_UNIPAY_III, ENUM_PD_IMG_IDX.NEO_UNIPAY_I_V
    '                Return True
    '            Case Else
    '                Return False
    '        End Select
    '    End Get
    'End Property
    '
    ReadOnly Property bIsPlatformAR_Pisces_2017 As Boolean
        Get
            If ((m_PD_IMG = ENUM_PD_IMG_IDX.AR_PISCES) Or (m_PD_IMG = ENUM_PD_IMG_IDX.AR_PISCES_V0)) Then
                Return True
            End If
            Return False
        End Get
    End Property
    '
    ReadOnly Property bIsPlatformNEO_VP3300AudioJack_2015 As Boolean
        Get
            If ((m_PD_IMG = ENUM_PD_IMG_IDX.NEO_VP3300_AUDIO_JACK) Or (m_PD_IMG = ENUM_PD_IMG_IDX.NEO_UNIPAY_III_iBase)) Then
                Return True
            End If
            Return False
        End Get
    End Property
    '
    ReadOnly Property bIsPlatformNEO_Unipay1D5_2015 As Boolean
        Get
            Return (m_PD_IMG = ENUM_PD_IMG_IDX.NEO_UNIPAY_1D5) Or (m_PD_IMG = ENUM_PD_IMG_IDX.NEO_UNIPAY_1D5_TTK)
        End Get
    End Property
    '
    'ReadOnly Property bIsPlatformNEOBTpayMini As Boolean
    ReadOnly Property bIsPlatformNEO_VP3300_Bluetooth_2017 As Boolean
        Get
            Return (m_PD_IMG = ENUM_PD_IMG_IDX.NEO_VP3300Bluetooth)
        End Get
    End Property
    '
    '
    ReadOnly Property bIsPlatformNEO_VP4880_2016 As Boolean
        Get
            Select Case (m_PD_IMG)
                Case ENUM_PD_IMG_IDX.NEO_VP3300USB, ENUM_PD_IMG_IDX.NEO_VP3300USB_C, ENUM_PD_IMG_IDX.NEO_VP3300USB_E
                    Return True
            End Select
            '
            Return False
        End Get
    End Property
    '
    ReadOnly Property bIsPlatformNEO_VP4880C_2016 As Boolean
        Get
            Return (m_PD_IMG = ENUM_PD_IMG_IDX.NEO_VP3300USB_C)
        End Get
    End Property
    '
    ReadOnly Property bIsPlatformNEO_VP4880E_2016 As Boolean
        Get
            Return (m_PD_IMG = ENUM_PD_IMG_IDX.NEO_VP3300USB_E)
        End Get
    End Property

    Property bIsAdminKeySupported As Boolean
        Get
            Return m_bIsAdminKeyInjection
        End Get
        Set(value As Boolean)
            m_bIsAdminKeyInjection = value
        End Set
    End Property

    ReadOnly Property bIsSAMSupported As Boolean
        Get
            Select Case (m_PD_IMG)
                Case ENUM_PD_IMG_IDX.AR_HG4_VENDIII, ENUM_PD_IMG_IDX.AR_HG4_VENDIII_AR_2_1_5, ENUM_PD_IMG_IDX.AR_PISCES, ENUM_PD_IMG_IDX.AR_PISCES_V0
                    Return True
                Case ENUM_PD_IMG_IDX.NEO_20_VP5300_SPTP_II, ENUM_PD_IMG_IDX.NEO_20_VP6300_VENDI_PRO
                    Return True
            End Select
            '
            Return False
        End Get
    End Property

    ReadOnly Property bIsTouchScreenSupported As Boolean
        Get
            'Select Case (m_PD_IMG)
            '    Case ENUM_PD_IMG_IDX.AR_PISCES, ENUM_PD_IMG_IDX.AR_PISCES_V0
            '        Return True
            'End Select
            ''
            'Return False
            Return m_bIsTouchScreenSupported
        End Get
    End Property
    '
    ReadOnly Property bIsBoardTMSupported As Boolean
        Get
            Select Case m_PD_IMG
                Case ENUM_PD_IMG_IDX.AR_PISCES, ENUM_PD_IMG_IDX.AR_PISCES_V0
                    Return True
            End Select
            Return False
        End Get
    End Property
    '
    ReadOnly Property bIsBatteryPoweredSupported As Boolean
        'This future is usually for Mobile Reader Product
        'Unipay III, Unipay 1.5
        Get
            Select Case (m_PD_IMG)
                Case ENUM_PD_IMG_IDX.NEO_UNIPAY_1D5, ENUM_PD_IMG_IDX.NEO_VP3300_AUDIO_JACK, ENUM_PD_IMG_IDX.NEO_UNIPAY_III_iBase
                    Return True ' TODO - Since Audio Jack Plug-in/-out sometimes also cause PMS back to Run Mode.
            End Select

            Return False
        End Get
    End Property
    '
    ReadOnly Property bIsEthernetSupported As Boolean
        Get
            'If (byteIsShippingCfg017_Form04_TITEM_Exclude_005_Intf_Ethernet) Then
            '    If (&H1 = byteValShippingCfg017_Form04_TITEM_EXCLUDE_005_Intf_Ethernet) Then
            '        Return False
            '    End If
            'End If

            Select Case (m_PD_IMG)
                Case ENUM_PD_IMG_IDX.AR_PISCES, ENUM_PD_IMG_IDX.AR_PISCES_V0
                    Return True
            End Select
            '
            Return False
        End Get
    End Property
    '
    ReadOnly Property bIsVCOMSupported As Boolean
        Get
            Select Case (m_PD_IMG)
                Case ENUM_PD_IMG_IDX.AR_HG4_VENDIII, ENUM_PD_IMG_IDX.AR_HG4_VENDIII_AR_2_1_5
                    Return True
            End Select
            '
            Return False
        End Get
    End Property

    Private m_strSerNum As String = ""
    Property strSerNum As String
        Get
            Return m_strSerNum
        End Get
        Set(value As String)
            m_strSerNum = value
        End Set
    End Property

    Private m_strEthMACAddress As String = ""
    Property strEthMACAddress As String
        Get
            Return m_strEthMACAddress
        End Get
        Set(value As String)
            m_strEthMACAddress = value
        End Set
    End Property

    'Any LED Futures is Supported this status
    ReadOnly Property bIsLEDsSupported As Boolean
        Get
            Select Case (m_PD_IMG)

            End Select
            '
            Return bIsLEDsBlueSupport Or bIsLEDsGreenSupport Or bIsLEDsMSRSupport Or bIsLEDsPowerChargedSupport Or bIsLEDTriColorSupport
        End Get
    End Property
    '
    '[Note] Don't Free the System AID List Resource
    'It's done by ClassReaderInfo.Cleanup() Procedure
    Private m_listSystemAID As List(Of String)
    ReadOnly Property GetSystemAIDList As List(Of String)
        Get
            Return m_listSystemAID
        End Get
    End Property

    Private m_bIsForcedReset As Boolean
    Property bIsForcedReset As Boolean
        Set(value As Boolean)
            m_bIsForcedReset = value
        End Set
        '
        Get
            Dim bNative As Boolean = m_bIsForcedReset
            m_bIsForcedReset = False
            Return bNative
        End Get
    End Property




    ' MSR Encryption Enable & EMV Encryption Enable Feature are controlled in division
    ' For Encryption Mode - U3 & U1.5 using MSR Encryption (Isolated from EMV setting) + EMV Encryption
    ReadOnly Property bIsEncTypeMSRSupported As Byte
        Get
            Select Case (m_PD_IMG)
                Case ENUM_PD_IMG_IDX.NEO_UNIPAY_1D5, ENUM_PD_IMG_IDX.NEO_VP3300_AUDIO_JACK, ENUM_PD_IMG_IDX.NEO_UNIPAY_III_iBase, _
                    ENUM_PD_IMG_IDX.NEO_VP3300USB, ENUM_PD_IMG_IDX.AR_PISCES, ENUM_PD_IMG_IDX.AR_PISCES_V0, ENUM_PD_IMG_IDX.NEO_VENDI_V0, ENUM_PD_IMG_IDX.NEO_VENDI_NEW, ENUM_PD_IMG_IDX.NEO_UNIPAY_1D5_TTK, ENUM_PD_IMG_IDX.NEO_VP3300Bluetooth, ENUM_PD_IMG_IDX.NEO_20_VP5300_SPTP_II
                    Return True
                Case ENUM_PD_IMG_IDX.NEO_20_VP6300_VENDI_PRO
                    Return True
                Case ENUM_PD_IMG_IDX.NEO_VP3300USB_C, ENUM_PD_IMG_IDX.NEO_VP3300USB_E
                    'No MSR --> False
                Case Else
                    'Other Case
            End Select

            Return False
        End Get
    End Property
    '
    ReadOnly Property GetXferGranularity As Integer
        Get
            Select Case (m_PD_IMG)
                Case ENUM_PD_IMG_IDX.AR_HG4_VENDIII, ENUM_PD_IMG_IDX.AR_HG4_VENDIII_AR_2_1_5
                    If (m_refobjRdrCmder.bIsConnectedRS232) Then
                        Return 512
                    End If
                    Return 512
                Case ENUM_PD_IMG_IDX.AR_PISCES, ENUM_PD_IMG_IDX.AR_PISCES_V0
                    '2016, PISCES basic Conn = USB-HID, let it faster
                    If (m_refobjRdrCmder.bIsConnectedRS232) Then
                        Return 1024
                    End If
                    Return 1024
            End Select

            Return 512
        End Get
    End Property
    '
    ReadOnly Property bIsTamperSupported As Boolean
        Get
            Select Case (m_PD_IMG)
                Case ENUM_PD_IMG_IDX.AR_PISCES, ENUM_PD_IMG_IDX.AR_PISCES_V0
                    Return True
                Case ENUM_PD_IMG_IDX.NEO_20_VP6300_VENDI_PRO, ENUM_PD_IMG_IDX.NEO_20_VP5300_SPTP_II
                    Return True
            End Select
            '
            Return False
        End Get
    End Property
    '
    ReadOnly Property bIsSDCardSupported As Boolean
        Get
            'Select Case (m_PD_IMG)
            '    Case ENUM_PD_IMG_IDX.AR_PISCES, ENUM_PD_IMG_IDX.AR_PISCES_V0
            '        Return True
            'End Select
            ''
            'Return False

            Return m_bIsSDCardSupported
        End Get
    End Property
    '
    ReadOnly Property GetCOMBaudrate As Integer
        Get
            Select Case (m_PD_IMG)
                Case ENUM_PD_IMG_IDX.AR_HG4_VENDIII, ENUM_PD_IMG_IDX.AR_HG4_VENDIII_AR_2_1_5
                    Return 19200
                Case ENUM_PD_IMG_IDX.AR_PISCES, ENUM_PD_IMG_IDX.AR_PISCES_V0
                    Return 115200
            End Select
            ' Default Baud rate = 9600
            Return 9600
        End Get
    End Property
    '
    ReadOnly Property bIsLCDImgFileRemovable As Boolean
        Get
            Select Case (m_PD_IMG)
                Case ENUM_PD_IMG_IDX.AR_HG4_VENDIII, ENUM_PD_IMG_IDX.AR_HG4_VENDIII_AR_2_1_5
                    Return True
            End Select
            'Return False
            Return Not bIsLCDImgCopyInternal
        End Get
    End Property

    'Reader : PISCES
    ReadOnly Property bIsLCDImgCopyInternal As Boolean
        Get
            Select Case (m_PD_IMG)
                Case ENUM_PD_IMG_IDX.AR_PISCES, ENUM_PD_IMG_IDX.AR_PISCES_V0
                    Return True 'Comment it to make TS to ask for download process
            End Select
            '
            Return False
        End Get
    End Property

    'Reader : Vend III ; PISCES
    ReadOnly Property bIsLCDImgDownloadable As Boolean
        Get
            Select Case (m_PD_IMG)
                Case ENUM_PD_IMG_IDX.AR_PISCES, ENUM_PD_IMG_IDX.AR_PISCES_V0, ENUM_PD_IMG_IDX.AR_HG4_VENDIII, ENUM_PD_IMG_IDX.AR_HG4_VENDIII_AR_2_1_5
                    Return False
            End Select
            '
            Return False
        End Get
    End Property
    '
    ReadOnly Property bIsMCU_G53_Supported As Boolean
        'G53 is MCU of Contactless IC
        Get
            Select Case (m_PD_IMG)
                Case ENUM_PD_IMG_IDX.AR_PISCES, ENUM_PD_IMG_IDX.AR_PISCES_V0
                    Return True
            End Select
            '
            Return False
        End Get
    End Property
    '
    ReadOnly Property bIsAudioJackSupport As Boolean
        Get
            Select Case (m_PD_IMG)
                Case ENUM_PD_IMG_IDX.NEO_UNIPAY_1D5, ENUM_PD_IMG_IDX.NEO_VP3300_AUDIO_JACK
                    Return True
            End Select

            Return False
        End Get
    End Property
    '
    ReadOnly Property GetProcessorTagName As String
        Get
            'Select Case (m_PD_IMG)
            '    Case ENUM_PD_IMG_IDX.AR_PISCES, ENUM_PD_IMG_IDX.AR_PISCES_V0
            '        Return tagTLV.TAG_DFEE61
            'End Select
            '
            Return tagTLV.TAG_DF61_or_DFEE61
        End Get
    End Property
    '
    ReadOnly Property bIsPCI_RTCSupported As Boolean
        Get
            Select Case (m_PD_IMG)
                Case ENUM_PD_IMG_IDX.AR_PISCES, ENUM_PD_IMG_IDX.AR_PISCES_V0
                    Return True
                    '
                Case ENUM_PD_IMG_IDX.NEO_20_VP6300_VENDI_PRO, ENUM_PD_IMG_IDX.NEO_20_VP5300_SPTP_II
                    Return True
            End Select

            Return False
        End Get
    End Property

    ReadOnly Property bIsEthernetMACAddrInjectionSupported As Boolean
        Get
            Select Case (m_PD_IMG)
                Case ENUM_PD_IMG_IDX.AR_PISCES, ENUM_PD_IMG_IDX.AR_PISCES_V0
                    Return True
            End Select
            '
            Return False
        End Get
    End Property

    Public ReadOnly Property bIsPlatformWinRunAsAdminUser As Boolean
        Get
            Select Case (m_PD_IMG)
                Case ENUM_PD_IMG_IDX.AR_PISCES
                    Return True
            End Select
            '
            Return False
        End Get
    End Property

    ReadOnly Property bIsPlatformNEO_Unipay1D5_TTK_2016 As Boolean
        Get
            Select Case (m_PD_IMG)
                Case ENUM_PD_IMG_IDX.NEO_UNIPAY_1D5_TTK
                    Return True
            End Select
            '
            Return False
        End Get
    End Property

    ReadOnly Property bIsPlatform_NEO20_2017 As Boolean
        Get
            Select Case (m_PD_IMG)
                Case ENUM_PD_IMG_IDX.NEO_20_VP6300_VENDI_PRO, ENUM_PD_IMG_IDX.NEO_20_VP5300_SPTP_II
                    Return True
            End Select
            '
            Return False
        End Get

    End Property

    Private ReadOnly Property bIsPlatformAR_V300_2016 As Boolean
        Get
            Select Case (m_PD_IMG)
                Case ENUM_PD_IMG_IDX.AR_PISCES, ENUM_PD_IMG_IDX.AR_PISCES_V0
                    Return True
            End Select
            Return False
        End Get
    End Property

    ReadOnly Property bIsPlatformNEO20_VP6300 As Boolean
        Get
            Select Case (m_PD_IMG)
                Case ENUM_PD_IMG_IDX.NEO_20_VP6300_VENDI_PRO
                    Return True
            End Select
            Return False
        End Get
    End Property

    ReadOnly Property bIsPlatformNEO20_VP5300 As Boolean
        Get
            Select Case (m_PD_IMG)
                Case ENUM_PD_IMG_IDX.NEO_20_VP5300_SPTP_II
                    Return True
            End Select
            Return False
        End Get
    End Property



    ReadOnly Property GetTamperListStdMsg As String()
        Get
            Select Case (m_PD_IMG)
                Case ENUM_PD_IMG_IDX.NEO_20_VP6300_VENDI_PRO
                    Return s_strLstTamperPair_VP6300_VendiPro
                Case ENUM_PD_IMG_IDX.NEO_20_VP5300_SPTP_II
                    Return s_strLstTamperPair_VP5300_SPTP_II
            End Select
            Return Nothing
        End Get
    End Property

    ReadOnly Property bIsResetToDefaultAfterAllTestPassed As Boolean
        Get
            Select Case (m_PD_IMG)
                Case Else

            End Select
            '
            Return True
        End Get
    End Property
    '
    ReadOnly Property bIsPlatformNEO20_2017 As Boolean
        Get
            Return bIsPlatformNEO20_VP5300 Or bIsPlatformNEO20_VP6300
        End Get
    End Property
    '
    Public Function PartNumListDefault() As String()
        'ToDo: New Part Number Add Here, New Reader
        'Keyword For Search: Part Number, New Reader
        'Note: Caller must erase this string list to prevent from memory leaks
        Dim strPNList As String() = Nothing
        Select Case (m_PD_IMG)
            Case ENUM_PD_IMG_IDX.AR_HG4_VENDIII, ENUM_PD_IMG_IDX.AR_HG4_VENDIII_AR_2_1_5
                'Vend III, CL, SC, MSR, SAM x 3, LCD (Graphic+Font), LED(Green x 4, Blue x 4, MSR x 1), Buttons x 4 [Cancel][Done][Btn 1, Under Cancel][Btn 2, Under Done]
                '--------------------------------------------------------
                'IDVV-380131 = Vend3;NFC+MSR+SC;3SAM
                'IDVV-380131D = Vend3;NFC+MSR+SC;3SAM;Dev
                'IDVV-381131 = Vend3;NFC+MSR+SC;3SAM;SRED
                'IDVV-381131D = Vend3;NFC+MSR+SC;3SAM;SRED;Dev
                '--------------------------------------------------------
                strPNList = New String() {"IDVV-380131", "IDVV-380131D", "IDVV-381131", "IDVV-381131D"}

            Case ENUM_PD_IMG_IDX.NEO_VENDI_V0, ENUM_PD_IMG_IDX.NEO_VENDI_NEW
                'Vendi, CL, MSR, LCD (Font Only), LED (Green x 4, Blue x 3, MSR x 1), Buttons x 2 [Done][Swipe Card]
                '--------------------------------------------------------
                'IDVV-120101 = Vendi;NFC;T12;Std
                'IDVV-120101-CR = Vendi;NFC;T12;Std; For Crane
                'IDVV-120101-US = Vendi;NFC;T12;Std; For USA Tech
                strPNList = New String() {"IDVV-120101", "IDVV-120101-CR", "IDVV-120101-US"}
                '--------------------------------------------------------
            Case ENUM_PD_IMG_IDX.NEO_VP3300_AUDIO_JACK
                'ID-80149003-001 = iBase version
                'IDMR-AB93133 = Black;
                'IDMR-AB93133W = White;
                strPNList = New String() {"ID-80149003-001", "IDMR-AB93133", "IDMR-AB93133W"}
                '--------------------------------------------------------
            Case ENUM_PD_IMG_IDX.NEO_UNIPAY_1D5
                'Unipay 1.5
                'S = Side USB
                strPNList = New String() {"IDMR-AB83133S", "IDMR-AB83133"}
                '--------------------------------------------------------
            Case ENUM_PD_IMG_IDX.NEO_VP3300USB, ENUM_PD_IMG_IDX.NEO_VP3300USB_C, ENUM_PD_IMG_IDX.NEO_VP3300USB_E
                strPNList = New String() {"IDVP-31", "IDVP-21", "IDVP-11", "IDVP-61"}
            Case Else
                'Other Case
        End Select

        Return strPNList
    End Function
    '
    Public Function PartNumFormatChk(strInput As String, Optional ByVal nPDType As ENUM_PD_IMG_IDX = ENUM_PD_IMG_IDX.UNKNOWN_MAX) As Boolean
        ' Note : InProgressVerifiedProductPropertiesCreate() also effects the Properties
        ' Last Updated Date: April 28, 2015, by goofy_liu@idtechproducts.com.tw, + Vendi
        ' Last Updated Date: Mar 02, 2016, by goofy_liu@idtechproducts.com.tw + Unipay III, Unipay 1.5
        Dim bValid As Boolean = False
        Dim strTmp As String
        Dim nVal As Integer
        Dim strPattern As String
        Dim nPDTypeChk As ENUM_PD_IMG_IDX = nPDType

        If (nPDTypeChk = ENUM_PD_IMG_IDX.UNKNOWN_MAX) Then
            If (m_PD_IMG = ENUM_PD_IMG_IDX.UNKNOWN_MAX) Then
                'Should Probe Reader Type
            End If
            '
            nPDTypeChk = m_PD_IMG
        End If
        Select Case (nPDTypeChk)
            Case ENUM_PD_IMG_IDX.AR_PISCES

            Case ENUM_PD_IMG_IDX.AR_HG4_VENDIII, ENUM_PD_IMG_IDX.AR_HG4_VENDIII_AR_2_1_5
                'Vend III, CL, SC, MSR, SAM x 3, LCD (Graphic+Font), LED(Green x 4, Blue x 4, MSR x 1), Buttons x 4 [Cancel][Done][Btn 1, Under Cancel][Btn 2, Under Done]
                '--------------------------------------------------------
                'IDVV-380131 = Vend3;NFC+MSR+SC;3SAM
                'IDVV-380131D = Vend3;NFC+MSR+SC;3SAM;Dev
                'IDVV-381131 = Vend3;NFC+MSR+SC;3SAM;SRED
                'IDVV-381131D = Vend3;NFC+MSR+SC;3SAM;SRED;Dev
                '--------------------------------------------------------
                'Length >= 11
                nVal = strInput.Length - 11
                If (nVal < 0) Then
                    Exit Select
                End If
                'Format : IDVV-3XXXXX[-ZZ][D]
                strPattern = "IDVV-3"
                strTmp = strInput.Substring(0, strPattern.Length)
                If (strTmp.Equals(strPattern)) Then
                    bValid = True
                End If
                'Dev Mode
                If (strInput.Last() = "D"c) Then
                    m_bIsDevMode = True
                Else
                    m_bIsDevMode = False
                End If
                'Digit 7= '1' --> SRED mode
                If (strInput.ElementAt(7) = "1"c) Then
                    m_bIsSREDSupported = True
                Else
                    m_bIsSREDSupported = False
                End If

            Case ENUM_PD_IMG_IDX.NEO_VENDI_V0, ENUM_PD_IMG_IDX.NEO_VENDI_NEW
                'Vendi, CL, MSR, LCD (Font Only), LED (Green x 4, Blue x 3, MSR x 1), Buttons x 2 [Done][Swipe Card]
                '--------------------------------------------------------
                'IDVV-120101 = Vendi;NFC;T12;Std
                'Length >= 11
                nVal = strInput.Length - 11
                If (nVal < 0) Then
                    Exit Select
                End If
                'Format : IDVV-1XXXXX[-ZZ][D]
                strPattern = "IDVV-1"
                strTmp = strInput.Substring(0, strPattern.Length)
                If (strTmp.Equals(strPattern)) Then
                    bValid = True
                End If
                'Dev Mode
                If (strInput.Last() = "D"c) Then
                    m_bIsDevMode = True
                Else
                    m_bIsDevMode = False
                End If
                'Digit 7= '1' --> SRED mode
                If (strInput.ElementAt(7) = "1"c) Then
                    m_bIsSREDSupported = True
                Else
                    m_bIsSREDSupported = False
                End If

            Case ENUM_PD_IMG_IDX.NEO_VP3300USB, ENUM_PD_IMG_IDX.NEO_VP3300USB_C, ENUM_PD_IMG_IDX.NEO_VP3300USB_E
                '--------------------------------------------------------
                'VP4880	White	IDVP-31
                'VP4880E	White	IDVP-21
                'VP4880C	White	IDVP-11
                'VP4880OEM	White	IDVP-61

                'IDVP-XX, Min Length
                'Length >= 5
                nVal = strInput.Length - 5
                If (nVal < 0) Then
                    Exit Select
                End If
                '
                If (Not strInput.Contains("IDVP-")) Then
                    Exit Select
                End If
                bValid = True
                '--------------------------------------------------------
            Case ENUM_PD_IMG_IDX.NEO_VP3300_AUDIO_JACK, ENUM_PD_IMG_IDX.NEO_UNIPAY_III_iBase, ENUM_PD_IMG_IDX.NEO_UNIPAY_1D5
                '--------------------------------------------------------
                'ID-80149003-001
                'IDMR-AB83133 = 
                'IDMR-AB83133S = 
                'IDMR-AB93133 = 
                'IDMR-AB93133W = 
                '--------------------------------------------------------
                'Audio Jack 3-in-1(i.e. Vendi 3-in-1)
                '--------------------------------------------------------
                'IDMR-AB93133, Min Length
                'Length >= 12
                nVal = strInput.Length - 12
                If (nVal < 0) Then
                    Exit Select
                End If
                '
                If (Not strInput.Contains("IDMR-")) Then
                    Exit Select
                End If
                bValid = True
            Case Else
                'Other Case
                bValid = True
        End Select
        '
        Return bValid
    End Function


    Public ReadOnly Property bIsUIEnable_EMEA As Boolean
        Get
            Return m_byteUIScheme = tagTLV.ENUM_FFF8_UI_SCHEME.x03_EMEA
        End Get
    End Property
    Public ReadOnly Property bIsUIEnable_VisaWave As Boolean
        Get
            Return m_byteUIScheme = tagTLV.ENUM_FFF8_UI_SCHEME.x02_VISA_WAVE
        End Get
    End Property
    Public ReadOnly Property bIsUIEnable_ViVO As Boolean
        Get
            Return m_byteUIScheme = tagTLV.ENUM_FFF8_UI_SCHEME.x00_VIVOPAY
        End Get
    End Property
    Public ReadOnly Property bIsUIEnable_Unknown As Boolean
        Get
            Return m_byteUIScheme = tagTLV.ENUM_FFF8_UI_SCHEME.xFF_UNKNOWN
        End Get
    End Property


    Public ReadOnly Property refobjRdrCmder As ClassReaderProtocolBasic
        Get
            Return m_refobjRdrCmder
        End Get
    End Property


    Public ReadOnly Property bIsLCDSupported As Boolean
        Get
            Return m_bIsLCDSupported
        End Get
    End Property

    Public ReadOnly Property bIsLCDContrastSupported As Boolean
        Get
            Return m_bIsLCDContrastSupported
        End Get
    End Property

    Public ReadOnly Property bIsBLESupported As Boolean
        Get
            Select Case (m_PD_IMG)
                Case ENUM_PD_IMG_IDX.NEO_VP3300Bluetooth
                    Return True
            End Select
            Return False
        End Get
    End Property
    '
    Public ReadOnly Property bIsDateTimeSupported_RTCNoBattery As Boolean
        Get
            Return m_bIsDateTimeSupported_RTCNoBattery
        End Get
    End Property

    '
    Public ReadOnly Property bIsDateTimeSupported As Boolean
        Get
            Return m_bIsDateTimeSupported
        End Get
    End Property

    Public ReadOnly Property bIsButtonsCfgSupported As Boolean
        Get
            Return m_bIsButtonsCfgSupported
        End Get
    End Property
    '
    Public ReadOnly Property bIsKeyPadSupported As Boolean
        Get
            'Select Case (m_PD_IMG)
            '    Case ENUM_PD_IMG_IDX.NEO_VENDI_V0, ENUM_PD_IMG_IDX.NEO_VENDI_NEW, ENUM_PD_IMG_IDX.AR_HG4_VENDIII, ENUM_PD_IMG_IDX.AR_HG4_VENDIII_AR_2_1_5, ENUM_PD_IMG_IDX.AR_PISCES, ENUM_PD_IMG_IDX.AR_PISCES_V0
            '        Return True
            'End Select
            ''
            'Return False

            Return m_bIsKeyPadSupported
        End Get
    End Property
    '
    Public ReadOnly Property bIsLEDsGreenSupport As Boolean
        Get
            Return m_bIsLEDsGreenSupport
        End Get
    End Property
    Public ReadOnly Property bIsLEDsBlueSupport As Boolean
        Get
            Return m_bIsLEDsBlueSupport
        End Get
    End Property
    Public ReadOnly Property bIsLEDsMSRSupport As Boolean
        Get
            Return m_bIsLEDsMSRSupport
        End Get
    End Property

    Public ReadOnly Property bIsLEDsPowerChargedSupport As Boolean
        Get
            Select Case (m_PD_IMG)
                Case ENUM_PD_IMG_IDX.NEO_VP3300_AUDIO_JACK, ENUM_PD_IMG_IDX.NEO_UNIPAY_III_iBase, ENUM_PD_IMG_IDX.NEO_VP3300Bluetooth
                    Return True
                    '
                Case ENUM_PD_IMG_IDX.NEO_VP3300USB, ENUM_PD_IMG_IDX.NEO_VP3300USB_C, ENUM_PD_IMG_IDX.NEO_VP3300USB_E
                    'No Power LED
                    Return False
            End Select
            '
            Return False
        End Get
    End Property




    Public ReadOnly Property GetTestButtonsEnableAll As Byte()
        Get
            Return m_arrayByteTestButtonsConfigEnableAll
        End Get
    End Property

    Public ReadOnly Property GetUIScheme As tagTLV.ENUM_FFF8_UI_SCHEME
        Get
            Select Case (m_PD_IMG)
                Case ENUM_PD_IMG_IDX.NEO_VENDI_NEW, ENUM_PD_IMG_IDX.NEO_VENDI_V0, ENUM_PD_IMG_IDX.NEO_VP3300_AUDIO_JACK, ENUM_PD_IMG_IDX.NEO_UNIPAY_III_iBase, ENUM_PD_IMG_IDX.NEO_UNIPAY_1D5, ENUM_PD_IMG_IDX.NEO_VP3300USB, ENUM_PD_IMG_IDX.NEO_VP3300USB_C, ENUM_PD_IMG_IDX.NEO_VP3300USB_E, ENUM_PD_IMG_IDX.NEO_UNIPAY_1D5_TTK, ENUM_PD_IMG_IDX.NEO_VP3300Bluetooth
                    m_byteUIScheme = tagTLV.ENUM_FFF8_UI_SCHEME.x00_VIVOPAY
            End Select
            ' Others user default
            Return m_byteUIScheme
        End Get
    End Property
    '
    Public WriteOnly Property SetUIScheme As tagTLV.ENUM_FFF8_UI_SCHEME
        Set(value As tagTLV.ENUM_FFF8_UI_SCHEME)
            m_byteUIScheme = value
        End Set
    End Property
    '
    ReadOnly Property bIsMSRAvailableTrack1 As Boolean
        Get
            'Select Case (m_PD_IMG)
            '    Case ENUM_PD_IMG_IDX.NEO_VENDI_NEW, ENUM_PD_IMG_IDX.AR_HG4_VENDIII, ENUM_PD_IMG_IDX.NEO_VENDI_V0, ENUM_PD_IMG_IDX.NEO_UNIPAY_III, ENUM_PD_IMG_IDX.NEO_UNIPAY_III_iBase, ENUM_PD_IMG_IDX.NEO_UNIPAY_1D5, ENUM_PD_IMG_IDX.AR_HG4_VENDIII_AR_2_1_5, ENUM_PD_IMG_IDX.AR_PISCES, ENUM_PD_IMG_IDX.AR_PISCES_V0, ENUM_PD_IMG_IDX.NEO_VP4880
            '        Return True
            '    Case ENUM_PD_IMG_IDX.NEO_VP4880C, ENUM_PD_IMG_IDX.NEO_VP4880E

            'End Select
            'Return False
            '
            Return m_bIsMSRTk1
        End Get
    End Property


    ReadOnly Property bIsMSRAvailableTrack2 As Boolean
        Get
            'Select Case (m_PD_IMG)
            '    Case ENUM_PD_IMG_IDX.NEO_VENDI_NEW, ENUM_PD_IMG_IDX.AR_HG4_VENDIII, ENUM_PD_IMG_IDX.NEO_VENDI_V0, ENUM_PD_IMG_IDX.NEO_UNIPAY_III, ENUM_PD_IMG_IDX.NEO_UNIPAY_III_iBase, ENUM_PD_IMG_IDX.NEO_UNIPAY_1D5, ENUM_PD_IMG_IDX.AR_HG4_VENDIII_AR_2_1_5, ENUM_PD_IMG_IDX.AR_PISCES, ENUM_PD_IMG_IDX.AR_PISCES_V0, ENUM_PD_IMG_IDX.NEO_VP4880
            '        Return True
            'End Select
            'Return False

            '
            Return m_bIsMSRTk2
        End Get
    End Property

    ReadOnly Property bIsMSRAvailableTrack3 As Boolean
        Get
            'Select Case (m_PD_IMG)
            '    Case ENUM_PD_IMG_IDX.AR_HG4_VENDIII, ENUM_PD_IMG_IDX.NEO_UNIPAY_III, ENUM_PD_IMG_IDX.NEO_UNIPAY_III_iBase, ENUM_PD_IMG_IDX.NEO_UNIPAY_1D5, ENUM_PD_IMG_IDX.AR_HG4_VENDIII_AR_2_1_5, ENUM_PD_IMG_IDX.AR_PISCES, ENUM_PD_IMG_IDX.AR_PISCES_V0, ENUM_PD_IMG_IDX.NEO_VP4880
            '        Return True
            'End Select
            'Return False
            '
            Return m_bIsMSRTk3
        End Get
    End Property

    ReadOnly Property bIsICCAvailable As Boolean
        Get
            Select Case (m_PD_IMG)
                Case ENUM_PD_IMG_IDX.NEO_VP3300_AUDIO_JACK, ENUM_PD_IMG_IDX.NEO_UNIPAY_III_iBase, ENUM_PD_IMG_IDX.NEO_UNIPAY_1D5, ENUM_PD_IMG_IDX.NEO_VP3300USB, ENUM_PD_IMG_IDX.NEO_VP3300USB_E, ENUM_PD_IMG_IDX.NEO_UNIPAY_1D5_TTK, ENUM_PD_IMG_IDX.NEO_VP3300Bluetooth
                    Return True
                Case ENUM_PD_IMG_IDX.AR_HG4_VENDIII
                    Return True
                Case ENUM_PD_IMG_IDX.AR_PISCES, ENUM_PD_IMG_IDX.AR_PISCES_V0, ENUM_PD_IMG_IDX.AR_HG4_VENDIII_AR_2_1_5
                    Return True
                Case ENUM_PD_IMG_IDX.NEO_20_VP6300_VENDI_PRO, ENUM_PD_IMG_IDX.NEO_20_VP5300_SPTP_II
                    Return True
                Case ENUM_PD_IMG_IDX.NEO_VP3300USB_C
                    Return False
            End Select
            Return False
        End Get
    End Property

    ReadOnly Property bIsPICCAvailable As Boolean
        Get
            '==============================================
            ' All of Products are PICC/PCD Available...
            'Select Case (m_PD_IMG)
            '    Case ENUM_PD_IMG_IDX.NEO_UNIPAY_III, ENUM_PD_IMG_IDX.AR_HG4_VENDIII
            '        Return False
            'End Select
            Select Case (m_PD_IMG)
                Case ENUM_PD_IMG_IDX.NEO_UNIPAY_1D5, ENUM_PD_IMG_IDX.NEO_UNIPAY_1D5_TTK
                    Return False
            End Select
            Return True
        End Get
    End Property


   
    Public Sub Init()
        ' =====[ Revision / History ]========
        ' 2015 Sept 11, goofy_liu@idtechproducts.com.tw
        ' 1.Add Production ID to Name String Mapping - Multiple 2 One(Single One)
        '--------------[ Customized Area ]--------------
        'Firmware Version 
        '-------[ ToDo Note ]-------
        ' AR_PISCES Key String @Product Tree ID is TBD
        '2016 June 20.
        'https://idtech01.managed.contegix.com/confluence/display/FW/FW+Versioning
        m_dictProductTree = New SortedDictionary(Of String, SortedDictionary(Of String, Integer)) From { _
            {"AR", New SortedDictionary(Of String, Integer) From {{"HG4 AR 2.1.5", ENUM_PD_IMG_IDX.AR_HG4_VENDIII_AR_2_1_5}, {"HG4", ENUM_PD_IMG_IDX.AR_HG4_VENDIII}, {"HG3", ENUM_PD_IMG_IDX.AR_HG3_VP4800}, {"HG2", ENUM_PD_IMG_IDX.AR_HG2_VP8100}, {"HG1", ENUM_PD_IMG_IDX.AR_HG1_VP8800}, {"HG0", ENUM_PD_IMG_IDX.AR_HG0_VP8600}, {"PISCES", ENUM_PD_IMG_IDX.AR_PISCES}, {"PISCES Bad PID", ENUM_PD_IMG_IDX.AR_PISCES_V0}} _
            } _
            , {"GR", New SortedDictionary(Of String, Integer) From {{"EC3", ENUM_PD_IMG_IDX.GR_EC3_VP5000}, {"EB7", ENUM_PD_IMG_IDX.GR_EB7_VP5000}, {"EC7", ENUM_PD_IMG_IDX.GR_EC7_KIOSKI}, {"ECA", ENUM_PD_IMG_IDX.GR_ECA_KIOSKII} _
                , {"EC8", ENUM_PD_IMG_IDX.GR_EC8_KIOSKII}} _
            } _
            , {"NEO", New SortedDictionary(Of String, Integer) From {{"Vendi", ENUM_PD_IMG_IDX.NEO_VENDI_NEW}, _
            {"Unipay III-iBase", ENUM_PD_IMG_IDX.NEO_UNIPAY_III_iBase}, {"UnipayIII", ENUM_PD_IMG_IDX.NEO_VP3300_AUDIO_JACK}, _
            {"VP3300 Audio Jack", ENUM_PD_IMG_IDX.NEO_VP3300_AUDIO_JACK}, _
            {"UnipayI_V", ENUM_PD_IMG_IDX.NEO_UNIPAY_1D5}, _
            {"VPBTPay Mini", ENUM_PD_IMG_IDX.NEO_VP3300Bluetooth}, _
            {"VP3300 USB-E", ENUM_PD_IMG_IDX.NEO_VP3300USB_E}, {"VP3300 USB-C", ENUM_PD_IMG_IDX.NEO_VP3300USB_C}, {"VP3300 USB", ENUM_PD_IMG_IDX.NEO_VP3300USB} _
            } _
            } _
           , {"VP5300", New SortedDictionary(Of String, Integer) From {{"VP5300", ENUM_PD_IMG_IDX.NEO_20_VP5300_SPTP_II} _
            } _
            } _
           , {"VP6300", New SortedDictionary(Of String, Integer) From {{"VP6300", ENUM_PD_IMG_IDX.NEO_20_VP6300_VENDI_PRO} _
            } _
            } _
            }
#If 1 Then
        m_dictProductPicList = New Dictionary(Of ENUM_PD_IMG_IDX, Image) From { _
            {ENUM_PD_IMG_IDX.NEO_UNIPAY_1D5, Global.Main.My.Resources.Resources.Unipay_1_dot_5_100x92}, _
            {ENUM_PD_IMG_IDX.AR_HG4_VENDIII, Global.Main.My.Resources.Resources.UnknownDevice},
            {ENUM_PD_IMG_IDX.AR_HG4_VENDIII_AR_2_1_5, Global.Main.My.Resources.Resources.UnknownDevice}, _
            {ENUM_PD_IMG_IDX.NEO_20_VP5300_SPTP_II, Global.Main.My.Resources.Resources.UnknownDevice}, _
            {ENUM_PD_IMG_IDX.NEO_20_VP6300_VENDI_PRO, Global.Main.My.Resources.Resources.UnknownDevice}, _
            {ENUM_PD_IMG_IDX.NEO_VENDI_V0, Global.Main.My.Resources.Resources.UnknownDevice}, _
            {ENUM_PD_IMG_IDX.NEO_VENDI_NEW, Global.Main.My.Resources.Resources.UnknownDevice}, _
            {ENUM_PD_IMG_IDX.AR_HG2_VP8100, Global.Main.My.Resources.Resources.UnknownDevice}, _
            {ENUM_PD_IMG_IDX.AR_HG3_VP4800, Global.Main.My.Resources.Resources.UnknownDevice}, _
            {ENUM_PD_IMG_IDX.AR_HG0_VP8600, Global.Main.My.Resources.Resources.UnknownDevice}, _
            {ENUM_PD_IMG_IDX.AR_HG1_VP8800, Global.Main.My.Resources.Resources.UnknownDevice}, _
            {ENUM_PD_IMG_IDX.GR_EC7_KIOSKI, Global.Main.My.Resources.Resources.UnknownDevice}, _
            {ENUM_PD_IMG_IDX.GR_ECA_KIOSKII, Global.Main.My.Resources.Resources.UnknownDevice}, _
            {ENUM_PD_IMG_IDX.GR_EC8_KIOSKII, Global.Main.My.Resources.Resources.UnknownDevice}, _
            {ENUM_PD_IMG_IDX.AR_PISCES, Global.Main.My.Resources.Resources.UnknownDevice}, _
            {ENUM_PD_IMG_IDX.AR_PISCES_V0, Global.Main.My.Resources.Resources.UnknownDevice}, _
            {ENUM_PD_IMG_IDX.NEO_VP3300_AUDIO_JACK, Global.Main.My.Resources.Resources.UnknownDevice}, _
            {ENUM_PD_IMG_IDX.NEO_UNIPAY_III_iBase, Global.Main.My.Resources.Resources.UnknownDevice}, _
            {ENUM_PD_IMG_IDX.NEO_VP3300Bluetooth, Global.Main.My.Resources.Resources.UnknownDevice}, _
            {ENUM_PD_IMG_IDX.NEO_UNIPAY_1D5_TTK, Global.Main.My.Resources.Resources.UnknownDevice}, _
            {ENUM_PD_IMG_IDX.NEO_VP3300USB, Global.Main.My.Resources.Resources.UnknownDevice}, _
            {ENUM_PD_IMG_IDX.NEO_VP3300USB_E, Global.Main.My.Resources.Resources.UnknownDevice}, _
            {ENUM_PD_IMG_IDX.NEO_VP3300USB_C, Global.Main.My.Resources.Resources.UnknownDevice}, _
            {ENUM_PD_IMG_IDX.GR_EC3_VP5000, Global.Main.My.Resources.Resources.UnknownDevice}, _
            {ENUM_PD_IMG_IDX.GR_EB7_VP5000, Global.Main.My.Resources.Resources.UnknownDevice}, _
            {ENUM_PD_IMG_IDX.UNKNOWN_MAX, Global.Main.My.Resources.Resources.UnknownDevice} _
           }
#Else
           m_dictProductPicList = New Dictionary(Of ENUM_PD_IMG_IDX, Image) From { _
            {ENUM_PD_IMG_IDX.NEO_UNIPAY_1D5, Global.Main.My.Resources.Resources.Unipay_1_dot_5_100x92}, _
            {ENUM_PD_IMG_IDX.UNKNOWN_MAX, Global.Main.My.Resources.Resources.UnknownDevice} _
            {ENUM_PD_IMG_IDX.AR_HG4_VENDIII, Global.Main.My.Resources.Resources.VendIII105x128_Name},
            {ENUM_PD_IMG_IDX.AR_HG4_VENDIII_AR_2_1_5, Global.Main.My.Resources.Resources.VendIII105x128_Name}, _
            {ENUM_PD_IMG_IDX.NEO_20_VP5300_SPTP_II, Global.Main.My.Resources.Resources.VP5300_107x74}, _
            {ENUM_PD_IMG_IDX.NEO_20_VP6300_VENDI_PRO, Global.Main.My.Resources.Resources.VP6300_97x105}, _
            {ENUM_PD_IMG_IDX.NEO_VENDI_V0, Global.Main.My.Resources.Resources.Vend96x99_Name}, _
            {ENUM_PD_IMG_IDX.NEO_VENDI_NEW, Global.Main.My.Resources.Resources.Vend96x99_Name}, _
            {ENUM_PD_IMG_IDX.AR_HG2_VP8100, Global.Main.My.Resources.Resources.UnknownDevice}, _
            {ENUM_PD_IMG_IDX.AR_HG3_VP4800, Global.Main.My.Resources.Resources.VP4800_99x117}, _
            {ENUM_PD_IMG_IDX.AR_HG0_VP8600, Global.Main.My.Resources.Resources.UnknownDevice}, _
            {ENUM_PD_IMG_IDX.AR_HG1_VP8800, Global.Main.My.Resources.Resources.UnknownDevice}, _
            {ENUM_PD_IMG_IDX.GR_EC7_KIOSKI, Global.Main.My.Resources.Resources.UnknownDevice}, _
            {ENUM_PD_IMG_IDX.GR_ECA_KIOSKII, Global.Main.My.Resources.Resources.KIOSKII_100x121}, _
            {ENUM_PD_IMG_IDX.GR_EC8_KIOSKII, Global.Main.My.Resources.Resources.KIOSKII_100x121}, _
            {ENUM_PD_IMG_IDX.AR_PISCES, Global.Main.My.Resources.Resources.PISCES_75x150}, _
            {ENUM_PD_IMG_IDX.AR_PISCES_V0, Global.Main.My.Resources.Resources.PISCES_75x150}, _
            {ENUM_PD_IMG_IDX.NEO_VP3300_AUDIO_JACK, Global.Main.My.Resources.Resources.UnipayIII_85x143}, _
            {ENUM_PD_IMG_IDX.NEO_UNIPAY_III_iBase, Global.Main.My.Resources.Resources.UnipayIII_85x143}, _
            {ENUM_PD_IMG_IDX.NEO_VP3300Bluetooth, Global.Main.My.Resources.Resources.BTPayMini_81x107}, _
            {ENUM_PD_IMG_IDX.NEO_UNIPAY_1D5_TTK, Global.Main.My.Resources.Resources.Unipay_1_dot_5_TTK_100x83}, _
            {ENUM_PD_IMG_IDX.NEO_VP3300USB, Global.Main.My.Resources.Resources.VP4880_85x143}, _
            {ENUM_PD_IMG_IDX.NEO_VP3300USB_E, Global.Main.My.Resources.Resources.VP4880_85x143}, _
            {ENUM_PD_IMG_IDX.NEO_VP3300USB_C, Global.Main.My.Resources.Resources.VP4880_85x143}, _
            {ENUM_PD_IMG_IDX.GR_EC3_VP5000, Global.Main.My.Resources.Resources.UnknownDevice}, _
            {ENUM_PD_IMG_IDX.GR_EB7_VP5000, Global.Main.My.Resources.Resources.UnknownDevice}, 
#End If



        m_dictPlatformNameStr2EnuVal = New SortedDictionary(Of String, ENU_FW_PLATFORM_TYPE) From {
              {"VP6300", ENU_FW_PLATFORM_TYPE.NEO_IDG} _
            , {"VP5300", ENU_FW_PLATFORM_TYPE.NEO_IDG} _
            , {"AR", ENU_FW_PLATFORM_TYPE.AR}, {"GR", ENU_FW_PLATFORM_TYPE.GR} _
            , {"FW", ENU_FW_PLATFORM_TYPE.FW}, {"NEO", ENU_FW_PLATFORM_TYPE.NEO_IDG} _
            , {"Unknown", ENU_FW_PLATFORM_TYPE.UNKNOWN}
            }

        'Unipay 1.5 = UnipayI_V-BL-V3.00.001
        'Unipay III = UnipayIII-BL-V3.00.003
        'Vendi = Vendi-BL-V3.00.006

        Dim pkv As KeyValuePair(Of ENUM_PD_IMG_IDX, ENU_FW_PLATFORM_TYPE) = New KeyValuePair(Of ENUM_PD_IMG_IDX, ENU_FW_PLATFORM_TYPE)(ENUM_PD_IMG_IDX.AR_HG0_VP8600, ENU_FW_PLATFORM_TYPE.AR)

        m_dictBootloaderVerStr2Platform = New SortedDictionary(Of String, KeyValuePair(Of ENUM_PD_IMG_IDX, ENU_FW_PLATFORM_TYPE)) From {
            {"KH0 AR", New KeyValuePair(Of ENUM_PD_IMG_IDX, ENU_FW_PLATFORM_TYPE)(ENUM_PD_IMG_IDX.AR_PISCES, ENU_FW_PLATFORM_TYPE.AR)} _
            , {"Vend III", New KeyValuePair(Of ENUM_PD_IMG_IDX, ENU_FW_PLATFORM_TYPE)(ENUM_PD_IMG_IDX.AR_HG4_VENDIII, ENU_FW_PLATFORM_TYPE.AR)}, {"KIOSK", New KeyValuePair(Of ENUM_PD_IMG_IDX, ENU_FW_PLATFORM_TYPE)(ENUM_PD_IMG_IDX.GR_EC8_KIOSKII, ENU_FW_PLATFORM_TYPE.GR)} _
            , {"Vendi", New KeyValuePair(Of ENUM_PD_IMG_IDX, ENU_FW_PLATFORM_TYPE)(ENUM_PD_IMG_IDX.NEO_VENDI_V0, ENU_FW_PLATFORM_TYPE.NEO_IDG)} _
            , {"UnipayIII", New KeyValuePair(Of ENUM_PD_IMG_IDX, ENU_FW_PLATFORM_TYPE)(ENUM_PD_IMG_IDX.NEO_VP3300_AUDIO_JACK, ENU_FW_PLATFORM_TYPE.NEO_IDG)} _
            , {"BTPAY_MINI", New KeyValuePair(Of ENUM_PD_IMG_IDX, ENU_FW_PLATFORM_TYPE)(ENUM_PD_IMG_IDX.NEO_VP3300Bluetooth, ENU_FW_PLATFORM_TYPE.NEO_IDG)} _
            , {"UnipayI_V", New KeyValuePair(Of ENUM_PD_IMG_IDX, ENU_FW_PLATFORM_TYPE)(ENUM_PD_IMG_IDX.NEO_UNIPAY_1D5, ENU_FW_PLATFORM_TYPE.NEO_IDG)} _
            , {"VP4880", New KeyValuePair(Of ENUM_PD_IMG_IDX, ENU_FW_PLATFORM_TYPE)(ENUM_PD_IMG_IDX.NEO_VP3300USB, ENU_FW_PLATFORM_TYPE.NEO_IDG)} _
            , {"VP4880E", New KeyValuePair(Of ENUM_PD_IMG_IDX, ENU_FW_PLATFORM_TYPE)(ENUM_PD_IMG_IDX.NEO_VP3300USB_E, ENU_FW_PLATFORM_TYPE.NEO_IDG)} _
            , {"VP4880C", New KeyValuePair(Of ENUM_PD_IMG_IDX, ENU_FW_PLATFORM_TYPE)(ENUM_PD_IMG_IDX.NEO_VP3300USB_C, ENU_FW_PLATFORM_TYPE.NEO_IDG)} _
            , {"VP5300", New KeyValuePair(Of ENUM_PD_IMG_IDX, ENU_FW_PLATFORM_TYPE)(ENUM_PD_IMG_IDX.NEO_20_VP5300_SPTP_II, ENU_FW_PLATFORM_TYPE.NEO_IDG)} _
            , {"VP6300", New KeyValuePair(Of ENUM_PD_IMG_IDX, ENU_FW_PLATFORM_TYPE)(ENUM_PD_IMG_IDX.NEO_20_VP6300_VENDI_PRO, ENU_FW_PLATFORM_TYPE.NEO_IDG)} _
            , {"Unknown", New KeyValuePair(Of ENUM_PD_IMG_IDX, ENU_FW_PLATFORM_TYPE)(ENUM_PD_IMG_IDX.UNKNOWN_MAX, ENU_FW_PLATFORM_TYPE.UNKNOWN)}
            }
        m_listPlatformGBCMDefOn = New List(Of ENUM_PD_IMG_IDX) From {
            ENUM_PD_IMG_IDX.AR_HG4_VENDIII
            }

        '
        m_pRefMF = Form01_Main.GetInstance()

        '
        m_arrayByteTestButtonsConfigEnableAll = New Byte() {&H1, &H1, &HF}

        'Vendi Default Value, 2015 Mar 25
        m_arrayByteTestButtonsConfigDefault = New Byte() {&H1, &H1, &HF}

        'Get Reader <--> TS mapping
        'Note: Using "From" Keyword @ VB, the comma "," must be at end of line, can't be the first char of line
        m_tblTSName = New SortedDictionary(Of ClassReaderInfo.ENUM_PD_IMG_IDX, String) From {
        {ENUM_PD_IMG_IDX.AR_HG4_VENDIII, "TS128"},
        {ENUM_PD_IMG_IDX.AR_HG4_VENDIII_AR_2_1_5, "TS128"},
        {ENUM_PD_IMG_IDX.AR_PISCES, "TS158"},
        {ENUM_PD_IMG_IDX.AR_HG3_VP4800, "TS Unknown"},
        {ENUM_PD_IMG_IDX.AR_HG2_VP8100, "TS Unknown"},
        {ENUM_PD_IMG_IDX.AR_HG1_VP8800, "TS Unknown"},
        {ENUM_PD_IMG_IDX.AR_HG0_VP8600, "TS Unknown"},
        {ENUM_PD_IMG_IDX.GR_EC3_VP5000, "TS Unknown"},
        {ENUM_PD_IMG_IDX.GR_EB7_VP5000, "TS Unknown"},
        {ENUM_PD_IMG_IDX.GR_EC7_KIOSKI, "TS Unknown"},
        {ENUM_PD_IMG_IDX.GR_EC8_KIOSKII, "TS Unknown"},
        {ENUM_PD_IMG_IDX.GR_ECA_KIOSKII, "TS Unknown"},
        {ENUM_PD_IMG_IDX.NEO_VENDI_V0, "TS129"},
        {ENUM_PD_IMG_IDX.NEO_VENDI_NEW, "TS156"},
        {ENUM_PD_IMG_IDX.NEO_VP3300_AUDIO_JACK, "TS146"},
        {ENUM_PD_IMG_IDX.NEO_UNIPAY_III_iBase, "TS146"},
        {ENUM_PD_IMG_IDX.NEO_UNIPAY_1D5, "TS146"},
        {ENUM_PD_IMG_IDX.NEO_UNIPAY_1D5_TTK, "TS165"},
        {ENUM_PD_IMG_IDX.NEO_VP3300USB, "TS160"},
        {ENUM_PD_IMG_IDX.NEO_VP3300USB_C, "TS160"},
        {ENUM_PD_IMG_IDX.NEO_VP3300USB_E, "TS160"},
        {ENUM_PD_IMG_IDX.NEO_VP3300Bluetooth, "TS166"},
        {ENUM_PD_IMG_IDX.NEO_20_VP6300_VENDI_PRO, "TS172"}
        }
        'Note : Unipay III has couple of TS... 
        '1st TS = TS145 For Unipay III with Audio Jack + USBHID
        '2nd TS = TS146 For Unipay III with USBHID ONLY (Project iBase 
        '
        m_refsdictTLVsDefault = Nothing
        '
        '======[ Create System AID List ]=======
        m_listSystemAID = New List(Of String) From {"A00000002501", "A0000000041010", "A0000000659001", "A0000000043060", "A0000000031010", "A0000000032010", "A0000000033010", "A0000000651010", "A0000003241010", "A0000000023060D15800", "A0000004170101", "A0000002771010", "A0000001523010"}

        '
        m_bIsForcedReset = False
        'Other Stuffs
        ReInit()

    End Sub
    '
    '===========================[ Properties Area ]===========================
    ReadOnly Property bIsDefAutoPollType As Boolean
        Get
            Select Case (m_PD_IMG)
                Case ENUM_PD_IMG_IDX.GR_EC8_KIOSKII, ENUM_PD_IMG_IDX.GR_ECA_KIOSKII, ENUM_PD_IMG_IDX.AR_HG3_VP4800
                    Return True
                Case ENUM_PD_IMG_IDX.NEO_VP3300_AUDIO_JACK, ENUM_PD_IMG_IDX.NEO_UNIPAY_III_iBase, ENUM_PD_IMG_IDX.NEO_UNIPAY_1D5, ENUM_PD_IMG_IDX.NEO_VP3300USB, ENUM_PD_IMG_IDX.NEO_VP3300USB_C, ENUM_PD_IMG_IDX.NEO_VP3300USB_E, ENUM_PD_IMG_IDX.NEO_UNIPAY_1D5_TTK, ENUM_PD_IMG_IDX.NEO_VP3300Bluetooth
                    Return False
                Case ENUM_PD_IMG_IDX.NEO_VENDI_V0, ENUM_PD_IMG_IDX.NEO_VENDI_NEW
                    'Poll On Demand, Venei Default Setting_20150323_01.docx, made by SQA
                    Return True
            End Select
            '
            Return False
        End Get
    End Property

    ReadOnly Property bIsDefGBCM As Boolean
        Get
            Return (m_listPlatformGBCMDefOn.Contains(m_PD_IMG))
        End Get
    End Property

    Public ReadOnly Property bIsDefBurstMode As ClassReaderProtocolBasic.TAG_BURST_MODE
        Get
            Select Case (m_PD_IMG)
                Case ENUM_PD_IMG_IDX.AR_HG3_VP4800
                    Return ClassReaderProtocolBasic.TAG_BURST_MODE._x01_ON
                Case ENUM_PD_IMG_IDX.NEO_VENDI_NEW, ENUM_PD_IMG_IDX.NEO_VENDI_V0
                    Return ClassReaderProtocolBasic.TAG_BURST_MODE._x02_AUTOEXIT
                Case ENUM_PD_IMG_IDX.NEO_VP3300_AUDIO_JACK, ENUM_PD_IMG_IDX.NEO_UNIPAY_III_iBase, ENUM_PD_IMG_IDX.NEO_VP3300USB, ENUM_PD_IMG_IDX.NEO_VP3300USB_C, ENUM_PD_IMG_IDX.NEO_VP3300USB_E, ENUM_PD_IMG_IDX.NEO_UNIPAY_1D5_TTK, ENUM_PD_IMG_IDX.NEO_UNIPAY_1D5, ENUM_PD_IMG_IDX.NEO_VP3300Bluetooth
                    Return ClassReaderProtocolBasic.TAG_BURST_MODE._x00_OFF 'TODO the final mode
            End Select
            '
            Return m_bIsDefBurstMode
            'Default Should be False, but somehow in Engineer Mode it default value may be changed for test purpose. Please be aware in Engineer Mode.
        End Get
    End Property
    '
    Public WriteOnly Property SetDefBurstMode As ClassReaderProtocolBasic.TAG_BURST_MODE
        Set(value As ClassReaderProtocolBasic.TAG_BURST_MODE)
            m_bIsDefBurstMode = value
        End Set
    End Property

    Public Function bIsPlatform(ByVal enPlatform As ENU_FW_PLATFORM_TYPE)
        Return (m_nFwPlatform = enPlatform)
    End Function
    '
    ReadOnly Property bIsPlatformNEO_2014 As Boolean
        Get
            Return (m_nFwPlatform = ENU_FW_PLATFORM_TYPE.NEO_IDG)
        End Get
    End Property
    '
    ReadOnly Property bIsPlatformGR_2013 As Boolean
        Get
            Return (m_nFwPlatform = ENU_FW_PLATFORM_TYPE.GR) Or (m_nFwPlatform = ENU_FW_PLATFORM_TYPE.NEO_IDG)
        End Get
    End Property
    ReadOnly Property bIsPlatformAR_2013 As Boolean
        Get
            Return (m_nFwPlatform = ENU_FW_PLATFORM_TYPE.AR)
        End Get
    End Property

    '================[ Product Type Check ]====================
    ReadOnly Property bIsPlatformNEO_Vendi_2015 As Boolean
        Get
            Return ((m_PD_IMG = ENUM_PD_IMG_IDX.NEO_VENDI_V0) Or (m_PD_IMG = ENUM_PD_IMG_IDX.NEO_VENDI_NEW))
        End Get
    End Property
    ReadOnly Property bIsPlatformGR_KioskII_2014 As Boolean
        Get
            Return ((m_PD_IMG = ENUM_PD_IMG_IDX.GR_ECA_KIOSKII) Or (m_PD_IMG = ENUM_PD_IMG_IDX.GR_EC8_KIOSKII))
        End Get
    End Property

    ReadOnly Property bIsPlatformAR_VendIII_2014 As Boolean
        Get
            Return (m_PD_IMG = ENUM_PD_IMG_IDX.AR_HG4_VENDIII)
        End Get
    End Property

    ReadOnly Property bIsPlatformARVendIII_AR215_2014 As Boolean
        Get
            Return (m_PD_IMG = ENUM_PD_IMG_IDX.AR_HG4_VENDIII_AR_2_1_5)
        End Get
    End Property

    ReadOnly Property bIsPlatformUnknown As Boolean
        Get
            Return (m_nFwPlatform = ENU_FW_PLATFORM_TYPE.UNKNOWN)
        End Get
    End Property

    'ReadOnly Property bIsGCBMType As Boolean
    '    Get
    '        If (m_PD_IMG = ENUM_PD_IMG_IDX.AR_HG4_VENDIII) Then
    '            Return True
    '        End If
    '        If (m_PD_IMG = ENUM_PD_IMG_IDX.AR_HG3_VP4800) Then
    '            Return True
    '        End If

    '        Return False
    '    End Get
    'End Property


    Public Sub ReInit()
        'Default Reader Production is VendIII
        m_PD_IMG = ENUM_PD_IMG_IDX.UNKNOWN_MAX
        m_bIsGBCMDefault = False
        m_nFwPlatform = ENU_FW_PLATFORM_TYPE.UNKNOWN
        m_byteUIScheme = tagTLV.ENUM_FFF8_UI_SCHEME.xFF_UNKNOWN
        m_bIsLCDSupported = True
        m_bIsLCDContrastSupported = False
        m_bIsButtonsCfgSupported = False
        m_bIsBuzzerSupported = True
        m_bIsDefBurstMode = ClassReaderProtocolBasic.TAG_BURST_MODE._x00_OFF
        '
        m_bIsAdminKeyInjection = False

        'Track Stuffs
        m_bMSRTrackChkAndParse = False
        m_bMSRTrackChkAndParseDone = False
    End Sub

    Public Function InProgressVerifiedProduct(ByVal strGetVersion As String) As Boolean
        Dim bFound As Boolean = False
        Dim nImageIdx As Integer
        Dim nKeywordLen As Integer = 0
        '
        ReInit()
        '
        If (m_dictProductTree.Count > 0) Then
            For Each iteratorLv1 In m_dictProductTree
                If strGetVersion.Contains(iteratorLv1.Key) Then
                    If (Not m_dictPlatformNameStr2EnuVal.ContainsKey(iteratorLv1.Key)) Then
                        Exit For
                    End If

                    'Firmware Platform Value
                    m_nFwPlatform = m_dictPlatformNameStr2EnuVal.Item(iteratorLv1.Key)
                    'If (m_nFwPlatform = ENU_FW_PLATFORM_TYPE.FW) Then
                    '    m_nFwPlatform = ENU_FW_PLATFORM_TYPE.GR
                    'End If

                    'Next looking for Product Type
                    If (iteratorLv1.Value.Count > 0) Then
                        For Each iteratorLv2 In iteratorLv1.Value
                            If (strGetVersion.Contains(iteratorLv2.Key)) Then

                                If (nKeywordLen < iteratorLv2.Key.Length()) Then
                                    bFound = True
                                    nImageIdx = iteratorLv2.Value

                                    'Save Product Image Index
                                    m_PD_IMG = nImageIdx

                                    If (m_dictProductPicList.ContainsKey(nImageIdx)) Then
                                        m_pRefMF.SystemProductAutoDetectionPicSelect(m_dictProductPicList.Item(nImageIdx))
                                    Else
                                        m_pRefMF.LogLn("[Warning] " + iteratorLv1.Key + " - " + iteratorLv2.Key + " , No Product Picture Available !!")
                                    End If

                                    'Check LCD Display is supported
                                    InProgressVerifiedProductPropertiesCreate(m_PD_IMG)


                                    'Init TLV List  Save Link
                                    m_refsdictTLVsDefault = Nothing

                                    'Return bFound
                                    nKeywordLen = iteratorLv2.Key.Length()
                                End If
                            End If
                        Next
                    End If
                End If
            Next
        End If

        If (bFound) Then
            Return bFound
        End If
        'Not Found, we use another rule to identify the DUT
        For Each iterChk In m_dictBootloaderVerStr2Platform
            If (strGetVersion.Contains(iterChk.Key)) Then
                m_nFwPlatform = iterChk.Value.Value
                m_PD_IMG = iterChk.Value.Key
                If (m_dictProductPicList.ContainsKey(m_PD_IMG)) Then
                    m_pRefMF.SystemProductAutoDetectionPicSelect(m_dictProductPicList.Item(m_PD_IMG))
                Else
                    m_pRefMF.LogLn("[Warning] " + strGetVersion + " , No Product Picture Available !!")
                End If

                'Check LCD Display is supported
                InProgressVerifiedProductPropertiesCreate(m_PD_IMG)

                'Init TLV List  Save Link
                m_refsdictTLVsDefault = Nothing

                bFound = True
                Return bFound
                Exit For
            End If
        Next
        '
        Return bFound
    End Function

    Public Sub SetUnknowProduct()
        m_pRefMF.SystemProductAutoDetectionPicSelect(m_dictProductPicList.Item(ENUM_PD_IMG_IDX.UNKNOWN_MAX))

        'Default Reader Production is VendIII
        m_PD_IMG = ENUM_PD_IMG_IDX.UNKNOWN_MAX
        m_nFwPlatform = ENU_FW_PLATFORM_TYPE.UNKNOWN
        m_bIsGBCMDefault = False
        m_byteUIScheme = tagTLV.ENUM_FFF8_UI_SCHEME.xFF_UNKNOWN
        m_strFWVersionInfo = "Unknown FW Version Info"

        m_bIsLEDsGreenSupport = False
        m_bIsLEDsBlueSupport = False
        m_bIsLEDsMSRSupport = False

        m_bIsDevMode = False
        m_bIsSREDSupported = False
    End Sub

    Public Sub SetProduct(enProductID As ENUM_PD_IMG_IDX, Optional ByVal nPlatform As ENU_FW_PLATFORM_TYPE = ENU_FW_PLATFORM_TYPE.GR)

        If (Not m_dictProductPicList.ContainsKey(enProductID)) Then
            'Throw New Exception("[Err] SetProduct() : Unsupported Product ID = " + enProductID.ToString())
            'Set as Vendi Product
            enProductID = ENUM_PD_IMG_IDX.NEO_VENDI_V0
        End If
        m_nFwPlatform = GetPlatformByProductID(enProductID)

        If (m_nFwPlatform = ENU_FW_PLATFORM_TYPE.UNKNOWN) Then
            m_nFwPlatform = nPlatform
        End If

        'Save Product Image Index
        m_PD_IMG = enProductID
        m_pRefMF.SystemProductAutoDetectionPicSelect(m_dictProductPicList.Item(enProductID))

        'Check LCD/LED/Buttons are supported
        InProgressVerifiedProductPropertiesCreate(m_PD_IMG)

        'Init TLV List  Save Link
        m_refsdictTLVsDefault = Nothing

        'Default Reader Type is VendIII
        m_bIsGBCMDefault = False
        m_byteUIScheme = tagTLV.ENUM_FFF8_UI_SCHEME.xFF_UNKNOWN
        m_strFWVersionInfo = "Unknown FW Version Info"
    End Sub

    Public Sub Cleanup()
        ' =====[ Revision / History ]========
        If (m_dictProductTree.Count > 0) Then
            For Each iteratorLv1 In m_dictProductTree
                If (iteratorLv1.Value.Count > 0) Then
                    For Each iteratorLv2 In iteratorLv1.Value
                        'Do Release something
                    Next
                    '
                    iteratorLv1.Value.Clear()
                End If
            Next
            '
            m_dictProductTree.Clear()
        End If
        '
        If (m_dictProductPicList IsNot Nothing) Then
            m_dictProductPicList.Clear()
        End If
        '
        If (m_dictPlatformNameStr2EnuVal IsNot Nothing) Then
            m_dictPlatformNameStr2EnuVal.Clear()
        End If

        If (m_dictBootloaderVerStr2Platform IsNot Nothing) Then
            m_dictBootloaderVerStr2Platform.Clear()
        End If


        '
        If (m_listPlatformGBCMDefOn IsNot Nothing) Then
            m_listPlatformGBCMDefOn.Clear()
        End If

        Erase m_arrayByteTestButtonsConfigEnableAll
        Erase m_arrayByteTestButtonsConfigDefault

        If (m_tblTSName IsNot Nothing) Then
            m_tblTSName.Clear()
        End If

        If (m_mapPrdtID IsNot Nothing) Then
            m_mapPrdtID.Clear()
        End If

        If (m_listSystemAID IsNot Nothing) Then
            m_listSystemAID.Clear()
        End If
    End Sub
    '
    '        DeviceProperties DevicePropertiesFactory::GetDeviceProperties(eDeviceType type)
    '{
    '	DeviceProperties props;
    '	switch(type)
    '	{
    '	case VP4800:
    '		props = DeviceProperties(type, 0, 0, 0, 0,
    '			TEST_LED|TEST_BUZZER|TEST_PICC|TEST_SAM);
    '		break;

    '	case VP8100:
    '		props = DeviceProperties(type, 16, 2, 
    '			KEY_1|KEY_2|KEY_3|KEY_4|KEY_5|KEY_6|KEY_7|KEY_8|KEY_9|KEY_0|KEY_CANCEL|KEY_BACKSPACE|KEY_ENTER,
    '			KEY_F1|KEY_F2|KEY_F3,
    '            TEST_LED|TEST_BUZZER|TEST_MSR|TEST_PICC|TEST_ICC|TEST_SAM|TEST_SECURITY|TEST_KEY|TEST_ETH|TEST_LCDCONTRA| TEST_PICC_PCD);
    '		break;

    '	case VP8600:
    '		props = DeviceProperties(type, 240, 160, 
    '			KEY_1|KEY_2|KEY_3|KEY_4|KEY_5|KEY_6|KEY_7|KEY_8|KEY_9|KEY_0|KEY_CANCEL|KEY_BACKSPACE|KEY_ENTER,
    '			0,
    '			TEST_LED|TEST_BUZZER|TEST_MSR|TEST_PICC|TEST_ICC|TEST_SAM|TEST_SECURITY|TEST_LCD|TEST_TOUCH|TEST_KEY|TEST_ETH|TEST_LCDCONTRA);
    '		break;

    '	case VP8800:
    '		props = DeviceProperties(type, 480, 272, 
    '			KEY_1|KEY_2|KEY_3|KEY_4|KEY_5|KEY_6|KEY_7|KEY_8|KEY_9|KEY_0|KEY_CANCEL|KEY_BACKSPACE|KEY_ENTER,
    '			0,
    '			TEST_LED|TEST_BUZZER|TEST_MSR|TEST_PICC|TEST_ICC|TEST_SAM|TEST_SECURITY|TEST_LCD|TEST_TOUCH|TEST_KEY|TEST_ETH);
    '		break;

    '	case VPVEND3:
    '		props = DeviceProperties(type, 128, 64,
    '			0,
    '			KEY_F1|KEY_F2|KEY_F3|KEY_F4,
    '			TEST_LED|TEST_BUZZER|TEST_MSR|TEST_PICC|TEST_ICC|TEST_SAM|TEST_SECURITY|TEST_KEY|TEST_ETH|TEST_LCDCONTRA);
    '		break;
    '	}

    '	return props;
    '}
    Public Function GetKeyList() As SortedDictionary(Of ClassReaderProtocolBasic.TAG_GET_INPUT_EVETN.EVT_KEY_ID, Boolean)

        Select Case (m_PD_IMG)
            Case ENUM_PD_IMG_IDX.AR_HG4_VENDIII, ENUM_PD_IMG_IDX.AR_HG4_VENDIII_AR_2_1_5
                'This is VendIII
                Return New SortedDictionary(Of ClassReaderProtocolBasic.TAG_GET_INPUT_EVETN.EVT_KEY_ID, Boolean) From {
                    {ClassReaderProtocolBasic.TAG_GET_INPUT_EVETN.EVT_KEY_ID.FUNC_KEY_LEFT_F1, False}, {ClassReaderProtocolBasic.TAG_GET_INPUT_EVETN.EVT_KEY_ID.FUNC_KEY_MIDDLE_F2, False}, {ClassReaderProtocolBasic.TAG_GET_INPUT_EVETN.EVT_KEY_ID.FUNC_KEY_RIGHT_F3, False}, {ClassReaderProtocolBasic.TAG_GET_INPUT_EVETN.EVT_KEY_ID.FUNC_KEY_DOWN_F4, False}}
                '
            Case ENUM_PD_IMG_IDX.AR_PISCES, ENUM_PD_IMG_IDX.AR_PISCES_V0
                Return New SortedDictionary(Of ClassReaderProtocolBasic.TAG_GET_INPUT_EVETN.EVT_KEY_ID, Boolean) From {
                    {ClassReaderProtocolBasic.TAG_GET_INPUT_EVETN.EVT_KEY_ID.KEYPAD_KEY_0, False},
                   {ClassReaderProtocolBasic.TAG_GET_INPUT_EVETN.EVT_KEY_ID.KEYPAD_KEY_1, False}, {ClassReaderProtocolBasic.TAG_GET_INPUT_EVETN.EVT_KEY_ID.KEYPAD_KEY_2, False}, {ClassReaderProtocolBasic.TAG_GET_INPUT_EVETN.EVT_KEY_ID.KEYPAD_KEY_3, False}, {ClassReaderProtocolBasic.TAG_GET_INPUT_EVETN.EVT_KEY_ID.KEYPAD_KEY_4, False},
                   {ClassReaderProtocolBasic.TAG_GET_INPUT_EVETN.EVT_KEY_ID.KEYPAD_KEY_5, False}, {ClassReaderProtocolBasic.TAG_GET_INPUT_EVETN.EVT_KEY_ID.KEYPAD_KEY_6, False}, {ClassReaderProtocolBasic.TAG_GET_INPUT_EVETN.EVT_KEY_ID.KEYPAD_KEY_7, False}, {ClassReaderProtocolBasic.TAG_GET_INPUT_EVETN.EVT_KEY_ID.KEYPAD_KEY_8, False},
                   {ClassReaderProtocolBasic.TAG_GET_INPUT_EVETN.EVT_KEY_ID.KEYPAD_KEY_9, False}, {ClassReaderProtocolBasic.TAG_GET_INPUT_EVETN.EVT_KEY_ID.KEYPAD_KEY_ENTER, False}, {ClassReaderProtocolBasic.TAG_GET_INPUT_EVETN.EVT_KEY_ID.KEYPAD_KEY_CLEAR_BACKSPACE, False}, {ClassReaderProtocolBasic.TAG_GET_INPUT_EVETN.EVT_KEY_ID.KEYPAD_KEY_CANCEL, False},
                   {ClassReaderProtocolBasic.TAG_GET_INPUT_EVETN.EVT_KEY_ID.FUNC_KEY_SHARP, False}, {ClassReaderProtocolBasic.TAG_GET_INPUT_EVETN.EVT_KEY_ID.FUNC_KEY_STAR, False}}
                '
            Case Else
                'This is VendIII
                Return New SortedDictionary(Of ClassReaderProtocolBasic.TAG_GET_INPUT_EVETN.EVT_KEY_ID, Boolean) From {
                    {ClassReaderProtocolBasic.TAG_GET_INPUT_EVETN.EVT_KEY_ID.KEYPAD_KEY_1, False}, {ClassReaderProtocolBasic.TAG_GET_INPUT_EVETN.EVT_KEY_ID.KEYPAD_KEY_2, False}, {ClassReaderProtocolBasic.TAG_GET_INPUT_EVETN.EVT_KEY_ID.KEYPAD_KEY_3, False}, {ClassReaderProtocolBasic.TAG_GET_INPUT_EVETN.EVT_KEY_ID.KEYPAD_KEY_4, False},
                    {ClassReaderProtocolBasic.TAG_GET_INPUT_EVETN.EVT_KEY_ID.KEYPAD_KEY_5, False}, {ClassReaderProtocolBasic.TAG_GET_INPUT_EVETN.EVT_KEY_ID.KEYPAD_KEY_6, False}, {ClassReaderProtocolBasic.TAG_GET_INPUT_EVETN.EVT_KEY_ID.KEYPAD_KEY_7, False}, {ClassReaderProtocolBasic.TAG_GET_INPUT_EVETN.EVT_KEY_ID.KEYPAD_KEY_8, False},
                    {ClassReaderProtocolBasic.TAG_GET_INPUT_EVETN.EVT_KEY_ID.KEYPAD_KEY_9, False}, {ClassReaderProtocolBasic.TAG_GET_INPUT_EVETN.EVT_KEY_ID.KEYPAD_KEY_ENTER, False}, {ClassReaderProtocolBasic.TAG_GET_INPUT_EVETN.EVT_KEY_ID.KEYPAD_KEY_CLEAR_BACKSPACE, False}, {ClassReaderProtocolBasic.TAG_GET_INPUT_EVETN.EVT_KEY_ID.KEYPAD_KEY_CANCEL, False},
                    {ClassReaderProtocolBasic.TAG_GET_INPUT_EVETN.EVT_KEY_ID.FUNC_KEY_LEFT_F1, False}, {ClassReaderProtocolBasic.TAG_GET_INPUT_EVETN.EVT_KEY_ID.FUNC_KEY_MIDDLE_F2, False}, {ClassReaderProtocolBasic.TAG_GET_INPUT_EVETN.EVT_KEY_ID.FUNC_KEY_RIGHT_F3, False}, {ClassReaderProtocolBasic.TAG_GET_INPUT_EVETN.EVT_KEY_ID.FUNC_KEY_DOWN_F4, False}}
        End Select
        '
        Return Nothing
    End Function

    Public Enum ENUM_TEST_FILE_LCD_IMAGE
        ALL_ON
        ALL_OFF
        ALL_CHK
        ALL_INV
    End Enum

    Public Function GetTestFileList() As List(Of String)
        Dim a As List(Of String)

        '
        Select Case (m_PD_IMG)
            Case ENUM_PD_IMG_IDX.AR_PISCES, ENUM_PD_IMG_IDX.AR_PISCES_V0
                '---< Update List >---
                If (bIsLCDImgDownloadable) Then
                    ' 2016 Sept 02, goofy_liu@idtechproducts.com.tw
                    '{"colorbars320-480.bmp"}
                    a = New List(Of String) From {
                        {"lcdallon320-480.bmp"},
                        {"lcdalloff320-480.bmp"},
                        {"colorbars320-480.bmp"}
                    }
                Else
                    ' 2016 Nov 02, goofy_liu@idtechproducts.com.tw
                    '
                    a = New List(Of String) From {
                        {"lcdalloff-hvga.bmp"},
                        {"lcdallon-hvga.bmp"},
                        {"colorbars-hvga.bmp"}
                    }
                End If



                ' 2016 Sept 02, goofy_liu@idtechproducts.com.tw
                'a = New List(Of String) From {
                '    {"lcdallon320-480.bmp"},
                '    {"lcdalloff320-480.bmp"},
                '    {"colorbars320-480.bmp"}
                '}


            Case ENUM_PD_IMG_IDX.AR_HG4_VENDIII, ENUM_PD_IMG_IDX.AR_HG4_VENDIII_AR_2_1_5
                a = New List(Of String) From {
                    {"lcdallon128-64.bmp"},
                    {"lcdalloff128-64.bmp"},
                    {"lcdchecker128-64.bmp"}
                }
                ',
                '                    {"lcdchecker128-64Inverse.bmp"}
                '                    }
            Case ENUM_PD_IMG_IDX.AR_HG0_VP8600
                a = New List(Of String) From {
                    {"lcdallon128-64.bmp"},
                    {"lcdalloff128-64.bmp"},
                    {"lcdchecker128-64.bmp"}}
                ',
                '                    {"lcdchecker128-64Inverse.bmp"}
                '                    }
            Case ENUM_PD_IMG_IDX.AR_HG1_VP8800
                a = New List(Of String) From {
                    {"lcdallon128-64.bmp"},
                    {"lcdalloff128-64.bmp"},
                    {"lcdchecker128-64.bmp"}}
                ',
                '                    {"lcdchecker128-64Inverse.bmp"}
                '                    }
            Case Else
                a = Nothing
        End Select
        '
        Return a
    End Function

    Function GetTestFileListAllTypes() As List(Of String)
        Dim a As List(Of String)
        a = New List(Of String) From {
            {"lcdallon128-64.bmp"},
            {"lcdalloff128-64.bmp"},
            {"lcdchecker128-64.bmp"},
            {"lcdallon240-160.bmp"},
            {"lcdalloff240-160.bmp"},
            {"lcdchecker240-160.bmp"},
            {"lcdallon480-272.bmp"},
            {"lcdalloff480-272.bmp"},
            {"colorbars480-272.bmp"},
            {"lcdallon320-480.bmp"},
            {"lcdalloff320-480.bmp"},
            {"colorbars320-480.bmp"},
            {"lcdallon-hvga.bmp"},
            {"lcdalloff-hvga.bmp"},
            {"colorbars-hvga.bmp"}
            }
        '
        Return a
    End Function

    Function GetTestFileNameImagePattern() As String
        'Vend III, PISCES

        Select Case (m_PD_IMG)
            Case ENUM_PD_IMG_IDX.AR_HG4_VENDIII, ENUM_PD_IMG_IDX.AR_HG4_VENDIII_AR_2_1_5  'Little - 128x64
                Return "lcdchecker128-64.bmp"
            Case ENUM_PD_IMG_IDX.AR_PISCES, ENUM_PD_IMG_IDX.AR_PISCES_V0   'Little - 128x64
                'In PISCES
                'dir path = /rsc/custom
                If (bIsLCDImgDownloadable) Then
                    Return "colorbars320-480.bmp"
                End If

                Return "colorbars-hvga.bmp"
                'Return "colorbars.bmp" @ /rsc/image directory
            Case ENUM_PD_IMG_IDX.AR_HG0_VP8600 'Small - 240x160
                Return "lcdchecker240-160.bmp"
            Case ENUM_PD_IMG_IDX.AR_HG1_VP8800 ' Large - 480x272
                Return "lcdchecker480-272.bmp"
            Case Else ' Default one use little one (smallest)
                Return "lcdchecker128-64.bmp"
        End Select
    End Function

    Function GetTestFileNameImagePixelOn() As String
        Select Case (m_PD_IMG)
            Case ENUM_PD_IMG_IDX.AR_HG4_VENDIII, ENUM_PD_IMG_IDX.AR_HG4_VENDIII_AR_2_1_5  'Little - 128x64
                Return "lcdallon128-64.bmp"

            Case ENUM_PD_IMG_IDX.AR_PISCES, ENUM_PD_IMG_IDX.AR_PISCES_V0   'Little - 320x480
                '2016 Sept 02
                If (bIsLCDImgDownloadable) Then
                    Return "lcdallon320-480.bmp"
                End If

                '2016 Nov 02 - In PISCES Reader, off (black) = White
                Return "lcdallon-hvga.bmp"
        End Select
        Return "lcdallon128-64.bmp"
    End Function

    Function GetTestFileNameImagePixelOff() As String
        Select Case (m_PD_IMG)
            Case ENUM_PD_IMG_IDX.AR_HG4_VENDIII, ENUM_PD_IMG_IDX.AR_HG4_VENDIII_AR_2_1_5  'Little - 128x64
                Return "lcdalloff128-64.bmp"
            Case ENUM_PD_IMG_IDX.AR_PISCES, ENUM_PD_IMG_IDX.AR_PISCES_V0   'Little - 320x480
                '2016 Sept 02 Updated
                If (bIsLCDImgDownloadable) Then
                    Return "lcdalloff320-480.bmp"
                End If


                ' 2016 Nov 02 Updated
                Return "lcdalloff-hvga.bmp"
        End Select
        '
        Return "lcdalloff128-64.bmp"
    End Function

    Private m_bIsEngMode As Boolean = True

   

    Function GetDefaultButtonsList() As Byte()
        ' Three bytes are  [Done], [Swipe Card], and [Delay Time].
        Dim arrayByte As Byte() = Nothing

        Select Case (m_PD_IMG)
            Case ENUM_PD_IMG_IDX.NEO_VENDI_V0, ENUM_PD_IMG_IDX.NEO_VENDI_NEW
                arrayByte = m_arrayByteTestButtonsConfigDefault ' 01 01 0F
        End Select

        Return arrayByte
    End Function
    '

    '
    Private Sub InProgressVerifiedProductPropertiesCreate(ByVal nPD_IMG As ENUM_PD_IMG_IDX)
        'Please Note: Part Number Selection  "PartNumFormatChk ()" also effect the properties
        'Dim pCmdInst As ClassCmdLineArgumentOptions = ClassCmdLineArgumentOptions.GetInstance()

        'Decide Parser Type
        ClassReaderCommanderParser.Init(bIsPlatform_NEO20_2017)
        ' selection of 2 Byte TLV or 3 Byte TLV
        tagTLV.Init(bIsPlatform_NEO20_2017 Or bIsPlatformAR_V300_2016)
        '
        m_bIsLCDSupported = True
        m_bIsLCDContrastSupported = False
        m_bIsButtonsCfgSupported = False
        ' Default is 3 kinds of LEDs are supported
        m_bIsLEDsGreenSupport = True
        m_bIsLEDsBlueSupport = True
        m_bIsLEDsMSRSupport = True
        '=====================
        m_bIsDateTimeSupported = False

        m_bIsDateTimeSupported_RTCNoBattery = False
        '=====================
        m_bIsMSRTk1 = True
        m_bIsMSRTk2 = True
        m_bIsMSRTk3 = True
        m_bIsKeyPadSupported = False
        m_bIsTouchScreenSupported = False
        m_bIsSDCardSupported = False
        '
        m_Rect_Signature = New Rectangle With {.X = 0, .Y = 0, .Width = 0, .Height = 0}

        '
        Select Case (m_nFwPlatform)
            Case ENU_FW_PLATFORM_TYPE.GR, ENU_FW_PLATFORM_TYPE.NEO_IDG, ENU_FW_PLATFORM_TYPE.FW
                m_bIsDateTimeSupported = True
        End Select
        '=====================
        m_bIsLEDsTriColorSupport = False
        m_bIsAdminKeyInjection = False

        Select Case (m_PD_IMG)
            Case ENUM_PD_IMG_IDX.NEO_20_VP5300_SPTP_II
                m_bIsDateTimeSupported = True

                m_bIsLCDSupported = False
                ' Default is 3 kinds of LEDs are supported
                m_bIsLEDsGreenSupport = False
                m_bIsLEDsBlueSupport = False
                'm_bIsLEDsMSRSupport = False ' SPTP II has ICC LED
                '
                m_bIsLEDsTriColorSupport = True
                '
            Case ENUM_PD_IMG_IDX.NEO_20_VP6300_VENDI_PRO
                m_bIsDateTimeSupported = True
                m_bIsButtonsCfgSupported = True
                'm_bIsLCDSupported = True
                ' Default is 3 kinds of LEDs are supported
                m_bIsLEDsGreenSupport = False '2017, Beta 1 version, Remove it in Mass Production Version
                m_bIsLEDsBlueSupport = True
                m_bIsLEDsMSRSupport = False '2017, Beta 1 version, Remove it in Mass Production Version
                m_bIsKeyPadSupported = True
                '
            Case ENUM_PD_IMG_IDX.AR_PISCES, ENUM_PD_IMG_IDX.AR_PISCES_V0

                m_bIsDateTimeSupported = True
                m_bIsLCDContrastSupported = True

                m_bIsSDCardSupported = True

                m_Rect_Signature = New Rectangle With {.X = 5, .Y = 5, .Width = 310, .Height = 406}

                '2016 Oct 20, goofy_liu@idtechproducts.com.tw
                'Board Level Test
                'If (pCmdInst.bIsBoardTM) Then
                '    m_bIsLCDSupported = False
                '    m_bIsMSRTk1 = False
                '    m_bIsMSRTk2 = False
                '    m_bIsMSRTk3 = False

                '    'PISCES's MSR LEDx1 = RGB Color (On/Off status for every color)
                '    m_bIsLEDsMSRSupport = False
                '    m_bIsLEDsBlueSupport = False
                '    m_bIsLEDsGreenSupport = False
                '    'm_bIsLEDsTriColorSupport = True
                '    'm_bIsAdminKeyInjection = False
                'Else
                'm_bIsAdminKeyInjection = True
                m_bIsKeyPadSupported = True
                m_bIsTouchScreenSupported = True
                'End If

            Case ENUM_PD_IMG_IDX.AR_HG4_VENDIII, ENUM_PD_IMG_IDX.AR_HG4_VENDIII_AR_2_1_5
                m_bIsLCDContrastSupported = True
                m_bIsKeyPadSupported = True

            Case ENUM_PD_IMG_IDX.GR_EC8_KIOSKII, ENUM_PD_IMG_IDX.GR_ECA_KIOSKII, ENUM_PD_IMG_IDX.GR_EC7_KIOSKI
                m_bIsLCDSupported = False

                m_bIsLEDsGreenSupport = True 'KIOSK Serials has 4 Green LEDs
                m_bIsLEDsBlueSupport = False
                m_bIsLEDsMSRSupport = False

                m_bIsMSRTk1 = False
                m_bIsMSRTk2 = False
                m_bIsMSRTk3 = False

                m_byteUIScheme = tagTLV.ENUM_FFF8_UI_SCHEME.x03_EMEA
            Case ENUM_PD_IMG_IDX.NEO_VP3300USB, ENUM_PD_IMG_IDX.NEO_VP3300USB_C, ENUM_PD_IMG_IDX.NEO_VP3300USB_E
                m_bIsLCDSupported = False
                'm_bIsButtonsCfgSupported = False

                m_bIsLEDsGreenSupport = True ' has NFC LEDs , Green x 4 LEDs
                m_bIsLEDsBlueSupport = False
                m_bIsLEDsTriColorSupport = False
                '
                Select Case m_PD_IMG
                    Case ENUM_PD_IMG_IDX.NEO_VP3300USB
                    Case ENUM_PD_IMG_IDX.NEO_VP3300USB_C, ENUM_PD_IMG_IDX.NEO_VP3300USB_E
                        m_bIsMSRTk1 = False
                        m_bIsMSRTk2 = False
                        m_bIsMSRTk3 = False

                        m_bIsDateTimeSupported = False
                End Select
                '
                m_bIsLEDsMSRSupport = False
                '
                m_bIsAdminKeyInjection = True
                '
                m_byteUIScheme = tagTLV.ENUM_FFF8_UI_SCHEME.x03_EMEA
                '
                '[Note] Don'w Support Time/Date Sync since Battery Powered Reader will fail often if the battery is not full charged !!
                'So we use No Battery RTC Test Scenario

                m_bIsDateTimeSupported_RTCNoBattery = True
                '
            Case ENUM_PD_IMG_IDX.NEO_VP3300Bluetooth
                m_bIsLCDSupported = False
                'm_bIsButtonsCfgSupported = False

                m_bIsLEDsGreenSupport = True 'Unipay III has Green x 4 LEDs
                m_bIsLEDsBlueSupport = False
                m_bIsLEDsTriColorSupport = True
                m_bIsLEDsMSRSupport = False ' Unipay III has MSR Intf but the UI LEDs are Green LED
                '
                m_bIsAdminKeyInjection = True
                '
                m_byteUIScheme = tagTLV.ENUM_FFF8_UI_SCHEME.x03_EMEA
                '
                '[Note] Don't Support Time/Date Sync since Battery Powered Reader will fail often if the battery is not full charged !!
                m_bIsDateTimeSupported = False
                m_bIsDateTimeSupported_RTCNoBattery = True

            Case ENUM_PD_IMG_IDX.NEO_VP3300_AUDIO_JACK, ENUM_PD_IMG_IDX.NEO_UNIPAY_III_iBase
                m_bIsLCDSupported = False
                'm_bIsButtonsCfgSupported = False

                m_bIsLEDsGreenSupport = True 'Unipay III has Green x 4 LEDs
                m_bIsLEDsBlueSupport = False
                m_bIsLEDsTriColorSupport = True
                m_bIsLEDsMSRSupport = False ' Unipay III has MSR Intf but the UI LEDs are Green LED
                '
                m_bIsAdminKeyInjection = True
                '
                m_byteUIScheme = tagTLV.ENUM_FFF8_UI_SCHEME.x03_EMEA
                '
                '[Note] Don't Support Time/Date Sync since Battery Powered Reader will fail often if the battery is not full charged !!
                m_bIsDateTimeSupported = False
                'So we use No Battery RTC Test Scenario
                m_bIsDateTimeSupported_RTCNoBattery = True

            Case ENUM_PD_IMG_IDX.AR_HG4_VENDIII
                m_bIsLCDContrastSupported = True

            Case ENUM_PD_IMG_IDX.NEO_UNIPAY_1D5, ENUM_PD_IMG_IDX.NEO_UNIPAY_1D5_TTK
                m_bIsLCDSupported = False
                'm_bIsButtonsCfgSupported = False

                m_bIsLEDsGreenSupport = False ' Unipay 1.5 has NO Green LEDs x 4
                m_bIsLEDsBlueSupport = False
                m_bIsLEDsTriColorSupport = True
                m_bIsLEDsMSRSupport = False ' Unipay III has MSR Intf but the UI LEDs are Green LED
                '
                m_bIsAdminKeyInjection = True
                '
                m_byteUIScheme = tagTLV.ENUM_FFF8_UI_SCHEME.x03_EMEA

                '[Note] Don'w Support Time/Date Sync since Battery Powered Reader will fail often if the battery is not full charged !!
                'ToDo --> Still Test RTC but in QC Mode, TS uses Same RTC Test Item in Production Mode.
                m_bIsDateTimeSupported = False

            Case ENUM_PD_IMG_IDX.NEO_VENDI_V0, ENUM_PD_IMG_IDX.NEO_VENDI_NEW
                m_byteUIScheme = tagTLV.ENUM_FFF8_UI_SCHEME.x00_VIVOPAY

                m_bIsButtonsCfgSupported = True

                m_bIsAdminKeyInjection = True

                m_bIsMSRTk3 = False ' Track 3 N/A in Vendi

                m_bIsKeyPadSupported = True
            Case Else
                'm_bIsLCDSupported = False
                'm_bIsButtonsSupported = False

                'm_bisLEDsGreenSupport = False
                'm_bisLEDsBlueSupport = False
                'm_bisLEDsMSRSupport = False
        End Select
    End Sub

    Public Sub SaveConfigDefaultTLVsAndProprietaryIdentification(sdictTLVsDefault As SortedDictionary(Of String, tagTLV))
        m_refsdictTLVsDefault = sdictTLVsDefault

        ' Check Burst mode - FFF7 burst mode tag
        If m_refsdictTLVsDefault.ContainsKey(tagTLV.TAG_FFF7_or_DFEE7E_BurstMode) Then
            Dim tlv As tagTLV
            tlv = m_refsdictTLVsDefault.Item(tagTLV.TAG_FFF7_or_DFEE7E_BurstMode)
            'If (tlv.m_arrayTLVal(0) > &H0) Then
            '    '=&H0, End Burst Mode
            '    '=&H1 , Burst Mode
            '    '=&H2, Burst Mode but Auto Exit
            '    m_bIsDefBurstMode = CType(tlv.m_arrayTLVal(0), ClassReaderCommander.TAG_BURST_MODE)
            'End If
            m_bIsDefBurstMode = CType(tlv.m_arrayTLVal(0), ClassReaderProtocolBasic.TAG_BURST_MODE)
        End If

        'Check FFF8 UI Scheme tag
        If m_refsdictTLVsDefault.ContainsKey(tagTLV.TAG_FFF8_or_DFEE37_UIScheme) Then
            Dim tlv As tagTLV
            tlv = m_refsdictTLVsDefault.Item(tagTLV.TAG_FFF8_or_DFEE37_UIScheme)
            m_byteUIScheme = tlv.m_arrayTLVal(0)
        End If

    End Sub
    Public Function SaveConfigDefaultTLVsGet() As SortedDictionary(Of String, tagTLV)
        Return m_refsdictTLVsDefault
    End Function

    Private Function GetPlatformByProductID(enProductID As ENUM_PD_IMG_IDX) As ENU_FW_PLATFORM_TYPE
        Dim nPltTypeSeek As ENU_FW_PLATFORM_TYPE = ENU_FW_PLATFORM_TYPE.UNKNOWN

        For Each iteratorLv1 In m_dictProductTree
            If (iteratorLv1.Value.ContainsValue(enProductID)) Then
                nPltTypeSeek = m_dictPlatformNameStr2EnuVal.Item(iteratorLv1.Key)
            End If
        Next
        Return nPltTypeSeek
    End Function

    Function GetDefRS232Buadrate() As ClassDevRS232.EnumBUAD_RATE
        Select Case (m_PD_IMG)
            Case ENUM_PD_IMG_IDX.AR_HG3_VP4800, ENUM_PD_IMG_IDX.GR_EC8_KIOSKII, ENUM_PD_IMG_IDX.GR_EC7_KIOSKI
                Return ClassDevRS232.EnumBUAD_RATE._19200
            Case ENUM_PD_IMG_IDX.AR_HG2_VP8100, ENUM_PD_IMG_IDX.AR_HG0_VP8600, ENUM_PD_IMG_IDX.AR_HG1_VP8800
                Return ClassDevRS232.EnumBUAD_RATE._115200
            Case ENUM_PD_IMG_IDX.AR_HG4_VENDIII, ENUM_PD_IMG_IDX.AR_HG4_VENDIII_AR_2_1_5, ENUM_PD_IMG_IDX.NEO_VENDI_V0, ENUM_PD_IMG_IDX.NEO_VP3300_AUDIO_JACK
                Return ClassDevRS232.EnumBUAD_RATE._9600
                'Case ENUM_PD_IMG_IDX.NEO_VENDI
                '    Return ClassDevRS232.EnumBUAD_RATE._19200
        End Select
        Return ClassDevRS232.EnumBUAD_RATE._9600
    End Function
    '
    Shared Function GetIntance() As ClassReaderInfo
        If (m_pInstance Is Nothing) Then
            m_pInstance = New ClassReaderInfo
            m_pInstance.Init()
        End If

        Return m_pInstance
    End Function
    '
    Sub VerifyMSRTrackIsReady_StartStop(Optional bGo As Boolean = True)
        m_bMSRTrackChkAndParse = bGo
        m_bMSRTrackChkAndParseDone = False
        If (bGo) Then
            'Clear All Track Data
            m_strTrack1 = ""
            m_strTrack2 = ""
            m_strTrack3 = ""
        End If
    End Sub
    '========================================================
    '[INPUT] chSS = Start Sentential 
    '               chES = End Sentential
    '[OUTPUT]
    '   <If Succeed Found, return True, chSS + chES are involved>
    '               o_Track = Extracted Track start from 'chSS' to 'chES'(involved)
    '               o_strSubNext = the substring starts from the 1st char after chES
    '   <If Not Found, return False>
    '               o_Track = ""
    '               o_strSubNext = strInputData
    ' Note01 - 2016 Mar 01, Enhanced MSR Format
#If 0 Then
      28          32  IN     55 55 55 55  55 55 55 55  55 55 55 55  55 55 55 55  55 55 55 55  55 55 55 55  55 55 55 55  55 55 66 56  UUUUUUUUUUUUUUUUUUUUUUUUUUUUUUfV  7.2sc     13429.1.0        unknown       
  28          32  IN     69 56 4f 74  65 63 68 32  00 ee 00 01  16 08 df ee  25 02 00 11  df ee 23 81  f9 02 f3 00  83 3f 4f 28  iVOtech2........%.....#......?O(   64ms     13430.1.0        unknown       
  28          32  IN     6b 87 00 25  54 52 41 43  4b 31 37 36  37 36 37 36  30 37 30 37  30 37 37 36  37 36 37 36  30 37 30 37  k..%TRACK17676760707077676760707   66ms     13431.1.0        unknown       
  28          32  IN     30 37 37 36  37 36 37 36  30 37 30 37  30 37 37 36  37 36 37 36  30 37 30 37  30 37 37 36  37 36 37 36  07767676070707767676070707767676   66ms     13432.1.0        unknown       
  28          32  IN     30 37 30 37  30 37 37 36  37 36 37 36  30 37 30 37  3f 43 3b 32  31 32 31 32  31 32 31 32  31 37 36 37  0707077676760707?C;2121212121767   66ms     13433.1.0        unknown       
  28          32  IN     36 37 36 30  37 30 37 30  37 37 36 37  36 37 36 37  36 32 31 32  31 32 31 32  3f 30 3b 33  33 33 33 33  676070707767676762121212?0;33333   66ms     13434.1.0        unknown       
  28          32  IN     33 33 33 33  33 37 36 37  36 37 36 30  37 30 37 30  37 37 36 37  36 37 36 33  33 33 33 33  33 33 33 33  33333767676070707767676333333333   66ms     13435.1.0        unknown       
  28          32  IN     33 37 36 37  36 37 36 30  37 30 37 30  37 37 36 37  36 37 36 33  33 33 33 33  33 33 33 33  33 37 36 37  37676760707077676763333333333767   66ms     13436.1.0        unknown       
  28          32  IN     36 37 36 30  37 30 37 30  37 37 36 37  36 37 36 33  33 33 33 33  33 33 33 33  33 37 36 37  36 37 36 30  67607070776767633333333337676760   66ms     13437.1.0        unknown       
  28          32  IN     37 30 37 3f  32 30 30 30  30 30 30 30  30 30 31 26  00 03 9f 39  01 80 ff ee  01 04 df 30  01 0c df ee  707?20000000001&...9.......0....   66ms     13438.1.0        unknown       
  28           5  IN     26 01 08 a8  c1                                                                                         &....                              66ms     13439.1.0        unknown       
#End If

    '=========[ Note 01 ]=========
    ' chSS : we may use not only a char data type but also a string one as wanted initial pattern.
    Function MSRExtractTrack(strInputData As String, chSS As String, chES As Char, ByRef o_Track As String, ByRef o_StrSubNext As String) As Boolean
        Dim bIsExtracted As Boolean = False
        '
        Dim nPosStart, nPosStop As Integer
        Try
            'Normalize - Upper Case
            strInputData = strInputData.ToUpper()
            chSS = chSS.ToUpper()
            chES = Char.ToUpper(chES)

            'Error Handling As Default
            o_Track = ""
            o_StrSubNext = strInputData
            '
            nPosStart = strInputData.IndexOf(chSS)
            If (nPosStart < 0) Then
                Return False
            End If
            '
            nPosStop = strInputData.Substring(nPosStart).IndexOf(chES)
            If (nPosStop < 0) Then
                Return False
            End If
            '
            o_Track = strInputData.Substring(nPosStart, nPosStop + 1)
            bIsExtracted = True
            o_StrSubNext = strInputData.Substring(nPosStart + nPosStop + 1)
            '
        Catch ex As Exception

        Finally

        End Try
        Return bIsExtracted
    End Function
    '
    Function MSRExtractTrack(strInputData As String, chSS As Char, chES As Char, ByRef o_Track As String) As Boolean
        Dim strNext As String = ""
        Return MSRExtractTrack(strInputData, chSS, chES, o_Track, strNext)
    End Function
    '
    ReadOnly Property VerifyMSRTrackIsReady_bIsAutoPollBurstModeDone As Boolean
        Get
            Return m_bMSRTrackChkAndParseDone
        End Get
    End Property
    '
    Function ParseAndVerifyMSRTrackIsReady_AutoPollBurstMode(strDumpingRecvBuffer As String, Optional bIsDefLeadStr As Boolean = True) As Boolean
        'Track 1 : SS = '%', ES = '?'
        Dim strTmp As String = ""
        Dim byteFlag As Byte = &H0
        Dim byteFlagReaderSpec As Byte = &H0
        Dim str1stTrackSS As String = "%"

        If (Not m_bMSRTrackChkAndParse) Then
            'Must have invoking "VerifyMSRTrackIsReady_Start(True)" to skip this protection
            Return False
        End If

        'Remove Space
        strDumpingRecvBuffer = Replace(strDumpingRecvBuffer, " ", "")

        'Track1 Extraction

#If 1 Then
        '2016 Sept 09 Updated, + To All Platform
        ' Policy 01 - If  Using Default Lead String  (i.e. %TRACK1 or %012345678) to Seek MSR Data
        ' Policy 02 - If Not Using Default Lead String  (i.e. SS  "%" Only ) to Seek MSR Data. It May be Failed to Seek MSR in Encryption Mode - Txn Response Data
        If (bIsDefLeadStr) Then
#Else
            If (bIsPlatformNEOUnipayIII Or bIsPlatformNEOUnipay1D5 Or bIsPlatformNEOVP4880 Or bIsPlatformNEOVP4880C Or bIsPlatformNEOVP4880E) Then
#End If

            '[Note]
            'Unipay III & Unipay 1.5 Using Enhanced MSR Format which is tagged by DFEE23 @ NEO Response
            'Here we limit the Test MSR Card must contains 1st Track SS pattern "%TRACK1" to resolve this format SIMPLY.
            str1stTrackSS = "%TRACK1" 'ID-TECH MSR Card, PN = 80005202-002 @ Arena
            If (MSRExtractTrack(strDumpingRecvBuffer, str1stTrackSS, "?"c, m_strTrack1, strTmp)) Then
                byteFlag = byteFlag Or &H1
            Else
                str1stTrackSS = "%0123456789" 'Q-Card, PN = 05260455
                If (MSRExtractTrack(strDumpingRecvBuffer, str1stTrackSS, "?"c, m_strTrack1, strTmp)) Then
                    byteFlag = byteFlag Or &H1
                Else
                    strTmp = strDumpingRecvBuffer
                End If
            End If
        Else
            If (MSRExtractTrack(strDumpingRecvBuffer, str1stTrackSS, "?"c, m_strTrack1, strTmp)) Then
                byteFlag = byteFlag Or &H1
            Else
                strTmp = strDumpingRecvBuffer
            End If
        End If

        'Track2 Extraction
        If (MSRExtractTrack(strTmp, ";", "?"c, m_strTrack2, strTmp)) Then
            byteFlag = byteFlag Or &H2
        End If

        'Track3 Extraction
        If (MSRExtractTrack(strTmp, ";", "?"c, m_strTrack3, strTmp)) Then
            byteFlag = byteFlag Or &H4
        End If

        '===============================================
        'Judge MSR Tracks depending on Reader Type(Product)
        If (bIsMSRAvailableTrack1) Then
            byteFlagReaderSpec = byteFlagReaderSpec Or &H1
        End If

        If (bIsMSRAvailableTrack2) Then
            byteFlagReaderSpec = byteFlagReaderSpec Or &H2
        End If

        If (bIsMSRAvailableTrack3) Then
            byteFlagReaderSpec = byteFlagReaderSpec Or &H4
        End If
        '
        m_bMSRTrackChkAndParseDone = (byteFlag = byteFlagReaderSpec)
        '
        Return m_bMSRTrackChkAndParseDone
    End Function
    '
    Public Sub New()
        m_strTrack1 = ""
        m_strTrack2 = ""
        m_strTrack3 = ""
        '
        'm_TagXMLPartNumCustCfgParams.Init()
    End Sub


    ReadOnly Property bIsGBCMAllowed As Boolean
        Get
            Select Case (m_PD_IMG)
                Case ENUM_PD_IMG_IDX.AR_HG4_VENDIII
                    Return True
            End Select
            Return False
        End Get
    End Property

    'This is to get masked-bit list of tamper base-on IDG Commands Bit Definition
    Private Shared m_listTampersMask_Pisces As List(Of Byte) = New List(Of Byte)() From {&H0, &H1, &H3, &H2, &H4, &H5, &H6, &H9 _
        }
    Private Shared m_listTampersMask_VP6300_VendiPro As List(Of Byte) = New List(Of Byte)() From {&H0, &H1, &H2, &H3 _
                                                                                                 , &H4, &H5, &H6 _
      , &H8, &H9, &HA} ', &HB}
    '2017 Sept 04
    Private Shared m_listTampersMask_VP5300_SPTP_II As List(Of Byte) = m_listTampersMask_VP6300_VendiPro

    Private Shared m_listTampersDummy As List(Of Byte) = New List(Of Byte)()


    Public ReadOnly Property GetTamperListStd() As List(Of Byte)
        Get
            Select Case (m_PD_IMG)
                Case ENUM_PD_IMG_IDX.AR_PISCES, ENUM_PD_IMG_IDX.AR_PISCES_V0
                    Return m_listTampersMask_Pisces
                Case ENUM_PD_IMG_IDX.NEO_20_VP6300_VENDI_PRO
                    Return m_listTampersMask_VP6300_VendiPro
                Case ENUM_PD_IMG_IDX.NEO_20_VP5300_SPTP_II
                    Return m_listTampersMask_VP5300_SPTP_II
            End Select
            Return m_listTampersDummy
        End Get
    End Property

    '[OUT]
    Public Function SetProduct_LV2(ByRef io_NUM_PD_IMG_IDX As ENUM_PD_IMG_IDX, ByVal strInfo As String) As Boolean
        Dim nKeyLen As Integer = 0
        Dim bUpdateReq As Boolean = False

        'Here we update to new 
        For Each itrItem In m_mapPrdtID
            If (strInfo.Contains(itrItem.Key)) Then
                If (nKeyLen < itrItem.Key.Length()) Then
                    io_NUM_PD_IMG_IDX = itrItem.Value
                    'Update to new Key Value
                    nKeyLen = itrItem.Key.Length()
                End If
                '
                bUpdateReq = True
            End If
        Next
        '
        SetProduct(io_NUM_PD_IMG_IDX)
        '
        Return bUpdateReq
    End Function

    '========[ VP6300, Vendi-Pro, Tamper Map ]=======
    'B0,
    'b0 = 0-2; b1 = 1-3; b2=4-6; b3=5-7
    'b4 = Voltage;
    'b5 = Temperature;
    'b6 = Clock
    'B1,
    'bit0 -> TM4 Physical Tamper detected
    'bit1 -> TM4 Voltage Tamper detected
    'bit2 -> TM4 Temperature Tamper detected
    'bit3 -> TM4 Battery Tamper detected
    Shared s_strLstTamperPair_VP6300_VendiPro As String() = New String() {"0-2", "1-3", "4-6", "5-7" _
            , "Volt", "Temp", "Clock", "N/A" _
            , "TM4-Phy", "TM4-Volt", "TM4-Temp", "TM4-Bat"}

    '========[ VP5300, SPTP II, Tamper Map. same as VP6300 ]=======
    Shared s_strLstTamperPair_VP5300_SPTP_II As String() = s_strLstTamperPair_VP6300_VendiPro

    '[ Return ] = "Tamper #" or "Wire #", where # is 0-based number.
    Function GetStrTestTamper(byteIndicator As Byte) As String
        Select Case (m_PD_IMG)
            Case ENUM_PD_IMG_IDX.AR_PISCES, ENUM_PD_IMG_IDX.AR_PISCES_V0
                Select Case (byteIndicator)
                    'Passive Tamper --> Wire
                    Case &H2, &H3
                        Return "Tamper " + (byteIndicator - &H2).ToString()
                    Case Else
                        'Return "Tamper " + byteIndicator.ToString()
                End Select
            Case ENUM_PD_IMG_IDX.NEO_20_VP6300_VENDI_PRO
                If (byteIndicator < s_strLstTamperPair_VP6300_VendiPro.Count) Then
                    Return "Tamper " + s_strLstTamperPair_VP6300_VendiPro(byteIndicator)
                End If
            Case ENUM_PD_IMG_IDX.NEO_20_VP5300_SPTP_II
                If (byteIndicator < s_strLstTamperPair_VP5300_SPTP_II.Count) Then
                    Return "Tamper " + s_strLstTamperPair_VP5300_SPTP_II(byteIndicator)
                End If
            Case Else
                'Return "Tamper " + byteIndicator.ToString()
        End Select
        '
        Return "Unknown Tamper " + byteIndicator.ToString()
    End Function

    Property GetTouchSignRect As Rectangle
        Set(value As Rectangle)
            m_Rect_Signature = value
        End Set
        Get
            Return m_Rect_Signature
        End Get
    End Property

    Public Sub GetBrightnessValRange(ByRef o_u16ContrastMax As UInt16, ByRef o_u16Contrast As UInt16, ByRef o_u16Off As UInt16)
        'See 83-2E Set LCD Brightness Cmd/Resp @ AR 2.1.5
        ' 2016 Oct 28 Updated, goofy_liu@idtechproducts.com.tw
        o_u16ContrastMax = 63
        o_u16Contrast = 0
        o_u16Off = 5
        '
        Select Case (m_PD_IMG)
            Case ENUM_PD_IMG_IDX.AR_HG0_VP8600
                o_u16ContrastMax = 511
        End Select
    End Sub

#If 0 Then
     Function GetBtnLayoutDisablePartDuringUSBHIDConn() As List(Of Button)
        'Dim pInst As ClassCmdLineArgumentOptions = ClassCmdLineArgumentOptions.GetInstance()
        'If (pInst.bIsEngMode) Then
        If (m_bIsEngMode) Then
            Return GetBtnLayoutDisablePartDuringRS232Conn()
        End If
        With Form01_Main
            Select Case (m_PD_IMG)
                Case ENUM_PD_IMG_IDX.AR_HG4_VENDIII, ENUM_PD_IMG_IDX.NEO_VENDI_V0, ENUM_PD_IMG_IDX.GR_ECA_KIOSKII, ENUM_PD_IMG_IDX.GR_EC8_KIOSKII, ENUM_PD_IMG_IDX.AR_HG4_VENDIII_AR_2_1_5
                    'Return New List(Of Button) From {
                    '    .Button_TM001_MSR,
                    '    .Button_QC001_MSR,
                    '    .Button_TM002_RFIDAntenna,
                    '    .Button_QC002_RFIDAntenna,
                    '    .Button_TM003_SecurityElements,
                    '    .Button_QC003_SecurityElements,
                    '    .Button_TM004_TouchScreen,
                    '    .Button_QC004_TouchScreen,
                    '    .Button_TM005_LEDs,
                    '    .Button_QC005_LEDs,
                    '    .Button_TM006_LCDPixels,
                    '    .Button_QC006_LCDPxiels,
                    '    .Button_TM007_LCDContrast,
                    '    .Button_QC007_LCDContrast,
                    '    .Button_TM008_ICC,
                    '    .Button_QC008_ICC,
                    '    .Button_TM009_SAMs,
                    '    .Button_QC009_SAMs,
                    '    .Button_TM010_KeyPad,
                    '    .Button_QC010_KeyPad,
                    '    .Button_TM011_Buzzer,
                    '    .Button_QC011_Buzzer,
                    '.Button_TM801_SerialNo_Get,
                    '.Button_QC804_SystemGRBackCompatibleMode,
                    '.Button_TM803_SystemProcessTypeGet,
                    '.Button_QC802_SystemProcessorType,
                    '.Button_TM802_SystemProcessorIdGet,
                    '.Button_QC801_SystemProcessorId,
                    '.Button_QC805_SerialNumber,
                    '.Button_QC806_SystemHardwareInfo_And_ReaderButtonsConfiguration,
                    '.Button_TM804_SystemHardwareInfo_And_ReaderButtonsConfiguration,
                    '.Button_QC804_SystemGRBackCompatibleMode
                    '    }
                    '                        .Button_TM800_FirmwareVersion,
                    '.Button_QC803_SystemFirmwareVersion,

            End Select

        End With

        Return Nothing
    End Function

    Function GetBtnLayoutDisablePartDuringRS232Conn() As List(Of Button)
        Dim lstBtn As List(Of Button)
        Dim pFormMain As Form01_Main = Form01_Main.GetInstance()

        Return New List(Of Control) From {
                       pFormMain.SplitContainer1
                       }
#If 0 Then


        With Form01_Main
            Select Case (m_PD_IMG)
                Case ENUM_PD_IMG_IDX.AR_HG4_VENDIII, ENUM_PD_IMG_IDX.AR_HG4_VENDIII_AR_2_1_5  'Little - 128x64
                    Return New List(Of Button) From {
                        .Button_TM014_EthPing,
                        .Button_TM015_Tamper,
                        .Button_TM010_TouchScreen,
                        .Button_QC010_TouchScreen,
                        .Button_TM804_SystemHardwareInfo_And_ReaderButtonsConfiguration,
                        .Button_QC806_SystemHardwareInfo_And_ReaderButtonsConfiguration,
                        .Button_TM800_FirmwareVersion,
                        .Button_TM802_SystemProcessorIdGet,
                        .Button_QC801_SystemProcessorId,
                        .Button_QC804_SystemGRBackCompatibleMode,
                        .Button_TM800_FirmwareVersion,
                        .Button_TM803_SystemProcessTypeGet,
                        .Button_QC802_SystemProcessorType,
                        .Button_TM804_SystemHardwareInfo_And_ReaderButtonsConfiguration,
                        .Button_QC806_SystemHardwareInfo_And_ReaderButtonsConfiguration
                        }
                Case ENUM_PD_IMG_IDX.NEO_VENDI_V0, ENUM_PD_IMG_IDX.NEO_VENDI_NEW
                    Return New List(Of Button) From {
                        .Button_TM014_EthPing,
                        .Button_TM015_Tamper,
                        .Button_TM805_MasterKeys,
                        .Button_QC807_MasterKeys,
                        .Button_TM010_TouchScreen,
                        .Button_QC010_TouchScreen,
                        .Button_TM007_LCDContrast,
                        .Button_QC007_LCDContrast,
                        .Button_TM003_ICC,
                        .Button_QC003_ICC,
                        .Button_TM008_SAMs,
                        .Button_QC008_SAMs,
                        .Button_TM802_SystemProcessorIdGet,
                        .Button_QC801_SystemProcessorId,
                        .Button_QC804_SystemGRBackCompatibleMode,
                        .Button_TM800_FirmwareVersion,
                        .Button_TM803_SystemProcessTypeGet,
                        .Button_QC802_SystemProcessorType,
                        .Button_TM804_SystemHardwareInfo_And_ReaderButtonsConfiguration,
                        .Button_QC806_SystemHardwareInfo_And_ReaderButtonsConfiguration
                        }
                Case ENUM_PD_IMG_IDX.NEO_VP3300_AUDIO_JACK, ENUM_PD_IMG_IDX.NEO_UNIPAY_III_iBase, ENUM_PD_IMG_IDX.NEO_VP3300USB, ENUM_PD_IMG_IDX.NEO_VP3300USB_C, ENUM_PD_IMG_IDX.NEO_VP3300USB_E
                    lstBtn = New List(Of Button) From {
                        .Button_TM805_MasterKeys,
                        .Button_QC807_MasterKeys,
                        .Button_TM010_TouchScreen,
                        .Button_QC010_TouchScreen,
                        .Button_TM007_LCDContrast,
                        .Button_QC007_LCDContrast,
                        .Button_TM006_LCDPixels,
                        .Button_QC006_LCDPixels,
                        .Button_TM008_SAMs,
                        .Button_QC008_SAMs,
                        .Button_TM014_EthPing,
                        .Button_TM015_Tamper,
                        .Button_TM802_SystemProcessorIdGet,
                        .Button_QC801_SystemProcessorId,
                        .Button_QC804_SystemGRBackCompatibleMode,
                        .Button_TM800_FirmwareVersion,
                        .Button_TM803_SystemProcessTypeGet,
                        .Button_QC802_SystemProcessorType,
                        .Button_TM804_SystemHardwareInfo_And_ReaderButtonsConfiguration,
                       .Button_QC806_SystemHardwareInfo_And_ReaderButtonsConfiguration
                        }
                    If (m_PD_IMG = ENUM_PD_IMG_IDX.NEO_VP3300USB) Then
                        lstBtn.Add(.Button_TM008_RS232)
                        lstBtn.Add(.Button_QC010_RS232)
                    End If
                    If (m_PD_IMG = ENUM_PD_IMG_IDX.NEO_VP3300USB_E) Then
                        lstBtn.Add(.Button_TM008_RS232)
                        lstBtn.Add(.Button_QC010_RS232)
                        lstBtn.Add(.Button_TM001_MSR)
                        lstBtn.Add(.Button_QC001_MSR)
                    End If
                    If (m_PD_IMG = ENUM_PD_IMG_IDX.NEO_VP3300USB_C) Then
                        lstBtn.Add(.Button_TM008_RS232)
                        lstBtn.Add(.Button_QC010_RS232)
                        lstBtn.Add(.Button_TM001_MSR)
                        lstBtn.Add(.Button_QC001_MSR)
                        lstBtn.Add(.Button_TM003_ICC)
                        lstBtn.Add(.Button_QC003_ICC)
                    End If
                    Return lstBtn
                Case ENUM_PD_IMG_IDX.NEO_UNIPAY_1D5
                    Return New List(Of Button) From {
                        .Button_TM002_RFIDAntenna,
                    .Button_QC002_RFIDAntenna,
                    .Button_TM805_MasterKeys,
                    .Button_QC807_MasterKeys,
                    .Button_TM010_TouchScreen,
                    .Button_QC010_TouchScreen,
                    .Button_TM007_LCDContrast,
                    .Button_QC007_LCDContrast,
                    .Button_TM006_LCDPixels,
                    .Button_QC006_LCDPixels,
                    .Button_TM007_DateTimeSync,
                    .Button_QC007_DateTimeChk,
                    .Button_TM014_EthPing,
                    .Button_TM015_Tamper,
                    .Button_TM008_SAMs,
                    .Button_QC008_SAMs,
                    .Button_TM802_SystemProcessorIdGet,
                    .Button_QC801_SystemProcessorId,
                    .Button_QC804_SystemGRBackCompatibleMode,
                    .Button_TM800_FirmwareVersion,
                    .Button_TM803_SystemProcessTypeGet,
                    .Button_QC802_SystemProcessorType,
                    .Button_TM804_SystemHardwareInfo_And_ReaderButtonsConfiguration,
                    .Button_QC806_SystemHardwareInfo_And_ReaderButtonsConfiguration
                        }
                Case ENUM_PD_IMG_IDX.NEO_UNIPAY_1D5_TTK
                    Return New List(Of Button) From {
                        .Button_TM002_RFIDAntenna,
                    .Button_QC002_RFIDAntenna,
                    .Button_TM805_MasterKeys,
                    .Button_QC807_MasterKeys,
                    .Button_TM010_TouchScreen,
                    .Button_QC010_TouchScreen,
                    .Button_TM007_LCDContrast,
                    .Button_QC007_LCDContrast,
                    .Button_TM006_LCDPixels,
                    .Button_QC006_LCDPixels,
                    .Button_TM003_KeyPad,
                    .Button_QC003_KeyPad,
                    .Button_QC005_Buzzer,
                    .Button_TM005_Buzzer,
                    .Button_TM008_RS232,
                    .Button_QC010_RS232,
                    .Button_TM007_DateTimeSync,
                    .Button_QC007_DateTimeChk,
                    .Button_TM014_EthPing,
                    .Button_TM015_Tamper,
                    .Button_TM008_SAMs,
                    .Button_QC008_SAMs,
                    .Button_TM802_SystemProcessorIdGet,
                    .Button_QC801_SystemProcessorId,
                    .Button_QC804_SystemGRBackCompatibleMode,
                    .Button_TM800_FirmwareVersion,
                    .Button_TM803_SystemProcessTypeGet,
                    .Button_QC802_SystemProcessorType,
                    .Button_TM804_SystemHardwareInfo_And_ReaderButtonsConfiguration,
                    .Button_QC806_SystemHardwareInfo_And_ReaderButtonsConfiguration
                        }
                Case ENUM_PD_IMG_IDX.GR_ECA_KIOSKII, ENUM_PD_IMG_IDX.GR_EC8_KIOSKII
                    Return New List(Of Button) From {
                        .Button_TM014_EthPing,
                        .Button_TM015_Tamper,
                        .Button_TM001_MSR,
                        .Button_QC001_MSR,
                        .Button_TM805_MasterKeys,
                        .Button_QC807_MasterKeys,
                        .Button_TM010_TouchScreen,
                        .Button_QC010_TouchScreen,
                        .Button_TM007_LCDContrast,
                        .Button_QC007_LCDContrast,
                        .Button_TM006_LCDPixels,
                        .Button_QC006_LCDPixels,
                        .Button_TM003_ICC,
                        .Button_QC003_ICC,
                        .Button_TM008_SAMs,
                        .Button_QC008_SAMs,
                        .Button_TM802_SystemProcessorIdGet,
                        .Button_QC801_SystemProcessorId,
                        .Button_QC804_SystemGRBackCompatibleMode,
                        .Button_TM800_FirmwareVersion,
                        .Button_TM803_SystemProcessTypeGet,
                        .Button_QC802_SystemProcessorType,
                        .Button_TM804_SystemHardwareInfo_And_ReaderButtonsConfiguration,
                       .Button_QC806_SystemHardwareInfo_And_ReaderButtonsConfiguration
                        }
            End Select

        End With
        Return Nothing
#End If
    End Function

    Function GetBtnLayoutDisablePart() As List(Of Button)

        If (m_refobjRdrCmder.bIsConnectedUSBHID()) Then
            Return GetBtnLayoutDisablePartDuringUSBHIDConn()
        Else
            Return GetBtnLayoutDisablePartDuringRS232Conn()
        End If

        Return Nothing

    End Function

     ReadOnly Property bIsBaudrateCustShipping As Boolean
        Get
            Return byteIsShippingCfg007_COMBaudrate
        End Get
    End Property


    ReadOnly Property getStrTagXMLPartNumCustCfgParams As String
        Get
            Dim strCfgTxtInfo As String = m_TagXMLPartNumCustCfgParams.getStrTagXMLPartNumCustCfgParams
            '2016 Dec 06, UniPay 1.5 TTK Special Case, FFEE1D, Pre-PAN Setting
            If (m_TagXMLPartNumCustCfgParams.GetItemEnable(ENU_CUST_CFG_KEYTYPE.CTCFG_ITEM010_ENCRPT_3_PAN_PRE)) Then
                strCfgTxtInfo = strCfgTxtInfo + m_TagXMLPartNumCustCfgParams.getStrTagXMLPartNumCustCfgParams_Ext_001_PrePAN
            End If

            Return strCfgTxtInfo
        End Get
    End Property


    Public ReadOnly Property refobjTestScenario As ClassTestScenarioBasic
        Get
            Return m_refobjTestScenario
        End Get
    End Property


 Public Sub ConfigReaderCmderAndTestScenario(ByVal refobjRdrCmder As ClassReaderProtocolBasic, ByVal refobjTestScenario As ClassTestScenarioBasic, Optional ByVal bReplace As Boolean = True)
        If (bReplace) Then
            m_refobjRdrCmder = refobjRdrCmder
            m_refobjTestScenario = refobjTestScenario

        Else
            If (refobjRdrCmder IsNot Nothing) Then
                m_refobjRdrCmder = refobjRdrCmder
            End If
            If (refobjTestScenario IsNot Nothing) Then
                m_refobjTestScenario = refobjTestScenario
            End If
        End If
        'Initialize shipping configuration values
        m_refobjTestScenario.InitProgressBarPrepare()
    End Sub
#End If
End Class
