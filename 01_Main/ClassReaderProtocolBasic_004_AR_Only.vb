﻿'==================================================================================
' File Name: ClassReaderProtocolBasic.vb
' Description: The ClassReaderProtocolBasic is as Must-be Inherited Base Class of Reader's Packet Parser & Composer.
' Who use it as Base Class: ClassReaderProtocolBasicIDGAR, ClassReaderProtocolBasicIDGAR, and ClassReaderProtocolBasicIDTECH
' For AR Platform ONLY Functions
' 
' Revision:
' -----------------------[ Initial Version ]-----------------------
' 2017 Oct 06, by goofy_liu@idtechproducts.com.tw

'==================================================================================
'
'----------------------[ Importing Stuffs ]----------------------
Imports System.Runtime.Serialization
Imports System.Runtime.Serialization.Formatters.Binary
'Imports DiagTool_HardwareExclusive.ClassReaderProtocolBasicIDGAR
Imports System.IO
Imports System.IO.Ports
Imports System.Threading

'-------------------[ Class Declaration ]-------------------

Partial Public Class ClassReaderProtocolBasic
    '
    '
    Public Structure ARSystemFileDirList
        '
        Public m_listFileInfo As SortedDictionary(Of String, UInt32)
        '
        ReadOnly Property GetTotalSizeAmount As Integer
            Get
                Dim nSum As Integer = 0
                For Each iteratorSeek In m_listFileInfo
                    nSum = nSum + iteratorSeek.Value
                Next
                Return nSum
            End Get
        End Property
        '
        Public Sub RemoveFile(strFileName As String)
            If (m_listFileInfo.ContainsKey(strFileName)) Then
                m_listFileInfo.Remove(strFileName)
            End If
        End Sub
        '
        Public Function ParseReaderData(arrayByteData As Byte()) As Boolean
            Dim strFileList As String = ""
            Dim strList As String() = Nothing

            Try
                'Cleanup Stuff And Renew if necessary
                If (m_listFileInfo Is Nothing) Then
                    Init()
                Else
                    Cleanup()
                End If
                ' Example = "/,Filename.bmp:19000,filename2.bin:9500"
                ClassTableListMaster.ConvertFromHexToAscii(arrayByteData, strFileList, True)
                'Common sym is delimiter
                strList = Split(strFileList, ",")
                'Setup List
                If (strList.Count > 0) Then
                    Dim strFileName As String = ""
                    Dim u32FileSize As UInt32
                    For Each iteratorSeek In strList
                        ParseReaderDataSingleGetFNameSize(iteratorSeek, strFileName, u32FileSize)
                        m_listFileInfo.Add(strFileName, u32FileSize)
                    Next
                End If
            Catch ex As Exception
                Throw 'ex
            Finally
                If (strList IsNot Nothing) Then
                    Erase strList
                End If
            End Try

            Return True
        End Function

        Public Sub Init()
            If (m_listFileInfo Is Nothing) Then
                m_listFileInfo = New SortedDictionary(Of String, UInt32)
            Else
                m_listFileInfo.Clear()
            End If

        End Sub
        '
        Sub Init(listTestFiles As List(Of String), strFilePath As String)
            'This is For Local file
            Cleanup()
            '
            For Each strFileName In listTestFiles
                If (File.Exists(strFilePath + strFileName)) Then
                    m_listFileInfo.Add(strFileName, CUInt(FileLen(strFilePath + strFileName)))
                End If
            Next
        End Sub
        '
        Public Sub Cleanup()
            If (m_listFileInfo IsNot Nothing) Then
                m_listFileInfo.Clear()
            End If
        End Sub

        Private Sub ParseReaderDataSingleGetFNameSize(strSource As String, ByRef o_strFileName As String, ByRef u32FileSize As UInteger)
            'Filename.ext:1234567890"
            Dim strList As String()
            strList = Split(strSource, ":")

            If ((strList IsNot Nothing) And (strList.Count > 1)) Then
                o_strFileName = strList(0)
                u32FileSize = Convert.ToUInt32(strList(1), 10)
            Else
                o_strFileName = ""
                u32FileSize = 0
            End If
        End Sub

        Function IsPresent(strFileName As String) As Boolean
            Return m_listFileInfo.ContainsKey(strFileName)
        End Function
    End Structure
    '
    '
    Public Enum GR_BACKWARD_COMPATIBLE_DF59 As Byte
        GBCM_OFF = &H0
        GBCM_ON = &H1
    End Enum
    '
    Public Overridable Sub ARExlusive_SystemSetGRBackwardCompatibleMode(Optional ByVal bModeOn As GR_BACKWARD_COMPATIBLE_DF59 = GR_BACKWARD_COMPATIBLE_DF59.GBCM_OFF, Optional ByVal nTimeout As Integer = ClassDevRS232.m_cnTX_TIMEOUT_IN_SEC)
        Try

        Catch ex As Exception
            LogLn("[Err] ARExlusive_SystemSetGRBackwardCompatibleMode() = " + ex.Message)
        Finally

        End Try
    End Sub

    '
    Public Shared m_dictARExclusive_ContactCardATRResponseMsg As Dictionary(Of UInt32, String)
    Private Sub ARExclusive_ATRResponseMsg_Init()
        'Captured from AR 2.1.3 Appendix I
        'ERR_ICC_BAD_RESPONSE, 0005 0000, ICCD Response was Incorrect
        'ERR_ICC_SLOT_IN_USE, 0005 0001,The slot is already in use for a complex command such as polling or Card Removal
        'ERR_ICC_DRIVER_RESOURCE_BUSY, 0005 0002, The driver resources are dedicated for processing a command on another slot.
        'ERR_ICC_CARD_NOT_PRESENT, 0005 0003, Error - Card was not present
        'ERR_ICC_CARD_NOT_READY, 0005 0004, The card is not ready for application level transactions since a TTL session has not been established with the card yet
        'ERR_ICC_L1_ERROR_CARD_DEACTIVATED,0005 0005,Level 1 error occurred. Card was deactivated by the ICC Service or the smart card driver

        'ERR_ICC_L1_ERROR_CARD_NOT_DEACTIVATED, 0005 0006,Level 1 error occurred. Card was not deactivated by the ICC Service or the smart card driver. Decision to deactivate card left up to the client application
        'ERR_ICC_CARD_DID_NOT_RESPOND, 0005 0007, The card failed to respond to the APDU command ViVOpay Advanced Reader Serial Interface Developers Guide 730-1002-07 Rev 1.0 Page 320
        'ERR_ICC_ATR_NOT_AVAILABLE, 0005 0008, Card is present but ATR data not yet available. Wait until Card is Ready before calling this function.
        'ERR_ICC_UNKNOWN_MODE, 0005 0009, ATR was returned but the Mode is not known. This condition will occur if a non-standard ATR is returned by the card.
        'ERR_ICC_COMMAND_NOT_ALLOWED, 0005 000A, Command not allowed (Command used out of sequence)
        If (m_dictARExclusive_ContactCardATRResponseMsg Is Nothing) Then
            m_dictARExclusive_ContactCardATRResponseMsg = New Dictionary(Of UInteger, String) From {
                {&H50000, "ICCD Response was Incorrect"},
                {&H50001, "The slot is already in use for a complex command such as polling or Card Removal"},
                {&H50002, "The driver resources are dedicated for processing a command on another slot."},
                {&H50003, "Error - Card was not present"},
                {&H50004, "The card is not ready for application level transactions since a TTL session has not been established with the card yet"},
                {&H50005, "Level 1 error occurred. Card was deactivated by the ICC Service or the smart card driver"},
                {&H50006, "Level 1 error occurred. Card was not deactivated by the ICC Service or the smart card driver. Decision to deactivate card left up to the client application"},
                {&H50007, "The card failed to respond to the APDU command ViVOpay Advanced Reader Serial Interface Developers Guide 730-1002-07 Rev 1.0 Page 320"},
                {&H50008, "Card is present but ATR data not yet available. Wait until Card is Ready before calling this function."},
                {&H50009, "ATR was returned but the Mode is not known. This condition will occur if a non-standard ATR is returned by the card."},
                {&H5000A, "Command not allowed (Command used out of sequence)"}
                }
        End If
    End Sub
    Private Sub ARExclusive_ATRResponseMsg_Cleanup()
        If (m_dictARExclusive_ContactCardATRResponseMsg IsNot Nothing) Then
            m_dictARExclusive_ContactCardATRResponseMsg.Clear()
        End If
    End Sub

    Overridable Sub ARExclusive_System_EnterBootloader(Optional ByVal nTimeout As Integer = ClassDevRS232.m_cnTX_TIMEOUT_IN_SEC)
        '2017 Feb 22, From PISCES, AR 3.0.0
        Dim u16Cmd As UInt16 = &H770A
        Dim strFuncName As String = "ARExclusive_System_EnterBootloader"

        If (m_bIDGCmdBusy) Then
            Return
        End If
        m_bIDGCmdBusy = True

        Try
            '-----------------------------------------------------------------[ Send to reader ]-----------------------------------------------------------------
            System_Customized_Command(u16Cmd, strFuncName, Nothing, Nothing, AddressOf ClassReaderProtocolBasic.PreProcessCbRecv, nTimeout)

            If (bIsRetStatusOK) Then

            Else

            End If

        Catch ext As TimeoutException
            LogLn("[Err][Timeout] [IDG: " + GetIDGCmdString(u16Cmd) + "] " + strFuncName + "() = " + ext.Message)
        Catch ex As Exception
            LogLn("[Err] [IDG:  " + GetIDGCmdString(u16Cmd) + "] " + strFuncName + "() = " + ex.Message)
        Finally
            m_tagIDGCmdDataInfo.Cleanup()
            m_bIDGCmdBusy = False
        End Try
    End Sub

    '
    Public Overridable Sub SystemLCDClear(Optional ByVal nTimeout As Integer = ClassDevRS232.m_cnTX_TIMEOUT_IN_SEC)
        Dim u16Cmd As UInt16 = &HF0F9
        Dim strFuncName As String = "SystemLCDClear"
        Dim arrayByteSend As Byte() = Nothing
        Dim arrayByteRecv As Byte() = Nothing

        If (m_bIDGCmdBusy) Then
            Return
        End If
        m_bIDGCmdBusy = True

        Try
            'ClassTableListMaster.ConvertValue2ArrayByte(byteModeOn, arrayByteSend)

            System_Customized_Command(u16Cmd, strFuncName, arrayByteSend, arrayByteRecv, AddressOf PreProcessCbRecv, nTimeout)

            If (bIsRetStatusOK) Then

            Else

            End If
        Catch ex As Exception

        Finally
            If (arrayByteSend IsNot Nothing) Then
                Erase arrayByteSend
            End If
            If (arrayByteRecv IsNot Nothing) Then
                Erase arrayByteRecv
            End If

            m_bIDGCmdBusy = False
        End Try
    End Sub
    '
  

    Public Overridable Sub ARExclusive_System_GraphiceReset(Optional ByVal nTimeout As Integer = ClassDevRS232.m_cnTX_TIMEOUT_IN_SEC)
        Dim u16Cmd As UInt16 = &H8301
        Dim strFuncName As String = "ARExclusive_System_GraphiceReset"

        If (m_bIDGCmdBusy) Then
            Return
        End If
        m_bIDGCmdBusy = True

        Try
            '-----------------------------------------------------------------[ Send to reader ]-----------------------------------------------------------------
            System_Customized_Command(u16Cmd, strFuncName, Nothing, Nothing, AddressOf ClassReaderProtocolBasic.PreProcessCbRecv, nTimeout)

            If (bIsRetStatusOK) Then

            Else

            End If

        Catch ext As TimeoutException
            LogLn("[Err][Timeout] [IDG: " + GetIDGCmdString(u16Cmd) + "] " + strFuncName + "() = " + ext.Message)
        Catch ex As Exception
            LogLn("[Err] [IDG:  " + GetIDGCmdString(u16Cmd) + "] " + strFuncName + "() = " + ex.Message)
        Finally
            m_tagIDGCmdDataInfo.Cleanup()
            m_bIDGCmdBusy = False
        End Try

    End Sub
    '
    Public Overridable Sub ARExclusive_System_LCDCustMode_Stop(nTimeout As Integer)
        Dim u16Cmd As UInt16 = &H8309
        Dim strFuncName As String = "ARExclusive_System_LCDCustMode_Stop"

        If (m_bIDGCmdBusy) Then
            Return
        End If
        m_bIDGCmdBusy = True

        Try
            '-----------------------------------------------------------------[ Send to reader ]-----------------------------------------------------------------
            System_Customized_Command(u16Cmd, strFuncName, Nothing, Nothing, AddressOf ClassReaderProtocolBasic.PreProcessCbRecv, nTimeout)

            If (bIsRetStatusOK) Then

            Else

            End If

        Catch ext As TimeoutException
            LogLn("[Err][Timeout] [IDG: " + GetIDGCmdString(u16Cmd) + "] " + strFuncName + "() = " + ext.Message)
        Catch ex As Exception
            LogLn("[Err] [IDG:  " + GetIDGCmdString(u16Cmd) + "] " + strFuncName + "() = " + ex.Message)
        Finally
            m_tagIDGCmdDataInfo.Cleanup()
            m_bIDGCmdBusy = False
        End Try

    End Sub

    Public Sub ARExclusive_System_Touch_Signature_Get_TxONLY()
        Dim u16Cmd As UInt16 = &H8309
        Dim strFuncName As String = "ARExclusive_System_LCDCustMode_Stop"
        Dim aryTxData() As Byte = Nothing
        If (m_bIDGCmdBusy) Then
            Return
        End If
        m_bIDGCmdBusy = True

        Try
            '-----------------------------------------------------------------[ Send to reader ]-----------------------------------------------------------------

            aryTxData = New Byte() {&H28, &H2}
            ' No Wait since waiting for
            System_Customized_Command(u16Cmd, strFuncName, aryTxData, Nothing, AddressOf ClassReaderProtocolBasic.PreProcessCbRecv, 10 * 1000, False, False)

            If (bIsRetStatusOK) Then

            Else

            End If

        Catch ext As TimeoutException
            LogLn("[Err][Timeout] [IDG: " + GetIDGCmdString(u16Cmd) + "] " + strFuncName + "() = " + ext.Message)
        Catch ex As Exception
            LogLn("[Err] [IDG:  " + GetIDGCmdString(u16Cmd) + "] " + strFuncName + "() = " + ex.Message)
        Finally
            Erase aryTxData
            m_tagIDGCmdDataInfo.Cleanup()
            m_bIDGCmdBusy = False
        End Try
    End Sub
    '
    Public Sub ARExclusive_System_Touch_Signature_Get(ByRef o_aryRxPNGData() As Byte, Optional ByVal nTimeout As Integer = ClassDevRS232.m_cnTX_TIMEOUT_IN_SEC)
        Dim u16Cmd As UInt16 = &H8302
        Dim strFuncName As String = "ARExclusive_System_LCDCustMode_Stop, V2"
        Dim aryTxData() As Byte = Nothing
        If (m_bIDGCmdBusy) Then
            Return
        End If
        m_bIDGCmdBusy = True

        Try
            '-----------------------------------------------------------------[ Send to reader ]-----------------------------------------------------------------

            aryTxData = New Byte() {&H28, &H2}
            ' No Wait since waiting for
            System_Customized_Command(u16Cmd, strFuncName, aryTxData, o_aryRxPNGData, AddressOf ClassReaderProtocolBasic.PreProcessCbRecv, nTimeout)

            If (bIsRetStatusOK) Then

            Else

            End If

        Catch ext As TimeoutException
            LogLn("[Err][Timeout] [IDG: " + GetIDGCmdString(u16Cmd) + "] " + strFuncName + "() = " + ext.Message)
        Catch ex As Exception
            LogLn("[Err] [IDG:  " + GetIDGCmdString(u16Cmd) + "] " + strFuncName + "() = " + ex.Message)
        Finally
            Erase aryTxData
            m_tagIDGCmdDataInfo.Cleanup()
            m_bIDGCmdBusy = False
        End Try
    End Sub
    '
    Public Sub ARExclusive_System_Touch_Signature_Cust_Start(ByVal i_Rect As Rectangle, ByRef o_u32GrphId As UInt32, Optional ByVal nTimeout As Integer = ClassDevRS232.m_cnTX_TIMEOUT_IN_SEC)
        Dim u16Cmd As UInt16 = &H8319
        Dim strFuncName As String = "ARExclusive_System_Touch_Signature_Cust_Start"
        Dim aryTxData() As Byte = Nothing
        Dim aryRxData() As Byte = Nothing
        Dim strTx As String

        If (m_bIDGCmdBusy) Then
            Return
        End If
        m_bIDGCmdBusy = True

        Try
            '-----------------------------------------------------------------[ Send to reader ]-----------------------------------------------------------------

            'V2 Tx Format : X_Pos\00Y_Pos\00Width\00Height\00\02\01\00
            strTx = String.Format("{0} 00 {1} 00 {2} 00 {3} 00 02 01",
                                  ClassTableListMaster.ConvertFromAscii2HexString(i_Rect.Left.ToString()),
                                  ClassTableListMaster.ConvertFromAscii2HexString(i_Rect.Top.ToString()),
                                  ClassTableListMaster.ConvertFromAscii2HexString(i_Rect.Width.ToString()),
                                  ClassTableListMaster.ConvertFromAscii2HexString(i_Rect.Height.ToString()))
            'Hex Str to Byte Array
            ClassTableListMaster.TLVauleFromString2ByteArray(strTx, aryTxData)

            System_Customized_Command(u16Cmd, strFuncName, aryTxData, Nothing, AddressOf ClassReaderProtocolBasic.PreProcessCbRecv, nTimeout)

            '
            o_u32GrphId = &H0
            If (bIsRetStatusOK) Then
                If (m_tagIDGCmdDataInfo.bIsRawData) Then
                    m_tagIDGCmdDataInfo.ParserDataGet(o_u32GrphId)
                End If
            End If

        Catch ext As TimeoutException
            LogLn("[Err][Timeout] [IDG: " + GetIDGCmdString(u16Cmd) + "] " + strFuncName + "() = " + ext.Message)
        Catch ex As Exception
            LogLn("[Err] [IDG:  " + GetIDGCmdString(u16Cmd) + "] " + strFuncName + "() = " + ex.Message)
        Finally
            Erase aryTxData
            m_tagIDGCmdDataInfo.Cleanup()
            m_bIDGCmdBusy = False
        End Try
    End Sub
    '
    Public Sub ARExclusive_System_Touch_Signature_Cust_Get(ByVal u32GrphId As UInt32, ByRef o_aryBytePNG As Byte(), Optional ByVal nTimeout As Integer = ClassDevRS232.m_cnTX_TIMEOUT_IN_SEC)
        Dim u16Cmd As UInt16 = &H831A
        Dim strFuncName As String = "ARExclusive_System_Touch_Signature_Cust_Get"
        Dim aryTxData() As Byte = Nothing
        Dim aryRxData() As Byte = Nothing

        If (m_bIDGCmdBusy) Then
            Return
        End If
        m_bIDGCmdBusy = True

        Try
            '-----------------------------------------------------------------[ Send to reader ]-----------------------------------------------------------------
            'Must use "And &HFF" to prevent from Arithmetic  Overflow Exception
            aryTxData = New Byte() {
                                  CType((u32GrphId >> 24) And &HFF, Byte),
                                 CType((u32GrphId >> 16) And &HFF, Byte),
                                  CType((u32GrphId >> 8) And &HFF, Byte),
                                  CType(u32GrphId And &HFF, Byte)
                              }
            System_Customized_Command(u16Cmd, strFuncName, aryTxData, o_aryBytePNG, AddressOf ClassReaderProtocolBasic.PreProcessCbRecv, nTimeout)

            '
            If (bIsRetStatusOK) Then

            End If

        Catch ext As TimeoutException
            LogLn("[Err][Timeout] [IDG: " + GetIDGCmdString(u16Cmd) + "] " + strFuncName + "() = " + ext.Message)
        Catch ex As Exception
            LogLn("[Err] [IDG:  " + GetIDGCmdString(u16Cmd) + "] " + strFuncName + "() = " + ex.Message)
        Finally
            Erase aryTxData
            '
            m_bIDGCmdBusy = False
        End Try
    End Sub
    '
    '
    Public Sub ARExclusive_System_Touch_Signature_Cust_Clear(ByVal u32GrphId As UInt32, Optional ByVal nTimeout As Integer = ClassDevRS232.m_cnTX_TIMEOUT_IN_SEC)
        Dim u16Cmd As UInt16 = &H831B
        Dim strFuncName As String = "ARExclusive_System_Touch_Signature_Cust_Clear"
        Dim aryTxData() As Byte = Nothing
        Dim aryRxData() As Byte = Nothing

        If (m_bIDGCmdBusy) Then
            Return
        End If
        m_bIDGCmdBusy = True

        Try
            '-----------------------------------------------------------------[ Send to reader ]-----------------------------------------------------------------
            'Must use "And &HFF" to prevent from Arithmetic  Overflow Exception
            aryTxData = New Byte() {
                                  CType((u32GrphId >> 24) And &HFF, Byte),
                                 CType((u32GrphId >> 16) And &HFF, Byte),
                                  CType((u32GrphId >> 8) And &HFF, Byte),
                                  CType(u32GrphId And &HFF, Byte)
                              }
            System_Customized_Command(u16Cmd, strFuncName, aryTxData, Nothing, AddressOf ClassReaderProtocolBasic.PreProcessCbRecv, nTimeout)

            '
            If (bIsRetStatusOK) Then

            End If

        Catch ext As TimeoutException
            LogLn("[Err][Timeout] [IDG: " + GetIDGCmdString(u16Cmd) + "] " + strFuncName + "() = " + ext.Message)
        Catch ex As Exception
            LogLn("[Err] [IDG:  " + GetIDGCmdString(u16Cmd) + "] " + strFuncName + "() = " + ex.Message)
        Finally
            Erase aryTxData
            '
            m_bIDGCmdBusy = False
        End Try
    End Sub
    '
    '
    Protected Sub ARExclusive_System_UISourceCust(ByVal i_aryUICfg_SetSource As Byte(), ByRef o_aryUICfg_GetSource As Byte(), Optional ByVal nTimeout As Integer = ClassDevRS232.m_cnTX_TIMEOUT_IN_SEC)
        '01-05 , Set Source --> len = 02, dat = XX,YY, no data
        '01-05 , Get Source --> len = 00, dat = XX,YY, resp data len = 2
        'Using AR platform.

        Dim u16Cmd As UInt16 = &H105
        Dim strFuncName As String = "ARExclusive_System_UISourceCust"
        Dim aryDatTx As Byte() '= New Byte() {byteTamperCmd}
        'Dim aryDatRx As Byte() '= Nothing

        If (m_bIDGCmdBusy) Then
            Return
        End If
        m_bIDGCmdBusy = True

        Try
            '-----------------------------------------------------------------[ Send to reader ]-----------------------------------------------------------------
            System_Customized_Command(u16Cmd, strFuncName, i_aryUICfg_SetSource, o_aryUICfg_GetSource, AddressOf ClassReaderProtocolBasic.PreProcessCbRecv, nTimeout)

            If (bIsRetStatusOK) Then

            Else

            End If

        Catch ext As TimeoutException
            LogLn("[Err][Tx/Rx Timeout] [IDG: " + GetIDGCmdString(u16Cmd) + "] " + strFuncName + "() = " + ext.Message)
        Catch ex As Exception
            LogLn("[Err] [IDG:  " + GetIDGCmdString(u16Cmd) + "] " + strFuncName + "() = " + ex.Message)
        Finally
            m_tagIDGCmdDataInfo.Cleanup()
            m_bIDGCmdBusy = False
            Erase aryDatTx
            '
        End Try

    End Sub
    '
    Public Overridable Sub ARExclusive_System_UISourceGet(ByRef o_aryUICfg As Byte(), Optional ByVal nTimeout As Integer = ClassDevRS232.m_cnTX_TIMEOUT_IN_SEC)
        ARExclusive_System_UISourceCust(Nothing, o_aryUICfg, nTimeout)
    End Sub

    Public Overridable Sub ARExclusive_System_UISourceSet(ByVal i_aryUICfg As Byte(), Optional ByVal nTimeout As Integer = ClassDevRS232.m_cnTX_TIMEOUT_IN_SEC)
        If (i_aryUICfg Is Nothing) Then
            Return
        End If
        If (i_aryUICfg.Count <> 2) Then
            Return
        End If
        ARExclusive_System_UISourceCust(i_aryUICfg, Nothing, nTimeout)
    End Sub
    '
    Public Overridable Sub ARExclusive_SystemFileDirList(ByRef listFiles As ARSystemFileDirList, Optional ByVal nTimeout As Integer = ClassDevRS232.m_cnTX_TIMEOUT_IN_SEC)
        Try

        Catch ex As Exception
            LogLn("[Err] () = " + ex.Message)
        Finally

        End Try
    End Sub
    '
End Class
