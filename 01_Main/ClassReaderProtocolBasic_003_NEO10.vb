﻿'==================================================================================
' File Name: ClassReaderProtocolBasic.vb
' Description: The ClassReaderProtocolBasic is as Must-be Inherited Base Class of Reader's Packet Parser & Composer.
' Who use it as Base Class: ClassReaderProtocolBasicIDGAR, ClassReaderProtocolBasicIDGAR, and ClassReaderProtocolBasicIDTECH
' For NEO 1.0 Functions
' 
' Revision:
' -----------------------[ Initial Version ]-----------------------
' 2017 Oct 06, by goofy_liu@idtechproducts.com.tw

'==================================================================================
'
'----------------------[ Importing Stuffs ]----------------------
Imports System.Runtime.Serialization
Imports System.Runtime.Serialization.Formatters.Binary
'Imports DiagTool_HardwareExclusive.ClassReaderProtocolBasicIDGAR
Imports System.IO
Imports System.IO.Ports
Imports System.Threading

'-------------------[ Class Declaration ]-------------------

Partial Public Class ClassReaderProtocolBasic
    '================[V1 Protocol]==================
    '------------------[ For Frame Type C, Command ]---------------------
    Protected Sub PktComposerCommandV1FrameHeaderCmdSetDataLengthCRCData_FrameTypeC(ByRef refIDGCmdData As tagIDGCmdData, ByVal u16CmdSub As UInt16, ByVal u16Data As UInt16, Optional ByVal bCRCMSBFirst As Boolean = False)
        '1)Protocol Header Command Type = ViVOtech\0C
        Dim strProtocol1Header_FrameTypeC As String = "ViVOtech" + Chr(0) + Chr(ClassReaderProtocolBasic.EnumProtocolV1FrameType._C)
        Dim nIdx As Integer
        Dim strFuncName As String = "PktComposerCommandV1FrameHeaderCmdSetDataLengthCRCData_FrameTypeC"

        Try

            With refIDGCmdData
                'Save Command Set
                .m_u16CmdSet = u16CmdSub

                '2) Put Header
                For nIdx = 0 To strProtocol1Header_FrameTypeC.Count - 1
                    .m_refarraybyteData(nIdx) = Convert.ToByte(strProtocol1Header_FrameTypeC(nIdx))
                Next

                '2) Command Set, (10, 11), 2 bytes
                .m_refarraybyteData(ClassReaderProtocolBasic.EnumIDGCmdOffset.Protocol1_OffSet_CmdMain_Len_1B) = CType((u16CmdSub >> 8) And &HFF, Byte)
                .m_refarraybyteData(ClassReaderProtocolBasic.EnumIDGCmdOffset.Protocol1_OffSet_CmdSub_Len_1B) = CType((u16CmdSub) And &HFF, Byte)

                '3) Data (Length).  2 bytes 
                .m_refarraybyteData(ClassReaderProtocolBasic.EnumIDGCmdOffset.Protocol1_OffSet_Cmd_Data1_1B) = CType((u16Data >> 8) And &HFF, Byte)
                .m_refarraybyteData(ClassReaderProtocolBasic.EnumIDGCmdOffset.Protocol1_OffSet_Cmd_Data2_1B) = CType((u16Data) And &HFF, Byte)

                '4) Command Frame CRC Data
                'ClassReaderProtocolBasic.IDGCmdFrameCRCSet(.m_refarraybyteData, .m_nDataSizeSend + tagIDGCmdData.m_nPktHeader, False)
                ClassReaderProtocolBasic.IDGCmdFrameCRCSet(.m_refarraybyteData, tagIDGCmdData.m_nPktHeaderV1 + 4, bCRCMSBFirst)

                '5) Set Packet Length (Header + Raw Data + Trail)
                .m_nDataSizeSend = tagIDGCmdData.m_nPktHeaderV1 + 4 + tagIDGCmdData.m_nPktTrail
            End With
        Catch ex As Exception
            LogLn("[Err] " + strFuncName + "() = " + ex.Message)
        End Try
    End Sub
    '------------------[ For Frame Type D or S, Data or Special ]---------------------
    Protected Sub PktComposerCommandV1FrameHeaderCmdSetDataLengthCRCData_FrameTypeDS(ByRef refIDGCmdData As tagIDGCmdData, byteType As ClassReaderProtocolBasic.EnumProtocolV1FrameType, arrayByteDataSend As Byte(), Optional ByVal bCRCMSBFirst As Boolean = False)
        '1)Protocol Header Command Type = ViVOtech\0C
        Dim strProtocol1Header_FrameTypeDS As String = "ViVOtech" + Chr(0) + Chr(byteType)
        Dim nIdx As Integer
        Dim strFuncName As String = "PktComposerCommandV1FrameHeaderCmdSetDataLengthCRCData_FrameTypeDS"

        Try
            With refIDGCmdData

                '2) Put Header
                For nIdx = 0 To strProtocol1Header_FrameTypeDS.Count - 1
                    .m_refarraybyteData(nIdx) = Convert.ToByte(strProtocol1Header_FrameTypeDS(nIdx))
                Next

                '3) Copy Data Now
                Array.Copy(arrayByteDataSend, 0, .m_refarraybyteData, ClassReaderProtocolBasic.EnumIDGCmdOffset.Protocol1_OffSet_TypeDS_Data_Byte10_Len_1B, arrayByteDataSend.Count)

                '4) Command Frame CRC Data
                ClassReaderProtocolBasic.IDGCmdFrameCRCSet(.m_refarraybyteData, tagIDGCmdData.m_nPktHeaderV1 + arrayByteDataSend.Count, bCRCMSBFirst)

                '5) Set Packet Length (Header + Raw Data + Trail)
                .m_nDataSizeSend = tagIDGCmdData.m_nPktHeaderV1 + arrayByteDataSend.Count + tagIDGCmdData.m_nPktTrail
            End With
        Catch ex As Exception
            LogLn("[Err] " + strFuncName + "() = " + ex.Message)
        End Try
    End Sub

    '
    <System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2200:RethrowToPreserveStackDetails")> Public Sub SystemV1_Customized_Command(ByVal u16Cmd As UInt16, ByVal strFuncName As String, ByVal byteFrameType As ClassReaderProtocolBasic.EnumProtocolV1FrameType, ByVal arrayByteSendData As Byte(), ByRef o_arrayByteRecv As Byte(), ByVal bFrameResponseDataFollowed As Boolean, ByVal bFrameDataSendIsLength As Boolean, callbackExternalRecvProcedure As ClassDevRS232.DelegatecallbackExternalRecvProcedure, Optional ByVal nTimeout As Integer = ClassDevRS232.m_cnTX_TIMEOUT_IN_SEC, Optional bCRCMSB As Boolean = False)
        'Dim strFuncName As String = "System_Customized_Command"
        '
        Try
            m_bIsRetStatusOK = False

            m_tagIDGCmdDataInfo.Reinit(u16Cmd, m_arraybyteSendBuf, Me, bFrameResponseDataFollowed, m_arraybyteSendBufUnknown)

            '-----------------------------------------------------------------[ Send to reader with Frame Type C with Data Length ]-----------------------------------------------------------------
            'Compose 1)Protocol Header 2) Command Set 3) Length 4) Command Frame CRC Data
            Select Case byteFrameType
                '---------------------------------------
                Case EnumProtocolV1FrameType._C
                    Dim u16Data As UInt16
                    If (bFrameDataSendIsLength) Then
                        If (arrayByteSendData Is Nothing) Then
                            u16Data = &H0
                        Else
                            u16Data = arrayByteSendData.Count
                        End If

                    Else
                        If ((arrayByteSendData Is Nothing) And (Not bFrameDataSendIsLength)) Then
                            Throw New Exception("Invalid Parameter, 3rd And 6th (0-based parameter order)")
                        End If
                        u16Data = ((CType(arrayByteSendData(0), UInt16) << 8) And &HFF00) + arrayByteSendData(1)
                    End If
                    PktComposerCommandV1FrameHeaderCmdSetDataLengthCRCData_FrameTypeC(m_tagIDGCmdDataInfo, u16Cmd, u16Data, bCRCMSB)
                    '-------------------------------------------------------
                Case EnumProtocolV1FrameType._D, EnumProtocolV1FrameType._S
                    PktComposerCommandV1FrameHeaderCmdSetDataLengthCRCData_FrameTypeDS(m_tagIDGCmdDataInfo, byteFrameType, arrayByteSendData, bCRCMSB)
                Case Else
                    Return
            End Select


            '----------------[ Debug Section ]----------------
            If (m_bDebug) Then
                'Note: After PktComposerCommandFrameHeaderCmdSetDataLengthCRCData(), m_tagIDGCmdDataInfo.m_nDataSizeSend includes m_nPktHeader + m_nPktTrail
                'm_refwinformMain.LogLnDumpData(m_tagIDGCmdDataInfo.m_refarraybyteData, tagIDGCmdData.m_nPktHeader + m_tagIDGCmdDataInfo.m_nDataSizeSend + tagIDGCmdData.m_nPktTrail)
                Form01_Main.LogLnDumpData(m_tagIDGCmdDataInfo.m_refarraybyteData.Skip(m_tagIDGCmdDataInfo.m_nDataRecvOffSkipBytes).ToArray(), m_tagIDGCmdDataInfo.m_nDataSizeRecv)
            End If

            '----------------[ End Debug Section ]----------------
            'Waiting For Response in SetConfigurationsGroupSingleCbRecv()
            If (callbackExternalRecvProcedure Is Nothing) Then
                callbackExternalRecvProcedure = AddressOf ClassReaderProtocolBasic.PreProcessCbRecvV1
            End If

            m_refdevConn.SendCommandFrameAndWaitForResponseFrame(callbackExternalRecvProcedure, m_tagIDGCmdDataInfo, m_nIDGCommanderType, True, nTimeout * 1000)

            'Its' response code = 0 => OK
            If (m_tagIDGCmdDataInfo.bIsResponseOK) Then
                m_bIsRetStatusOK = True
            Else
                m_bIsRetStatusOK = False
            End If

            'If (o_arrayByteRecv IsNot Nothing) Then
            '    If (m_tagIDGCmdDataInfo.bIsRawData) Then
            '        m_tagIDGCmdDataInfo.ParserDataGetArrayRawData(o_arrayByteRecv)
            '    Else
            '        'Set First byte as 0 
            '        o_arrayByteRecv(0) = &H0
            '    End If
            'Else
            '    m_tagIDGCmdDataInfo.ParserDataGetArrayRawData(o_arrayByteRecv)
            'End If
            m_tagIDGCmdDataInfo.ParserDataGetArrayRawData(o_arrayByteRecv)

        Catch ext As TimeoutException
            LogLn("[Err][Timeout] [IDG: " + GetIDGCmdString(u16Cmd) + "] " + strFuncName + "() = " + ext.Message)
        Catch ex As Exception
            LogLn("[Err] [IDG:  " + GetIDGCmdString(u16Cmd) + "] " + strFuncName + "() = " + ex.Message)
            Throw 'ex
        Finally

        End Try
    End Sub
    '
    '================================[ RTC Control functions ]================================
    Public m_sdictRTCErrMsg As SortedDictionary(Of Byte, String) = New SortedDictionary(Of Byte, String) From {
        {&H0, "No Error"}, {&H1, "Unknown Error"}, {&H2, "Invalid Data"}, {&H3, "RTC not found or not responding"}
        }
    'Note
    'SystemV1 = Using V1 Protocol. Frame format like
    ' Command Type : "ViVOtech\0"+"C"+CmdByte1+SubByte1+DataN....+CRC_LSB+CRC_MSB
    ' Response ACK/OK Type : "ViVOtech\0"+"A"+CmdByte1+SubByte1+DataN....+CRC_LSB+CRC_MSB
    ' Response NAK/Error Type : "ViVOtech\0"+"N"+CmdByte1+SubByte1+DataN....+CRC_LSB+CRC_MSB
    Public Overridable Sub SystemV1SetRTCTime(nHH As Integer, nMM As Integer, Optional ByVal nTimeout As Integer = ClassDevRS232.m_cnTX_TIMEOUT_IN_SEC)
        Dim u16Cmd As UInt16 = &H2501
        Dim strFuncName As String = "SystemV1SetRTCTime"
        Dim byteHH, byteMM As Byte
        Dim bIsSendDataLength As Boolean
        Dim bIsFrameResponseDataFollowed As Boolean

        'Set Serial Number Length = 15 bytes
        Dim arrayByteSend As Byte() = Nothing
        Dim arrayByteRecv As Byte() = Nothing


        If (m_bIDGCmdBusy) Then
            Return
        End If
        m_bIDGCmdBusy = True

        Try
            'Convert to HH MM format to 2-digit BCD Format
            byteHH = (((nHH \ 10) << 4) And &HF0) + (nHH Mod 10)
            byteMM = (((nMM \ 10) << 4) And &HF0) + (nMM Mod 10)
            arrayByteSend = New Byte(1) {byteHH, byteMM}
            '-----------------------------------------------------------------[ Send V1 Frame Type C + Data Length to reader ]-----------------------------------------------------------------
            bIsFrameResponseDataFollowed = False
            bIsSendDataLength = False
            SystemV1_Customized_Command(u16Cmd, strFuncName, EnumProtocolV1FrameType._C, arrayByteSend, arrayByteRecv, bIsFrameResponseDataFollowed, bIsSendDataLength, AddressOf ClassReaderProtocolBasic.PreProcessCbRecvV1, nTimeout)

            If (bIsRetStatusOK) Then
            Else
            End If

        Catch ext As TimeoutException
            LogLn("[Err][Timeout] [IDG: " + GetIDGCmdString(u16Cmd) + "] " + strFuncName + "() = " + ext.Message)
        Catch ex As Exception
            LogLn("[Err] [IDG:  " + GetIDGCmdString(u16Cmd) + "] " + strFuncName + "() = " + ex.Message)
        Finally
            If (arrayByteSend IsNot Nothing) Then
                Erase arrayByteSend
            End If
            If (arrayByteRecv IsNot Nothing) Then
                Erase arrayByteRecv
            End If
            m_tagIDGCmdDataInfo.Cleanup()
            m_bIDGCmdBusy = False
        End Try
    End Sub
    '
    Public Overridable Sub SystemV1GetRTCTime(ByRef o_nHH As Integer, ByRef o_nMM As Integer, Optional ByVal nTimeout As Integer = ClassDevRS232.m_cnTX_TIMEOUT_IN_SEC)
        Dim u16Cmd As UInt16 = &H2502
        Dim strFuncName As String = "SystemV1GetRTCTime"
        Dim bIsSendDataLength As Boolean
        Dim bIsFrameResponseDataFollowed As Boolean

        'Set Serial Number Length = 15 bytes
        Dim arrayByteSend As Byte() = Nothing
        Dim arrayByteRecv As Byte() = Nothing


        If (m_bIDGCmdBusy) Then
            Return
        End If
        m_bIDGCmdBusy = True

        Try
            '-----------------------------------------------------------------[ Send V1 Frame Type C + Data Length to reader ]-----------------------------------------------------------------
            bIsFrameResponseDataFollowed = False
            bIsSendDataLength = True
            SystemV1_Customized_Command(u16Cmd, strFuncName, EnumProtocolV1FrameType._C, arrayByteSend, arrayByteRecv, bIsFrameResponseDataFollowed, bIsSendDataLength, AddressOf ClassReaderProtocolBasic.PreProcessCbRecvV1, nTimeout)

            If (bIsRetStatusOK) Then
            Else
            End If

            If (m_bIsRetStatusOK And (arrayByteRecv IsNot Nothing)) Then
                Dim byteTmp As Byte
                byteTmp = arrayByteRecv(0)
                o_nHH = ((byteTmp >> 4) And &HF) * 10 + (byteTmp And &HF) '2-digit BCD
                byteTmp = arrayByteRecv(1)
                o_nMM = ((byteTmp >> 4) And &HF) * 10 + (byteTmp And &HF) '2-digit BCD
            Else
                o_nHH = 0
                o_nMM = 0
            End If

        Catch ext As TimeoutException
            LogLn("[Err][Timeout] [IDG: " + GetIDGCmdString(u16Cmd) + "] " + strFuncName + "() = " + ext.Message)
        Catch ex As Exception
            LogLn("[Err] [IDG:  " + GetIDGCmdString(u16Cmd) + "] " + strFuncName + "() = " + ex.Message)
        Finally
            If (arrayByteSend IsNot Nothing) Then
                Erase arrayByteSend
            End If
            If (arrayByteRecv IsNot Nothing) Then
                Erase arrayByteRecv
            End If
            m_tagIDGCmdDataInfo.Cleanup()
            m_bIDGCmdBusy = False
        End Try
    End Sub
    '
    Public Overridable Sub SystemV1SetRTCDate(nYear As Integer, nMonth As Integer, nDate As Integer, Optional ByVal nTimeout As Integer = ClassDevRS232.m_cnTX_TIMEOUT_IN_SEC)
        Dim u16Cmd As UInt16 = &H2503
        Dim strFuncName As String = "SystemV1SetRTCDate"
        Dim byteYearH, byteYearL, byteMonth, byteDate As Byte
        Dim bIsSendDataLength As Boolean
        Dim bIsFrameResponseDataFollowed As Boolean
        'Set Serial Number Length = 15 bytes
        Dim arrayByteSend As Byte() = Nothing
        Dim arrayByteRecv As Byte() = Nothing


        If (m_bIDGCmdBusy) Then
            Return
        End If
        m_bIDGCmdBusy = True

        Try
            'Convert Year(4-digit),Month(2-digit), and Date(2-digit) to BCD Format
            byteYearH = (((nYear \ 1000) << 4) And &HF0) + (((nYear \ 100) Mod 10) And &HF)
            byteYearL = ((((nYear Mod 100) \ 10) << 4) And &HF0) + ((nYear Mod 10) And &HF)
            byteMonth = (((nMonth \ 10) << 4) And &HF0) + (nMonth Mod 10)
            byteDate = (((nDate \ 10) << 4) And &HF0) + (nDate Mod 10)
            arrayByteSend = New Byte(3) {byteYearH, byteYearL, byteMonth, byteDate}
            '-----------------------------------------------------------------[ Send V1 Frame Type C + Data Length to reader ]-----------------------------------------------------------------
            bIsFrameResponseDataFollowed = False
            bIsSendDataLength = True
            SystemV1_Customized_Command(u16Cmd, strFuncName, EnumProtocolV1FrameType._C, arrayByteSend, arrayByteRecv, bIsFrameResponseDataFollowed, bIsSendDataLength, AddressOf ClassReaderProtocolBasic.PreProcessCbRecvV1, nTimeout)
            If (bIsRetStatusOK) Then
            Else
                Throw New Exception("Set Frame Type C + Data Length Failed")
            End If

            '-----------------------------------------------------------------[ if Okay, Send V1 Frame Type D + Data to reader ]-----------------------------------------------------------------'
            bIsSendDataLength = False
            SystemV1_Customized_Command(u16Cmd, strFuncName, EnumProtocolV1FrameType._D, arrayByteSend, arrayByteRecv, bIsFrameResponseDataFollowed, bIsSendDataLength, AddressOf ClassReaderProtocolBasic.PreProcessCbRecvV1, nTimeout)
            If (bIsRetStatusOK) Then
            Else
            End If

        Catch ext As TimeoutException
            LogLn("[Err][Timeout] [IDG: " + GetIDGCmdString(u16Cmd) + "] " + strFuncName + "() = " + ext.Message)
        Catch ex As Exception
            LogLn("[Err] [IDG:  " + GetIDGCmdString(u16Cmd) + "] " + strFuncName + "() = " + ex.Message)
        Finally
            If (arrayByteSend IsNot Nothing) Then
                Erase arrayByteSend
            End If
            If (arrayByteRecv IsNot Nothing) Then
                Erase arrayByteRecv
            End If
            m_tagIDGCmdDataInfo.Cleanup()
            m_bIDGCmdBusy = False
        End Try
    End Sub
    '
    Public Overridable Sub SystemV1GetRTCDate(ByRef o_nYear As Integer, ByRef o_nMonth As Integer, ByRef o_nDate As Integer, Optional ByVal nTimeout As Integer = ClassDevRS232.m_cnTX_TIMEOUT_IN_SEC)
        Dim u16Cmd As UInt16 = &H2504
        Dim strFuncName As String = "SystemV1GetRTCDate"
        Dim bIsSendDataLength As Boolean
        Dim bIsFrameResponseDataFollowed As Boolean

        'Set Serial Number Length = 15 bytes
        Dim arrayByteSend As Byte() = Nothing
        Dim arrayByteRecv As Byte() = Nothing


        m_bIsRetStatusOK = False
        If (m_bIDGCmdBusy) Then
            Return
        End If
        m_bIDGCmdBusy = True

        Try
            '-----------------------------------------------------------------[ Send V1 Frame Type C + Data Length to reader ]-----------------------------------------------------------------
            bIsFrameResponseDataFollowed = True
            bIsSendDataLength = True
            SystemV1_Customized_Command(u16Cmd, strFuncName, EnumProtocolV1FrameType._C, arrayByteSend, arrayByteRecv, bIsFrameResponseDataFollowed, bIsSendDataLength, AddressOf ClassReaderProtocolBasic.PreProcessCbRecvV1, nTimeout)
            '
            If (m_tagIDGCmdDataInfo.bIsV1ResponseOK) Then
                m_bIsRetStatusOK = True
            Else
                m_bIsRetStatusOK = False
            End If


            If ((m_bIsRetStatusOK) And (arrayByteRecv IsNot Nothing)) Then
                Dim byteTmp As Byte
                'Year
                byteTmp = arrayByteRecv(0)
                o_nYear = CType(((byteTmp >> 4) And &HF), Integer) * 10 + (byteTmp And &HF) '2-digit BCD
                byteTmp = arrayByteRecv(1)
                o_nYear = o_nYear * 100 + CType(((byteTmp >> 4) And &HF), Integer) * 10 + (byteTmp And &HF)
                'Month
                byteTmp = arrayByteRecv(2)
                o_nMonth = ((byteTmp >> 4) And &HF) * 10 + (byteTmp And &HF) '2-digit BCD, 1~12
                'Date
                byteTmp = arrayByteRecv(3)
                o_nDate = ((byteTmp >> 4) And &HF) * 10 + (byteTmp And &HF) '2-digit BCD, 1~31
            Else
                o_nYear = 0
                o_nMonth = 0
                o_nDate = 0
            End If
        Catch ext As TimeoutException
            LogLn("[Err][Timeout] [IDG: " + GetIDGCmdString(u16Cmd) + "] " + strFuncName + "() = " + ext.Message)
        Catch ex As Exception
            LogLn("[Err] [IDG:  " + GetIDGCmdString(u16Cmd) + "] " + strFuncName + "() = " + ex.Message)
        Finally
            If (arrayByteSend IsNot Nothing) Then
                Erase arrayByteSend
            End If
            If (arrayByteRecv IsNot Nothing) Then
                Erase arrayByteRecv
            End If
            m_tagIDGCmdDataInfo.Cleanup()
            m_bIDGCmdBusy = False
        End Try
    End Sub

End Class
