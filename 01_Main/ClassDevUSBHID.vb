﻿'==================================================================================
' File Name: ClassDevRS232.vb
' Description: The ClassDevRS232 is as Non-inherit Class of RS232 object
' Who use it as component: ClassReaderCommander
' Similar Class: ClassDevUsbHID
'
' Revision:
' -----------------------[ Initial Version ]-----------------------
' 2014 May 29, by goofy_liu@idtechproducts.com.tw
'==================================================================================
Imports System
Imports System.IO.Ports
Imports System.Threading
Imports System.Runtime.InteropServices
Imports Main.SafeNativeMethods



Public Class ClassDevUSBHID
    Inherits ClassDev

    Public Const m_cstrClassName As String = "ClassDevUSBHID"
    'Public Delegate Function AppDoEvent() As UInt32


    'Private Declare Sub getDLLVersion Lib "USB_HID_IDTW" (strVersion As String)
    'Private Declare Sub testfunc Lib "USB_HID_IDTW" ()

    '[Return Value] > 0 --> Rx DatSize (IDG Frame Size), = 0 -->  I/O Error / Timeout
    'Private Declare Function USBHIDSendAndResponse_u32 Lib "USB_HID_IDTW" ( _
    '         u32VID As UInt32, u32PID As UInt32, _
    '        arrayByteSendData As Byte(), u32SendSize As UInt32, _
    '        pArrayByteRecvData As Byte(), u32RecvDataBufSize As UInt32, _
    '        u32TimeoutMS As UInt32) _
    '        As UInt32

    'Private Declare Function USBHIDSendAndResponse_u32 Lib "USB_HID_IDTW" ( _
    '      u32VID As UInt32, u32PID As UInt32, _
    '     arrayByteSendData As Byte(), u32SendSize As UInt32, _
    '     pArrayByteRecvData As Byte(), u32RecvDataBufSize As UInt32, _
    '     u32TimeoutMS As UInt32, _
    '     pfuncCB As SafeNativeMethods.AppDoEvent) _
    '     As UInt32

    '' Return = 0 : Failed
    ''              = 1 : Succeed
    'Private Declare Function USBHIDRecvData_u32 Lib "USB_HID_IDTW" ( _
    '      u32VID As UInt32, u32PID As UInt32, _
    '     pArrayByteRecvData As Byte(), u32RecvDataBufSize As UInt32, _
    '     u32TimeoutMS As UInt32, _
    '     pfuncCB As SafeNativeMethods.AppDoEvent) _
    'As UInt32

    '' Return = 0 : Failed
    ''              > 01 : Succeed Read Byte Size
    'Private Declare Function USBHIDRecvDataIDGCmd_u32 Lib "USB_HID_IDTW" ( _
    '      u32VID As UInt32, u32PID As UInt32, _
    '     pArrayByteRecvData As Byte(), u32RecvDataBufSize As UInt32, _
    '     u32TimeoutMS As UInt32, _
    '     pfuncCB As SafeNativeMethods.AppDoEvent) _
    '     As UInt32


    ''[Return ] = 0 --> Not Found; = 1 --> Found
    'Private Declare Function USBHIDOpen_u8 Lib "USB_HID_IDTW" (u32VID As UInt32, u32PID As UInt32) As Byte
    'Private Declare Sub USBHIDClose Lib "USB_HID_IDTW" ()

    '=====================[ End of DLL Import Function ]====================

    'Private Declare Function USBHID_Version Lib "IDTECH_USB" (ByRef btVersion As Byte) As UInt32
    'Private Declare Function USB_SendCommandAndResponse Lib "IDTECH_USB" Alias "USB_SendCommand" (ByVal auiVid As UInt32, _
    '                                                            ByVal auiPid As UInt32, _
    '                                                            ByRef aucmpSource As Byte, _
    '                                                            ByVal auiSourceLen As UInt32, _
    '                                                            ByRef aucmpResp As Byte, _
    '                                                            ByVal auiWaitMs As UInt32) As UInt32

    'Private Declare Sub USB_Close Lib "IDTECH_USB" ()
    Private m_bIsOpen As Boolean

    'Vendi
    'Private m_u32VID As UInt32 = &HACD
    'Private m_u32PID As UInt32 = &H1599
    'KIOSK II
    'Private m_u32VID As UInt32 = &H1D5F
    'Private m_u32PID As UInt32 = &H100

    Private m_u32VID As UInt32 = &H0
    Private m_u32PID As UInt32 = &H0
    Private m_u32RecvLen As UInt32

    Private m_enNameID As ClassReaderInfo.ENUM_PD_IMG_IDX
    Private m_strProductName As String = ""

    Public Structure USB_HID_DEV_INFO
        Dim m_u32VID As UInt32
        Dim m_u32PID As UInt32
        Dim m_strProductName As String
        Dim m_enNameID As ClassReaderInfo.ENUM_PD_IMG_IDX

        Sub New(u32VID As UInt32, u32PID As UInt32, enNameID As ClassReaderInfo.ENUM_PD_IMG_IDX, strProductName As String)
            m_u32VID = u32VID
            m_u32PID = u32PID
            m_enNameID = enNameID
            m_strProductName = strProductName
        End Sub

        Sub Cleanup()

        End Sub
    End Structure

    ' =====[ Revision / History ]========
    '"Vendi 3-in-1" --> "Unipay III"
    'Note 01 - Unipay III must do 09-13 cmd checks same serials reader type(VP4880,VP4880E,VP4880C).
    '        VP4880 .Unipay III are same VID/PID.
    Public Shared m_listUSBKB_Wedge_DevInfo As List(Of USB_HID_DEV_INFO) = New List(Of USB_HID_DEV_INFO) From {
        New USB_HID_DEV_INFO(&HACD, &H3131, ClassReaderInfo.ENUM_PD_IMG_IDX.NEO_UNIPAY_1D5, "Unipay 1.5, KB Wedge"),
                New USB_HID_DEV_INFO(&HACD, &H3511, ClassReaderInfo.ENUM_PD_IMG_IDX.NEO_VENDI_NEW, "Vendi New, KB Wedge"),
        New USB_HID_DEV_INFO(&HACD, &H3521, ClassReaderInfo.ENUM_PD_IMG_IDX.NEO_VP3300_AUDIO_JACK, "VP3300 Audio Jack (Unipay III), KB Wedge"),
        New USB_HID_DEV_INFO(&HACD, &H3531, ClassReaderInfo.ENUM_PD_IMG_IDX.NEO_VP3300USB, "VP3300 USB (VP4880), KB Wedge"),
        New USB_HID_DEV_INFO(&HACD, &H3541, ClassReaderInfo.ENUM_PD_IMG_IDX.NEO_VP3300Bluetooth, "VP3300 Bluetooth (BTPay Mini), KB Wedge"),
        New USB_HID_DEV_INFO(&HACD, &H3141, ClassReaderInfo.ENUM_PD_IMG_IDX.NEO_UNIPAY_1D5_TTK, "Unipay 1.5 TTK, KB Wedge")
        }
    Public Shared m_listUSBHIDDevInfo As List(Of USB_HID_DEV_INFO) = New List(Of USB_HID_DEV_INFO) From {
        New USB_HID_DEV_INFO(&HACD, &H1599, ClassReaderInfo.ENUM_PD_IMG_IDX.NEO_VENDI_V0, "Vendi Old"),
        New USB_HID_DEV_INFO(&HACD, &H3510, ClassReaderInfo.ENUM_PD_IMG_IDX.NEO_VENDI_NEW, "Vendi New"),
        New USB_HID_DEV_INFO(&HACD, &H3520, ClassReaderInfo.ENUM_PD_IMG_IDX.NEO_VP3300_AUDIO_JACK, "VP3300 Audio Jack (Unipay III)"),
        New USB_HID_DEV_INFO(&HACD, &H3530, ClassReaderInfo.ENUM_PD_IMG_IDX.NEO_VP3300USB, "VP3300 USB (VP4880)"),
        New USB_HID_DEV_INFO(&HACD, &H3540, ClassReaderInfo.ENUM_PD_IMG_IDX.NEO_VP3300Bluetooth, "VP3300 Bluetooth (BTPay Mini)"),
        New USB_HID_DEV_INFO(&HACD, &H3130, ClassReaderInfo.ENUM_PD_IMG_IDX.NEO_UNIPAY_1D5, "Unipay 1.5"),
        New USB_HID_DEV_INFO(&HACD, &H3140, ClassReaderInfo.ENUM_PD_IMG_IDX.NEO_UNIPAY_1D5_TTK, "Unipay 1.5 TTK"),
        New USB_HID_DEV_INFO(&HACD, &H4210, ClassReaderInfo.ENUM_PD_IMG_IDX.AR_PISCES, "PISCES"),
                New USB_HID_DEV_INFO(&H525, &HA4AC, ClassReaderInfo.ENUM_PD_IMG_IDX.AR_PISCES_V0, "PISCES "),
        New USB_HID_DEV_INFO(&H525, &HA4AC,
ClassReaderInfo.ENUM_PD_IMG_IDX.GR_EC8_KIOSKII, "Kiosk II"),
        New USB_HID_DEV_INFO(&HACD, &H3710, ClassReaderInfo.ENUM_PD_IMG_IDX.NEO_K_III, "Kiosk III"),
        New USB_HID_DEV_INFO(&HACD, &H4450, ClassReaderInfo.ENUM_PD_IMG_IDX.NEO_20_VP5300_SPTP_II, "VP5300,Spectrum Pro II"),
        New USB_HID_DEV_INFO(&HACD, &H4440, ClassReaderInfo.ENUM_PD_IMG_IDX.NEO_20_VP6300_VENDI_PRO, "VP6300,Vendi-Pro_SRED"),
        New USB_HID_DEV_INFO(&HACD, &H4441, ClassReaderInfo.ENUM_PD_IMG_IDX.NEO_20_VP6300_VENDI_PRO, "VP6300,Vendi-Pro_NonSRED")
        }
    Private m_dictUSBHIDDevInfo As SortedDictionary(Of ClassReaderInfo.ENUM_PD_IMG_IDX, USB_HID_DEV_INFO) = New SortedDictionary(Of ClassReaderInfo.ENUM_PD_IMG_IDX, USB_HID_DEV_INFO)
    Public ReadOnly Property strpropGetPID(nProdId As ClassReaderInfo.ENUM_PD_IMG_IDX) As String
        Get
            If ((m_dictUSBHIDDevInfo.Count < 1) Or (Not m_dictUSBHIDDevInfo.ContainsKey(nProdId))) Then
                Return ""
            End If
            Return m_dictUSBHIDDevInfo(nProdId).m_u32PID.ToString("X")
        End Get
    End Property
    Public ReadOnly Property strpropGetVID(nProdId As ClassReaderInfo.ENUM_PD_IMG_IDX) As String
        Get
            If ((m_dictUSBHIDDevInfo.Count < 1) Or (Not m_dictUSBHIDDevInfo.ContainsKey(nProdId))) Then
                Return ""
            End If
            Return m_dictUSBHIDDevInfo(nProdId).m_u32VID.ToString("X")
        End Get
    End Property
    '
    Public ReadOnly Property strpropGetPIDCurrent() As String
        Get
            Return m_u32PID.ToString("X")
        End Get
    End Property
    Public ReadOnly Property strpropGetVIDCurrent() As String
        Get
            Return m_u32VID.ToString("X")
        End Get
    End Property
    '
    Public Property propGetNameID As ClassReaderInfo.ENUM_PD_IMG_IDX
        Set(value As ClassReaderInfo.ENUM_PD_IMG_IDX)
            m_enNameID = value
        End Set
        Get
            Return m_enNameID
        End Get
    End Property
    '
    Public ReadOnly Property propGetNameProduct As String
        Get
            Return m_strProductName
        End Get
    End Property
    'For PISCES Case, we need Rx Buffer up to 16K~20K for Signature Rx PNG Max Data Accommodation
    'Use 20K as Safety Buffer Size.
    Private Const m_cu32BufLen As UInt32 = 20480 '=20K;4096
    Private m_arrayByteRecv As Byte()

    ReadOnly Property bIsKBWedge As Boolean
        Get
            Return m_bUsbDevMode_IsKBWedge
        End Get
    End Property

    Public Sub SetVidPid(u32VID As UInt32, u32PID As UInt32)
        Dim bFound As Boolean = False

        m_u32VID = u32VID
        m_u32PID = u32PID
        '
        For Each iterator In m_listUSBHIDDevInfo
            If ((iterator.m_u32PID = u32PID) And (iterator.m_u32PID = u32PID)) Then
                bFound = True
            End If
        Next
        If (Not bFound) Then
            m_listUSBHIDDevInfo.Add(New USB_HID_DEV_INFO(u32VID, u32PID, ClassReaderInfo.ENUM_PD_IMG_IDX.NEO_VENDI_V0, "GR Unknown Type"))
        End If
    End Sub

    ReadOnly Property u32VID As UInt32
        Get
            Return m_u32VID
        End Get
    End Property
    ReadOnly Property u32PID As UInt32
        Get
            Return m_u32PID
        End Get
    End Property
    '
    '==================[Constructor / Destructor Area ]=====================
    Sub New(refwinformMain As Form01_Main)
        MyBase.New(refwinformMain)
        'Create Logging Stuffs
        LogLn = New Form01_Main.LogLnDelegate(AddressOf m_pMF.LogLn)
        LogLnThreadSafe = New Form01_Main.LogLnDelegate(AddressOf m_pMF.LogThreadSafe)

        '------------------[ Device RS232 Stuffs ]------------------
        m_bIsOpen = False
        m_bLogTurnOnOff = False
        m_arrayByteRecv = New Byte(m_cu32BufLen - 1) {}

        For Each iterator In m_listUSBHIDDevInfo
            If (Not m_dictUSBHIDDevInfo.ContainsKey(iterator.m_enNameID)) Then
                m_dictUSBHIDDevInfo.Add(iterator.m_enNameID, iterator)
            End If
        Next

    End Sub
    '
    Public Overrides Sub Cleanup()
        MyBase.Cleanup()
        Close()

        For Each iterator In m_listUSBHIDDevInfo
            iterator.Cleanup()
        Next
        m_listUSBHIDDevInfo.Clear()
        m_dictUSBHIDDevInfo.Clear()

        If (m_arrayByteRecv IsNot Nothing) Then
            Erase m_arrayByteRecv
            m_arrayByteRecv = Nothing
        End If
    End Sub

    Public Overrides Sub Close()

        SafeNativeMethods.USBHIDClose()

        m_bIsOpen = False
        m_u32VID = &H0
        m_u32PID = &H0
        m_enNameID = ClassReaderInfo.ENUM_PD_IMG_IDX.UNKNOWN_MAX
        m_strProductName = ""
        '
        m_bUsbDevMode_IsKBWedge = False
    End Sub

    ReadOnly Property bIsOpen As Boolean
        Get
            Return m_bIsOpen
        End Get
    End Property

    Private m_bUsbDevMode_IsKBWedge As Boolean = False
    Public Overrides Sub Open()
        Dim u8Val As Byte

        If (bIsOpen) Then
            Close()
        End If

        m_enNameID = ClassReaderInfo.ENUM_PD_IMG_IDX.UNKNOWN_MAX

        For Each iterator In m_listUSBHIDDevInfo
            u8Val = SafeNativeMethods.USBHIDOpen_u8(iterator.m_u32VID, iterator.m_u32PID)
            If (u8Val > 0) Then
                m_bIsOpen = True
                m_u32VID = iterator.m_u32VID
                m_u32PID = iterator.m_u32PID
                m_enNameID = iterator.m_enNameID
                m_strProductName = iterator.m_strProductName
                '
                Exit For
            End If
        Next

        For Each iterator In m_listUSBKB_Wedge_DevInfo
            u8Val = SafeNativeMethods.USBKB_Wedge_Open_u8(iterator.m_u32VID, iterator.m_u32PID)
            If (u8Val > 0) Then
                m_bIsOpen = True
                m_u32VID = iterator.m_u32VID
                m_u32PID = iterator.m_u32PID
                m_enNameID = iterator.m_enNameID
                m_strProductName = iterator.m_strProductName
                '
                m_bUsbDevMode_IsKBWedge = True
                Exit For
            End If
        Next

    End Sub
    '
    ' ================[ Special Open Functions ]==============
    ' 2016 Sept 23, goofy_liu@idtechproducts.com.tw
    ' For USB Reader, which has Sleep Mode while USB Cable Plugged 
    ' but TS doesn't OPEN Device Path yet.
    ' Supported Reader Type : VP4880 (UniPay III Serials)
    ' After Open USB Device Path, TS waits 500 ms to wait Reader wakes up !!
    Public Sub OpenAndWait(Optional ByVal dwTimeMSWakeup As UInt32 = 500)

        If (Not bIsOpen()) Then
            Open()

            Thread.Sleep(dwTimeMSWakeup)
        End If

    End Sub

    'Declare Auto Function MBox Lib "user32.dll" Alias "MessageBox" (
    'ByVal hWnd As Integer,
    'ByVal txt As String,
    'ByVal caption As String,
    'ByVal Typ As Integer) As Integer

    'Const MB_ICONQUESTION As Integer = &H20
    'Const MB_YESNO As Integer = &H4
    'Const IDYES As Integer = 6
    'Const IDNO As Integer = 7

    Private Shared m_u32DllCallbackRet As UInt32

    Private Shared pfuncAppDoEvent As SafeNativeMethods.AppDoEvent = New SafeNativeMethods.AppDoEvent(AddressOf ClassDevUSBHID.AppDoUpdate)
    ' To Continue --> Return 0
    ' To Interrupt External Loop  --> Return 1 or greater...
    Private Shared Function AppDoUpdate() As UInt32
        Try
            Application.DoEvents()

        Catch ex As Exception

        End Try

        'Control External Send Function Loop to interruption
        Return m_u32DllCallbackRet
    End Function
    Public Sub InterruptExternalLoop(bDoInt As Boolean)
        If (bDoInt) Then
            m_u32DllCallbackRet = 1
        Else
            m_u32DllCallbackRet = 0
        End If
    End Sub
    Private Enum USB_HID_DLL_IMPORT_FUNC_TXRX_RESPCODE As UInt32
        RESP_xFFFFFFFF_OPENFAILED = &HFFFFFFFFUI
        RESP_xFFFFFFFE_ARGUMENT_OUTBUF_NULL = &HFFFFFFFEUI
        RESP_xFFFFFFFD_ARGUMENT_INBUF_NULL = &HFFFFFFFDUI
        RESP_xFFFFFFFC_ARGUMENT_OUTBUF_DATA_LEN_ZERO = &HFFFFFFFCUI
        RESP_xFFFFFFFB_ARGUMENT_INBUF_SIZE_ZERO = &HFFFFFFFBUI
    End Enum

    Private Function SendCommandAndResponseCommon(btInBuff() As Byte, iLen As UInt32, btRcvBuff() As Byte, u32RecvBufSize As UInt32, u32TimeoutMS As UInt32) As UInt32
        Dim u32IDGFrameSize As UInt32

        ' Stores the return value. 
        'Dim RetVal As Integer
        'RetVal = MBox(0, "Declare DLL Test", "Windows API MessageBox",
        '    MB_ICONQUESTION Or MB_YESNO)
        'Dim byteVal(1024) As Byte
        'Dim strVersion As String = New String(Chr(&H20), 64)
        'For nIdx = 0 To 100
        '    getDLLVersion(strVersion)
        'Next

        '--------------------[ Prevent from previous failed DLL Loading without Closed USB Handle ]-------------------
        'u32IDGFrameSize = getDLLVersion(strVersion)
        'u32IDGFrameSize = USBHID_Version(byteVal(0))


        'u32IDGFrameSize = USBHIDSendAndResponse_u32( _
        '     m_u32VID, m_u32PID, _
        '    btInBuff, iLen, _
        '    btRcvBuff, u32RecvBufSize, _
        '    u32TimeoutMS, _
        '    ClassDevUSBHID.pfuncAppDoEvent)
        If (btInBuff.Length = 0) Then
            Return 0
        End If
        u32IDGFrameSize = SafeNativeMethods.USBHIDSendAndResponse_u32( _
             m_u32VID, m_u32PID, _
            btInBuff, iLen, _
            btRcvBuff, u32RecvBufSize, _
            u32TimeoutMS, _
            ClassDevUSBHID.pfuncAppDoEvent)


        Select Case (u32IDGFrameSize)
            Case USB_HID_DLL_IMPORT_FUNC_TXRX_RESPCODE.RESP_xFFFFFFFF_OPENFAILED, USB_HID_DLL_IMPORT_FUNC_TXRX_RESPCODE.RESP_xFFFFFFFE_ARGUMENT_OUTBUF_NULL, USB_HID_DLL_IMPORT_FUNC_TXRX_RESPCODE.RESP_xFFFFFFFD_ARGUMENT_INBUF_NULL, USB_HID_DLL_IMPORT_FUNC_TXRX_RESPCODE.RESP_xFFFFFFFC_ARGUMENT_OUTBUF_DATA_LEN_ZERO, USB_HID_DLL_IMPORT_FUNC_TXRX_RESPCODE.RESP_xFFFFFFFB_ARGUMENT_INBUF_SIZE_ZERO
                SendCommandAndResponseCommon = 0
            Case Else
                SendCommandAndResponseCommon = u32IDGFrameSize

                m_bIsOpen = True
        End Select
    End Function


    Public Overrides Sub SendCommandAndResponse(ByRef arraybyte As Byte(), ByVal u32DataSize As UInt32, ByVal u32TimeoutMS As UInt32)
        Dim i As Integer = 99
        Dim nRetry = 0, nRetryMax As Integer = 2
        Dim lLen As Long
        'Dim pForm02 As Form02_StartupSelection = Form02_StartupSelection.GetInstance()
        Dim u32TOOffset As UInteger = 2000 ' More 2 secs
        Try
            'Clear Interrupt Flag to false status (i.e. = 0)
            m_u32DllCallbackRet = 0
            lLen = 0 'For 1st while loop round check
            '--------------------[ Stress Test Check ]-------------------
            ' 2016 April 12, goofy_liu@idtechproducts.com.tw
            'If (pForm02.bIsStressTest) Then
            If (m_bIsStressTest) Then
                With m_callbackParametersTagIDGCmdData
                    If (ClassDevRS232.SendCommandAndResponseStressTest(arraybyte, u32DataSize, .m_refarraybyteData, .m_nDataSizeRecv)) Then
                        'Return
                        '-------------[ Fill Up some Rx Information ]--------------
                        ClassReaderProtocolBasic.PreProcessCbRecv_NormalMode_StressTest(m_callbackParametersTagIDGCmdData)
                        lLen = m_callbackParametersTagIDGCmdData.m_nDataSizeRecv
                    Else
                        lLen = SendCommandAndResponseCommon(arraybyte, u32DataSize, m_arrayByteRecv, m_cu32BufLen, u32TimeoutMS + u32TOOffset)
                        RecvUSBHIDExtractData2IDGCmdData(m_arrayByteRecv, lLen)
                    End If
                End With
            Else
                lLen = SendCommandAndResponseCommon(arraybyte, u32DataSize, m_arrayByteRecv, m_cu32BufLen, u32TimeoutMS + u32TOOffset)
                RecvUSBHIDExtractData2IDGCmdData(m_arrayByteRecv, lLen)
            End If

            'In case of Transaction(MSR Test) --> Cancel Txn --> Tx Cmd <> Rx Cmd Situation
            'In case of  Key Pad Button Test --> Cancel Test --> Tx Cmd <> Rx Cmd Situation
            '========[ Important Note - Watch Points for Tx/Rx Mismatched Cases ]======
            '2016-02-19, goofy_liu@idtechproducts.com.tw
            If ((lLen > 0) And (Not m_callbackParametersTagIDGCmdData.bIsTxRxCmdEqualed)) Then
                'Mismatched Cases sometimes return value
                i = 0 'Put Break Point here for Tx/Rx Main Cmd Code Inconsistency Problem,Tx = x18, Rx = x02
            Else
                If (lLen = 0) Then
                    i = 1 'Break Point Here, to Watch Tx Cmd Interrupt Case
                End If
            End If
            '========[ End of Watch Points ]======

            '2017 July 27, correction skipped offset
            m_callbackParametersTagIDGCmdData.ParserDataUpdateBeforeRetrieve()

        Catch ex As Exception
            LogLn("[Err] " + m_cstrClassName + " Send() = " + ex.Message)
        End Try
    End Sub
    '
    Public Function RecvDataRealTimeUpdate(ByRef arrayByte As Byte(), u32RecvLen As UInt32, u32TimeoutMS As UInt32, Optional ByVal bStripOffTrailZERO As Boolean = True) As UInt32
        Dim u32RecvDataSize As UInt32
        '2016 Sept 07, removed it.
        'Rx Mode in Real time Update, disable timeout latency duration (a.k.a. u32TOOffset)
        Dim u32TOOffset As UInt32 = 0 '2000
        Dim nIdx, nIdxMax As Integer
        Dim byteValPre As Byte = 0
        u32RecvDataSize = SafeNativeMethods.USBHIDRecvData_u32( _
                                            m_u32VID, m_u32PID, _
                                        arrayByte, u32RecvLen, _
                                        u32TimeoutMS + u32TOOffset, _
                                        ClassDevUSBHID.pfuncAppDoEvent)

        '-----------------------[ Remove Trail ZERO Padding Data @ USB HID Response Frame ]--------------------------
        'Seek First ZERO from index 0
        If (bStripOffTrailZERO) And (u32RecvDataSize > 0) Then 'Remove this check since Vendi's KeyPad Test Failed sometimes,2016 Aug 26, goofy_liu@idtechproducts.com.tw
            nIdxMax = u32RecvDataSize - 1
            For nIdx = nIdxMax To 0 Step -1
                If ((arrayByte(nIdx) <> &H0) And (byteValPre = &H0)) Then
                    Exit For
                End If
                byteValPre = arrayByte(nIdx)
            Next

            u32RecvDataSize = nIdx + 1

        End If

        Return u32RecvDataSize
    End Function
    '
    '==========================[ Stress Test ]==========================
#Const _DEBUG = 0
    Private Sub RecvUSBHIDExtractData2IDGCmdData(ByRef arrayByte As Byte(), u32RecvLen As UInt32)

        'To Do = Interrupt Command to send if Stop/Cancel Transaction is requested.
        With m_callbackParametersTagIDGCmdData
            If (u32RecvLen < 1) Then
                ' Stop Waiting in SendAndResponse() Layer
                .CallByIDGCmdTimeOutChk_SetTimeoutEventIDGCancelStop()
#If _DEBUG Then
                LogLn("[Err] RecvUSBHID() : nRecvLen = " + u32RecvLen.ToString())
#End If
                'm_refwinformMain.Button_LogSaveFile.PerformClick()
                'm_refwinformMain.LogCleanup()
                Return
            End If
            Array.Copy(arrayByte, .m_refarraybyteData, u32RecvLen)
            .m_nResponse = .m_refarraybyteData(ClassReaderProtocolBasic.EnumIDGCmdOffset.Protocol2_OffSet_Rx_RetStatusCode_LSB_Len_1B) '11) '2016-02-19 , incorrect Response Code, goofy_liu@idtechproducts.com.tw
            .m_byteCmdRx = .m_refarraybyteData(ClassReaderProtocolBasic.EnumIDGCmdOffset.Protocol2_OffSet_CmdMain_Len_1B) '2016-02-19 added.
            .m_nDataSizeRecvRaw = ((CType(.m_refarraybyteData(ClassReaderProtocolBasic.EnumIDGCmdOffset.Protocol2_OffSet_DataLen_MSB_Len_1B), UInt16) << 8) And &HFF00) + CType(.m_refarraybyteData(ClassReaderProtocolBasic.EnumIDGCmdOffset.Protocol2_OffSet_DataLen_LSB_Len_1B), UInt16)
            .m_nDataSizeRecv = tagIDGCmdData.m_nPktHeader + .m_nDataSizeRecvRaw + tagIDGCmdData.m_nPktTrail
            .m_nDataRecvOff = .m_nDataSizeRecv
            If (.m_nDataSizeRecv <> u32RecvLen) Then
                LogLn("[RecvUSBHID] Data Recv Length is invalid, V2 Frame Size of Header =" + .m_nDataSizeRecv.ToString() + " <> USB Recv Len = " + u32RecvLen.ToString())
            End If
        End With

        If (Not ClassReaderProtocolBasic.IsOkWhileCRCCheckFrameResponse(m_callbackParametersTagIDGCmdData)) Then
            LogLn("[V2 Err][CRC] Invalid")
        End If
    End Sub

    '
    '
    '
    Private Sub SendCommandFrameAndWaitForResponseFrameNormal(ByRef callbackExternalRecvProcedure As DelegatecallbackExternalRecvProcedure, ByRef refIDGCmdData As tagIDGCmdData, Optional ByVal bWait As Boolean = True, Optional ByVal u32TimeoutMS As UInt32 = ClassDevRS232.m_cnTX_TIMEOUT)
        'Dim bDebug As Boolean = True
        Try
            Dim nTicksCountDelay As Integer = 0
            Dim nTicksCountStart As Integer
            Dim nTicksCountStartSuitableDeplay As Integer
            Dim strTmp As String
            '
            m_bTimeout = False

            Dim strMsgCmdSub As String = refIDGCmdData.strCmdNEOFormat
            Dim strWaitStatus As String
            If bWait Then
                strWaitStatus = "Wait Response"
            Else
                strWaitStatus = "Exit Without Wait"
            End If

            ' Set to Command Activate Mode
            refIDGCmdData.CommInterruptFSMAct()

            'Config Receiver Call Back Function & Reset Response Value to default(non-use state)
            ConfigCallbackRecv(callbackExternalRecvProcedure, refIDGCmdData)
            '
            refIDGCmdData.DefaultResponse()
            strTmp = "<<<<<<<<<< Tx Dat , " + Form01_Main.GetTimeStampYear2Millisec() + " >>>>>>>>>>"
            If (u32TimeoutMS = 0) Then
                '==========[ Warning ]==========
                'Invalid Timeout value for USB-HID Connections
                'Please check caller's timeout value
                LogLn(strTmp)
                Form01_Main.LogLnDumpData(refIDGCmdData.m_refarraybyteData, refIDGCmdData.m_nDataSizeSend)
                LogLn(" ")
                LogLn("[Err][USB-HID TxRx][Invalid Timeout Value][Not Allowed] = " + u32TimeoutMS.ToString())
                Return
            ElseIf (m_bLogTurnOnOff) Then
                LogLn(strTmp)
                Form01_Main.LogLnDumpData(refIDGCmdData.m_refarraybyteData, refIDGCmdData.m_nDataSizeSend)
                LogLn(" ")
            End If

            If (refIDGCmdData.m_refarraybyteData IsNot Nothing) Then
                If (refIDGCmdData.m_refarraybyteData.Length = 0) Then
                    Dim bChk As Boolean
                    bChk = False
                End If
            End If
            '=======================[  Send/Recv USB  HID Process ]=======================
            SendCommandAndResponse(refIDGCmdData.m_refarraybyteData, refIDGCmdData.m_nDataSizeSend, u32TimeoutMS)

            'Sync IDG Cmd Data Status
            refIDGCmdData = m_callbackParametersTagIDGCmdData
            'LogLn("SendCommandFrameAndWaitForResponseFrame() 4")
            '----------------------[ Waiting for Response Frame ]----------------------
            If bWait Then
                'Get Start System Ticks
                nTicksCountStart = Environment.TickCount
                nTicksCountStartSuitableDeplay = nTicksCountStart
                While Not IsResponseFrameReady(refIDGCmdData)

                    m_pMF.CalledByPreProcessCbRecv_InterruptByExternalStatusUpdate(refIDGCmdData)

                    '================================================================================
                    'Make Other Windows Control Events Available
                    Application.DoEvents()

                    If (refIDGCmdData.CallByIDGCmdTimeOutChk_IsCommIDGCancelStop) Then
                        ' This is triggered by "Transaction Stop" Button
                        Exit While
                    End If

                    If (refIDGCmdData.CallByIDGCmdTimeOutChk_IsCommTimeOutCancelStop) Then
                        ' This is triggered by Transaction Timer

                        m_bTimeout = True

                        Exit While
                    End If

                    If (refIDGCmdData.bIsInterruptedByExternal) Then
                        ' This is triggered by User to Press "Cancel" Button
                        Exit While
                    End If

                    'Thread.Sleep(20)
                    ''
                    ''LogLn("SendCommandFrameAndWaitForResponseFrame() 5")
                    ''
                    'nTicksCountCurrent = Environment.TickCount
                    ''Check Timeout Ticks Reached ?
                    'If (nTicksCountCurrent - nTicksCountStart >= nTimeout) Then
                    '    bTimeout = True
                    '    Exit While
                    'End If

                    ''Make Suitable Delay 50 ms / 150 ms to sleep the thread
                    'If (nTicksCountCurrent - nTicksCountStartSuitableDeplay >= 100) Then

                    '    nTicksCountStartSuitableDeplay = nTicksCountCurrent
                    '    'Make Other Windows Control Events Available
                    '    'Application.DoEvents()

                    '    'LogLn("SendCommandFrameAndWaitForResponseFrame() 6")
                    'End If
                End While

                If (refIDGCmdData.bIsInterruptedByExternal) Then
                    'User press a Cancel Button to stop IDG Commands
                    Throw New Exception("[Warning] Cancel Button Pressed to Stop USB HID Receiving Handler")
                End If
                '
                If refIDGCmdData.CallByIDGCmdTimeOutChk_IsCommIDGCancelStop Then
                    Throw New Exception("[Warning] Stopping USB HID Recv Handler")

                End If
                '
                If m_bTimeout Then
                    strMsgCmdSub = String.Format("[Err][Timeout] Cmd Frame, Cmd ={0}", strMsgCmdSub)
                    Throw New TimeoutException(strMsgCmdSub)
                End If
                '
                If (Not m_bIsEngMode_HWTest) Then
                    If ((Not m_bLogTurnOnOff) Or (Not refIDGCmdData.bIsResponseOK)) Then
                        LogLn(strMsgCmdSub + " << RxSts = " + refIDGCmdData.strGetResponseMsg + "( " & refIDGCmdData.byteGetResponseCode & " )")
                    End If

                    '
                    If (m_bLogTurnOnOff) Or (Not refIDGCmdData.bIsResponseOK) Then
                        LogLn(">>>>>>>>>> Rx Dat, " + Form01_Main.GetTimeStampYear2Millisec() + " <<<<<<<<<<")
                        'Form01_Main.LogLnDumpData(refIDGCmdData.m_refarraybyteData.Skip(refIDGCmdData.m_nDataRecvOffSkipBytes).ToArray(), refIDGCmdData.m_nDataSizeRecv)
                        With refIDGCmdData
                            Form01_Main.LogLnDumpData(.m_refarraybyteData.Skip(.m_nDataRecvOffSkipBytes - .m_nDataSizeRecv).ToArray(), .m_nDataSizeRecv)
                        End With
                        LogLn(" ")
                    End If
                End If
                
            Else
                ' Do Nothing
            End If
        Catch ex As Exception
            Throw 'ex
        Finally
            If ((refIDGCmdData.CallByIDGCmdTimeOutChk_IsCommIDGCancelStop) Or (refIDGCmdData.CallByIDGCmdTimeOutChk_IsCommTimeOutCancelStop)) Then
                refIDGCmdData.CallByIDGCmdTimeOutChk_SetTimeoutEvent()
            Else
                If ((Not refIDGCmdData.CallByIDGCmdTimeOutChk_IsCommIdle) And (m_bLogTurnOnOff)) Then
                    'LogLn("[Dev USB HID][Warning] Not in Stopping Status or No instance stop Receiving Procedure, must have some exception, FSM = " & refIDGCmdData.CallByIDGCmdTimeOutChk_GetStrCommFSM) '2016 July 06
                End If
            End If
            'Start Time Out Mechanism in Main Form
            m_refwinformMain.IDGCmdTimeOutRealTimeResetAndStart(u32TimeoutMS)
            refIDGCmdData.CommInterruptFSMIdle()
        End Try
    End Sub


    Public Overrides Sub SendCommandFrameAndWaitForResponseFrame(ByRef callbackExternalRecvProcedure As DelegatecallbackExternalRecvProcedure, ByRef refIDGCmdData As tagIDGCmdData, ByVal nPlatform As ClassReaderInfo.ENU_FW_PLATFORM_TYPE, Optional ByVal bWait As Boolean = True, Optional ByVal u32TimeoutInMilliSeconds As UInt32 = ClassDevRS232.m_cnTX_TIMEOUT)
        Try
            Dim strMsg As String = String.Format("{0,4:X4}", refIDGCmdData.m_u16CmdSet)
            If (m_bLogTurnOnOff) Then
                If (nPlatform = ClassReaderInfo.ENU_FW_PLATFORM_TYPE.AR) Then
                    strMsg = strMsg + ", " + ClassReaderProtocolBasic.GetCommandDescription(refIDGCmdData.m_u16CmdSet)
                    'ElseIf (nPlatform = ClassReaderInfo.ENU_FW_PLATFORM_TYPE.GR) Then
                Else
                    strMsg = strMsg + ", " + ClassReaderProtocolIDGGR.GetCommandDescription(refIDGCmdData.m_u16CmdSet)

                End If
                LogLn("IDG Cmd [ " + strMsg + " ]")
            End If

            If m_bDebugRecvMode Then
                SendCommandFrameAndWaitForResponseFrameDebugRecvMode(callbackExternalRecvProcedure, refIDGCmdData, bWait, u32TimeoutInMilliSeconds)
            Else
                'Start Time Out Mechanism in Main Form
                m_refwinformMain.IDGCmdTimeOutRealTimeResetAndStart(u32TimeoutInMilliSeconds)
                'Prevent from External Loop Interruption
                InterruptExternalLoop(False)
                SendCommandFrameAndWaitForResponseFrameNormal(callbackExternalRecvProcedure, refIDGCmdData, bWait, u32TimeoutInMilliSeconds)
            End If
        Catch ex As Exception
            Throw 'ex
        Finally
            'm_refwinformMain.IDGCmdTimeOutRealTimeStop()
        End Try

    End Sub


    'Note:
    'If DUT is Not in USB Connection before invoking this function,
    'this function will open --> check status --> close usb port
    Public Function bIsUSBCablePlugged(Optional ByVal bRenewPort As Boolean = False, Optional ByVal bClose As Boolean = True) As Boolean
        Dim bIsPlugged As Boolean = False

        If (Not bRenewPort) Then
            If (bIsOpen()) Then
                Return True
            End If
        End If

        Open()
        '
        If (bIsOpen()) Then
            bIsPlugged = True
        End If
        '
        If (bClose) Then
            Close()
        End If

        Return bIsPlugged
    End Function



End Class
