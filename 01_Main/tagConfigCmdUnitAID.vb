﻿
Public Structure tagConfigCmdUnit
    Public m_nConfigCmd As ClassReaderCommanderParser.EnumConfigCmd
    '
    Public m_bIsConfigCmdStart As Boolean
    Public m_bIsConfigCmdEnd As Boolean
    Public m_strCommentTrail As String

End Structure

Public Structure tagConfigCmdUnitAID

    Public m_tagConfigCmd As tagConfigCmdUnit
    Public m_dictTLV As Dictionary(Of String, tagTLV)
    Public m_strTerminalID As String
    Public m_strTerminalIDOld As String
    Public m_strGroupID As String
    Public m_strKeyTagTerminalIDGroupID As String
    '
    Public m_strUIDriverTabName As String
    '---------------
    Sub New(ByVal nConfigCmd As ClassReaderCommanderParser.EnumConfigCmd)

        Cleanup()
        'Setup Init Values to member vars
        m_tagConfigCmd.m_nConfigCmd = nConfigCmd
        m_dictTLV = New Dictionary(Of String, tagTLV)

    End Sub
    '---------------
    Sub Cleanup()
        'Dim iteratorKeyVal As KeyValuePair(Of String, tagTLV)

        m_tagConfigCmd = Nothing

        ''TLV Value Clean up
        'If m_dictTLV IsNot Nothing Then
        '    If m_dictTLV.Count > 0 Then
        '        For Each iteratorKeyVal In m_dictTLV
        '            iteratorKeyVal.Value.Cleanup()
        '        Next
        '        m_dictTLV.Clear()
        '    End If
        'End If
        ClassTableListMaster.CleanupConfigurationsTLVList(m_dictTLV)

        ' Set to integer max value
        m_strTerminalID = ""
        m_strTerminalIDOld = ""
        m_strGroupID = ""
        m_strUIDriverTabName = ""
    End Sub

End Structure
