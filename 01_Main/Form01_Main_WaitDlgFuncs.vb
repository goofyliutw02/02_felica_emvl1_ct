﻿Imports Microsoft.Win32


Partial Public Class Form01_Main
    '
    Private m_winformWaitTillDone As FormWaitTillDone
    Private m_bWaitMsgDlgON As Boolean
    '============[ Wait Message Box Control Functions ]============
    Private Sub MainForm_Load_WaitTillDone_Dlg_Init()
        '
        Dim bKeepLocationAndStartPositionSetting As Boolean = False
        Dim ptNewLocation As Point
        Dim formStartLocMode As FormStartPosition
        Dim sz As Size
        'Need to inherit external mode for MainForm Service Functions Control
        If (m_winformWaitTillDone IsNot Nothing) Then
            If (m_winformWaitTillDone.IsDisposed) Then
                If (m_winformWaitTillDone.bServiceMode) Then
                    bKeepLocationAndStartPositionSetting = True
                    ptNewLocation = m_winformWaitTillDone.Location
                    formStartLocMode = m_winformWaitTillDone.StartPosition
                    sz = m_winformWaitTillDone.Size
                End If
            End If
        End If
        '
        m_winformWaitTillDone = New FormWaitTillDone
        m_winformWaitTillDone.Init()
        m_winformWaitTillDone.Hide()
        ' Inherits properties here
        If (bKeepLocationAndStartPositionSetting) Then
            m_winformWaitTillDone.ExternalModeConfigParent = m_refRichTextBox_LogWin
            m_winformWaitTillDone.bServiceMode = True
            m_winformWaitTillDone.Location = ptNewLocation
            m_winformWaitTillDone.StartPosition = formStartLocMode
            m_winformWaitTillDone.Size = sz
        End If
    End Sub

    Public Sub WaitMsgShow(Optional bCancelSystemCtrls As Boolean = False, Optional ByVal strMsgCust As String = "", Optional ByVal bNoInterruptCmd As Boolean = False, Optional ByVal nTimeoutMS As Integer = 0)
        If (Not m_bWaitMsgDlgON) Then '
            Return
        End If
        If (m_winformWaitTillDone Is Nothing) Then
            MainForm_Load_WaitTillDone_Dlg_Init()
        End If
        If (m_winformWaitTillDone.IsDisposed()) Then
            MainForm_Load_WaitTillDone_Dlg_Init()
        End If
        '
        '
        If (Not m_winformWaitTillDone.Visible) Then
            'Show message box without Cancel Button And System Control Box
            m_winformWaitTillDone.SetCancelMode(bCancelSystemCtrls, strMsgCust, bNoInterruptCmd)
            'If (nTimeoutMS > 0) Then
            m_winformWaitTillDone.TimerUpdateTimeoutReset(nTimeoutMS)
            'Else
            '    m_winformWaitTillDone.TimerUpdateTimeoutStop()
            'End If

            m_winformWaitTillDone.Show()
        End If

        'Center Location
        'X, Y-aix Coordination
        'm_winformWaitTillDone.SetBounds(Me.Location.X + (Me.Size.Width - m_winformWaitTillDone.Size.Width) / 2, Me.Location.Y + (Me.Size.Height - m_winformWaitTillDone.Size.Height) / 2, 0, 0, BoundsSpecified.Location)

    End Sub
    '
#If 0 Then

    Public Sub WaitMsgShow(Optional bCancelSystemCtrls As Boolean = False, Optional ByVal idstrMsgCust As ClassServiceLanguages._WTD_STRID = ClassServiceLanguages._WTD_STRID.COMMON_NA, Optional ByVal bNoInterruptCmd As Boolean = False, Optional ByVal nTimeOutInMS As Integer = 0)
        Dim strMsgCust As String = m_pRefLang.GetMsgLangTranslation(idstrMsgCust)

        WaitMsgShow(bCancelSystemCtrls, strMsgCust, bNoInterruptCmd, nTimeOutInMS)

    End Sub


    'Dialog Box Wait Till User Press OK
    Public Sub WaitMsgShowDialogBox(Optional bCancelSystemCtrls As Boolean = False, Optional ByVal idstrMsgCust As ClassServiceLanguages._WTD_STRID = ClassServiceLanguages._WTD_STRID.COMMON_NA, Optional ByVal bNoInterruptCmd As Boolean = False)
        Dim strMsgCust As String = m_pRefLang.GetMsgLangTranslation(idstrMsgCust)

        WaitMsgShowDialogBox(bCancelSystemCtrls, strMsgCust, bNoInterruptCmd)

    End Sub
#End If

    Public Sub WaitMsgShowDialogBox(Optional bCancelSystemCtrls As Boolean = False, Optional ByVal strMsgCust As String = "", Optional ByVal bNoInterruptCmd As Boolean = False)
        If (Not m_bWaitMsgDlgON) Then '
            Return
        End If
        If (m_winformWaitTillDone Is Nothing) Then
            MainForm_Load_WaitTillDone_Dlg_Init()
        End If
        If (m_winformWaitTillDone.IsDisposed()) Then
            MainForm_Load_WaitTillDone_Dlg_Init()
        End If

        If (Not m_winformWaitTillDone.Visible) Then
            'Show message box without Cancel Button And System Control Box
            m_winformWaitTillDone.SetCancelMode(bCancelSystemCtrls, strMsgCust, bNoInterruptCmd)

            m_winformWaitTillDone.ShowDialog()
        End If

        'Center Location
        'X, Y-aix Coordination
        'm_winformWaitTillDone.SetBounds(Me.Location.X + (Me.Size.Width - m_winformWaitTillDone.Size.Width) / 2, Me.Location.Y + (Me.Size.Height - m_winformWaitTillDone.Size.Height) / 2, 0, 0, BoundsSpecified.Location)

    End Sub
    '

    '
    ReadOnly Property bIsWaitMsgEnd As Boolean
        Get
            Return (m_winformWaitTillDone.IsDisposed Or (Not m_winformWaitTillDone.Visible))
        End Get
    End Property

    ReadOnly Property bIsWaitMsgCanceled As Boolean
        Get
            Return m_winformWaitTillDone.bIsCanceled
        End Get
    End Property
    ReadOnly Property bIsWaitMsgPassPressed As Boolean
        Get
            Return m_winformWaitTillDone.bIsPass
        End Get
    End Property
    '
    Public Sub WaitMsgShowPassFailMode(Optional bPassFailCtrls As Boolean = False, Optional ByVal strMsgCust As String = "", Optional ByVal nAnsMode As FormWaitTillDone._ANS = FormWaitTillDone._ANS._PASS_FAIL, Optional ByVal nTimeoutMS As Integer = ClassDevRS232.m_cnRX_TIMEOUT_IN_SEC * 1000)
        If (Not m_bWaitMsgDlgON) Then '
            Return
        End If
        '
        If (m_winformWaitTillDone Is Nothing) Then
            MainForm_Load_WaitTillDone_Dlg_Init()
        End If
        If (m_winformWaitTillDone.IsDisposed()) Then
            MainForm_Load_WaitTillDone_Dlg_Init()
        End If

        If (Not m_winformWaitTillDone.Visible) Then
            m_winformWaitTillDone.AnsMode = nAnsMode
            'Show message box without Cancel Button And System Control Box
            m_winformWaitTillDone.SetPASSFAILMode(bPassFailCtrls, strMsgCust)
            m_winformWaitTillDone.TimerUpdateTimeoutReset(nTimeoutMS)
            m_winformWaitTillDone.Show()
        End If

        If (Not m_winformWaitTillDone.bServiceMode) Then
            'Center Location, External Mode is not allowed here !!
            'X, Y-aix Coordination
            m_winformWaitTillDone.SetBounds(Me.Location.X + (Me.Size.Width - m_winformWaitTillDone.Size.Width) / 2, Me.Location.Y + (Me.Size.Height - m_winformWaitTillDone.Size.Height) / 2, 0, 0, BoundsSpecified.Location)
        End If

    End Sub
    '
    Public Sub WaitMsgHide()
        If (Not m_bWaitMsgDlgON) Then '
            Return
        End If

        If (m_winformWaitTillDone Is Nothing) Then
            Return
        End If
        If (m_winformWaitTillDone.IsDisposed()) Then
            Return
        End If

        If (m_winformWaitTillDone.Visible) Then
            m_winformWaitTillDone.Hide()
            'Invisible to make it Unable to cancel after window hidden
            m_winformWaitTillDone.SetCancelMode(False)
        Else
        End If
    End Sub
    '
    Private Shared Sub GetVersionFromRegistryPrintout(strMsg As String)

        Console.WriteLine(strMsg)

        If (m_pInstance IsNot Nothing) Then
            m_pInstance.LogLn(strMsg)
        End If
    End Sub
    '
    Private Shared Sub GetVersionFromRegistry()
        Using ndpKey As RegistryKey = RegistryKey.OpenBaseKey(RegistryHive.LocalMachine, RegistryView.Registry32).OpenSubKey("SOFTWARE\Microsoft\NET Framework Setup\NDP\")

            For Each versionKeyName As String In ndpKey.GetSubKeyNames()
                If versionKeyName.StartsWith("v") Then
                    Dim versionKey As RegistryKey = ndpKey.OpenSubKey(versionKeyName)
                    Dim name As String = DirectCast(versionKey.GetValue("Version", ""), String)
                    Dim sp As String = versionKey.GetValue("SP", "").ToString()
                    Dim install As String = versionKey.GetValue("Install", "").ToString()
                    If install = "" Then
                        'no install info, ust be later
                        GetVersionFromRegistryPrintout(versionKeyName & "  " & name)
                    Else
                        If sp <> "" AndAlso install = "1" Then
                            GetVersionFromRegistryPrintout(versionKeyName & "  " & name & "  SP" & sp)
                        End If
                    End If
                    If name <> "" Then
                        Continue For
                    End If
                    For Each subKeyName As String In versionKey.GetSubKeyNames()
                        Dim subKey As RegistryKey = versionKey.OpenSubKey(subKeyName)
                        name = DirectCast(subKey.GetValue("Version", ""), String)
                        If name <> "" Then
                            sp = subKey.GetValue("SP", "").ToString()
                        End If
                        install = subKey.GetValue("Install", "").ToString()
                        If install = "" Then
                            'no install info, ust be later
                            GetVersionFromRegistryPrintout(versionKeyName & "  " & name)
                        Else
                            If sp <> "" AndAlso install = "1" Then
                                GetVersionFromRegistryPrintout("  " & subKeyName & "  " & name & "  SP" & sp)
                            ElseIf install = "1" Then
                                GetVersionFromRegistryPrintout("  " & subKeyName & "  " & name)

                            End If
                        End If
                    Next
                End If
            Next
        End Using
    End Sub
    '
#If 0 Then
     '
    Public Sub WaitMsgShow(Optional bCancelSystemCtrls As Boolean = False, Optional ByVal nMsgIDCust As ClassServiceLanguages._WTD_STRID = ClassServiceLanguages._WTD_STRID._XX, Optional ByVal nAnsMode As FormWaitTillDone._ANS = FormWaitTillDone._ANS._PASS_FAIL, Optional ByVal bNoInterruptCmd As Boolean = False)
        If (Not m_bWaitMsgDlgON) Then '
            Return
        End If
        '
        If (m_winformWaitTillDone Is Nothing) Then
            MainForm_Load_WaitTillDone_Dlg_Init()
        End If
        If (m_winformWaitTillDone.IsDisposed()) Then
            MainForm_Load_WaitTillDone_Dlg_Init()
        End If

        If (Not m_winformWaitTillDone.Visible) Then
            'Show message box without Cancel Button And System Control Box
            m_winformWaitTillDone.SetCancelModeLang(bCancelSystemCtrls, nMsgIDCust, bNoInterruptCmd)

            m_winformWaitTillDone.Show()
        End If

        m_winformWaitTillDone.AnsMode = nAnsMode

        ''Center Location
        ''X, Y-aix Coordination
        'm_winformWaitTillDone.SetBounds(Me.Location.X + (Me.Size.Width - m_winformWaitTillDone.Size.Width) / 2, Me.Location.Y + (Me.Size.Height - m_winformWaitTillDone.Size.Height) / 2, 0, 0, BoundsSpecified.Location)

    End Sub
#End If
End Class
