﻿Partial Public Class Form01_Main
    '===============[ COM Port Selections ]===============
    Private m_refwinobjComBoRS232SelectPortName As ComboBox
    Private m_refwinobjComBoRS232SelectBaudRate As ComboBox

    '
    Public Function ServiceBuadrateGet() As ClassDevRS232.EnumBUAD_RATE
        Dim strFuncName As String = "ServiceBuadrateGet"
        '
        If (m_refwinobjComBoRS232SelectBaudRate.Items.Count < 1) Then
            LogLn("[Err] " + strFuncName + "() = No Buad-rate List Available")
            Return ClassDevRS232.EnumBUAD_RATE._UNKNOWN
        End If
        If (m_refwinobjComBoRS232SelectBaudRate.SelectedIndex = -1) Then
            LogLn("[Err] " + strFuncName + "() = No Buad-rate List Available")
            Return ClassDevRS232.EnumBUAD_RATE._UNKNOWN
        End If
        '
        Return CType(Convert.ToInt32(m_refwinobjComBoRS232SelectBaudRate.Items(m_refwinobjComBoRS232SelectBaudRate.SelectedIndex), 10), ClassDevRS232.EnumBUAD_RATE)
    End Function
    '
    Public Function ServiceBuadrateGetIdx() As Integer
        Dim strFuncName As String = "ServiceBuadrateGetIdx"
        '
        If (m_refwinobjComBoRS232SelectBaudRate.Items.Count < 1) Then
            LogLn("[Err] " + strFuncName + "() = No Buad-rate List Available")
            Return -1
        End If
        If (m_refwinobjComBoRS232SelectBaudRate.SelectedIndex = -1) Then
            LogLn("[Err] " + strFuncName + "() = No Buad-rate List Available")
            Return -1
        End If
        '
        Return m_refwinobjComBoRS232SelectBaudRate.SelectedIndex
    End Function

    '
    Public Sub ServicePortSet(nIdx As Integer)
        Dim strFuncName As String = "ServicePortSet"
        If (m_refwinobjComBoRS232SelectPortName.Items.Count < 1) Then
            LogLn("[Err] " + strFuncName + "() = No Port Available")
            Return
        End If
        If (nIdx >= m_refwinobjComBoRS232SelectPortName.Items.Count) Then
            nIdx = m_refwinobjComBoRS232SelectPortName.Items.Count - 1
        End If
        If (nIdx < 0) Then
            nIdx = 0
        End If
        '
        ComboBox_PortSel_COM.SelectedIndex = nIdx
        m_refwinobjComBoRS232SelectPortName.SelectedIndex = nIdx
    End Sub
    '
    '=================================================
    '
    Public Sub ServiceRS232PortScan(Optional ByRef refComboList As ComboBox = Nothing, Optional ByVal bSelectKeepPrevious As Boolean = False)
        Dim strPortNameList() As String = Nothing
        Dim nIdx As Integer
        Dim strCOMPortPre As String = ""
        Dim strPortName As String
        '
        If (refComboList Is Nothing) Then
            refComboList = m_refwinobjComBoRS232SelectPortName
        End If
        ' If there is preselected
        With refComboList
            If ((bSelectKeepPrevious) And (.Items.Count > 0)) Then
                If (.SelectedIndex < 0) Then
                    .SelectedIndex = 0
                End If
                strCOMPortPre = .Items.Item(.SelectedIndex)
            End If
        End With
        'Put Port Name List in the combo box
        refComboList.Items.Clear()
        ClassDevRS232.EnumeratePorts(strPortNameList)
        If (strPortNameList IsNot Nothing) Then
            For nIdx = 0 To strPortNameList.Count - 1
                strPortName = strPortNameList(nIdx)
                If (Not refComboList.Items.Contains(strPortName)) Then
                    'Prevent from duplicated Ports
                    refComboList.Items.Add(strPortName)
                End If
            Next
            '
            ' If there is preselected , restore it back
            With refComboList
                If (bSelectKeepPrevious) Then
                    If (Not strCOMPortPre.Equals("")) Then
                        If (.Items.IndexOf(strCOMPortPre) > -1) Then
                            .SelectedIndex = .Items.IndexOf(strCOMPortPre)
                        End If
                    End If
                End If
            End With
        Else
            refComboList.Items.Clear()
        End If
    End Sub
    '
    Public Function ServicePortGet() As String
        Dim strFuncName As String = "ServicePortGet"
        '
        If (m_refwinobjComBoRS232SelectPortName.Items.Count < 1) Then
            LogLn("[Err] " + strFuncName + "() = No Port Available")
            Return ""
        End If
        If (m_refwinobjComBoRS232SelectPortName.SelectedIndex = -1) Then
            m_refwinobjComBoRS232SelectPortName.SelectedIndex = 0
            'LogLn("[Err] " + strFuncName + "() = No Port Selected")
            'Return ""
        End If
        '
        Return m_refwinobjComBoRS232SelectPortName.Items(m_refwinobjComBoRS232SelectPortName.SelectedIndex)
    End Function
    '
    '
    ReadOnly Property ServiceGetSelPortNum As Integer
        Get
            If (m_refwinobjComBoRS232SelectPortName IsNot Nothing) Then
                Return m_refwinobjComBoRS232SelectPortName.Items.Count
            End If
            Return 0
        End Get
    End Property
    '
    Public Function ServicePortGetIdx() As Integer
        Dim strFuncName As String = "ServicePortGet"
        '
        If (m_refwinobjComBoRS232SelectPortName.Items.Count < 1) Then
            LogLn("[Err] " + strFuncName + "() = No Port Available")
            Return -1
        End If
        If (m_refwinobjComBoRS232SelectPortName.SelectedIndex = -1) Then
            m_refwinobjComBoRS232SelectPortName.SelectedIndex = 0
            'LogLn("[Err] " + strFuncName + "() = No Port Selected")
            'Return -1
        End If
        '
        Return m_refwinobjComBoRS232SelectPortName.SelectedIndex
    End Function
    '
    '
    Function ServiceSetzCOMPortStr(strCOMPort As String) As Boolean
        Dim bRet As Boolean = m_refwinobjComBoRS232SelectPortName.Items.Contains(strCOMPort)
        '
        If (bRet) Then
            'Yes, there is the COM Port
            m_refwinobjComBoRS232SelectPortName.SelectedIndex = m_refwinobjComBoRS232SelectPortName.Items.IndexOf(strCOMPort)
        End If
        '
        Return bRet
    End Function
    '
    Public Function ServiceConnectRS232AndPing(Optional ByVal nTimeout As Integer = ClassDevRS232.m_cnTX_TIMEOUT_IN_SEC, Optional ByVal bTurnOffWaitMessage As Boolean = True) As Boolean
        Dim nTimeoutOld As Integer = IDGCmdTimeoutGet()
        Dim bTmp As Boolean = m_bWaitMsgDlgON
        Dim strFuncName As String = "ServiceConnectRS232AndPing"
        Dim bIsDUTAvailable As Boolean = False
        '
        Try
            WaitMsgServiceModeStart()

            m_bWaitMsgDlgON = Not bTurnOffWaitMessage
            IDGCmdTimeoutSet(nTimeout)

            'Select either  AR or GR Commander Instance And Open port
            m_refObjRdrCmder = m_objRdrCmderIDGGR
            m_refObjRdrCmder.ConnectionOpen(ServicePortGet(), ServiceBuadrateGet())
            '
            'LogLn("[Dbg Info] Baudrtate = " + CType(ServiceBuadrateGet(), Integer).ToString())
            '
            If (m_refObjRdrCmder.bIsConnectedRS232) Then
                'Ping the reader device is alive or not
                m_refObjRdrCmder.SystemPing(nTimeout)

                '------------[ Check If Status Is Not Ready ]----------------
                If (m_refObjRdrCmder.bIsRetStatusOK) Then
                    bIsDUTAvailable = True
                End If
            End If

        Catch ex As Exception
            LogLn("[Err] ServiceConnectRS232AndPing() = " + ex.Message)
        Finally
            m_refObjRdrCmder.ConnectionClose()
            m_refObjRdrCmder = Nothing
            '
            IDGCmdTimeoutSet(nTimeoutOld)
            m_bWaitMsgDlgON = bTmp


            WaitMsgServiceModeEnd()
        End Try

        Return bIsDUTAvailable
    End Function
    '
    Public Sub WaitMsgServiceModeStart()

    End Sub

    Public Sub WaitMsgServiceModeEnd()

    End Sub
    '
    Private Sub ConnectDeviceRS232_Native()
#If 0 Then


        Dim nProgressCnt, nProgressCntOffsetInLoop, nProgressMax As Integer

        Dim nTimeout As Integer = IDGCmdTimeoutGet()
        Dim nStepsProductPlatformIdentification As Integer = 0
        'Dim bResult As Boolean
        Dim pCDFCL As ClassDataFilesCmpLog = ClassDataFilesCmpLog.GetInstance()

        nProgressCnt = 1
        'nProgressCntOffsetInLoop value is judged by while loops worst case steps
        nProgressCntOffsetInLoop = 6
        ' Use GR+AR Test Scenario to get number of worst case steps since First time Test Scenario Object has not been selected yet
        nProgressMax = nProgressCntOffsetInLoop + 4 + m_objTestScenarioIDGAR.InitProgressBarPrepare()
        nProgressMax = nProgressCntOffsetInLoop + 4 + m_objTestScenarioIDGGR.InitProgressBarPrepare()


        Try
            LogCleanup()

            '=======================[ Start Reader Platform Identification Process ]=========================
            ''--------------------[ Start System Checking ]--------------------
            'WaitMsgHide()
            'WaitMsgShow(False, m_pRefLang.GetMsgLangTranslation(ClassServiceLanguages._WTD_STRID.CONN_CONNING)) '"Connecting..."
            UIEnable(False)

            ProgressBarStart(nProgressMax)

            '----------[ Get COM Port & Buadrate ]-------------------
            If (bIsReaderTypeSelected) Then
                '=========================[ Reader Type is selected by command line option ]================================
                ' Close Port if it's opened
                ComPortOpen_InterruptIDGCmdAndClosePort()
                '------------[ Config RS232 Port  &  Open COM Port ]----------------
                'Select AR/ GR Commander
                If (m_objReaderInfo.bIsPlatformAR_2013) Then
                    m_refObjRdrCmder = m_objRdrCmderIDGAR
                    m_refObjDevRS232 = m_objRdrCmderIDGAR.m_devRS232
                    m_refObjTestScenario = m_objTestScenarioIDGAR
                ElseIf (m_objReaderInfo.bIsPlatformGR_2013) Then
                    m_refObjRdrCmder = m_objRdrCmderIDGGR
                    m_refObjDevRS232 = m_objRdrCmderIDGGR.m_devRS232
                    m_refObjTestScenario = m_objTestScenarioIDGGR
                Else
                    ConnectDeviceUseMyReaderSetting()
                End If

                m_refObjRdrCmder.ConnectionOpen(ServicePortGet(), ServiceBuadrateGet())

                m_objUIDriver.ProgressBarUpdate(nProgressCnt)
                nProgressCntOffsetInLoop = nProgressCntOffsetInLoop - 1
                WaitMsgShow(True, ClassServiceLanguages._WTD_STRID.COMMON_WAIT, FormWaitTillDone._ANS._PASS_FAIL)



                '------------[ Ping the Reader & Check Result while in Transaction None  Debug Mode ]----------------
                If ((Not m_bDebug) And (m_refObjRdrCmder.bIsConnectedRS232)) Then
                    'Turn Off PT
                    m_refObjTestScenario.TestTM_Common_PassthroughMode_And_ThrowErrException(False, False)
                    'Turn Off Custom Display Interface
                    m_refObjTestScenario.TestTM_Common_UI_LCDCustomDisplayMode_And_ThrowErrExceptinOccured(False, False)

                    'GetFirmwareVersionAndFigureOutProductType(m_refObjRdrCmder, m_objReaderInfo)
                    'Ping the reader device is alive or not
                    'IDGCmdTimeoutSet(nTimeout)
                    m_refObjRdrCmder.SystemPing(nTimeout)

                    '------------[ Check If Status Is Not Ready ]----------------
                    If Not (m_refObjRdrCmder.bIsRetStatusOK) Then
                        m_refObjRdrCmder.ConnectionClose()
                        m_objUIDriver.ProgressBarEnd()
                        WaitMsgHide()
                        Throw New Exception(" No Response")
                    End If
                End If

                m_objUIDriver.ProgressBarUpdate(nProgressCnt)
            Else
                '====================================[ Auto Detection to Reader's Type : Start ]==============================================
                While (True)
                    '------------[ Close Com Port if it's opened --> Prepare Interrupt IDG Commands and Close Com Port If it's necessary ]----------------
                    ComPortOpen_InterruptIDGCmdAndClosePort()

                    '2017 Nov 13, goofy_liu
                    'Using GR/NEO/NEO20 setting as default first
                    m_refObjDevRS232 = m_objRdrCmderIDGGR.m_devRS232
                    'm_refObjDevUSBHID = m_objRdrCmderIDGGR.m_devUSBHID
                    m_refObjRdrCmder = m_objRdrCmderIDGGR
                    m_refObjTestScenario = m_objTestScenarioIDGGR

                    '------------[ Config RS232 Port  &  Open COM Port ]----------------

                    m_refObjRdrCmder.ConnectionOpen(ServicePortGet(), ServiceBuadrateGet())
                    'End If
                    m_objUIDriver.ProgressBarUpdate(nProgressCnt)
                    nProgressCntOffsetInLoop = nProgressCntOffsetInLoop - 1
                    WaitMsgShow(True, ClassServiceLanguages._WTD_STRID.COMMON_WAIT, FormWaitTillDone._ANS._PASS_FAIL)

                    '------------[ Ping the Reader & Check Result while in Transaction None  Debug Mode ]----------------

                    If (m_refObjRdrCmder.bIsConnectedRS232) Then

                        If (Not m_bDebug) Then
                            'GetFirmwareVersionAndFigureOutProductType(m_refObjRdrCmder, m_objReaderInfo)
                            'Ping the reader device is alive or not
                            m_refObjRdrCmder.SystemPing(nTimeout)

                            '------------[ Check If Status Is Not Ready ]----------------
                            If Not (m_refObjRdrCmder.bIsRetStatusOK) Then
                                'If m_refObjDevRS232.bIsOpen Then
                                '    m_refObjDevRS232.Close()
                                'End If
                                m_refObjRdrCmder.ConnectionClose()
                                m_objUIDriver.ProgressBarEnd()
                                WaitMsgHide()
                                Throw New Exception(" No Response")
                            End If
                        End If
                        ProgressBarUpdate(nProgressCnt, True)

                        nProgressCntOffsetInLoop = nProgressCntOffsetInLoop - 1
                        '------------[ Printout Firmware Version Info and Identify AR/ GR/ Unknown Products ]----------------
                        If (GetFirmwareVersionAndFigureOutProductType(m_refObjRdrCmder, m_objReaderInfo)) Then

                            If (m_objReaderInfo.bIsPlatformNEO20_2017 Or m_objReaderInfo.bIsPlatformNEO_2014 Or m_objReaderInfo.bIsPlatformGR_2013) Then
                                m_objUIDriver.ProgressBarUpdate(nProgressCnt)
                                'Replace AR platform settings with GR/NEO/NEO20,
                                m_refObjDevRS232 = m_objRdrCmderIDGGR.m_devRS232
                                m_refObjRdrCmder = m_objRdrCmderIDGGR
                            ElseIf (m_objReaderInfo.bIsPlatformAR_2013 Or m_objReaderInfo.bIsPlatformAR300) Then
                                m_objUIDriver.ProgressBarUpdate(nProgressCnt)
                                'Close GR Command Reader first
                                m_refObjRdrCmder.ConnectionClose()
                                m_refObjRdrCmder = m_objRdrCmderIDGAR
                                m_refObjRdrCmder.ConnectionOpen(ServicePortGet(), ServiceBuadrateGet())
                                ' AR Type - Reopen with correct IDG Commander Object
                                If (m_refObjRdrCmder.bIsConnected) Then
                                    m_refObjDevRS232 = m_objRdrCmderIDGAR.m_devRS232
                                    m_refObjRdrCmder = m_objRdrCmderIDGAR
                                    m_refObjTestScenario = m_objTestScenarioIDGAR
                                Else
                                    Throw New Exception("Can't Connect to AR Reader")
                                End If
                            Else
                                ProgressBarEnd()
                                WaitMsgHide()
                                '------------[ Step 2 = show message box with error information "Unsupported UUT (Unknown Product Type)" ]----------------

                                LogLn("[Warning] It  couldn't find Compatible Reader Info @ RS232 Interface --> Use MyReader Setting as default")

                                ConnectDeviceUseMyReaderSetting()

                                ''------------[ Open COM Port ]----------------
                                m_refObjRdrCmder.ConnectionOpen(ServicePortGet, ServiceBuadrateGet())
                                'Throw New Exception("Unsupported Platform")
                            End If

                            'End of Reader Identification.
                            Exit While

                        End If
                    End If
                    '
                    nStepsProductPlatformIdentification = nStepsProductPlatformIdentification + 1
                End While
                '====================================[ Auto Detection to Reader's Type : End ]==============================================
            End If

            m_objUIDriver.ProgressBarUpdate(nProgressCntOffsetInLoop)
            '
            m_refObjRdrCmder.bLogTurnOnOff = CheckBox_Engineer_Debug_On.Checked
            m_refObjRdrCmder.bIsHWTestOnly = ChkBox_EngMode_HWTestMode.Checked

            WaitMsgHide()
            '
            If (Not m_refObjRdrCmder.bIsConnected) Then
                ComPortOpen_InterruptIDGCmdAndClosePort()
                Return
            End If
            '
            WaitMsgShow(False, m_pRefLang.GetMsgLangTranslation(ClassServiceLanguages._WTD_STRID.TEST_PREPARING)) '"Preparing Test..."

            ConnectDeviceConnectedDevInit_Common(AddressOf m_objUIDriver.ProgressBarUpdate, nProgressCnt)

            ProgressBarEnd()

            'MainForm_Load_CmdLineProcess_After_Connection()

            'ConnectDevice_UpdateVivoProcolV2_V3()

            '------------[ Save Successful Setting ]-------------
        Catch ex As Exception
            LogLn("[Err] ConnectDeviceRS232() = " + ex.Message)
        Finally
            If (bIsConnected) Then
                'Show Log Main Info
                LogLn("Connected !!")
                UIEnable(True, 2)
            Else
                LogLn("Not Connected !!")
                UIEnable(True, 1)
            End If
        End Try
#End If
    End Sub

    '
    Private Function ConnectDeviceRS232_Simple() As Boolean
        Dim nTimeout As Integer = 1
        Dim nStepsProductPlatformIdentification As Integer = 0
        Dim bResult As Boolean = False
        Dim strFuncName As String = "ConnectDeviceRS232_Simple"

        Try
            LogCleanup()

            '=======================[ Start Reader Platform Identification Process ]=========================
            '--------------------[ Start System Checking ]--------------------
            UIEnable(False)

            '-------------------[ Config As Vendi, GR platform ]--------------------
            Select Case (m_objReaderInfo.GetPlatformType)
                Case ClassReaderInfo.ENU_FW_PLATFORM_TYPE.AR
                    m_refObjDevRS232 = m_objRdrCmderIDGGR.m_devRS232
                    m_refObjRdrCmder = m_objRdrCmderIDGGR
                    'm_refObjTestScenario = m_objTestScenarioIDGAR
                Case Else
                    m_refObjDevRS232 = m_objRdrCmderIDGGR.m_devRS232
                    m_refObjRdrCmder = m_objRdrCmderIDGGR
                    'm_refObjTestScenario = m_objTestScenarioIDGGR
            End Select
            '
            m_refObjRdrCmder.ConnectionOpen(ServicePortGet(), ServiceBuadrateGet())
            If (Not m_refObjRdrCmder.bIsConnected) Then
                Throw New Exception("Connection Open Failed")
            End If
            '
            IDGCmdTimeoutSet(nTimeout)
            m_refObjRdrCmder.SystemPing(nTimeout)
            If (Not m_refObjRdrCmder.bIsRetStatusOK) Then
                Throw New Exception("Connection Open Ping Failed")
            End If

            'TODO, to modify it in the future 
            'Setup Reader Product Type, Since this is simple connection, we will keep use previous reader info settings
#If 1 Then
            Dim lstHWInfo As List(Of String) = New List(Of String)()
            Dim io_nNativePrdID As ClassReaderInfo.ENUM_PD_IMG_IDX = m_refObjRdrCmder.propUSBHIDGetProductNameID

            m_refObjRdrCmder.SystemGetHardwareInfo(lstHWInfo)
            If (Not m_refObjRdrCmder.bIsRetStatusOK) Then
                '
                'Exit Try
            End If
            '- [ New Method ]-
            'Possible has sub-reader type, TS needs to perform advanced Version /HW Info Checks
            'For Ex: Unipay III --> Branches to Unipay III, VP4880, VP4880E, and VP4880C.
            ' All of them have same VID:PID
            If m_objReaderInfo.SetProduct_LV2(io_nNativePrdID, String.Join(",", lstHWInfo.ToArray())) Then
                m_refObjRdrCmder.propUSBHIDGetProductNameID = io_nNativePrdID
            End If
            '
            lstHWInfo.Clear()
#End If

            '===============[ TODO End of Reader Type Setting ]===============
            'm_refObjRdrCmder.bLogTurnOnOff = CheckBox_Engineer_Debug_On.Checked

            bResult = True

        Catch ex As Exception
            LogLn("[Err] " + strFuncName + "() = " + ex.Message)
        Finally
            UIEnable(True)
        End Try

        Return bResult
    End Function

    Public Function ServiceConnectionRS232Init(bSimple As Boolean, Optional ByVal nTimeout As Integer = ClassDevRS232.m_cnTX_TIMEOUT_IN_SEC, Optional ByVal bTurnOffWaitMessage As Boolean = True) As Boolean
        Dim nTimeoutOld As Integer = IDGCmdTimeoutGet()
        Dim bTmp As Boolean = m_bWaitMsgDlgON
        Dim strFuncName As String = "ServiceConnectionRS232Init"
        '
        Try
            WaitMsgServiceModeStart()

            m_bWaitMsgDlgON = Not bTurnOffWaitMessage
            IDGCmdTimeoutSet(nTimeout)
            '
            If (bSimple) Then
                ConnectDeviceRS232_Simple()
            Else
                ConnectDeviceRS232_Native()
            End If


        Catch ex As Exception
            LogLn("[Err] " + strFuncName + "() = " + ex.Message)
        Finally
            IDGCmdTimeoutSet(nTimeoutOld)
            m_bWaitMsgDlgON = bTmp

            WaitMsgServiceModeEnd()
        End Try
        'Possible connected failed
        If (m_refObjRdrCmder Is Nothing) Then
            Return False
        End If
        Return m_refObjRdrCmder.bIsConnectedRS232
    End Function
    '
End Class
