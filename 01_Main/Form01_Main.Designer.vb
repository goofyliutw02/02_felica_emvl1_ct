﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class Form01_Main
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(Form01_Main))
        Me.Panel1_Tier01 = New System.Windows.Forms.Panel()
        Me.SplitContainer1 = New System.Windows.Forms.SplitContainer()
        Me.Panel1 = New System.Windows.Forms.Panel()
        Me.SplitContainer4 = New System.Windows.Forms.SplitContainer()
        Me.PictureBox1 = New System.Windows.Forms.PictureBox()
        Me.SplitContainer9 = New System.Windows.Forms.SplitContainer()
        Me.SplitContainer10 = New System.Windows.Forms.SplitContainer()
        Me.SplitContainer3 = New System.Windows.Forms.SplitContainer()
        Me.SplitContainer8 = New System.Windows.Forms.SplitContainer()
        Me.TabCtrl_CommIntf = New System.Windows.Forms.TabControl()
        Me.TabPg_Intf_USB = New System.Windows.Forms.TabPage()
        Me.Panel_S00_3_DevMode_Lst = New System.Windows.Forms.Panel()
        Me.GroupBox_InterfaceMode = New System.Windows.Forms.GroupBox()
        Me.RdoBtn_S00_01_USB_KB_WEDGE = New System.Windows.Forms.RadioButton()
        Me.RdoBtn_S00_01_USB_HID = New System.Windows.Forms.RadioButton()
        Me.TabPg_Intf_2_RS232 = New System.Windows.Forms.TabPage()
        Me.Label3 = New System.Windows.Forms.Label()
        Me.Label4 = New System.Windows.Forms.Label()
        Me.ComboBox_PortSel_COM = New System.Windows.Forms.ComboBox()
        Me.ComboBox_PortSel_Baudrate = New System.Windows.Forms.ComboBox()
        Me.SplitContainer12 = New System.Windows.Forms.SplitContainer()
        Me.TabCtrl_EndMode = New System.Windows.Forms.TabControl()
        Me.TabPg_End_Rounds = New System.Windows.Forms.TabPage()
        Me.GrpBox_02_RndSel = New System.Windows.Forms.GroupBox()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.Rnd_02_Btn_Get_Rnd = New System.Windows.Forms.Button()
        Me.Rnd_01_Cmbox_Sel_Rnd = New System.Windows.Forms.ComboBox()
        Me.TabPg_End_TWait = New System.Windows.Forms.TabPage()
        Me.m_CmbBox_MifareRunMode = New System.Windows.Forms.ComboBox()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.m_CBox_End_TWaitTime = New System.Windows.Forms.ComboBox()
        Me.GrpBox_SeekCardType = New System.Windows.Forms.GroupBox()
        Me.GrpBox_04_TxRx_ToutMs = New System.Windows.Forms.GroupBox()
        Me.NumUpDwn_Timeout_TxRx = New System.Windows.Forms.NumericUpDown()
        Me.cbox_SeekCardType = New System.Windows.Forms.ComboBox()
        Me.SplitContainer11 = New System.Windows.Forms.SplitContainer()
        Me.Btn_S00_Connect_Start = New System.Windows.Forms.Button()
        Me.Btn_S00_Stop_Close = New System.Windows.Forms.Button()
        Me.ProgressBar1 = New System.Windows.Forms.ProgressBar()
        Me.Panel2 = New System.Windows.Forms.Panel()
        Me.GroupBox2 = New System.Windows.Forms.GroupBox()
        Me.SplitContainer7 = New System.Windows.Forms.SplitContainer()
        Me.SplitContainer2 = New System.Windows.Forms.SplitContainer()
        Me.Btn_S999_Clear = New System.Windows.Forms.Button()
        Me.Button1 = New System.Windows.Forms.Button()
        Me.GroupBox_IDGCmd_Timeout = New System.Windows.Forms.GroupBox()
        Me.Label12 = New System.Windows.Forms.Label()
        Me.TextBox4_TxnTimeout = New System.Windows.Forms.TextBox()
        Me.Button_Timeout = New System.Windows.Forms.Button()
        Me.SplitContainer6 = New System.Windows.Forms.SplitContainer()
        Me.m_logRichText = New System.Windows.Forms.RichTextBox()
        Me.SplitContainer5 = New System.Windows.Forms.SplitContainer()
        Me.m_RTxtBox_001_UID = New System.Windows.Forms.RichTextBox()
        Me.m_RTxtBox_002_MifareBlock = New System.Windows.Forms.RichTextBox()
        Me.Timer1 = New System.Windows.Forms.Timer(Me.components)
        Me.ToolTip1 = New System.Windows.Forms.ToolTip(Me.components)
        Me.OpenFileDialog1 = New System.Windows.Forms.OpenFileDialog()
        Me.Panel1_Tier01.SuspendLayout()
        CType(Me.SplitContainer1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SplitContainer1.Panel1.SuspendLayout()
        Me.SplitContainer1.Panel2.SuspendLayout()
        Me.SplitContainer1.SuspendLayout()
        Me.Panel1.SuspendLayout()
        CType(Me.SplitContainer4, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SplitContainer4.Panel1.SuspendLayout()
        Me.SplitContainer4.Panel2.SuspendLayout()
        Me.SplitContainer4.SuspendLayout()
        CType(Me.PictureBox1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.SplitContainer9, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SplitContainer9.Panel1.SuspendLayout()
        Me.SplitContainer9.Panel2.SuspendLayout()
        Me.SplitContainer9.SuspendLayout()
        CType(Me.SplitContainer10, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SplitContainer10.Panel1.SuspendLayout()
        Me.SplitContainer10.Panel2.SuspendLayout()
        Me.SplitContainer10.SuspendLayout()
        CType(Me.SplitContainer3, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SplitContainer3.Panel1.SuspendLayout()
        Me.SplitContainer3.SuspendLayout()
        CType(Me.SplitContainer8, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SplitContainer8.Panel1.SuspendLayout()
        Me.SplitContainer8.Panel2.SuspendLayout()
        Me.SplitContainer8.SuspendLayout()
        Me.TabCtrl_CommIntf.SuspendLayout()
        Me.TabPg_Intf_USB.SuspendLayout()
        Me.Panel_S00_3_DevMode_Lst.SuspendLayout()
        Me.GroupBox_InterfaceMode.SuspendLayout()
        Me.TabPg_Intf_2_RS232.SuspendLayout()
        CType(Me.SplitContainer12, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SplitContainer12.Panel1.SuspendLayout()
        Me.SplitContainer12.Panel2.SuspendLayout()
        Me.SplitContainer12.SuspendLayout()
        Me.TabCtrl_EndMode.SuspendLayout()
        Me.TabPg_End_Rounds.SuspendLayout()
        Me.GrpBox_02_RndSel.SuspendLayout()
        Me.TabPg_End_TWait.SuspendLayout()
        Me.GrpBox_SeekCardType.SuspendLayout()
        Me.GrpBox_04_TxRx_ToutMs.SuspendLayout()
        CType(Me.NumUpDwn_Timeout_TxRx, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.SplitContainer11, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SplitContainer11.Panel1.SuspendLayout()
        Me.SplitContainer11.Panel2.SuspendLayout()
        Me.SplitContainer11.SuspendLayout()
        Me.Panel2.SuspendLayout()
        Me.GroupBox2.SuspendLayout()
        CType(Me.SplitContainer7, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SplitContainer7.Panel1.SuspendLayout()
        Me.SplitContainer7.Panel2.SuspendLayout()
        Me.SplitContainer7.SuspendLayout()
        CType(Me.SplitContainer2, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SplitContainer2.Panel1.SuspendLayout()
        Me.SplitContainer2.Panel2.SuspendLayout()
        Me.SplitContainer2.SuspendLayout()
        Me.GroupBox_IDGCmd_Timeout.SuspendLayout()
        CType(Me.SplitContainer6, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SplitContainer6.Panel1.SuspendLayout()
        Me.SplitContainer6.Panel2.SuspendLayout()
        Me.SplitContainer6.SuspendLayout()
        CType(Me.SplitContainer5, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SplitContainer5.Panel1.SuspendLayout()
        Me.SplitContainer5.Panel2.SuspendLayout()
        Me.SplitContainer5.SuspendLayout()
        Me.SuspendLayout()
        '
        'Panel1_Tier01
        '
        Me.Panel1_Tier01.Controls.Add(Me.SplitContainer1)
        Me.Panel1_Tier01.Dock = System.Windows.Forms.DockStyle.Fill
        Me.Panel1_Tier01.Location = New System.Drawing.Point(0, 0)
        Me.Panel1_Tier01.Name = "Panel1_Tier01"
        Me.Panel1_Tier01.Size = New System.Drawing.Size(1021, 781)
        Me.Panel1_Tier01.TabIndex = 0
        '
        'SplitContainer1
        '
        Me.SplitContainer1.Dock = System.Windows.Forms.DockStyle.Fill
        Me.SplitContainer1.FixedPanel = System.Windows.Forms.FixedPanel.Panel1
        Me.SplitContainer1.Location = New System.Drawing.Point(0, 0)
        Me.SplitContainer1.Name = "SplitContainer1"
        Me.SplitContainer1.Orientation = System.Windows.Forms.Orientation.Horizontal
        '
        'SplitContainer1.Panel1
        '
        Me.SplitContainer1.Panel1.AutoScroll = True
        Me.SplitContainer1.Panel1.Controls.Add(Me.Panel1)
        '
        'SplitContainer1.Panel2
        '
        Me.SplitContainer1.Panel2.AutoScroll = True
        Me.SplitContainer1.Panel2.Controls.Add(Me.Panel2)
        Me.SplitContainer1.Size = New System.Drawing.Size(1021, 781)
        Me.SplitContainer1.SplitterDistance = 153
        Me.SplitContainer1.SplitterWidth = 5
        Me.SplitContainer1.TabIndex = 0
        '
        'Panel1
        '
        Me.Panel1.Controls.Add(Me.SplitContainer4)
        Me.Panel1.Dock = System.Windows.Forms.DockStyle.Fill
        Me.Panel1.Location = New System.Drawing.Point(0, 0)
        Me.Panel1.Name = "Panel1"
        Me.Panel1.Size = New System.Drawing.Size(1021, 153)
        Me.Panel1.TabIndex = 0
        '
        'SplitContainer4
        '
        Me.SplitContainer4.Dock = System.Windows.Forms.DockStyle.Fill
        Me.SplitContainer4.FixedPanel = System.Windows.Forms.FixedPanel.Panel1
        Me.SplitContainer4.IsSplitterFixed = True
        Me.SplitContainer4.Location = New System.Drawing.Point(0, 0)
        Me.SplitContainer4.Name = "SplitContainer4"
        '
        'SplitContainer4.Panel1
        '
        Me.SplitContainer4.Panel1.Controls.Add(Me.PictureBox1)
        '
        'SplitContainer4.Panel2
        '
        Me.SplitContainer4.Panel2.Controls.Add(Me.SplitContainer9)
        Me.SplitContainer4.Size = New System.Drawing.Size(1021, 153)
        Me.SplitContainer4.SplitterDistance = 145
        Me.SplitContainer4.TabIndex = 1
        '
        'PictureBox1
        '
        Me.PictureBox1.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D
        Me.PictureBox1.Image = Global.Main.My.Resources.Resources.logo
        Me.PictureBox1.Location = New System.Drawing.Point(3, 12)
        Me.PictureBox1.Name = "PictureBox1"
        Me.PictureBox1.Size = New System.Drawing.Size(132, 64)
        Me.PictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        Me.PictureBox1.TabIndex = 0
        Me.PictureBox1.TabStop = False
        '
        'SplitContainer9
        '
        Me.SplitContainer9.Dock = System.Windows.Forms.DockStyle.Fill
        Me.SplitContainer9.FixedPanel = System.Windows.Forms.FixedPanel.Panel2
        Me.SplitContainer9.Location = New System.Drawing.Point(0, 0)
        Me.SplitContainer9.Name = "SplitContainer9"
        Me.SplitContainer9.Orientation = System.Windows.Forms.Orientation.Horizontal
        '
        'SplitContainer9.Panel1
        '
        Me.SplitContainer9.Panel1.Controls.Add(Me.SplitContainer10)
        '
        'SplitContainer9.Panel2
        '
        Me.SplitContainer9.Panel2.Controls.Add(Me.ProgressBar1)
        Me.SplitContainer9.Size = New System.Drawing.Size(872, 153)
        Me.SplitContainer9.SplitterDistance = 124
        Me.SplitContainer9.TabIndex = 0
        '
        'SplitContainer10
        '
        Me.SplitContainer10.Dock = System.Windows.Forms.DockStyle.Fill
        Me.SplitContainer10.FixedPanel = System.Windows.Forms.FixedPanel.Panel2
        Me.SplitContainer10.Location = New System.Drawing.Point(0, 0)
        Me.SplitContainer10.Name = "SplitContainer10"
        '
        'SplitContainer10.Panel1
        '
        Me.SplitContainer10.Panel1.Controls.Add(Me.SplitContainer3)
        '
        'SplitContainer10.Panel2
        '
        Me.SplitContainer10.Panel2.Controls.Add(Me.SplitContainer11)
        Me.SplitContainer10.Size = New System.Drawing.Size(872, 124)
        Me.SplitContainer10.SplitterDistance = 487
        Me.SplitContainer10.TabIndex = 0
        '
        'SplitContainer3
        '
        Me.SplitContainer3.Dock = System.Windows.Forms.DockStyle.Fill
        Me.SplitContainer3.FixedPanel = System.Windows.Forms.FixedPanel.Panel2
        Me.SplitContainer3.Location = New System.Drawing.Point(0, 0)
        Me.SplitContainer3.Name = "SplitContainer3"
        '
        'SplitContainer3.Panel1
        '
        Me.SplitContainer3.Panel1.Controls.Add(Me.SplitContainer8)
        Me.SplitContainer3.Size = New System.Drawing.Size(487, 124)
        Me.SplitContainer3.SplitterDistance = 457
        Me.SplitContainer3.TabIndex = 2
        '
        'SplitContainer8
        '
        Me.SplitContainer8.Dock = System.Windows.Forms.DockStyle.Fill
        Me.SplitContainer8.Location = New System.Drawing.Point(0, 0)
        Me.SplitContainer8.Name = "SplitContainer8"
        '
        'SplitContainer8.Panel1
        '
        Me.SplitContainer8.Panel1.Controls.Add(Me.TabCtrl_CommIntf)
        '
        'SplitContainer8.Panel2
        '
        Me.SplitContainer8.Panel2.Controls.Add(Me.SplitContainer12)
        Me.SplitContainer8.Size = New System.Drawing.Size(457, 124)
        Me.SplitContainer8.SplitterDistance = 177
        Me.SplitContainer8.TabIndex = 3
        '
        'TabCtrl_CommIntf
        '
        Me.TabCtrl_CommIntf.Controls.Add(Me.TabPg_Intf_USB)
        Me.TabCtrl_CommIntf.Controls.Add(Me.TabPg_Intf_2_RS232)
        Me.TabCtrl_CommIntf.Dock = System.Windows.Forms.DockStyle.Fill
        Me.TabCtrl_CommIntf.Location = New System.Drawing.Point(0, 0)
        Me.TabCtrl_CommIntf.Name = "TabCtrl_CommIntf"
        Me.TabCtrl_CommIntf.SelectedIndex = 0
        Me.TabCtrl_CommIntf.Size = New System.Drawing.Size(177, 124)
        Me.TabCtrl_CommIntf.TabIndex = 1
        '
        'TabPg_Intf_USB
        '
        Me.TabPg_Intf_USB.Controls.Add(Me.Panel_S00_3_DevMode_Lst)
        Me.TabPg_Intf_USB.Location = New System.Drawing.Point(4, 22)
        Me.TabPg_Intf_USB.Name = "TabPg_Intf_USB"
        Me.TabPg_Intf_USB.Padding = New System.Windows.Forms.Padding(3)
        Me.TabPg_Intf_USB.Size = New System.Drawing.Size(169, 98)
        Me.TabPg_Intf_USB.TabIndex = 0
        Me.TabPg_Intf_USB.Text = "USB"
        Me.TabPg_Intf_USB.UseVisualStyleBackColor = True
        '
        'Panel_S00_3_DevMode_Lst
        '
        Me.Panel_S00_3_DevMode_Lst.BackColor = System.Drawing.Color.FromArgb(CType(CType(192, Byte), Integer), CType(CType(255, Byte), Integer), CType(CType(255, Byte), Integer))
        Me.Panel_S00_3_DevMode_Lst.Controls.Add(Me.GroupBox_InterfaceMode)
        Me.Panel_S00_3_DevMode_Lst.Dock = System.Windows.Forms.DockStyle.Fill
        Me.Panel_S00_3_DevMode_Lst.Location = New System.Drawing.Point(3, 3)
        Me.Panel_S00_3_DevMode_Lst.Name = "Panel_S00_3_DevMode_Lst"
        Me.Panel_S00_3_DevMode_Lst.Size = New System.Drawing.Size(163, 92)
        Me.Panel_S00_3_DevMode_Lst.TabIndex = 0
        '
        'GroupBox_InterfaceMode
        '
        Me.GroupBox_InterfaceMode.Controls.Add(Me.RdoBtn_S00_01_USB_KB_WEDGE)
        Me.GroupBox_InterfaceMode.Controls.Add(Me.RdoBtn_S00_01_USB_HID)
        Me.GroupBox_InterfaceMode.Font = New System.Drawing.Font("PMingLiU", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(136, Byte))
        Me.GroupBox_InterfaceMode.Location = New System.Drawing.Point(3, 8)
        Me.GroupBox_InterfaceMode.Name = "GroupBox_InterfaceMode"
        Me.GroupBox_InterfaceMode.Size = New System.Drawing.Size(126, 75)
        Me.GroupBox_InterfaceMode.TabIndex = 0
        Me.GroupBox_InterfaceMode.TabStop = False
        Me.GroupBox_InterfaceMode.Text = "Interface"
        '
        'RdoBtn_S00_01_USB_KB_WEDGE
        '
        Me.RdoBtn_S00_01_USB_KB_WEDGE.AutoSize = True
        Me.RdoBtn_S00_01_USB_KB_WEDGE.Location = New System.Drawing.Point(6, 49)
        Me.RdoBtn_S00_01_USB_KB_WEDGE.Name = "RdoBtn_S00_01_USB_KB_WEDGE"
        Me.RdoBtn_S00_01_USB_KB_WEDGE.Size = New System.Drawing.Size(81, 20)
        Me.RdoBtn_S00_01_USB_KB_WEDGE.TabIndex = 1
        Me.RdoBtn_S00_01_USB_KB_WEDGE.Text = "USB-KB"
        Me.RdoBtn_S00_01_USB_KB_WEDGE.UseVisualStyleBackColor = True
        '
        'RdoBtn_S00_01_USB_HID
        '
        Me.RdoBtn_S00_01_USB_HID.AutoSize = True
        Me.RdoBtn_S00_01_USB_HID.Checked = True
        Me.RdoBtn_S00_01_USB_HID.Location = New System.Drawing.Point(6, 25)
        Me.RdoBtn_S00_01_USB_HID.Name = "RdoBtn_S00_01_USB_HID"
        Me.RdoBtn_S00_01_USB_HID.Size = New System.Drawing.Size(87, 20)
        Me.RdoBtn_S00_01_USB_HID.TabIndex = 0
        Me.RdoBtn_S00_01_USB_HID.TabStop = True
        Me.RdoBtn_S00_01_USB_HID.Text = "USB-HID"
        Me.RdoBtn_S00_01_USB_HID.UseVisualStyleBackColor = True
        '
        'TabPg_Intf_2_RS232
        '
        Me.TabPg_Intf_2_RS232.Controls.Add(Me.Label3)
        Me.TabPg_Intf_2_RS232.Controls.Add(Me.Label4)
        Me.TabPg_Intf_2_RS232.Controls.Add(Me.ComboBox_PortSel_COM)
        Me.TabPg_Intf_2_RS232.Controls.Add(Me.ComboBox_PortSel_Baudrate)
        Me.TabPg_Intf_2_RS232.Location = New System.Drawing.Point(4, 22)
        Me.TabPg_Intf_2_RS232.Name = "TabPg_Intf_2_RS232"
        Me.TabPg_Intf_2_RS232.Padding = New System.Windows.Forms.Padding(3)
        Me.TabPg_Intf_2_RS232.Size = New System.Drawing.Size(169, 98)
        Me.TabPg_Intf_2_RS232.TabIndex = 1
        Me.TabPg_Intf_2_RS232.Text = "RS232"
        Me.TabPg_Intf_2_RS232.UseVisualStyleBackColor = True
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.Font = New System.Drawing.Font("PMingLiU", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(136, Byte))
        Me.Label3.Location = New System.Drawing.Point(7, 8)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(37, 16)
        Me.Label3.TabIndex = 2
        Me.Label3.Text = "Port:"
        '
        'Label4
        '
        Me.Label4.AutoSize = True
        Me.Label4.Font = New System.Drawing.Font("PMingLiU", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(136, Byte))
        Me.Label4.Location = New System.Drawing.Point(7, 39)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(68, 16)
        Me.Label4.TabIndex = 3
        Me.Label4.Text = "Baudrate:"
        '
        'ComboBox_PortSel_COM
        '
        Me.ComboBox_PortSel_COM.Font = New System.Drawing.Font("PMingLiU", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(136, Byte))
        Me.ComboBox_PortSel_COM.FormattingEnabled = True
        Me.ComboBox_PortSel_COM.Items.AddRange(New Object() {"COM1", "COM2", "COM3", "COM4", "COM5", "COM6", "COM7", "COM8", "COM9", "COM10"})
        Me.ComboBox_PortSel_COM.Location = New System.Drawing.Point(76, 8)
        Me.ComboBox_PortSel_COM.Margin = New System.Windows.Forms.Padding(3, 5, 3, 5)
        Me.ComboBox_PortSel_COM.Name = "ComboBox_PortSel_COM"
        Me.ComboBox_PortSel_COM.Size = New System.Drawing.Size(72, 24)
        Me.ComboBox_PortSel_COM.TabIndex = 4
        Me.ComboBox_PortSel_COM.Text = "COM1"
        '
        'ComboBox_PortSel_Baudrate
        '
        Me.ComboBox_PortSel_Baudrate.Font = New System.Drawing.Font("PMingLiU", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(136, Byte))
        Me.ComboBox_PortSel_Baudrate.FormattingEnabled = True
        Me.ComboBox_PortSel_Baudrate.Items.AddRange(New Object() {"9600", "19200", "38400", "57600", "115200"})
        Me.ComboBox_PortSel_Baudrate.Location = New System.Drawing.Point(76, 37)
        Me.ComboBox_PortSel_Baudrate.Margin = New System.Windows.Forms.Padding(3, 5, 3, 5)
        Me.ComboBox_PortSel_Baudrate.Name = "ComboBox_PortSel_Baudrate"
        Me.ComboBox_PortSel_Baudrate.Size = New System.Drawing.Size(72, 24)
        Me.ComboBox_PortSel_Baudrate.TabIndex = 5
        Me.ComboBox_PortSel_Baudrate.Text = "115200"
        '
        'SplitContainer12
        '
        Me.SplitContainer12.Dock = System.Windows.Forms.DockStyle.Fill
        Me.SplitContainer12.Location = New System.Drawing.Point(0, 0)
        Me.SplitContainer12.Name = "SplitContainer12"
        '
        'SplitContainer12.Panel1
        '
        Me.SplitContainer12.Panel1.Controls.Add(Me.TabCtrl_EndMode)
        '
        'SplitContainer12.Panel2
        '
        Me.SplitContainer12.Panel2.Controls.Add(Me.GrpBox_SeekCardType)
        Me.SplitContainer12.Size = New System.Drawing.Size(276, 124)
        Me.SplitContainer12.SplitterDistance = 157
        Me.SplitContainer12.TabIndex = 0
        '
        'TabCtrl_EndMode
        '
        Me.TabCtrl_EndMode.Controls.Add(Me.TabPg_End_Rounds)
        Me.TabCtrl_EndMode.Controls.Add(Me.TabPg_End_TWait)
        Me.TabCtrl_EndMode.Dock = System.Windows.Forms.DockStyle.Fill
        Me.TabCtrl_EndMode.Location = New System.Drawing.Point(0, 0)
        Me.TabCtrl_EndMode.Name = "TabCtrl_EndMode"
        Me.TabCtrl_EndMode.SelectedIndex = 0
        Me.TabCtrl_EndMode.Size = New System.Drawing.Size(157, 124)
        Me.TabCtrl_EndMode.TabIndex = 2
        '
        'TabPg_End_Rounds
        '
        Me.TabPg_End_Rounds.Controls.Add(Me.GrpBox_02_RndSel)
        Me.TabPg_End_Rounds.Location = New System.Drawing.Point(4, 22)
        Me.TabPg_End_Rounds.Name = "TabPg_End_Rounds"
        Me.TabPg_End_Rounds.Padding = New System.Windows.Forms.Padding(3)
        Me.TabPg_End_Rounds.Size = New System.Drawing.Size(149, 98)
        Me.TabPg_End_Rounds.TabIndex = 0
        Me.TabPg_End_Rounds.Text = "Rounds"
        Me.TabPg_End_Rounds.UseVisualStyleBackColor = True
        '
        'GrpBox_02_RndSel
        '
        Me.GrpBox_02_RndSel.BackColor = System.Drawing.Color.FromArgb(CType(CType(192, Byte), Integer), CType(CType(255, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.GrpBox_02_RndSel.Controls.Add(Me.Label2)
        Me.GrpBox_02_RndSel.Controls.Add(Me.Rnd_02_Btn_Get_Rnd)
        Me.GrpBox_02_RndSel.Controls.Add(Me.Rnd_01_Cmbox_Sel_Rnd)
        Me.GrpBox_02_RndSel.Location = New System.Drawing.Point(3, 3)
        Me.GrpBox_02_RndSel.Name = "GrpBox_02_RndSel"
        Me.GrpBox_02_RndSel.Size = New System.Drawing.Size(176, 93)
        Me.GrpBox_02_RndSel.TabIndex = 1
        Me.GrpBox_02_RndSel.TabStop = False
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Font = New System.Drawing.Font("PMingLiU", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(136, Byte))
        Me.Label2.Location = New System.Drawing.Point(78, 29)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(56, 16)
        Me.Label2.TabIndex = 2
        Me.Label2.Text = "Rounds"
        '
        'Rnd_02_Btn_Get_Rnd
        '
        Me.Rnd_02_Btn_Get_Rnd.Location = New System.Drawing.Point(6, 59)
        Me.Rnd_02_Btn_Get_Rnd.Name = "Rnd_02_Btn_Get_Rnd"
        Me.Rnd_02_Btn_Get_Rnd.Size = New System.Drawing.Size(80, 23)
        Me.Rnd_02_Btn_Get_Rnd.TabIndex = 1
        Me.ToolTip1.SetToolTip(Me.Rnd_02_Btn_Get_Rnd, "1.Show Customized Num Of Rounds" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10) & "2.Hit this Button to Input Again !!")
        Me.Rnd_02_Btn_Get_Rnd.UseVisualStyleBackColor = True
        Me.Rnd_02_Btn_Get_Rnd.Visible = False
        '
        'Rnd_01_Cmbox_Sel_Rnd
        '
        Me.Rnd_01_Cmbox_Sel_Rnd.Font = New System.Drawing.Font("PMingLiU", 14.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(136, Byte))
        Me.Rnd_01_Cmbox_Sel_Rnd.FormattingEnabled = True
        Me.Rnd_01_Cmbox_Sel_Rnd.Items.AddRange(New Object() {"50", "100", "200", "500", "1000", "Manual"})
        Me.Rnd_01_Cmbox_Sel_Rnd.Location = New System.Drawing.Point(6, 26)
        Me.Rnd_01_Cmbox_Sel_Rnd.Name = "Rnd_01_Cmbox_Sel_Rnd"
        Me.Rnd_01_Cmbox_Sel_Rnd.Size = New System.Drawing.Size(66, 27)
        Me.Rnd_01_Cmbox_Sel_Rnd.TabIndex = 0
        Me.Rnd_01_Cmbox_Sel_Rnd.Text = "50"
        '
        'TabPg_End_TWait
        '
        Me.TabPg_End_TWait.Controls.Add(Me.m_CmbBox_MifareRunMode)
        Me.TabPg_End_TWait.Controls.Add(Me.Label1)
        Me.TabPg_End_TWait.Controls.Add(Me.m_CBox_End_TWaitTime)
        Me.TabPg_End_TWait.Location = New System.Drawing.Point(4, 22)
        Me.TabPg_End_TWait.Name = "TabPg_End_TWait"
        Me.TabPg_End_TWait.Padding = New System.Windows.Forms.Padding(3)
        Me.TabPg_End_TWait.Size = New System.Drawing.Size(149, 98)
        Me.TabPg_End_TWait.TabIndex = 1
        Me.TabPg_End_TWait.Text = "Time Wait"
        Me.TabPg_End_TWait.UseVisualStyleBackColor = True
        '
        'm_CmbBox_MifareRunMode
        '
        Me.m_CmbBox_MifareRunMode.BackColor = System.Drawing.Color.FromArgb(CType(CType(192, Byte), Integer), CType(CType(255, Byte), Integer), CType(CType(255, Byte), Integer))
        Me.m_CmbBox_MifareRunMode.Font = New System.Drawing.Font("PMingLiU", 14.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(136, Byte))
        Me.m_CmbBox_MifareRunMode.FormattingEnabled = True
        Me.m_CmbBox_MifareRunMode.Items.AddRange(New Object() {"01 - Default", "02 - Cust Script"})
        Me.m_CmbBox_MifareRunMode.Location = New System.Drawing.Point(6, 17)
        Me.m_CmbBox_MifareRunMode.Name = "m_CmbBox_MifareRunMode"
        Me.m_CmbBox_MifareRunMode.Size = New System.Drawing.Size(129, 27)
        Me.m_CmbBox_MifareRunMode.TabIndex = 2
        Me.m_CmbBox_MifareRunMode.Text = "01 - Default"
        Me.ToolTip1.SetToolTip(Me.m_CmbBox_MifareRunMode, "01 - Default, Mifare Read" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10) & "02 - Cust Script, Docklight Log")
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Font = New System.Drawing.Font("PMingLiU", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(136, Byte))
        Me.Label1.Location = New System.Drawing.Point(81, 63)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(60, 16)
        Me.Label1.TabIndex = 1
        Me.Label1.Text = "Seconds"
        '
        'm_CBox_End_TWaitTime
        '
        Me.m_CBox_End_TWaitTime.BackColor = System.Drawing.Color.FromArgb(CType(CType(255, Byte), Integer), CType(CType(192, Byte), Integer), CType(CType(255, Byte), Integer))
        Me.m_CBox_End_TWaitTime.Font = New System.Drawing.Font("PMingLiU", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(136, Byte))
        Me.m_CBox_End_TWaitTime.FormattingEnabled = True
        Me.m_CBox_End_TWaitTime.Items.AddRange(New Object() {"R1", "R10", "30", "60", "180", "600", "900", "1800", "2700", "3600"})
        Me.m_CBox_End_TWaitTime.Location = New System.Drawing.Point(6, 60)
        Me.m_CBox_End_TWaitTime.Name = "m_CBox_End_TWaitTime"
        Me.m_CBox_End_TWaitTime.Size = New System.Drawing.Size(71, 24)
        Me.m_CBox_End_TWaitTime.TabIndex = 0
        Me.m_CBox_End_TWaitTime.Text = "60"
        '
        'GrpBox_SeekCardType
        '
        Me.GrpBox_SeekCardType.BackColor = System.Drawing.Color.FromArgb(CType(CType(192, Byte), Integer), CType(CType(255, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.GrpBox_SeekCardType.Controls.Add(Me.GrpBox_04_TxRx_ToutMs)
        Me.GrpBox_SeekCardType.Controls.Add(Me.cbox_SeekCardType)
        Me.GrpBox_SeekCardType.Dock = System.Windows.Forms.DockStyle.Fill
        Me.GrpBox_SeekCardType.Location = New System.Drawing.Point(0, 0)
        Me.GrpBox_SeekCardType.Name = "GrpBox_SeekCardType"
        Me.GrpBox_SeekCardType.Size = New System.Drawing.Size(115, 124)
        Me.GrpBox_SeekCardType.TabIndex = 0
        Me.GrpBox_SeekCardType.TabStop = False
        Me.GrpBox_SeekCardType.Text = "Tx / Rx Setting"
        '
        'GrpBox_04_TxRx_ToutMs
        '
        Me.GrpBox_04_TxRx_ToutMs.BackColor = System.Drawing.Color.FromArgb(CType(CType(255, Byte), Integer), CType(CType(192, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.GrpBox_04_TxRx_ToutMs.Controls.Add(Me.NumUpDwn_Timeout_TxRx)
        Me.GrpBox_04_TxRx_ToutMs.Location = New System.Drawing.Point(3, 19)
        Me.GrpBox_04_TxRx_ToutMs.Name = "GrpBox_04_TxRx_ToutMs"
        Me.GrpBox_04_TxRx_ToutMs.Size = New System.Drawing.Size(101, 58)
        Me.GrpBox_04_TxRx_ToutMs.TabIndex = 1
        Me.GrpBox_04_TxRx_ToutMs.TabStop = False
        Me.GrpBox_04_TxRx_ToutMs.Text = "Timeout, ms"
        Me.ToolTip1.SetToolTip(Me.GrpBox_04_TxRx_ToutMs, "There is no more")
        '
        'NumUpDwn_Timeout_TxRx
        '
        Me.NumUpDwn_Timeout_TxRx.Font = New System.Drawing.Font("PMingLiU", 14.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(136, Byte))
        Me.NumUpDwn_Timeout_TxRx.Location = New System.Drawing.Point(6, 21)
        Me.NumUpDwn_Timeout_TxRx.Maximum = New Decimal(New Integer() {10000, 0, 0, 0})
        Me.NumUpDwn_Timeout_TxRx.Minimum = New Decimal(New Integer() {10, 0, 0, 0})
        Me.NumUpDwn_Timeout_TxRx.Name = "NumUpDwn_Timeout_TxRx"
        Me.NumUpDwn_Timeout_TxRx.Size = New System.Drawing.Size(94, 30)
        Me.NumUpDwn_Timeout_TxRx.TabIndex = 0
        Me.NumUpDwn_Timeout_TxRx.Value = New Decimal(New Integer() {200, 0, 0, 0})
        '
        'cbox_SeekCardType
        '
        Me.cbox_SeekCardType.BackColor = System.Drawing.Color.White
        Me.cbox_SeekCardType.Font = New System.Drawing.Font("PMingLiU", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(136, Byte))
        Me.cbox_SeekCardType.FormattingEnabled = True
        Me.cbox_SeekCardType.Items.AddRange(New Object() {"A", "B", "C", "A+ B", "B+ C", "A+ C", "A+ B+ C"})
        Me.cbox_SeekCardType.Location = New System.Drawing.Point(3, 82)
        Me.cbox_SeekCardType.Name = "cbox_SeekCardType"
        Me.cbox_SeekCardType.Size = New System.Drawing.Size(98, 24)
        Me.cbox_SeekCardType.TabIndex = 0
        Me.cbox_SeekCardType.Text = "A+ B+ C"
        '
        'SplitContainer11
        '
        Me.SplitContainer11.Dock = System.Windows.Forms.DockStyle.Fill
        Me.SplitContainer11.FixedPanel = System.Windows.Forms.FixedPanel.Panel1
        Me.SplitContainer11.Location = New System.Drawing.Point(0, 0)
        Me.SplitContainer11.Name = "SplitContainer11"
        '
        'SplitContainer11.Panel1
        '
        Me.SplitContainer11.Panel1.AutoScroll = True
        Me.SplitContainer11.Panel1.Controls.Add(Me.Btn_S00_Connect_Start)
        '
        'SplitContainer11.Panel2
        '
        Me.SplitContainer11.Panel2.Controls.Add(Me.Btn_S00_Stop_Close)
        Me.SplitContainer11.Size = New System.Drawing.Size(381, 124)
        Me.SplitContainer11.SplitterDistance = 182
        Me.SplitContainer11.TabIndex = 0
        '
        'Btn_S00_Connect_Start
        '
        Me.Btn_S00_Connect_Start.Dock = System.Windows.Forms.DockStyle.Fill
        Me.Btn_S00_Connect_Start.Font = New System.Drawing.Font("PMingLiU", 15.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(136, Byte))
        Me.Btn_S00_Connect_Start.Location = New System.Drawing.Point(0, 0)
        Me.Btn_S00_Connect_Start.Name = "Btn_S00_Connect_Start"
        Me.Btn_S00_Connect_Start.Size = New System.Drawing.Size(182, 124)
        Me.Btn_S00_Connect_Start.TabIndex = 0
        Me.Btn_S00_Connect_Start.Text = "START (&S)"
        Me.Btn_S00_Connect_Start.UseVisualStyleBackColor = True
        '
        'Btn_S00_Stop_Close
        '
        Me.Btn_S00_Stop_Close.Dock = System.Windows.Forms.DockStyle.Fill
        Me.Btn_S00_Stop_Close.Font = New System.Drawing.Font("PMingLiU", 15.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(136, Byte))
        Me.Btn_S00_Stop_Close.Location = New System.Drawing.Point(0, 0)
        Me.Btn_S00_Stop_Close.Name = "Btn_S00_Stop_Close"
        Me.Btn_S00_Stop_Close.Size = New System.Drawing.Size(195, 124)
        Me.Btn_S00_Stop_Close.TabIndex = 1
        Me.Btn_S00_Stop_Close.Text = "CLOSE (&C)"
        Me.Btn_S00_Stop_Close.UseVisualStyleBackColor = True
        '
        'ProgressBar1
        '
        Me.ProgressBar1.Dock = System.Windows.Forms.DockStyle.Fill
        Me.ProgressBar1.Location = New System.Drawing.Point(0, 0)
        Me.ProgressBar1.Name = "ProgressBar1"
        Me.ProgressBar1.Size = New System.Drawing.Size(872, 25)
        Me.ProgressBar1.TabIndex = 0
        Me.ProgressBar1.Visible = False
        '
        'Panel2
        '
        Me.Panel2.Controls.Add(Me.GroupBox2)
        Me.Panel2.Dock = System.Windows.Forms.DockStyle.Fill
        Me.Panel2.Location = New System.Drawing.Point(0, 0)
        Me.Panel2.Name = "Panel2"
        Me.Panel2.Size = New System.Drawing.Size(1021, 623)
        Me.Panel2.TabIndex = 0
        '
        'GroupBox2
        '
        Me.GroupBox2.Controls.Add(Me.SplitContainer7)
        Me.GroupBox2.Dock = System.Windows.Forms.DockStyle.Fill
        Me.GroupBox2.Location = New System.Drawing.Point(0, 0)
        Me.GroupBox2.Name = "GroupBox2"
        Me.GroupBox2.Size = New System.Drawing.Size(1021, 623)
        Me.GroupBox2.TabIndex = 1
        Me.GroupBox2.TabStop = False
        Me.GroupBox2.Text = "S.999 - Log Window"
        '
        'SplitContainer7
        '
        Me.SplitContainer7.Dock = System.Windows.Forms.DockStyle.Fill
        Me.SplitContainer7.FixedPanel = System.Windows.Forms.FixedPanel.Panel1
        Me.SplitContainer7.Location = New System.Drawing.Point(3, 18)
        Me.SplitContainer7.Name = "SplitContainer7"
        '
        'SplitContainer7.Panel1
        '
        Me.SplitContainer7.Panel1.Controls.Add(Me.SplitContainer2)
        Me.SplitContainer7.Panel1.Controls.Add(Me.GroupBox_IDGCmd_Timeout)
        '
        'SplitContainer7.Panel2
        '
        Me.SplitContainer7.Panel2.Controls.Add(Me.SplitContainer6)
        Me.SplitContainer7.Size = New System.Drawing.Size(1015, 602)
        Me.SplitContainer7.SplitterDistance = 111
        Me.SplitContainer7.TabIndex = 1
        '
        'SplitContainer2
        '
        Me.SplitContainer2.Dock = System.Windows.Forms.DockStyle.Top
        Me.SplitContainer2.FixedPanel = System.Windows.Forms.FixedPanel.Panel1
        Me.SplitContainer2.IsSplitterFixed = True
        Me.SplitContainer2.Location = New System.Drawing.Point(0, 0)
        Me.SplitContainer2.Name = "SplitContainer2"
        Me.SplitContainer2.Orientation = System.Windows.Forms.Orientation.Horizontal
        '
        'SplitContainer2.Panel1
        '
        Me.SplitContainer2.Panel1.Controls.Add(Me.Btn_S999_Clear)
        '
        'SplitContainer2.Panel2
        '
        Me.SplitContainer2.Panel2.Controls.Add(Me.Button1)
        Me.SplitContainer2.Size = New System.Drawing.Size(111, 206)
        Me.SplitContainer2.TabIndex = 8
        '
        'Btn_S999_Clear
        '
        Me.Btn_S999_Clear.Dock = System.Windows.Forms.DockStyle.Fill
        Me.Btn_S999_Clear.Font = New System.Drawing.Font("PMingLiU", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(136, Byte))
        Me.Btn_S999_Clear.Location = New System.Drawing.Point(0, 0)
        Me.Btn_S999_Clear.Name = "Btn_S999_Clear"
        Me.Btn_S999_Clear.Size = New System.Drawing.Size(111, 50)
        Me.Btn_S999_Clear.TabIndex = 0
        Me.Btn_S999_Clear.Text = "Clear (&R)"
        Me.Btn_S999_Clear.UseVisualStyleBackColor = True
        '
        'Button1
        '
        Me.Button1.Dock = System.Windows.Forms.DockStyle.Bottom
        Me.Button1.Font = New System.Drawing.Font("PMingLiU", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(136, Byte))
        Me.Button1.Location = New System.Drawing.Point(0, 44)
        Me.Button1.Name = "Button1"
        Me.Button1.Size = New System.Drawing.Size(111, 108)
        Me.Button1.TabIndex = 7
        Me.Button1.Text = "Copy"
        Me.Button1.UseVisualStyleBackColor = True
        Me.Button1.Visible = False
        '
        'GroupBox_IDGCmd_Timeout
        '
        Me.GroupBox_IDGCmd_Timeout.BackColor = System.Drawing.Color.FromArgb(CType(CType(255, Byte), Integer), CType(CType(255, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.GroupBox_IDGCmd_Timeout.Controls.Add(Me.Label12)
        Me.GroupBox_IDGCmd_Timeout.Controls.Add(Me.TextBox4_TxnTimeout)
        Me.GroupBox_IDGCmd_Timeout.Controls.Add(Me.Button_Timeout)
        Me.GroupBox_IDGCmd_Timeout.Location = New System.Drawing.Point(3, 213)
        Me.GroupBox_IDGCmd_Timeout.Margin = New System.Windows.Forms.Padding(3, 4, 3, 4)
        Me.GroupBox_IDGCmd_Timeout.Name = "GroupBox_IDGCmd_Timeout"
        Me.GroupBox_IDGCmd_Timeout.Padding = New System.Windows.Forms.Padding(3, 4, 3, 4)
        Me.GroupBox_IDGCmd_Timeout.Size = New System.Drawing.Size(105, 95)
        Me.GroupBox_IDGCmd_Timeout.TabIndex = 6
        Me.GroupBox_IDGCmd_Timeout.TabStop = False
        Me.GroupBox_IDGCmd_Timeout.Text = "Cmd Timeout"
        Me.GroupBox_IDGCmd_Timeout.Visible = False
        '
        'Label12
        '
        Me.Label12.AutoSize = True
        Me.Label12.Location = New System.Drawing.Point(26, 62)
        Me.Label12.Name = "Label12"
        Me.Label12.Size = New System.Drawing.Size(41, 12)
        Me.Label12.TabIndex = 10
        Me.Label12.Text = "seconds"
        '
        'TextBox4_TxnTimeout
        '
        Me.TextBox4_TxnTimeout.Location = New System.Drawing.Point(7, 21)
        Me.TextBox4_TxnTimeout.Margin = New System.Windows.Forms.Padding(3, 4, 3, 4)
        Me.TextBox4_TxnTimeout.Name = "TextBox4_TxnTimeout"
        Me.TextBox4_TxnTimeout.Size = New System.Drawing.Size(60, 22)
        Me.TextBox4_TxnTimeout.TabIndex = 9
        Me.TextBox4_TxnTimeout.Text = "10"
        Me.TextBox4_TxnTimeout.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'Button_Timeout
        '
        Me.Button_Timeout.Location = New System.Drawing.Point(7, 50)
        Me.Button_Timeout.Name = "Button_Timeout"
        Me.Button_Timeout.Size = New System.Drawing.Size(72, 36)
        Me.Button_Timeout.TabIndex = 18
        Me.Button_Timeout.UseVisualStyleBackColor = True
        Me.Button_Timeout.Visible = False
        '
        'SplitContainer6
        '
        Me.SplitContainer6.Dock = System.Windows.Forms.DockStyle.Fill
        Me.SplitContainer6.Location = New System.Drawing.Point(0, 0)
        Me.SplitContainer6.Name = "SplitContainer6"
        Me.SplitContainer6.Orientation = System.Windows.Forms.Orientation.Horizontal
        '
        'SplitContainer6.Panel1
        '
        Me.SplitContainer6.Panel1.Controls.Add(Me.m_logRichText)
        '
        'SplitContainer6.Panel2
        '
        Me.SplitContainer6.Panel2.Controls.Add(Me.SplitContainer5)
        Me.SplitContainer6.Size = New System.Drawing.Size(900, 602)
        Me.SplitContainer6.SplitterDistance = 393
        Me.SplitContainer6.TabIndex = 2
        '
        'm_logRichText
        '
        Me.m_logRichText.BackColor = System.Drawing.Color.Black
        Me.m_logRichText.Dock = System.Windows.Forms.DockStyle.Fill
        Me.m_logRichText.Font = New System.Drawing.Font("Courier New", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.m_logRichText.ForeColor = System.Drawing.Color.Lime
        Me.m_logRichText.Location = New System.Drawing.Point(0, 0)
        Me.m_logRichText.Name = "m_logRichText"
        Me.m_logRichText.Size = New System.Drawing.Size(900, 393)
        Me.m_logRichText.TabIndex = 0
        Me.m_logRichText.Text = resources.GetString("m_logRichText.Text")
        '
        'SplitContainer5
        '
        Me.SplitContainer5.Dock = System.Windows.Forms.DockStyle.Fill
        Me.SplitContainer5.Location = New System.Drawing.Point(0, 0)
        Me.SplitContainer5.Name = "SplitContainer5"
        Me.SplitContainer5.Orientation = System.Windows.Forms.Orientation.Horizontal
        '
        'SplitContainer5.Panel1
        '
        Me.SplitContainer5.Panel1.Controls.Add(Me.m_RTxtBox_001_UID)
        '
        'SplitContainer5.Panel2
        '
        Me.SplitContainer5.Panel2.Controls.Add(Me.m_RTxtBox_002_MifareBlock)
        Me.SplitContainer5.Size = New System.Drawing.Size(900, 205)
        Me.SplitContainer5.SplitterDistance = 106
        Me.SplitContainer5.TabIndex = 1
        '
        'm_RTxtBox_001_UID
        '
        Me.m_RTxtBox_001_UID.BackColor = System.Drawing.Color.Black
        Me.m_RTxtBox_001_UID.Dock = System.Windows.Forms.DockStyle.Fill
        Me.m_RTxtBox_001_UID.Font = New System.Drawing.Font("PMingLiU", 14.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(136, Byte))
        Me.m_RTxtBox_001_UID.ForeColor = System.Drawing.Color.FromArgb(CType(CType(192, Byte), Integer), CType(CType(192, Byte), Integer), CType(CType(255, Byte), Integer))
        Me.m_RTxtBox_001_UID.Location = New System.Drawing.Point(0, 0)
        Me.m_RTxtBox_001_UID.Name = "m_RTxtBox_001_UID"
        Me.m_RTxtBox_001_UID.ReadOnly = True
        Me.m_RTxtBox_001_UID.Size = New System.Drawing.Size(900, 106)
        Me.m_RTxtBox_001_UID.TabIndex = 0
        Me.m_RTxtBox_001_UID.Text = "CL Card UID"
        '
        'm_RTxtBox_002_MifareBlock
        '
        Me.m_RTxtBox_002_MifareBlock.BackColor = System.Drawing.Color.Black
        Me.m_RTxtBox_002_MifareBlock.Dock = System.Windows.Forms.DockStyle.Fill
        Me.m_RTxtBox_002_MifareBlock.Font = New System.Drawing.Font("PMingLiU", 15.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(136, Byte))
        Me.m_RTxtBox_002_MifareBlock.ForeColor = System.Drawing.Color.FromArgb(CType(CType(255, Byte), Integer), CType(CType(128, Byte), Integer), CType(CType(128, Byte), Integer))
        Me.m_RTxtBox_002_MifareBlock.Location = New System.Drawing.Point(0, 0)
        Me.m_RTxtBox_002_MifareBlock.Name = "m_RTxtBox_002_MifareBlock"
        Me.m_RTxtBox_002_MifareBlock.Size = New System.Drawing.Size(900, 95)
        Me.m_RTxtBox_002_MifareBlock.TabIndex = 0
        Me.m_RTxtBox_002_MifareBlock.Text = "Mifare Record Block"
        '
        'Timer1
        '
        Me.Timer1.Enabled = True
        '
        'OpenFileDialog1
        '
        Me.OpenFileDialog1.FileName = "OpenFileDialog1"
        Me.OpenFileDialog1.Filter = "text sim dat|*.txt|All files|*.*"
        Me.OpenFileDialog1.Title = "Open Tx/Rx Sim Data"
        '
        'Form01_Main
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 12.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(1021, 781)
        Me.Controls.Add(Me.Panel1_Tier01)
        Me.Icon = CType(resources.GetObject("$this.Icon"), System.Drawing.Icon)
        Me.Name = "Form01_Main"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "EMV L1 SDK, IDTech Global Co. Ltd. 2018~2030"
        Me.Panel1_Tier01.ResumeLayout(False)
        Me.SplitContainer1.Panel1.ResumeLayout(False)
        Me.SplitContainer1.Panel2.ResumeLayout(False)
        CType(Me.SplitContainer1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.SplitContainer1.ResumeLayout(False)
        Me.Panel1.ResumeLayout(False)
        Me.SplitContainer4.Panel1.ResumeLayout(False)
        Me.SplitContainer4.Panel2.ResumeLayout(False)
        CType(Me.SplitContainer4, System.ComponentModel.ISupportInitialize).EndInit()
        Me.SplitContainer4.ResumeLayout(False)
        CType(Me.PictureBox1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.SplitContainer9.Panel1.ResumeLayout(False)
        Me.SplitContainer9.Panel2.ResumeLayout(False)
        CType(Me.SplitContainer9, System.ComponentModel.ISupportInitialize).EndInit()
        Me.SplitContainer9.ResumeLayout(False)
        Me.SplitContainer10.Panel1.ResumeLayout(False)
        Me.SplitContainer10.Panel2.ResumeLayout(False)
        CType(Me.SplitContainer10, System.ComponentModel.ISupportInitialize).EndInit()
        Me.SplitContainer10.ResumeLayout(False)
        Me.SplitContainer3.Panel1.ResumeLayout(False)
        CType(Me.SplitContainer3, System.ComponentModel.ISupportInitialize).EndInit()
        Me.SplitContainer3.ResumeLayout(False)
        Me.SplitContainer8.Panel1.ResumeLayout(False)
        Me.SplitContainer8.Panel2.ResumeLayout(False)
        CType(Me.SplitContainer8, System.ComponentModel.ISupportInitialize).EndInit()
        Me.SplitContainer8.ResumeLayout(False)
        Me.TabCtrl_CommIntf.ResumeLayout(False)
        Me.TabPg_Intf_USB.ResumeLayout(False)
        Me.Panel_S00_3_DevMode_Lst.ResumeLayout(False)
        Me.GroupBox_InterfaceMode.ResumeLayout(False)
        Me.GroupBox_InterfaceMode.PerformLayout()
        Me.TabPg_Intf_2_RS232.ResumeLayout(False)
        Me.TabPg_Intf_2_RS232.PerformLayout()
        Me.SplitContainer12.Panel1.ResumeLayout(False)
        Me.SplitContainer12.Panel2.ResumeLayout(False)
        CType(Me.SplitContainer12, System.ComponentModel.ISupportInitialize).EndInit()
        Me.SplitContainer12.ResumeLayout(False)
        Me.TabCtrl_EndMode.ResumeLayout(False)
        Me.TabPg_End_Rounds.ResumeLayout(False)
        Me.GrpBox_02_RndSel.ResumeLayout(False)
        Me.GrpBox_02_RndSel.PerformLayout()
        Me.TabPg_End_TWait.ResumeLayout(False)
        Me.TabPg_End_TWait.PerformLayout()
        Me.GrpBox_SeekCardType.ResumeLayout(False)
        Me.GrpBox_04_TxRx_ToutMs.ResumeLayout(False)
        CType(Me.NumUpDwn_Timeout_TxRx, System.ComponentModel.ISupportInitialize).EndInit()
        Me.SplitContainer11.Panel1.ResumeLayout(False)
        Me.SplitContainer11.Panel2.ResumeLayout(False)
        CType(Me.SplitContainer11, System.ComponentModel.ISupportInitialize).EndInit()
        Me.SplitContainer11.ResumeLayout(False)
        Me.Panel2.ResumeLayout(False)
        Me.GroupBox2.ResumeLayout(False)
        Me.SplitContainer7.Panel1.ResumeLayout(False)
        Me.SplitContainer7.Panel2.ResumeLayout(False)
        CType(Me.SplitContainer7, System.ComponentModel.ISupportInitialize).EndInit()
        Me.SplitContainer7.ResumeLayout(False)
        Me.SplitContainer2.Panel1.ResumeLayout(False)
        Me.SplitContainer2.Panel2.ResumeLayout(False)
        CType(Me.SplitContainer2, System.ComponentModel.ISupportInitialize).EndInit()
        Me.SplitContainer2.ResumeLayout(False)
        Me.GroupBox_IDGCmd_Timeout.ResumeLayout(False)
        Me.GroupBox_IDGCmd_Timeout.PerformLayout()
        Me.SplitContainer6.Panel1.ResumeLayout(False)
        Me.SplitContainer6.Panel2.ResumeLayout(False)
        CType(Me.SplitContainer6, System.ComponentModel.ISupportInitialize).EndInit()
        Me.SplitContainer6.ResumeLayout(False)
        Me.SplitContainer5.Panel1.ResumeLayout(False)
        Me.SplitContainer5.Panel2.ResumeLayout(False)
        CType(Me.SplitContainer5, System.ComponentModel.ISupportInitialize).EndInit()
        Me.SplitContainer5.ResumeLayout(False)
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents Panel1_Tier01 As System.Windows.Forms.Panel
    Friend WithEvents SplitContainer1 As System.Windows.Forms.SplitContainer
    Friend WithEvents PictureBox1 As System.Windows.Forms.PictureBox
    Friend WithEvents m_logRichText As System.Windows.Forms.RichTextBox
    Friend WithEvents GroupBox2 As System.Windows.Forms.GroupBox
    Friend WithEvents SplitContainer4 As System.Windows.Forms.SplitContainer
    Friend WithEvents Btn_S00_Connect_Start As System.Windows.Forms.Button
    Friend WithEvents Panel_S00_3_DevMode_Lst As System.Windows.Forms.Panel
    Friend WithEvents Btn_S00_Stop_Close As System.Windows.Forms.Button
    Friend WithEvents GroupBox_InterfaceMode As System.Windows.Forms.GroupBox
    Friend WithEvents RdoBtn_S00_01_USB_KB_WEDGE As System.Windows.Forms.RadioButton
    Friend WithEvents RdoBtn_S00_01_USB_HID As System.Windows.Forms.RadioButton
    Friend WithEvents SplitContainer7 As System.Windows.Forms.SplitContainer
    Friend WithEvents Btn_S999_Clear As System.Windows.Forms.Button
    Friend WithEvents GroupBox_IDGCmd_Timeout As System.Windows.Forms.GroupBox
    Friend WithEvents Label12 As System.Windows.Forms.Label
    Friend WithEvents TextBox4_TxnTimeout As System.Windows.Forms.TextBox
    Friend WithEvents Button_Timeout As System.Windows.Forms.Button
    Friend WithEvents Timer1 As System.Windows.Forms.Timer
    Friend WithEvents ProgressBar1 As System.Windows.Forms.ProgressBar
    Friend WithEvents SplitContainer9 As System.Windows.Forms.SplitContainer
    Friend WithEvents SplitContainer10 As System.Windows.Forms.SplitContainer
    Friend WithEvents SplitContainer11 As System.Windows.Forms.SplitContainer
    Friend WithEvents GrpBox_02_RndSel As System.Windows.Forms.GroupBox
    Friend WithEvents SplitContainer2 As System.Windows.Forms.SplitContainer
    Friend WithEvents Button1 As System.Windows.Forms.Button
    Friend WithEvents ToolTip1 As System.Windows.Forms.ToolTip
    Friend WithEvents Rnd_01_Cmbox_Sel_Rnd As System.Windows.Forms.ComboBox
    Friend WithEvents Rnd_02_Btn_Get_Rnd As System.Windows.Forms.Button
    Friend WithEvents SplitContainer3 As System.Windows.Forms.SplitContainer
    Friend WithEvents GrpBox_SeekCardType As System.Windows.Forms.GroupBox
    Friend WithEvents cbox_SeekCardType As System.Windows.Forms.ComboBox
    Friend WithEvents GrpBox_04_TxRx_ToutMs As System.Windows.Forms.GroupBox
    Friend WithEvents NumUpDwn_Timeout_TxRx As System.Windows.Forms.NumericUpDown
    Friend WithEvents SplitContainer6 As System.Windows.Forms.SplitContainer
    Friend WithEvents SplitContainer5 As System.Windows.Forms.SplitContainer
    Friend WithEvents m_RTxtBox_001_UID As System.Windows.Forms.RichTextBox
    Friend WithEvents m_RTxtBox_002_MifareBlock As System.Windows.Forms.RichTextBox
    Friend WithEvents TabCtrl_EndMode As System.Windows.Forms.TabControl
    Friend WithEvents TabPg_End_Rounds As System.Windows.Forms.TabPage
    Friend WithEvents TabPg_End_TWait As System.Windows.Forms.TabPage
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents m_CBox_End_TWaitTime As System.Windows.Forms.ComboBox
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents OpenFileDialog1 As System.Windows.Forms.OpenFileDialog
    Friend WithEvents SplitContainer8 As System.Windows.Forms.SplitContainer
    Friend WithEvents SplitContainer12 As System.Windows.Forms.SplitContainer
    Friend WithEvents m_CmbBox_MifareRunMode As System.Windows.Forms.ComboBox
    Friend WithEvents Panel1 As System.Windows.Forms.Panel
    Friend WithEvents TabCtrl_CommIntf As System.Windows.Forms.TabControl
    Friend WithEvents TabPg_Intf_USB As System.Windows.Forms.TabPage
    Friend WithEvents TabPg_Intf_2_RS232 As System.Windows.Forms.TabPage
    Friend WithEvents Label3 As System.Windows.Forms.Label
    Friend WithEvents Label4 As System.Windows.Forms.Label
    Friend WithEvents ComboBox_PortSel_COM As System.Windows.Forms.ComboBox
    Friend WithEvents ComboBox_PortSel_Baudrate As System.Windows.Forms.ComboBox
    Friend WithEvents Panel2 As System.Windows.Forms.Panel

End Class
