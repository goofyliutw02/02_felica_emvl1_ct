﻿'==================================================================================
' File Name: ClassReaderProtocolBasic.vb
' Description: The ClassReaderProtocolBasic is as Must-be Inherited Base Class of Reader's Packet Parser & Composer.
' Who use it as Base Class: ClassReaderProtocolBasicIDGAR, ClassReaderProtocolBasicIDGAR, and ClassReaderProtocolBasicIDTECH
' For EMV L1 Functions
' 
' Revision:
' -----------------------[ Initial Version ]-----------------------
' 2017 Oct 06, by goofy_liu@idtechproducts.com.tw

'==================================================================================
'
'----------------------[ Importing Stuffs ]----------------------
Imports System.Runtime.Serialization
Imports System.Runtime.Serialization.Formatters.Binary
'Imports DiagTool_HardwareExclusive.ClassReaderProtocolBasicIDGAR
Imports System.IO
Imports System.IO.Ports
Imports System.Threading

'-------------------[ Class Declaration ]-------------------

Partial Public Class ClassReaderProtocolBasic
    '
    Public Overridable Sub SystemEmvL1Cmd_CL_002_Loopbackstart(Optional ByVal bty01_DelayTime As Byte = &H5, Optional ByVal bty02_RunMode As Byte = &H0, Optional ByVal bty03_LogEnable As Byte = &H0, Optional ByVal nTimeout As Integer = 1)
        Dim u16Cmd As UInt16 = &HFD48
        Dim strFuncName As String = "SystemEmvL1Cmd_CL_002_Loopbackstart"
        Dim arrayByteSend As Byte() = Nothing
        Dim arrayByteRecv As Byte() = Nothing
        '
        If (m_bIDGCmdBusy) Then
            Return
        End If
        m_bIDGCmdBusy = True

        Try
            arrayByteSend = New Byte() {bty01_DelayTime, bty02_RunMode, bty03_LogEnable}

            System_Customized_Command(u16Cmd, strFuncName, arrayByteSend, arrayByteRecv, AddressOf ClassReaderProtocolBasic.PreProcessCbRecv, nTimeout)
            '2017 Oct 13
            'Start Prj: VP5300, 3-in-1 Test, Kevin Cheng.HW Request
            'Note - Don't care Rx since Loopback start cmd has NO Resp
            'Also we setup timeout value as 1 seconds
            'Note 02 - If Enable Log, must be very care ful in Rx processing.
            'Its' response code = 0 => OK
            If (bIsRetStatusOK) Then
                '
                'Else
                '    LogLn("[Err] " + strFuncName + "() = Response Code (" & m_tagIDGCmdDataInfo.m_nResponse & ") --> " + m_dictStatusCode2.Item(m_tagIDGCmdDataInfo.m_nResponse))
            End If

        Catch ext As TimeoutException
            LogLn("[Err][Timeout] [IDG: " + GetIDGCmdString(u16Cmd) + "] " + strFuncName + "() = " + ext.Message)
        Catch ex As Exception
            LogLn("[Err] [IDG: " + GetIDGCmdString(u16Cmd) + "] " + strFuncName + "() = " + ex.Message)
        Finally
            m_bIDGCmdBusy = False
            If (arrayByteSend IsNot Nothing) Then
                Erase arrayByteSend
            End If
            If (arrayByteRecv IsNot Nothing) Then
                Erase arrayByteRecv
            End If
        End Try
        '
    End Sub
    '
    Public Overridable Sub SystemEmvL1Cmd_CL_002_Loopbackstop(Optional ByVal nTimeout As Integer = 1)
        Dim u16Cmd As UInt16 = &HFD49
        Dim strFuncName As String = "SystemEmvL1Cmd_CL_002_Loopbackstop"
        Dim arrayByteSend As Byte() = Nothing
        Dim arrayByteRecv As Byte() = Nothing
        '
        If (m_bIDGCmdBusy) Then
            Return
        End If
        m_bIDGCmdBusy = True

        Try


            System_Customized_Command(u16Cmd, strFuncName, arrayByteSend, arrayByteRecv, AddressOf ClassReaderProtocolBasic.PreProcessCbRecv, nTimeout)
            '2017 Oct 13
            'Start Prj: VP5300, 3-in-1 Test, Kevin Cheng.HW Request
            'Note - Don't care Rx since Loopback start cmd has NO Resp
            'Its' response code = 0 => OK
            If (Not bIsRetStatusOK) Then
                LogLn("[Err] " + strFuncName + "() = Response Code (" & m_tagIDGCmdDataInfo.m_nResponse & ") --> " + m_dictStatusCode2.Item(m_tagIDGCmdDataInfo.m_nResponse))
            End If

        Catch ext As TimeoutException
            LogLn("[Err][Timeout] [IDG: " + GetIDGCmdString(u16Cmd) + "] " + strFuncName + "() = " + ext.Message)
        Catch ex As Exception
            LogLn("[Err] [IDG: " + GetIDGCmdString(u16Cmd) + "] " + strFuncName + "() = " + ex.Message)
        Finally
            m_bIDGCmdBusy = False
            If (arrayByteSend IsNot Nothing) Then
                Erase arrayByteSend
            End If
            If (arrayByteRecv IsNot Nothing) Then
                Erase arrayByteRecv
            End If
        End Try
        '
    End Sub
    '
    Public Overridable Sub SystemEmvL1Cmd_CL_001_Carrier(Optional ByVal bCarrier As Boolean = True, Optional ByVal nTimeout As Integer = ClassDevRS232.m_cnTX_TIMEOUT_IN_SEC)
        Dim u16Cmd As UInt16 = &HFD01
        Dim strFuncName As String = "SystemEmvL1Cmd_CL_001_Carrier"
        Dim arrayByteSend As Byte() = Nothing
        Dim arrayByteRecv As Byte() = Nothing
        '
        If (m_bIDGCmdBusy) Then
            Return
        End If
        m_bIDGCmdBusy = True

        Try
            If (bCarrier) Then
                arrayByteSend = New Byte() {&H1}
            Else
                arrayByteSend = New Byte() {&H0}
            End If


            System_Customized_Command(u16Cmd, strFuncName, arrayByteSend, arrayByteRecv, AddressOf ClassReaderProtocolBasic.PreProcessCbRecv, nTimeout)
            'Its' response code = 0 => OK
            If (bIsRetStatusOK) Then

            Else
                LogLn("[Err] " + strFuncName + "() = Response Code (" & m_tagIDGCmdDataInfo.m_nResponse & ") --> " + m_dictStatusCode2.Item(m_tagIDGCmdDataInfo.m_nResponse))
            End If

        Catch ext As TimeoutException
            LogLn("[Err][Timeout] [IDG: " + GetIDGCmdString(u16Cmd) + "] " + strFuncName + "() = " + ext.Message)
        Catch ex As Exception
            LogLn("[Err] [IDG: " + GetIDGCmdString(u16Cmd) + "] " + strFuncName + "() = " + ex.Message)
        Finally
            m_bIDGCmdBusy = False
            If (arrayByteSend IsNot Nothing) Then
                Erase arrayByteSend
            End If
            If (arrayByteRecv IsNot Nothing) Then
                Erase arrayByteRecv
            End If
        End Try
        '
    End Sub
    '
    'NEO 2.0, EMV L1, Set / Get Parameter
    'IDG Cmd Format Ref Doc
    'https://atlassian.idtechproducts.com/confluence/download/attachments/37193805/EMV%20Contact%20%26%20Contactless%20L1%20IDG%20Commands%20Specification%20for%20debug%20tool%20V1.5.docx?version=1&modificationDate=1504686895059&api=v2
    'Method
    'Get : [IN] io_aryByteParam = Nothing;    [OUT] = Parameters
    'Set : [IN] io_aryByteParam = Parameters; [OUT] = Nothing
    Public Overridable Sub SystemEmvL1Cmd_SetGetParameters_Generic(ByRef io_aryByteParam As Byte(), Optional ByVal nTimeout As Integer = ClassDevRS232.m_cnTX_TIMEOUT_IN_SEC)
        Dim u16Cmd As UInt16 = &HF800
        Dim strFuncName As String = "SystemEmvL1Cmd_SetGetParameters_Generic"
        Dim arrayByteSend As Byte() = io_aryByteParam
        Dim arrayByteRecv As Byte() = Nothing
        '
        If (m_bIDGCmdBusy) Then
            Return
        End If
        m_bIDGCmdBusy = True

        Try
            '
            System_Customized_Command(u16Cmd, strFuncName, arrayByteSend, arrayByteRecv, AddressOf ClassReaderProtocolBasic.PreProcessCbRecv, nTimeout)
            '* Remove In Data by Caller

            'Its' response code = 0 => OK
            If (bIsRetStatusOK) Then
                Dim arrayByteData As Byte() = Nothing

                m_tagIDGCmdDataInfo.ParserDataGetArrayRawData(arrayByteData)
                '
                If (arrayByteData IsNot Nothing) Then
                    io_aryByteParam = arrayByteData
                End If
            Else
                LogLn("[Err] " + strFuncName + "() = Response Code (" & m_tagIDGCmdDataInfo.m_nResponse & ") --> " + m_dictStatusCode2.Item(m_tagIDGCmdDataInfo.m_nResponse))
            End If

        Catch ext As TimeoutException
            LogLn("[Err][Timeout] [IDG: " + GetIDGCmdString(u16Cmd) + "] " + strFuncName + "() = " + ext.Message)
        Catch ex As Exception
            LogLn("[Err] [IDG: " + GetIDGCmdString(u16Cmd) + "] " + strFuncName + "() = " + ex.Message)
        Finally
            m_bIDGCmdBusy = False
            If (arrayByteSend IsNot Nothing) Then
                Erase arrayByteSend
            End If
            If (arrayByteRecv IsNot Nothing) Then
                Erase arrayByteRecv
            End If
        End Try
        '
    End Sub
    'Note: After Getting data,
    'io_aryByteParam should be reserved until next set functions.
    Public Sub SystemEmvL1Cmd_GetParameters(ByRef o_byte01_Volt As Byte, ByRef o_byte01_LvL As Byte, ByRef o_byte02_Volt As Byte, ByRef o_byte02_LvL As Byte, ByRef io_aryByteParam As Byte(), Optional ByVal nTimeout As Integer = ClassDevRS232.m_cnTX_TIMEOUT_IN_SEC)
        Dim nOff As Integer = 0
        io_aryByteParam = Nothing
        o_byte01_Volt = &HFF
        o_byte01_LvL = &HFF
        o_byte02_Volt = &HFF
        o_byte02_LvL = &HFF

        SystemEmvL1Cmd_SetGetParameters_Generic(io_aryByteParam, nTimeout)
        If (bIsRetStatusOK) Then
            If (io_aryByteParam IsNot Nothing) Then
                If (io_aryByteParam.Length >= 4) Then
                    o_byte01_Volt = io_aryByteParam(nOff) '3.3V = &H1; 5V= &H0
                    nOff = nOff + 1
                    o_byte01_LvL = io_aryByteParam(nOff) ' 5~14 (&H5~&HE)
                    nOff = nOff + 1
                    o_byte02_Volt = io_aryByteParam(nOff) '3.3V = &H1; 5V= &H0
                    nOff = nOff + 1
                    o_byte02_LvL = io_aryByteParam(nOff) ' 5~14 (&H5~&HE)
                End If
            End If
        End If
    End Sub
    '
    Public Sub SystemEmvL1Cmd_SetParameters(ByVal o_byte01_Volt As Byte, ByVal o_byte01_LvL As Byte, ByVal o_byte02_Volt As Byte, ByVal o_byte02_LvL As Byte, ByVal io_aryByteParam As Byte(), Optional ByVal nTimeout As Integer = ClassDevRS232.m_cnTX_TIMEOUT_IN_SEC)
        Dim nOff As Integer = 0
        If (io_aryByteParam.Length >= 4) Then
            io_aryByteParam(nOff) = o_byte01_Volt '3.3V = &H1; 5V= &H0
            nOff = nOff + 1
            io_aryByteParam(nOff) = o_byte01_LvL ' 5~14 (&H5~&HE)
            nOff = nOff + 1
            io_aryByteParam(nOff) = o_byte02_Volt  '3.3V = &H1; 5V= &H0
            nOff = nOff + 1
            io_aryByteParam(nOff) = o_byte02_LvL  ' 5~14 (&H5~&HE)
        Else
            m_tagIDGCmdDataInfo.byteSetResponseCode = &HA
            Return
        End If
        '
        SystemEmvL1Cmd_SetGetParameters_Generic(io_aryByteParam, nTimeout)

        If (Not bIsRetStatusOK) Then
            Return
        End If

        'To Do
        'Get Again & compare to make sure value done

    End Sub

    Private Sub SystemEmvL1Cmd_CL_PT_SystemSetEnhancePassthroughMode_LCDMsgs(ByRef io_strMsg As String, ByRef io_arrayByteSendData As Byte(), ByRef io_nSendDataSeekIdx As Integer)
        If (io_strMsg.Length > 0) Then
            Dim arrayByte As Byte()
            arrayByte = System.Text.Encoding.UTF8.GetBytes(io_strMsg)
            Array.Copy(arrayByte, 0, io_arrayByteSendData, io_nSendDataSeekIdx, io_strMsg.Length)
            io_nSendDataSeekIdx = io_nSendDataSeekIdx + io_strMsg.Length
        End If
        io_arrayByteSendData(io_nSendDataSeekIdx) = &H0
        io_nSendDataSeekIdx = io_nSendDataSeekIdx + 1
    End Sub
    '
    Public Sub SystemEmvL1Cmd_CL_PT_SystemSetEnhancePassthroughMode(u8SingleShotMode As ENUM_EPT_SINGLE_SHOT_MODE, u8LCDMsgIdx As ENUM_EPT_LCD_MSG_IDX, u8BeepIndicator As ENUM_SEPM_B7_BEEP_INDICATOR, u8LEDNum As ENUM_SEPM_LED_NUM, u8LEDOnOff As ENUM_SEPM_LED_ONOFF, strMsg1_Or_CustomMsg As String, strMsg2 As String, u8ActivateInterface As ENUM_SEPM_INTERFACE, ByRef o_byteCardType As Byte, ByRef o_arrayByteSerialNumUIC As Byte(), Optional ByVal bAlwaysSpecificUserInterface As Boolean = True, Optional ByVal nTimeout As Integer = ClassDevRS232.m_cnTX_TIMEOUT_IN_SEC)
        '
        Dim nSendDataMaxSize As Integer = 255
        Dim nSendDataSeekIdx As Integer = 0
        Dim arrayByteSendData As Byte() = New Byte(255) {}
        Dim u16Cmd As UInt16 = &H2C0B
        Dim strFuncName As String = "GRExclusive_PT_SystemSetEnhancePassthroughMode"
        Dim strCmd As String = GetIDGCmdString(m_tagIDGCmdDataInfo.m_u16CmdSet)

        m_bIsRetStatusOK = False
        If (m_bIDGCmdBusy) Then
            Return
        End If
        m_bIDGCmdBusy = True

        Try
            '-----------------------------------------------------------------[ Send to reader ]-----------------------------------------------------------------
            'Compose The IDG Packet 18-01, Not Command Frame Data, No Response Frame Data
            m_tagIDGCmdDataInfo.Reinit(u16Cmd, m_arraybyteSendBuf, Me)

            '[Single-Shot Commands , 1 byte]+ [LCD Message Index (for readers with a display only), 1 byte] + [Beep Indicator, 1 byte]+[LED Number,  1 byte]+ [LED Status, 1 byte]+[Timeout 1, 1 byte]+[Timeout 2, 1 byte = 0]+[LCD String1 Message, don't care]+[LCD String2 Message, don't care]+[Custom LCD Message, don't care]+[Selected Interface if user specific interface bit = 1]
            arrayByteSendData(nSendDataSeekIdx) = u8SingleShotMode
            nSendDataSeekIdx = nSendDataSeekIdx + 1
            arrayByteSendData(nSendDataSeekIdx) = u8LCDMsgIdx
            nSendDataSeekIdx = nSendDataSeekIdx + 1
            arrayByteSendData(nSendDataSeekIdx) = u8BeepIndicator
            nSendDataSeekIdx = nSendDataSeekIdx + 1
            arrayByteSendData(nSendDataSeekIdx) = u8LEDNum
            nSendDataSeekIdx = nSendDataSeekIdx + 1
            arrayByteSendData(nSendDataSeekIdx) = u8LEDOnOff
            nSendDataSeekIdx = nSendDataSeekIdx + 1
            arrayByteSendData(nSendDataSeekIdx) = nTimeout 'Time out in Second
            nSendDataSeekIdx = nSendDataSeekIdx + 1
            arrayByteSendData(nSendDataSeekIdx) = &H0 ' Timeout 2 in multiple of 10 seconds
            nSendDataSeekIdx = nSendDataSeekIdx + 1

            Select Case (u8LCDMsgIdx)
                Case ENUM_EPT_LCD_MSG_IDX.xFE_CUSTOM_USE_MSG
                    SystemEmvL1Cmd_CL_PT_SystemSetEnhancePassthroughMode_LCDMsgs(strMsg1_Or_CustomMsg, arrayByteSendData, nSendDataSeekIdx)
                Case ENUM_EPT_LCD_MSG_IDX.xFF_LED_BUZZER_ONLY_WITHOUT_LCD
                    'Nothing
                Case Else
                    If ((u8LCDMsgIdx And ENUM_EPT_LCD_MSG_IDX.x80_MASK) = ENUM_EPT_LCD_MSG_IDX.x80_MASK) Then
                        SystemEmvL1Cmd_CL_PT_SystemSetEnhancePassthroughMode_LCDMsgs(strMsg1_Or_CustomMsg, arrayByteSendData, nSendDataSeekIdx)
                        SystemEmvL1Cmd_CL_PT_SystemSetEnhancePassthroughMode_LCDMsgs(strMsg2, arrayByteSendData, nSendDataSeekIdx)
                    Else
                    End If
            End Select

            If (bAlwaysSpecificUserInterface) Then
                'KIOSK III always needs the last specific user interface byte
                arrayByteSendData(nSendDataSeekIdx) = u8ActivateInterface
                nSendDataSeekIdx = nSendDataSeekIdx + 1
            Else
                If ((u8SingleShotMode And ENUM_EPT_SINGLE_SHOT_MODE.x20_USE_SPECIFIED_INTERFACE) = ENUM_EPT_SINGLE_SHOT_MODE.x20_USE_SPECIFIED_INTERFACE) Then
                    arrayByteSendData(nSendDataSeekIdx) = u8ActivateInterface
                    nSendDataSeekIdx = nSendDataSeekIdx + 1
                End If
            End If

            'arrayByteSendData = New Byte() {&H0, CType(nTimeout, Byte), u32FlagsB0Transaction, &H0, u32FlagsB2SingleShot, &H0, byteICCType, byteBeepIndicator, byteLEDControl, &H0, &H0}

            'Set Passthrough Mode
            m_tagIDGCmdDataInfo.ComposerAddData(arrayByteSendData, nSendDataSeekIdx)

            PktComposerCommandFrameHeaderCmdSetDataLengthCRCData(m_tagIDGCmdDataInfo, True)


            'Waiting For Response in SystemPingCbRecv()
            m_refdevConn.SendCommandFrameAndWaitForResponseFrame(AddressOf ClassReaderProtocolBasic.PreProcessCbRecv, m_tagIDGCmdDataInfo, m_nIDGCommanderType, True, nTimeout * 1000)


            'Its' response code = 0 => OK
            If (m_tagIDGCmdDataInfo.bIsResponseOK) Then
                Dim arrayByteData As Byte() = Nothing

                m_bIsRetStatusOK = True
                '
                m_tagIDGCmdDataInfo.ParserDataGetArrayRawData(arrayByteData)

                If (arrayByteData IsNot Nothing) Then
                    ' byte 0 = Card Type
                    o_byteCardType = arrayByteData(0)

                    'Remained data is PICC's Serial Number/UID
                    If (o_arrayByteSerialNumUIC IsNot Nothing) Then
                        Erase o_arrayByteSerialNumUIC
                        o_arrayByteSerialNumUIC = Nothing
                    End If
                    '
                    If (arrayByteData.Count > 1) Then
                        'Copy Data (Serial Num Or UID) of the PICC
                        Dim nDataLen As Integer
                        nDataLen = arrayByteData.Count - 1
                        o_arrayByteSerialNumUIC = New Byte(nDataLen - 1) {}
                        Array.Copy(arrayByteData, 1, o_arrayByteSerialNumUIC, 0, nDataLen)
                    End If
                End If
            Else
                m_bIsRetStatusOK = False
            End If

        Catch ext As TimeoutException
            LogLn("[Err][RS232 Timeout] [IDG: " + GetIDGCmdString(u16Cmd) + "] " + strFuncName + "() = " + ext.Message)
        Catch ex As Exception
            LogLn("[Err] [IDG:  " + GetIDGCmdString(u16Cmd) + "] " + strFuncName + "() = " + ex.Message)
        Finally
            m_tagIDGCmdDataInfo.Cleanup()
            m_bIDGCmdBusy = False
            If (arrayByteSendData Is Nothing) Then
                Erase arrayByteSendData
            End If
        End Try

    End Sub

    Public Sub SystemEmvL1Cmd_CL_PT_PollInterfacePICC_DeActivate(ByRef o_byteCardType As Byte, ByRef o_arrayByteSerialNumUIC As Byte(), Optional ByVal bNoBuzzer As Boolean = False, Optional strMsg1 As String = "", Optional strMsg2 As String = "", Optional ByVal nTimeout As Integer = ClassDevRS232.m_cnTX_TIMEOUT_IN_SEC)
        Dim byteFlagBuzzer As ENUM_SEPM_B7_BEEP_INDICATOR

        If (bNoBuzzer) Then
            byteFlagBuzzer = ENUM_SEPM_B7_BEEP_INDICATOR.x00_BEEP_NO
        Else
            byteFlagBuzzer = ENUM_SEPM_B7_BEEP_INDICATOR.x06_BEEP_1LB_400ms
        End If
        SystemEmvL1Cmd_CL_PT_SystemSetEnhancePassthroughMode(ENUM_EPT_SINGLE_SHOT_MODE.x02_DEACTIVATE_INTERFACE Or ENUM_EPT_SINGLE_SHOT_MODE.x08_USE_INDEPENDENT_LED_INSTEAD Or ENUM_EPT_SINGLE_SHOT_MODE.x10_USE_INDEPENDENT_BUZZER_INSTEAD, ENUM_EPT_LCD_MSG_IDX.xFF_LED_BUZZER_ONLY_WITHOUT_LCD, byteFlagBuzzer, ENUM_SEPM_LED_NUM.xFF_LALL, ENUM_SEPM_LED_ONOFF.x01_LED_OFF, strMsg1, strMsg2, ENUM_SEPM_INTERFACE.x00_CL, o_byteCardType, o_arrayByteSerialNumUIC, nTimeout)
    End Sub


    '2017 Nov 08, updated for new 2C-02 commands.
    '+ ) In - i_byteCardTypeIndicator, bit 1 = Type A, bit 2 = Type B, bit 3 = Type C

    Public Sub SystemEmvL1Cmd_PT_PollForToken_TimeoutInMS_V02(ByVal i_byteCardTypeIndicator As Byte, ByRef o_byteCardType As Byte, ByRef o_arrayByteSerialNumUIC As Byte(), Optional ByVal nTimeoutMS As Integer = ClassDevRS232.m_cnTX_TIMEOUT_IN_SEC * 1000)
        Dim u16Cmd As UInt16 = &H2C02
        Dim strFuncName As String = "SystemEmvL1Cmd_PT_PollForToken_TimeoutInMS_V02" 'System.Reflection.MethodBase.GetCurrentMethod().Name
        '
        Dim arrayByteSend As Byte() = Nothing
        Dim arrayByteRecv As Byte() = Nothing

        If (m_bIDGCmdBusy) Then
            Return
        End If
        m_bIDGCmdBusy = True

        '
        Try
            arrayByteSend = New Byte() {CType(nTimeoutMS \ 1000, Byte), CType((nTimeoutMS Mod 1000) \ 10, Byte), i_byteCardTypeIndicator}


            System_Customized_Command(u16Cmd, strFuncName, arrayByteSend, arrayByteRecv, AddressOf ClassReaderProtocolBasic.PreProcessCbRecv, nTimeoutMS, True)

            'Its' response code = 0 => OK

            If (bIsRetStatusOK) Then
                ' Byte 0 = Card Type
                o_byteCardType = arrayByteRecv(0)
                ' Byte Remained = Serial Num / UID
                o_arrayByteSerialNumUIC = New Byte(arrayByteRecv.Count - 1 - 1) {}
                Array.Copy(arrayByteRecv, 1, o_arrayByteSerialNumUIC, 0, o_arrayByteSerialNumUIC.Count)
            Else
                If (Not m_refdevConn.bIsHWTestOnly) Then
                    LogLn("[Err] " + strFuncName + "() = Response Code (" & m_tagIDGCmdDataInfo.m_nResponse & ")")
                End If

            End If

        Catch ext As TimeoutException
            LogLn("[Err] [RS232 Timeout][IDG: " + GetIDGCmdString(u16Cmd) + "] " + strFuncName + "() = " + ext.Message)
        Catch ex As Exception
            LogLn("[Err][IDG: " + GetIDGCmdString(u16Cmd) + "] " + strFuncName + "() = " + ex.Message)
        Finally
            m_bIDGCmdBusy = False
            If (arrayByteSend IsNot Nothing) Then
                Erase arrayByteSend
            End If
            If (arrayByteRecv IsNot Nothing) Then
                Erase arrayByteRecv
            End If
        End Try
        '
    End Sub
    Public Sub SystemEmvL1Cmd_PT_PollForToken_TimeoutInMS(ByRef o_byteCardType As Byte, ByRef o_arrayByteSerialNumUIC As Byte(), Optional ByVal nTimeoutMS As Integer = ClassDevRS232.m_cnTX_TIMEOUT_IN_SEC * 1000)
        Dim u16Cmd As UInt16 = &H2C02
        Dim strFuncName As String = "SystemEmvL1Cmd_PT_PollForToken_TimeoutInMS" 'System.Reflection.MethodBase.GetCurrentMethod().Name
        '
        Dim arrayByteSend As Byte() = Nothing
        Dim arrayByteRecv As Byte() = Nothing

        If (m_bIDGCmdBusy) Then
            Return
        End If
        m_bIDGCmdBusy = True

        '
        Try
            arrayByteSend = New Byte() {CType(nTimeoutMS \ 1000, Byte), CType((nTimeoutMS Mod 1000) \ 10, Byte)}


            System_Customized_Command(u16Cmd, strFuncName, arrayByteSend, arrayByteRecv, AddressOf ClassReaderProtocolBasic.PreProcessCbRecv, nTimeoutMS, True)

            'Its' response code = 0 => OK

            If (bIsRetStatusOK) Then
                ' Byte 0 = Card Type
                o_byteCardType = arrayByteRecv(0)
                ' Byte Remained = Serial Num / UID
                o_arrayByteSerialNumUIC = New Byte(arrayByteRecv.Count - 1 - 1) {}
                Array.Copy(arrayByteRecv, 1, o_arrayByteSerialNumUIC, 0, o_arrayByteSerialNumUIC.Count)
            Else
                If (Not m_refdevConn.bIsHWTestOnly) Then
                    LogLn("[Err] " + strFuncName + "() = Response Code (" & m_tagIDGCmdDataInfo.m_nResponse & ")")
                End If

            End If

        Catch ext As TimeoutException
            LogLn("[Err] [RS232 Timeout][IDG: " + GetIDGCmdString(u16Cmd) + "] " + strFuncName + "() = " + ext.Message)
        Catch ex As Exception
            LogLn("[Err][IDG: " + GetIDGCmdString(u16Cmd) + "] " + strFuncName + "() = " + ex.Message)
        Finally
            m_bIDGCmdBusy = False
            If (arrayByteSend IsNot Nothing) Then
                Erase arrayByteSend
            End If
            If (arrayByteRecv IsNot Nothing) Then
                Erase arrayByteRecv
            End If
        End Try
        '
    End Sub

    Public Sub SystemEmvL1Cmd_PT_Mifare_Authentication(Optional ByVal nTimeoutMS As Integer = ClassDevRS232.m_cnTX_TIMEOUT_IN_SEC * 1000)
        'Ex: Mifare auth (2C-06)
        ' [TX] - 56 69 56 4F 74 65 63 68 32 00 2C 06 00 08 01 01 FF FF FF FF FF FF 4B C8
        ' [RX] - 56 69 56 4F 74 65 63 68 32 00 2C 00 00 00 1C 9B
        Dim u16Cmd As UInt16 = &H2C06
        Dim strFuncName As String = "SystemEmvL1Cmd_PT_Mifare_Authentication" 'System.Reflection.MethodBase.GetCurrentMethod().Name
        '
        Dim arrayByteSend As Byte() = New Byte() {&H1, &H1, &HFF, &HFF, &HFF, &HFF, &HFF, &HFF}
        Dim arrayByteRecv As Byte() = Nothing

        If (m_bIDGCmdBusy) Then
            Return
        End If
        m_bIDGCmdBusy = True
        '
        Try

            System_Customized_Command(u16Cmd, strFuncName, arrayByteSend, arrayByteRecv, AddressOf ClassReaderProtocolBasic.PreProcessCbRecv, nTimeoutMS, True)

            If (bIsRetStatusOK) Then
                'Do something if Authentication Okay.
            End If

        Catch ext As TimeoutException
            LogLn("[Err] [RS232 Timeout][IDG: " + GetIDGCmdString(u16Cmd) + "] " + strFuncName + "() = " + ext.Message)
        Catch ex As Exception
            LogLn("[Err][IDG: " + GetIDGCmdString(u16Cmd) + "] " + strFuncName + "() = " + ex.Message)
        Finally
            m_bIDGCmdBusy = False
            If (arrayByteSend IsNot Nothing) Then
                Erase arrayByteSend
            End If
            If (arrayByteRecv IsNot Nothing) Then
                Erase arrayByteRecv
            End If
        End Try
    End Sub

    Public Sub SystemEmvL1Cmd_PT_Mifare_ReadBlocks(ByRef o_BlockInfo As Byte(), Optional ByVal nTimeoutMS As Integer = ClassDevRS232.m_cnTX_TIMEOUT_IN_SEC * 1000)
        'Ex: Mifare auth (2C-06)
        '[TX] - 56 69 56 4F 74 65 63 68 32 00 2C 07 00 02 33 10 3F 8F 
        '[RX] - 56 69 56 4F 74 65 63 68 32 00 2C 00 00 30 87 19 00 00 78 E6 FF FF 87 19 00 00 00 FF 00 FF B7 1A 00 00 48 E5 FF FF B7 1A 00 00 00 FF 00 FF 88 19 00 00 77 E6 FF FF 88 19 00 00 00 FF 00 FF 4C D2  
        Dim u16Cmd As UInt16 = &H2C07
        Dim strFuncName As String = "SystemEmvL1Cmd_PT_Mifare_ReadBlocks" 'System.Reflection.MethodBase.GetCurrentMethod().Name
        '
        Dim arrayByteSend As Byte() = New Byte() {&H33, &H10}
        Dim arrayByteRecv As Byte() = Nothing

        If (m_bIDGCmdBusy) Then
            Return
        End If
        m_bIDGCmdBusy = True
        '
        Try
            o_BlockInfo = Nothing
            System_Customized_Command(u16Cmd, strFuncName, arrayByteSend, o_BlockInfo, AddressOf ClassReaderProtocolBasic.PreProcessCbRecv, nTimeoutMS, True)

            If (bIsRetStatusOK) Then
                'Do something(Rx Data) if Read Mifare Block okay.
                ' 
            End If

        Catch ext As TimeoutException
            LogLn("[Err] [RS232 Timeout][IDG: " + GetIDGCmdString(u16Cmd) + "] " + strFuncName + "() = " + ext.Message)
        Catch ex As Exception
            LogLn("[Err][IDG: " + GetIDGCmdString(u16Cmd) + "] " + strFuncName + "() = " + ex.Message)
        Finally
            m_bIDGCmdBusy = False
            If (arrayByteSend IsNot Nothing) Then
                Erase arrayByteSend
            End If
            If (arrayByteRecv IsNot Nothing) Then
                Erase arrayByteRecv
            End If
        End Try

    End Sub

End Class
