﻿Imports System.Text
Imports Main.tagConfigCmdUnitPollmode
'
#Const PLATFORM_NEO100_AR215 = 1
#Const PLATFORM_NEO200_AR300 = 1
'
Public Class ClassReaderCommanderParser
    Implements IDisposable

    Enum EnumConfigCmd As Integer
        CONFIG_UNKNOWN = -1
        '
        CONFIG_POLLMODE_SET
        CONFIG_POLLMODE_DATA
        '
        CONFIG_CONFIGURATION_SET
        '
        CONFIG_AID_SET
        CONFIG_AID_DELETE
        CONFIG_END
        CONFIG_GROUP_SET
        CONFIG_GROUP_DELETE
        CONFIG_COMMENT
        '
        CONFIG_TLV
    End Enum

    Enum EnumConfigurationsFormAddDefaultMode As Integer 'short as CFADefM
        CFADefM_STANDARD = 0
        CFADefM_AIDLIST
        CFADefM_GROUPLIST
    End Enum

    '----------------------------------------------------[Public: Parser Operation Variables]----------------------------------------------------
    Public Shared STR_TABPAGE_CONFIG_TAG_GRPSET_PREFIX As String = EnumConfigCmd.CONFIG_GROUP_SET & " = "
    Public Shared STR_TABPAGE_CONFIG_TAG_AIDSET_PREFIX As String = EnumConfigCmd.CONFIG_AID_SET & " = "
    Public Shared STR_TABPAGE_CONFIG_TAG_AIDDEL_PREFIX As String = EnumConfigCmd.CONFIG_AID_DELETE & " = "
    Public Shared STR_TABPAGE_CONFIG_TAG_POLLMODE_PREFIX As String = EnumConfigCmd.CONFIG_POLLMODE_SET & " = "

    '----------------------------------------------------[Delegate Function type]----------------------------------------------------
    ' Private Delegate Sub LogLnDelegate(ByRef strInfo As String)
    Private Shared LogLn As Form01_Main.LogLnDelegate = New Form01_Main.LogLnDelegate(AddressOf Form01_Main.GetInstance().LogLn)


    '----------------------------------------------------[Lookup Table Operation]----------------------------------------------------
    'TLV Parser , Major
    Public Shared m_dictTLVOp_RefList_Major As Dictionary(Of String, tagTLVNewOp) = Nothing

#If PLATFORM_NEO200_AR300 Then
    'Legacy TLV Parser for NEW 2.00 and AR 3.0.0
    ' products, NEO 2.00 = Spectrum Pro II (SPII,VP5300), Vendi Pro(VP6300)
    '           AR 3.0.0 = PISCES (VP8800)
    Private Shared m_dictTLVOp_New_NEO_200_and_AR300 As Dictionary(Of String, tagTLVNewOp) = New Dictionary(Of String, tagTLVNewOp) From {
      {"4F", New tagTLVNewOp("4F", 16, 0, AddressOf ClassReaderProtocolIDGGR.m_pfuncCbComposer, , "Application Identifier, AID")}, _
     {"50", New tagTLVNewOp("50", 20, 0, AddressOf ClassReaderProtocolIDGGR.m_pfuncCbComposer, AddressOf tagTLVNewOp.PktParser2Printout_50_ApplicationLabel, "Application Label")}, _
     {"56", New tagTLVNewOp("56", 76, 0, AddressOf ClassReaderProtocolIDGGR.m_pfuncCbComposer, AddressOf tagTLVNewOp.PktParser2Printout_56_Track_1_Data, "Track 1 Data")}, _
     {"57", New tagTLVNewOp("57", 100, 0, AddressOf ClassReaderProtocolIDGGR.m_pfuncCbComposer, AddressOf tagTLVNewOp.PktParser2Printout_57_Track_2_Equivalent_Data, "Track 2 Equiv Data returned by card")}, _
     {"5A", New tagTLVNewOp("5A", 19, 0, AddressOf ClassReaderProtocolIDGGR.m_pfuncCbComposer, Nothing, "Application Primary Account Number, PAN")}, _
     {"61", New tagTLVNewOp("61", 252, 0, AddressOf ClassReaderProtocolIDGGR.m_pfuncCbComposer, AddressOf tagTLVNewOp.PktParser2Printout_61_ApplicationTemplate, "Application Template")}, _
     {"6F", New tagTLVNewOp("6F", 65535, 0, AddressOf ClassReaderProtocolIDGGR.m_pfuncCbComposer, AddressOf tagTLVNewOp.PktParser2Printout_6F_FileControlInformationTemplate, "File Control Information Template")}, _
     {"70", New tagTLVNewOp("70", 255, 0, AddressOf ClassReaderProtocolIDGGR.m_pfuncCbComposer, AddressOf tagTLVNewOp.PktParser2Printout_70_MC3_READRECORDResponseMessageTemplate_Or_Amex_ApplicationElementaryFile_ieAEF_DataTemplate)}, _
     {"77", New tagTLVNewOp("77", 65535, 0, AddressOf ClassReaderProtocolIDGGR.m_pfuncCbComposer, AddressOf tagTLVNewOp.PktParser2Printout_77_MC3_WithTagLenght_Or_Amex_WIthoutTagLength_ResponseMessageTemplateFormat2, "Response Message Template Format 2")}, _
     {"80", New tagTLVNewOp("80", 255, 0, AddressOf ClassReaderProtocolIDGGR.m_pfuncCbComposer, , "Response Message Template Format 1")}, _
     {"82", New tagTLVNewOp("82", 2, 0, AddressOf ClassReaderProtocolIDGGR.m_pfuncCbComposer, AddressOf tagTLVNewOp.PktParser2Printout_82_AIP, "Application Interchange Profile, AIP")}, _
     {"84", New tagTLVNewOp("84", 16, 0, AddressOf ClassReaderProtocolIDGGR.m_pfuncCbComposer, Nothing, "DF Name")}, _
     {"87", New tagTLVNewOp("87", 1, 0, AddressOf ClassReaderProtocolIDGGR.m_pfuncCbComposer, , "Application Priority Indicator")}, _
     {"88", New tagTLVNewOp("88", 1, 0, AddressOf ClassReaderProtocolIDGGR.m_pfuncCbComposer, AddressOf tagTLVNewOp.PktParser2Printout_88_Amex_ShortFileIdentifier_SFI, "Short File Identifier (SFI)")}, _
   {"89", New tagTLVNewOp("89", 6, 0, AddressOf ClassReaderProtocolIDGGR.m_pfuncCbComposer, AddressOf tagTLVNewOp.PktParser2Printout_89_Amex_AuthorizationCode, "Authorization Code")}, _
     {"8A", New tagTLVNewOp("8A", 2, 0, AddressOf ClassReaderProtocolIDGGR.m_pfuncCbComposer, AddressOf tagTLVNewOp.PktParser2Printout_8A_Amex_AuthorizationResponseCode, "Authorization Response Code, ARC")}, _
       {"8C", New tagTLVNewOp("8C", 252, 0, AddressOf ClassReaderProtocolIDGGR.m_pfuncCbComposer, , "CDOL1")}, _
       {"8D", New tagTLVNewOp("8D", 64, 0, AddressOf ClassReaderProtocolIDGGR.m_pfuncCbComposer, , "CDOL2")}, _
     {"8E", New tagTLVNewOp("8E", 252, 0, AddressOf ClassReaderProtocolIDGGR.m_pfuncCbComposer, AddressOf tagTLVNewOp.PktParser2Printout_8E_CVMList, "CVM List")}, _
     {"8F", New tagTLVNewOp("8F", 1, 0, AddressOf ClassReaderProtocolIDGGR.m_pfuncCbComposer, , "CA Public Key Index (Card)")}, _
     {"90", New tagTLVNewOp("90", 255, 0, AddressOf ClassReaderProtocolIDGGR.m_pfuncCbComposer, AddressOf tagTLVNewOp.PktParser2Printout_90_IssuerPublicKeyCertificate, "Issuer Public Key Certificate")}, _
     {"91", New tagTLVNewOp("91", 16, 0, AddressOf ClassReaderProtocolIDGGR.m_pfuncCbComposer, AddressOf tagTLVNewOp.PktParser2Printout_91_IssuerAuthenticationData, "Issuer Authentication Data")}, _
      {"92", New tagTLVNewOp("92", 255, 0, AddressOf ClassReaderProtocolIDGGR.m_pfuncCbComposer, AddressOf tagTLVNewOp.PktParser2Printout_92_IssuerPublicKeyRemainder, "Issuer Public Key Remainder")}, _
     {"93", New tagTLVNewOp("93", 248, 0, AddressOf ClassReaderProtocolIDGGR.m_pfuncCbComposer, , "Signed Application Data [Amex]")}, _
     {"94", New tagTLVNewOp("94", 252, 0, AddressOf ClassReaderProtocolIDGGR.m_pfuncCbComposer, AddressOf tagTLVNewOp.PktParser2Printout_94_ApplicationFileLocator, "Application File Locator")}, _
     {"95", New tagTLVNewOp("95", 5, 0, AddressOf ClassReaderProtocolIDGGR.m_pfuncCbComposer, AddressOf tagTLVNewOp.PktParser2Printout_95_TVR, "Terminal Verification Results, TVR")}, _
      {"97", New tagTLVNewOp("97", 64, 0, AddressOf ClassReaderProtocolIDGGR.m_pfuncCbComposer, , "TDOL")}, _
     {"9A", New tagTLVNewOp("9A", 3, 0, AddressOf ClassReaderProtocolIDGGR.m_pfuncCbComposer, AddressOf tagTLVNewOp.PktParser2Printout_9A_TransactionDate, "Transaction Date")}, _
     {"9B", New tagTLVNewOp("9B", 2, 0, AddressOf ClassReaderProtocolIDGGR.m_pfuncCbComposer, , "Transaction Status Information (Amex)")}, _
     {"9C", New tagTLVNewOp("9C", 1, 0, AddressOf ClassReaderProtocolIDGGR.m_pfuncCbComposer, AddressOf tagTLVNewOp.PktParser2Printout_9C_TransactionType, "Transaction Type")}, _
     {"9D", New tagTLVNewOp("9D", 16, 0, AddressOf ClassReaderProtocolIDGGR.m_pfuncCbComposer, , "Directory Definition File (DDF) Name")}, _
     {"A5", New tagTLVNewOp("A5", 1, 0, AddressOf ClassReaderProtocolIDGGR.m_pfuncCbComposer, AddressOf tagTLVNewOp.PktParser2Printout_A5_FileControlInformationProprietaryTemplate, "File Control Information Proprietary Template")}, _
     {"E1", New tagTLVNewOp("E1", 1, 0, AddressOf ClassReaderProtocolIDGGR.m_pfuncCbComposer)}, _
     {"5F20", New tagTLVNewOp("5F20", 26, 0, AddressOf ClassReaderProtocolIDGGR.m_pfuncCbComposer, AddressOf tagTLVNewOp.PktParser2Printout_5F20_CardholderName, "Cardholder Name")}, _
     {"5F24", New tagTLVNewOp("5F24", 3, 0, AddressOf ClassReaderProtocolIDGGR.m_pfuncCbComposer, AddressOf tagTLVNewOp.PktParser2Printout_5F24_ApplicationExpirationDate, "Application Expiration Date")}, _
     {"5F25", New tagTLVNewOp("5F25", 3, 0, AddressOf ClassReaderProtocolIDGGR.m_pfuncCbComposer, AddressOf tagTLVNewOp.PktParser2Printout_5F25_ApplicationEffectiveDate, "Application Effective Date")}, _
       {"5F28", New tagTLVNewOp("5F28", 2, 0, AddressOf ClassReaderProtocolIDGGR.m_pfuncCbComposer, Nothing, "Issuer Contry Code")}, _
     {"5F2A", New tagTLVNewOp("5F2A", 2, 0, AddressOf ClassReaderProtocolIDGGR.m_pfuncCbComposer, Nothing, "Transaction Currency Code")}, _
     {"5F2D", New tagTLVNewOp("5F2D", 8, 0, AddressOf ClassReaderProtocolIDGGR.m_pfuncCbComposer, AddressOf tagTLVNewOp.PktParser2Printout_DF2D_LanguagePreference, "Language Preference")}, _
       {"5F30", New tagTLVNewOp("5F30", 2, 0, AddressOf ClassReaderProtocolIDGGR.m_pfuncCbComposer, Nothing, "Service Code")}, _
     {"5F34", New tagTLVNewOp("5F34", 1, 0, AddressOf ClassReaderProtocolIDGGR.m_pfuncCbComposer, , "Application PAN Sequence Number")}, _
     {"5F36", New tagTLVNewOp("5F36", 2, 0, AddressOf ClassReaderProtocolIDGGR.m_pfuncCbComposer, Nothing, "Transaction Currency Exponent")}, _
     {"5F57", New tagTLVNewOp("5F57", 1, 0, AddressOf ClassReaderProtocolIDGGR.m_pfuncCbComposer, , "Account Type")}, _
       {"8350", New tagTLVNewOp("8350", 10, 0, AddressOf ClassReaderProtocolIDGGR.m_pfuncCbComposer, , "Unknown Tag, Comes Inside Data Record (FF8150)")}, _
     {"9F01", New tagTLVNewOp("9F01", 6, 0, AddressOf ClassReaderProtocolIDGGR.m_pfuncCbComposer, , "Acquirer Identifier")}, _
     {"9F02", New tagTLVNewOp("9F02", 6, 0, AddressOf ClassReaderProtocolIDGGR.m_pfuncCbComposer, Nothing, "Amount Authorized")}, _
     {"9F03", New tagTLVNewOp("9F03", 6, 0, AddressOf ClassReaderProtocolIDGGR.m_pfuncCbComposer, Nothing, "Amount Other")}, _
     {"9F05", New tagTLVNewOp("9F05", 32, 0, AddressOf ClassReaderProtocolIDGGR.m_pfuncCbComposer, , "Application Discrtionary Data")}, _
     {"9F06", New tagTLVNewOp("9F06", 16, 0, AddressOf ClassReaderProtocolIDGGR.m_pfuncCbComposer, Nothing, "AID")}, _
       {"9F07", New tagTLVNewOp("9F07", 2, 0, AddressOf ClassReaderProtocolIDGGR.m_pfuncCbComposer, AddressOf tagTLVNewOp.PktParser2Printout_9F07_ApplicationUsageControl, "Application Usage Control")}, _
     {"9F08", New tagTLVNewOp("9F08", 2, 0, AddressOf ClassReaderProtocolIDGGR.m_pfuncCbComposer, AddressOf tagTLVNewOp.PktParser2Printout_9F08_ApplicationVersionNumber_Card_OR_Amex_ApplicationVersionNumber)}, _
     {"9F09", New tagTLVNewOp("9F09", 2, 0, AddressOf ClassReaderProtocolIDGGR.m_pfuncCbComposer, Nothing, "Application Version Number (Reader)")}, _
     {"9F0B", New tagTLVNewOp("9F0B", 45, 0, AddressOf ClassReaderProtocolIDGGR.m_pfuncCbComposer, AddressOf tagTLVNewOp.PktParser2Printout_9F0B_Amex_CardHolderName_Extended, "Card Holder Name - Extended")}, _
     {"9F0D", New tagTLVNewOp("9F0D", 5, 0, AddressOf ClassReaderProtocolIDGGR.m_pfuncCbComposer, , "Issuer Action Code - Default")}, _
     {"9F0E", New tagTLVNewOp("9F0E", 5, 0, AddressOf ClassReaderProtocolIDGGR.m_pfuncCbComposer, , "Issuer Action Code - Denial")}, _
     {"9F0F", New tagTLVNewOp("9F0F", 5, 0, AddressOf ClassReaderProtocolIDGGR.m_pfuncCbComposer, , "Issuer Action Code - Online")}, _
     {"9F10", New tagTLVNewOp("9F10", 32, 0, AddressOf ClassReaderProtocolIDGGR.m_pfuncCbComposer, Nothing, "Issuer Application Data (IAD)")}, _
     {"9F11", New tagTLVNewOp("9F11", 2, 0, AddressOf ClassReaderProtocolIDGGR.m_pfuncCbComposer, AddressOf tagTLVNewOp.PktParser2Printout_9F11_IssuerCodeTableIndex, "Issuer Code Table")}, _
     {"9F12", New tagTLVNewOp("9F12", 16, 0, AddressOf ClassReaderProtocolIDGGR.m_pfuncCbComposer, AddressOf tagTLVNewOp.PktParser2Printout_9F12_ApplicationPreferredName, "Application Preferred Name")}, _
     {"9F13", New tagTLVNewOp("9F13", 2, 0, AddressOf ClassReaderProtocolIDGGR.m_pfuncCbComposer, , "Last Online ATC Register")}, _
     {"9F15", New tagTLVNewOp("9F15", 15, 0, AddressOf ClassReaderProtocolIDGGR.m_pfuncCbComposer, , "Merchant Category Code")}, _
     {"9F16", New tagTLVNewOp("9F16", 15, 0, AddressOf ClassReaderProtocolIDGGR.m_pfuncCbComposer, AddressOf tagTLVNewOp.PktParser2Printout_9F16_MerchantIdentifier, "Merchant Identifier")}, _
     {"9F1A", New tagTLVNewOp("9F1A", 2, 0, AddressOf ClassReaderProtocolIDGGR.m_pfuncCbComposer, Nothing, "Terminal Country Code")}, _
     {"9F1B", New tagTLVNewOp("9F1B", 4, 0, AddressOf ClassReaderProtocolIDGGR.m_pfuncCbComposer, , "Terminal Floor Limit")}, _
     {"9F1C", New tagTLVNewOp("9F1C", 8, 0, AddressOf ClassReaderProtocolIDGGR.m_pfuncCbComposer, AddressOf tagTLVNewOp.PktParser2Printout_9F1C_TerminalIdentification, "Terminal Identification")}, _
     {"9F1E", New tagTLVNewOp("9F1E", 8, 0, AddressOf ClassReaderProtocolIDGGR.m_pfuncCbComposer, AddressOf tagTLVNewOp.PktParser2Printout_9F1E_InterfaceDeviceSerialNumber, "Interface Device Serial Number, Terminal IFD (allowed to be any value)")}, _
     {"9F1F", New tagTLVNewOp("9F1F", 255, 0, AddressOf ClassReaderProtocolIDGGR.m_pfuncCbComposer, AddressOf tagTLVNewOp.PktParser2Printout_9F1F_Track1DiscretionaryData, "Track 1 Discretionary Data")}, _
     {"9F20", New tagTLVNewOp("9F20", 255, 0, AddressOf ClassReaderProtocolIDGGR.m_pfuncCbComposer, AddressOf tagTLVNewOp.PktParser2Printout_9F20_Track2DiscretionaryData, "Track 2 Discretionary Data")}, _
     {"9F21", New tagTLVNewOp("9F21", 3, 0, AddressOf ClassReaderProtocolIDGGR.m_pfuncCbComposer, AddressOf tagTLVNewOp.PktParser2Printout_9F21_TransactionTime, "Transaction Time")}, _
     {"9F26", New tagTLVNewOp("9F26", 8, 0, AddressOf ClassReaderProtocolIDGGR.m_pfuncCbComposer, Nothing, "Application Cryptogram")}, _
     {"9F27", New tagTLVNewOp("9F27", 3, 0, AddressOf ClassReaderProtocolIDGGR.m_pfuncCbComposer, AddressOf tagTLVNewOp.PktParser2Printout_9F27_CryptogramInformationData, "Cryptogram Information Data ,CID")}, _
     {"9F2A", New tagTLVNewOp("9F2A", 20, 0, AddressOf ClassReaderProtocolIDGGR.m_pfuncCbComposer)}, _
     {"9F2D", New tagTLVNewOp("9F2D", 128, 0, AddressOf ClassReaderProtocolIDGGR.m_pfuncCbComposer, , "Application PIN Encipherment Public Key Certificate")}, _
     {"9F2E", New tagTLVNewOp("9F2E", 3, 0, AddressOf ClassReaderProtocolIDGGR.m_pfuncCbComposer, , "Application PIN Encipherment Public Key Exponent")}, _
     {"9F2F", New tagTLVNewOp("9F2F", 65535, 0, AddressOf ClassReaderProtocolIDGGR.m_pfuncCbComposer, , "Application PIN Encipherment Public Key Remainder")}, _
     {"9F32", New tagTLVNewOp("9F32", 3, 0, AddressOf ClassReaderProtocolIDGGR.m_pfuncCbComposer, , "Issuer Public Key Exponent")}, _
     {"9F33", New tagTLVNewOp("9F33", 3, 0, AddressOf ClassReaderProtocolIDGGR.m_pfuncCbComposer, AddressOf tagTLVNewOp.PktParser2Printout_9F33_TerminalCapabilities, "Terminal Capabilities")}, _
     {"9F34", New tagTLVNewOp("9F34", 3, 0, AddressOf ClassReaderProtocolIDGGR.m_pfuncCbComposer, AddressOf tagTLVNewOp.PktParser2Printout_9F34_CardholderVerificationMethod_Result, "CVM Result")}, _
     {"9F35", New tagTLVNewOp("9F35", 1, 0, AddressOf ClassReaderProtocolIDGGR.m_pfuncCbComposer, AddressOf tagTLVNewOp.PktParser2Printout_9F35_TerminalType, "Terminal Type")}, _
     {"9F36", New tagTLVNewOp("9F36", 2, 0, AddressOf ClassReaderProtocolIDGGR.m_pfuncCbComposer, Nothing, "Application Transaction Counter, ATC")}, _
     {"9F37", New tagTLVNewOp("9F37", 4, 0, AddressOf ClassReaderProtocolIDGGR.m_pfuncCbComposer, Nothing, "Unpredictable Number, UN")}, _
     {"9F38", New tagTLVNewOp("9F38", 65535, 0, AddressOf ClassReaderProtocolIDGGR.m_pfuncCbComposer, AddressOf tagTLVNewOp.PktParser2Printout_9F38_PDOL_MasterCard_Or_Amex, "PDOL")}, _
     {"9F39", New tagTLVNewOp("9F39", 1, 0, AddressOf ClassReaderProtocolIDGGR.m_pfuncCbComposer, AddressOf tagTLVNewOp.PktParser2Printout_9F39_POSEntryMode, "Point of Service (POS) Entry Mode")}, _
     {"9F40", New tagTLVNewOp("9F40", 5, 0, AddressOf ClassReaderProtocolIDGGR.m_pfuncCbComposer, AddressOf tagTLVNewOp.PktParser2Printout_9F40_AdditionalTerminalCapabilities, "Additional Terminal Capabilities")}, _
     {"9F41", New tagTLVNewOp("9F41", 4, 0, AddressOf ClassReaderProtocolIDGGR.m_pfuncCbComposer, , "Transaction Sequence Counter")}, _
     {"9F42", New tagTLVNewOp("9F42", 2, 0, AddressOf ClassReaderProtocolIDGGR.m_pfuncCbComposer, , "Application Currency Code")}, _
     {"9F44", New tagTLVNewOp("9F44", 1, 0, AddressOf ClassReaderProtocolIDGGR.m_pfuncCbComposer, , "Application Currency Exponent")}, _
     {"9F45", New tagTLVNewOp("9F45", 2, 0, AddressOf ClassReaderProtocolIDGGR.m_pfuncCbComposer, , "Data Authentication Code as a TLV object")}, _
     {"9F46", New tagTLVNewOp("9F46", 65535, 0, AddressOf ClassReaderProtocolIDGGR.m_pfuncCbComposer, AddressOf tagTLVNewOp.PktParser2Printout_9F46_MC3_ICCPublicKeyCertificate_OR_Amex_ApplicationCDAPublicKeyCertificate)}, _
     {"9F47", New tagTLVNewOp("9F47", 3, 0, AddressOf ClassReaderProtocolIDGGR.m_pfuncCbComposer, AddressOf tagTLVNewOp.PktParser2Printout_9F47_MC3_ICCPublicKeyExponent_OR_Amex_ApplicationCDAPublicKeyExponent)}, _
     {"9F48", New tagTLVNewOp("9F48", 65535, 0, AddressOf ClassReaderProtocolIDGGR.m_pfuncCbComposer, AddressOf tagTLVNewOp.PktParser2Printout_9F48_MC3_ICCPublicKeyRemainder_OR_Amex_ApplicationCDAPublicKeyRemainder)}, _
     {"9F49", New tagTLVNewOp("9F49", 32, 0, AddressOf ClassReaderProtocolIDGGR.m_pfuncCbComposer, , "Dynamic Data Authentication Data Object List (DDOL)")}, _
   {"9F4A", New tagTLVNewOp("9F4A", 65535, 0, AddressOf ClassReaderProtocolIDGGR.m_pfuncCbComposer, AddressOf tagTLVNewOp.PktParser2Printout_9F4A_StaticDataAuthenticationTagList_MasterCard_Or_Amex, "Static Data Authentication Tag List")}, _
     {"9F4B", New tagTLVNewOp("9F4B", 65535, 0, AddressOf ClassReaderProtocolIDGGR.m_pfuncCbComposer, , "Signed Dynamic Application Data")}, _
     {"9F4C", New tagTLVNewOp("9F4C", 8, 0, AddressOf ClassReaderProtocolIDGGR.m_pfuncCbComposer, , "ICC Dynamic Number")}, _
     {"9F4D", New tagTLVNewOp("9F4D", 2, 0, AddressOf ClassReaderProtocolIDGGR.m_pfuncCbComposer, , "Log Entry")}, _
     {"9F4E", New tagTLVNewOp("9F4E", 255, 0, AddressOf ClassReaderProtocolIDGGR.m_pfuncCbComposer, AddressOf tagTLVNewOp.PktParser2Printout_DF2D_LanguagePreference, "Merchant Name And Location")}, _
     {"9F50", New tagTLVNewOp("9F50", 6, 0, AddressOf ClassReaderProtocolIDGGR.m_pfuncCbComposer, , "Offline Accumulator Balance")}, _
     {"9F51", New tagTLVNewOp("9F51", 65535, 0, AddressOf ClassReaderProtocolIDGGR.m_pfuncCbComposer, , "DRDOL")}, _
     {"9F53", New tagTLVNewOp("9F53", 1, 0, AddressOf ClassReaderProtocolIDGGR.m_pfuncCbComposer, AddressOf tagTLVNewOp.PktParser2Printout_DF2D_LanguagePreference, "Transaction Category Code")}, _
     {"9F54", New tagTLVNewOp("9F54", 160, 0, AddressOf ClassReaderProtocolIDGGR.m_pfuncCbComposer, , "DS ODS Card")}, _
     {"9F58", New tagTLVNewOp("9F58", 1, 0, AddressOf ClassReaderProtocolIDGGR.m_pfuncCbComposer)}, _
     {"9F59", New tagTLVNewOp("9F59", 3, 0, AddressOf ClassReaderProtocolIDGGR.m_pfuncCbComposer)}, _
     {"9F5A", New tagTLVNewOp("9F5A", 1, 0, AddressOf ClassReaderProtocolIDGGR.m_pfuncCbComposer)}, _
     {"9F5B", New tagTLVNewOp("9F5B", 65535, 0, AddressOf ClassReaderProtocolIDGGR.m_pfuncCbComposer, , "DSDOL")}, _
     {"9F5C", New tagTLVNewOp("9F5C", 8, 0, AddressOf ClassReaderProtocolIDGGR.m_pfuncCbComposer, , "DS Requested Operator ID")}, _
     {"9F5D", New tagTLVNewOp("9F5D", 6, 0, AddressOf ClassReaderProtocolIDGGR.m_pfuncCbComposer, AddressOf tagTLVNewOp.PktParser2Printout_9F5D_AvailableOfflineSpendingAmount_Len6_AtVisa_CLBookC3_Or_ApplicationCapabilitiesInformation_Len3_AtMaster_CLBookC2, "Available Offline Spending Amount")}, _
     {"9F5E", New tagTLVNewOp("9F5E", 11, 0, AddressOf ClassReaderProtocolIDGGR.m_pfuncCbComposer, , "DS ID")}, _
     {"9F5F", New tagTLVNewOp("9F5F", 6, 0, AddressOf ClassReaderProtocolIDGGR.m_pfuncCbComposer, AddressOf tagTLVNewOp.PktParser2Printout_9F5F_DSSlotAvailability, "DS Slot Availability")}, _
     {"9F60", New tagTLVNewOp("9F60", 6, 0, AddressOf ClassReaderProtocolIDGGR.m_pfuncCbComposer, AddressOf tagTLVNewOp.PktParser2Printout_9F60_MC_CVC3_Track1_Or_Amex_ContactlessCumulativeTotalTransactionAmountLowerLimit)}, _
     {"9F61", New tagTLVNewOp("9F61", 6, 0, AddressOf ClassReaderProtocolIDGGR.m_pfuncCbComposer, AddressOf tagTLVNewOp.PktParser2Printout_9F61_MC_CVC3_Track2_Or_Amex_ContactlessCumulativeTotalTransactionAmountUpperLimit)}, _
     {"9F62", New tagTLVNewOp("9F62", 6, 0, AddressOf ClassReaderProtocolIDGGR.m_pfuncCbComposer, , "PCVC3 (Track 1)")}, _
     {"9F63", New tagTLVNewOp("9F63", 6, 0, AddressOf ClassReaderProtocolIDGGR.m_pfuncCbComposer, AddressOf tagTLVNewOp.PktParser2Printout_9F63_MC_PUNATC_Track1_Or_Amex_ContactlessNonDomesticConsecutiveTransactionCounter)}, _
     {"9F64", New tagTLVNewOp("9F64", 6, 0, AddressOf ClassReaderProtocolIDGGR.m_pfuncCbComposer, AddressOf tagTLVNewOp.PktParser2Printout_9F64_MC_NATC_Track1_Or_Amex_SingleTransactionValueUpperLimit)}, _
     {"9F65", New tagTLVNewOp("9F65", 6, 0, AddressOf ClassReaderProtocolIDGGR.m_pfuncCbComposer, AddressOf tagTLVNewOp.PktParser2Printout_9F65_MC_PCVC3_Track3_Or_Amex_SingleTransactionValueUpperLimitDualCurrency)}, _
     {"9F66", New tagTLVNewOp("9F66", 4, 0, AddressOf ClassReaderProtocolIDGGR.m_pfuncCbComposer, AddressOf tagTLVNewOp.PktParser2Printout_9F66_TTQInSpecK3AtVisaEMVCL0203_Or_PUNACTTrack2InSpecK2AtMasterCardEMVCL0203_Or_Amex_SingleTransactionValueLimitCheck_DualCurrencyCheck, "Terminal Transaction Qualifier,TTQ")}, _
       {"9F67", New tagTLVNewOp("9F67", 3, 0, AddressOf ClassReaderProtocolIDGGR.m_pfuncCbComposer, AddressOf tagTLVNewOp.PktParser2Printout_9F67_MC3_NATC_Track2_OR_Amex_PaymentDeviceTypeAndCapabilitiesIndicator)}, _
       {"9F69", New tagTLVNewOp("9F69", 65535, 0, AddressOf ClassReaderProtocolIDGGR.m_pfuncCbComposer, , "UDOL")}, _
       {"9F6A", New tagTLVNewOp("9F6A", 4, 0, AddressOf ClassReaderProtocolIDGGR.m_pfuncCbComposer, , "Unpredictable Number (Numeric)")}, _
       {"9F6B", New tagTLVNewOp("9F6B", 19, 0, AddressOf ClassReaderProtocolIDGGR.m_pfuncCbComposer, AddressOf tagTLVNewOp.PktParser2Printout_9F6B_Track_2_Data, "Track 2 Data")}, _
     {"9F6C", New tagTLVNewOp("9F6C", 2, 0, AddressOf ClassReaderProtocolIDGGR.m_pfuncCbComposer, Nothing, "Card Transaction Qualifiers")}, _
   {"9F6D", New tagTLVNewOp("9F6D", 2, 0, AddressOf ClassReaderProtocolIDGGR.m_pfuncCbComposer, AddressOf tagTLVNewOp.PktParser2Printout_Amex_9F6D_ExpresspayTerminalCapabilities)}, _
     {"9F6E", New tagTLVNewOp("9F6E", 32, 0, AddressOf ClassReaderProtocolIDGGR.m_pfuncCbComposer, , "Form Factor Indicator / PayPass Third Party Data")}, _
       {"9F6F", New tagTLVNewOp("9F6F", 1, 0, AddressOf ClassReaderProtocolIDGGR.m_pfuncCbComposer, AddressOf tagTLVNewOp.PktParser2Printout_9F6F_DSSlotManagementControl, "DS Slot Management Control")}, _
       {"9F70", New tagTLVNewOp("9F70", 192, 0, AddressOf ClassReaderProtocolIDGGR.m_pfuncCbComposer, , "Protected Data Envelope 1")}, _
       {"9F71", New tagTLVNewOp("9F71", 192, 0, AddressOf ClassReaderProtocolIDGGR.m_pfuncCbComposer, , "Protected Data Envelope 2")}, _
       {"9F72", New tagTLVNewOp("9F72", 192, 0, AddressOf ClassReaderProtocolIDGGR.m_pfuncCbComposer, , "Protected Data Envelope 3")}, _
       {"9F73", New tagTLVNewOp("9F73", 192, 0, AddressOf ClassReaderProtocolIDGGR.m_pfuncCbComposer, , "Protected Data Envelope 4")}, _
       {"9F74", New tagTLVNewOp("9F74", 192, 0, AddressOf ClassReaderProtocolIDGGR.m_pfuncCbComposer, , "Protected Data Envelope 5")}, _
       {"9F75", New tagTLVNewOp("9F75", 192, 0, AddressOf ClassReaderProtocolIDGGR.m_pfuncCbComposer, , "Unprotected Data Envelope 1")}, _
       {"9F76", New tagTLVNewOp("9F76", 192, 0, AddressOf ClassReaderProtocolIDGGR.m_pfuncCbComposer, , "Unprotected Data Envelope 2")}, _
       {"9F77", New tagTLVNewOp("9F77", 192, 0, AddressOf ClassReaderProtocolIDGGR.m_pfuncCbComposer, , "Unprotected Data Envelope 3")}, _
       {"9F78", New tagTLVNewOp("9F78", 192, 0, AddressOf ClassReaderProtocolIDGGR.m_pfuncCbComposer, , "Unprotected Data Envelope 4")}, _
       {"9F79", New tagTLVNewOp("9F79", 192, 0, AddressOf ClassReaderProtocolIDGGR.m_pfuncCbComposer, , "Unprotected Data Envelope 5")}, _
     {"9F7C", New tagTLVNewOp("9F7C", 32, 0, AddressOf ClassReaderProtocolIDGGR.m_pfuncCbComposer, AddressOf tagTLVNewOp.PktParser2Printout_9F7E_MobileSupportIndicator_MC3_MerchantCustomData_Or_Visa_CustomerExclusiveData, "Merchant Custom Data")}, _
     {"9F7D", New tagTLVNewOp("9F7D", 16, 0, AddressOf ClassReaderProtocolIDGGR.m_pfuncCbComposer, , "DS Summary 1")}, _
     {"9F7E", New tagTLVNewOp("9F7E", 1, 0, AddressOf ClassReaderProtocolIDGGR.m_pfuncCbComposer, AddressOf tagTLVNewOp.PktParser2Printout_9F7E_MobileSupportIndicator, "Mobile Support Indicator")}, _
         {"9F7F", New tagTLVNewOp("9F7F", 4, 0, AddressOf ClassReaderProtocolIDGGR.m_pfuncCbComposer, , "DS Unpredictable Number")}, _
     {"BF0C", New tagTLVNewOp("BF0C", 222, 0, AddressOf ClassReaderProtocolIDGGR.m_pfuncCbComposer, AddressOf tagTLVNewOp.PktParser2Printout_BF0C_FileControlInformationIssureDiscretionaryData, "File Control Information (FCI) Issuer Discretionary Data")}, _
 {"DF03", New tagTLVNewOp("DF03", 2, 0, AddressOf ClassReaderProtocolIDGGR.m_pfuncCbComposer, , "Application Default Action")}, _
     {"DFEE10", New tagTLVNewOp("DFEE10", 2, 0, AddressOf ClassReaderProtocolIDGGR.m_pfuncCbComposer)}, _
     {"DFEE13", New tagTLVNewOp("DFEE13", 5, 0, AddressOf ClassReaderProtocolIDGGR.m_pfuncCbComposer)}, _
     {"DFEE14", New tagTLVNewOp("DFEE14", 5, 0, AddressOf ClassReaderProtocolIDGGR.m_pfuncCbComposer)}, _
     {"DFEE04", New tagTLVNewOp("DFEE04", 5, 0, AddressOf ClassReaderProtocolIDGGR.m_pfuncCbComposer)}, _
     {"DFEE05", New tagTLVNewOp("DFEE05", 4, 0, AddressOf ClassReaderProtocolIDGGR.m_pfuncCbComposer)}, _
     {"DFEE06", New tagTLVNewOp("DFEE06", 1, 0, AddressOf ClassReaderProtocolIDGGR.m_pfuncCbComposer)}, _
     {"DFEE07", New tagTLVNewOp("DFEE07", 1, 0, AddressOf ClassReaderProtocolIDGGR.m_pfuncCbComposer)}, _
     {"DFEE0D", New tagTLVNewOp("DFEE0D", 1, 0, AddressOf ClassReaderProtocolIDGGR.m_pfuncCbComposer)}, _
     {"DFEE0E", New tagTLVNewOp("DFEE0E", 9, 0, AddressOf ClassReaderProtocolIDGGR.m_pfuncCbComposer)}, _
     {"DFEE0F", New tagTLVNewOp("DFEE0F", 1, 0, AddressOf ClassReaderProtocolIDGGR.m_pfuncCbComposer)}, _
     {"DFEE3E", New tagTLVNewOp("DFEE3E", 1, 0, AddressOf ClassReaderProtocolIDGGR.m_pfuncCbComposer)}, _
     {"DFEE28", New tagTLVNewOp("DFEE28", 8, 0, AddressOf ClassReaderProtocolIDGGR.m_pfuncCbComposer, , "-Terminal Capabilities - No CVM Required - See 9F33, 2nd Byte,'28' For PPS_CVM_2")}, _
     {"DFEE29", New tagTLVNewOp("DFEE29", 3, 0, AddressOf ClassReaderProtocolIDGGR.m_pfuncCbComposer, , "-Terminal Capabilities - CVM Req. if trx amount >= to CVM limit, 'F8' For PPS_CVM_2")}, _
     {"DFEE2A", New tagTLVNewOp("DFEE2A", 6, 0, AddressOf ClassReaderProtocolIDGGR.m_pfuncCbComposer)}, _
     {"DFEE2B", New tagTLVNewOp("DFEE2B", 1, 0, AddressOf ClassReaderProtocolIDGGR.m_pfuncCbComposer)}, _
     {"DFEE2C", New tagTLVNewOp("DFEE2C", 1, 0, AddressOf ClassReaderProtocolIDGGR.m_pfuncCbComposer)}, _
     {"DFEE30", New tagTLVNewOp("DFEE30", 4, 0, AddressOf ClassReaderProtocolIDGGR.m_pfuncCbComposer, AddressOf tagTLVNewOp.PktParser2Printout_DF30_ViVOProprietaryTag_TrackDataSource, "-Track Data Source")}, _
     {"DFEE31", New tagTLVNewOp("DFEE31", 56, 0, AddressOf ClassReaderProtocolIDGGR.m_pfuncCbComposer, Nothing, "-DD Card Track 1 (MagStripe Card)")}, _
     {"DFEE32", New tagTLVNewOp("DFEE32", 13, 0, AddressOf ClassReaderProtocolIDGGR.m_pfuncCbComposer, Nothing, "-DD Card Track 2 (MagStripe Card)")}, _
     {"DFEE33", New tagTLVNewOp("DFEE33", 4, 0, AddressOf ClassReaderProtocolIDGGR.m_pfuncCbComposer, AddressOf tagTLVNewOp.PktParser2Printout_DF33_ViVOProprietaryTag_ReceiptRequirement, "- Interac Receipt Requirement")}, _
     {"DFEE40", New tagTLVNewOp("DFEE40", 22, 0, AddressOf ClassReaderProtocolIDGGR.m_pfuncCbComposer)}, _
     {"DFEE41", New tagTLVNewOp("DFEE41", 12, 0, AddressOf ClassReaderProtocolIDGGR.m_pfuncCbComposer)}, _
     {"DFEE42", New tagTLVNewOp("DFEE42", 16, 0, AddressOf ClassReaderProtocolIDGGR.m_pfuncCbComposer)}, _
     {"DFEE43", New tagTLVNewOp("DFEE43", 14, 0, AddressOf ClassReaderProtocolIDGGR.m_pfuncCbComposer)}, _
     {"DFEE45", New tagTLVNewOp("DFEE45", 1, 0, AddressOf ClassReaderProtocolIDGGR.m_pfuncCbComposer)}, _
     {"DFEE46", New tagTLVNewOp("DFEE43", 1, 0, AddressOf ClassReaderProtocolIDGGR.m_pfuncCbComposer)}, _
       {"DFEE49", New tagTLVNewOp("DFEE49", 9, 0, AddressOf ClassReaderProtocolIDGGR.m_pfuncCbComposer, AddressOf tagTLVNewOp.PktParser2Printout_DF49_ViVOProprietaryTag_PrePostPPSE, "Pre/Post PPSE")}, _
       {"DF4B", New tagTLVNewOp("DF4B", 3, 0, AddressOf ClassReaderProtocolIDGGR.m_pfuncCbComposer, AddressOf tagTLVNewOp.PktParser2Printout_DF4B_POSCardholderInteractionInformation, "POS Cardholder Interaction Information")}, _
     {"DF51", New tagTLVNewOp("DF51", 1, 0, AddressOf ClassReaderProtocolIDGGR.m_pfuncCbComposer, AddressOf tagTLVNewOp.PktParser2Printout_Amex_9F6D_ExpresspayTerminalCapabilities, "Amex Terminal Capability")}, _
     {"DFEE52", New tagTLVNewOp("DFEE52", 1, 0, AddressOf ClassReaderProtocolIDGGR.m_pfuncCbComposer, AddressOf tagTLVNewOp.PktParser2Printout_DF52_ViVOProprietaryTag_TransactionCVM, "-Transaction CVM")}, _
     {"DFEE57", New tagTLVNewOp("DFEE57", 2, 0, AddressOf ClassReaderProtocolIDGGR.m_pfuncCbComposer)}, _
     {"DFEE58", New tagTLVNewOp("DFEE58", 2, 0, AddressOf ClassReaderProtocolIDGGR.m_pfuncCbComposer)}, _
     {"DF59", New tagTLVNewOp("DF59", 1, 0, AddressOf ClassReaderProtocolIDGGR.m_pfuncCbComposer)}, _
     {"DFEE5B", New tagTLVNewOp("DFEE5B", 1, 0, AddressOf ClassReaderProtocolIDGGR.m_pfuncCbComposer, AddressOf tagTLVNewOp.PktParser2Printout_DF5B_ViVOProprietaryTag_TerminalEntryCapability, "-Terminal Entry Capability")}, _
     {"DFEE5C", New tagTLVNewOp("DFEE5C", 4, 0, AddressOf ClassReaderProtocolIDGGR.m_pfuncCbComposer)}, _
     {"DFEE60", New tagTLVNewOp("DFEE60", 8, 0, AddressOf ClassReaderProtocolIDGGR.m_pfuncCbComposer, , "-DS Input (Card)")}, _
     {"DFEE61", New tagTLVNewOp("DFEE61", 8, 0, AddressOf ClassReaderProtocolIDGGR.m_pfuncCbComposer, , "-DS Digest H")}, _
     {"DFEE62", New tagTLVNewOp("DFEE62", 1, 0, AddressOf ClassReaderProtocolIDGGR.m_pfuncCbComposer, AddressOf tagTLVNewOp.PktParser2Printout_DF62_DS_ODS_Info, "-DS ODS Info")}, _
     {"DFEE63", New tagTLVNewOp("DFEE63", 160, 0, AddressOf ClassReaderProtocolIDGGR.m_pfuncCbComposer, , "-DS ODS Term")}, _
     {"DFEE64", New tagTLVNewOp("DFEE64", 1, 0, AddressOf ClassReaderProtocolIDGGR.m_pfuncCbComposer)}, _
     {"DFEE65", New tagTLVNewOp("DFEE65", 1, 0, AddressOf ClassReaderProtocolIDGGR.m_pfuncCbComposer)}, _
     {"DFEE66", New tagTLVNewOp("DFEE66", 1, 0, AddressOf ClassReaderProtocolIDGGR.m_pfuncCbComposer)}, _
     {"DFEE68", New tagTLVNewOp("DFEE68", 1, 0, AddressOf ClassReaderProtocolIDGGR.m_pfuncCbComposer)}, _
     {"DFEE69", New tagTLVNewOp("DFEE69", 1, 0, AddressOf ClassReaderProtocolIDGGR.m_pfuncCbComposer)}, _
     {"DFEE6A", New tagTLVNewOp("DFEE6A", 1, 0, AddressOf ClassReaderProtocolIDGGR.m_pfuncCbComposer)}, _
     {"DFEF08", New tagTLVNewOp("DFEF08", 1, 0, AddressOf ClassReaderProtocolIDGGR.m_pfuncCbComposer)}, _
     {"DFEF0A", New tagTLVNewOp("DFEF0A", 2, 0, AddressOf ClassReaderProtocolIDGGR.m_pfuncCbComposer)}, _
     {"DFEF0B", New tagTLVNewOp("DFEF0B", 2, 0, AddressOf ClassReaderProtocolIDGGR.m_pfuncCbComposer)}, _
     {"DFEF0E", New tagTLVNewOp("DFEF0E", 2, 0, AddressOf ClassReaderProtocolIDGGR.m_pfuncCbComposer)}, _
     {"DF74", New tagTLVNewOp("DF74", 1, 0, AddressOf ClassReaderProtocolIDGGR.m_pfuncCbComposer)}, _
     {"DF75", New tagTLVNewOp("DF75", 3, 0, AddressOf ClassReaderProtocolIDGGR.m_pfuncCbComposer)}, _
     {"DF76", New tagTLVNewOp("DF76", 5, 0, AddressOf ClassReaderProtocolIDGGR.m_pfuncCbComposer, AddressOf tagTLVNewOp.PktParser2Printout_DF76_TVRBeforeGenAC, "TVR before GenAC")}, _
     {"DF7C", New tagTLVNewOp("DF7C", 1, 0, AddressOf ClassReaderProtocolIDGGR.m_pfuncCbComposer)}, _
     {"DF7D", New tagTLVNewOp("DF7D", 1, 0, AddressOf ClassReaderProtocolIDGGR.m_pfuncCbComposer)}, _
     {"DF7E", New tagTLVNewOp("DF7E", 1, 0, AddressOf ClassReaderProtocolIDGGR.m_pfuncCbComposer)}, _
     {"DF7F", New tagTLVNewOp("DF7F", 1, 0, AddressOf ClassReaderProtocolIDGGR.m_pfuncCbComposer)}, _
     {"E300", New tagTLVNewOp("E300", 8, 0, AddressOf ClassReaderProtocolIDGGR.m_pfuncCbComposer, , "Application Cryptogram Auth Code")}, _
     {"FF67", New tagTLVNewOp("FF67", 5, 0, AddressOf ClassReaderProtocolIDGGR.m_pfuncCbComposer)}, _
     {"FF69", New tagTLVNewOp("FF69", 12, 0, AddressOf ClassReaderProtocolIDGGR.m_pfuncCbComposer, AddressOf tagTLVNewOp.PktParser2Printout_FF69_ViVOProprietaryTag, "FF69")}, _
     {"FF89", New tagTLVNewOp("FF89", 3, 0, AddressOf ClassReaderProtocolIDGGR.m_pfuncCbComposer)}, _
     {"DFEE4B", New tagTLVNewOp("DFEE4B", 1, 0, AddressOf ClassReaderProtocolIDGGR.m_pfuncCbComposer, , "-Enables Partial Selection")}, _
     {"DFEE4C", New tagTLVNewOp("DFEE4C", 1, 0, AddressOf ClassReaderProtocolIDGGR.m_pfuncCbComposer, , "-App Flow")}, _
     {"DFEE4D", New tagTLVNewOp("DFEE4D", 1, 0, AddressOf ClassReaderProtocolIDGGR.m_pfuncCbComposer, , "-Selection Features, turn on Kernel ID ")}, _
     {"DFEE2D", New tagTLVNewOp("DFEE2D", 1, 0, AddressOf ClassReaderProtocolIDGGR.m_pfuncCbComposer, , "-First Data element is always the group number")}, _
     {"DFEE2E", New tagTLVNewOp("DFEE2E", 1, 0, AddressOf ClassReaderProtocolIDGGR.m_pfuncCbComposer, , "-Specify Maximum Partial Selection Length / Max AID Length")}, _
     {"DFEE2F", New tagTLVNewOp("DFEE2F", 1, 0, AddressOf ClassReaderProtocolIDGGR.m_pfuncCbComposer, , "-AID Disabled")}, _
     {"DFEE4F", New tagTLVNewOp("DFEE4F", 1, 0, AddressOf ClassReaderProtocolIDGGR.m_pfuncCbComposer, , "-Interface Support")}, _
     {"DFEE53", New tagTLVNewOp("DFEE53", 1, 0, AddressOf ClassReaderProtocolIDGGR.m_pfuncCbComposer, , "-Exclude from Processing")}, _
     {"DFEE54", New tagTLVNewOp("DFEE54", 15, 0, AddressOf ClassReaderProtocolIDGGR.m_pfuncCbComposer, , "-Kernel ID Transaction type Group List [K|TT|GRP]")}, _
     {"DFEE59", New tagTLVNewOp("DFEE59", 1, 0, AddressOf ClassReaderProtocolIDGGR.m_pfuncCbComposer, , "-Default Kernel ID")}, _
     {"DFEE67", New tagTLVNewOp("DFEE67", 3, 0, AddressOf ClassReaderProtocolIDGGR.m_pfuncCbComposer, , "-ViVOtech internal tag - Specific Feature Switch")}, _
     {"DFEE34", New tagTLVNewOp("DFEE34", 6, 0, AddressOf ClassReaderProtocolIDGGR.m_pfuncCbComposer, , "-Terminal Contactless Transaction Limit")}, _
     {"DFEE6B", New tagTLVNewOp("DFEE6B", 8, 0, AddressOf ClassReaderProtocolIDGGR.m_pfuncCbComposer, , "ViVOtech internal tag - Terminal IFD Serial Number")}, _
     {"FFF3", New tagTLVNewOp("FFF3", 2, 0, AddressOf ClassReaderProtocolIDGGR.m_pfuncCbComposer, , "ViVOtech internal tag - Application Capability")}, _
     {"DFEE35", New tagTLVNewOp("DFEE35", 3, 0, AddressOf ClassReaderProtocolIDGGR.m_pfuncCbComposer, , "-Reader Risk Flags")}, _
     {"DFEE7E", New tagTLVNewOp("DFEE7E", 1, 0, AddressOf ClassReaderProtocolIDGGR.m_pfuncCbComposer, , "-ViVOtech internal tag - Enable/Disable Burst Mode")}, _
     {"DFEE37", New tagTLVNewOp("DFEE37", 1, 0, AddressOf ClassReaderProtocolIDGGR.m_pfuncCbComposer, , "-UI Scheme")}, _
     {"DFEE1C", New tagTLVNewOp("DFEE1C", 1, 0, AddressOf ClassReaderProtocolIDGGR.m_pfuncCbComposer, , "-ViVOtech internal tag - LCD Font Size")}, _
     {"DFEF29", New tagTLVNewOp("DFEF29", 2, 0, AddressOf ClassReaderProtocolIDGGR.m_pfuncCbComposer, , "-ViVOtech internal tag - LCD Delay Size")}, _
     {"DFEE38", New tagTLVNewOp("DFEE38", 1, 0, AddressOf ClassReaderProtocolIDGGR.m_pfuncCbComposer, , "-LCD Language (English)")}, _
     {"DFEE39", New tagTLVNewOp("DFEE39", 1, 0, AddressOf ClassReaderProtocolIDGGR.m_pfuncCbComposer, , "-Kernel Configuration - supports both EMV and MStripe")}, _
     {"FFFF", New tagTLVNewOp("FFFF", 5, 0, AddressOf ClassReaderProtocolIDGGR.m_pfuncCbComposer, , "TAC Denial")}, _
     {"DF8104", New tagTLVNewOp("DF8104", 6, 0, AddressOf ClassReaderProtocolIDGGR.m_pfuncCbComposer, , "Balance Read Before Gen AC")}, _
     {"DF8105", New tagTLVNewOp("DF8105", 6, 0, AddressOf ClassReaderProtocolIDGGR.m_pfuncCbComposer, , "Balance Read After Gen AC")}, _
     {"DF8106", New tagTLVNewOp("DF8106", 255, 0, AddressOf ClassReaderProtocolIDGGR.m_pfuncCbComposer, , "Data Needed")}, _
     {"DF8107", New tagTLVNewOp("DF8107", 255, 0, AddressOf ClassReaderProtocolIDGGR.m_pfuncCbComposer, , "CDOL1 Related Data")}, _
     {"DF8108", New tagTLVNewOp("DF8108", 1, 0, AddressOf ClassReaderProtocolIDGGR.m_pfuncCbComposer, AddressOf tagTLVNewOp.PktParser2Printout_MasterCard_DF8108_DS_AC_Type, "DS AC Type")}, _
     {"DF8109", New tagTLVNewOp("DF8109", 8, 0, AddressOf ClassReaderProtocolIDGGR.m_pfuncCbComposer, , "DS Input (Term)")}, _
     {"DF810A", New tagTLVNewOp("DF810A", 1, 0, AddressOf ClassReaderProtocolIDGGR.m_pfuncCbComposer, AddressOf tagTLVNewOp.PktParser2Printout_MasterCard_DF810A_DS_ODS_InfoForReader, "DS ODS Info For Reader")}, _
     {"DF810B", New tagTLVNewOp("DF810B", 1, 0, AddressOf ClassReaderProtocolIDGGR.m_pfuncCbComposer, AddressOf tagTLVNewOp.PktParser2Printout_MasterCard_DF810B_DS_Summary_Status, "DS Summary Status")}, _
     {"DF810C", New tagTLVNewOp("DF810C", 1, 0, AddressOf ClassReaderProtocolIDGGR.m_pfuncCbComposer, , "Kernel ID")}, _
     {"DF810D", New tagTLVNewOp("DF810D", 255, 0, AddressOf ClassReaderProtocolIDGGR.m_pfuncCbComposer, , "DSVN Term")}, _
     {"DF810E", New tagTLVNewOp("DF810E", 1, 0, AddressOf ClassReaderProtocolIDGGR.m_pfuncCbComposer, AddressOf tagTLVNewOp.PktParser2Printout_MasterCard_DF810E_Post_Gen_AC_Put_Data_Status, "Post-Gen AC Put Data Status")}, _
       {"DF810F", New tagTLVNewOp("DF810F", 1, 0, AddressOf ClassReaderProtocolIDGGR.m_pfuncCbComposer, AddressOf tagTLVNewOp.PktParser2Printout_MasterCard_DF810F_PreGenACPutDataStatus, "Pre-Gen AC Put Data Status")}, _
     {"DF8110", New tagTLVNewOp("DF8110", 255, 0, AddressOf ClassReaderProtocolIDGGR.m_pfuncCbComposer, , "Proceed To First Write Flag")}, _
     {"DF8111", New tagTLVNewOp("DF8111", 255, 0, AddressOf ClassReaderProtocolIDGGR.m_pfuncCbComposer, , "PDOL  Related Data")}, _
       {"DF8112", New tagTLVNewOp("DF8112", 255, 0, AddressOf ClassReaderProtocolIDGGR.m_pfuncCbComposer, , "Tags To Read")}, _
     {"DF8113", New tagTLVNewOp("DF8113", 255, 0, AddressOf ClassReaderProtocolIDGGR.m_pfuncCbComposer, , "DRDOL Related Data")}, _
     {"DF8114", New tagTLVNewOp("DF8114", 1, 0, AddressOf ClassReaderProtocolIDGGR.m_pfuncCbComposer, AddressOf tagTLVNewOp.PktParser2Printout_MasterCard_DF8114_ReferenceControlParameter, "Reference Control Parameter")}, _
     {"DF8115", New tagTLVNewOp("DF8115", 6, 0, AddressOf ClassReaderProtocolIDGGR.m_pfuncCbComposer, AddressOf tagTLVNewOp.PktParser2Printout_MasterCard_DF8115_ErrorIndication, "Error Indication")}, _
     {"DF8116", New tagTLVNewOp("DF8116", 22, 0, AddressOf ClassReaderProtocolIDGGR.m_pfuncCbComposer, AddressOf tagTLVNewOp.PktParser2Printout_MasterCard_DF8116_UserInterfaceRequestData, "User Interface Request Data")}, _
     {"DF8117", New tagTLVNewOp("DF8117", 1, 0, AddressOf ClassReaderProtocolIDGGR.m_pfuncCbComposer, AddressOf tagTLVNewOp.PktParser2Printout_DF8117_CVMCapability_CardDataInputCapability, "Card Data Input Capability")}, _
     {"DF8118", New tagTLVNewOp("DF8118", 1, 0, AddressOf ClassReaderProtocolIDGGR.m_pfuncCbComposer, AddressOf tagTLVNewOp.PktParser2Printout_DF8118_CVMCapability_CVMRequired, "CVM Capability – CVM Required")}, _
     {"DF8119", New tagTLVNewOp("DF8119", 1, 0, AddressOf ClassReaderProtocolIDGGR.m_pfuncCbComposer, AddressOf tagTLVNewOp.PktParser2Printout_DF8119_CVMCapability_NoCVMRequired, "CVM Capability – No CVM Required")}, _
     {"DF811A", New tagTLVNewOp("DF811A", 3, 0, AddressOf ClassReaderProtocolIDGGR.m_pfuncCbComposer, , "Default UDOL")}, _
     {"DF811B", New tagTLVNewOp("DF811B", 1, 0, AddressOf ClassReaderProtocolIDGGR.m_pfuncCbComposer, AddressOf tagTLVNewOp.PktParser2Printout_DF811B_KernelConfiguration, "Kernel Configuration")}, _
     {"DF811C", New tagTLVNewOp("DF811C", 2, 0, AddressOf ClassReaderProtocolIDGGR.m_pfuncCbComposer, , "Max Lifetime of Torn Transaction Log Record")}, _
     {"DF811D", New tagTLVNewOp("DF811D", 1, 0, AddressOf ClassReaderProtocolIDGGR.m_pfuncCbComposer, , "Max Number of Torn Transaction Log Records")}, _
     {"DF811E", New tagTLVNewOp("DF811E", 1, 0, AddressOf ClassReaderProtocolIDGGR.m_pfuncCbComposer, AddressOf tagTLVNewOp.PktParser2Printout_MasterCard_DF812C_MagStripeCVMCapabilityNoCVMRequired, "Mag-stripe CVM Capability – CVM Required")}, _
     {"DF811F", New tagTLVNewOp("DF811F", 1, 0, AddressOf ClassReaderProtocolIDGGR.m_pfuncCbComposer, AddressOf tagTLVNewOp.PktParser2Printout_DF811F_SecurityCapability, "Security Capability")}, _
     {"DF8120", New tagTLVNewOp("DF8120", 6, 0, AddressOf ClassReaderProtocolIDGGR.m_pfuncCbComposer, , "Terminal Action Code – Default")}, _
     {"DF8121", New tagTLVNewOp("DF8121", 6, 0, AddressOf ClassReaderProtocolIDGGR.m_pfuncCbComposer, , "Terminal Action Code – Daniel")}, _
     {"DF8122", New tagTLVNewOp("DF8122", 5, 0, AddressOf ClassReaderProtocolIDGGR.m_pfuncCbComposer, , "Terminal Action Code – Online")}, _
     {"DF8123", New tagTLVNewOp("DF8123", 6, 0, AddressOf ClassReaderProtocolIDGGR.m_pfuncCbComposer, , "Reader Contactless Floor Limit")}, _
     {"DF8124", New tagTLVNewOp("DF8124", 6, 0, AddressOf ClassReaderProtocolIDGGR.m_pfuncCbComposer, , "Reader Contactless Transaction Limit (No Ondevice CVM)")}, _
     {"DF8125", New tagTLVNewOp("DF8125", 6, 0, AddressOf ClassReaderProtocolIDGGR.m_pfuncCbComposer, , "Reader Contactless Transaction Limit (On-device CVM)")}, _
     {"DF8126", New tagTLVNewOp("DF8126", 6, 0, AddressOf ClassReaderProtocolIDGGR.m_pfuncCbComposer, , "Reader CVM Required Limit")}, _
     {"DF8127", New tagTLVNewOp("DF8127", 2, 0, AddressOf ClassReaderProtocolIDGGR.m_pfuncCbComposer, , "Time Out Value")}, _
   {"DF8128", New tagTLVNewOp("DF8128", 1, 0, AddressOf ClassReaderProtocolIDGGR.m_pfuncCbComposer, AddressOf tagTLVNewOp.PktParser2Printout_DF8128_IDSStatus, "IDS Status")}, _
     {"DF8129", New tagTLVNewOp("DF8129", 8, 0, AddressOf ClassReaderProtocolIDGGR.m_pfuncCbComposer, AddressOf tagTLVNewOp.PktParser2Printout_MasterCard_DF8129_OutcomeParameterSet, "Outcome Parameter Set")}, _
     {"DF812A", New tagTLVNewOp("DF812A", 56, 0, AddressOf ClassReaderProtocolIDGGR.m_pfuncCbComposer, AddressOf tagTLVNewOp.PktParser2Printout_DF2D_LanguagePreference, "DD Card (Track 1)")}, _
     {"DF812B", New tagTLVNewOp("DF812B", 8, 0, AddressOf ClassReaderProtocolIDGGR.m_pfuncCbComposer, , "DD Card (Track 2)")}, _
     {"DF812C", New tagTLVNewOp("DF812C", 1, 0, AddressOf ClassReaderProtocolIDGGR.m_pfuncCbComposer, AddressOf tagTLVNewOp.PktParser2Printout_MasterCard_DF812C_MagStripeCVMCapabilityNoCVMRequired, "Mag-stripe CVM Capability – No CVM Required")}, _
     {"DF812D", New tagTLVNewOp("DF812D", 3, 0, AddressOf ClassReaderProtocolIDGGR.m_pfuncCbComposer, AddressOf tagTLVNewOp.PktParser2Printout_MasterCard_DF812D_MessageHoldTime, "Message Hold Time")}, _
     {"DF8130", New tagTLVNewOp("DF8130", 1, 0, AddressOf ClassReaderProtocolIDGGR.m_pfuncCbComposer, AddressOf tagTLVNewOp.PktParser2Printout_MasterCard_DF8130_HoldTimeValue, "Hold Time Value")}, _
     {"DF8131", New tagTLVNewOp("DF8131", 1, 0, AddressOf ClassReaderProtocolIDGGR.m_pfuncCbComposer, AddressOf tagTLVNewOp.PktParser2Printout_MasterCard_DF8131_PhoneMessageTable, "Phone Message Table")}, _
     {"DF891A", New tagTLVNewOp("DF819A", 16, 0, AddressOf ClassReaderProtocolIDGGR.m_pfuncCbComposer)}, _
     {"DF891B", New tagTLVNewOp("DF819B", 1, 0, AddressOf ClassReaderProtocolIDGGR.m_pfuncCbComposer)}, _
     {"FF8101", New tagTLVNewOp("FF8101", 255, 0, AddressOf ClassReaderProtocolIDGGR.m_pfuncCbComposer, , "Torn Record")}, _
     {"FF8102", New tagTLVNewOp("FF8102", 255, 0, AddressOf ClassReaderProtocolIDGGR.m_pfuncCbComposer, AddressOf tagTLVNewOp.PktParser2Printout_MasterCard_FF8102_TagsToWriteBeforeGenAC, "Tags To Write Before Gen AC")}, _
     {"FF8103", New tagTLVNewOp("FF8103", 255, 0, AddressOf ClassReaderProtocolIDGGR.m_pfuncCbComposer, AddressOf tagTLVNewOp.PktParser2Printout_MasterCard_FF8103_TagsToWriteAfterGenAC, "Tags To Write After Gen AC")}, _
     {"FF8104", New tagTLVNewOp("FF8104", 255, 0, AddressOf ClassReaderProtocolIDGGR.m_pfuncCbComposer, , "Data To Send")}, _
     {"FF8105", New tagTLVNewOp("FF8105", 255, 0, AddressOf ClassReaderProtocolIDGGR.m_pfuncCbComposer, AddressOf tagTLVNewOp.PktParser2Printout_MasterCard_FF8105_DataRecord, "Data Record")}, _
     {"FF8106", New tagTLVNewOp("FF8106", 255, 0, AddressOf ClassReaderProtocolIDGGR.m_pfuncCbComposer, AddressOf tagTLVNewOp.PktParser2Printout_MasterCard_FF8106_DiscretionaryData, "Discretionary Data")}, _
     {"FF8901", New tagTLVNewOp("FF8901", 255, 0, AddressOf ClassReaderProtocolIDGGR.m_pfuncCbComposer, AddressOf tagTLVNewOp.PktParser2Printout_MasterCard_FF8901_UnknownTLV01, "Proprietary Unkown 1")}, _
     {"FF8902", New tagTLVNewOp("FF8902", 255, 0, AddressOf ClassReaderProtocolIDGGR.m_pfuncCbComposer, AddressOf tagTLVNewOp.PktParser2Printout_MasterCard_FF8901_UnknownTLV01, "Proprietary Unkown 2")}, _
     {"FFEE01", New tagTLVNewOp("FFEE01", 255, 0, AddressOf ClassReaderProtocolIDGGR.m_pfuncCbComposer, AddressOf tagTLVNewOp.PktParser2Printout_MasterCard_FFEE01_ViVOtechGroupTag, "ViVOtech TLV Group Tag")}, _
     {"FFEE02", New tagTLVNewOp("FFEE02", 255, 0, AddressOf ClassReaderProtocolIDGGR.m_pfuncCbComposer, , "ViVOpay Pre-PPSE Special Flow Group Tag")}, _
     {"FFEE03", New tagTLVNewOp("FFEE03", 255, 0, AddressOf ClassReaderProtocolIDGGR.m_pfuncCbComposer, , "ViVOpay Post-PPSE Special Flow Group Tag")}, _
     {"FFEE04", New tagTLVNewOp("FFEE04", 65535, 0, AddressOf ClassReaderProtocolIDGGR.m_pfuncCbComposer, AddressOf tagTLVNewOp.PktParser2Printout_MasterCard_FFEE04_ViVOtechMC3SignalDataTLV, "ViVOtech M/Chip3 Signal Data")}, _
     {"FFEE05", New tagTLVNewOp("FFEE05", 65535, 0, AddressOf ClassReaderProtocolIDGGR.m_pfuncCbComposer, AddressOf tagTLVNewOp.PktParser2Printout_MasterCard_FFEE05_ViVOtechMC3ProprietaryTag, "ViVOtech M/Chip3 Signal Marker")} _
    }
#End If
    '
#If PLATFORM_NEO100_AR215 Then


    'Legacy TLV Parser for NEW 1.00 and AR 2.1.5
    ' products, NEO 1.00 = Unipay III, Unipay 1.5, VP4880, BTPayMini(VP3300), Vendi
    '           AR 2.1.5 = Vend III
    Private Shared m_dictTLVOp_Legacy_NEO100_and_AR215_Older As Dictionary(Of String, tagTLVNewOp) = New Dictionary(Of String, tagTLVNewOp) From {
      {"4F", New tagTLVNewOp("4F", 16, 0, AddressOf ClassReaderProtocolIDGGR.m_pfuncCbComposer, , "Application Identifier, AID")}, _
     {"50", New tagTLVNewOp("50", 20, 0, AddressOf ClassReaderProtocolIDGGR.m_pfuncCbComposer, AddressOf tagTLVNewOp.PktParser2Printout_50_ApplicationLabel, "Application Label")}, _
     {"56", New tagTLVNewOp("56", 76, 0, AddressOf ClassReaderProtocolIDGGR.m_pfuncCbComposer, AddressOf tagTLVNewOp.PktParser2Printout_56_Track_1_Data, "Track 1 Data")}, _
     {"57", New tagTLVNewOp("57", 100, 0, AddressOf ClassReaderProtocolIDGGR.m_pfuncCbComposer, AddressOf tagTLVNewOp.PktParser2Printout_57_Track_2_Equivalent_Data, "Track 2 Equiv Data returned by card")}, _
     {"5A", New tagTLVNewOp("5A", 19, 0, AddressOf ClassReaderProtocolIDGGR.m_pfuncCbComposer, Nothing, "Application Primary Account Number, PAN")}, _
     {"61", New tagTLVNewOp("61", 252, 0, AddressOf ClassReaderProtocolIDGGR.m_pfuncCbComposer, AddressOf tagTLVNewOp.PktParser2Printout_61_ApplicationTemplate, "Application Template")}, _
     {"6F", New tagTLVNewOp("6F", 65535, 0, AddressOf ClassReaderProtocolIDGGR.m_pfuncCbComposer, AddressOf tagTLVNewOp.PktParser2Printout_6F_FileControlInformationTemplate, "File Control Information Template")}, _
     {"70", New tagTLVNewOp("70", 255, 0, AddressOf ClassReaderProtocolIDGGR.m_pfuncCbComposer, AddressOf tagTLVNewOp.PktParser2Printout_70_MC3_READRECORDResponseMessageTemplate_Or_Amex_ApplicationElementaryFile_ieAEF_DataTemplate)}, _
     {"77", New tagTLVNewOp("77", 65535, 0, AddressOf ClassReaderProtocolIDGGR.m_pfuncCbComposer, AddressOf tagTLVNewOp.PktParser2Printout_77_MC3_WithTagLenght_Or_Amex_WIthoutTagLength_ResponseMessageTemplateFormat2, "Response Message Template Format 2")}, _
     {"80", New tagTLVNewOp("80", 255, 0, AddressOf ClassReaderProtocolIDGGR.m_pfuncCbComposer, , "Response Message Template Format 1")}, _
     {"82", New tagTLVNewOp("82", 2, 0, AddressOf ClassReaderProtocolIDGGR.m_pfuncCbComposer, AddressOf tagTLVNewOp.PktParser2Printout_82_AIP, "Application Interchange Profile, AIP")}, _
     {"84", New tagTLVNewOp("84", 16, 0, AddressOf ClassReaderProtocolIDGGR.m_pfuncCbComposer, Nothing, "DF Name")}, _
     {"87", New tagTLVNewOp("87", 1, 0, AddressOf ClassReaderProtocolIDGGR.m_pfuncCbComposer, , "Application Priority Indicator")}, _
     {"88", New tagTLVNewOp("88", 1, 0, AddressOf ClassReaderProtocolIDGGR.m_pfuncCbComposer, AddressOf tagTLVNewOp.PktParser2Printout_88_Amex_ShortFileIdentifier_SFI, "Short File Identifier (SFI)")}, _
   {"89", New tagTLVNewOp("89", 6, 0, AddressOf ClassReaderProtocolIDGGR.m_pfuncCbComposer, AddressOf tagTLVNewOp.PktParser2Printout_89_Amex_AuthorizationCode, "Authorization Code")}, _
     {"8A", New tagTLVNewOp("8A", 2, 0, AddressOf ClassReaderProtocolIDGGR.m_pfuncCbComposer, AddressOf tagTLVNewOp.PktParser2Printout_8A_Amex_AuthorizationResponseCode, "Authorization Response Code, ARC")}, _
       {"8C", New tagTLVNewOp("8C", 252, 0, AddressOf ClassReaderProtocolIDGGR.m_pfuncCbComposer, , "CDOL1")}, _
       {"8D", New tagTLVNewOp("8D", 64, 0, AddressOf ClassReaderProtocolIDGGR.m_pfuncCbComposer, , "CDOL2")}, _
     {"8E", New tagTLVNewOp("8E", 252, 0, AddressOf ClassReaderProtocolIDGGR.m_pfuncCbComposer, AddressOf tagTLVNewOp.PktParser2Printout_8E_CVMList, "CVM List")}, _
     {"8F", New tagTLVNewOp("8F", 1, 0, AddressOf ClassReaderProtocolIDGGR.m_pfuncCbComposer, , "CA Public Key Index (Card)")}, _
     {"90", New tagTLVNewOp("90", 255, 0, AddressOf ClassReaderProtocolIDGGR.m_pfuncCbComposer, AddressOf tagTLVNewOp.PktParser2Printout_90_IssuerPublicKeyCertificate, "Issuer Public Key Certificate")}, _
     {"91", New tagTLVNewOp("91", 16, 0, AddressOf ClassReaderProtocolIDGGR.m_pfuncCbComposer, AddressOf tagTLVNewOp.PktParser2Printout_91_IssuerAuthenticationData, "Issuer Authentication Data")}, _
      {"92", New tagTLVNewOp("92", 255, 0, AddressOf ClassReaderProtocolIDGGR.m_pfuncCbComposer, AddressOf tagTLVNewOp.PktParser2Printout_92_IssuerPublicKeyRemainder, "Issuer Public Key Remainder")}, _
     {"93", New tagTLVNewOp("93", 248, 0, AddressOf ClassReaderProtocolIDGGR.m_pfuncCbComposer, , "Signed Application Data [Amex]")}, _
     {"94", New tagTLVNewOp("94", 252, 0, AddressOf ClassReaderProtocolIDGGR.m_pfuncCbComposer, AddressOf tagTLVNewOp.PktParser2Printout_94_ApplicationFileLocator, "Application File Locator")}, _
     {"95", New tagTLVNewOp("95", 5, 0, AddressOf ClassReaderProtocolIDGGR.m_pfuncCbComposer, AddressOf tagTLVNewOp.PktParser2Printout_95_TVR, "Terminal Verification Results, TVR")}, _
      {"97", New tagTLVNewOp("97", 64, 0, AddressOf ClassReaderProtocolIDGGR.m_pfuncCbComposer, , "TDOL")}, _
     {"9A", New tagTLVNewOp("9A", 3, 0, AddressOf ClassReaderProtocolIDGGR.m_pfuncCbComposer, AddressOf tagTLVNewOp.PktParser2Printout_9A_TransactionDate, "Transaction Date")}, _
     {"9B", New tagTLVNewOp("9B", 2, 0, AddressOf ClassReaderProtocolIDGGR.m_pfuncCbComposer, , "Transaction Status Information (Amex)")}, _
     {"9C", New tagTLVNewOp("9C", 1, 0, AddressOf ClassReaderProtocolIDGGR.m_pfuncCbComposer, AddressOf tagTLVNewOp.PktParser2Printout_9C_TransactionType, "Transaction Type")}, _
     {"9D", New tagTLVNewOp("9D", 16, 0, AddressOf ClassReaderProtocolIDGGR.m_pfuncCbComposer, , "Directory Definition File (DDF) Name")}, _
     {"A5", New tagTLVNewOp("A5", 1, 0, AddressOf ClassReaderProtocolIDGGR.m_pfuncCbComposer, AddressOf tagTLVNewOp.PktParser2Printout_A5_FileControlInformationProprietaryTemplate, "File Control Information Proprietary Template")}, _
     {"E1", New tagTLVNewOp("E1", 1, 0, AddressOf ClassReaderProtocolIDGGR.m_pfuncCbComposer)}, _
     {"5F20", New tagTLVNewOp("5F20", 26, 0, AddressOf ClassReaderProtocolIDGGR.m_pfuncCbComposer, AddressOf tagTLVNewOp.PktParser2Printout_5F20_CardholderName, "Cardholder Name")}, _
     {"5F24", New tagTLVNewOp("5F24", 3, 0, AddressOf ClassReaderProtocolIDGGR.m_pfuncCbComposer, AddressOf tagTLVNewOp.PktParser2Printout_5F24_ApplicationExpirationDate, "Application Expiration Date")}, _
     {"5F25", New tagTLVNewOp("5F25", 3, 0, AddressOf ClassReaderProtocolIDGGR.m_pfuncCbComposer, AddressOf tagTLVNewOp.PktParser2Printout_5F25_ApplicationEffectiveDate, "Application Effective Date")}, _
       {"5F28", New tagTLVNewOp("5F28", 2, 0, AddressOf ClassReaderProtocolIDGGR.m_pfuncCbComposer, Nothing, "Issuer Contry Code")}, _
     {"5F2A", New tagTLVNewOp("5F2A", 2, 0, AddressOf ClassReaderProtocolIDGGR.m_pfuncCbComposer, Nothing, "Transaction Currency Code")}, _
     {"5F2D", New tagTLVNewOp("5F2D", 8, 0, AddressOf ClassReaderProtocolIDGGR.m_pfuncCbComposer, AddressOf tagTLVNewOp.PktParser2Printout_DF2D_LanguagePreference, "Language Preference")}, _
       {"5F30", New tagTLVNewOp("5F30", 2, 0, AddressOf ClassReaderProtocolIDGGR.m_pfuncCbComposer, Nothing, "Service Code")}, _
     {"5F34", New tagTLVNewOp("5F34", 1, 0, AddressOf ClassReaderProtocolIDGGR.m_pfuncCbComposer, , "Application PAN Sequence Number")}, _
     {"5F36", New tagTLVNewOp("5F36", 2, 0, AddressOf ClassReaderProtocolIDGGR.m_pfuncCbComposer, Nothing, "Transaction Currency Exponent")}, _
     {"5F57", New tagTLVNewOp("5F57", 1, 0, AddressOf ClassReaderProtocolIDGGR.m_pfuncCbComposer, , "Account Type")}, _
       {"8350", New tagTLVNewOp("8350", 10, 0, AddressOf ClassReaderProtocolIDGGR.m_pfuncCbComposer, , "Unknown Tag, Comes Inside Data Record (FF8150)")}, _
     {"9F01", New tagTLVNewOp("9F01", 6, 0, AddressOf ClassReaderProtocolIDGGR.m_pfuncCbComposer, , "Acquirer Identifier")}, _
     {"9F02", New tagTLVNewOp("9F02", 6, 0, AddressOf ClassReaderProtocolIDGGR.m_pfuncCbComposer, Nothing, "Amount Authorized")}, _
     {"9F03", New tagTLVNewOp("9F03", 6, 0, AddressOf ClassReaderProtocolIDGGR.m_pfuncCbComposer, Nothing, "Amount Other")}, _
     {"9F05", New tagTLVNewOp("9F05", 32, 0, AddressOf ClassReaderProtocolIDGGR.m_pfuncCbComposer, , "Application Discrtionary Data")}, _
     {"9F06", New tagTLVNewOp("9F06", 16, 0, AddressOf ClassReaderProtocolIDGGR.m_pfuncCbComposer, Nothing, "AID")}, _
       {"9F07", New tagTLVNewOp("9F07", 2, 0, AddressOf ClassReaderProtocolIDGGR.m_pfuncCbComposer, AddressOf tagTLVNewOp.PktParser2Printout_9F07_ApplicationUsageControl, "Application Usage Control")}, _
     {"9F08", New tagTLVNewOp("9F08", 2, 0, AddressOf ClassReaderProtocolIDGGR.m_pfuncCbComposer, AddressOf tagTLVNewOp.PktParser2Printout_9F08_ApplicationVersionNumber_Card_OR_Amex_ApplicationVersionNumber)}, _
     {"9F09", New tagTLVNewOp("9F09", 2, 0, AddressOf ClassReaderProtocolIDGGR.m_pfuncCbComposer, Nothing, "Application Version Number (Reader)")}, _
     {"9F0B", New tagTLVNewOp("9F0B", 45, 0, AddressOf ClassReaderProtocolIDGGR.m_pfuncCbComposer, AddressOf tagTLVNewOp.PktParser2Printout_9F0B_Amex_CardHolderName_Extended, "Card Holder Name - Extended")}, _
     {"9F0D", New tagTLVNewOp("9F0D", 5, 0, AddressOf ClassReaderProtocolIDGGR.m_pfuncCbComposer, , "Issuer Action Code - Default")}, _
     {"9F0E", New tagTLVNewOp("9F0E", 5, 0, AddressOf ClassReaderProtocolIDGGR.m_pfuncCbComposer, , "Issuer Action Code - Denial")}, _
     {"9F0F", New tagTLVNewOp("9F0F", 5, 0, AddressOf ClassReaderProtocolIDGGR.m_pfuncCbComposer, , "Issuer Action Code - Online")}, _
     {"9F10", New tagTLVNewOp("9F10", 32, 0, AddressOf ClassReaderProtocolIDGGR.m_pfuncCbComposer, Nothing, "Issuer Application Data (IAD)")}, _
     {"9F11", New tagTLVNewOp("9F11", 2, 0, AddressOf ClassReaderProtocolIDGGR.m_pfuncCbComposer, AddressOf tagTLVNewOp.PktParser2Printout_9F11_IssuerCodeTableIndex, "Issuer Code Table")}, _
     {"9F12", New tagTLVNewOp("9F12", 16, 0, AddressOf ClassReaderProtocolIDGGR.m_pfuncCbComposer, AddressOf tagTLVNewOp.PktParser2Printout_9F12_ApplicationPreferredName, "Application Preferred Name")}, _
     {"9F13", New tagTLVNewOp("9F13", 2, 0, AddressOf ClassReaderProtocolIDGGR.m_pfuncCbComposer, , "Last Online ATC Register")}, _
     {"9F15", New tagTLVNewOp("9F15", 15, 0, AddressOf ClassReaderProtocolIDGGR.m_pfuncCbComposer, , "Merchant Category Code")}, _
     {"9F16", New tagTLVNewOp("9F16", 15, 0, AddressOf ClassReaderProtocolIDGGR.m_pfuncCbComposer, AddressOf tagTLVNewOp.PktParser2Printout_9F16_MerchantIdentifier, "Merchant Identifier")}, _
     {"9F1A", New tagTLVNewOp("9F1A", 2, 0, AddressOf ClassReaderProtocolIDGGR.m_pfuncCbComposer, Nothing, "Terminal Country Code")}, _
     {"9F1B", New tagTLVNewOp("9F1B", 4, 0, AddressOf ClassReaderProtocolIDGGR.m_pfuncCbComposer, , "Terminal Floor Limit")}, _
     {"9F1C", New tagTLVNewOp("9F1C", 8, 0, AddressOf ClassReaderProtocolIDGGR.m_pfuncCbComposer, AddressOf tagTLVNewOp.PktParser2Printout_9F1C_TerminalIdentification, "Terminal Identification")}, _
     {"9F1E", New tagTLVNewOp("9F1E", 8, 0, AddressOf ClassReaderProtocolIDGGR.m_pfuncCbComposer, AddressOf tagTLVNewOp.PktParser2Printout_9F1E_InterfaceDeviceSerialNumber, "Interface Device Serial Number, Terminal IFD (allowed to be any value)")}, _
     {"9F1F", New tagTLVNewOp("9F1F", 255, 0, AddressOf ClassReaderProtocolIDGGR.m_pfuncCbComposer, AddressOf tagTLVNewOp.PktParser2Printout_9F1F_Track1DiscretionaryData, "Track 1 Discretionary Data")}, _
     {"9F20", New tagTLVNewOp("9F20", 255, 0, AddressOf ClassReaderProtocolIDGGR.m_pfuncCbComposer, AddressOf tagTLVNewOp.PktParser2Printout_9F20_Track2DiscretionaryData, "Track 2 Discretionary Data")}, _
     {"9F21", New tagTLVNewOp("9F21", 3, 0, AddressOf ClassReaderProtocolIDGGR.m_pfuncCbComposer, AddressOf tagTLVNewOp.PktParser2Printout_9F21_TransactionTime, "Transaction Time")}, _
     {"9F26", New tagTLVNewOp("9F26", 8, 0, AddressOf ClassReaderProtocolIDGGR.m_pfuncCbComposer, Nothing, "Application Cryptogram")}, _
     {"9F27", New tagTLVNewOp("9F27", 3, 0, AddressOf ClassReaderProtocolIDGGR.m_pfuncCbComposer, AddressOf tagTLVNewOp.PktParser2Printout_9F27_CryptogramInformationData, "Cryptogram Information Data ,CID")}, _
     {"9F2A", New tagTLVNewOp("9F2A", 20, 0, AddressOf ClassReaderProtocolIDGGR.m_pfuncCbComposer)}, _
     {"9F2D", New tagTLVNewOp("9F2D", 128, 0, AddressOf ClassReaderProtocolIDGGR.m_pfuncCbComposer, , "Application PIN Encipherment Public Key Certificate")}, _
     {"9F2E", New tagTLVNewOp("9F2E", 3, 0, AddressOf ClassReaderProtocolIDGGR.m_pfuncCbComposer, , "Application PIN Encipherment Public Key Exponent")}, _
     {"9F2F", New tagTLVNewOp("9F2F", 65535, 0, AddressOf ClassReaderProtocolIDGGR.m_pfuncCbComposer, , "Application PIN Encipherment Public Key Remainder")}, _
     {"9F32", New tagTLVNewOp("9F32", 3, 0, AddressOf ClassReaderProtocolIDGGR.m_pfuncCbComposer, , "Issuer Public Key Exponent")}, _
     {"9F33", New tagTLVNewOp("9F33", 3, 0, AddressOf ClassReaderProtocolIDGGR.m_pfuncCbComposer, AddressOf tagTLVNewOp.PktParser2Printout_9F33_TerminalCapabilities, "Terminal Capabilities")}, _
     {"9F34", New tagTLVNewOp("9F34", 3, 0, AddressOf ClassReaderProtocolIDGGR.m_pfuncCbComposer, AddressOf tagTLVNewOp.PktParser2Printout_9F34_CardholderVerificationMethod_Result, "CVM Result")}, _
     {"9F35", New tagTLVNewOp("9F35", 1, 0, AddressOf ClassReaderProtocolIDGGR.m_pfuncCbComposer, AddressOf tagTLVNewOp.PktParser2Printout_9F35_TerminalType, "Terminal Type")}, _
     {"9F36", New tagTLVNewOp("9F36", 2, 0, AddressOf ClassReaderProtocolIDGGR.m_pfuncCbComposer, Nothing, "Application Transaction Counter, ATC")}, _
     {"9F37", New tagTLVNewOp("9F37", 4, 0, AddressOf ClassReaderProtocolIDGGR.m_pfuncCbComposer, Nothing, "Unpredictable Number, UN")}, _
     {"9F38", New tagTLVNewOp("9F38", 65535, 0, AddressOf ClassReaderProtocolIDGGR.m_pfuncCbComposer, AddressOf tagTLVNewOp.PktParser2Printout_9F38_PDOL_MasterCard_Or_Amex, "PDOL")}, _
     {"9F39", New tagTLVNewOp("9F39", 1, 0, AddressOf ClassReaderProtocolIDGGR.m_pfuncCbComposer, AddressOf tagTLVNewOp.PktParser2Printout_9F39_POSEntryMode, "Point of Service (POS) Entry Mode")}, _
     {"9F40", New tagTLVNewOp("9F40", 5, 0, AddressOf ClassReaderProtocolIDGGR.m_pfuncCbComposer, AddressOf tagTLVNewOp.PktParser2Printout_9F40_AdditionalTerminalCapabilities, "Additional Terminal Capabilities")}, _
     {"9F41", New tagTLVNewOp("9F41", 4, 0, AddressOf ClassReaderProtocolIDGGR.m_pfuncCbComposer, , "Transaction Sequence Counter")}, _
     {"9F42", New tagTLVNewOp("9F42", 2, 0, AddressOf ClassReaderProtocolIDGGR.m_pfuncCbComposer, , "Application Currency Code")}, _
     {"9F44", New tagTLVNewOp("9F44", 1, 0, AddressOf ClassReaderProtocolIDGGR.m_pfuncCbComposer, , "Application Currency Exponent")}, _
     {"9F45", New tagTLVNewOp("9F45", 2, 0, AddressOf ClassReaderProtocolIDGGR.m_pfuncCbComposer, , "Data Authentication Code as a TLV object")}, _
     {"9F46", New tagTLVNewOp("9F46", 65535, 0, AddressOf ClassReaderProtocolIDGGR.m_pfuncCbComposer, AddressOf tagTLVNewOp.PktParser2Printout_9F46_MC3_ICCPublicKeyCertificate_OR_Amex_ApplicationCDAPublicKeyCertificate)}, _
     {"9F47", New tagTLVNewOp("9F47", 3, 0, AddressOf ClassReaderProtocolIDGGR.m_pfuncCbComposer, AddressOf tagTLVNewOp.PktParser2Printout_9F47_MC3_ICCPublicKeyExponent_OR_Amex_ApplicationCDAPublicKeyExponent)}, _
     {"9F48", New tagTLVNewOp("9F48", 65535, 0, AddressOf ClassReaderProtocolIDGGR.m_pfuncCbComposer, AddressOf tagTLVNewOp.PktParser2Printout_9F48_MC3_ICCPublicKeyRemainder_OR_Amex_ApplicationCDAPublicKeyRemainder)}, _
     {"9F49", New tagTLVNewOp("9F49", 32, 0, AddressOf ClassReaderProtocolIDGGR.m_pfuncCbComposer, , "Dynamic Data Authentication Data Object List (DDOL)")}, _
   {"9F4A", New tagTLVNewOp("9F4A", 65535, 0, AddressOf ClassReaderProtocolIDGGR.m_pfuncCbComposer, AddressOf tagTLVNewOp.PktParser2Printout_9F4A_StaticDataAuthenticationTagList_MasterCard_Or_Amex, "Static Data Authentication Tag List")}, _
     {"9F4B", New tagTLVNewOp("9F4B", 65535, 0, AddressOf ClassReaderProtocolIDGGR.m_pfuncCbComposer, , "Signed Dynamic Application Data")}, _
     {"9F4C", New tagTLVNewOp("9F4C", 8, 0, AddressOf ClassReaderProtocolIDGGR.m_pfuncCbComposer, , "ICC Dynamic Number")}, _
     {"9F4D", New tagTLVNewOp("9F4D", 2, 0, AddressOf ClassReaderProtocolIDGGR.m_pfuncCbComposer, , "Log Entry")}, _
     {"9F4E", New tagTLVNewOp("9F4E", 255, 0, AddressOf ClassReaderProtocolIDGGR.m_pfuncCbComposer, AddressOf tagTLVNewOp.PktParser2Printout_DF2D_LanguagePreference, "Merchant Name And Location")}, _
     {"9F50", New tagTLVNewOp("9F50", 6, 0, AddressOf ClassReaderProtocolIDGGR.m_pfuncCbComposer, , "Offline Accumulator Balance")}, _
     {"9F51", New tagTLVNewOp("9F51", 65535, 0, AddressOf ClassReaderProtocolIDGGR.m_pfuncCbComposer, , "DRDOL")}, _
     {"9F53", New tagTLVNewOp("9F53", 1, 0, AddressOf ClassReaderProtocolIDGGR.m_pfuncCbComposer, AddressOf tagTLVNewOp.PktParser2Printout_DF2D_LanguagePreference, "Transaction Category Code")}, _
     {"9F54", New tagTLVNewOp("9F54", 160, 0, AddressOf ClassReaderProtocolIDGGR.m_pfuncCbComposer, , "DS ODS Card")}, _
     {"9F58", New tagTLVNewOp("9F58", 1, 0, AddressOf ClassReaderProtocolIDGGR.m_pfuncCbComposer)}, _
     {"9F59", New tagTLVNewOp("9F59", 3, 0, AddressOf ClassReaderProtocolIDGGR.m_pfuncCbComposer)}, _
     {"9F5A", New tagTLVNewOp("9F5A", 1, 0, AddressOf ClassReaderProtocolIDGGR.m_pfuncCbComposer)}, _
     {"9F5B", New tagTLVNewOp("9F5B", 65535, 0, AddressOf ClassReaderProtocolIDGGR.m_pfuncCbComposer, , "DSDOL")}, _
     {"9F5C", New tagTLVNewOp("9F5C", 8, 0, AddressOf ClassReaderProtocolIDGGR.m_pfuncCbComposer, , "DS Requested Operator ID")}, _
     {"9F5D", New tagTLVNewOp("9F5D", 6, 0, AddressOf ClassReaderProtocolIDGGR.m_pfuncCbComposer, AddressOf tagTLVNewOp.PktParser2Printout_9F5D_AvailableOfflineSpendingAmount_Len6_AtVisa_CLBookC3_Or_ApplicationCapabilitiesInformation_Len3_AtMaster_CLBookC2, "Available Offline Spending Amount")}, _
     {"9F5E", New tagTLVNewOp("9F5E", 11, 0, AddressOf ClassReaderProtocolIDGGR.m_pfuncCbComposer, , "DS ID")}, _
     {"9F5F", New tagTLVNewOp("9F5F", 6, 0, AddressOf ClassReaderProtocolIDGGR.m_pfuncCbComposer, AddressOf tagTLVNewOp.PktParser2Printout_9F5F_DSSlotAvailability, "DS Slot Availability")}, _
     {"9F60", New tagTLVNewOp("9F60", 6, 0, AddressOf ClassReaderProtocolIDGGR.m_pfuncCbComposer, AddressOf tagTLVNewOp.PktParser2Printout_9F60_MC_CVC3_Track1_Or_Amex_ContactlessCumulativeTotalTransactionAmountLowerLimit)}, _
     {"9F61", New tagTLVNewOp("9F61", 6, 0, AddressOf ClassReaderProtocolIDGGR.m_pfuncCbComposer, AddressOf tagTLVNewOp.PktParser2Printout_9F61_MC_CVC3_Track2_Or_Amex_ContactlessCumulativeTotalTransactionAmountUpperLimit)}, _
     {"9F62", New tagTLVNewOp("9F62", 6, 0, AddressOf ClassReaderProtocolIDGGR.m_pfuncCbComposer, , "PCVC3 (Track 1)")}, _
     {"9F63", New tagTLVNewOp("9F63", 6, 0, AddressOf ClassReaderProtocolIDGGR.m_pfuncCbComposer, AddressOf tagTLVNewOp.PktParser2Printout_9F63_MC_PUNATC_Track1_Or_Amex_ContactlessNonDomesticConsecutiveTransactionCounter)}, _
     {"9F64", New tagTLVNewOp("9F64", 6, 0, AddressOf ClassReaderProtocolIDGGR.m_pfuncCbComposer, AddressOf tagTLVNewOp.PktParser2Printout_9F64_MC_NATC_Track1_Or_Amex_SingleTransactionValueUpperLimit)}, _
     {"9F65", New tagTLVNewOp("9F65", 6, 0, AddressOf ClassReaderProtocolIDGGR.m_pfuncCbComposer, AddressOf tagTLVNewOp.PktParser2Printout_9F65_MC_PCVC3_Track3_Or_Amex_SingleTransactionValueUpperLimitDualCurrency)}, _
     {"9F66", New tagTLVNewOp("9F66", 4, 0, AddressOf ClassReaderProtocolIDGGR.m_pfuncCbComposer, AddressOf tagTLVNewOp.PktParser2Printout_9F66_TTQInSpecK3AtVisaEMVCL0203_Or_PUNACTTrack2InSpecK2AtMasterCardEMVCL0203_Or_Amex_SingleTransactionValueLimitCheck_DualCurrencyCheck, "Terminal Transaction Qualifier,TTQ")}, _
       {"9F67", New tagTLVNewOp("9F67", 3, 0, AddressOf ClassReaderProtocolIDGGR.m_pfuncCbComposer, AddressOf tagTLVNewOp.PktParser2Printout_9F67_MC3_NATC_Track2_OR_Amex_PaymentDeviceTypeAndCapabilitiesIndicator)}, _
       {"9F69", New tagTLVNewOp("9F69", 65535, 0, AddressOf ClassReaderProtocolIDGGR.m_pfuncCbComposer, , "UDOL")}, _
       {"9F6A", New tagTLVNewOp("9F6A", 4, 0, AddressOf ClassReaderProtocolIDGGR.m_pfuncCbComposer, , "Unpredictable Number (Numeric)")}, _
       {"9F6B", New tagTLVNewOp("9F6B", 19, 0, AddressOf ClassReaderProtocolIDGGR.m_pfuncCbComposer, AddressOf tagTLVNewOp.PktParser2Printout_9F6B_Track_2_Data, "Track 2 Data")}, _
     {"9F6C", New tagTLVNewOp("9F6C", 2, 0, AddressOf ClassReaderProtocolIDGGR.m_pfuncCbComposer, Nothing, "Card Transaction Qualifiers")}, _
   {"9F6D", New tagTLVNewOp("9F6D", 2, 0, AddressOf ClassReaderProtocolIDGGR.m_pfuncCbComposer, AddressOf tagTLVNewOp.PktParser2Printout_Amex_9F6D_ExpresspayTerminalCapabilities)}, _
     {"9F6E", New tagTLVNewOp("9F6E", 32, 0, AddressOf ClassReaderProtocolIDGGR.m_pfuncCbComposer, , "Form Factor Indicator / PayPass Third Party Data")}, _
       {"9F6F", New tagTLVNewOp("9F6F", 1, 0, AddressOf ClassReaderProtocolIDGGR.m_pfuncCbComposer, AddressOf tagTLVNewOp.PktParser2Printout_9F6F_DSSlotManagementControl, "DS Slot Management Control")}, _
       {"9F70", New tagTLVNewOp("9F70", 192, 0, AddressOf ClassReaderProtocolIDGGR.m_pfuncCbComposer, , "Protected Data Envelope 1")}, _
       {"9F71", New tagTLVNewOp("9F71", 192, 0, AddressOf ClassReaderProtocolIDGGR.m_pfuncCbComposer, , "Protected Data Envelope 2")}, _
       {"9F72", New tagTLVNewOp("9F72", 192, 0, AddressOf ClassReaderProtocolIDGGR.m_pfuncCbComposer, , "Protected Data Envelope 3")}, _
       {"9F73", New tagTLVNewOp("9F73", 192, 0, AddressOf ClassReaderProtocolIDGGR.m_pfuncCbComposer, , "Protected Data Envelope 4")}, _
       {"9F74", New tagTLVNewOp("9F74", 192, 0, AddressOf ClassReaderProtocolIDGGR.m_pfuncCbComposer, , "Protected Data Envelope 5")}, _
       {"9F75", New tagTLVNewOp("9F75", 192, 0, AddressOf ClassReaderProtocolIDGGR.m_pfuncCbComposer, , "Unprotected Data Envelope 1")}, _
       {"9F76", New tagTLVNewOp("9F76", 192, 0, AddressOf ClassReaderProtocolIDGGR.m_pfuncCbComposer, , "Unprotected Data Envelope 2")}, _
       {"9F77", New tagTLVNewOp("9F77", 192, 0, AddressOf ClassReaderProtocolIDGGR.m_pfuncCbComposer, , "Unprotected Data Envelope 3")}, _
       {"9F78", New tagTLVNewOp("9F78", 192, 0, AddressOf ClassReaderProtocolIDGGR.m_pfuncCbComposer, , "Unprotected Data Envelope 4")}, _
       {"9F79", New tagTLVNewOp("9F79", 192, 0, AddressOf ClassReaderProtocolIDGGR.m_pfuncCbComposer, , "Unprotected Data Envelope 5")}, _
     {"9F7C", New tagTLVNewOp("9F7C", 32, 0, AddressOf ClassReaderProtocolIDGGR.m_pfuncCbComposer, AddressOf tagTLVNewOp.PktParser2Printout_9F7E_MobileSupportIndicator_MC3_MerchantCustomData_Or_Visa_CustomerExclusiveData, "Merchant Custom Data")}, _
     {"9F7D", New tagTLVNewOp("9F7D", 16, 0, AddressOf ClassReaderProtocolIDGGR.m_pfuncCbComposer, , "DS Summary 1")}, _
     {"9F7E", New tagTLVNewOp("9F7E", 1, 0, AddressOf ClassReaderProtocolIDGGR.m_pfuncCbComposer, AddressOf tagTLVNewOp.PktParser2Printout_9F7E_MobileSupportIndicator, "Mobile Support Indicator")}, _
         {"9F7F", New tagTLVNewOp("9F7F", 4, 0, AddressOf ClassReaderProtocolIDGGR.m_pfuncCbComposer, , "DS Unpredictable Number")}, _
     {"BF0C", New tagTLVNewOp("BF0C", 222, 0, AddressOf ClassReaderProtocolIDGGR.m_pfuncCbComposer, AddressOf tagTLVNewOp.PktParser2Printout_BF0C_FileControlInformationIssureDiscretionaryData, "File Control Information (FCI) Issuer Discretionary Data")}, _
     {"DF03", New tagTLVNewOp("DF03", 2, 0, AddressOf ClassReaderProtocolIDGGR.m_pfuncCbComposer, , "Application Default Action")}, _
     {"DF10", New tagTLVNewOp("DF10", 2, 0, AddressOf ClassReaderProtocolIDGGR.m_pfuncCbComposer)}, _
     {"DF13", New tagTLVNewOp("DF13", 5, 0, AddressOf ClassReaderProtocolIDGGR.m_pfuncCbComposer)}, _
     {"DF14", New tagTLVNewOp("DF14", 5, 0, AddressOf ClassReaderProtocolIDGGR.m_pfuncCbComposer)}, _
     {"DF15", New tagTLVNewOp("DF15", 5, 0, AddressOf ClassReaderProtocolIDGGR.m_pfuncCbComposer)}, _
     {"DF17", New tagTLVNewOp("DF17", 4, 0, AddressOf ClassReaderProtocolIDGGR.m_pfuncCbComposer)}, _
     {"DF18", New tagTLVNewOp("DF18", 1, 0, AddressOf ClassReaderProtocolIDGGR.m_pfuncCbComposer)}, _
     {"DF19", New tagTLVNewOp("DF19", 1, 0, AddressOf ClassReaderProtocolIDGGR.m_pfuncCbComposer)}, _
     {"DF22", New tagTLVNewOp("DF22", 1, 0, AddressOf ClassReaderProtocolIDGGR.m_pfuncCbComposer)}, _
     {"DF25", New tagTLVNewOp("DF25", 9, 0, AddressOf ClassReaderProtocolIDGGR.m_pfuncCbComposer)}, _
     {"DF26", New tagTLVNewOp("DF26", 1, 0, AddressOf ClassReaderProtocolIDGGR.m_pfuncCbComposer)}, _
     {"DF27", New tagTLVNewOp("DF27", 1, 0, AddressOf ClassReaderProtocolIDGGR.m_pfuncCbComposer)}, _
     {"DF28", New tagTLVNewOp("DF28", 8, 0, AddressOf ClassReaderProtocolIDGGR.m_pfuncCbComposer, , "Terminal Capabilities - No CVM Required - See 9F33, 2nd Byte,'28' For PPS_CVM_2")}, _
     {"DF29", New tagTLVNewOp("DF29", 3, 0, AddressOf ClassReaderProtocolIDGGR.m_pfuncCbComposer, , "Terminal Capabilities - CVM Req. if trx amount >= to CVM limit, 'F8' For PPS_CVM_2")}, _
     {"DF2A", New tagTLVNewOp("DF2A", 6, 0, AddressOf ClassReaderProtocolIDGGR.m_pfuncCbComposer)}, _
     {"DF2B", New tagTLVNewOp("DF2B", 1, 0, AddressOf ClassReaderProtocolIDGGR.m_pfuncCbComposer)}, _
     {"DF2C", New tagTLVNewOp("DF2C", 1, 0, AddressOf ClassReaderProtocolIDGGR.m_pfuncCbComposer)}, _
     {"DF30", New tagTLVNewOp("DF30", 4, 0, AddressOf ClassReaderProtocolIDGGR.m_pfuncCbComposer, AddressOf tagTLVNewOp.PktParser2Printout_DF30_ViVOProprietaryTag_TrackDataSource, "Track Data Source")}, _
     {"DF31", New tagTLVNewOp("DF31", 56, 0, AddressOf ClassReaderProtocolIDGGR.m_pfuncCbComposer, Nothing, "DD Card Track 1 (MagStripe Card)")}, _
     {"DF32", New tagTLVNewOp("DF32", 13, 0, AddressOf ClassReaderProtocolIDGGR.m_pfuncCbComposer, Nothing, "DD Card Track 2 (MagStripe Card)")}, _
     {"DF33", New tagTLVNewOp("DF33", 4, 0, AddressOf ClassReaderProtocolIDGGR.m_pfuncCbComposer, AddressOf tagTLVNewOp.PktParser2Printout_DF33_ViVOProprietaryTag_ReceiptRequirement, "Receipt Requirement")}, _
     {"DF40", New tagTLVNewOp("DF40", 22, 0, AddressOf ClassReaderProtocolIDGGR.m_pfuncCbComposer)}, _
     {"DF41", New tagTLVNewOp("DF41", 12, 0, AddressOf ClassReaderProtocolIDGGR.m_pfuncCbComposer)}, _
     {"DF42", New tagTLVNewOp("DF42", 16, 0, AddressOf ClassReaderProtocolIDGGR.m_pfuncCbComposer)}, _
     {"DF43", New tagTLVNewOp("DF43", 14, 0, AddressOf ClassReaderProtocolIDGGR.m_pfuncCbComposer)}, _
     {"DF45", New tagTLVNewOp("DF43", 1, 0, AddressOf ClassReaderProtocolIDGGR.m_pfuncCbComposer)}, _
     {"DF46", New tagTLVNewOp("DF43", 1, 0, AddressOf ClassReaderProtocolIDGGR.m_pfuncCbComposer)}, _
       {"DF49", New tagTLVNewOp("DF49", 9, 0, AddressOf ClassReaderProtocolIDGGR.m_pfuncCbComposer, AddressOf tagTLVNewOp.PktParser2Printout_DF49_ViVOProprietaryTag_PrePostPPSE, "Pre/Post PPSE")}, _
       {"DF4B", New tagTLVNewOp("DF4B", 3, 0, AddressOf ClassReaderProtocolIDGGR.m_pfuncCbComposer, AddressOf tagTLVNewOp.PktParser2Printout_DF4B_POSCardholderInteractionInformation, "POS Cardholder Interaction Information")}, _
     {"DF51", New tagTLVNewOp("DF51", 1, 0, AddressOf ClassReaderProtocolIDGGR.m_pfuncCbComposer, AddressOf tagTLVNewOp.PktParser2Printout_Amex_9F6D_ExpresspayTerminalCapabilities, "Amex Terminal Capability")}, _
     {"DF52", New tagTLVNewOp("DF52", 1, 0, AddressOf ClassReaderProtocolIDGGR.m_pfuncCbComposer, AddressOf tagTLVNewOp.PktParser2Printout_DF52_ViVOProprietaryTag_TransactionCVM, "Transaction CVM")}, _
     {"DF57", New tagTLVNewOp("DF57", 2, 0, AddressOf ClassReaderProtocolIDGGR.m_pfuncCbComposer)}, _
     {"DF58", New tagTLVNewOp("DF58", 2, 0, AddressOf ClassReaderProtocolIDGGR.m_pfuncCbComposer)}, _
     {"DF59", New tagTLVNewOp("DF59", 1, 0, AddressOf ClassReaderProtocolIDGGR.m_pfuncCbComposer)}, _
     {"DF5B", New tagTLVNewOp("DF5B", 1, 0, AddressOf ClassReaderProtocolIDGGR.m_pfuncCbComposer, AddressOf tagTLVNewOp.PktParser2Printout_DF5B_ViVOProprietaryTag_TerminalEntryCapability, "Terminal Entry Capability")}, _
     {"DF5C", New tagTLVNewOp("DF5C", 4, 0, AddressOf ClassReaderProtocolIDGGR.m_pfuncCbComposer)}, _
     {"DF60", New tagTLVNewOp("DF60", 8, 0, AddressOf ClassReaderProtocolIDGGR.m_pfuncCbComposer, , "DS Input (Card)")}, _
     {"DF61", New tagTLVNewOp("DF61", 8, 0, AddressOf ClassReaderProtocolIDGGR.m_pfuncCbComposer, , "DS Digest H")}, _
     {"DF62", New tagTLVNewOp("DF62", 1, 0, AddressOf ClassReaderProtocolIDGGR.m_pfuncCbComposer, AddressOf tagTLVNewOp.PktParser2Printout_DF62_DS_ODS_Info, "DS ODS Info")}, _
     {"DF63", New tagTLVNewOp("DF63", 160, 0, AddressOf ClassReaderProtocolIDGGR.m_pfuncCbComposer, , "DS ODS Term")}, _
     {"DF64", New tagTLVNewOp("DF64", 1, 0, AddressOf ClassReaderProtocolIDGGR.m_pfuncCbComposer)}, _
     {"DF65", New tagTLVNewOp("DF65", 1, 0, AddressOf ClassReaderProtocolIDGGR.m_pfuncCbComposer)}, _
     {"DF66", New tagTLVNewOp("DF66", 1, 0, AddressOf ClassReaderProtocolIDGGR.m_pfuncCbComposer)}, _
     {"DF68", New tagTLVNewOp("DF68", 1, 0, AddressOf ClassReaderProtocolIDGGR.m_pfuncCbComposer)}, _
     {"DF69", New tagTLVNewOp("DF69", 1, 0, AddressOf ClassReaderProtocolIDGGR.m_pfuncCbComposer)}, _
     {"DF6A", New tagTLVNewOp("DF6A", 1, 0, AddressOf ClassReaderProtocolIDGGR.m_pfuncCbComposer)}, _
     {"DF70", New tagTLVNewOp("DF70", 1, 0, AddressOf ClassReaderProtocolIDGGR.m_pfuncCbComposer)}, _
     {"DF71", New tagTLVNewOp("DF71", 2, 0, AddressOf ClassReaderProtocolIDGGR.m_pfuncCbComposer)}, _
     {"DF72", New tagTLVNewOp("DF72", 2, 0, AddressOf ClassReaderProtocolIDGGR.m_pfuncCbComposer)}, _
     {"DF73", New tagTLVNewOp("DF73", 2, 0, AddressOf ClassReaderProtocolIDGGR.m_pfuncCbComposer)}, _
     {"DF74", New tagTLVNewOp("DF74", 1, 0, AddressOf ClassReaderProtocolIDGGR.m_pfuncCbComposer)}, _
     {"DF75", New tagTLVNewOp("DF75", 3, 0, AddressOf ClassReaderProtocolIDGGR.m_pfuncCbComposer)}, _
     {"DF76", New tagTLVNewOp("DF76", 5, 0, AddressOf ClassReaderProtocolIDGGR.m_pfuncCbComposer, AddressOf tagTLVNewOp.PktParser2Printout_DF76_TVRBeforeGenAC, "TVR before GenAC")}, _
     {"DF7C", New tagTLVNewOp("DF7C", 1, 0, AddressOf ClassReaderProtocolIDGGR.m_pfuncCbComposer)}, _
     {"DF7D", New tagTLVNewOp("DF7D", 1, 0, AddressOf ClassReaderProtocolIDGGR.m_pfuncCbComposer)}, _
     {"DF7E", New tagTLVNewOp("DF7E", 1, 0, AddressOf ClassReaderProtocolIDGGR.m_pfuncCbComposer)}, _
     {"DF7F", New tagTLVNewOp("DF7F", 1, 0, AddressOf ClassReaderProtocolIDGGR.m_pfuncCbComposer)}, _
     {"E300", New tagTLVNewOp("E300", 8, 0, AddressOf ClassReaderProtocolIDGGR.m_pfuncCbComposer, , "Application Cryptogram Auth Code")}, _
     {"FF67", New tagTLVNewOp("FF67", 5, 0, AddressOf ClassReaderProtocolIDGGR.m_pfuncCbComposer)}, _
     {"FF69", New tagTLVNewOp("FF69", 12, 0, AddressOf ClassReaderProtocolIDGGR.m_pfuncCbComposer, AddressOf tagTLVNewOp.PktParser2Printout_FF69_ViVOProprietaryTag, "FF69")}, _
     {"FF89", New tagTLVNewOp("FF89", 3, 0, AddressOf ClassReaderProtocolIDGGR.m_pfuncCbComposer)}, _
     {"FFE1", New tagTLVNewOp("FFE1", 1, 0, AddressOf ClassReaderProtocolIDGGR.m_pfuncCbComposer, , "Enables Partial Selection")}, _
     {"FFE2", New tagTLVNewOp("FFE2", 1, 0, AddressOf ClassReaderProtocolIDGGR.m_pfuncCbComposer, , "App Flow")}, _
     {"FFE3", New tagTLVNewOp("FFE3", 1, 0, AddressOf ClassReaderProtocolIDGGR.m_pfuncCbComposer, , "Selection Features, turn on Kernel ID ")}, _
      {"FFE4", New tagTLVNewOp("FFE4", 1, 0, AddressOf ClassReaderProtocolIDGGR.m_pfuncCbComposer, , "First Data element is always the group number")}, _
     {"FFE5", New tagTLVNewOp("FFE5", 1, 0, AddressOf ClassReaderProtocolIDGGR.m_pfuncCbComposer, , "Specify Maximum Partial Selection Length / Max AID Length")}, _
     {"FFE6", New tagTLVNewOp("FFE6", 1, 0, AddressOf ClassReaderProtocolIDGGR.m_pfuncCbComposer, , "AID Disabled")}, _
     {"FFE7", New tagTLVNewOp("FFE7", 1, 0, AddressOf ClassReaderProtocolIDGGR.m_pfuncCbComposer, , "Interface Support")}, _
     {"FFE8", New tagTLVNewOp("FFE8", 1, 0, AddressOf ClassReaderProtocolIDGGR.m_pfuncCbComposer, , "Exclude from Processing")}, _
     {"FFE9", New tagTLVNewOp("FFE9", 15, 0, AddressOf ClassReaderProtocolIDGGR.m_pfuncCbComposer, , "Kernel ID Transaction type Group List [K|TT|GRP]")}, _
     {"FFEA", New tagTLVNewOp("FFEA", 1, 0, AddressOf ClassReaderProtocolIDGGR.m_pfuncCbComposer, , "Default Kernel ID")}, _
     {"FFF0", New tagTLVNewOp("FFF0", 3, 0, AddressOf ClassReaderProtocolIDGGR.m_pfuncCbComposer, , "ViVOtech internal tag - Specific Feature Switch")}, _
     {"FFF1", New tagTLVNewOp("FFF1", 6, 0, AddressOf ClassReaderProtocolIDGGR.m_pfuncCbComposer, , "Terminal Contactless Transaction Limit")}, _
     {"FFF2", New tagTLVNewOp("FFF2", 8, 0, AddressOf ClassReaderProtocolIDGGR.m_pfuncCbComposer, , "ViVOtech internal tag - Terminal IFD Serial Number")}, _
     {"FFF3", New tagTLVNewOp("FFF3", 2, 0, AddressOf ClassReaderProtocolIDGGR.m_pfuncCbComposer, , "ViVOtech internal tag - Application Capability")}, _
     {"FFF4", New tagTLVNewOp("FFF4", 3, 0, AddressOf ClassReaderProtocolIDGGR.m_pfuncCbComposer, , "Reader Risk Flags")}, _
     {"FFF5", New tagTLVNewOp("FFF5", 6, 0, AddressOf ClassReaderProtocolIDGGR.m_pfuncCbComposer, , "CVM Required Limit")}, _
     {"FFF7", New tagTLVNewOp("FFF7", 1, 0, AddressOf ClassReaderProtocolIDGGR.m_pfuncCbComposer, , "ViVOtech internal tag - Enable/Disable Burst Mode")}, _
     {"FFF8", New tagTLVNewOp("FFF8", 1, 0, AddressOf ClassReaderProtocolIDGGR.m_pfuncCbComposer, , "UI Scheme")}, _
     {"FFF9", New tagTLVNewOp("FFF9", 1, 0, AddressOf ClassReaderProtocolIDGGR.m_pfuncCbComposer, , "ViVOtech internal tag - LCD Font Size")}, _
     {"FFFA", New tagTLVNewOp("FFFA", 2, 0, AddressOf ClassReaderProtocolIDGGR.m_pfuncCbComposer, , "ViVOtech internal tag - LCD Delay Size")}, _
     {"FFFB", New tagTLVNewOp("FFFB", 1, 0, AddressOf ClassReaderProtocolIDGGR.m_pfuncCbComposer, , "LCD Language (English)")}, _
     {"FFFC", New tagTLVNewOp("FFFC", 1, 0, AddressOf ClassReaderProtocolIDGGR.m_pfuncCbComposer, , "Kernel Configuration - supports both EMV and MStripe")}, _
     {"FFFD", New tagTLVNewOp("FFFD", 5, 0, AddressOf ClassReaderProtocolIDGGR.m_pfuncCbComposer, , "TAC Online")}, _
     {"FFFE", New tagTLVNewOp("FFFE", 5, 0, AddressOf ClassReaderProtocolIDGGR.m_pfuncCbComposer, , "TAC Default")}, _
     {"FFFF", New tagTLVNewOp("FFFF", 5, 0, AddressOf ClassReaderProtocolIDGGR.m_pfuncCbComposer, , "TAC Denial")}, _
     {"DF8104", New tagTLVNewOp("DF8104", 6, 0, AddressOf ClassReaderProtocolIDGGR.m_pfuncCbComposer, , "Balance Read Before Gen AC")}, _
     {"DF8105", New tagTLVNewOp("DF8105", 6, 0, AddressOf ClassReaderProtocolIDGGR.m_pfuncCbComposer, , "Balance Read After Gen AC")}, _
     {"DF8106", New tagTLVNewOp("DF8106", 255, 0, AddressOf ClassReaderProtocolIDGGR.m_pfuncCbComposer, , "Data Needed")}, _
     {"DF8107", New tagTLVNewOp("DF8107", 255, 0, AddressOf ClassReaderProtocolIDGGR.m_pfuncCbComposer, , "CDOL1 Related Data")}, _
     {"DF8108", New tagTLVNewOp("DF8108", 1, 0, AddressOf ClassReaderProtocolIDGGR.m_pfuncCbComposer, AddressOf tagTLVNewOp.PktParser2Printout_MasterCard_DF8108_DS_AC_Type, "DS AC Type")}, _
     {"DF8109", New tagTLVNewOp("DF8109", 8, 0, AddressOf ClassReaderProtocolIDGGR.m_pfuncCbComposer, , "DS Input (Term)")}, _
     {"DF810A", New tagTLVNewOp("DF810A", 1, 0, AddressOf ClassReaderProtocolIDGGR.m_pfuncCbComposer, AddressOf tagTLVNewOp.PktParser2Printout_MasterCard_DF810A_DS_ODS_InfoForReader, "DS ODS Info For Reader")}, _
     {"DF810B", New tagTLVNewOp("DF810B", 1, 0, AddressOf ClassReaderProtocolIDGGR.m_pfuncCbComposer, AddressOf tagTLVNewOp.PktParser2Printout_MasterCard_DF810B_DS_Summary_Status, "DS Summary Status")}, _
     {"DF810C", New tagTLVNewOp("DF810C", 1, 0, AddressOf ClassReaderProtocolIDGGR.m_pfuncCbComposer, , "Kernel ID")}, _
     {"DF810D", New tagTLVNewOp("DF810D", 255, 0, AddressOf ClassReaderProtocolIDGGR.m_pfuncCbComposer, , "DSVN Term")}, _
     {"DF810E", New tagTLVNewOp("DF810E", 1, 0, AddressOf ClassReaderProtocolIDGGR.m_pfuncCbComposer, AddressOf tagTLVNewOp.PktParser2Printout_MasterCard_DF810E_Post_Gen_AC_Put_Data_Status, "Post-Gen AC Put Data Status")}, _
       {"DF810F", New tagTLVNewOp("DF810F", 1, 0, AddressOf ClassReaderProtocolIDGGR.m_pfuncCbComposer, AddressOf tagTLVNewOp.PktParser2Printout_MasterCard_DF810F_PreGenACPutDataStatus, "Pre-Gen AC Put Data Status")}, _
     {"DF8110", New tagTLVNewOp("DF8110", 255, 0, AddressOf ClassReaderProtocolIDGGR.m_pfuncCbComposer, , "Proceed To First Write Flag")}, _
     {"DF8111", New tagTLVNewOp("DF8111", 255, 0, AddressOf ClassReaderProtocolIDGGR.m_pfuncCbComposer, , "PDOL  Related Data")}, _
       {"DF8112", New tagTLVNewOp("DF8112", 255, 0, AddressOf ClassReaderProtocolIDGGR.m_pfuncCbComposer, , "Tags To Read")}, _
     {"DF8113", New tagTLVNewOp("DF8113", 255, 0, AddressOf ClassReaderProtocolIDGGR.m_pfuncCbComposer, , "DRDOL Related Data")}, _
     {"DF8114", New tagTLVNewOp("DF8114", 1, 0, AddressOf ClassReaderProtocolIDGGR.m_pfuncCbComposer, AddressOf tagTLVNewOp.PktParser2Printout_MasterCard_DF8114_ReferenceControlParameter, "Reference Control Parameter")}, _
     {"DF8115", New tagTLVNewOp("DF8115", 6, 0, AddressOf ClassReaderProtocolIDGGR.m_pfuncCbComposer, AddressOf tagTLVNewOp.PktParser2Printout_MasterCard_DF8115_ErrorIndication, "Error Indication")}, _
     {"DF8116", New tagTLVNewOp("DF8116", 22, 0, AddressOf ClassReaderProtocolIDGGR.m_pfuncCbComposer, AddressOf tagTLVNewOp.PktParser2Printout_MasterCard_DF8116_UserInterfaceRequestData, "User Interface Request Data")}, _
     {"DF8117", New tagTLVNewOp("DF8117", 1, 0, AddressOf ClassReaderProtocolIDGGR.m_pfuncCbComposer, AddressOf tagTLVNewOp.PktParser2Printout_DF8117_CVMCapability_CardDataInputCapability, "Card Data Input Capability")}, _
     {"DF8118", New tagTLVNewOp("DF8118", 1, 0, AddressOf ClassReaderProtocolIDGGR.m_pfuncCbComposer, AddressOf tagTLVNewOp.PktParser2Printout_DF8118_CVMCapability_CVMRequired, "CVM Capability – CVM Required")}, _
     {"DF8119", New tagTLVNewOp("DF8119", 1, 0, AddressOf ClassReaderProtocolIDGGR.m_pfuncCbComposer, AddressOf tagTLVNewOp.PktParser2Printout_DF8119_CVMCapability_NoCVMRequired, "CVM Capability – No CVM Required")}, _
     {"DF811A", New tagTLVNewOp("DF811A", 3, 0, AddressOf ClassReaderProtocolIDGGR.m_pfuncCbComposer, , "Default UDOL")}, _
     {"DF811B", New tagTLVNewOp("DF811B", 1, 0, AddressOf ClassReaderProtocolIDGGR.m_pfuncCbComposer, AddressOf tagTLVNewOp.PktParser2Printout_DF811B_KernelConfiguration, "Kernel Configuration")}, _
     {"DF811C", New tagTLVNewOp("DF811C", 2, 0, AddressOf ClassReaderProtocolIDGGR.m_pfuncCbComposer, , "Max Lifetime of Torn Transaction Log Record")}, _
     {"DF811D", New tagTLVNewOp("DF811D", 1, 0, AddressOf ClassReaderProtocolIDGGR.m_pfuncCbComposer, , "Max Number of Torn Transaction Log Records")}, _
     {"DF811E", New tagTLVNewOp("DF811E", 1, 0, AddressOf ClassReaderProtocolIDGGR.m_pfuncCbComposer, AddressOf tagTLVNewOp.PktParser2Printout_MasterCard_DF812C_MagStripeCVMCapabilityNoCVMRequired, "Mag-stripe CVM Capability – CVM Required")}, _
     {"DF811F", New tagTLVNewOp("DF811F", 1, 0, AddressOf ClassReaderProtocolIDGGR.m_pfuncCbComposer, AddressOf tagTLVNewOp.PktParser2Printout_DF811F_SecurityCapability, "Security Capability")}, _
     {"DF8120", New tagTLVNewOp("DF8120", 6, 0, AddressOf ClassReaderProtocolIDGGR.m_pfuncCbComposer, , "Terminal Action Code – Default")}, _
     {"DF8121", New tagTLVNewOp("DF8121", 6, 0, AddressOf ClassReaderProtocolIDGGR.m_pfuncCbComposer, , "Terminal Action Code – Daniel")}, _
     {"DF8122", New tagTLVNewOp("DF8122", 5, 0, AddressOf ClassReaderProtocolIDGGR.m_pfuncCbComposer, , "Terminal Action Code – Online")}, _
     {"DF8123", New tagTLVNewOp("DF8123", 6, 0, AddressOf ClassReaderProtocolIDGGR.m_pfuncCbComposer, , "Reader Contactless Floor Limit")}, _
     {"DF8124", New tagTLVNewOp("DF8124", 6, 0, AddressOf ClassReaderProtocolIDGGR.m_pfuncCbComposer, , "Reader Contactless Transaction Limit (No Ondevice CVM)")}, _
     {"DF8125", New tagTLVNewOp("DF8125", 6, 0, AddressOf ClassReaderProtocolIDGGR.m_pfuncCbComposer, , "Reader Contactless Transaction Limit (On-device CVM)")}, _
     {"DF8126", New tagTLVNewOp("DF8126", 6, 0, AddressOf ClassReaderProtocolIDGGR.m_pfuncCbComposer, , "Reader CVM Required Limit")}, _
     {"DF8127", New tagTLVNewOp("DF8127", 2, 0, AddressOf ClassReaderProtocolIDGGR.m_pfuncCbComposer, , "Time Out Value")}, _
   {"DF8128", New tagTLVNewOp("DF8128", 1, 0, AddressOf ClassReaderProtocolIDGGR.m_pfuncCbComposer, AddressOf tagTLVNewOp.PktParser2Printout_DF8128_IDSStatus, "IDS Status")}, _
     {"DF8129", New tagTLVNewOp("DF8129", 8, 0, AddressOf ClassReaderProtocolIDGGR.m_pfuncCbComposer, AddressOf tagTLVNewOp.PktParser2Printout_MasterCard_DF8129_OutcomeParameterSet, "Outcome Parameter Set")}, _
     {"DF812A", New tagTLVNewOp("DF812A", 56, 0, AddressOf ClassReaderProtocolIDGGR.m_pfuncCbComposer, AddressOf tagTLVNewOp.PktParser2Printout_DF2D_LanguagePreference, "DD Card (Track 1)")}, _
     {"DF812B", New tagTLVNewOp("DF812B", 8, 0, AddressOf ClassReaderProtocolIDGGR.m_pfuncCbComposer, , "DD Card (Track 2)")}, _
     {"DF812C", New tagTLVNewOp("DF812C", 1, 0, AddressOf ClassReaderProtocolIDGGR.m_pfuncCbComposer, AddressOf tagTLVNewOp.PktParser2Printout_MasterCard_DF812C_MagStripeCVMCapabilityNoCVMRequired, "Mag-stripe CVM Capability – No CVM Required")}, _
     {"DF812D", New tagTLVNewOp("DF812D", 3, 0, AddressOf ClassReaderProtocolIDGGR.m_pfuncCbComposer, AddressOf tagTLVNewOp.PktParser2Printout_MasterCard_DF812D_MessageHoldTime, "Message Hold Time")}, _
     {"DF8130", New tagTLVNewOp("DF8130", 1, 0, AddressOf ClassReaderProtocolIDGGR.m_pfuncCbComposer, AddressOf tagTLVNewOp.PktParser2Printout_MasterCard_DF8130_HoldTimeValue, "Hold Time Value")}, _
     {"DF8131", New tagTLVNewOp("DF8131", 1, 0, AddressOf ClassReaderProtocolIDGGR.m_pfuncCbComposer, AddressOf tagTLVNewOp.PktParser2Printout_MasterCard_DF8131_PhoneMessageTable, "Phone Message Table")}, _
     {"DF891A", New tagTLVNewOp("DF819A", 16, 0, AddressOf ClassReaderProtocolIDGGR.m_pfuncCbComposer)}, _
     {"DF891B", New tagTLVNewOp("DF819B", 1, 0, AddressOf ClassReaderProtocolIDGGR.m_pfuncCbComposer)}, _
     {"FF8101", New tagTLVNewOp("FF8101", 255, 0, AddressOf ClassReaderProtocolIDGGR.m_pfuncCbComposer, , "Torn Record")}, _
     {"FF8102", New tagTLVNewOp("FF8102", 255, 0, AddressOf ClassReaderProtocolIDGGR.m_pfuncCbComposer, AddressOf tagTLVNewOp.PktParser2Printout_MasterCard_FF8102_TagsToWriteBeforeGenAC, "Tags To Write Before Gen AC")}, _
     {"FF8103", New tagTLVNewOp("FF8103", 255, 0, AddressOf ClassReaderProtocolIDGGR.m_pfuncCbComposer, AddressOf tagTLVNewOp.PktParser2Printout_MasterCard_FF8103_TagsToWriteAfterGenAC, "Tags To Write After Gen AC")}, _
     {"FF8104", New tagTLVNewOp("FF8104", 255, 0, AddressOf ClassReaderProtocolIDGGR.m_pfuncCbComposer, , "Data To Send")}, _
     {"FF8105", New tagTLVNewOp("FF8105", 255, 0, AddressOf ClassReaderProtocolIDGGR.m_pfuncCbComposer, AddressOf tagTLVNewOp.PktParser2Printout_MasterCard_FF8105_DataRecord, "Data Record")}, _
     {"FF8106", New tagTLVNewOp("FF8106", 255, 0, AddressOf ClassReaderProtocolIDGGR.m_pfuncCbComposer, AddressOf tagTLVNewOp.PktParser2Printout_MasterCard_FF8106_DiscretionaryData, "Discretionary Data")}, _
     {"FF8901", New tagTLVNewOp("FF8901", 255, 0, AddressOf ClassReaderProtocolIDGGR.m_pfuncCbComposer, AddressOf tagTLVNewOp.PktParser2Printout_MasterCard_FF8901_UnknownTLV01, "Proprietary Unkown 1")}, _
     {"FF8902", New tagTLVNewOp("FF8902", 255, 0, AddressOf ClassReaderProtocolIDGGR.m_pfuncCbComposer, AddressOf tagTLVNewOp.PktParser2Printout_MasterCard_FF8901_UnknownTLV01, "Proprietary Unkown 2")}, _
     {"FFEE01", New tagTLVNewOp("FFEE01", 255, 0, AddressOf ClassReaderProtocolIDGGR.m_pfuncCbComposer, AddressOf tagTLVNewOp.PktParser2Printout_MasterCard_FFEE01_ViVOtechGroupTag, "ViVOtech TLV Group Tag")}, _
     {"FFEE02", New tagTLVNewOp("FFEE02", 255, 0, AddressOf ClassReaderProtocolIDGGR.m_pfuncCbComposer, , "ViVOpay Pre-PPSE Special Flow Group Tag")}, _
     {"FFEE03", New tagTLVNewOp("FFEE03", 255, 0, AddressOf ClassReaderProtocolIDGGR.m_pfuncCbComposer, , "ViVOpay Post-PPSE Special Flow Group Tag")}, _
     {"FFEE04", New tagTLVNewOp("FFEE04", 65535, 0, AddressOf ClassReaderProtocolIDGGR.m_pfuncCbComposer, AddressOf tagTLVNewOp.PktParser2Printout_MasterCard_FFEE04_ViVOtechMC3SignalDataTLV, "ViVOtech M/Chip3 Signal Data")}, _
     {"FFEE05", New tagTLVNewOp("FFEE05", 65535, 0, AddressOf ClassReaderProtocolIDGGR.m_pfuncCbComposer, AddressOf tagTLVNewOp.PktParser2Printout_MasterCard_FFEE05_ViVOtechMC3ProprietaryTag, "ViVOtech M/Chip3 Signal Marker")} _
    }
#End If

    Public Shared Sub Init(Optional ByVal bIsNew3ByteTLV As Boolean = False)
        If (bIsNew3ByteTLV) Then
#If PLATFORM_NEO200_AR300 Then
            m_dictTLVOp_RefList_Major = m_dictTLVOp_New_NEO_200_and_AR300
#Else
             Throw New Exception("Undefined TLV Table m_dictTLVOp_New_NEO_200_and_AR300")
#End If
        Else

#If PLATFORM_NEO100_AR215 Then
            m_dictTLVOp_RefList_Major = m_dictTLVOp_Legacy_NEO100_and_AR215_Older
#Else
            Throw New Exception("Undefined TLV Table m_dictTLVOp_Legacy_NEO100_and_AR215_Older")
#End If
        End If
    End Sub
    '===============================================[ End of TLV Tables ]===============================================
    '

    Private Shared m_bDebug As Boolean = False

    ''----------------------------------------------------[Private Variables]----------------------------------------------------
    'Private m_refobjTxtParserConfigurations As ClassTxtFileParserConfigurations
    'Private m_refobjCTLM As ClassTableListMaster

    ''===========[ Constructor ]===========
    'Sub New(ByRef refobjTableListMaster As ClassTableListMaster, ByRef refobjTxtParserConfigurations As ClassTxtFileParserConfigurations)

    '    m_refobjTxtParserConfigurations = refobjTxtParserConfigurations
    '    'External Variables
    '    m_refobjCTLM = refobjTableListMaster
    '    'Setup Delegate(Callback) Functions
    '    'LogLn = New Form01_Main.LogLnDelegate(AddressOf WindowsApplication1.My.Forms.Form01_Main.LogLn)

    'End Sub
    Private Shared m_s_objInstance As ClassReaderCommanderParser
    Private m_pRefMF As Form01_Main = Nothing
    Private m_strCAPKConfigFilePath As String
    Private m_strConfigFilePath As String
    Private m_refobjCTLM As ClassTableListMaster
    Private m_winobjOFD As OpenFileDialog
    Private m_strTrailComment As String
    Private Shared m_listKeyGroupComment As List(Of String)
    Public m_lConfigTextLine As Long
    Public m_CAPK_bWait2ndTextLine As Boolean
    Public m_CAPK_strCurrentTabName As String

    Private m_refobjReaderCmder As ClassReaderProtocolBasic
    Private m_strInPre As String
    Private m_tagCAPKUnit As Object
    Private m_nCFADefM As EnumConfigurationsFormAddDefaultMode
    Private m_nConfigCmd As EnumConfigCmd

    '
    Public Shared m_dictConfigCmdTbl As Dictionary(Of String, tagTxtFileConfigurationsCmd) = New Dictionary(Of String, tagTxtFileConfigurationsCmd) From { _
       {"POLLMODE SET", New tagTxtFileConfigurationsCmd("POLLMODE SET", EnumConfigCmd.CONFIG_POLLMODE_SET)}, _
       {"END", New tagTxtFileConfigurationsCmd("END", EnumConfigCmd.CONFIG_END)}, _
       {"CONFIGURATION SET", New tagTxtFileConfigurationsCmd("CONFIGURATION SET", EnumConfigCmd.CONFIG_CONFIGURATION_SET)}, _
       {"AID SET", New tagTxtFileConfigurationsCmd("AID SET", EnumConfigCmd.CONFIG_AID_SET)}, _
       {"AID DELETE", New tagTxtFileConfigurationsCmd("AID DELETE", EnumConfigCmd.CONFIG_AID_DELETE)}, _
        {"GROUP SET", New tagTxtFileConfigurationsCmd("GROUP SET", EnumConfigCmd.CONFIG_GROUP_SET)}, _
       {"GROUP DELETE", New tagTxtFileConfigurationsCmd("GROUP DELETE", EnumConfigCmd.CONFIG_GROUP_DELETE)}, _
        {";", New tagTxtFileConfigurationsCmd(";", EnumConfigCmd.CONFIG_COMMENT)} _
                                     }
    Public Shared m_dictConfigCmdTbl2 As Dictionary(Of EnumConfigCmd, tagTxtFileConfigurationsCmd) = New Dictionary(Of EnumConfigCmd, tagTxtFileConfigurationsCmd)
    '

    Private Sub GetInstanceInit()
        m_pRefMF = Form01_Main.GetInstance()
        m_s_objInstance.m_refobjCTLM = ClassTableListMaster.GetInstance()
        m_s_objInstance.m_winobjOFD = New OpenFileDialog
        '======================[ Group / AID ]=========================
        m_tagConfigCmdPollmode = New tagConfigCmdUnitPollmode(EnumConfigCmd.CONFIG_POLLMODE_SET)
        m_tagConfigCmdUnitAID = New tagConfigCmdUnitAID(EnumConfigCmd.CONFIG_AID_SET)
        m_tagConfigCmdUnitGroup = New tagConfigCmdUnitGroup(EnumConfigCmd.CONFIG_GROUP_SET)

        m_colConfigCmdValidNext = New List(Of EnumConfigCmd)

        m_lConfigTextLine = 0
        m_lConfigTextLineKeywordToMakeTabName = tagConfigCmdUnitGroup.EnumValueRange.LINE_MAX

        m_nConfigCmdIndex = EnumConfigCmd.CONFIG_UNKNOWN

        'External Variables
        m_nCFADefM = EnumConfigurationsFormAddDefaultMode.CFADefM_STANDARD

        '======================[ CAPK ]=========================
        m_s_objInstance.m_strInPre = ""
        m_s_objInstance.m_CAPK_bWait2ndTextLine = False

        'm_refobjReaderCmder = m_pRefMF.m_refObjRdrCmder

        m_bIsParsedOk = False
        'Building Config Text File Command Table for parsing the file
        For Each configCmdIterator In m_dictConfigCmdTbl
            ' Key = Integer Value
            m_dictConfigCmdTbl2.Add(CType(configCmdIterator.Value.m_nCmdType, EnumConfigCmd), configCmdIterator.Value)
        Next
    End Sub

    Public Shared Function GetInstance() As ClassReaderCommanderParser
        If (m_s_objInstance Is Nothing) Then
            m_s_objInstance = New ClassReaderCommanderParser

            m_s_objInstance.GetInstanceInit()

            '=================[ Key Group Comment ]================
            m_listKeyGroupComment = New List(Of String)
            m_listKeyGroupComment.Add("MASTER")
            m_listKeyGroupComment.Add("TEST AID")
        End If

        Return m_s_objInstance
    End Function

    '============================================[Reader Configuration Group / AID  Data Parser]=====================================================
    ' INPUT: configGroup = Nothing
    'OUTPUT: configGroup with TLV Table
    Public Shared Sub Config_ParseFromReaderGroupInfo(ByRef tagIDGCmdDataInfo As tagIDGCmdData, ByRef o_sdictTLV As SortedDictionary(Of String, tagTLV), Optional ByVal bIsGroupDefault As Boolean = False)

        Dim arrayByteDataPortion() As Byte = Nothing

        Try

            ' 1. Get Raw Data
            tagIDGCmdDataInfo.ParserDataGetArrayRawData(arrayByteDataPortion)
            If (arrayByteDataPortion Is Nothing) Then
                Throw New Exception("No Response Data")
            End If

            '=========================[ New Parser ]============================
            '2. Parse it
            'Create Group Info Block ( = tagConfigCmdUnitGroup ) & Create Group's TLV Table
            'o_configGroup = New tagConfigCmdUnitGroup(EnumConfigCmd.CONFIG_GROUP_SET)
            TLV_ParseFromReaderMultipleTLVs(arrayByteDataPortion, o_sdictTLV)

            '================[ TLV Parser Loop End ]================

            'With o_configGroup
            '    '------------[Step 6]------------
            '    ' Set up Group Info Other Fields
            '    If .m_dictTLV.ContainsKey(tagTLV.TAG_FFE4_or_DFEE2D_GrpID) Then
            '        Dim tlvGrpID As tagTLV = .m_dictTLV.Item(tagTLV.TAG_FFE4_or_DFEE2D_GrpID)
            '        'Save Group ID
            '        .m_nGroupID = tlvGrpID.m_arrayTLVal(0)
            '    Else
            '        Throw New Exception("Can't Found Group ID")
            '    End If

            '    '------------[Step 7]------------
            '    'Put Group Info Block to ClassTableListMaster.Group List
            '    'If Same Group ID Block found, Replace Old Info with New One the List.
            '    .m_strUIDriverTabName = "Reader Group (" & .m_nGroupID & ")"

            'End With


        Catch ex As Exception
            LogLn("[Err Dumping]")
            Form01_Main.LogLnDumpData(arrayByteDataPortion, arrayByteDataPortion.Count)
            LogLn("[Err] ParseFromReaderGroupInfo() = " + ex.Message)
            ' Free configGroup
            'o_configGroup.Cleanup()
            'o_configGroup = Nothing
        Finally
            If (arrayByteDataPortion IsNot Nothing) Then
                Erase arrayByteDataPortion
            End If
        End Try

        '===============================[ Old Version ]===============================================

        'Dim arrayByteDataPortion() As Byte
        'Dim arrayByteTag() As Byte = New Byte(3) {}
        'Dim nLenHdrTrail As Integer = tagIDGCmdData.m_nPktHeader + tagIDGCmdData.m_nPktTrail
        'Dim strTag As String
        'Dim arrayTLVal() As Byte
        'Dim tlv As tagTLV
        'Dim tlvOp As tagTLVNewOp
        'Dim nOffset, nTlvTagLen, nTlvValLenFromDataPortion As Integer
        'Dim bResult As Boolean = False
        'Try
        '    ' Prepare data capacitor
        '    If (o_sdictTLV Is Nothing) Then
        '        o_sdictTLV = New SortedDictionary(Of String, tagTLV)
        '    Else
        '        ClassTableListMaster.CleanupConfigurationsTLVList(o_sdictTLV)
        '    End If
        '    '
        '    tagIDGCmdDataInfo.ParserDataGetArrayRawData(arrayByteDataPortion)
        '    If (arrayByteDataPortion Is Nothing) Then
        '        Throw New Exception("No Response Data")
        '    End If

        '    '================[ TLV Parser Loop Begin ]================
        '    nOffset = 0
        '    nTlvTagLen = 0
        '    While (True)
        '        'Start To Take Tag Length = 3-->2-->1 --> Failed if Not Found
        '        Select Case nTlvTagLen
        '            Case 0
        '                ' First Try
        '                nTlvTagLen = 3
        '            Case 1
        '                ' Failed to Detect All kind of Tag Length 3~1 bytes
        '                LogLn("[Err] TLV Parsing Config_ParseFromReaderGroupInfo() =  Failed")
        '                m_pRefMF.LogLnDumpData(arrayByteDataPortion, arrayByteDataPortion.Count)
        '                Exit Sub
        '            Case 2
        '                '------------[Step 4]------------
        '                'Start To Take Tag Length = 1 if Step 3 (=2) NOT Found
        '                nTlvTagLen = 1
        '            Case 3
        '                '------------[Step 3]------------
        '                'Start To Take Tag Length = 2 if Step 2 (=3) NOT Found
        '                nTlvTagLen = 2
        '        End Select
        '        '

        '        'It's possible remained data is smaller then nTlvTagLen
        '        If ((nOffset + nTlvTagLen) >= arrayByteDataPortion.Count) Then
        '            nTlvTagLen = arrayByteDataPortion.Count - nOffset - 1
        '            If (nTlvTagLen < 1) Then
        '                nTlvTagLen = 1
        '                Continue While
        '            End If
        '        End If


        '        Array.Copy(arrayByteDataPortion, nOffset, arrayByteTag, 0, nTlvTagLen)
        '        ClassTableListMaster.ConvertFromArrayByte2String(arrayByteTag, nTlvTagLen, strTag, False)
        '        If m_dictTLVOp.ContainsKey(strTag) Then
        '            tlvOp = m_dictTLVOp.Item(strTag)

        '            'Check Length is Valid
        '            nTlvValLenFromDataPortion = arrayByteDataPortion(nOffset + nTlvTagLen)
        '            If nTlvValLenFromDataPortion > tlvOp.m_nLen Then
        '                Throw New Exception("[Err] TLV (tag= " + strTag + " ) New Op Len = " & tlvOp.m_nLen & ", Get Len = " & nTlvValLenFromDataPortion)
        '            End If
        '            'Get Data & Parser
        '            ' tlvOp.PktParser(tlv, tagIDGCmdDataInfo)
        '            '-------[ Allocate Preferred Buffer Size to accommodate Value ]-------
        '            If arrayTLVal Is Nothing Then
        '                arrayTLVal = New Byte(nTlvValLenFromDataPortion - 1) {}
        '            ElseIf (arrayTLVal.Count < nTlvValLenFromDataPortion) Then
        '                Erase arrayTLVal
        '                arrayTLVal = New Byte(nTlvValLenFromDataPortion - 1) {}
        '            End If

        '            '
        '            Array.Copy(arrayByteDataPortion, nOffset + nTlvTagLen + 1, arrayTLVal, 0, nTlvValLenFromDataPortion)
        '            '
        '            tlv = New tagTLV(strTag, nTlvValLenFromDataPortion, arrayTLVal)
        '            'tlv = New tagTLV(strTag, tlvOp.m_nLen, arrayTLVal)
        '            '
        '            nOffset = nOffset + nTlvTagLen + 1 + nTlvValLenFromDataPortion
        '            ' Reset State Machine to Begin
        '            nTlvTagLen = 0

        '        Else
        '            '=====================[ Continue To Next Tag Length ]=====================
        '            Continue While ' Select Next Length
        '        End If

        '        '------------[Step 5 : Add New TLV to TLV Table ]------------

        '        Dim strTmp As String
        '        'If Found Add Parsed TLV (Or Replace) to TLV Table
        '        If o_sdictTLV.ContainsKey(tlv.m_strTag) Then
        '            ClassTableListMaster.ConvertFromArrayByte2String(tlv.m_arrayTLVal, strTmp)
        '            LogLn("[Warning] Duplicated TLV = " + tlv.m_strTag + ", Value = " + strTmp + ", Replace it as New One")
        '            o_sdictTLV.Remove(tlv.m_strTag)
        '        End If
        '        o_sdictTLV.Add(tlv.m_strTag, tlv)


        '        '---------------[Step 6 : End of TLV Parsing Progress]--------------
        '        If nOffset = arrayByteDataPortion.Count Then
        '            Exit While
        '        ElseIf nOffset > arrayByteDataPortion.Count Then
        '            Throw New Exception("TLV Data Error")
        '        End If
        '    End While
        '    '================[ TLV Parser Loop End ]================

        '    '------------[Step 6]------------
        '    If (Not bIsGroupDefault) Then
        '        ' Set up Group Info Other Fields
        '        If o_sdictTLV.ContainsKey(tagTLV.TAG_FFE4_or_DFEE2D_GrpID_GrpID) Then
        '            Dim tlvGrpID As tagTLV = o_sdictTLV.Item(tagTLV.TAG_FFE4_or_DFEE2D_GrpID_GrpID)
        '            'Show Group ID
        '            LogLn("Group ID  = " + tlvGrpID.m_arrayTLVal(0).ToString())
        '            bResult = True
        '        Else
        '            Dim txnResult As ClassTransaction.TAG_TXN_PARSE_RESPONSE_INFO = New ClassTransaction.TAG_TXN_PARSE_RESPONSE_INFO(tagIDGCmdDataInfo)
        '            LogLn("Can't Found Group ID, TLVs Count =" + o_sdictTLV.Count.ToString())
        '            ClassUIDriver.PrintoutTransactionResponseTLVs2PrintBuffer(txnResult, o_sdictTLV, "TLV Parsing Result")
        '            ClassUIDriver.PrintoutTransactionResponseMsgONLY(txnResult)
        '            txnResult.Cleanup()
        '            bResult = True
        '        End If
        '    Else
        '        bResult = True
        '    End If

        'Catch ex As Exception
        '    LogLn("[Err Dumping]")
        '    m_pRefMF.LogLnDumpData(arrayByteDataPortion, arrayByteDataPortion.Count)
        '    LogLn("[Err] Config_ParseFromReaderGroupInfo() = " + ex.Message)
        'Finally
        '    If arrayByteDataPortion IsNot Nothing Then
        '        Erase arrayByteDataPortion
        '    End If

        '    If arrayByteTag IsNot Nothing Then
        '        Erase arrayByteTag
        '    End If

        '    If arrayTLVal IsNot Nothing Then
        '        Erase arrayTLVal
        '    End If

        '    If (Not bResult) Then
        '        'Cleanup Every TLVs
        '        ClassTableListMaster.CleanupConfigurationsTLVList(o_sdictTLV)
        '    End If
        'End Try

    End Sub
    '
    Public Shared Sub Config_ParseFromReaderGroupInfo(ByRef tagIDGCmdDataInfo As tagIDGCmdData, ByRef o_configGroup As tagConfigCmdUnitGroup)

        Dim arrayByteDataPortion() As Byte = Nothing

        Try

            ' 1. Get Raw Data
            tagIDGCmdDataInfo.ParserDataGetArrayRawData(arrayByteDataPortion)
            If (arrayByteDataPortion Is Nothing) Then
                Throw New Exception("No Response Data")
            End If

            '=========================[ New Parser ]============================
            '2. Parse it
            'Create Group Info Block ( = tagConfigCmdUnitGroup ) & Create Group's TLV Table
            o_configGroup = New tagConfigCmdUnitGroup(EnumConfigCmd.CONFIG_GROUP_SET)
            TLV_ParseFromReaderMultipleTLVs(arrayByteDataPortion, o_configGroup.m_dictTLV)

            '================[ TLV Parser Loop End ]================

            With o_configGroup
                '------------[Step 6]------------
                ' Set up Group Info Other Fields
                If .m_dictTLV.ContainsKey(tagTLV.TAG_FFE4_or_DFEE2D_GrpID) Then
                    Dim tlvGrpID As tagTLV = .m_dictTLV.Item(tagTLV.TAG_FFE4_or_DFEE2D_GrpID)
                    'Save Group ID
                    .m_nGroupID = tlvGrpID.m_arrayTLVal(0)
                Else
                    Throw New Exception("Can't Found Group ID")
                End If

                '------------[Step 7]------------
                'Put Group Info Block to ClassTableListMaster.Group List
                'If Same Group ID Block found, Replace Old Info with New One the List.
                .m_strUIDriverTabName = "Reader Group (" & .m_nGroupID & ")"

            End With


        Catch ex As Exception
            LogLn("[Err Dumping]")
            Form01_Main.LogLnDumpData(arrayByteDataPortion, arrayByteDataPortion.Count)
            LogLn("[Err] ParseFromReaderGroupInfo() = " + ex.Message)
            ' Free configGroup
            o_configGroup.Cleanup()
            o_configGroup = Nothing
        Finally
            If (arrayByteDataPortion IsNot Nothing) Then
                Erase arrayByteDataPortion
            End If
        End Try

        '=========================================================================
        'Try
        '    '------------[Step 2]------------
        '    'Create Group Info Block ( = tagConfigCmdUnitGroup ) & Create Group's TLV Table
        '    o_configGroup = New tagConfigCmdUnitGroup(EnumConfigCmd.CONFIG_GROUP_SET)

        '    'Parsing TLV(s) Data
        '    Config_ParseFromReaderGroupInfo(tagIDGCmdDataInfo, o_configGroup.m_dictTLV)

        '    With o_configGroup
        '        '------------[Step 6]------------
        '        ' Set up Group Info Other Fields
        '        If .m_dictTLV.ContainsKey(tagTLV.TAG_FFE4_or_DFEE2D_GrpID_GrpID) Then
        '            Dim tlvGrpID As tagTLV = .m_dictTLV.Item(tagTLV.TAG_FFE4_or_DFEE2D_GrpID_GrpID)
        '            'Save Group ID
        '            .m_nGroupID = tlvGrpID.m_arrayTLVal(0)
        '        Else
        '            Throw New Exception("Can't Found Group ID Tag (= FFE4)")
        '        End If

        '        '------------[Step 7]------------
        '        'Put Group Info Block to ClassTableListMaster.Group List
        '        'If Same Group ID Block found, Replace Old Info with New One the List.
        '        .m_strUIDriverTabName = "Reader Group (" & .m_nGroupID & ")"

        '    End With

        'Catch ex As Exception
        '    Throw 'ex
        'Finally

        'End Try
    End Sub
    '
    'Public Shared Sub Config_ParseFromReaderGroupInfo(ByRef tagIDGCmdDataInfo As tagIDGCmdData, ByRef configGroup As tagConfigCmdUnitGroup)
    '    Dim arrayByteDataPortion() As Byte
    '    Dim arrayByteTag() As Byte = New Byte(3) {}
    '    Dim nLenHdrTrail As Integer = tagIDGCmdData.m_nPktHeader + tagIDGCmdData.m_nPktTrail
    '    Dim strTag As String
    '    Dim arrayTLVal() As Byte
    '    Dim tlv As tagTLV
    '    Dim tlvOp As tagTLVNewOp
    '    Dim nOffset, nTlvTagLen, nTlvValLenFromDataPortion As Integer

    '    Try
    '        tagIDGCmdDataInfo.ParserDataGetArrayRawData(arrayByteDataPortion)
    '        If (arrayByteDataPortion Is Nothing) Then
    '            Throw New Exception("No Response Data")
    '        End If

    '        '------------[Step 2]------------
    '        'Create Group Info Block ( = tagConfigCmdUnitGroup ) & Create Group's TLV Table
    '        configGroup = New tagConfigCmdUnitGroup(EnumConfigCmd.CONFIG_GROUP_SET)

    '        '================[ TLV Parser Loop Begin ]================
    '        nOffset = 0
    '        nTlvTagLen = 0
    '        While (True)
    '            'Start To Take Tag Length = 3-->2-->1 --> Failed if Not Found
    '            Select Case nTlvTagLen
    '                Case 0
    '                    ' First Try
    '                    nTlvTagLen = 3
    '                Case 1
    '                    ' Failed to Detect All kind of Tag Length 3~1 bytes
    '                    LogLn("[Err] TLV Parsing ParseFromReaderGroupInfo() =  Failed")
    '                    m_pRefMF.LogLnDumpData(arrayByteDataPortion, arrayByteDataPortion.Count)
    '                    Exit Sub
    '                Case 2
    '                    '------------[Step 4]------------
    '                    'Start To Take Tag Length = 1 if Step 3 (=2) NOT Found
    '                    nTlvTagLen = 1
    '                Case 3
    '                    '------------[Step 3]------------
    '                    'Start To Take Tag Length = 2 if Step 2 (=3) NOT Found
    '                    nTlvTagLen = 2
    '            End Select
    '            '

    '            '
    '            Array.Copy(arrayByteDataPortion, nOffset, arrayByteTag, 0, nTlvTagLen)
    '            ClassTableListMaster.ConvertFromArrayByte2String(arrayByteTag, nTlvTagLen, strTag, False)
    '            If m_dictTLVOp.ContainsKey(strTag) Then
    '                tlvOp = m_dictTLVOp.Item(strTag)

    '                'Check Length is Valid
    '                nTlvValLenFromDataPortion = arrayByteDataPortion(nOffset + nTlvTagLen)
    '                If nTlvValLenFromDataPortion > tlvOp.m_nLen Then
    '                    Throw New Exception("[Err] TLV (tag= " + strTag + " ) New Op Len = " & tlvOp.m_nLen & ", Get Len = " & nTlvValLenFromDataPortion)
    '                End If
    '                'Get Data & Parser
    '                ' tlvOp.PktParser(tlv, tagIDGCmdDataInfo)
    '                '-------[ Allocate Preferred Buffer Size to accommodate Value ]-------
    '                If arrayTLVal Is Nothing Then
    '                    arrayTLVal = New Byte(nTlvValLenFromDataPortion - 1) {}
    '                ElseIf (arrayTLVal.Count < nTlvValLenFromDataPortion) Then
    '                    Erase arrayTLVal
    '                    arrayTLVal = New Byte(nTlvValLenFromDataPortion - 1) {}
    '                End If

    '                '
    '                Array.Copy(arrayByteDataPortion, nOffset + nTlvTagLen + 1, arrayTLVal, 0, nTlvValLenFromDataPortion)
    '                '
    '                tlv = New tagTLV(strTag, tlvOp.m_nLen, arrayTLVal)
    '                '
    '                nOffset = nOffset + nTlvTagLen + 1 + nTlvValLenFromDataPortion
    '                ' Reset State Machine to Begin
    '                nTlvTagLen = 0

    '            Else
    '                '=====================[ Continue To Next Tag Length ]=====================
    '                Continue While ' Select Next Length
    '            End If

    '            '------------[Step 5 : Add New TLV to TLV Table ]------------
    '            With configGroup
    '                Dim strTmp As String
    '                'If Found Add Parsed TLV (Or Replace) to TLV Table
    '                If .m_dictTLV.ContainsKey(tlv.m_strTag) Then
    '                    ClassTableListMaster.ConvertFromArrayByte2String(tlv.m_arrayTLVal, strTmp)
    '                    LogLn("[Warning] Duplicated TLV = " + tlv.m_strTag + ", Value = " + strTmp + ", Replace it as New One")
    '                    .m_dictTLV.Remove(tlv.m_strTag)
    '                End If
    '                .m_dictTLV.Add(tlv.m_strTag, tlv)
    '                'Else Show Error Message (Current TLV Info) if Step 4 NOT Found
    '            End With

    '            '---------------[Step 6 : End of TLV Parsing Progress]--------------
    '            If nOffset = arrayByteDataPortion.Count Then
    '                Exit While
    '            ElseIf nOffset > arrayByteDataPortion.Count Then
    '                Throw New Exception("TLV Data Error")
    '            End If
    '        End While
    '        '================[ TLV Parser Loop End ]================

    '        With configGroup
    '            '------------[Step 6]------------
    '            ' Set up Group Info Other Fields
    '            If .m_dictTLV.ContainsKey(tagTLV.TAG_FFE4_or_DFEE2D_GrpID) Then
    '                Dim tlvGrpID As tagTLV = .m_dictTLV.Item(tagTLV.TAG_FFE4_or_DFEE2D_GrpID)
    '                'Save Group ID
    '                .m_nGroupID = tlvGrpID.m_arrayTLVal(0)
    '            Else
    '                Throw New Exception("Can't Found Group ID")
    '            End If

    '            '------------[Step 7]------------
    '            'Put Group Info Block to ClassTableListMaster.Group List
    '            'If Same Group ID Block found, Replace Old Info with New One the List.
    '            .m_strUIDriverTabName = "Reader Group (" & .m_nGroupID & ")"

    '        End With


    '    Catch ex As Exception
    '        LogLn("[Err Dumping]")
    '        m_pRefMF.LogLnDumpData(arrayByteDataPortion, arrayByteDataPortion.Count)
    '        LogLn("[Err] ParseFromReaderGroupInfo() = " + ex.Message)
    '        ' Free configGroup
    '        configGroup.Cleanup()
    '        configGroup = Nothing
    '    Finally
    '        If arrayByteDataPortion IsNot Nothing Then
    '            Erase arrayByteDataPortion
    '        End If

    '        If arrayByteTag IsNot Nothing Then
    '            Erase arrayByteTag
    '        End If

    '        If arrayTLVal IsNot Nothing Then
    '            Erase arrayTLVal
    '        End If
    '    End Try

    'End Sub

    ' Note: INPUT : o_configGroupList Must be Nothing otherwise it will be cleanup automatically before creating new Dictionary Instance
    ' INPUT: configGroup = Nothing
    'OUTPUT: configGroup with TLV Table
    Public Shared Sub Config_ParseFromReaderGroupInfo(ByRef tagIDGCmdDataInfo As tagIDGCmdData, ByRef o_configGroupList As Dictionary(Of Integer, tagConfigCmdUnitGroup))
        Dim configGroup As tagConfigCmdUnitGroup = Nothing

        Try
            Config_ParseFromReaderGroupInfo(tagIDGCmdDataInfo, configGroup)
            '-------------[ Put the last Group Info into the Group List ]-------------
            Config_ParseFromReaderGroupInfoSetupGroupTabTxtAndAddToList(o_configGroupList, configGroup)
        Catch ex As Exception
            Throw 'ex
        Finally
            ' Do Nothing
        End Try

    End Sub

    'Public Shared Sub Config_ParseFromReaderGroupInfo(ByRef tagIDGCmdDataInfo As tagIDGCmdData, ByRef o_configGroupList As Dictionary(Of Integer, tagConfigCmdUnitGroup))
    '    Dim arrayByteDataPortion() As Byte
    '    Dim arrayByteTag() As Byte = New Byte(3) {}
    '    Dim nLenHdrTrail As Integer = tagIDGCmdData.m_nPktHeader + tagIDGCmdData.m_nPktTrail
    '    Dim strTag As String
    '    Dim arrayTLVal() As Byte
    '    Dim tlv As tagTLV
    '    Dim tlvOp As tagTLVNewOp
    '    Dim nOffset, nTlvTagLen, nTlvValLenFromDataPortion As Integer
    '    Dim configGroup As tagConfigCmdUnitGroup




    '    Try
    '        '------------------------[ Prepare Stage ]------------------------
    '        'If o_configGroupList is Nothing --> Create
    '        If o_configGroupList IsNot Nothing Then
    '            'If o_configGroupList DOES Exist --> Cleanup Before using
    '            ClassTableListMaster.CleanupConfigurationsGroupList(o_configGroupList)
    '        End If
    '        o_configGroupList = New Dictionary(Of Integer, tagConfigCmdUnitGroup)

    '        tagIDGCmdDataInfo.ParserDataGetArrayRawData(arrayByteDataPortion)
    '        '------------------------[ End of Prepare Stage ]-----------------------

    '        '================[ Step 2 :  TLV Parser Loop Begin ]================
    '        nOffset = 0
    '        nTlvTagLen = 0
    '        While (True)
    '            'Start To Take Tag Length = 3-->2-->1 --> Failed if Not Found
    '            Select Case nTlvTagLen
    '                Case 0
    '                    ' First Try
    '                    nTlvTagLen = 3
    '                Case 1
    '                    '--------------[Special Case]-------------
    '                    ' While Get All Config Group, there are blank useless data &H00 between Group Block and Another's On. In that case we try to skip those useless &H00 until different value found or out of range.
    '                    'This case (Encounter Bulk of &H00 between Group Blocks) has been meet in the following Productions
    '                    ' AR = Vend III, FW = HG4 AR 2.1.4 - C20 - R18589
    '                    If (arrayByteTag(0) = 0) Then
    '                        LogLn("------------------------------[Warning: Dumping Recv Data Content]------------------------------")
    '                        m_pRefMF.LogLnDumpData(arrayByteDataPortion, arrayByteDataPortion.Count)
    '                        LogLn("------------------------------[End of Warning]------------------------------")
    '                        If SeekoutTillNoneZeroValueOrEndOfData(arrayByteDataPortion, nOffset) Then
    '                            nTlvTagLen = 0
    '                            Continue While
    '                        End If
    '                    End If
    '                    ' Failed to Detect All kind of Tag Length 3~1 bytes
    '                    LogLn("[Err] TLV Parsing ParseFromReaderGroupInfo() =  Failed")
    '                    'm_pRefMF.LogLnDumpData(arrayByteDataPortion, arrayByteDataPortion.Count)
    '                    Exit Sub
    '                Case 2
    '                    '------------[Step 4]------------
    '                    'Start To Take Tag Length = 1 if Step 3 (=2) NOT Found
    '                    nTlvTagLen = 1
    '                Case 3
    '                    '------------[Step 3]------------
    '                    'Start To Take Tag Length = 2 if Step 2 (=3) NOT Found
    '                    nTlvTagLen = 2
    '            End Select
    '            '

    '            'Get Tag
    '            Array.Copy(arrayByteDataPortion, nOffset, arrayByteTag, 0, nTlvTagLen)
    '            ClassTableListMaster.ConvertFromArrayByte2String(arrayByteTag, nTlvTagLen, strTag, False)


    '            If m_dictTLVOp.ContainsKey(strTag) Then
    '                tlvOp = m_dictTLVOp.Item(strTag)

    '                'Check Length is Valid
    '                nTlvValLenFromDataPortion = arrayByteDataPortion(nOffset + nTlvTagLen)
    '                If nTlvValLenFromDataPortion > tlvOp.m_nLen Then
    '                    Throw New Exception("[Err] TLV (tag= " + strTag + " ) New Op Len = " & tlvOp.m_nLen & ", Get Len = " & nTlvValLenFromDataPortion)
    '                End If
    '                'Get Data & Parser
    '                ' tlvOp.PktParser(tlv, tagIDGCmdDataInfo)
    '                '-------[ Allocate Preferred Buffer Size to accommodate Value ]-------
    '                If arrayTLVal Is Nothing Then
    '                    arrayTLVal = New Byte(nTlvValLenFromDataPortion - 1) {}
    '                ElseIf (arrayTLVal.Count < nTlvValLenFromDataPortion) Then
    '                    Erase arrayTLVal
    '                    arrayTLVal = New Byte(nTlvValLenFromDataPortion - 1) {}
    '                End If

    '                '
    '                Array.Copy(arrayByteDataPortion, nOffset + nTlvTagLen + 1, arrayTLVal, 0, nTlvValLenFromDataPortion)
    '                '
    '                tlv = New tagTLV(strTag, nTlvValLenFromDataPortion, arrayTLVal)
    '                '
    '                nOffset = nOffset + nTlvTagLen + 1 + nTlvValLenFromDataPortion
    '                ' Reset State Machine to Begin
    '                nTlvTagLen = 0

    '            Else
    '                '=====================[ Continue To Next Tag Length ]=====================
    '                Continue While ' Select Next Length
    '            End If

    '            '=================[ Seek Tag FFE4 , Group ID ]=================
    '            If (strTag = tagTLV.TAG_FFE4_or_DFEE2D_GrpID) Then
    '                '--> Put Current Group Info Block into the List 
    '                Config_ParseFromReaderGroupInfoSetupGroupTabTxtAndAddToList(o_configGroupList, configGroup)
    '                '--> Create New Group Info Block
    '                'Create Group Info Block ( = tagConfigCmdUnitGroup ) & Create Group's TLV Table
    '                configGroup = New tagConfigCmdUnitGroup(EnumConfigCmd.CONFIG_GROUP_SET)
    '            End If

    '            '----------------[Step 8 : Add New TLV To current Group TLV List ]----------------------------
    '            With configGroup
    '                Dim strTmp As String
    '                'If Found Add Parsed TLV (Or Replace) to TLV Table
    '                If .m_dictTLV.ContainsKey(tlv.m_strTag) Then
    '                    ClassTableListMaster.ConvertFromArrayByte2String(tlv.m_arrayTLVal, strTmp)
    '                    LogLn("[Warning] Duplicated TLV = " + tlv.m_strTag + ", Value = " + strTmp + ", Replace it as New One")
    '                    .m_dictTLV.Remove(tlv.m_strTag)
    '                End If
    '                .m_dictTLV.Add(tlv.m_strTag, tlv)
    '                'Else Show Error Message (Current TLV Info) if Step 4 NOT Found
    '            End With

    '            '---------------[Step 9: End of TLV Parsing Progress]--------------
    '            If nOffset = arrayByteDataPortion.Count Then
    '                Exit While
    '            ElseIf nOffset > arrayByteDataPortion.Count Then
    '                Throw New Exception("TLV Data Error")
    '            End If
    '        End While
    '        '================[ TLV Parser Loop End ]================

    '        '-----------[ Step 10 : Put the last Group Info into the Group List ]-------------
    '        '
    '        Config_ParseFromReaderGroupInfoSetupGroupTabTxtAndAddToList(o_configGroupList, configGroup)

    '        ' For Debug ONLY
    '        If m_bDebug Then
    '            m_pRefMF.LogLnDumpData(arrayByteDataPortion, arrayByteDataPortion.Count)
    '        End If
    '    Catch ex As Exception
    '        LogLn("[Err Dumping]")
    '        m_pRefMF.LogLnDumpData(arrayByteDataPortion, arrayByteDataPortion.Count)
    '        LogLn("[Err] ParseFromReaderGroupInfo() = " + ex.Message)
    '        ' Free configGroup
    '        configGroup.Cleanup()
    '        configGroup = Nothing
    '    Finally
    '        If arrayByteDataPortion IsNot Nothing Then
    '            ' Make Sure it's error exiting, then we have to release o_configGroupList
    '            If nOffset <> arrayByteDataPortion.Count Then
    '                ClassTableListMaster.CleanupConfigurationsGroupList(o_configGroupList)
    '            End If
    '            '
    '            Erase arrayByteDataPortion
    '        End If

    '        If arrayByteTag IsNot Nothing Then
    '            Erase arrayByteTag
    '        End If

    '        If arrayTLVal IsNot Nothing Then
    '            Erase arrayTLVal
    '        End If
    '    End Try
    'End Sub
    '
    'INPUT: arrayByteDataPortion = data buffer; nOffset = 0-based start position
    'OUTPUT: 
    'Return : True = Found; False = End of Data
    Private Shared Function SeekoutTillNoneZeroValueOrEndOfData(ByVal arrayByteDataPortion As Byte(), ByRef nOffset As Integer) As Boolean
        Dim nIdx As Integer
        Dim nIdxMax As Integer = arrayByteDataPortion.Count - 1
        For nIdx = nOffset To nIdxMax
            If arrayByteDataPortion.ElementAt(nIdx) <> 0 Then
                nOffset = nIdx
                Return True
            End If
        Next

        Return False
    End Function

    Private Shared Sub Config_ParseFromReaderGroupInfoSetupGroupTabTxtAndAddToList(ByRef o_configGroupList As Dictionary(Of Integer, tagConfigCmdUnitGroup), ByRef o_configGroup As tagConfigCmdUnitGroup)
        Try
            If o_configGroup.m_dictTLV IsNot Nothing Then
                '------------[Step 6]------------
                ' Set up Group Info Other Fields
                If o_configGroup.m_dictTLV.ContainsKey(tagTLV.TAG_FFE4_or_DFEE2D_GrpID) Then
                    Dim tlvGrpID As tagTLV = o_configGroup.m_dictTLV.Item(tagTLV.TAG_FFE4_or_DFEE2D_GrpID)
                    'Save Group ID
                    o_configGroup.m_nGroupID = tlvGrpID.m_arrayTLVal(0)
                Else
                    Throw New Exception("Can't Found Group ID")
                End If

                '------------[Step 7]------------
                'Put Group Info Block to ClassTableListMaster.Group List
                'If Same Group ID Block found, Replace Old Info with New One the List.
                o_configGroup.m_strUIDriverTabName = "Reader Group (" & o_configGroup.m_nGroupID & ")"

                o_configGroupList.Add(o_configGroup.m_nGroupID, o_configGroup)
            End If
        Catch ex As Exception
            Throw 'ex
        End Try

    End Sub
    '
    '==================================[ Configuration File Loader ]===========================================
    'Poll Mode
    Public m_tagConfigCmdPollmode As tagConfigCmdUnitPollmode
    Public m_tagConfigCmdUnitAID As tagConfigCmdUnitAID
    Public m_tagConfigCmdUnitGroup As tagConfigCmdUnitGroup
    Public m_tagTLVTmp As tagTLV
    'Check Next Cmd is Valid
    Public m_colConfigCmdValidNext As List(Of EnumConfigCmd)

    Public m_bIsParsedOk As Boolean

    Public m_lConfigTextLineKeywordToMakeTabName As Long
    'Current Config Cmd Index
    Private m_nConfigCmdIndex As EnumConfigCmd

    Private Function Config_IsPreCmdNotDone() As Boolean
        Dim confCmd As tagTxtFileConfigurationsCmd = Nothing
        '
        If m_colConfigCmdValidNext.Count > 0 Then
            Dim cmdIterator As EnumConfigCmd
            Dim sb As New StringBuilder
            'sb.Clear() '.Net Framework V4.5
            sb.Length = 0 '.Net Framework V3.5
            '
            For Each cmdIterator In m_colConfigCmdValidNext
                confCmd = m_dictConfigCmdTbl2.Item(cmdIterator)
                sb.Append("[Cmd = " + confCmd.m_strCmdType + "]")
            Next
            MsgBox("[Error]" + sb.ToString() + ": not done yet, ln = " & m_lConfigTextLine)
            Return True
        End If
        '
        Return False
        '
    End Function

    Private Sub Config_LoadFromFile2PaserLine_Pollmode(ByRef strIn As String)
        Dim iteratorConfCmd As tagConfigCmdUnitPollmode = New tagConfigCmdUnitPollmode(m_nConfigCmd)

        'Make Sure Previous Command is done, If not done--> Exit
        If Config_IsPreCmdNotDone() Then
            Exit Sub
        End If

        'Prepare to permitted next process stages
        m_colConfigCmdValidNext.Add(EnumConfigCmd.CONFIG_END)
        m_colConfigCmdValidNext.Add(EnumConfigCmd.CONFIG_POLLMODE_DATA)

        m_tagConfigCmdPollmode.m_tagConfigCmd.m_bIsConfigCmdStart = True
        m_tagConfigCmdPollmode.m_tagConfigCmd.m_nConfigCmd = m_nConfigCmd

        'Save to current config cmd index
        m_nConfigCmdIndex = m_nConfigCmd

        'Add Comment if available
        m_tagConfigCmdPollmode.m_tagConfigCmd.m_strCommentTrail = m_strTrailComment

    End Sub

    Private Sub Config_LoadFromFile2PaserLine_PollmodeData(ByRef strIn As String, ByVal bPollmodeVal As Byte)
        ' Save Pollmode Value
        m_tagConfigCmdPollmode.m_bModeOn = bPollmodeVal

        'Find Trailing Comment String
        m_tagConfigCmdPollmode.m_tagConfigCmd.m_strCommentTrail = m_strTrailComment

    End Sub

    Private Sub Config_LoadFromFile2PaserLine_NewSetConfiguration(ByRef strIn As String)
        Config_LoadFromFile2PaserLine_NewSetGroup(strIn)
    End Sub

    Private Sub Config_LoadFromFile2PaserLine_NewSetAID(ByRef strIn As String)
        'Set up AID Group Unit
        'Clean up at beginning
        If (m_tagConfigCmdUnitAID.Equals(Nothing)) Then
            m_tagConfigCmdUnitAID.Cleanup()
        Else
            m_tagConfigCmdUnitAID = New tagConfigCmdUnitAID(m_nConfigCmd)
        End If

        'Seeking Data or End Tag
        m_tagConfigCmdUnitAID.m_tagConfigCmd.m_bIsConfigCmdStart = True
        m_tagConfigCmdUnitAID.m_tagConfigCmd.m_bIsConfigCmdEnd = False
        'm_tagConfigCmdUnitAID.m_tagConfigCmd.m_nConfigCmd = m_nConfigCmd

        'Prepare to permitted next process stages
        m_colConfigCmdValidNext.Clear()
        m_colConfigCmdValidNext.Add(EnumConfigCmd.CONFIG_END)
        m_colConfigCmdValidNext.Add(EnumConfigCmd.CONFIG_TLV)

        'Save to current config cmd index
        m_nConfigCmdIndex = m_nConfigCmd

        'Add Comment if available
        m_tagConfigCmdUnitAID.m_tagConfigCmd.m_strCommentTrail = m_strTrailComment

    End Sub

   

    Private Sub Config_LoadFromFile2PaserLine_NewSetGroup(ByRef strIn As String)
        'Set up AID Group Unit
        m_tagConfigCmdUnitGroup = New tagConfigCmdUnitGroup(m_nConfigCmd)

        'Seeking Data or End Tag
        m_tagConfigCmdUnitGroup.m_tagConfigCmd.m_bIsConfigCmdStart = True
        m_tagConfigCmdUnitGroup.m_tagConfigCmd.m_bIsConfigCmdEnd = False
        'm_tagConfigCmdUnitGroup.m_tagConfigCmd.m_nConfigCmd = m_nConfigCmd

        'Prepare to permitted next process stages
        m_colConfigCmdValidNext.Clear()
        m_colConfigCmdValidNext.Add(EnumConfigCmd.CONFIG_END)
        m_colConfigCmdValidNext.Add(EnumConfigCmd.CONFIG_TLV)

        'Save to current config cmd index
        m_nConfigCmdIndex = m_nConfigCmd

        'Add Comment if available
        m_tagConfigCmdUnitGroup.m_tagConfigCmd.m_strCommentTrail = m_strTrailComment

    End Sub



  
    Private Shared Sub Config_ParseFromReaderAIDInfoSetupAIDTabTxtAndAddToList(ByRef o_AIDList As Dictionary(Of String, tagConfigCmdUnitAID), ByRef o_configAID As tagConfigCmdUnitAID)
        Dim tlvAID As tagTLV
        '
        Try
            With o_configAID
                If .m_dictTLV IsNot Nothing Then
                    '------------[Step 6]------------
                    ' Set up Group Info Other Fields
                    If .m_dictTLV.ContainsKey(tagTLV.TAG_9F06_TermID_AID) Then
                        tlvAID = .m_dictTLV.Item(tagTLV.TAG_9F06_TermID_AID)
                        'Save Terminal ID
                        ClassTableListMaster.ConvertFromArrayByte2String(tlvAID.m_arrayTLVal, .m_strTerminalID)
                    Else
                        Throw New Exception("Can't Found Terminal ID")
                    End If
                    'Set Group ID String
                    If .m_dictTLV.ContainsKey(tagTLV.TAG_FFE4_or_DFEE2D_GrpID) Then
                        tlvAID = .m_dictTLV.Item(tagTLV.TAG_FFE4_or_DFEE2D_GrpID)
                        'Save Terminal ID
                        ClassTableListMaster.ConvertFromArrayByte2String(tlvAID.m_arrayTLVal, .m_strGroupID)
                    Else
                        Throw New Exception("Can't Found Group ID")
                    End If

                    '------------[Step 7]------------
                    'Put Group Info Block to ClassTableListMaster.Group List
                    'If Same Group ID Block found, Replace Old Info with New One the List.
                    .m_strKeyTagTerminalIDGroupID = .m_strTerminalID + " " + .m_strGroupID
                    .m_strUIDriverTabName = "Reader (AID,Group)=(" + .m_strTerminalID + ", " + .m_strGroupID + ")"
                    o_AIDList.Add(.m_strKeyTagTerminalIDGroupID, o_configAID)
                End If
            End With

        Catch ex As Exception
            Throw 'ex
        End Try

    End Sub
    '
    Private Sub CleanupTLV()
        '
#If PLATFORM_NEO100_AR215 Then
        For Each iteratorSeek In m_dictTLVOp_Legacy_NEO100_and_AR215_Older
            iteratorSeek.Value.Cleanup()
        Next
        m_dictTLVOp_Legacy_NEO100_and_AR215_Older.Clear()
#End If
        '
#If PLATFORM_NEO200_AR300 Then

        For Each iteratorSeek In m_dictTLVOp_New_NEO_200_and_AR300
            iteratorSeek.Value.Cleanup()
        Next
        m_dictTLVOp_New_NEO_200_and_AR300.Clear()
#End If
    End Sub

    Private Sub CleanupTLVCAPK()
        'For Each iteratorSeek In m_dictCAPK_TLV
        '    iteratorSeek.Value.Cleanup()
        'Next
        'm_dictCAPK_TLV.Clear()
    End Sub
    '
    Public Sub Cleanup()
        '--------------------[Common Data]-------------------------
        CleanupTLV()
        '--------------------[Group Data]-------------------------
        '--------------------[CAPK Data]-------------------------
        CleanupTLVCAPK()
        '--------------------[CRL Data]-------------------------

        '----------------[MSIC Stuffs]-------------------
        'Do this in IDisposing Status
        'If (m_winobjOFD IsNot Nothing) Then
        '    m_winobjOFD.Dispose()
        '    m_winobjOFD = Nothing
        'End If

        If (m_listKeyGroupComment IsNot Nothing) Then
            m_listKeyGroupComment.Clear()
        End If

    End Sub

    '============================================[TLV Data Parser]=====================================================

    'Ex: 
    'Dim dataJLT As Byte() = New Byte() {&HFF, &HEE, &H1, &H82, &H0, &H3, &H11, &H22, &H33}
    'Dim nLen, nPosData As Integer
    'ClassReaderCommanderParser.ParseFromReaderMultipleTLVsGetRealLengthOfTLV("FFEE01", dataJLT, 3, nLen, nPosData)
    Private Shared Sub TLV_ParseFromReaderMultipleTLVsGetRealLengthOfTLV(strTlvName As String, refArrayByteDataTLVs As Byte(), nPosOfLen As Integer, ByRef nLenOfData As Integer, ByRef nPosOfData As Integer)
        Dim byte80hBiggerOrLen As Byte
        Dim strLen As String = ""
        Dim arrayByteLen As Byte()
        Dim nChkLen As Integer
        'Validation Check for normal TLV
        If (refArrayByteDataTLVs.Count < (nPosOfLen + 1)) Then
            'In Case Of TLV No Data ( Length = 0) We consider allow it.
            LogLn("[Warning] GetRealLengthOfTLV() = Parsing TLV (" + strTlvName + ") Format in Data buffer is incorrect. Data Buffer size = " & refArrayByteDataTLVs.Count & " < wanted Normal size = " & (nPosOfLen + 1))
        End If
        'Check first byte is Bigger than 80h, right now we ONLY have to parse 81h, 82h
        byte80hBiggerOrLen = refArrayByteDataTLVs(nPosOfLen)
        If (byte80hBiggerOrLen > &H80) Then
            byte80hBiggerOrLen = byte80hBiggerOrLen - CType(&H80, Byte)
            Select Case byte80hBiggerOrLen
                Case &H1
                    'Get Data Length
                    nLenOfData = refArrayByteDataTLVs(nPosOfLen + 1)
                Case &H2
                    'Get Data Length
                    nLenOfData = ((CType(refArrayByteDataTLVs(nPosOfLen + 1), UInt16) << 8) And &HFF00) + refArrayByteDataTLVs(nPosOfLen + 2)
                Case Else
                    arrayByteLen = New Byte(byte80hBiggerOrLen - 1) {}
                    Array.Copy(refArrayByteDataTLVs, nPosOfLen + 1, arrayByteLen, 0, byte80hBiggerOrLen)
                    ClassTableListMaster.ConvertFromArrayByte2String(arrayByteLen, strLen, False)
                    Erase arrayByteLen
                    Throw New Exception("[Err]  GetRealLengthOfTLV() = Unsupported TLV (" + strTlvName + ") Length = " + strLen)
            End Select
            'Validation Check for Very Large TLV
            nChkLen = (nPosOfLen + nLenOfData + 1 + byte80hBiggerOrLen)
            If (refArrayByteDataTLVs.Count < nChkLen) Then
                Throw New Exception("[Err] GetRealLengthOfTLV() = Parsing  Size-over-256-byte TLV (" + strTlvName + ") Format in Data buffer is incorrect. Data Buffer size = " & refArrayByteDataTLVs.Count & " < wanted  Large Size = " & nChkLen)
            End If
            '[Note] 1= check byte length
            nPosOfData = nPosOfLen + 1 + byte80hBiggerOrLen
        Else
            nLenOfData = byte80hBiggerOrLen
            nPosOfData = nPosOfLen + 1
        End If

    End Sub
    '===================================================================================================
    '
    Public Shared Sub TLV_ParseFromReaderMultipleTLVs(ByVal strDataTLVs As String, ByRef o_TLVsList As Dictionary(Of String, tagTLV))
        Dim refArrayByteDataTLVs As Byte() = Nothing
        Try
            '1.Normalize String
            strDataTLVs = ClassTableListMaster.StrNormalized(strDataTLVs)
            '2.Conver Hex String to Byte Array
            ClassTableListMaster.TLVauleFromString2ByteArray(strDataTLVs, refArrayByteDataTLVs)
            '3.Parse to TLV List
            TLV_ParseFromReaderMultipleTLVs(refArrayByteDataTLVs, 0, refArrayByteDataTLVs.Length, o_TLVsList)
        Catch ex As Exception
            Throw
        Finally
            If (refArrayByteDataTLVs IsNot Nothing) Then
                Erase refArrayByteDataTLVs
            End If
        End Try
    End Sub
    '
    Public Shared Sub TLV_ParseFromReaderMultipleTLVs(ByVal strDataTLVs As String, ByRef o_TLVsList As SortedDictionary(Of String, tagTLV))
        Dim refArrayByteDataTLVs As Byte() = Nothing
        Try
            '1.Normalize String
            strDataTLVs = ClassTableListMaster.StrNormalized(strDataTLVs)
            '2.Conver Hex String to Byte Array
            ClassTableListMaster.TLVauleFromString2ByteArray(strDataTLVs, refArrayByteDataTLVs)
            '3.Parse to TLV List
            TLV_ParseFromReaderMultipleTLVs(refArrayByteDataTLVs, 0, refArrayByteDataTLVs.Length, o_TLVsList)
        Catch ex As Exception
            Throw
        Finally
            If (refArrayByteDataTLVs IsNot Nothing) Then
                Erase refArrayByteDataTLVs
            End If
        End Try

    End Sub
    Public Shared Sub TLV_ParseFromReaderMultipleTLVs(ByVal refArrayByteDataTLVs As Byte(), ByRef o_TLVsList As SortedList(Of String, tagTLV))
        TLV_ParseFromReaderMultipleTLVs(refArrayByteDataTLVs, 0, refArrayByteDataTLVs.Length, o_TLVsList)
    End Sub
    Public Shared Sub TLV_ParseFromReaderMultipleTLVs(ByVal refArrayByteDataTLVs As Byte(), ByRef o_TLVsList As SortedDictionary(Of String, tagTLV))
        TLV_ParseFromReaderMultipleTLVs(refArrayByteDataTLVs, 0, refArrayByteDataTLVs.Length, o_TLVsList)
    End Sub
    Public Shared Sub TLV_ParseFromReaderMultipleTLVs(ByVal refArrayByteDataTLVs As Byte(), ByRef o_TLVsList As Dictionary(Of String, tagTLV))
        TLV_ParseFromReaderMultipleTLVs(refArrayByteDataTLVs, 0, refArrayByteDataTLVs.Length, o_TLVsList)
    End Sub
    Public Shared Sub TLV_ParseFromReaderMultipleTLVs(ByVal refArrayByteDataTLVs As Byte(), ByRef o_TLVsList As List(Of tagTLV))
        TLV_ParseFromReaderMultipleTLVs(refArrayByteDataTLVs, 0, refArrayByteDataTLVs.Length, o_TLVsList)
    End Sub
    '
    Public Shared Sub TLV_ParseFromReaderMultipleTLVs(ByVal refArrayByteDataTLVs As Byte(), ByVal nPosStart As Integer, ByVal nLen As Integer, ByRef o_TLVsList As SortedDictionary(Of String, tagTLV))
        Dim o_TLVsList2 As List(Of tagTLV) = Nothing

        TLV_ParseFromReaderMultipleTLVs(refArrayByteDataTLVs, nPosStart, nLen, o_TLVsList2)

        'If o_configGroupList is Nothing --> Create
        If o_TLVsList IsNot Nothing Then
            'If o_configGroupList DOES Exist --> Cleanup Before using
            If (o_TLVsList.Count > 0) Then
                ClassTableListMaster.CleanupConfigurationsTLVList(o_TLVsList)
            End If
        Else
            o_TLVsList = New SortedDictionary(Of String, tagTLV)
        End If

        If (o_TLVsList2.Count > 0) Then
            For Each iteratorSeek In o_TLVsList2
                ParseFromReaderMultipleTLVs_CheckDuplicatedTlvAndRemoveOld(o_TLVsList, iteratorSeek)

                o_TLVsList.Add(iteratorSeek.m_strTag, iteratorSeek)
            Next

            o_TLVsList2.Clear()
        End If
    End Sub

    Public Shared Sub TLV_ParseFromReaderMultipleTLVs(ByVal refArrayByteDataTLVs As Byte(), ByVal nPosStart As Integer, ByVal nLen As Integer, ByRef o_TLVsList As List(Of tagTLV))
        Dim arrayByteTag() As Byte = New Byte(3) {}
        Dim nLenHdrTrail As Integer = tagIDGCmdData.m_nPktHeader + tagIDGCmdData.m_nPktTrail
        Dim strTag As String = ""
        Dim arrayTLVal As Byte() = Nothing
        Dim tlv As tagTLV = Nothing
        Dim tlvOp As tagTLVNewOp
        Dim nOffset, nTlvTagLen, nTlvValLenFromDataPortion As Integer
        Dim listUnknownData As List(Of String) = Nothing
        Dim nRetry As Integer = 0
        Dim nRetryMax As Integer = 3
        Try
            '------------------------[ Prepare Stage ]------------------------
            'If o_configGroupList is Nothing --> Create
            If o_TLVsList IsNot Nothing Then
                'If o_configGroupList DOES Exist --> Cleanup Before using
                ClassTableListMaster.CleanupConfigurationsTLVList(o_TLVsList)
            End If
            o_TLVsList = New List(Of tagTLV)
            listUnknownData = New List(Of String)

            '================[ Step 2 :  TLV Parser Loop Begin ]================
            nOffset = nPosStart
            nTlvTagLen = 0
            While (True)

                '---------------[Step 0: End of TLV Parsing Progress]--------------
                If nOffset = refArrayByteDataTLVs.Count Then
                    Exit While
                ElseIf nOffset > refArrayByteDataTLVs.Count Then
                    Throw New Exception("TLV Data Error - Overflow of data , Last Tag = " + strTag)
                End If

                'Start To Take Tag Length = 3-->2-->1 --> Failed if Not Found
                Select Case nTlvTagLen
                    Case 0
                        ' First Try
                        nTlvTagLen = 3
                    Case 1
                        '--------------[Special Case]-------------
                        ' While Get All Config Group(03-07), there are blank useless data &H00 between Group Block and Another's On. In that case we try to skip those useless &H00 until different value found or out of range.
                        'This case (Encounter Bulk of &H00 between Group Blocks) has been meet in the following Productions
                        ' AR = Vend III, FW = HG4 AR 2.1.4 - C20 - R18589
                        '----------------------[Example]------------------------
                        ' FF E4 01 00 9C 01 00 5F 2A 02 08 40 5F 36 01 02
                        '9F 09 02 00 02 9F 15 02 00 00 9F 1A 02 08 40 9F
                        '1B 04 00 00 1F 40 9F 33 03 00 08 E8 9F 35 01 22
                        '9F 40 05 60 00 00 30 00 9F 66 04 A0 00 40 00 9F
                        '6D 02 00 01 9F 7C 14 00 00 00 00 00 00 00 00 00
                        '00 00 00 00 00 00 00 00 00 00 00 DF 28 03 00 08
                        'E8 DF 29 03 00 68 E8 DF 5C 04 01 50 00 00 DF 70
                        '01 00 DF 71 02 00 00 DF 72 02 00 00 DF 73 02 00
                        '00 DF 74 01 00 FF F1 06 00 00 00 01 00 00 FF F4
                        '03 00 07 03 FF F5 06 00 00 00 00 60 00 FF F8 01
                        '03 FF FB 01 00 FF FC 01 01 FF FD 05 F8 50 AC F8
                        '00 FF FE 05 F8 50 AC A0 00 FF FF 05 00 00 00 00
                        '00 FF E4 01 02 9F 09 02 01 00 9F 66 04 B0 00 C0
                        '00 DF 74 01 01 00 00 00 00 00 00 00 00 00 00 00
                        '00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00
                        '00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00
                        '00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00
                        '00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00
                        '00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00
                        '00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00
                        '00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00
                        '00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00
                        '00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00
                        '00 00 00 00 00 00 00 FF E4 01 04 9F 09 02 01 00
                        '9F 66 04 B0 00 C0 00 FF E4 01 08 9F 09 02 00 8C
                        '5F 2A 02 08 40 5F 36 01 02 9F 1A 02 08 40 9F 1B
                        '04 00 00 27 10 9F 1C 08 30 30 30 30 30 30 30 30
                        '9F 1E 08 30 30 30 30 30 30 30 30 9F 33 03 60 F8
                        'C8 9F 35 01 22 9F 40 05 70 00 90 A0 01 DF 10 02
                        '45 4E DF 13 05 00 00 00 00 00 DF 14 05 00 00 00
                        '00 00 DF 15 05 00 00 00 00 00 DF 17 04 00 00 13
                        '88 DF 18 01 01 DF 19 01 01 DF 22 01 00 DF 26 01
                        '01 DF 27 01 01 DF 25 09 9F 02 06 9F 03 06 9F 37
                        '04 DF 28 08 9F 02 06 9F 03 06 8A 02 DF 40 16 50
                        '49 4E 20 54 52 59 20 4C 49 4D 49 54 20 45 58 43
                        '45 45 44 45 44 DF 41 0C 4C 41 53 54 20 50 49 4E
                        '20 54 52 59 DF 42 10 50 4C 45 41 53 45 20 54 52
                        '59 20 41 47 41 49 4E DF 43 0E 43 41 4C 4C 20 59
                        '4F 55 52 20 42 41 4E 4B

                        If (arrayByteTag(0) = 0) Then
                            Dim nOffsetEnd As Integer = nOffset
                            LogLn("------------------------------[Warning: Dumping Recv Data Content]------------------------------")

                            If SeekoutTillNoneZeroValueOrEndOfData(refArrayByteDataTLVs, nOffsetEnd) Then
                                Form01_Main.LogLnDumpData(refArrayByteDataTLVs, nOffset, nOffsetEnd - nOffset)
                                LogLn("------------------------------[End of Warning]------------------------------")
                                nOffset = nOffsetEnd 'Update new offset data <> 0
                                nTlvTagLen = 0

                                nRetry = nRetry + 1
                                If (nRetry >= nRetryMax) Then
                                    Exit While
                                End If
                                Continue While
                            End If
                            'Till End of Data , this rest data are all of ZERO
                            Form01_Main.LogLnDumpData(refArrayByteDataTLVs, nOffset, refArrayByteDataTLVs.Count - nOffset)
                            LogLn("------------------------------[End of Warning]------------------------------")
                            Throw New Exception("[Err] ParseFromReaderMultipleTLVs() =  Failed")
                        End If

                        ' Failed to Detect All kind of Tag Length 3~1 bytes, use BER-TLV Method to parse TLV
                        If (TLV_ParseFromReaderMultipleTLVs_SmartParser(refArrayByteDataTLVs, nOffset, o_TLVsList)) Then
                            nTlvTagLen = 0
                            Continue While
                        End If
                        If (TLV_ParseFromReaderMultipleTLVs_SkipUnknownData(refArrayByteDataTLVs, nOffset, listUnknownData)) Then
                            nTlvTagLen = 0
                            Continue While
                        End If
                        Form01_Main.LogLnDumpData(refArrayByteDataTLVs, refArrayByteDataTLVs.Count)
                        Throw New Exception("[Err] ParseFromReaderMultipleTLVs() =  Failed")
                    Case 2
                        '------------[Step 4]------------
                        'Start To Take Tag Length = 1 if Step 3 (=2) NOT Found
                        nTlvTagLen = 1
                    Case 3
                        '------------[Step 3]------------
                        'Start To Take Tag Length = 2 if Step 2 (=3) NOT Found
                        nTlvTagLen = 2
                End Select
                '

                '-----------------------[ Boundary Check ]--------------------------
                If ((nOffset + nTlvTagLen) >= refArrayByteDataTLVs.Count) Then
                    Continue While
                End If

                'Get Tag
                Array.Copy(refArrayByteDataTLVs, nOffset, arrayByteTag, 0, nTlvTagLen)
                ClassTableListMaster.ConvertFromArrayByte2String(arrayByteTag, nTlvTagLen, strTag, False)

                If m_dictTLVOp_RefList_Major.ContainsKey(strTag) Then
                    tlvOp = m_dictTLVOp_RefList_Major.Item(strTag)

                    'Check Length is Valid
                    '[Special Case ] Check &H81, &H82, Length of m_nLen in Bytes. This is for very large data process
                    'Ex: 81 AF --> Length is AFh
                    '      82 01 59 --> Length is  159h
                    '      83 FF 91 32 --> Length is FF9132h
                    '      and so on
                    TLV_ParseFromReaderMultipleTLVsGetRealLengthOfTLV(tlvOp.m_strTag, refArrayByteDataTLVs, nOffset + nTlvTagLen, nTlvValLenFromDataPortion, nOffset)
                    'If nTlvValLenFromDataPortion > tlvOp.m_nLen Then
                    '    Throw New Exception("[Err] TLV (tag= " + strTag + " ) New Op Len = " & tlvOp.m_nLen & ", Get Len = " & nTlvValLenFromDataPortion)
                    'End If
                    'Get Data & Parser
                    ' tlvOp.PktParser(tlv, tagIDGCmdDataInfo)
                    '-------[ Allocate Preferred Buffer Size to accommodate Value ]-------
                    If arrayTLVal Is Nothing Then
                        arrayTLVal = New Byte(nTlvValLenFromDataPortion - 1) {}
                    ElseIf (arrayTLVal.Count < nTlvValLenFromDataPortion) Then
                        Erase arrayTLVal
                        arrayTLVal = New Byte(nTlvValLenFromDataPortion - 1) {}
                    End If

                    Array.Copy(refArrayByteDataTLVs, nOffset, arrayTLVal, 0, nTlvValLenFromDataPortion)
                    '
                    tlv = New tagTLV(strTag, nTlvValLenFromDataPortion, arrayTLVal)
                    '
                    nOffset = nOffset + nTlvValLenFromDataPortion
                    ' Reset State Machine to Begin
                    nTlvTagLen = 0

                    'Add to Output TLV List
                    o_TLVsList.Add(tlv)
                Else
                    '=====================[ Continue To Next Tag Length ]=====================
                    Continue While
                End If


            End While
            '================[ TLV Parser Loop End ]================

        Catch ex As Exception
            LogLn("----------------------------[Err Dumping] ParseFromReaderMultipleTLVs() --------------------------------")
            Form01_Main.LogLnDumpData(refArrayByteDataTLVs, refArrayByteDataTLVs.Count)
            LogLn("[Err] ParseFromReaderMultipleTLVs() = " + ex.Message + ", nOffset = " + nOffset.ToString() + ", Last Resolved Tag = " + tlv.m_strTag)
        Finally
            If arrayByteTag IsNot Nothing Then
                Erase arrayByteTag
            End If

            If arrayTLVal IsNot Nothing Then
                Erase arrayTLVal
            End If

            If (listUnknownData IsNot Nothing) Then
                listUnknownData.Clear()
            End If
        End Try
    End Sub
    '
    Public Shared Sub TLV_ParseFromReaderMultipleTLVs(ByVal refArrayByteDataTLVs As Byte(), ByVal nPosStart As Integer, ByVal nLen As Integer, ByRef o_TLVsList As SortedList(Of String, tagTLV))
        Dim o_TLVsList2 As List(Of tagTLV) = Nothing

        TLV_ParseFromReaderMultipleTLVs(refArrayByteDataTLVs, nPosStart, nLen, o_TLVsList2)

        'If o_configGroupList is Nothing --> Create
        If o_TLVsList IsNot Nothing Then
            'If o_configGroupList DOES Exist --> Cleanup Before using
            ClassTableListMaster.CleanupConfigurationsTLVList(o_TLVsList)
        Else
            o_TLVsList = New SortedList(Of String, tagTLV)
        End If

        If (o_TLVsList2.Count > 0) Then
            For Each iteratorSeek In o_TLVsList2
                ParseFromReaderMultipleTLVs_CheckDuplicatedTlvAndRemoveOld(o_TLVsList, iteratorSeek)

                o_TLVsList.Add(iteratorSeek.m_strTag, iteratorSeek)
            Next

            o_TLVsList2.Clear()
        End If

    End Sub
    '
    Public Shared Sub TLV_ParseFromReaderMultipleTLVs(ByVal refArrayByteDataTLVs As Byte(), ByVal nPosStart As Integer, ByVal nLen As Integer, ByRef o_TLVsList As Dictionary(Of String, tagTLV))
        Dim o_TLVsList2 As List(Of tagTLV) = Nothing

        TLV_ParseFromReaderMultipleTLVs(refArrayByteDataTLVs, nPosStart, nLen, o_TLVsList2)


        If o_TLVsList IsNot Nothing Then
            'If Returning TLV Table(List) DOES Exist --> Cleanup Before using
            If (o_TLVsList.Count > 0) Then
                ClassTableListMaster.CleanupConfigurationsTLVList(o_TLVsList)
            End If
        Else
            'If Returning TLV Table(List) is Nothing --> Create
            o_TLVsList = New Dictionary(Of String, tagTLV)
        End If

        If (o_TLVsList2.Count > 0) Then
            For Each iteratorSeek In o_TLVsList2
                ParseFromReaderMultipleTLVs_CheckDuplicatedTlvAndRemoveOld(o_TLVsList, iteratorSeek)

                o_TLVsList.Add(iteratorSeek.m_strTag, iteratorSeek)
            Next

            o_TLVsList2.Clear()
        End If
    End Sub

   
    '
    '============================================[CRLParser for Reader CAPK Data]=====================================================
    Enum EnumCRLCmd As Integer
        CRLCMD_UNKNOWN = -1

        '
        CRLCMD_REVOCATIONLIST_ADD
        '
        CRLCMD_REVOCATIONLIST_DELETE
        CRLCMD_REVOCATIONLIST_DELETE_ALL
        '
        CRLCMD_REVOCATION_RID_INDEX_SERIALNO
        '
        CRLCMD_COMMENT
        '
        CRLCMD_END
    End Enum
    '
    '
    Public Structure GetCRLRecNumInfo
        Private m_u32RNRd As UInt32
        Private m_u32RNRst As UInt32
        Private m_u32SRS As UInt32

        Sub Init()
            m_u32RNRd = 0
            m_u32RNRst = 0
            m_u32SRS = 0
        End Sub

        Property u32RecNumRead As UInt32
            Get
                Return m_u32RNRd
            End Get
            Set(value As UInt32)
                m_u32RNRd = value
            End Set
        End Property
        Property u32RecNumRest As UInt32
            Get
                Return m_u32RNRst
            End Get
            Set(value As UInt32)
                m_u32RNRst = value
            End Set
        End Property
        Property u32SingleRecSize As UInt32
            Get
                Return m_u32SRS
            End Get
            Set(value As UInt32)
                m_u32SRS = value
            End Set
        End Property
    End Structure


  
    '
    Private Shared Function TLV_ParseFromReaderMultipleTLVs_SmartParserGetIdentifier(refArrayByteDataTLVs As Byte(), ByRef o_nOffset As Integer, ByRef o_bytesTLVIdentifier As Byte()) As Boolean
        Dim byteSeek As Byte
        Dim nIdLen As Integer = 0
        Dim nStageProcess As Integer = 0
        Dim bRet As Boolean = False

        While (True)
            byteSeek = refArrayByteDataTLVs(o_nOffset + nIdLen)
            nIdLen = nIdLen + 1
            Select Case (nStageProcess)
                Case 0
                    '1. Check 1st byte tag number(b5~b1) = 0x1F --> next byte is subsequence of this identifier
                    If (Not ((byteSeek And &H1F) = &H1F)) Then
                        Exit While
                    End If
                    nStageProcess = nStageProcess + 1

                Case 1
                    '2. A)Check 2nd or later byte b8 = 1 (i.e. 0x80) --> this byte is subsequence of this identifer
                    '2. B)Check 2nd of later byte b8 = 0 --> this is the last byte of this identifer
                    If ((byteSeek And &H80) = &H0) Then
                        Exit While
                    End If
            End Select

        End While

        ' Get Identifier
        If (nIdLen > 0) Then
            o_bytesTLVIdentifier = New Byte(nIdLen - 1) {}
            'Boundary Check
            If ((o_nOffset + nIdLen) > refArrayByteDataTLVs.Count) Then
                Dim nRestLen As Integer = refArrayByteDataTLVs.Count - o_nOffset
                Throw New Exception("Invalid TLV Identifier Format, id len = " + nIdLen.ToString() + ". Rest Data Length =" + nRestLen.ToString())
            End If
            '
            Array.Copy(refArrayByteDataTLVs, o_nOffset, o_bytesTLVIdentifier, 0, nIdLen)
            o_nOffset = o_nOffset + nIdLen
        End If
        '
        Return (nIdLen > 0)

    End Function

    Private Shared Function TLV_ParseFromReaderMultipleTLVs_SmartParserGetLength(refArrayByteDataTLVs As Byte(), ByRef o_nOffset As Integer, ByRef o_u16TLVLength As UInt16) As Boolean
        Dim byteSeek As Byte
        Dim nSeekLen As Integer = 0
        Dim nStageProcess As Integer = 0
        Dim bRet As Boolean = False
        Dim u8LenLen As Byte
        '
        o_u16TLVLength = 0
        While (True)
            byteSeek = refArrayByteDataTLVs(o_nOffset + nSeekLen)
            nSeekLen = nSeekLen + 1
            Select Case (nStageProcess)
                Case 0
                    '1. Check 1st byte tag number(b5~b1) = 0x1F --> next byte is subsequence of this identifier
                    If (Not ((byteSeek And &H80) = &H80)) Then
                        u8LenLen = 0
                        o_u16TLVLength = byteSeek
                        Exit While
                    End If
                    nStageProcess = nStageProcess + 1
                    u8LenLen = byteSeek And CType(&H7F, Byte)
                    If (u8LenLen > 4) Then
                        'Should Only 0x81, 0x82, 0x83, 0x84
                        Dim strLen As String = ""
                        ClassTableListMaster.ConvertFromArrayByte2String(byteSeek, strLen)
                        Throw New Exception("BER-TLV Length is out of range, length byte = " + strLen)
                    End If
                    '======================================================================
                Case 1
                    '2. Get Byte Data As Length until u8LenLen = 0
                    o_u16TLVLength = (o_u16TLVLength << 8) + byteSeek
                    u8LenLen = u8LenLen - CType(1, Byte)
                    If (u8LenLen = 0) Then
                        Exit While
                    End If
            End Select
            '
        End While
        '
        o_nOffset = o_nOffset + nSeekLen
        '
        bRet = True
        Return bRet
    End Function

    Private Shared Function TLV_ParseFromReaderMultipleTLVs_SmartParser(refArrayByteDataTLVs As Byte(), ByRef nOffset As Integer, ByRef o_TLVsList As List(Of tagTLV)) As Boolean
        Dim bRet As Boolean = False
        Dim o_bytesTLVIdentifier As Byte() = Nothing
        Dim o_u16TLVLength As UInt16
        Dim o_bytesTLVData As Byte() = Nothing
        Dim tlv As tagTLV
        Dim tlvOp As tagTLVNewOp
        '
        Try
            If (o_TLVsList Is Nothing) Then
                o_TLVsList = New List(Of tagTLV)
            End If

            '1. Get Tag Identifier
            If (Not TLV_ParseFromReaderMultipleTLVs_SmartParserGetIdentifier(refArrayByteDataTLVs, nOffset, o_bytesTLVIdentifier)) Then
                Throw New Exception("Can't Get TLV Identifier")
            End If

            '2. Get Length Value
            If (Not TLV_ParseFromReaderMultipleTLVs_SmartParserGetLength(refArrayByteDataTLVs, nOffset, o_u16TLVLength)) Then
                Throw New Exception("Can't Get TLV Length info")
            End If

            '3.Get Data
            If (o_u16TLVLength > 0) Then
                '-----------------[ Boundary Check ]------------------
                If ((nOffset + o_u16TLVLength) > refArrayByteDataTLVs.Count) Then
                    Dim strTag As String = ""
                    Dim nTmp As Integer = nOffset + o_u16TLVLength
                    ClassTableListMaster.ConvertFromArrayByte2String(o_bytesTLVIdentifier, strTag)
                    Throw New Exception("Out of range, data size = " + refArrayByteDataTLVs.Count.ToString() + ", compute tag=" + strTag + ", compute length=" + nTmp.ToString())
                End If
                o_bytesTLVData = New Byte(o_u16TLVLength - 1) {}
                Array.Copy(refArrayByteDataTLVs, nOffset, o_bytesTLVData, 0, o_u16TLVLength)
                nOffset = nOffset + o_u16TLVLength
            End If

            '4.Create BER-TLV
            tlv = New tagTLV(o_bytesTLVIdentifier, o_u16TLVLength, o_bytesTLVData)
            o_TLVsList.Add(tlv)

            '5. Update TLV Lookup Table
            If (Not m_dictTLVOp_RefList_Major.ContainsKey(tlv.m_strTag)) Then
                'tlvOp = ClassTxtFileParserConfigurations.m_dictTLVOp.Item(strTag)
                tlvOp = New tagTLVNewOp(tlv.m_strTag, 65535, 0)
                m_dictTLVOp_RefList_Major.Add(tlvOp.m_strTag, tlvOp)
            End If

            '6.
            bRet = True

        Catch ex As Exception
            Throw 'ex
        Finally
            If (o_bytesTLVIdentifier IsNot Nothing) Then
                Erase o_bytesTLVIdentifier
            End If
            '
            If (o_bytesTLVData IsNot Nothing) Then
                Erase o_bytesTLVData
            End If
            '

        End Try
        '
        Return bRet
    End Function

    Private Shared Function TLV_ParseFromReaderMultipleTLVs_SkipUnknownData(refArrayByteDataTLVs As Byte(), ByRef nOffset As Integer, ByRef listUnknownData As List(Of String)) As Boolean
        Throw New NotImplementedException("[Err] ParseFromReaderMultipleTLVs_SkipUnknownData() = Not Implemented Yet")
    End Function

    Private Shared Sub ParseFromReaderMultipleTLVs_CheckDuplicatedTlvAndRemoveOld(ByRef o_TLVsList As SortedList(Of String, tagTLV), tlv As tagTLV, Optional bRemoveOld As Boolean = True)
        'If Found Add Parsed TLV (Or Replace) to TLV Table
        If o_TLVsList.ContainsKey(tlv.m_strTag) Then
            Dim tlvTmp As tagTLV = o_TLVsList.Item(tlv.m_strTag)
            LogLn("[Warning] Duplicated TLV = " + tlv.m_strTag + ", (New) Value = " + tlv.strVal + ",( Old )Value = " + tlvTmp.strVal)
            If (bRemoveOld) Then
                o_TLVsList.Remove(tlvTmp.m_strTag)
            End If
        End If
    End Sub
    Private Shared Sub ParseFromReaderMultipleTLVs_CheckDuplicatedTlvAndRemoveOld(ByRef o_TLVsList As SortedDictionary(Of String, tagTLV), tlv As tagTLV, Optional bRemoveOld As Boolean = True)
        'If Found Add Parsed TLV (Or Replace) to TLV Table
        If o_TLVsList.ContainsKey(tlv.m_strTag) Then
            Dim tlvTmp As tagTLV = o_TLVsList.Item(tlv.m_strTag)
            LogLn("[Warning] Duplicated TLV = " + tlv.m_strTag + ", (New) Value = " + tlv.strVal + ",( Old )Value = " + tlvTmp.strVal)
            If (bRemoveOld) Then
                o_TLVsList.Remove(tlvTmp.m_strTag)
            End If
        End If
    End Sub
    Private Shared Sub ParseFromReaderMultipleTLVs_CheckDuplicatedTlvAndRemoveOld(ByRef o_TLVsList As Dictionary(Of String, tagTLV), tlv As tagTLV, Optional bRemoveOld As Boolean = True)
        'If Found Add Parsed TLV (Or Replace) to TLV Table
        If o_TLVsList.ContainsKey(tlv.m_strTag) Then
            Dim tlvTmp As tagTLV = o_TLVsList.Item(tlv.m_strTag)
            LogLn("[Warning] Duplicated TLV = " + tlv.m_strTag + ", (New) Value = " + tlv.strVal + ",( Old )Value = " + tlvTmp.strVal)
            If (bRemoveOld) Then
                o_TLVsList.Remove(tlvTmp.m_strTag)
            End If
        End If
    End Sub
    '
#If 0 Then
     Public Sub CAPK_LoadAllIntoReader(Optional ByVal funcProressUpdate As Action(Of Integer) = Nothing)
        Dim iteratorRIDDel As KeyValuePair(Of String, ClassCAPKDelUnit)
        Dim iteratorRIDKeyIndex As KeyValuePair(Of String, ClassCAPKUnit)
        Dim tagCAPKDel As ClassCAPKDelUnit
        Dim tagCAPKInfo As ClassCAPKUnit
        Dim arrayByteIndexes() As Byte
        Dim arrayByteRID() As Byte
        Dim byteIndex As Byte
        Dim nIdgCmdTimeout As Integer = m_pRefMF.IDGCmdTimeoutGet()

        Try
            'Remove RID+Indexes List
            If m_refobjCTLM.m_dictCAPKCmdDelList.Count > 0 Then
                For Each iteratorRIDDel In m_refobjCTLM.m_dictCAPKCmdDelList
                    tagCAPKDel = iteratorRIDDel.Value
                    arrayByteIndexes = tagCAPKDel.GetArrayByteIndexes
                    arrayByteRID = tagCAPKDel.GetArrayByteRID
                    For Each byteIndex In arrayByteIndexes
                        m_refobjReaderCmder.DelCAPublicKeyRIDSingle(arrayByteRID, byteIndex, nIdgCmdTimeout)
                        '----------------------------[ Update Remove CA Public Status ]--------------------------------
                        funcProressUpdate(1)
                        Dim nRCode As Integer = m_refobjReaderCmder.ResponseCode
                        'ClassReaderCommanderIDGGR.m_dictStatusCodeCAPKRIDKeyIndex
                        If nRCode <> ClassReaderProtocolIDGGR.EnumSTATUSCODE2.x00OK Then
                            Dim strRCode As String
                            Dim byteRCode As Byte = CType(nRCode, Byte)
                            If ClassReaderProtocolIDGGR.m_dictStatusCode2.ContainsKey(byteRCode) Then
                                strRCode = ClassReaderProtocolIDGGR.m_dictStatusCode2.Item(byteRCode)
                            ElseIf ClassReaderProtocolIDGGR.m_dictStatusCodeCAPKRIDKeyIndex.ContainsKey(byteRCode) Then
                                strRCode = ClassReaderProtocolIDGGR.m_dictStatusCodeCAPKRIDKeyIndex.Item(byteRCode)
                            Else
                                strRCode = "Unknown Response Code"
                            End If
                            LogLn("[Err] RID + Key Index = " + tagCAPKDel.strRIDDel + " " + String.Format("{0,2:X2}", byteIndex) + " , couldn't be deleted, Response Code (" & m_refobjReaderCmder.ResponseCode & ") = " + strRCode)
                        Else
                            LogLn("[Done] Remove RID (Key Index) =  " + tagCAPKDel.strRIDDel + " (" + tagCAPKDel.strIndexes + ")")
                        End If
                    Next
                Next
            End If

            'Set All RIDs + Key Indexes
            For Each iteratorRIDKeyIndex In m_refobjCTLM.m_dictCAPKCmdAddList
                tagCAPKInfo = iteratorRIDKeyIndex.Value

                m_refobjReaderCmder.SetCAPublicKeySingle(tagCAPKInfo, nIdgCmdTimeout)
                '----------------------------[ Update Remove CA Public Status ]--------------------------------
                funcProressUpdate(1)

                Dim nRCode As Integer = m_refobjReaderCmder.ResponseCode
                'ClassReaderCommanderIDGGR.m_dictStatusCodeCAPKRIDKeyIndex
                If nRCode <> ClassReaderProtocolIDGGR.EnumSTATUSCODE2.x00OK Then
                    Dim strRCode As String
                    Dim byteRCode As Byte = CType(nRCode, Byte)
                    If ClassReaderProtocolIDGGR.m_dictStatusCode2.ContainsKey(byteRCode) Then
                        strRCode = ClassReaderProtocolIDGGR.m_dictStatusCode2.Item(byteRCode)
                    ElseIf ClassReaderProtocolIDGGR.m_dictStatusCodeCAPKRIDKeyIndex.ContainsKey(byteRCode) Then
                        strRCode = ClassReaderProtocolIDGGR.m_dictStatusCodeCAPKRIDKeyIndex.Item(byteRCode)
                    Else
                        strRCode = "Unknown Response Code"
                    End If
                    LogLn("[Err] RID (Key Index) = " + tagCAPKInfo.strRID + " (" + tagCAPKInfo.strIndex + ") , couldn't be SET, Response Code (" & m_refobjReaderCmder.ResponseCode & ") = " + strRCode)
                Else
                    LogLn("[Done] RID (Key Index) = " + tagCAPKInfo.strRID + " (" + tagCAPKInfo.strIndex + ")")
                End If
            Next
        Catch ex As Exception
            LogLn("[Err] CAPKManuallySetAll_Click() = " + ex.Message)
        End Try
    End Sub
    '
 Private Sub Config_LoadFromFile2PaserLine_End(ByRef strIn As String)
        Dim bEndNoErr As Boolean = True
        Dim strMsg As String = ""
        Dim tagConfigCmdLineMapTmp As tagConfigCmdUnitLineMap = Nothing

        If Not m_colConfigCmdValidNext.Contains(m_nConfigCmd) Then
            MsgBox("[Error] Invalid Config Text Ln = " & m_lConfigTextLine)
            Exit Sub
        End If
        '
        m_colConfigCmdValidNext.Clear()
        '
        tagConfigCmdLineMapTmp.m_lTxtLine = m_lConfigTextLine
        tagConfigCmdLineMapTmp.m_tagConfigCmdType = m_nConfigCmdIndex

        ' Set To No Position Value as Default
        tagConfigCmdLineMapTmp.m_nPosOfListGroup = tagConfigCmdUnitLineMap.EnumValueRange.LINE_MAX

        Select Case m_nConfigCmdIndex
            Case EnumConfigCmd.CONFIG_POLLMODE_SET
                m_tagConfigCmdPollmode.m_tagConfigCmd.m_bIsConfigCmdEnd = True
                '

                m_refobjCTLM.m_dictConfigCmdLineMap.Add(m_lConfigTextLine, tagConfigCmdLineMapTmp)

                '----------------------------------------------------------------
            Case EnumConfigCmd.CONFIG_AID_DELETE
                'Do Nothing
                LogLn("[Note] AID DELETE tag End, Removed AID Items Num = " & m_refobjCTLM.m_dictAIDListDel.Count & ", TxtLn= " & m_lConfigTextLine)

            Case EnumConfigCmd.CONFIG_AID_SET
                m_tagConfigCmdUnitAID.m_tagConfigCmd.m_bIsConfigCmdEnd = True

                'Seek Group ID  TLV : FFE4 as Key
                If m_tagConfigCmdUnitAID.m_dictTLV.ContainsKey(tagTLV.TAG_FFE4_or_DFEE2D_GrpID) Then
                    Dim tlvGrpID As tagTLV = m_tagConfigCmdUnitAID.m_dictTLV.Item(tagTLV.TAG_FFE4_or_DFEE2D_GrpID)
                    'Save Group ID, To be used as part of AID List Key & Part of Index which is in TabPage.Tag
                    ClassTableListMaster.ConvertFromArrayByte2String(tlvGrpID.m_arrayTLVal, m_tagConfigCmdUnitAID.m_strGroupID)
                Else
                    bEndNoErr = False
                    strMsg = "[Err] Can't Found This AID's Group ID (TLV:FFE4) While In End Line : " & m_lConfigTextLine
                End If

                'Seek Terminal ID(AID)  TLV : 9F06 as Key
                If m_tagConfigCmdUnitAID.m_dictTLV.ContainsKey(tagTLV.TAG_9F06_TermID_AID) Then
                    Dim tlvTermID As tagTLV = m_tagConfigCmdUnitAID.m_dictTLV.Item(tagTLV.TAG_9F06_TermID_AID)
                    'Save Terminal ID (AID)
                    ClassTableListMaster.ConvertFromArrayByte2String(tlvTermID.m_arrayTLVal, m_tagConfigCmdUnitAID.m_strTerminalID)
                Else
                    bEndNoErr = False
                    strMsg = "[Err] Can't Found This AID (TLV:9F06) While In End Line : " & m_lConfigTextLine
                End If

                If bEndNoErr Then
                    'Create Tab Name
                    'If m_nConfigCmdIndex = EnumConfigCmd.CONFIG_AID_SET Then
                    '    ConfigTabNameAIDCreate(m_tagConfigCmdUnitAID, m_lConfigTextLineKeywordToMakeTabName)
                    'End If
                    'Add To AID List, Key = AID + " " + GroupID
                    m_refobjCTLM.m_dictAIDList.Add(m_tagConfigCmdUnitAID.m_strTerminalID + " " + m_tagConfigCmdUnitAID.m_strGroupID, m_tagConfigCmdUnitAID)

                    'Get Current AID List Position, useless in AID Tables
                    tagConfigCmdLineMapTmp.m_nPosOfListGroup = m_refobjCTLM.m_dictAIDList.Count - 1
                End If

                m_refobjCTLM.m_dictConfigCmdLineMap.Add(m_lConfigTextLine, tagConfigCmdLineMapTmp)

                '----------------------------------------------------------------
                'Case 
                '    m_tagConfigCmdUnitGroup.m_tagConfigCmd.m_bIsConfigCmdEnd = True
                '    m_tagConfigCmdUnitGroup.m_nGroupID = 0

                '----------------------------------------------------------------
            Case EnumConfigCmd.CONFIG_GROUP_DELETE, EnumConfigCmd.CONFIG_GROUP_SET, EnumConfigCmd.CONFIG_CONFIGURATION_SET
                m_tagConfigCmdUnitGroup.m_tagConfigCmd.m_bIsConfigCmdEnd = True

                'Cmd = CONFIGURATRION SET, use default Group 0 setting
                If (m_nConfigCmdIndex = EnumConfigCmd.CONFIG_CONFIGURATION_SET) Then
                    m_tagConfigCmdUnitGroup.m_nGroupID = 0
                Else
                    'Cmd = Group Set --> use designated Group
                    'Seek Group ID TLV : FFE4 as Key
                    If m_tagConfigCmdUnitGroup.m_dictTLV.ContainsKey(tagTLV.TAG_FFE4_or_DFEE2D_GrpID) Then
                        Dim tlvGrpID As tagTLV = m_tagConfigCmdUnitGroup.m_dictTLV.Item(tagTLV.TAG_FFE4_or_DFEE2D_GrpID)
                        'Save Group ID
                        m_tagConfigCmdUnitGroup.m_nGroupID = tlvGrpID.m_arrayTLVal(0)
                    Else
                        bEndNoErr = False
                        strMsg = "[Err] Can't Found Group ID in Line : " & m_lConfigTextLine
                    End If
                End If


                'Seek Transaction / Payment Type : 9C
                If bEndNoErr And m_tagConfigCmdUnitGroup.m_dictTLV.ContainsKey(tagTLV.TAG_9C_TxnType) Then
                    Dim tlvGrpID As tagTLV = m_tagConfigCmdUnitGroup.m_dictTLV.Item(tagTLV.TAG_9C_TxnType)
                    m_tagConfigCmdUnitGroup.m_nTransactionType = tlvGrpID.m_arrayTLVal(0)
                Else
                    bEndNoErr = False
                    strMsg = "[**Waring**] Can't Found Group " + m_tagConfigCmdUnitGroup.m_nGroupID.ToString + "'s Transaction/Payment Type in Line : " & m_lConfigTextLine
                End If

                If bEndNoErr Then

                    ''Create Tab Name for Form Configurations
                    'If (m_nConfigCmdIndex = EnumConfigCmd.CONFIG_GROUP_SET) Or (m_nConfigCmdIndex = EnumConfigCmd.CONFIG_CONFIGURATION_SET) Then
                    '    ConfigTabNameGroupCreate(m_tagConfigCmdUnitGroup, m_lConfigTextLineKeywordToMakeTabName)
                    'End If
                    'Add to Group List
                    m_refobjCTLM.m_dictGroupList.Add(m_tagConfigCmdUnitGroup.m_nGroupID, m_tagConfigCmdUnitGroup)

                    '
                    tagConfigCmdLineMapTmp.m_nPosOfListGroup = m_tagConfigCmdUnitGroup.m_nGroupID
                End If

                m_refobjCTLM.m_dictConfigCmdLineMap.Add(m_lConfigTextLine, tagConfigCmdLineMapTmp)

                '----------------------------------------------------------------
            Case Else
                strMsg = "[Error][ParseIt()] Invalid Config Command Text Ln = " & m_lConfigTextLine & "Txt= " + strIn
                bEndNoErr = False
        End Select

        If Not bEndNoErr Then
            LogLn(strMsg)
        End If

    End Sub


    Private Sub Config_LoadFromFile2PaserLine_ConfigCommentLineMapAdd(ByRef TestArray() As String, ByRef strIn As String)
        Dim tagConfigCmdLineMapTmp As tagConfigCmdUnitLineMap = New tagConfigCmdUnitLineMap
        Dim strInTmp As String = strIn
        'Don't Add to comment if Only ";" exists
        If TestArray.Count > 1 Then
            'LogLn("[ParseIt()] Ln: " & m_lConfigTextLine & " Txt: " + strIn)
            If m_refobjCTLM.m_dictConfigCmdCommentList.ContainsKey(m_lConfigTextLine) And m_nCFADefM <> EnumConfigurationsFormAddDefaultMode.CFADefM_STANDARD Then
                m_refobjCTLM.m_dictConfigCmdCommentList.Remove(m_lConfigTextLine)
            End If
            m_refobjCTLM.m_dictConfigCmdCommentList.Add(m_lConfigTextLine, strIn)
            '
            tagConfigCmdLineMapTmp.m_lTxtLine = m_lConfigTextLine
            tagConfigCmdLineMapTmp.m_tagConfigCmdType = EnumConfigCmd.CONFIG_COMMENT
            tagConfigCmdLineMapTmp.m_nPosOfListGroup = m_refobjCTLM.m_dictConfigCmdCommentList.Count() - 1

            'For Add Default Group / AID Tab Page , don't record comment
            If m_nCFADefM = EnumConfigurationsFormAddDefaultMode.CFADefM_STANDARD Then
                m_refobjCTLM.m_dictConfigCmdLineMap.Add(m_lConfigTextLine, tagConfigCmdLineMapTmp)
            End If

            'Find the last "Set Group" Keyword in Line Comment
            strInTmp = strInTmp.ToLower()
            If strInTmp.Contains("set group") Or strInTmp.Contains("this sets") Or strInTmp.Contains("set up groups") Then
                m_lConfigTextLineKeywordToMakeTabName = m_lConfigTextLine
            End If

        End If
    End Sub

  Public Overridable Sub Config_LoadFromFile2PaserLine(ByVal strIn As String, ByVal lTxtLn As Long, ByRef nConfigCmd As EnumConfigCmd, Optional ByVal nDefaultMode As EnumConfigurationsFormAddDefaultMode = EnumConfigurationsFormAddDefaultMode.CFADefM_STANDARD)
        'Replace all [Table] with blank 
        strIn = Replace(strIn, vbTab, " ")

        ' Find Trailing Comment & Truncate it
        If strIn.ToCharArray()(0) <> ";"c Then
            ParserItConfigCmd_TrailCommentSaveAndTruncate(strIn, m_strTrailComment, True)
        End If
        '
        Dim TestArray() As String = Config_LoadFromFile2PaserLine_FormatNormalize(strIn)
        Dim TestArrayFirstLevel As String = TestArray(0)
        Dim TestArraySecondLevel As String = TestArray(0)
        Dim bParseAgain As Boolean = True
        Dim bPollmodeVal As Byte = &H0

        Dim aryTrimChars() As Char = {" "c, "'"c}
        Dim strTxtSeek As String
        Dim nIndexSeek As Integer
        '
        Dim bTrailCommentFound As Boolean = False

        Try
            '===========[Init Stuffs]=============
            TestArrayFirstLevel = TestArrayFirstLevel.Trim(aryTrimChars)
            m_nCFADefM = nDefaultMode
            '==================================================================================
            'Check TestArray(1) is valid since it's possible ONLY 1 token ("string")
            If (TestArray.Count > 1) Then
                nIndexSeek = 0
                For Each strTxtSeek In TestArray
                    If (nIndexSeek > 0) And (strTxtSeek.Length > 0) Then
                        ' Update 
                        TestArraySecondLevel = TestArrayFirstLevel + " " + strTxtSeek.Trim(aryTrimChars)
                        Exit For
                    End If
                    nIndexSeek = nIndexSeek + 1
                Next
            End If

            'Increase line number of config text
            m_lConfigTextLine = lTxtLn

            'Change to Upper Case for Key Seeking
            TestArrayFirstLevel = TestArrayFirstLevel.ToUpper()
            TestArraySecondLevel = TestArraySecondLevel.ToUpper()

            ' Parsing Strategy -> Check Priority :  Key Word Length = Short --> Long 
            'Zero Level : Comment Check ';'
            If TestArrayFirstLevel.ToCharArray()(0) = ";"c Then
                bParseAgain = False
                m_nConfigCmd = EnumConfigCmd.CONFIG_COMMENT
                'Third Level Check --> To make sure it is TLV tag
            ElseIf bParseAgain And m_dictTLVOp_RefList_Major.ContainsKey(TestArrayFirstLevel) Then
                bParseAgain = False
                m_nConfigCmd = EnumConfigCmd.CONFIG_TLV
                'First Level Check, for example "END"
            ElseIf bParseAgain And m_dictConfigCmdTbl.ContainsKey(TestArrayFirstLevel) Then
                bParseAgain = False
                m_nConfigCmd = CType(m_dictConfigCmdTbl.Item(TestArrayFirstLevel).m_nCmdType, EnumConfigCmd)
                'Second Level Check(2 phrases combination check, for example "AID SET")
            ElseIf bParseAgain And m_dictConfigCmdTbl.ContainsKey(TestArraySecondLevel) Then
                bParseAgain = False
                m_nConfigCmd = CType(m_dictConfigCmdTbl.Item(TestArraySecondLevel).m_nCmdType, EnumConfigCmd)
                'Forth Level Check --> Special case: In POLLMODE SET case, there is a setting value
            ElseIf bParseAgain And m_nConfigCmd = EnumConfigCmd.CONFIG_POLLMODE_SET Then
                'Convert the value, Throw 'exception if error
                bPollmodeVal = Convert.ToByte(TestArrayFirstLevel)
                m_nConfigCmd = EnumConfigCmd.CONFIG_POLLMODE_DATA
                bParseAgain = False
                'The Last Level It's Unknown Data
            ElseIf bParseAgain Then
                m_nConfigCmd = EnumConfigCmd.CONFIG_UNKNOWN
            End If

            'Set Return(Out) Config Command Type Value
            nConfigCmd = m_nConfigCmd

            '
            Select Case m_nConfigCmd
                Case EnumConfigCmd.CONFIG_POLLMODE_SET
                    Config_LoadFromFile2PaserLine_Pollmode(strIn)

                    '--------------------------------------------------------------------------------
                Case EnumConfigCmd.CONFIG_POLLMODE_DATA
                    ' Move it back
                    m_nConfigCmd = EnumConfigCmd.CONFIG_POLLMODE_SET

                    Config_LoadFromFile2PaserLine_PollmodeData(strIn, bPollmodeVal)

                Case EnumConfigCmd.CONFIG_CONFIGURATION_SET
                    Config_LoadFromFile2PaserLine_NewSetConfiguration(strIn)

                    '--------------------------------------------------------------------------------
                Case EnumConfigCmd.CONFIG_AID_SET, EnumConfigCmd.CONFIG_AID_DELETE
                    Config_LoadFromFile2PaserLine_NewSetAID(strIn)

                    '--------------------------------------------------------------------------------

                Case EnumConfigCmd.CONFIG_END
                    Config_LoadFromFile2PaserLine_End(strIn)
                    '--------------------------------------------------------------------------------
                Case EnumConfigCmd.CONFIG_GROUP_SET, EnumConfigCmd.CONFIG_GROUP_DELETE
                    Config_LoadFromFile2PaserLine_NewSetGroup(strIn)

                    '--------------------------------------------------------------------------------
                Case EnumConfigCmd.CONFIG_COMMENT
                    Config_LoadFromFile2PaserLine_ConfigCommentLineMapAdd(TestArray, strIn)

                    '--------------------------------------------------------------------------------
                    'TLV Data, Tag,Len,
                Case EnumConfigCmd.CONFIG_TLV
                    Config_LoadFromFile2PaserLine_TLV(strIn, TestArray)

                    '--------------------------------------------------------------------------------
                Case EnumConfigCmd.CONFIG_UNKNOWN
                    Dim bytesTLV As Byte() = Nothing
                    Dim tlvList As List(Of tagTLV) = Nothing
                    ClassTableListMaster.TLVauleFromString2ByteArray(strIn, bytesTLV)
                    If (bytesTLV IsNot Nothing) Then
                        'Try to use BER-TLV Method to resolve unknown TLV data and add it to TLV OP Table for future usage
                        If (Not ClassReaderCommanderParser.TLV_ParseFromReaderMultipleTLVs_SmartParser(bytesTLV, 0, tlvList)) Then
                            LogLn("[Error:Unknown Cmd] Txt Ln = " & m_lConfigTextLine & ", Txt = " + strIn)
                        Else
                            m_nConfigCmd = EnumConfigCmd.CONFIG_TLV
                            Config_LoadFromFile2PaserLine_TLV(strIn, TestArray)
                        End If

                        Erase bytesTLV
                        ClassTableListMaster.CleanupConfigurationsTLVList(tlvList)
                    Else
                        LogLn("[Error:Unknown Cmd] Txt Ln = " & m_lConfigTextLine & ", Txt = " + strIn)
                    End If
                    '--------------------[Other unknown cases]-------------------------
                Case Else
                    LogLn("[Error:Invalid Cmd] Cmd= " & m_nConfigCmd & ", Invalid Txt Ln = " & m_lConfigTextLine & ", Txt = " + strIn)
            End Select
            '==================================================================================
        Catch ex As Exception
            Dim strMsg As String = "[Except Err]  ParseIt() ==> Invalid Txt Ln = " & m_lConfigTextLine & ", Txt = " + strIn + "," + ex.Message
            LogLn(strMsg)
        Finally
            'Release  Array Resource
            Erase TestArray
            TestArray = Nothing
        End Try
    End Sub
    '
    Public Sub Config_LoadFromFile2Paser(ByVal szFileNamePath As String)
        Dim currentField As String = ""

        Try
            Dim nCFADefM As EnumConfigurationsFormAddDefaultMode = EnumConfigurationsFormAddDefaultMode.CFADefM_STANDARD
            Dim tagType As EnumConfigCmd
            'Get Memory File Obj
            Dim reader = My.Computer.FileSystem.OpenTextFieldParser(szFileNamePath)
            reader.TextFieldType = Microsoft.VisualBasic.FileIO.FieldType.Delimited
            reader.Delimiters = {"#@*$(@#&"}

            Dim currentRow As String()
            Dim nIdx As Integer = 0

            LogLn("--------> FileName = " + szFileNamePath)

            nIdx = nIdx + 1

            'For Default AID / Group Add Case in Form "AID List/ Group Table Pages @ Configurations Operation"
            'objParser.ResetForParseDefaultFile(nCFADefM)

            'Start to parser every line
            While Not reader.EndOfData
                currentRow = reader.ReadFields()


                For Each currentField In currentRow
                    'Parser it
                    Config_LoadFromFile2PaserLine(currentField, nIdx, tagType, nCFADefM)

                Next
                nIdx = nIdx + 1
            End While
        Catch ex As Exception ' Microsoft.VisualBasic.FileIO.MalformedLineException
            MsgBox("Line " + currentField + ex.Message + "is not valid and will be skipped.")
        Finally

        End Try
    End Sub

    Public Function Config_LoadFromFile(Optional ByVal strFileName As String = "Transaction\GroupAID.txt") As Boolean
        Dim bResult As Boolean = False

        Try
            '=================[ 1. Check Load File Path is available ]===============
            'Cleanup File Path first
            m_strConfigFilePath = ""

            If (Not System.IO.File.Exists(strFileName)) Then
                LogLn("[Err] No CAPK Configuration File ( " + strFileName + " ) Found")
                Return False
            End If
            'Check file type is .txt
            If strFileName.Contains(".txt") Then
                m_strConfigFilePath = strFileName
            End If

            '======[ 2. If the input file name is invalid, then to show up  open file dialogbox to select file by the user ]======
            If m_strConfigFilePath.Equals("") Then
                Dim dlgResult As System.Windows.Forms.DialogResult
                '
                If m_winobjOFD Is Nothing Then
                    Throw New Exception("Open File Dialog isn't created")
                End If
                '
                'm_winobjOFD.InitialDirectory = "c:\" 
                m_winobjOFD.InitialDirectory = My.Computer.FileSystem.CurrentDirectory
                m_winobjOFD.Filter = "text files (*.txt)|*.txt|All files (*.*)|*.*"
                m_winobjOFD.FilterIndex = 1
                m_winobjOFD.RestoreDirectory = True
                m_winobjOFD.Title = "Open Group / AID Configuration File (.txt)"

                'Select CAPK Text File Name
                dlgResult = m_winobjOFD.ShowDialog()

                If (dlgResult <> System.Windows.Forms.DialogResult.OK) And (dlgResult <> System.Windows.Forms.DialogResult.Yes) Then
                    LogLn("[Note] Cancel CA Public Text File Selection And Exit")
                    Return False
                End If

                If m_winobjOFD.FileName = "" Then
                    Throw New Exception("No File is selected")
                End If
                'Update selected File Name
                m_strConfigFilePath = m_winobjOFD.FileName
            End If

            If m_strConfigFilePath.Equals("") Then
                Return False
            End If

            '1)Confirm  to close all tab pages , 2)Parser Cleanup , 3)GroupAIDTableMaster.Cleanup()
            'Close Current Tab Pages down to 1  with 2 x 4 table
            m_refobjCTLM.CleanupConfigurations()

            'Load it
            Config_LoadFromFile2Paser(m_strConfigFilePath)

            'Check Configurations Data Available
            If Not m_refobjCTLM.bIsDataAvailableConfigurations Then
                Beep()
                Throw New Exception("Not Configurations Text File, File Name = " + m_strConfigFilePath)
            End If

            bResult = True
        Catch ex As Exception
            LogLn("[Err] Configurations_LoadFileManually() = " + ex.Message)
        Finally

        End Try

        Return bResult
    End Function

    Public Sub Config_LoadAllIntoReader(Optional ByVal funcProressUpdate As Action(Of Integer) = Nothing)
        '============================================[ Alternative Way ]======================================================
        Try
            m_refobjReaderCmder = m_pRefMF.m_refObjRdrCmder
            If (m_refobjReaderCmder IsNot Nothing) Then
                m_refobjReaderCmder.SetAllCommandsFromTables(m_refobjCTLM, m_tagConfigCmdPollmode.m_bModeOn, m_pRefMF.IDGCmdTimeoutGet(), funcProressUpdate)
            Else
                Throw New Exception("Invalid IDG Commander Ref Instance")
            End If

        Catch ex As Exception
            LogLn("[Err] Config_LoadAllIntoReader() = " + ex.Message)
        End Try
        '
    End Sub
    '
    Public Shared Sub Config_ParseFromReaderAIDInfo(ByRef tagIDGCmdDataInfo As tagIDGCmdData, ByRef o_AIDList As Dictionary(Of String, tagConfigCmdUnitAID))
        Dim arrayByteDataPortion() As Byte = Nothing
        Dim arrayByteTag() As Byte = New Byte(3) {}
        Dim nLenHdrTrail As Integer = tagIDGCmdData.m_nPktHeader + tagIDGCmdData.m_nPktTrail
        Dim strTag As String = ""
        Dim arrayTLVal() As Byte = Nothing
        Dim tlv As tagTLV
        Dim tlvOp As tagTLVNewOp
        Dim nOffset, nTlvTagLen, nTlvValLenFromDataPortion As Integer
        Dim configAID As tagConfigCmdUnitAID = Nothing

        Try
            '------------------------[ Prepare Stage ]------------------------
            'If o_configGroupList is Nothing --> Create
            If o_AIDList IsNot Nothing Then
                'If o_configGroupList DOES Exist --> Cleanup Before using
                ClassTableListMaster.CleanupConfigurationsAIDList(o_AIDList)
            End If
            o_AIDList = New Dictionary(Of String, tagConfigCmdUnitAID)

            '
            With tagIDGCmdDataInfo
                If .m_nDataSizeRecv < nLenHdrTrail Then
                    Throw New Exception("No Recv Data, Recv Len = " & .m_nDataSizeRecv & ", < Header + CRC ( " & nLenHdrTrail & " )")
                End If
                '
                arrayByteDataPortion = New Byte(.m_nDataSizeRecv - 1 - nLenHdrTrail) {}
                '------------[Step 1]------------
                'Extract Data Portion without IDG Header & IDG Trail (i.e. CRC)
                Array.Copy(.m_refarraybyteData, tagIDGCmdData.m_nPktHeader, arrayByteDataPortion, 0, arrayByteDataPortion.Count)
            End With
            '------------------------[ End of Prepare Stage ]-----------------------

            '================[ Step 2 :  TLV Parser Loop Begin ]================
            nOffset = 0
            nTlvTagLen = 0
            While (True)
                'Start To Take Tag Length = 3-->2-->1 --> Failed if Not Found
                Select Case nTlvTagLen
                    Case 0
                        ' First Try
                        nTlvTagLen = 3
                    Case 1
                        '--------------[Special Case]-------------
                        ' While Get All Config Group, there are blank useless data &H00 between Group Block and Another's On. In that case we try to skip those useless &H00 until different value found or out of range.
                        If (arrayByteTag(0) = 0) Then
                            If SeekoutTillNoneZeroValueOrEndOfData(arrayByteDataPortion, nOffset) Then
                                nTlvTagLen = 0
                                Continue While
                            End If
                        End If
                        ' Failed to Detect All kind of Tag Length 3~1 bytes
                        LogLn("[Err] TLV Parsing Config_ParseFromReaderAIDInfo() =  Failed")
                        Form01_Main.LogLnDumpData(arrayByteDataPortion, arrayByteDataPortion.Count)
                        Exit Sub
                    Case 2
                        '------------[Step 4]------------
                        'Start To Take Tag Length = 1 if Step 3 (=2) NOT Found
                        nTlvTagLen = 1
                    Case 3
                        '------------[Step 3]------------
                        'Start To Take Tag Length = 2 if Step 2 (=3) NOT Found
                        nTlvTagLen = 2
                End Select
                '

                'Get Tag
                Array.Copy(arrayByteDataPortion, nOffset, arrayByteTag, 0, nTlvTagLen)
                ClassTableListMaster.ConvertFromArrayByte2String(arrayByteTag, nTlvTagLen, strTag, False)


                If m_dictTLVOp_RefList_Major.ContainsKey(strTag) Then
                    tlvOp = m_dictTLVOp_RefList_Major.Item(strTag)

                    'Check Length is Valid
                    nTlvValLenFromDataPortion = arrayByteDataPortion(nOffset + nTlvTagLen)
                    If nTlvValLenFromDataPortion > tlvOp.m_nLen Then
                        Throw New Exception("[Err] TLV (tag= " + strTag + " ) New Op Len = " & tlvOp.m_nLen & ", Get Len = " & nTlvValLenFromDataPortion)
                    End If
                    'Get Data & Parser
                    ' tlvOp.PktParser(tlv, tagIDGCmdDataInfo)
                    '-------[ Allocate Preferred Buffer Size to accommodate Value ]-------
                    If arrayTLVal Is Nothing Then
                        arrayTLVal = New Byte(nTlvValLenFromDataPortion - 1) {}
                    ElseIf (arrayTLVal.Count < nTlvValLenFromDataPortion) Then
                        Erase arrayTLVal
                        arrayTLVal = New Byte(nTlvValLenFromDataPortion - 1) {}
                    End If

                    '
                    Array.Copy(arrayByteDataPortion, nOffset + nTlvTagLen + 1, arrayTLVal, 0, nTlvValLenFromDataPortion)
                    '
                    tlv = New tagTLV(strTag, nTlvValLenFromDataPortion, arrayTLVal)
                    '
                    nOffset = nOffset + nTlvTagLen + 1 + nTlvValLenFromDataPortion
                    ' Reset State Machine to Begin
                    nTlvTagLen = 0

                Else
                    '=====================[ Continue To Next Tag Length ]=====================
                    Continue While
                End If

                '=================[ Seek Tag FFE4 , Group ID ]=================
                If (strTag = tagTLV.TAG_FFE4_or_DFEE2D_GrpID) Then ' --> 1st TLV Tag
                    'If (strTag = tagTLV.TAG_9F06) Then ' --> 2nd TVL Tag
                    '--> Put Current AID Info Block into the AID List 
                    Config_ParseFromReaderAIDInfoSetupAIDTabTxtAndAddToList(o_AIDList, configAID)
                    '--> Create New AID Info Block
                    configAID = New tagConfigCmdUnitAID(EnumConfigCmd.CONFIG_AID_SET)
                End If

                '----------------[Step 8 : Add New TLV To current AID Info TLV List ]----------------------------
                With configAID
                    'If Found Add Parsed TLV (Or Replace) to TLV Table
                    If .m_dictTLV.ContainsKey(tlv.m_strTag) Then
                        LogLn("[Warning] Duplicated TLV = " + tlv.m_strTag + ", Replace it as New One")
                        .m_dictTLV.Remove(tlv.m_strTag)
                    End If
                    .m_dictTLV.Add(tlv.m_strTag, tlv)
                    'Else Show Error Message (Current TLV Info) if Step 4 NOT Found
                End With

                '---------------[Step 9: End of TLV Parsing Progress]--------------
                If nOffset = arrayByteDataPortion.Count Then
                    Exit While
                ElseIf nOffset > arrayByteDataPortion.Count Then
                    Throw New Exception("TLV Data Error")
                End If
            End While
            '================[ TLV Parser Loop End ]================

            '-----------[ Step 10 : Put the last AID Info into the AID List ]-------------
            '
            Config_ParseFromReaderAIDInfoSetupAIDTabTxtAndAddToList(o_AIDList, configAID)
            ' For Debug ONLY
            If m_bDebug Then
                Form01_Main.LogLnDumpData(arrayByteDataPortion, arrayByteDataPortion.Count)
            End If
        Catch ex As Exception
            LogLn("[Err Dumping]")
            Form01_Main.LogLnDumpData(arrayByteDataPortion, arrayByteDataPortion.Count)
            LogLn("[Err] Config_ParseFromReaderAIDInfo() = " + ex.Message)
            ' Free configGroup
            configAID.Cleanup()
            configAID = Nothing
        Finally
            If arrayByteDataPortion IsNot Nothing Then
                ' Make Sure it's error exiting, then we have to release o_configGroupList
                If nOffset <> arrayByteDataPortion.Count Then
                    ClassTableListMaster.CleanupConfigurationsAIDList(o_AIDList)
                End If
                '
                Erase arrayByteDataPortion
            Else
                If o_AIDList IsNot Nothing Then
                    ClassTableListMaster.CleanupConfigurationsAIDList(o_AIDList)
                End If
            End If

            If arrayByteTag IsNot Nothing Then
                Erase arrayByteTag
            End If

            If arrayTLVal IsNot Nothing Then
                Erase arrayTLVal
            End If
        End Try
    End Sub
'
      '======================[CA Public Key File Loader & Parser ]==============================
    Shared Sub ParserItConfigCmd_TrailCommentSaveAndTruncate(ByRef strIn As String, ByRef strCommentOut As String, Optional ByVal bTruncate As Boolean = False, Optional ByVal chCommentTag As Char = ";"c)
        Dim position As Integer = strIn.IndexOf(chCommentTag)

        If position = -1 Then
            strCommentOut = ""
            Return
        Else
            strCommentOut = strIn.Substring(position)
            ' Truncate It ?
            If bTruncate Then
                strIn = strIn.Replace(strCommentOut, "")
            End If
        End If

        'Remove Comment Delimiter ";"
        If strCommentOut <> "" Then
            strCommentOut = Replace(strCommentOut, chCommentTag, "")
            strCommentOut = strCommentOut.Trim()
        End If

        ' Truncate Left Blank
        If bTruncate Then
            strIn = RTrim(strIn)
        End If
    End Sub


    '----[Example]-----
    '# Removed RID A0 00 00 00 04, Index 02
    '# Removed RID = B0 12 34 56 78, Indices F5, F6, F9
    Private Function CAPK_LoadFromFileParserLineRemoveRID(strIn As String, TestArray As String(), strTrailComment As String) As Boolean
        Dim chIndexTag() As String = {"Index", "Indices", "Indexes"}
        Dim nIdx As Integer
        Dim position As Integer
        Dim strRID As String
        Dim strIndex As String


        Try
            'Seek out the delimiter "index" and so on.
            For nIdx = 0 To chIndexTag.Count - 1
                position = strIn.LastIndexOf(chIndexTag(nIdx))
                If position <> -1 Then
                    Exit For
                End If
            Next

            If position = -1 Then
                'Not "Removed RID ...." Format
                Return False
            Else
                ' Get Index
                strIndex = strIn.Substring(position)
                ' Get RID
                strRID = strIn.Replace(strIndex, "")
            End If

            '1.Process RID 
            strRID = Replace(strRID, ClassCAPKUnit.m_strCAPK11RemoveRID, "")
            strRID = Replace(strRID, "=", "")
            strRID = Replace(strRID, ",", "")
            strRID = Trim(strRID)

            '2. Process Index
            strIndex = Replace(strIndex, chIndexTag(nIdx), "")
            'Replace ','
            strIndex = Replace(strIndex, ",", "")
            strIndex = Trim(strIndex)

            'Add to Delete List
            m_refobjCTLM.m_dictCAPKCmdDelList.Add(strRID, New ClassCAPKDelUnit(strRID, strIndex))

            Return True

        Catch ex As Exception
            Throw New Exception(ex.Message)
        Finally

        End Try
        '
    End Function

    Private Function IsWaitModulusDataMultipleTextLineIsDone() As Boolean
        Dim strList As String() = Split(m_strInPre)

        If ((strList.Count() - 2) >= m_tagCAPKUnit.u16ModLength) Then
            Return True
        End If

        Return False
    End Function


    'Shared Sub TrimFromPosOfStrArray(TestArray As String(), nPos As Integer, ByRef strIDHex As String)
    '    Dim sb As StringBuilder = New StringBuilder
    '    Dim nIdx As Integer

    '    For nIdx = nPos To TestArray.Count - 2
    '        sb.Append(TestArray(nIdx) + " ")
    '    Next
    '    sb.Append(TestArray(TestArray.Count - 1))
    '    strIDHex = sb.ToString()

    'End Sub

    Private Sub ParserItConfigCmd_NewSetConfiguration(ByRef strIn As String)
        Config_LoadFromFile2PaserLine_NewSetGroup(strIn)
    End Sub

    Private Function Config_LoadFromFile2PaserLine_TLV_GetLength(TestArray As String(), ByVal nIdxStart As Integer) As String
        Dim nTmp As Integer = TestArray.Count - 1
        Dim nIdx As Integer
        Dim strChk As String = ""

        If (nIdxStart > nTmp) Then
            Throw New Exception("[Err] Config_LoadFromFile2PaserLine_TLV_GetLength() = Index (" + nIdxStart + ") is out of Range (" + nTmp + ")")
        End If
        '
        For nIdx = nIdxStart To nTmp
            strChk = TestArray(nIdx)
            If ((strChk <> "") And (strChk <> " ")) Then
                Exit For
            End If
        Next

        If (nIdx > nTmp) Then
            Throw New Exception("[Err] Config_LoadFromFile2PaserLine_TLV_GetLength() = Length Data Not Found. From idx = " + nIdxStart + " to Max(" + nTmp + ")")
        End If

        Return strChk
    End Function

    Private Function Config_LoadFromFile2PaserLine_FormatNormalize(strIn As String) As String()
        Dim strList As String() = Split(strIn)
        Dim strListOut As String()
        Dim nIdx, nIdxMax As Integer
        Dim listIdx As List(Of Integer) = New List(Of Integer)
        Dim strChk As String

        nIdxMax = strList.Count() - 1
        'Sorting Out Valid Split sub String Items
        For nIdx = 0 To nIdxMax
            strChk = strList(nIdx)
            If ((strChk <> "") And (strChk <> " ")) Then
                listIdx.Add(nIdx)
            End If
        Next
        'Now Move to Out String List
        strListOut = New String(listIdx.Count - 1) {}
        nIdx = 0
        For Each iteratorIdx In listIdx
            strListOut(nIdx) = strList(iteratorIdx)
            nIdx = nIdx + 1
        Next
        Erase strList

        Return strListOut
    End Function


    '                                                         -->  [AID] ???
    ' Param: [IN] Line Text from CA Public 
    ' Return Value = True -> Parser Okay, otherwise False
    Public Sub CAPK_LoadFromFileParserLine(ByVal strIn As String, ByVal lTxtLn As Long)

        Dim chCommentDelimiter As Char = "#"c '";"c
        '
        Dim TestArray() As String
        Dim TestArrayFirstLevel As String
        'Second Level for "Removed RID"
        Dim TestArraySecondLevel As String

        Dim aryTrimChars() As Char = {" "c, "'"c}
        '
        Dim bTrailCommentFound As Boolean = False

        Try
            'Dim tagCAPKUnitTmp As tagCAPKUnit
            Dim tagTLVNewOpCAPKTmp As tagTLVNewOpCAPK

            '============[String Normalize & Preprocessing]============
            '0. Special Case, Data is move to 2nd line status
            'Now we Compose 2 lines into ONE
            If m_CAPK_bWait2ndTextLine Then
                strIn = Trim(m_strInPre) + " " + Trim(strIn)
                m_strInPre = strIn
                If (IsWaitModulusDataMultipleTextLineIsDone()) Then
                    m_CAPK_bWait2ndTextLine = False
                    strIn = m_strInPre
                    m_strInPre = ""
                Else
                    m_lConfigTextLine = lTxtLn
                    Return
                End If
            End If

            'Replace all [Table] with NULL Char
            strIn = Replace(strIn, vbTab, "")
            TestArray = Split(strIn)
            TestArrayFirstLevel = TestArray(0)
            TestArraySecondLevel = TestArrayFirstLevel
            If TestArray.Count > 1 Then
                TestArraySecondLevel = TestArraySecondLevel + " " + TestArray(1)
            End If

            ' Find Trailing Comment & Truncate it
            If TestArrayFirstLevel.ToCharArray()(0) <> chCommentDelimiter Then
                If strIn.Contains(chCommentDelimiter) Then
                    ClassReaderCommanderParser.ParserItConfigCmd_TrailCommentSaveAndTruncate(strIn, m_strTrailComment, True, chCommentDelimiter)
                End If
            End If


            '===========[Init Stuffs]=============
            TestArrayFirstLevel = TestArrayFirstLevel.Trim(aryTrimChars)

            'Increase line number of config text
            m_lConfigTextLine = lTxtLn

            'Change to Upper Case for Key Seeking
            'TestArrayFirstLevel = TestArrayFirstLevel.ToUpper()

            '=========[Begin To Parse the Text Line Process]=========


            '1 Check Comment Type First
            If TestArrayFirstLevel = chCommentDelimiter Then
                'Remember Important Comment Note
                Dim strIterator As String
                For Each strIterator In m_listKeyGroupComment
                    If strIn.Contains(strIterator) Then
                        'm_lConfigTextLineKeywordMaster = lTxtLn
                        Exit For
                    End If
                Next

                ' 2. Check "Removed RID" Value
            ElseIf ClassCAPKUnit.m_strCAPK11RemoveRID = TestArraySecondLevel Then
                '------[Example]-------
                '# Removed RID A0 00 00 00 04, Index 02
                '# Removed RID = B0 12 34 56 78, Indices F5, F6, F9
                CAPK_LoadFromFileParserLineRemoveRID(strIn, TestArray, m_strTrailComment)

                ' 3. Check Key Group Field Type
            ElseIf m_dictCAPK_TLV.ContainsKey(TestArrayFirstLevel) Then
                tagTLVNewOpCAPKTmp = m_dictCAPK_TLV.Item(TestArrayFirstLevel)
                tagTLVNewOpCAPKTmp.m_funcptrTLVParserCAPK(Me, tagTLVNewOpCAPKTmp, strIn, TestArray, m_strTrailComment, m_tagCAPKUnit)
                ' Save for next time usage
                If m_CAPK_bWait2ndTextLine Then
                    m_strInPre = strIn
                End If
                ' 4. Unknown Text Line
            Else
                Throw New Exception("Unknown Line Statement")
            End If

            '================[End of Parser Process]================

            '==================================================================================
        Catch ex As Exception
            Dim strMsg As String = "[Except Err]  CAPK.ParseIt() ==> Invalid Text Ln = " & m_lConfigTextLine & ", Text = " + strIn + "," + ex.Message
            LogLn(strMsg)
        Finally
            ' Record All Contents
            m_refobjCTLM.m_dictCAPKCmdLineMap.Add(m_lConfigTextLine, strIn)

            'Release  Array Resource
            Erase TestArray
            TestArray = Nothing
        End Try
    End Sub

    Private Sub CAPK_LoadFromFileParser(strCAPKConfigFilePath As String)
        Try
            ' Dim tagType As ClassTxtFileParserConfigurations.ConfigCmd

            'Get Memory File Obj
            Dim reader = My.Computer.FileSystem.OpenTextFieldParser(strCAPKConfigFilePath)
            reader.TextFieldType = Microsoft.VisualBasic.FileIO.FieldType.Delimited
            reader.Delimiters = {"#@*$(@#&"}

            Dim currentRow As String()
            Dim nIdx As Integer = 0

            LogLn("--------> FileName = " + strCAPKConfigFilePath)

            nIdx = nIdx + 1

            'Start to parser every line
            While Not reader.EndOfData
                currentRow = reader.ReadFields()

                Dim currentField As String
                For Each currentField In currentRow

                    'Parser it
                    'refobjTxtFileParserCAPK.ParseIt(currentField, nIdx)
                    CAPK_LoadFromFileParserLine(currentField, reader.LineNumber - 1)

                Next
                nIdx = nIdx + 1
            End While
        Catch ex As Exception ' Microsoft.VisualBasic.FileIO.MalformedLineException
            MsgBox("Line " & ex.Message &
            "is not valid and will be skipped.")
        Finally

        End Try
    End Sub

    ' Load CAPK configuration for Transaction Test
    Public Function CAPK_LoadFromFile(Optional ByVal strFileNameCAPK As String = "Transaction\CAPK.txt") As Boolean
        Dim bResult As Boolean = False

        Try
            '=================[ 1. Check Load File Path is available ]===============
            'Cleanup File Path first
            m_strCAPKConfigFilePath = ""

            If (Not System.IO.File.Exists(strFileNameCAPK)) Then
                LogLn("[Err] No CAPK Configuration File ( " + strFileNameCAPK + " ) Found")
                Return False
            End If
            'Check file type is .txt
            If strFileNameCAPK.Contains(".txt") Then
                m_strCAPKConfigFilePath = strFileNameCAPK
            End If

            '======[ 2. If the input file name is invalid, then to show up  open file dialogbox to select file by the user ]======
            If m_strCAPKConfigFilePath.Equals("") Then
                Dim dlgResult As System.Windows.Forms.DialogResult
                '
                If m_winobjOFD Is Nothing Then
                    Throw New Exception("Open File Dialog isn't created")
                End If
                '
                'm_winobjOFD.InitialDirectory = "c:\" 
                m_winobjOFD.InitialDirectory = My.Computer.FileSystem.CurrentDirectory
                m_winobjOFD.Filter = "text files (*.txt)|*.txt|All files (*.*)|*.*"
                m_winobjOFD.FilterIndex = 1
                m_winobjOFD.RestoreDirectory = True
                m_winobjOFD.Title = "Open CA Public Configuration File (.txt)"

                'Select CAPK Text File Name
                dlgResult = m_winobjOFD.ShowDialog()

                If (dlgResult <> System.Windows.Forms.DialogResult.OK) And (dlgResult <> System.Windows.Forms.DialogResult.Yes) Then
                    LogLn("[Note] Cancel CA Public Text File Selection And Exit")
                    Return False
                End If

                If m_winobjOFD.FileName = "" Then
                    Throw New Exception("No File is selected")
                End If
                'Update selected File Name
                m_strCAPKConfigFilePath = m_winobjOFD.FileName
            End If

            If m_strCAPKConfigFilePath.Equals("") Then
                Return False
            End If

            '1)Cleanup CAPK List
            m_refobjCTLM.CleanupCAPK()
            'KeyGroupTableAndTabPagesCleanUp(1)

            'Load and Parse the configuration text file line by line
            CAPK_LoadFromFileParser(m_strCAPKConfigFilePath)

            'Check the text file is valid CAPK File
            If Not m_refobjCTLM.bIsDataAvailableCAPK Then
                Beep()
                Throw New Exception("The Text File ( " + m_strCAPKConfigFilePath + " ) Is Not Valid CA Public Key (CAPK) File")
                m_strCAPKConfigFilePath = ""
            End If

            'Put RIDs  into New(or two more) Page(s)
            'm_objUIDriver.CAPKTagPagesRIDIndex(m_winobjManuallyRIDIndexTabPages)

            bResult = True
        Catch ex As Exception
            LogLn("[Err]" + ex.Message)
        Finally

        End Try

        Return bResult

    End Function
    '

    Private Sub Config_LoadFromFile2PaserLine_TLV(ByRef strIn As String, ByRef TestArray() As String)
        Dim TestArrayFirstLevel As String = TestArray(0)
        Dim nTLVLen As Integer = Convert.ToInt32(Config_LoadFromFile2PaserLine_TLV_GetLength(TestArray, 1), 16)
        Dim nIdxChk As Integer = 2
        Dim nIdxChkEnd As Integer = nIdxChk + nTLVLen - 1
        Dim nIdxData As Integer
        Dim strTrailComment As String = ""
        Dim sbTLVData As StringBuilder = New StringBuilder
        Dim strIDOld As String = ""

        TestArrayFirstLevel.ToUpper()

        '------[The Following is For TLV structure test ONLY]------
        'Dim arrayVal4() As Byte = New Byte() {&H0, &HA0, &H8D, &HAA}
        'Dim arrayVal1() As Byte = New Byte() {&H0, &HA0, &H8D, &HAA}
        'Dim arrayVal0() As Byte = New Byte() {}
        'Dim tagTLVTmp As tagTLV = New tagTLV("9F06", 4, "00 A0 F9 A8")
        'Dim tagTLVTmp2 As tagTLV = New tagTLV("9F06", 0, arrayVal0)

        Try
            '------[The Following is For TLV structure test ONLY]------
            'tagTLVTmp.Reset("9C", 0, arrayVal0)
            'tagTLVTmp.Reset("9F06", 4, "00 A0 F9 A8", "Comment001")
            'tagTLVTmp.Reset("9C", 0, "", "Comment009")
            'tagTLVTmp.Reset("9C", 0, arrayVal1, "")

            'Command Sequence Validation
            If Not m_colConfigCmdValidNext.Contains(m_nConfigCmd) Then
                LogLn("[Error] Invalid Config Text Ln = " & m_lConfigTextLine)
                Exit Sub
            End If

            ' Verify TLV Fomrat
            If m_dictTLVOp_RefList_Major.ContainsKey(TestArrayFirstLevel) Then
                Dim opTLV As tagTLVNewOp = m_dictTLVOp_RefList_Major.Item(TestArrayFirstLevel)

                'If TestArrayFirstLevel = "FF69" Then
                '    Dim i As Integer
                '    i = 0
                'End If
                ' Verify TLV Length is valid
                If (opTLV.m_nLen < nTLVLen) Or (opTLV.m_nLenMin > nTLVLen) Then
                    Dim strMsg As String = "TLV Length Range Invalid Std:" & opTLV.m_nLenMin & "-" & opTLV.m_nLen & ",Current = " & nTLVLen
                    Throw New Exception(strMsg)
                End If

                ' Verify TLV Data Length = Length Definition
                ' TLV(1) + Length(1) + Data(nTLVLen)
                'If (TestArray.Count <> (2 + nTLVLen)) Then
                '    Dim strMsg As String = "TLV Length" & TestArray.Count & " != TLV Data Size " & (2 + nTLVLen)
                '    Throw New Exception(strMsg)
                'End If

                'Save Old Group ID or Old AID Before <> CFADefM_STANDARD mode (Legacy Mode)
                ClassTableListMaster.TrimFromPosOfStrArray(TestArray, 2, strIDOld)

                'Special Case for FFE4  (Group ID)/ 9F06  (AID) to Get Default Value for Configurations Form [Add AID] or [Add Group] Functions
                'If m_nCFADefM <> EnumConfigurationsFormAddDefaultMode.CFADefM_STANDARD Then
                '    If TestArray.Count > 2 Then
                '        Config_LoadFromFile2PaserLine_TLV_ConfigurationsFormDefaultGroupOrAID(TestArrayFirstLevel, TestArray)
                '    End If
                'End If

                ' Translate TLV Value  From String To Byte Array
                nIdxData = 0
                'There is possible nIdxChkEnd = 1 If nTLVLen =0
                For nIdxChk = 2 To nIdxChkEnd
                    'Build TLV Data in Binary Array
                    'If m_tagTLVTmp.m_arrayTLVData IsNot Nothing Then
                    '    m_tagTLVTmp.m_arrayTLVData.SetValue(CByte(TestArray(nIdxChk)), nIdxData)
                    'End If

                    'Rebuild TLV Data in string type
                    If nIdxData = 0 Then
                        sbTLVData.Append(TestArray(nIdxChk))
                    Else
                        sbTLVData.Append(" " + TestArray(nIdxChk))
                    End If

                    ' Data Index Advanced to next
                    nIdxData = nIdxData + 1
                Next

                ' Make TLV Data
                m_tagTLVTmp.Reset(TestArray(0), nTLVLen, sbTLVData.ToString(), strTrailComment)

                ' Add Comment of TLV if Available
                m_tagTLVTmp.m_strCommentTrail = m_strTrailComment

                'Add Line number
                m_tagTLVTmp.m_lTxtLn = m_lConfigTextLine

                ' Add TLV to Current AID TLV List / Group TLV Dictionary
                Select Case m_nConfigCmdIndex
                    Case EnumConfigCmd.CONFIG_AID_DELETE
                        ClassTableListMaster.TrimFromPosOfStrArray(TestArray, 2, strIDOld)
                        'Use AID as Key in Removed AID List
                        If Not m_refobjCTLM.m_dictAIDListDel.ContainsKey(strIDOld) Then
                            m_refobjCTLM.m_dictAIDListDel.Add(strIDOld, m_tagTLVTmp)
                        Else
                            LogLn("[Warning] Remove AID Duplicated,AID = " + strIDOld + ",Ln( " & m_lConfigTextLine & " ), Txt= " + strIn)

                        End If

                    Case EnumConfigCmd.CONFIG_AID_SET
                        If (m_nCFADefM = EnumConfigurationsFormAddDefaultMode.CFADefM_STANDARD) And (TestArray(0) = tagTLV.TAG_9F06_TermID_AID) Then
                            'Normal Mode, Terminal ID Old = Terminal ID
                            m_tagConfigCmdUnitAID.m_strTerminalIDOld = m_tagConfigCmdUnitAID.m_strTerminalID
                        End If

                        'Special Case For TLV Group ID (FFE4) in Default Value Mode, Group ID Could be auto regenerated in 
                        'ParseItConfigCmd_TLV_ConfigurationsFormDefaultGroupOrAID()
                        If (m_tagTLVTmp.m_strTag = tagTLV.TAG_FFE4_or_DFEE2D_GrpID) And (m_nCFADefM = EnumConfigurationsFormAddDefaultMode.CFADefM_AIDLIST) And (m_tagConfigCmdUnitAID.m_dictTLV.ContainsKey(tagTLV.TAG_FFE4_or_DFEE2D_GrpID)) Then
                            LogLn("[Warning][AID Default Mode] Changing Group ID From " & m_tagTLVTmp.m_arrayTLVal(0) & " To " + m_tagConfigCmdUnitAID.m_strGroupID)
                        Else
                            'Add To TLV List of this AID
                            m_tagConfigCmdUnitAID.m_dictTLV.Add(m_tagTLVTmp.m_strTag, m_tagTLVTmp)
                        End If
                        '
                    Case EnumConfigCmd.CONFIG_GROUP_SET, EnumConfigCmd.CONFIG_GROUP_DELETE, EnumConfigCmd.CONFIG_CONFIGURATION_SET
                        If m_nCFADefM = EnumConfigurationsFormAddDefaultMode.CFADefM_STANDARD And TestArray(0) = tagTLV.TAG_FFE4_or_DFEE2D_GrpID Then
                            'Normal Mode, Group ID Old = Group ID
                            m_tagConfigCmdUnitGroup.m_nGroupIDOld = m_tagConfigCmdUnitGroup.m_nGroupID
                        End If
                        'Add To TLV List of this Group
                        m_tagConfigCmdUnitGroup.m_dictTLV.Add(m_tagTLVTmp.m_strTag, m_tagTLVTmp)
                        '
                    Case Else
                        Dim strMsg As String = "Invalid Config Command Set = " & m_nConfigCmdIndex & ", Line =" & m_lConfigTextLine & ", Txt = " + strIn
                        Throw New Exception(strMsg)
                End Select
            Else
                Dim strMsg As String = "TLV Tag Not Defined(Not Found), T= " + TestArrayFirstLevel + "L= " & nTLVLen & "V= " + sbTLVData.ToString()
                Throw New Exception(strMsg)
            End If
        Catch ex As Exception
            LogLn("[Err TLV Parse]" + ex.Message + ", TLV  =" + strIn + ", Ln =" & m_lConfigTextLine)
        Finally
            'TODO: Free Local Resources
        End Try
    End Sub


    Public Shared Sub CRL_GetCRLMassFromResponse(ByRef o_tagIDGCmdDataInfo As tagIDGCmdData, ByRef o_dictCRLCmdAddList As Dictionary(Of String, tagCRLCmdUnit), ByRef o_CRLRecInfo As GetCRLRecNumInfo)
        Dim nPosOff, nRecTotalSize As Integer
        Dim u32RNRd, u32RNRst, u32SRS As UInt32
        Dim tagCRLSingle As tagCRLCmdUnitSingle
        Dim tagCRLUnit As tagCRLCmdUnit
        Dim arrayByteRID = Nothing, arrayByteSerialNo As Byte() = Nothing
        Dim byteIndex As Byte

        Try
            '0. Cleanup o_dictCRLCmdAddList / New It
            If o_dictCRLCmdAddList Is Nothing Then
                o_dictCRLCmdAddList = New Dictionary(Of String, tagCRLCmdUnit)
            Else
                ClassTableListMaster.CleanupCRLLists(o_dictCRLCmdAddList)
            End If

            nPosOff = 0
            '1. 12 bytes Rece Info Header = Rec Num Read(RNRrd): 4 bytes + Rec Num Rest(RNRst): 4 bytes + Single Rec Size(SRS): 4 bytes
            o_tagIDGCmdDataInfo.ParserDataGet(u32RNRd, nPosOff)
            o_tagIDGCmdDataInfo.ParserDataGet(u32RNRst, nPosOff)
            o_tagIDGCmdDataInfo.ParserDataGet(u32SRS, nPosOff)
            nRecTotalSize = CInt(12 + u32SRS * u32RNRd)

            ' 1.1 Check SRS must be 9 bytes
            If u32SRS <> 9 Then
                Throw New Exception("GetCRLMassFromResponse(), CRL Single Size = " & u32SRS)
            End If
            'Copy to Return Value
            o_CRLRecInfo.u32RecNumRead = u32RNRd
            o_CRLRecInfo.u32RecNumRest = u32RNRst
            o_CRLRecInfo.u32SingleRecSize = u32SRS

            '2. Use For Each Rec SRS Size ( 9 bytes = RID:5 bytes + Index:1 byte + Serial No: 3 bytes) to get from Array
            While (nPosOff < nRecTotalSize)

                tagCRLSingle = New tagCRLCmdUnitSingle

                o_tagIDGCmdDataInfo.ParserDataGetArray(nPosOff, 5, arrayByteRID)
                o_tagIDGCmdDataInfo.ParserDataGetByte(byteIndex, nPosOff)
                o_tagIDGCmdDataInfo.ParserDataGetArray(nPosOff, 3, arrayByteSerialNo)
                '
                ClassTableListMaster.ConvertFromArrayByte2String(arrayByteRID, tagCRLSingle.m_strRID)
                tagCRLSingle.m_strIndex = String.Format("{0,2:X2}", byteIndex)
                ClassTableListMaster.ConvertFromArrayByte2String(arrayByteSerialNo, tagCRLSingle.m_strSerialNo)
                '
                '================[ Offset Value is accumulated in ParserDataXXXX()  ]=================
                'nPosOff = nPosOff + CType(u32SRS, Integer)

                'Prepare RID+Index List ?
                If o_dictCRLCmdAddList.ContainsKey(tagCRLSingle.strRIDIndex) Then
                    tagCRLUnit = o_dictCRLCmdAddList.Item(tagCRLSingle.strRIDIndex)
                Else
                    tagCRLUnit = New tagCRLCmdUnit(EnumCRLCmd.CRLCMD_REVOCATIONLIST_ADD)
                    tagCRLUnit.m_strRIDIndex = tagCRLSingle.strRIDIndex
                    o_dictCRLCmdAddList.Add(tagCRLSingle.strRIDIndex, tagCRLUnit)
                End If
                tagCRLUnit.m_dictCRLCmdList.Add(tagCRLSingle.strSerialNo, tagCRLSingle)

            End While
        Catch ex As Exception
            Throw 'ex
        End Try

    End Sub

#End If
    '
#Region "IDisposable Support"
    Private disposedValue As Boolean ' To detect redundant calls

    ' IDisposable
    Protected Overridable Sub Dispose(disposing As Boolean)
        If Not Me.disposedValue Then
            If disposing Then
                ' TODO: dispose managed state (managed objects).
                ' dispose managed resources
                If (m_winobjOFD IsNot Nothing) Then
                    m_winobjOFD.Dispose()
                    m_winobjOFD = Nothing
                End If
            End If

            ' TODO: free unmanaged resources (unmanaged objects) and override Finalize() below.
            ' TODO: set large fields to null.
        End If
        Me.disposedValue = True
    End Sub

    ' TODO: override Finalize() only if Dispose(ByVal disposing As Boolean) above has code to free unmanaged resources.
    'Protected Overrides Sub Finalize()
    '    ' Do not change this code.  Put cleanup code in Dispose(ByVal disposing As Boolean) above.
    '    Dispose(False)
    '    MyBase.Finalize()
    'End Sub

    ' This code added by Visual Basic to correctly implement the disposable pattern.
    Public Sub Dispose() Implements IDisposable.Dispose
        ' Do not change this code.  Put cleanup code in Dispose(disposing As Boolean) above.
        Dispose(True)
        GC.SuppressFinalize(Me)
    End Sub
#End Region

End Class
