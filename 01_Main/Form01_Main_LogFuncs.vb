﻿Partial Public Class Form01_Main

    Private Sub LogCleanup()
        m_logRichText.Clear()
        '
        '---------------[ External Information ]---------------
        If (m_refRichTextBox_LogWin IsNot Nothing) Then
            If (Not m_refRichTextBox_LogWin.IsDisposed) Then
                m_refRichTextBox_LogWin.Clear()
            End If
        End If
    End Sub

    Private m_lstStrInfoFiltered As List(Of String) = New List(Of String)() From {
        "<< RxSts ="
        }

    Private Function Log_bIsFiltered(ByVal strInfoCheck As String)
        For Each itrStrFilter As String In m_lstStrInfoFiltered
            If (strInfoCheck.Contains(itrStrFilter)) Then
                Return True
            End If
        Next
        '
        Return False
    End Function

    Public Sub Log(ByRef strInfo As String, Optional ByVal bCrLf As Boolean = True)
        '
        If (m_logRichText Is Nothing) Then
            'Sometimes there is an exception since  m_logRichText 
            'is disposed but here we are still looking for access it 
            'to display messages
            Return
        End If

        If (m_logRichText.IsDisposed) Then
            Return
        End If

        'If (m_bLogBufferCapacitySafe) Then
        If (m_logRichText.Lines.Count > 10000) Then
            m_logRichText.Clear()
        End If
        'End If

        If (Log_bIsFiltered(strInfo)) Then
            '
            Return
        End If

        '
        If bCrLf Then
            strInfo = strInfo + vbCrLf
        End If
        '
        m_logRichText.AppendText(strInfo)
        'Scroll to Last Line
        m_logRichText.ScrollToCaret()
        '
        '---------------[ External Information ]---------------
        If (m_refRichTextBox_LogWin IsNot Nothing) Then
            If (Not m_refRichTextBox_LogWin.IsDisposed) Then
                m_refRichTextBox_LogWin.AppendText(strInfo)
                'Scroll to Last Line
                m_refRichTextBox_LogWin.ScrollToCaret()

            End If
        End If
    End Sub
    '
    Public Sub LogLn_MifareBlkRec(ByVal strInfo As String, Optional ByVal bIsClr As Boolean = True)
        If (bIsClr) Then
            m_RTxtBox_002_MifareBlock.Clear()
        End If
        '
        Log_MifareBlkRec(strInfo)
    End Sub
    '
    Public Sub Log_MifareBlkRec(ByRef strInfo As String, Optional ByVal bCrLf As Boolean = True)
        '
        If (m_RTxtBox_002_MifareBlock Is Nothing) Then
            'Sometimes there is an exception since  m_RTxtBox_002_MifareBlock 
            'is disposed but here we are still looking for access it 
            'to display messages
            Return
        End If

        If (m_RTxtBox_002_MifareBlock.IsDisposed) Then
            Return
        End If

        'If (m_bLogBufferCapacitySafe) Then
        'If (m_RTxtBox_002_MifareBlock.Lines.Count > 10000) Then
        '    m_RTxtBox_002_MifareBlock.Clear()
        'End If
        'End If
        m_RTxtBox_002_MifareBlock.Clear()
        '
        If bCrLf Then
            strInfo = strInfo + vbCrLf
        End If
        '
        m_RTxtBox_002_MifareBlock.AppendText(strInfo)
        'Scroll to Last Line
        m_RTxtBox_002_MifareBlock.ScrollToCaret()
        '
        ' 
#If 0 Then
        '---------------[ External Information ]---------------
        If (m_refRichTextBox_LogWin IsNot Nothing) Then
            If (Not m_refRichTextBox_LogWin.IsDisposed) Then
                m_refRichTextBox_LogWin.AppendText(strInfo)
                'Scroll to Last Line
                m_refRichTextBox_LogWin.ScrollToCaret()

            End If
        End If
#End If
    End Sub
    '
    '  '
    Public Sub LogLn_UID(ByVal strInfo As String, Optional ByVal bIsClr As Boolean = True)
        If (bIsClr) Then
            m_RTxtBox_001_UID.Clear()
        End If
        '
        Log_UID(strInfo)
    End Sub
    '
    Public Sub Log_UID(ByRef strInfo As String, Optional ByVal bCrLf As Boolean = True)
        '
        If (m_RTxtBox_001_UID Is Nothing) Then
            'Sometimes there is an exception since  m_RTxtBox_001_UID 
            'is disposed but here we are still looking for access it 
            'to display messages
            Return
        End If

        If (m_RTxtBox_001_UID.IsDisposed) Then
            Return
        End If

        'If (m_bLogBufferCapacitySafe) Then
        If (m_RTxtBox_001_UID.Lines.Count > 10000) Then
            m_RTxtBox_001_UID.Clear()
        End If
        'End If
        m_RTxtBox_001_UID.Clear()
        '
        If bCrLf Then
            strInfo = strInfo + vbCrLf
        End If
        '
        m_RTxtBox_001_UID.AppendText(strInfo)
        'Scroll to Last Line
        m_RTxtBox_001_UID.ScrollToCaret()
        '
        ' 
#If 0 Then
        '---------------[ External Information ]---------------
        If (m_refRichTextBox_LogWin IsNot Nothing) Then
            If (Not m_refRichTextBox_LogWin.IsDisposed) Then
                m_refRichTextBox_LogWin.AppendText(strInfo)
                'Scroll to Last Line
                m_refRichTextBox_LogWin.ScrollToCaret()

            End If
        End If
#End If
    End Sub
    '
    Public Sub LogLn(ByVal strInfo As String)
        Log(strInfo)
    End Sub

    Public Sub LogLn(ByVal strInfoList As List(Of String))
        Dim strInfo As String

        For Each strInfo In strInfoList
            Log(strInfo)
        Next
    End Sub

    '
    'strInfo, fontOld, m_refRichTextBox_LogWin.Width, m_refRichTextBox_LogWin.Height
    Public Function LogFindTextFontMatchSizeDependingOnSelectedFont(strInfo As String, fontOld As Drawing.Font, nAreaWidth As Integer, nAreaHeight As Integer, ByRef ref_fontNew As Font) As Boolean
        Dim sglFontSize As Single = fontOld.Size
        Dim sglFontSizeW, sglFontSizeH As Single
        Dim bIsAllowed As Boolean = False
        Dim rt As Size

        If (strInfo.Equals("")) Then
            Return False
        End If
        '0.Pre-Calculate Selected Font Size to haste performance
        rt = TextRenderer.MeasureText(strInfo, fontOld)
        sglFontSizeW = Math.Floor(nAreaWidth * sglFontSize / rt.Width)
        sglFontSizeH = Math.Floor(nAreaHeight * sglFontSize / rt.Height)
        If (sglFontSizeH > sglFontSizeW) Then
            sglFontSize = sglFontSizeW
        Else
            sglFontSize = sglFontSizeH
        End If

        'sglFontSize = Math.Floor(nAreaHeight * sglFontSize / rt.Height)
        '
        While (Not bIsAllowed)
            '1.Calculate Selected Font Size
            ref_fontNew = New Font("Calibri", sglFontSize)
            rt = TextRenderer.MeasureText(strInfo, ref_fontNew)

            '2. If area is equals or smaller than size
            If ((rt.Width <= nAreaWidth) And (rt.Height <= nAreaHeight)) Then
                bIsAllowed = True
            Else
                '3.Decrease Font Size 1.0F by 1.0F
                ref_fontNew.Dispose()
                ref_fontNew = Nothing
                sglFontSize = sglFontSize - 1.0F
                If (sglFontSize < 8.0F) Then
                    Exit While
                End If
            End If

        End While

        '
        Return bIsAllowed
    End Function
    '
    '   '
    'Exampe 1----------
    'INPUT: arraybyteData = { &H00,&HA0, &H0E, &H18}, nDataSize = 4
    'OUTPUT: "00 A0 0E 18"
    'Exampe 2----------
    'INPUT: arraybyteData = { &H00,&HA0, &H0E, &H18,&HF0,&HB0, &H4E, &HFF, &H3C, &HD3}, nDataSize = 10
    'OUTPUT: "00 A0 0E 18 F0 B0 4E FF"
    'OUTPUT: "3C D3"
    '[Special Note] <-- Don't Do Dump Function in Recv Thread since Dump Function is not thread Safe Function -->
    Public Shared Sub LogLnDumpData(ByVal arraybyteData As Byte(), ByVal nDataSize As Integer)
        LogLnDumpData(arraybyteData, 0, nDataSize)
    End Sub

    Public Shared Sub LogLnDumpData(ByVal arraybyteData As Byte(), ByVal nPosStart As Integer, ByVal nDataSize As Integer)
        LogLnDumpData(arraybyteData, nPosStart, nDataSize, 16)
    End Sub

    Public Shared Sub LogLnDumpData(ByVal arraybyteData As Byte(), ByVal nPosStart As Integer, ByVal nDataSize As Integer, ByVal nFormatLineWIdth As Integer)
        Dim strMsgList() As String = New String() {""}
        Dim nIdx As Integer
        '
        ClassTableListMaster.ConvertFromArrayByte2StringListFormated(arraybyteData, nPosStart, nDataSize, strMsgList, nFormatLineWIdth)
        '
        For nIdx = 0 To strMsgList.Count - 1
            If (m_pInstance IsNot Nothing) Then
                m_pInstance.LogLn(strMsgList(nIdx))
            Else
                System.Console.WriteLine(strMsgList(nIdx))
            End If
        Next
        '
        Erase strMsgList
        '
    End Sub

    Public Sub LogThreadSafe(ByVal strInfo As String)
        Dim tagLogMsgInfo As tagLogTimeUpdateInfo
        '---------[Wait for another thread processing]---------
        While (m_blistLogTimeUpdateCriticalSection)
            AppSleep(20)
        End While

        m_blistLogTimeUpdateCriticalSection = True
        ' Remove Process Here
        For Each tagLogMsgInfo In m_listLogTimeUpdate
            If tagLogMsgInfo.m_bIsRemoved Then
                m_listLogTimeUpdate.Remove(tagLogMsgInfo)
            End If
        Next
        ' Add to Msg List
        tagLogMsgInfo = New tagLogTimeUpdateInfo(strInfo)
        m_listLogTimeUpdate.Add(tagLogMsgInfo)
        '
        m_blistLogTimeUpdateCriticalSection = False
        '
    End Sub
End Class
