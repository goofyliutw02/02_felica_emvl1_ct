﻿Imports System.IO.Ports

'==================================================================================
' File Name: ClassReaderCommanderIDGGR.vb
' Description: The ClassReaderCommanderIDGGR is as IDG GR Class of Reader's Packet Parser & Composer.
' Who use it : 
' Revision:
' -----------------------[ Initial Version ]-----------------------
' 2014 May 30, by goofy_liu@idtechproducts.com.tw
'==================================================================================

Public Class ClassReaderProtocolIDGGR
    Inherits ClassReaderProtocolBasic
    '
    'Public Shared m_bDebug As Boolean = True

    '

    '=================[Private Variables Area]===================
    'Private m_arraybyteSendBuf As Byte()

    '=================[Public Variables Area]===================

    '=================[Private Sub/Func Area]=================


    '==================[Public Overrides Sub / Func Area ]=====================


    '===========================================[ IDG Command Packet TLV Composer & Parser Section ]=================================================
    '  9A", 3, 0, ), 9C", 1, 0), 9F65", 2, 0), 94", 1, 0), 5F24", 3, 0), FF69", 12, 0), 5F57", 1, 0), 5F2A", 2, 0), 5F36", 1, 0), 9F21", 3, 0) _
    ', 9F53", 1, 0), 9F7C", 20, 0), DF8104", 6, 0), DF8105", 6, 0), DF812D", 3, 0) _
    ', FFEE04", 1, 0), DF52", 1, 0), 9F01", 6, 0), 9F02", 6, 0), 9F03", 6, 0) _
    ', 9F09", 2, 0), 9F15", 2, 0), 9F16", 15, 0), 9F1A", 2, 0), 9F1B", 4, 0) _
    ', 9F1C", 8, 0), 9F1E", 8, 0), 9F33", 3, 0), 9F35", 1, 0), 9F40", 5, 0), 9F58", 1, 0) _
    ', 9F59", 3, 0), 9F5A", 1, 0), 9F5D", 6, 0), 9F5E", 2, 0), 9F5F", 6, 0) _
    ', 9F66", 4, 0), 9F6D", 2, 0), 9F6E", 4, 0), 9F7E", 1, 0), DF26", 1, 0), DF28", 3, 0) _
    ', DF29", 3, 0), DF2A", 6, 0), DF2B", 1, 0), DF2C", 1, 0), DF51", 1, 0) _
    ', DF63", 1, 0), DF64", 1, 0), DF65", 1, 0), DF66", 1, 0), DF68", 1, 0) _
    ', DF74", 1, 0), DF7E", 1, 0), FFF0", 3, 0), FFF1", 6, 0), FFF2", 8, 0) _
    ', FFF3", 2, 0), FFF4", 3, 0), FFF5", 6, 0), FFF7", 1, 0), FFF8", 1, 0) _
    ', FFF9", 1, 0), FFFA", 2, 0), FFFB", 1, 0), FFFC", 1, 0), FFFD", 5, 0) _
    ', FFFE", 5, 0), FFFF", 5, 0), 9F4E", 30, 0), DF811A", 3, 0), DF811C", 2, 0) _
    ', DF811D", 1, 0), DF811E", 1, 0), DF8124", 6, 0), DF8125", 6, 0), DF812C", 1, 0) _
    ', DF8130", 1, 0), 9F06", 16, 0), FFE1", 1, 0), FFE2", 1, 0), FFE3", 1, 0) _
    ', FFE4", 1, 0), FFE5", 1, 0), FFE8", 1, 0), FFE9", 15, 0), FFEA", 1, 0) _
    'Total = 85 ?
    '----------------[ IDG Command Packet TLV  Composer & Parser : FFE4  ]----------------
    Public Shared Sub m_pfuncCbParserFFE4(ByRef refobjTLVAttr As tagTLV, ByRef idgCmdData As tagIDGCmdData)
        Try
            'idgCmdData.m_refarraybyteData()
        Catch ex As Exception
            LogLn("[Err] FFE4 Callback Parser() = " + ex.Message)
        End Try
    End Sub

    Public Shared Sub m_pfuncCbComposer(ByRef objTLV As tagTLV, ByRef idgCmdData As tagIDGCmdData)
        Try
            Dim arrayByteData() As Byte = idgCmdData.m_refarraybyteData
            Dim arrayByteTag() As Byte = Nothing
            ' If TLV Length = 0, In Debug Mode Sending Out Warning Message
            If objTLV.m_nLen = 0 Then
                LogLn("[Warning] Tag = " + objTLV.m_strTag + " , Length = 0 (Unusable)")
                Return
            End If

            'Put Group Number Here
            With idgCmdData
                'Dim nPosStart As Integer = tagIDGCmdData.m_nPktHeader + .m_nDataSendOff
                Dim nPosStart As Integer = tagIDGCmdData.m_nPktHeader + .m_nDataSizeSend

                'Tag
                ClassTableListMaster.TLVauleFromString2ByteArray(objTLV.m_strTag, arrayByteTag)
                If (arrayByteTag Is Nothing) Then
                    Throw New Exception("Can't Translate Tag String " + objTLV.m_strTag + " To Byte Array")
                End If
                arrayByteTag.CopyTo(arrayByteData, nPosStart)
                ' .m_nDataSendOff = .m_nDataSendOff + arrayByteTag.Count
                nPosStart = nPosStart + arrayByteTag.Count
                'Length
                arrayByteData(nPosStart) = CType(objTLV.m_nLen, Byte)
                nPosStart = nPosStart + 1
                'Value / No Value if Length = &H00
                If objTLV.m_arrayTLVal IsNot Nothing Then
                    objTLV.m_arrayTLVal.CopyTo(arrayByteData, nPosStart)
                    nPosStart = nPosStart + objTLV.m_arrayTLVal.Count
                Else
                    nPosStart = nPosStart
                End If

                '.m_nDataSendOff = nPosStart - tagIDGCmdData.m_nPktHeader
                .m_nDataSizeSend = nPosStart - tagIDGCmdData.m_nPktHeader
            End With
        Catch ex As Exception
            LogLn("[Err] Common TLV Callback Packet Composer () = " + ex.Message)
        End Try
    End Sub
    '
    Public Shared Sub m_pfuncCbComposerCAPKRIDExponentField(ByRef objTLV As tagTLV, ByRef idgCmdData As tagIDGCmdData)
        Dim arrayTLVal4Bytes() As Byte = Nothing
        Try
            Dim arrayByteData() As Byte = idgCmdData.m_refarraybyteData

            ' If TLV Length = 0, In Debug Mode Sending Out Warning Message
            If objTLV.m_nLen = 0 Then
                LogLn("[Warning] Tag = " + objTLV.m_strTag + " , Length = 0 (Unusable)")
                Return
            End If

            With idgCmdData
                Dim nPosStart As Integer = tagIDGCmdData.m_nPktHeader + .m_nDataSizeSend

                '==================== [Special Note]====================
                ' Exponent Field normally its length is 1 byte or 3 bytes, but  IDG Cmd D0-03 @ Protocol2 needs fixed 4 bytes field to set this value
                ' Special Handler here.
                arrayTLVal4Bytes = New Byte(3) {&H0, &H0, &H0, &H0}
                Array.Copy(objTLV.m_arrayTLVal, 0, arrayTLVal4Bytes, arrayTLVal4Bytes.Count - objTLV.m_nLen, objTLV.m_nLen)
                Array.Copy(arrayTLVal4Bytes, 0, arrayByteData, nPosStart, arrayTLVal4Bytes.Count)
                'nPosStart = nPosStart + objTLV.m_nLen
                nPosStart = nPosStart + arrayTLVal4Bytes.Count
                .m_nDataSizeSend = nPosStart - tagIDGCmdData.m_nPktHeader
            End With
        Catch ex As Exception
            LogLn("[Err] TLV New OP CAPK Exponent Callback Packet Composer () = " + ex.Message)
        Finally
            If arrayTLVal4Bytes IsNot Nothing Then
                Erase arrayTLVal4Bytes
            End If
        End Try
    End Sub

    Public Shared Sub m_pfuncCbComposerCAPK(ByRef objTLV As tagTLV, ByRef idgCmdData As tagIDGCmdData)
        Try
            Dim arrayByteData() As Byte = idgCmdData.m_refarraybyteData
            'Dim arrayByteTag() As Byte
            ' If TLV Length = 0, In Debug Mode Sending Out Warning Message
            If objTLV.m_nLen = 0 Then
                LogLn("[Warning] Tag = " + objTLV.m_strTag + " , Length = 0 (Unusable)")
                Return
            End If

            With idgCmdData
                Dim nPosStart As Integer = tagIDGCmdData.m_nPktHeader + .m_nDataSizeSend

                '==================== [Special Note]====================
                ' Exponent Field normally its length is 1 byte or 3 bytes, but  IDG Cmd D0-03 @ Protocol2 needs fixed 4 bytes field to set this value
                ' Special Handler here.
                Array.Copy(objTLV.m_arrayTLVal, 0, arrayByteData, nPosStart, objTLV.m_nLen)
                nPosStart = nPosStart + objTLV.m_nLen

                .m_nDataSizeSend = nPosStart - tagIDGCmdData.m_nPktHeader
            End With
        Catch ex As Exception
            LogLn("[Err] TLV New OP CAPK Callback Packet Composer () = " + ex.Message)
        End Try
    End Sub
    ''----------------[ IDG Command Packet TLV  Composer & Parser :  ]----------------
    'Public Shared Sub m_pfuncCbParser(ByRef refobjTLVAttr As tagTLVNewOp, ByRef idgCmdData As tagIDGCmdData)
    '    Throw New Exception("[Warning] tagTLVNewOp.m_pfuncCbDummy (T=" + refobjTLVAttr.m_strTag + ",L=" & refobjTLVAttr.m_nLen & ") is running")
    'End Sub
    'Public Shared Sub m_pfuncCbComposer(ByRef refobjTLVAttr As tagTLVNewOp, ByRef idgCmdData As tagIDGCmdData)
    '    Throw New Exception("[Warning] tagTLVNewOp.m_pfuncCbDummy (T=" + refobjTLVAttr.m_strTag + ",L=" & refobjTLVAttr.m_nLen & ") is running")
    'End Sub
    ''----------------[ IDG Command Packet TLV  Composer & Parser :  ]----------------
    'Public Shared Sub m_pfuncCbParser(ByRef refobjTLVAttr As tagTLVNewOp, ByRef idgCmdData As tagIDGCmdData)
    '    Throw New Exception("[Warning] tagTLVNewOp.m_pfuncCbDummy (T=" + refobjTLVAttr.m_strTag + ",L=" & refobjTLVAttr.m_nLen & ") is running")
    'End Sub
    'Public Shared Sub m_pfuncCbComposer(ByRef refobjTLVAttr As tagTLVNewOp, ByRef idgCmdData As tagIDGCmdData)
    '    Throw New Exception("[Warning] tagTLVNewOp.m_pfuncCbDummy (T=" + refobjTLVAttr.m_strTag + ",L=" & refobjTLVAttr.m_nLen & ") is running")
    'End Sub
    ''----------------[ IDG Command Packet TLV  Composer & Parser :  ]----------------
    'Public Shared Sub m_pfuncCbParser(ByRef refobjTLVAttr As tagTLVNewOp, ByRef idgCmdData As tagIDGCmdData)
    '    Throw New Exception("[Warning] tagTLVNewOp.m_pfuncCbDummy (T=" + refobjTLVAttr.m_strTag + ",L=" & refobjTLVAttr.m_nLen & ") is running")
    'End Sub
    'Public Shared Sub m_pfuncCbComposer(ByRef refobjTLVAttr As tagTLVNewOp, ByRef idgCmdData As tagIDGCmdData)
    '    Throw New Exception("[Warning] tagTLVNewOp.m_pfuncCbDummy (T=" + refobjTLVAttr.m_strTag + ",L=" & refobjTLVAttr.m_nLen & ") is running")
    'End Sub
    ''----------------[ IDG Command Packet TLV  Composer & Parser :  ]----------------
    'Public Shared Sub m_pfuncCbParser(ByRef refobjTLVAttr As tagTLVNewOp, ByRef idgCmdData As tagIDGCmdData)
    '    Throw New Exception("[Warning] tagTLVNewOp.m_pfuncCbDummy (T=" + refobjTLVAttr.m_strTag + ",L=" & refobjTLVAttr.m_nLen & ") is running")
    'End Sub
    'Public Shared Sub m_pfuncCbComposer(ByRef refobjTLVAttr As tagTLVNewOp, ByRef idgCmdData As tagIDGCmdData)
    '    Throw New Exception("[Warning] tagTLVNewOp.m_pfuncCbDummy (T=" + refobjTLVAttr.m_strTag + ",L=" & refobjTLVAttr.m_nLen & ") is running")
    'End Sub
    '===========================================[ IDG Command Packet TLV Composer & Parser Section End ]=================================================




    '================================================================================================
    Sub New(ByRef refwinformMain As Form01_Main, ByRef refobjCTLB As ClassTableListMaster) ', ByRef refDictTlvNewOpCAPK As Dictionary(Of String, tagTLVNewOpCAPK))
        'Sub New(ByRef refwinformMain As MainForm, ByRef refobjCTLB As ClassTableListMaster, ByRef refDictTlvNewOpConfigurations As Dictionary(Of String, tagTLVNewOp), ByRef refDictTlvNewOpCAPK As Dictionary(Of String, tagTLVNewOpCAPK))
        MyBase.New(refwinformMain, refobjCTLB) ', refDictTlvNewOpCAPK)
        '
        m_arraybyteSendBuf = New Byte(ClassDevRS232.m_cnRX_BUF_MAX - 1) {}
        '
        m_devRS232.SetCallbackRecvParameterSync(AddressOf ClassReaderProtocolIDGGR.RecvParameterSync)

        'm_tagCRLInfo.Init()
        '
        m_nIDGCommanderType = ClassReaderInfo.ENU_FW_PLATFORM_TYPE.GR
    End Sub

    '
    Public Shared Sub RecvParameterSync(ByRef tagIDGCmdInfo As tagIDGCmdData)
        ClassReaderProtocolIDGGR.m_tagIDGCmdDataInfo = tagIDGCmdInfo
    End Sub
    Public Shared Sub RecvParameterSyncForTransactionCancelCase(ByRef tagIDGCmdInfo As tagIDGCmdData)
        ClassReaderProtocolIDGGR.m_tagIDGCmdDataInfoTransactionCancel = tagIDGCmdInfo
    End Sub
    '


    'IDG CMD:_0403
    'Note: bSendIt, default = TRUE.
    '          For Mass Group Data Operations, the invoker may need to compose all/multiple group info until send them out (to reader), similar to batch job case
    '          In the case of above, the invoker should set bSendIt = False to prevent the sending  step triggered while invoking.
    Private Shared Sub SetConfigurationsGroupSingleCbRecv(ByRef devSerialPort As SerialPort, ByRef refIDGCmdData As tagIDGCmdData, ByVal bRS232CommOK As Boolean)

        Try
            ' Common Process to receive data 
            PreProcessCbRecv(devSerialPort, refIDGCmdData, bRS232CommOK)
            '
        Catch ex As Exception
            LogLnThreadSafe("[Err] SetConfigurationsGroupSingleCbRecv() = " + ex.Message)
        End Try
    End Sub

    Public Overrides Sub SetCRLMass()

    End Sub

    Public Overrides Sub SetCAPublicKeyMass()

    End Sub

    '================[ CMD_F0F4 (Set), and F0F5 (Get) ]================
    Public Overrides Sub SystemUI_ButtonsConfigSet(arrayBtnConfig As Byte(), Optional ByVal nTimeout As Integer = ClassDevRS232.m_cnTX_TIMEOUT_IN_SEC)
        Dim u16Cmd As UInt16 = &HF0F4
        Dim strFuncName As String = "SystemUI_ButtonsConfigSet"


        Dim arrayByteSend As Byte() = Nothing
        Dim arrayByteRecv As Byte() = Nothing

        If (m_bIDGCmdBusy) Then
            Return
        End If
        m_bIDGCmdBusy = True

        Try
            ''-----------------------------------------------------------------[ Send to reader ]-----------------------------------------------------------------
            System_Customized_Command(u16Cmd, strFuncName, arrayBtnConfig, arrayByteRecv, AddressOf ClassReaderProtocolBasic.PreProcessCbRecv, nTimeout)

            'Its' response code = 0 => OK
            If (bIsRetStatusOK) Then
            Else
            End If

        Catch ext As TimeoutException
            LogLn("[Err][RS232 Timeout] [IDG: " + GetIDGCmdString(u16Cmd) + "] " + strFuncName + "() = " + ext.Message)
        Catch ex As Exception
            LogLn("[Err] [IDG:  " + GetIDGCmdString(u16Cmd) + "] " + strFuncName + "() = " + ex.Message)
        Finally
            m_tagIDGCmdDataInfo.Cleanup()
            If (arrayByteSend IsNot Nothing) Then
                Erase arrayByteSend
            End If
            If (arrayByteRecv IsNot Nothing) Then
                Erase arrayByteRecv
            End If
            m_bIDGCmdBusy = False
        End Try

    End Sub

    Public Overrides Sub SystemUI_ButtonsConfigGet(ByRef o_arrayBtnConfig As Byte(), Optional ByVal nTimeout As Integer = ClassDevRS232.m_cnTX_TIMEOUT_IN_SEC)
        Dim u16Cmd As UInt16 = &HF0F5
        Dim strFuncName As String = "SystemUI_ButtonsConfigGet"
        Dim arrayByteSend As Byte() = Nothing
        Dim arrayByteRecv As Byte() = Nothing

        If (m_bIDGCmdBusy) Then
            Return
        End If
        m_bIDGCmdBusy = True

        Try
            ''-----------------------------------------------------------------[ Send to reader ]-----------------------------------------------------------------
            System_Customized_Command(u16Cmd, strFuncName, arrayByteSend, arrayByteRecv, AddressOf ClassReaderProtocolBasic.PreProcessCbRecv, nTimeout)
            'Its' response code = 0 => OK
            If (bIsRetStatusOK) Then
                m_tagIDGCmdDataInfo.ParserDataGetArrayRawData(o_arrayBtnConfig)
            Else
            End If

        Catch ext As TimeoutException
            LogLn("[Err][RS232 Timeout] [IDG: " + GetIDGCmdString(u16Cmd) + "] " + strFuncName + "() = " + ext.Message)
        Catch ex As Exception
            LogLn("[Err] [IDG:  " + GetIDGCmdString(u16Cmd) + "] " + strFuncName + "() = " + ex.Message)
        Finally
            m_tagIDGCmdDataInfo.Cleanup()
            If (arrayByteSend IsNot Nothing) Then
                Erase arrayByteSend
            End If
            If (arrayByteRecv IsNot Nothing) Then
                Erase arrayByteRecv
            End If
            m_bIDGCmdBusy = False
        End Try

    End Sub


    '======================[CMD: F0-FB Buzzer Beep Once]======================
    Public Overrides Sub SystemUI_NPT_BuzzerOnce(Optional ByVal nTimeout As Integer = ClassDevRS232.m_cnTX_TIMEOUT_IN_SEC)
        Dim u16Cmd As UInt16 = &HF0FE
        Dim strFuncName As String = "SystemUI_NPT_BuzzerOnce"

        Dim arrayByteSend As Byte() = Nothing
        Dim arrayByteRecv As Byte() = Nothing

        If (m_bIDGCmdBusy) Then
            Return
        End If
        m_bIDGCmdBusy = True

        Try
            '-----------------------------------------------------------------[ Send to reader ]-----------------------------------------------------------------
            System_Customized_Command(u16Cmd, strFuncName, arrayByteSend, arrayByteRecv, AddressOf ClassReaderProtocolBasic.PreProcessCbRecv, nTimeout)

            If (bIsRetStatusOK) Then
            Else
            End If

        Catch ext As TimeoutException
            LogLn("[Err][RS232 Timeout] [IDG: " + GetIDGCmdString(u16Cmd) + "] " + strFuncName + "() = " + ext.Message)
        Catch ex As Exception
            LogLn("[Err] [IDG:  " + GetIDGCmdString(u16Cmd) + "] " + strFuncName + "() = " + ex.Message)
        Finally
            m_tagIDGCmdDataInfo.Cleanup()
            If (arrayByteSend IsNot Nothing) Then
                Erase arrayByteSend
            End If
            If (arrayByteRecv IsNot Nothing) Then
                Erase arrayByteRecv
            End If
            m_bIDGCmdBusy = False
        End Try

    End Sub



    'IDG CMD:_1801
    '
    'IDG CMD:_F0F9
    Private Shared Sub SystemLCDClearCbRecv(ByRef devSerialPort As SerialPort, ByRef refIDGCmdData As tagIDGCmdData, ByVal bRS232CommOK As Boolean)

        Try
            ' Common Process to receive data 
            PreProcessCbRecv(devSerialPort, refIDGCmdData, bRS232CommOK)
            '
        Catch ex As Exception
            LogLnThreadSafe("[Err] SystemLCDClearCbRecv() = " + ex.Message)
        End Try
    End Sub
    Public Overrides Sub SystemLCDClear(Optional ByVal nTimeout As Integer = ClassDevRS232.m_cnTX_TIMEOUT_IN_SEC)
        '
        Dim u16Cmd As UInt16 = &HF0F9
        Dim strFuncName As String = "SystemLCDClear"

        Dim arrayByteSend As Byte() = Nothing
        Dim arrayByteRecv As Byte() = Nothing

        If (m_bIDGCmdBusy) Then
            Return
        End If
        m_bIDGCmdBusy = True

        Try
            '-----------------------------------------------------------------[ Send to reader ]-----------------------------------------------------------------

            System_Customized_Command(u16Cmd, strFuncName, arrayByteSend, arrayByteRecv, AddressOf ClassReaderProtocolBasic.PreProcessCbRecv, nTimeout)

            If (bIsRetStatusOK) Then
            Else
            End If

        Catch ext As TimeoutException
            LogLn("[Err][RS232 Timeout] [IDG: " + GetIDGCmdString(u16Cmd) + "] " + strFuncName + "() = " + ext.Message)
        Catch ex As Exception
            LogLn("[Err] [IDG:  " + GetIDGCmdString(u16Cmd) + "] " + strFuncName + "() = " + ex.Message)
        Finally
            m_tagIDGCmdDataInfo.Cleanup()
            m_bIDGCmdBusy = False
            If (arrayByteSend IsNot Nothing) Then
                Erase arrayByteSend
            End If
            If (arrayByteRecv IsNot Nothing) Then
                Erase arrayByteRecv
            End If
        End Try

    End Sub

    '====[F0-FC -- Line 1 Display, F0-FD - Line 2]====
    Private Shared Sub SystemLCDDisplayTextCbRecv(ByRef devSerialPort As SerialPort, ByRef refIDGCmdData As tagIDGCmdData, ByVal bRS232CommOK As Boolean)

        Try
            ' Common Process to receive data 
            PreProcessCbRecv(devSerialPort, refIDGCmdData, bRS232CommOK)
            '
        Catch ex As Exception
            LogLnThreadSafe("[Err] SystemLCDDisplayTextCbRecv() = " + ex.Message)
        End Try
    End Sub
    '
    Public Overrides Sub SystemLCDDisplayText(ByVal strText As String, ByVal nLine As Integer, ByVal nTimeout As Integer)

        Dim arrayByteResponse As Byte() = Nothing
        'Dim tlv1 As tagTLV
        Dim u16Cmd As UInt16 = &HF0FC
        Dim strFuncName As String = "SystemLCDDisplayText"
        'Dim u32LCDDisplayTextControlID As UInt32

        Dim arrayByteSend As Byte() = Nothing
        Dim arrayByteRecv As Byte() = Nothing

        If (m_bIDGCmdBusy) Then
            Return
        End If
        m_bIDGCmdBusy = True
        'Check Is LCD Line 2 Text Message
        If (nLine = 2) Then
            u16Cmd = &HF0FD
        End If
        Try
            '-----------------------------------------------------------------[ Send to reader ]-----------------------------------------------------------------
            'Check Length
            If (strText.Length > 32) Then
                'Throw New Exception(strText + "length (=" + strText.Length + ") is > 32")
                strText = strText.Substring(0, 32)
            End If

            If (strText.Length > 16) Then
                If (nLine = 2) Then
                    'Throw New Exception(strText + "length (=" + strText.Length + ") is > 16 but now put it into line 2 is invalid")
                    strText = strText.Substring(0, 16)
                End If
            End If

            arrayByteSend = System.Text.Encoding.ASCII.GetBytes(strText)
            System_Customized_Command(u16Cmd, strFuncName, arrayByteSend, arrayByteRecv, AddressOf ClassReaderProtocolBasic.PreProcessCbRecv, nTimeout)

            'Its' response code = 0 => OK
            If (bIsRetStatusOK) Then
                '
            Else
            End If

        Catch ext As TimeoutException
            LogLn("[Err][RS232 Timeout] [IDG: " + GetIDGCmdString(u16Cmd) + "] " + strFuncName + "() = " + ext.Message)
        Catch ex As Exception
            LogLn("[Err] [IDG:  " + GetIDGCmdString(u16Cmd) + "] " + strFuncName + "() = " + ex.Message)
        Finally
            m_tagIDGCmdDataInfo.Cleanup()
            m_bIDGCmdBusy = False
            If (Not (arrayByteResponse Is Nothing)) Then
                Erase arrayByteResponse
            End If
            If (arrayByteSend IsNot Nothing) Then
                Erase arrayByteSend
            End If
            If (arrayByteRecv IsNot Nothing) Then
                Erase arrayByteRecv
            End If
        End Try
    End Sub
    '

    '
    '=============================================[ IDG CMD:_84-07, Get CRL From indexed record (0-base index), now we start from 0th one ]=============================================
    'Referenced Code: Java Labs Tool V 2-03-10, RevFile.java
    Private Shared Sub GetCRLMassCbRecv(ByRef devSerialPort As SerialPort, ByRef refIDGCmdData As tagIDGCmdData, ByVal bRS232CommOK As Boolean)

        Try
            ' Common Process to receive data 
            PreProcessCbRecv(devSerialPort, refIDGCmdData, bRS232CommOK)
            '
        Catch ex As Exception
            LogLnThreadSafe("[Err] GetCRLMassCbRecv() = " + ex.Message)
        End Try
    End Sub


    '-----------[ For AR Platform ONLY ]---------------
    Public Sub SetConfigurationsGRBackwardCapability(Optional ByVal nTimeout As Integer = ClassDevRS232.m_cnTX_TIMEOUT_IN_SEC)
        Dim dictTLV As New Dictionary(Of String, tagTLV) From {{"DF59", New tagTLV("DF59", 1, "00")}}
        '
        SetConfigurations(dictTLV, nTimeout)
        '
        ClassTableListMaster.CleanupConfigurationsTLVList(dictTLV)

    End Sub
    '
    Public Overrides Sub SetConfigurations(ByVal i_arrayByteTLVData As Byte(), Optional ByVal nTimeout As Integer = ClassDevRS232.m_cnTX_TIMEOUT_IN_SEC)
        Dim u16Cmd As UInt16 = &H400
        Dim strFuncName As String = "SetConfigurations"
        'Dim arrayByteTLVData As Byte()
        'Dim tlvOp As tagTLVNewOp
        '
        m_bIsRetStatusOK = False
        If (m_bIDGCmdBusy) Then
            Return
        End If
        m_bIDGCmdBusy = True
        Try
            m_tagIDGCmdDataInfo.Reinit(u16Cmd, m_arraybyteSendBuf, Me)

            'Compose Rest TLVs
            'ClassTableListMaster.TLVauleFromString2ByteArray(strTLVString, arrayByteTLVData)
            m_tagIDGCmdDataInfo.ComposerAddData(i_arrayByteTLVData)

            '-----------------------------------------------------------------[ Send to reader ]-----------------------------------------------------------------
            'Compose 1)Protocol Header 2) Command Set 3) Length 4) Command Frame CRC Data
            PktComposerCommandFrameHeaderCmdSetDataLengthCRCData(m_tagIDGCmdDataInfo)

            '----------------[ Debug Section ]----------------
            If (m_bDebug) Then
                'Note: After PktComposerCommandFrameHeaderCmdSetDataLengthCRCData(), m_tagIDGCmdDataInfo.m_nDataSizeSend includes m_nPktHeader + m_nPktTrail
                'Form01_Main.LogLnDumpData(m_tagIDGCmdDataInfo.m_refarraybyteData, tagIDGCmdData.m_nPktHeader + m_tagIDGCmdDataInfo.m_nDataSizeSend + tagIDGCmdData.m_nPktTrail)
                Form01_Main.LogLnDumpData(m_tagIDGCmdDataInfo.m_refarraybyteData, m_tagIDGCmdDataInfo.m_nDataSizeSend)
            End If

            '----------------[ End Debug Section ]----------------
            'Waiting For Response in SetConfigurationsGroupSingleCbRecv()
            m_refdevConn.SendCommandFrameAndWaitForResponseFrame(AddressOf ClassReaderProtocolIDGGR.SetConfigurationsCbRecv, m_tagIDGCmdDataInfo, m_nIDGCommanderType, True, nTimeout * 1000)

            'Its' response code = 0 => OK
            If (m_tagIDGCmdDataInfo.bIsResponseOK) Then
                m_bIsRetStatusOK = True
            Else
                m_bIsRetStatusOK = False
            End If
        Catch ext As TimeoutException
            LogLn("[Err][RS232 Timeout] [IDG: " + GetIDGCmdString(u16Cmd) + "] " + strFuncName + "() = " + ext.Message)
        Catch ex As Exception
            LogLn("[Err] [IDG:  " + GetIDGCmdString(u16Cmd) + "] " + strFuncName + "() = " + ex.Message)
        Finally
            m_bIDGCmdBusy = False
        End Try
        '
    End Sub
    '
    Public Overrides Sub SetConfigurations(ByRef dictTLV As Dictionary(Of String, tagTLV), Optional ByVal nTimeout As Integer = ClassDevRS232.m_cnTX_TIMEOUT_IN_SEC)
        Dim u16Cmd As UInt16 = &H400
        Dim strFuncName As String = "SetConfigurations"

        Dim iteratorTLV As KeyValuePair(Of String, tagTLV)
        'Dim tlvOp As tagTLVNewOp
        '
        m_bIsRetStatusOK = False
        If (m_bIDGCmdBusy) Then
            Return
        End If
        m_bIDGCmdBusy = True
        Try
            m_tagIDGCmdDataInfo.Reinit(u16Cmd, m_arraybyteSendBuf, Me)

            'Compose Rest TLVs
            ComposeRestTLVsConfigurations(dictTLV, m_tagIDGCmdDataInfo)

            '-----------------------------------------------------------------[ Send to reader ]-----------------------------------------------------------------
            'Compose 1)Protocol Header 2) Command Set 3) Length 4) Command Frame CRC Data
            PktComposerCommandFrameHeaderCmdSetDataLengthCRCData(m_tagIDGCmdDataInfo)

            '----------------[ Debug Section ]----------------
            If (m_bDebug) Then
                'Note: After PktComposerCommandFrameHeaderCmdSetDataLengthCRCData(), m_tagIDGCmdDataInfo.m_nDataSizeSend includes m_nPktHeader + m_nPktTrail
                'Form01_Main.LogLnDumpData(m_tagIDGCmdDataInfo.m_refarraybyteData, tagIDGCmdData.m_nPktHeader + m_tagIDGCmdDataInfo.m_nDataSizeSend + tagIDGCmdData.m_nPktTrail)
                Form01_Main.LogLnDumpData(m_tagIDGCmdDataInfo.m_refarraybyteData, m_tagIDGCmdDataInfo.m_nDataSizeSend)
            End If

            '----------------[ End Debug Section ]----------------
            'Waiting For Response in SetConfigurationsGroupSingleCbRecv()
            m_refdevConn.SendCommandFrameAndWaitForResponseFrame(AddressOf ClassReaderProtocolIDGGR.SetConfigurationsCbRecv, m_tagIDGCmdDataInfo, m_nIDGCommanderType, True, nTimeout * 1000)

            'Its' response code = 0 => OK
            If (m_tagIDGCmdDataInfo.bIsResponseOK) Then
                m_bIsRetStatusOK = True
            Else
                m_bIsRetStatusOK = False
            End If
        Catch ext As TimeoutException
            LogLn("[Err][RS232 Timeout] [IDG: " + GetIDGCmdString(u16Cmd) + "] " + strFuncName + "() = " + ext.Message + ",dictTLV Count = " & dictTLV.Count)
        Catch ex As Exception
            LogLn("[Err] [IDG:  " + GetIDGCmdString(u16Cmd) + "] " + strFuncName + "() = " + ex.Message)
        Finally
            If dictTLV IsNot Nothing Then
                For Each iteratorTLV In dictTLV
                    iteratorTLV.Value.Cleanup()
                Next
                'Clean up stuffs
                dictTLV.Clear()
            End If
            m_bIDGCmdBusy = False
        End Try
        '
    End Sub
    Private Shared Sub SetConfigurationsCbRecv(ByRef devSerialPort As SerialPort, ByRef o_IDGCmdData As tagIDGCmdData, ByVal bRS232CommOK As Boolean)
        Try
            ' Common Process to receive data 
            PreProcessCbRecv(devSerialPort, o_IDGCmdData, bRS232CommOK)
            '
        Catch ex As Exception
            LogLnThreadSafe("[Err] SetConfigurationsGroupSingleCbRecv() = " + ex.Message)
        End Try
    End Sub

    'IDG CMD:_D007
    Public Overrides Sub GetCAPublicKeyListRIDMass()

    End Sub

    Public Overrides Sub SetConfigurationsAIDMass()

    End Sub



    Public Overrides Sub GetCAPublicKeyIDSingle()

    End Sub

    '--------------------------------------[ IDG CMD:_030C ]--------------------------------------
    Private Shared Sub GetCashTransactionReaderRiskParametersCbRecv(ByRef devSerialPort As SerialPort, ByRef refConfigGroupData As tagIDGCmdData, ByVal bRS232CommOK As Boolean)
        Try
            ' Common Process to receive data 
            PreProcessCbRecv(devSerialPort, refConfigGroupData, bRS232CommOK)
            '
        Catch ex As Exception
            LogLnThreadSafe("[Err] GetCashTransactionReaderRiskParametersCbRecv() = " + ex.Message)
        End Try
    End Sub
    Public Overrides Sub GetCashTransactionReaderRiskParameters(ByRef o_dictTLV As SortedDictionary(Of String, tagTLV), Optional ByVal nTimeout As Integer = ClassDevRS232.m_cnTX_TIMEOUT_IN_SEC)
        'Compose The IDG Packet 03-0C
        'Send to Reader
        'Get from Reader
        'Check result
        Dim iteratorTLV As KeyValuePair(Of String, tagTLV)
        Dim dictTLV As Dictionary(Of String, tagTLV) = Nothing
        'Dim tlvOp As tagTLVNewOp
        'Dim tlvTmp As tagTLV
        'Dim nIdx, nIdxEnd As Integer
        'Dim arrayByteData() As Byte
        Dim u16Cmd As UInt16 = &H30C
        Dim strFuncName As String = "GetCashTransactionReaderRiskParameters"


        '
        m_bIsRetStatusOK = False
        If (m_bIDGCmdBusy) Then
            Return
        End If
        m_bIDGCmdBusy = True
        Try
            m_tagIDGCmdDataInfo.Reinit(u16Cmd, m_arraybyteSendBuf, Me)
            '-----------------------------------------------------------------[ Send to reader ]-----------------------------------------------------------------
            'arrayByteData = m_tagIDGCmdDataInfo.m_refarraybyteData
            ''Composer The Packet with Group ID Info
            'tlvOp = ClassReaderCommanderParser .m_dictTLVOp_RefList_Major.Item(tagTLV.TAG_FFE4)
            ''Make a TLV with Group ID = 01
            'tlvTmp = New tagTLV(tagTLV.TAG_FFE4, 1, "01")
            'tlvOp.PktComposer(tlvTmp, m_tagIDGCmdDataInfo)

            'Compose 1)Protocol Header 2) Command Set 3) Length 4) Command Frame CRC Data
            PktComposerCommandFrameHeaderCmdSetDataLengthCRCData(m_tagIDGCmdDataInfo)
            'tlvTmp.Cleanup()

            '----------------[ Debug Section ]----------------
            If (m_bDebug) Then
                'Note: After PktComposerCommandFrameHeaderCmdSetDataLengthCRCData(), m_tagIDGCmdDataInfo.m_nDataSizeSend includes m_nPktHeader + m_nPktTrail
                'Form01_Main.LogLnDumpData(m_tagIDGCmdDataInfo.m_refarraybyteData, tagIDGCmdData.m_nPktHeader + m_tagIDGCmdDataInfo.m_nDataSizeSend + tagIDGCmdData.m_nPktTrail)
                Form01_Main.LogLnDumpData(m_tagIDGCmdDataInfo.m_refarraybyteData, m_tagIDGCmdDataInfo.m_nDataSizeSend)
            End If
            '----------------[ End Debug Section ]----------------

            'Waiting For Response in GetConfigurationsGroupSingleCbRecv()
            m_refdevConn.SendCommandFrameAndWaitForResponseFrame(AddressOf ClassReaderProtocolBasic.PreProcessCbRecv, m_tagIDGCmdDataInfo, m_nIDGCommanderType, True, nTimeout * 1000)

            'Its' response code = 0 => OK
            If (m_tagIDGCmdDataInfo.bIsResponseOK) Then
                m_bIsRetStatusOK = True

                With m_tagIDGCmdDataInfo
                    'Parser the Group Info
                    ClassReaderCommanderParser.TLV_ParseFromReaderMultipleTLVs(.m_refarraybyteData, 0, .m_refarraybyteData.Count, o_dictTLV)

                End With
            Else
                m_bIsRetStatusOK = False
            End If
        Catch ext As TimeoutException
            LogLn("[Err][RS232 Timeout] [IDG: " + GetIDGCmdString(u16Cmd) + "] " + strFuncName + "() = " + ext.Message)
        Catch ex As Exception
            LogLn("[Err] [IDG:  " + GetIDGCmdString(u16Cmd) + "] " + strFuncName + "() = " + ex.Message)
        Finally
            If dictTLV IsNot Nothing Then
                For Each iteratorTLV In dictTLV
                    iteratorTLV.Value.Cleanup()
                Next
                'Clean up stuffs
                dictTLV.Clear()
            End If
            m_bIDGCmdBusy = False
        End Try
        '
    End Sub
    '--------------------------------------[ IDG CMD:_030D ]--------------------------------------
    Public Overrides Sub GetCashbackTransactionReaderRiskParameters(ByRef o_dictTLV As SortedDictionary(Of String, tagTLV), Optional ByVal nTimeout As Integer = ClassDevRS232.m_cnTX_TIMEOUT_IN_SEC)
        Try

        Catch ex As Exception
            LogLn("[Err] GetCashbackTransactionReaderRiskParameters () = " + ex.Message)
        Finally

        End Try
    End Sub

    'IDG CMD:_0304
    Public Overrides Sub GetConfigurationsAIDSingle()

    End Sub



    '--------------------------------------[ IDG CMD:_0306 ]--------------------------------------
    Private Shared Sub GetConfigurationsGroupSingleCbRecv(ByRef devSerialPort As SerialPort, ByRef refConfigGroupData As tagIDGCmdData, ByVal bRS232CommOK As Boolean)
        Try
            ' Common Process to receive data 
            PreProcessCbRecv(devSerialPort, refConfigGroupData, bRS232CommOK)
            '
        Catch ex As Exception
            LogLnThreadSafe("[Err] SetConfigurationsGroupSingleCbRecv() = " + ex.Message)
        End Try
    End Sub
    'IDG CMD:_0306
    'IN = configGroup = Nothing
    'OUT = Wanted Group ID
    Public Overrides Sub GetConfigurationsGroupSingle(ByVal nGroupIdx As Integer, ByRef configGroup As tagConfigCmdUnitGroup, Optional ByVal nTimeout As Integer = ClassDevRS232.m_cnTX_TIMEOUT_IN_SEC)
        Dim iteratorTLV As KeyValuePair(Of String, tagTLV)
        Dim dictTLV As Dictionary(Of String, tagTLV) = Nothing
        Dim tlvOp As tagTLVNewOp
        Dim tlvTmp As tagTLV
        'Dim nIdx As Integer
        Dim arrayByteData() As Byte
        Dim u16Cmd As UInt16 = &H306
        Dim strFuncName As String = "GetConfigurationsGroupSingle"

        '
        m_bIsRetStatusOK = False
        If (m_bIDGCmdBusy) Then
            Return
        End If
        m_bIDGCmdBusy = True
        Try
            m_tagIDGCmdDataInfo.Reinit(u16Cmd, m_arraybyteSendBuf, Me)
            '-----------------------------------------------------------------[ Send to reader ]-----------------------------------------------------------------
            arrayByteData = m_tagIDGCmdDataInfo.m_refarraybyteData
            'Composer The Packet with Group ID Info
            tlvOp = ClassReaderCommanderParser.m_dictTLVOp_RefList_Major.Item(tagTLV.TAG_FFE4_or_DFEE2D_GrpID)
            'Make a TLV with Group ID = 01
            'tlvTmp = New tagTLV(tagTLV.TAG_FFE4, 1, "01")
            tlvTmp = New tagTLV(tagTLV.TAG_FFE4_or_DFEE2D_GrpID, nGroupIdx, nGroupIdx.ToString())
            tlvOp.PktComposer(tlvTmp, m_tagIDGCmdDataInfo)

            'Compose 1)Protocol Header 2) Command Set 3) Length 4) Command Frame CRC Data
            PktComposerCommandFrameHeaderCmdSetDataLengthCRCData(m_tagIDGCmdDataInfo)
            tlvTmp.Cleanup()

            '----------------[ Debug Section ]----------------
            If (m_bDebug) Then
                'Note: After PktComposerCommandFrameHeaderCmdSetDataLengthCRCData(), m_tagIDGCmdDataInfo.m_nDataSizeSend includes m_nPktHeader + m_nPktTrail
                'Form01_Main.LogLnDumpData(m_tagIDGCmdDataInfo.m_refarraybyteData, tagIDGCmdData.m_nPktHeader + m_tagIDGCmdDataInfo.m_nDataSizeSend + tagIDGCmdData.m_nPktTrail)
                Form01_Main.LogLnDumpData(m_tagIDGCmdDataInfo.m_refarraybyteData, m_tagIDGCmdDataInfo.m_nDataSizeSend)
            End If
            '----------------[ End Debug Section ]----------------

            'Waiting For Response in GetConfigurationsGroupSingleCbRecv()
            m_refdevConn.SendCommandFrameAndWaitForResponseFrame(AddressOf ClassReaderProtocolBasic.PreProcessCbRecv, m_tagIDGCmdDataInfo, m_nIDGCommanderType, True, nTimeout * 1000)

            'Its' response code = 0 => OK
            If (m_tagIDGCmdDataInfo.bIsResponseOK) Then
                'Parser the Group Info
                ClassReaderCommanderParser.Config_ParseFromReaderGroupInfo(m_tagIDGCmdDataInfo, configGroup)

                m_bIsRetStatusOK = True
            Else
                m_bIsRetStatusOK = False
            End If
        Catch ext As TimeoutException
            LogLn("[Err][RS232 Timeout] [IDG: " + GetIDGCmdString(u16Cmd) + "] " + strFuncName + "() = " + ext.Message + ",Group ID= " & configGroup.m_nGroupID)
        Catch ex As Exception
            LogLn("[Err][IDG: " + GetIDGCmdString(u16Cmd) + "] " + strFuncName + "() = " + ex.Message)
        Finally
            If dictTLV IsNot Nothing Then
                For Each iteratorTLV In dictTLV
                    iteratorTLV.Value.Cleanup()
                Next
                'Clean up stuffs
                dictTLV.Clear()
            End If
            m_bIDGCmdBusy = False
        End Try
        '
    End Sub

    'IDG CMD:_84??
    Public Overrides Sub GetCRLSingle(ByRef tagCRLSingle As tagCRLCmdUnitSingle, Optional ByVal nTimeout As Integer = ClassDevRS232.m_cnTX_TIMEOUT_IN_SEC)
        'Dim CRLGroupInfo As tagCRLCmdUnit
    End Sub

    'IDG CMD:_D003

    Private Shared Sub SetCAPublicKeySinglebRecv(ByRef devSerialPort As SerialPort, ByRef o_IDGCmdData As tagIDGCmdData, ByVal bRS232CommOK As Boolean)
        Try
            ' Common Process to receive data 
            PreProcessCbRecv(devSerialPort, o_IDGCmdData, bRS232CommOK)
            '
        Catch ex As Exception
            LogLnThreadSafe("[Err] SetCAPublicKeySinglebRecv() = " + ex.Message)
        End Try
    End Sub

    '---------------------------------------------[ IDG CMD:_04-02 ]---------------------------------------------
    Public Overrides Sub SetConfigurationsAIDSingle(ByRef tagAIDInfo As tagConfigCmdUnitAID, Optional ByVal nTimeout As Integer = ClassDevRS232.m_cnTX_TIMEOUT_IN_SEC)
        'Compose The IDG Packet 04-02
        'Send to Reader
        'Get from Reader
        'Check result
        Dim iteratorTLV As KeyValuePair(Of String, tagTLV)
        Dim dictTLV As Dictionary(Of String, tagTLV) = Nothing
        Dim tlvOp As tagTLVNewOp
        'Dim tlvTmp As tagTLV
        Dim u16Cmd As UInt16 = &H402
        Dim strFuncName As String = "SetConfigurationsAIDSingle"

        '
        m_bIsRetStatusOK = False
        If (m_bIDGCmdBusy) Then
            Return
        End If
        m_bIDGCmdBusy = True
        Try
            'Create Set Single AID Information
            m_tagIDGCmdDataInfo.Reinit(u16Cmd, m_arraybyteSendBuf, Me)

            'm_tagIDGCmdDataInfo.m_nGroupID = tagAIDInfo.m_nGroupID
            'm_tagIDGCmdDataInfo.m_nTransactionType = tagAIDInfo.m_nTransactionType
            'm_tagIDGCmdDataInfo.m_refarraybyteData = m_arraybyteSendBuf

            'Clone the dictionary for operation
            dictTLV = ClassReaderProtocolBasic.CloneDict(tagAIDInfo.m_dictTLV) ' tagAIDInfo.m_dictTLV

            '=====================[ Following is Digested From "IDG GR 1.2.9 Rev 1.pdf, Cmd: 04-02 "]=====================
            '            For System AIDs:
            '• Must always include the TLV Group Number TLV as the FIRST TLV in the message.
            '• Must always include the AID TLV as the SECOND TLV in the message.
            '• Must never include the Application Flow TLV in the message
            '• Must never include the RID TLV in the message
            '• The FOUR remaining TLVs are all optional
            '            There are 12 System AIDs in the reader. These can be disabled but cannot be deleted.
            'For User AIDs:
            '• Must always include the TLV Group Number TLV as the FIRST TLV in the message.
            '• Must always include the AID TLV as the SECOND TLV in the message.
            '• Must always include the Application Flow TLV in the message
            '• The FIVE remaining TLVs are all optional.
            '• The DISABLE AID tag is ignored if included in a USER AID.
            'There are eight User AIDs in the system. These can be added (set) or deleted at the user’s discretion.
            '• No User AID can have the same exact AID as a System AID.
            'In addition to the above requirements:
            '• All AIDs must reference a TLV Group (in the TLV Group Number TLV) that already exists
            '• Any AID with a Partial Select TLV must also include the Max AID Length TLV
            '------------------[ Put Group ID("FFE4") At 1st TLV for Mandatory of IDG GR Cmd 04-02Spec ]------------------
            If Not dictTLV.ContainsKey(tagTLV.TAG_FFE4_or_DFEE2D_GrpID) Then
                Throw New Exception("Group ID (T=" + tagTLV.TAG_FFE4_or_DFEE2D_GrpID + ", V = " & tagAIDInfo.m_strKeyTagTerminalIDGroupID & ") Is Unavailable !!")
            End If
            '
            'Composer The Packet with Group ID Info
            tlvOp = ClassReaderCommanderParser.m_dictTLVOp_RefList_Major.Item(tagTLV.TAG_FFE4_or_DFEE2D_GrpID)
            tlvOp.PktComposer(dictTLV.Item(tagTLV.TAG_FFE4_or_DFEE2D_GrpID), m_tagIDGCmdDataInfo)
            dictTLV.Remove(tagTLV.TAG_FFE4_or_DFEE2D_GrpID)
            '------------------[ Put AID("9F06") As 2nd TLV for Mandatory of IDG GR Cmd 04-02Spec ]------------------
            If Not dictTLV.ContainsKey(tagTLV.TAG_9F06_TermID_AID) Then
                Throw New Exception("AID (T=" + tagTLV.TAG_9F06_TermID_AID + ", V = " & tagAIDInfo.m_strKeyTagTerminalIDGroupID & ") Is Unavailable !!")
            End If
            '
            'Composer The Packet with Group ID Info
            tlvOp = ClassReaderCommanderParser.m_dictTLVOp_RefList_Major.Item(tagTLV.TAG_9F06_TermID_AID)
            tlvOp.PktComposer(dictTLV.Item(tagTLV.TAG_9F06_TermID_AID), m_tagIDGCmdDataInfo)
            dictTLV.Remove(tagTLV.TAG_9F06_TermID_AID)

            'Compose Rest TLVs
            ComposeRestTLVsConfigurations(dictTLV, m_tagIDGCmdDataInfo)

            '-----------------------------------------------------------------[ Send to reader ]-----------------------------------------------------------------
            'Compose 1)Protocol Header 2) Command Set 3) Length 4) Command Frame CRC Data
            PktComposerCommandFrameHeaderCmdSetDataLengthCRCData(m_tagIDGCmdDataInfo)

            '----------------[ Debug Section ]----------------
            If (m_bDebug) Then
                'Note: After PktComposerCommandFrameHeaderCmdSetDataLengthCRCData(), m_tagIDGCmdDataInfo.m_nDataSizeSend includes m_nPktHeader + m_nPktTrail
                'Form01_Main.LogLnDumpData(m_tagIDGCmdDataInfo.m_refarraybyteData, tagIDGCmdData.m_nPktHeader + m_tagIDGCmdDataInfo.m_nDataSizeSend + tagIDGCmdData.m_nPktTrail)
                Form01_Main.LogLnDumpData(m_tagIDGCmdDataInfo.m_refarraybyteData, m_tagIDGCmdDataInfo.m_nDataSizeSend)
            End If
            '----------------[ End Debug Section ]----------------

            'Waiting For Response in SetConfigurationsGroupSingleCbRecv()
            m_refdevConn.SendCommandFrameAndWaitForResponseFrame(AddressOf ClassReaderProtocolBasic.PreProcessCbRecv, m_tagIDGCmdDataInfo, m_nIDGCommanderType, True, nTimeout * 1000)

            'Its' response code = 0 => OK
            If (m_tagIDGCmdDataInfo.bIsResponseOK) Then
                m_bIsRetStatusOK = True
            Else
                m_bIsRetStatusOK = False
                Throw New Exception("Response Code (" & m_tagIDGCmdDataInfo.m_nResponse & ") = " + ClassReaderProtocolIDGGR.m_dictStatusCode2.Item(CType(m_tagIDGCmdDataInfo.m_nResponse, Byte)))
            End If
        Catch ext As TimeoutException
            LogLn("[Err][RS232 Timeout] [IDG: 04-02] SetConfigurationsAIDSingle() = " + ext.Message + ",Terminal ID + Group ID= " + tagAIDInfo.m_strKeyTagTerminalIDGroupID)
        Catch ex As Exception
            LogLn("[Err] [IDG: 04-02] SetConfigurationsAIDSingle() = " + ex.Message)
        Finally
            If dictTLV IsNot Nothing Then
                For Each iteratorTLV In dictTLV
                    iteratorTLV.Value.Cleanup()
                Next
                'Clean up stuffs
                dictTLV.Clear()
            End If
            m_bIDGCmdBusy = False
        End Try
        '
    End Sub
    Private Shared Sub SetConfigurationsAIDSingleCbRecv(ByRef devSerialPort As SerialPort, ByRef o_IDGCmdData As tagIDGCmdData, ByVal bRS232CommOK As Boolean)
        Try
            ' Common Process to receive data 
            PreProcessCbRecv(devSerialPort, o_IDGCmdData, bRS232CommOK)
            '
        Catch ex As Exception
            LogLnThreadSafe("[Err] SetConfigurationsGroupSingleCbRecv() = " + ex.Message)
        End Try
    End Sub

    '---------------------------------------------[ IDG CMD:_04-0C ]---------------------------------------------
    Public Sub SetCashTransactionReaderRiskParametersCommon(ByVal u16Cmd As UInt16, ByVal dictTLV As SortedDictionary(Of String, tagTLV), ByVal strExcptMsg As String, Optional ByVal nTimeout As Integer = ClassDevRS232.m_cnTX_TIMEOUT_IN_SEC)
        'Compose The IDG Packet 04-0C / 
        Dim iteratorTLV As KeyValuePair(Of String, tagTLV)
        'Dim tlvOp As tagTLVNewOp
        'Dim tlvTmp As tagTLV

        '
        m_bIsRetStatusOK = False
        If (m_bIDGCmdBusy) Then
            Return
        End If
        m_bIDGCmdBusy = True
        Try
            'Create Set Cash/ Cashback Information
            m_tagIDGCmdDataInfo.Reinit(u16Cmd, m_arraybyteSendBuf, Me)

            'Compose Rest TLVs
            ComposeRestTLVsConfigurations(dictTLV, m_tagIDGCmdDataInfo)

            '-----------------------------------------------------------------[ Send to reader ]-----------------------------------------------------------------
            'Compose 1)Protocol Header 2) Command Set 3) Length 4) Command Frame CRC Data
            PktComposerCommandFrameHeaderCmdSetDataLengthCRCData(m_tagIDGCmdDataInfo)

            '----------------[ Debug Section ]----------------
            If (m_bDebug) Then
                'Note: After PktComposerCommandFrameHeaderCmdSetDataLengthCRCData(), m_tagIDGCmdDataInfo.m_nDataSizeSend includes m_nPktHeader + m_nPktTrail
                'Form01_Main.LogLnDumpData(m_tagIDGCmdDataInfo.m_refarraybyteData, tagIDGCmdData.m_nPktHeader + m_tagIDGCmdDataInfo.m_nDataSizeSend + tagIDGCmdData.m_nPktTrail)
                Form01_Main.LogLnDumpData(m_tagIDGCmdDataInfo.m_refarraybyteData, m_tagIDGCmdDataInfo.m_nDataSizeSend)
            End If
            '----------------[ End Debug Section ]----------------

            'Waiting For Response in SetConfigurationsGroupSingleCbRecv()
            m_refdevConn.SendCommandFrameAndWaitForResponseFrame(AddressOf ClassReaderProtocolBasic.PreProcessCbRecv, m_tagIDGCmdDataInfo, m_nIDGCommanderType, True, nTimeout * 1000)

            'Its' response code = 0 => OK
            If (m_tagIDGCmdDataInfo.bIsResponseOK) Then
                m_bIsRetStatusOK = True
            Else
                m_bIsRetStatusOK = False
                Throw New Exception("Response Code (" & m_tagIDGCmdDataInfo.m_nResponse & ") = " + ClassReaderProtocolIDGGR.m_dictStatusCode2.Item(m_tagIDGCmdDataInfo.m_nResponse))
            End If
        Catch ext As TimeoutException
            LogLn("[Err][RS232 Timeout] [IDG: " + String.Format("{0,4:X4}", u16Cmd) + "] " + strExcptMsg + " () = " + ext.Message)
        Catch ex As Exception
            LogLn("[Err] [IDG:" + String.Format("{0,4:X4}", u16Cmd) + "] " + strExcptMsg + " () = " + ex.Message)
        Finally
            If dictTLV IsNot Nothing Then
                For Each iteratorTLV In dictTLV
                    iteratorTLV.Value.Cleanup()
                Next
                'Clean up stuffs
                dictTLV.Clear()
            End If
            m_bIDGCmdBusy = False
        End Try
        '
    End Sub
    Private Shared Sub SetCashTransactionReaderRiskParametersCbRecv(ByRef devSerialPort As SerialPort, ByRef o_IDGCmdData As tagIDGCmdData, ByVal bRS232CommOK As Boolean)
        Try
            ' Common Process to receive data 
            PreProcessCbRecv(devSerialPort, o_IDGCmdData, bRS232CommOK)
            '
        Catch ex As Exception
            LogLnThreadSafe("[Err] SetCashTransactionReaderRiskParametersCbRecv() = " + ex.Message)
        End Try
    End Sub

    Public Overrides Sub SetCashTransactionReaderRiskParameters(ByVal dictTLV As SortedDictionary(Of String, tagTLV), Optional ByVal nTimeout As Integer = ClassDevRS232.m_cnTX_TIMEOUT_IN_SEC)
        SetCashTransactionReaderRiskParametersCommon(&H40C, dictTLV, "SetCashTransactionReaderRiskParameters", nTimeout)
    End Sub
    Public Overrides Sub SetCashbackTransactionReaderRiskParameters(ByVal dictTLV As SortedDictionary(Of String, tagTLV), Optional ByVal nTimeout As Integer = ClassDevRS232.m_cnTX_TIMEOUT_IN_SEC)
        SetCashTransactionReaderRiskParametersCommon(&H40D, dictTLV, "SetCashbackTransactionReaderRiskParameters", nTimeout)
    End Sub
    '=========================================[ IDG CMD:_84-04 ]=========================================
    Public Sub SetCRLSingleList(ByRef tagCRL As tagCRLCmdUnit, Optional ByVal nTimeout As Integer = ClassDevRS232.m_cnTX_TIMEOUT_IN_SEC)
        Dim iteratorCRLSingle As KeyValuePair(Of String, tagCRLCmdUnitSingle)
        Dim tagCRLSingleInfo As tagCRLCmdUnitSingle

        'm_bIsRetStatusOK = False
        'If (m_bIDGCmdBusy) Then
        '    Return
        'End If
        'm_bIDGCmdBusy = True
        Try
            '=======================[ Composing Send Out Data for 84-04, Referenced IDG Code from Java Labs Tool V2.03.10 ]=======================
            'Send out packet frame data part = RID(5 Bytes)+ Index(1 Byte)+ Serial(3 Bytes)
            For Each iteratorCRLSingle In tagCRL.m_dictCRLCmdList
                tagCRLSingleInfo = iteratorCRLSingle.Value
                SetCRLSingle(tagCRLSingleInfo)
            Next

        Catch ex As Exception
            LogLn("[Err] SetCRLSingleList() = " + ex.Message)
        Finally

        End Try
        '
        'm_bIDGCmdBusy = False
    End Sub
    '---------------------------------------------------------------------------------------------------------------
    '=========================================[ IDG CMD:_84-04 ]=========================================
    Private Shared Sub SetCRLSingleCbRecv(ByRef devSerialPort As SerialPort, ByRef o_IDGCmdData As tagIDGCmdData, ByVal bRS232CommOK As Boolean)
        Try
            ' Common Process to receive data 
            PreProcessCbRecv(devSerialPort, o_IDGCmdData, bRS232CommOK)
            '
        Catch ex As Exception
            LogLnThreadSafe("[Err] SetCRLSingleCbRecv() = " + ex.Message)
        End Try
    End Sub
    Public Overrides Sub SetCRLSingle(ByRef tagCRLSingle As tagCRLCmdUnitSingle, Optional ByVal nTimeout As Integer = ClassDevRS232.m_cnTX_TIMEOUT_IN_SEC)
        Dim arrayByteTmp As Byte() = Nothing
        '
        m_bIsRetStatusOK = False
        If (m_bIDGCmdBusy) Then
            Return
        End If
        m_bIDGCmdBusy = True
        Try
            '=======================[ Composing Send Out Data for 84-04, Referenced IDG Code from Java Labs Tool V2.03.10 ]=======================
            'Send out packet frame data part = RID(5 Bytes)+ Index(1 Byte)+ Serial(3 Bytes)
            With m_tagIDGCmdDataInfo
                'Create Set CRL Command - Single Information
                .Reinit(&H8404, m_arraybyteSendBuf, Me)
                'Set Initial Start Data Position
                '.m_nDataSizeSend = tagIDGCmdData.m_nPktHeader
                'RID, 5 bytes
                arrayByteTmp = tagCRLSingle.byteRID
                'Array.Copy(arrayByteTmp, 0, .m_refarraybyteData, .m_nDataSizeSend, arrayByteTmp.Count)
                .ComposerAddData(arrayByteTmp)

                '.m_nDataSizeSend = .m_nDataSizeSend + arrayByteTmp.Count
                Erase arrayByteTmp
                arrayByteTmp = Nothing

                'Index, 1 byte
                '.m_refarraybyteData(.m_nDataSizeSend) = tagCRLSingle.byteIndex
                '.m_nDataSizeSend = .m_nDataSizeSend + 1
                .ComposerAddData(tagCRLSingle.byteIndex)

                'Serial No, 3 bytes
                arrayByteTmp = tagCRLSingle.byteSerialNo
                'Array.Copy(arrayByteTmp, 0, .m_refarraybyteData, .m_nDataSizeSend, arrayByteTmp.Count)
                '.m_nDataSizeSend = .m_nDataSizeSend + arrayByteTmp.Count
                .ComposerAddData(arrayByteTmp)
                Erase arrayByteTmp
                arrayByteTmp = Nothing

                'Correct Packet Data Length
                '.m_nDataSizeSend = .m_nDataSizeSend - tagIDGCmdData.m_nPktHeader
            End With

            '-----------------------------------------------------------------[ Send to reader ]-----------------------------------------------------------------
            'Compose 1)Protocol Header 2) Command Set 3) Length 4) Command Frame CRC Data
            PktComposerCommandFrameHeaderCmdSetDataLengthCRCData(m_tagIDGCmdDataInfo)

            '----------------[ Debug Section ]----------------
            If (m_bDebug) Then
                Form01_Main.LogLnDumpData(m_tagIDGCmdDataInfo.m_refarraybyteData, m_tagIDGCmdDataInfo.m_nDataSizeSend)
            End If
            '----------------[ End Debug Section ]----------------

            'Waiting For Response in SetConfigurationsGroupSingleCbRecv()
            m_refdevConn.SendCommandFrameAndWaitForResponseFrame(AddressOf ClassReaderProtocolBasic.PreProcessCbRecv, m_tagIDGCmdDataInfo, m_nIDGCommanderType, True, nTimeout * 1000)


            If (m_tagIDGCmdDataInfo.bIsResponseOK) Then
                m_bIsRetStatusOK = True
            Else
                LogLn("[Err][IDG:84-04] SetCRLSingle() = (RID, Index, Serial) = (" + tagCRLSingle.strRID + ", " + tagCRLSingle.strIndex + ", " + tagCRLSingle.strSerialNo + "), Response =" + m_tagIDGCmdDataInfo.strGetResponseMsg + " ( " & m_tagIDGCmdDataInfo.byteGetResponseCode & " )")
            End If

        Catch ext As TimeoutException
            LogLn("[Err][RS232 Timeout] [IDG: D0-03] SetCRLSingle() = " + ext.Message + ", (RID, Index, Serial) = (" + tagCRLSingle.strRID + ", " + tagCRLSingle.strIndex + ", " + tagCRLSingle.strSerialNo + ")")
        Catch ex As Exception
            LogLn("[Err] [IDG: D0-03] SetCRLSingle() = " + ex.Message)
        Finally
            If arrayByteTmp IsNot Nothing Then
                Erase arrayByteTmp
                arrayByteTmp = Nothing
            End If
            m_bIDGCmdBusy = False
        End Try
        '
    End Sub

    'IDG CMD:_0404
    Public Overrides Sub DelConfigurationsAIDSingle(ByRef tlv As tagTLV, Optional ByVal nTimeout As Integer = ClassDevRS232.m_cnTX_TIMEOUT_IN_SEC)
        'Compose The IDG Packet 04-04
        'Send to Reader
        'Get from Reader
        'Check result
        Dim u16Cmd As UInt16 = &H404
        Dim strFuncName As String = "DelConfigurationsAIDSingle"

        '
        m_bIsRetStatusOK = False
        If (m_bIDGCmdBusy) Then
            Return
        End If
        m_bIDGCmdBusy = True
        Try
            m_tagIDGCmdDataInfo.Reinit(u16Cmd, m_arraybyteSendBuf, Me)

            'Compose Rest TLVs
            ComposeRestTLVConfigurations(tlv, m_tagIDGCmdDataInfo)

            '-----------------------------------------------------------------[ Send to reader ]-----------------------------------------------------------------
            'Compose 1)Protocol Header 2) Command Set 3) Length 4) Command Frame CRC Data
            PktComposerCommandFrameHeaderCmdSetDataLengthCRCData(m_tagIDGCmdDataInfo)

            '----------------[ Debug Section ]----------------
            If (m_bDebug) Then
                'Note: After PktComposerCommandFrameHeaderCmdSetDataLengthCRCData(), m_tagIDGCmdDataInfo.m_nDataSizeSend includes m_nPktHeader + m_nPktTrail
                'Form01_Main.LogLnDumpData(m_tagIDGCmdDataInfo.m_refarraybyteData, tagIDGCmdData.m_nPktHeader + m_tagIDGCmdDataInfo.m_nDataSizeSend + tagIDGCmdData.m_nPktTrail)
                Form01_Main.LogLnDumpData(m_tagIDGCmdDataInfo.m_refarraybyteData, m_tagIDGCmdDataInfo.m_nDataSizeSend)
            End If

            '----------------[ End Debug Section ]----------------
            'Waiting For Response in SetConfigurationsGroupSingleCbRecv()
            m_refdevConn.SendCommandFrameAndWaitForResponseFrame(AddressOf ClassReaderProtocolBasic.PreProcessCbRecv, m_tagIDGCmdDataInfo, m_nIDGCommanderType, True, nTimeout * 1000)

            'Its' response code = 0 => OK
            If (m_tagIDGCmdDataInfo.bIsResponseOK) Then
                m_bIsRetStatusOK = True
            Else
                m_bIsRetStatusOK = False
            End If
        Catch ext As TimeoutException
            LogLn("[Err] [RS232 Timeout][IDG: " + GetIDGCmdString(u16Cmd) + "] " + strFuncName + "() = " + ext.Message + ",Tag= " & tlv.m_strTag)
        Catch ex As Exception
            LogLn("[Err][IDG: " + GetIDGCmdString(u16Cmd) + "] " + strFuncName + "() = " + ex.Message)
        Finally
            m_bIDGCmdBusy = False

        End Try
    End Sub

    Public Overrides Sub DelConfigurationsAIDMass(ByRef dictTLV As Dictionary(Of String, tagTLV), Optional ByVal nTimeout As Integer = ClassDevRS232.m_cnTX_TIMEOUT_IN_SEC)

    End Sub
    Private Shared Sub DelConfigurationsAIDMassCbRecv(ByRef devSerialPort As SerialPort, ByRef o_IDGCmdData As tagIDGCmdData, ByVal bRS232CommOK As Boolean)
        Try
            ' Common Process to receive data 
            PreProcessCbRecv(devSerialPort, o_IDGCmdData, bRS232CommOK)
            '
        Catch ex As Exception
            LogLnThreadSafe("[Err] DelConfigurationsAIDMassCbRecv() = " + ex.Message)
        End Try
    End Sub

    Public Overrides Sub DelConfigurationsGroupMass()

    End Sub

    'IDG CMD:_0405
    Public Overrides Sub DelConfigurationsGroupSingle()

    End Sub

    ''IDG CMD:_8405 --> Delete RID+Index Entries
    'Public Overrides Sub DelCRLRIDIndex(ByVal strRID As String, ByVal strIndex As String)

    'End Sub


    'IDG CMD:_840D --> Delete RID+Index+Serial Data
    Private Shared Sub DelCRLSingleCbRecv(ByRef devSerialPort As SerialPort, ByRef o_IDGCmdData As tagIDGCmdData, ByVal bRS232CommOK As Boolean)
        Try
            ' Common Process to receive data 
            PreProcessCbRecv(devSerialPort, o_IDGCmdData, bRS232CommOK)
            '
        Catch ex As Exception
            LogLnThreadSafe("[Err] DelCRLSingleCbRecv() = " + ex.Message)
        End Try
    End Sub
    Public Overrides Sub DelCRLSingle(ByRef tagCRLSingle As tagCRLCmdUnitSingle, Optional ByVal nTimeout As Integer = ClassDevRS232.m_cnTX_TIMEOUT_IN_SEC)
        Dim u16Cmd As UInt16 = &H840D
        Dim strFuncName As String = "DelCRLSingle"

        '
        m_bIsRetStatusOK = False
        If (m_bIDGCmdBusy) Then
            Return
        End If
        m_bIDGCmdBusy = True
        Try
            '
            'Send D0-06 And Get Return Value
            m_tagIDGCmdDataInfo.Reinit(u16Cmd, m_arraybyteSendBuf, Me)

            m_tagIDGCmdDataInfo.ComposerAddData(tagCRLSingle.byteRID)
            m_tagIDGCmdDataInfo.ComposerAddData(tagCRLSingle.byteIndex)
            m_tagIDGCmdDataInfo.ComposerAddData(tagCRLSingle.byteSerialNo)
            '-----------------------------------------------------------------[ Send to reader ]-----------------------------------------------------------------
            'Compose 1)Protocol Header 2) Command Set 3) Length 4) Command Frame CRC Data
            PktComposerCommandFrameHeaderCmdSetDataLengthCRCData(m_tagIDGCmdDataInfo)

            '----------------[ Debug Section ]----------------
            If (m_bDebug) Then
                'Note: After PktComposerCommandFrameHeaderCmdSetDataLengthCRCData(), m_tagIDGCmdDataInfo.m_nDataSizeSend includes m_nPktHeader + m_nPktTrail
                'Form01_Main.LogLnDumpData(m_tagIDGCmdDataInfo.m_refarraybyteData, tagIDGCmdData.m_nPktHeader + m_tagIDGCmdDataInfo.m_nDataSizeSend + tagIDGCmdData.m_nPktTrail)
                Form01_Main.LogLnDumpData(m_tagIDGCmdDataInfo.m_refarraybyteData, m_tagIDGCmdDataInfo.m_nDataSizeSend)
            End If

            '----------------[ End Debug Section ]----------------
            'Waiting For Response in SetConfigurationsGroupSingleCbRecv()
            m_refdevConn.SendCommandFrameAndWaitForResponseFrame(AddressOf ClassReaderProtocolBasic.PreProcessCbRecv, m_tagIDGCmdDataInfo, m_nIDGCommanderType, True, nTimeout * 1000)

            'Its' response code = 0 => OK
            If (m_tagIDGCmdDataInfo.bIsResponseOK) Then
                m_bIsRetStatusOK = True
            Else
                m_bIsRetStatusOK = False
                LogLn("[Err] [IDG: " + GetIDGCmdString(u16Cmd) + "] " + strFuncName + "() = Response Code (" & m_tagIDGCmdDataInfo.m_nResponse & ") --> " + m_dictStatusCode2.Item(m_tagIDGCmdDataInfo.m_nResponse))
            End If

        Catch ext As TimeoutException
            LogLn("[Err] [RS232 Timeout] [IDG: " + GetIDGCmdString(u16Cmd) + "] " + strFuncName + "() = " + ext.Message)
        Catch ex As Exception
            LogLn("[Err] [IDG: " + GetIDGCmdString(u16Cmd) + "] " + strFuncName + "() = " + ex.Message)
        Finally
            m_bIDGCmdBusy = False

        End Try
    End Sub

    'IDG CMD:_8406
    '------------------------------------------------------------------------[IDG CMD:_84-06, Del CRL All ]------------------------------------------------------------------------
    'Java Labs Tool Code Put in "RevFile.java"
    Private Shared Sub DelCRLMassCbRecv(ByRef devSerialPort As SerialPort, ByRef o_IDGCmdData As tagIDGCmdData, ByVal bRS232CommOK As Boolean)
        Try
            ' Common Process to receive data 
            PreProcessCbRecv(devSerialPort, o_IDGCmdData, bRS232CommOK)
            '
        Catch ex As Exception
            LogLnThreadSafe("[Err] DelCAPublicKeyRIDsAllCbRecv() = " + ex.Message)
        End Try
    End Sub
    Public Overrides Sub DelCRLMass(Optional ByVal nTimeout As Integer = ClassDevRS232.m_cnTX_TIMEOUT_IN_SEC)
        Dim u16Cmd As UInt16 = &H8406
        Dim strFuncName As String = "DelCRLMass"
        '
        m_bIsRetStatusOK = False
        If (m_bIDGCmdBusy) Then
            Return
        End If
        m_bIDGCmdBusy = True
        Try
            '
            'Send D0-06 And Get Return Value
            m_tagIDGCmdDataInfo.Reinit(u16Cmd, m_arraybyteSendBuf, Me)

            '-----------------------------------------------------------------[ Send to reader ]-----------------------------------------------------------------
            'Compose 1)Protocol Header 2) Command Set 3) Length 4) Command Frame CRC Data
            PktComposerCommandFrameHeaderCmdSetDataLengthCRCData(m_tagIDGCmdDataInfo)

            '----------------[ Debug Section ]----------------
            If (m_bDebug) Then
                'Note: After PktComposerCommandFrameHeaderCmdSetDataLengthCRCData(), m_tagIDGCmdDataInfo.m_nDataSizeSend includes m_nPktHeader + m_nPktTrail
                'Form01_Main.LogLnDumpData(m_tagIDGCmdDataInfo.m_refarraybyteData, tagIDGCmdData.m_nPktHeader + m_tagIDGCmdDataInfo.m_nDataSizeSend + tagIDGCmdData.m_nPktTrail)
                Form01_Main.LogLnDumpData(m_tagIDGCmdDataInfo.m_refarraybyteData, m_tagIDGCmdDataInfo.m_nDataSizeSend)
            End If

            '----------------[ End Debug Section ]----------------
            'Waiting For Response in SetConfigurationsGroupSingleCbRecv()
            m_refdevConn.SendCommandFrameAndWaitForResponseFrame(AddressOf ClassReaderProtocolBasic.PreProcessCbRecv, m_tagIDGCmdDataInfo, m_nIDGCommanderType, True, nTimeout * 1000)

            'Its' response code = 0 => OK
            If (m_tagIDGCmdDataInfo.bIsResponseOK) Then
                m_bIsRetStatusOK = True
            Else
                m_bIsRetStatusOK = False
                LogLn("[Err] [IDG: " + GetIDGCmdString(u16Cmd) + "] " + strFuncName + "() = Response Code (" & m_tagIDGCmdDataInfo.m_nResponse & ") --> " + m_dictStatusCode2.Item(m_tagIDGCmdDataInfo.m_nResponse))
            End If

        Catch ext As TimeoutException
            LogLn("[Err] [RS232 Timeout][IDG: " + GetIDGCmdString(u16Cmd) + "] " + strFuncName + "() = " + ext.Message)
        Catch ex As Exception
            LogLn("[Err][IDG: " + GetIDGCmdString(u16Cmd) + "] " + strFuncName + "() = " + ex.Message)
        Finally
            m_bIDGCmdBusy = False
        End Try
        '
    End Sub


    'IDG CMD:_2900
    Private Shared Sub SystemGetFirmwareVersionCbRecv(ByRef devSerialPort As SerialPort, ByRef o_IDGCmdData As tagIDGCmdData, ByVal bRS232CommOK As Boolean)
        Try
            ' Common Process to receive data 
            PreProcessCbRecv(devSerialPort, o_IDGCmdData, bRS232CommOK)
            '
        Catch ex As Exception
            LogLnThreadSafe("[Err] SystemGetFirmwareVersionCbRecv() = " + ex.Message)
        End Try
    End Sub
    Public Overrides Sub SystemGetFirmwareVersion(ByRef o_strFwVersionList As List(Of String), Optional ByVal nTimeout As Integer = ClassDevRS232.m_cnTX_TIMEOUT_IN_SEC)
        Dim u16Cmd As UInt16 = &H2900
        Dim strFuncName As String = "SystemGetFirmwareVersion"
        Dim arrayByteSend As Byte() = Nothing
        Dim arrayByteRecv As Byte() = Nothing
        '
        If (m_bIDGCmdBusy) Then
            Return
        End If
        m_bIDGCmdBusy = True
        Try
            '
            System_Customized_Command(u16Cmd, strFuncName, arrayByteSend, arrayByteRecv, AddressOf ClassReaderProtocolBasic.PreProcessCbRecv, nTimeout)
            'Its' response code = 0 => OK
            If (bIsRetStatusOK) Then
                Dim arrayByteData As Byte() = Nothing


                m_tagIDGCmdDataInfo.ParserDataGetArrayRawData(arrayByteData)
                '
                If (arrayByteData IsNot Nothing) Then
                    ClassTableListMaster.ConvertFromArrayByte2StringsList(arrayByteData, o_strFwVersionList)
                    ' Free Resource
                    Erase arrayByteData
                Else
                    o_strFwVersionList.Clear()
                End If
            Else
                LogLn("[Err] SystemGetFirmwareVersion() = Response Code (" & m_tagIDGCmdDataInfo.m_nResponse & ") --> " + m_dictStatusCode2.Item(m_tagIDGCmdDataInfo.m_nResponse))
            End If

        Catch ext As TimeoutException
            LogLn("[Err] [RS232 Timeout] [IDG: " + GetIDGCmdString(u16Cmd) + "] " + strFuncName + "() = " + ext.Message)
        Catch ex As Exception
            LogLn("[Err][IDG: " + GetIDGCmdString(u16Cmd) + "] " + strFuncName + "() = " + ex.Message)
        Finally
            If (arrayByteSend IsNot Nothing) Then
                Erase arrayByteSend
            End If
            If (arrayByteRecv IsNot Nothing) Then
                Erase arrayByteRecv
            End If
            m_bIDGCmdBusy = False
        End Try
        '
    End Sub


    '
    Public m_dictFirmwareModuleListHeadline As Dictionary(Of enumModuleVersionMode, String) = New Dictionary(Of ClassReaderProtocolBasic.enumModuleVersionMode, String) From {
        {enumModuleVersionMode.ENUM_GET_00_ALL_READER_VARIABLES, "All Reader Variables"},
        {enumModuleVersionMode.ENUM_GET_01_PRODUCT_TYPE, "Product Type"},
        {enumModuleVersionMode.ENUM_GET_02_PROCESSOR_TYPE, "Processor Type"},
        {enumModuleVersionMode.ENUM_GET_03_MAIN_FIRMWARE_VERSION, "Main Firmware Version"},
        {enumModuleVersionMode.ENUM_GET_04_FIRMWARE_SUBSYSTEM_SUITE, "Firmware Subsystem Suite"},
        {enumModuleVersionMode.ENUM_GET_06_SERIAL_PROTOCOL_SUITE, "Serial Protocol Suite"},
        {enumModuleVersionMode.ENUM_GET_07_LAYER_1_PAYPASS_VERSION, "Layer 1 PayPass Version"},
        {enumModuleVersionMode.ENUM_GET_08_LAYER_1_ANTI_COLLISION_RESOLUTION_VERSION, "Layer 1 Anti-Collision Resolution Version"},
        {enumModuleVersionMode.ENUM_GET_09_RFU, "RFU, Mode= 09h"},
        {enumModuleVersionMode.ENUM_GET_0A_GET_LAYER_2_CARD_APPLICATION_SUITE, "Get Layer 2 Card Application Suite"},
        {enumModuleVersionMode.ENUM_GET_0B_RFU, "RFU, Mode= 0Bh"},
        {enumModuleVersionMode.ENUM_GET_0C_GET_USER_EXPERIENCE_SUITE, "Get User Experience Suite"},
        {enumModuleVersionMode.ENUM_GET_0E_GET_SYSTEM_INFORMATION_SUITE, "System Information Suite"},
        {enumModuleVersionMode.ENUM_GET_0F_GET_PATCH_VERSION_NUMBER, "Get Patch Version Number"},
        {enumModuleVersionMode.ENUM_GET_11_GET_MISCELLANEOUS_AND_PROPRIETARY_VERSION_STRINGS, "Get Miscellaneous And Proprietary Version Strings"}
    }

    '
    '
    '==================[ CMD: 09-00 ]==================
    Private Shared Sub SystemGetFirmwareModuleVersionCbRecv(ByRef devSerialPort As SerialPort, ByRef o_IDGCmdData As tagIDGCmdData, ByVal bRS232CommOK As Boolean)
        Try
            ' Common Process to receive data 
            PreProcessCbRecv(devSerialPort, o_IDGCmdData, bRS232CommOK)
            '
        Catch ex As Exception
            LogLnThreadSafe("[Err] SystemGetFirmwareModuleVersionCbRecv() = " + ex.Message)
        End Try
    End Sub
    Public Overrides Sub SystemGetFirmwareModuleVersion(ByVal nMVM As enumModuleVersionMode, ByRef o_strFwVersionList As List(Of String), Optional ByVal nTimeout As Integer = ClassDevRS232.m_cnTX_TIMEOUT_IN_SEC)
        Dim strSubCmd As String
        Dim u16Cmd As UInt16 = &H900
        Dim strFuncName As String = "SystemGetFirmwareModuleVersion"
        Dim arrayByteSend As Byte() = Nothing
        Dim arrayByteRecv As Byte() = Nothing

        If (m_bIDGCmdBusy) Then
            Return
        End If
        m_bIDGCmdBusy = True
        '
        strSubCmd = String.Format("{0,2:X2}", CType(nMVM, Integer))
        Try
            'Create Command Set
            u16Cmd = u16Cmd + CType(nMVM, UInt16)


            System_Customized_Command(u16Cmd, strFuncName, arrayByteSend, arrayByteRecv, AddressOf ClassReaderProtocolBasic.PreProcessCbRecv, nTimeout)

            'Its' response code = 0 => OK
            If (bIsRetStatusOK) Then
                Dim arrayByteData As Byte() = Nothing
                Dim strTmp As String = ""


                m_tagIDGCmdDataInfo.ParserDataGetArrayRawData(arrayByteData)
                '============[ Not Implemented Yet ]==============
                If (arrayByteData Is Nothing) Then
                    o_strFwVersionList.Clear()
                    Return
                End If

                Throw New Exception("[Err][Not Ready] IDG Cmd = 09-" + strSubCmd)

                If (nMVM = enumModuleVersionMode.ENUM_GET_00_ALL_READER_VARIABLES) Then
                    ClassTableListMaster.ConvertFromArrayByte2String(arrayByteData, strTmp)
                    o_strFwVersionList.Add(strTmp)
                Else
                    ClassTableListMaster.ConvertFromArrayByte2StringsList(arrayByteData, o_strFwVersionList)
                End If
                o_strFwVersionList.Insert(0, m_dictFirmwareModuleListHeadline.Item(nMVM))
                ' Free Resource
                Erase arrayByteData
            Else
                LogLn("[Err][IDG: " + GetIDGCmdString(u16Cmd) + "] " + strFuncName + "() =  Response Code (" & m_tagIDGCmdDataInfo.m_nResponse & ") --> " + m_dictStatusCode2.Item(m_tagIDGCmdDataInfo.m_nResponse))
            End If
        Catch ext As TimeoutException
            LogLn("[Err] [RS232 Timeout] [IDG: " + GetIDGCmdString(u16Cmd) + "] " + strFuncName + "() = " + ext.Message)
        Catch ex As Exception
            LogLn("[Err] [IDG: " + GetIDGCmdString(u16Cmd) + "] " + strFuncName + "() = " + ex.Message)
        Finally
            If (arrayByteSend IsNot Nothing) Then
                Erase arrayByteSend
            End If
            If (arrayByteRecv IsNot Nothing) Then
                Erase arrayByteRecv
            End If
            m_bIDGCmdBusy = False
        End Try
        '
    End Sub

    'IDG CMD:_D002
    Public Overrides Sub GetCAPublicKeyHashSingle()

    End Sub


    '---------------------------------------------[ IDG CMD:_01-01 ]---------------------------------------------
    Private Shared Sub SystemSetPollModeCbRecv(ByRef devSerialPort As SerialPort, ByRef o_IDGCmdData As tagIDGCmdData, ByVal bRS232CommOK As Boolean)
        Try
            ' Common Process to receive data 
            PreProcessCbRecv(devSerialPort, o_IDGCmdData, bRS232CommOK)
            '
        Catch ex As Exception
            LogLnThreadSafe("[Err] SystemPingCbRecv() = " + ex.Message)
        End Try
    End Sub
    '

    Public Overrides Sub SystemSetPollMode(ByVal bPollMode As Boolean, Optional ByVal nTimeout As Integer = ClassDevRS232.m_cnTX_TIMEOUT_IN_SEC)
        Dim bytePollMode As Byte
        Dim u16Cmd As UInt16 = &H101
        Dim strFuncName As String = "SystemSetPollMode"
        Dim arrayByteSend As Byte() = Nothing
        Dim arrayByteRecv As Byte() = Nothing
        '
        If (m_bIDGCmdBusy) Then
            Return
        End If
        m_bIDGCmdBusy = True
        '
        Try
            'Set Poll Mode
            If bPollMode Then
                bytePollMode = &H1
            Else
                bytePollMode = &H0
            End If
            arrayByteSend = New Byte() {bytePollMode}

            System_Customized_Command(u16Cmd, strFuncName, arrayByteSend, arrayByteRecv, AddressOf ClassReaderProtocolBasic.PreProcessCbRecv, nTimeout)

            'Its' response code = 0 => OK
            If (bIsRetStatusOK) Then
            Else
            End If

        Catch ext As TimeoutException
            LogLn("[Err][RS232 Timeout] [IDG: " + GetIDGCmdString(u16Cmd) + "] " + strFuncName + "() = " + ext.Message)
        Catch ex As Exception
            LogLn("[Err] [IDG: " + GetIDGCmdString(u16Cmd) + "] " + strFuncName + "() = " + ex.Message)
        Finally
            m_tagIDGCmdDataInfo.Cleanup()
            If (arrayByteSend IsNot Nothing) Then
                Erase arrayByteSend
            End If
            If (arrayByteRecv IsNot Nothing) Then
                Erase arrayByteRecv
            End If
            m_bIDGCmdBusy = False
        End Try

    End Sub

    '---------------------------------------------[ IDG CMD:_02-01 ]---------------------------------------------
    Private Shared Sub TransactionActivateCbRecv(ByRef devSerialPort As SerialPort, ByRef o_IDGCmdData As tagIDGCmdData, ByVal bRS232CommOK As Boolean)
        Try
            ' Common Process to receive data 
            PreProcessCbRecv(devSerialPort, o_IDGCmdData, bRS232CommOK)
            '
        Catch ex As Exception
            LogLnThreadSafe("[Err] TransactionActivateCbRecv() = " + ex.Message)
        End Try
    End Sub

    ''<<<< Call this function before send >>>>>
    'Private Shared Function TransactionActivate_Cbfunc_Preprocessor(ByRef devSerialPort As SerialPort, ByRef o_IDGCmdData As tagIDGCmdData, ByVal bRS232CommOK As Boolean) As Boolean
    '    Try
    '        ' Common Process to receive data 
    '        PreProcessCbRecv(devSerialPort, o_IDGCmdData, bRS232CommOK)
    '        '
    '    Catch ex As Exception
    '        LogLnThreadSafe("[Err] TransactionActivateCbRecv() = " + ex.Message)
    '    End Try
    '    '
    '    Return True
    'End Function

    ''<<<<< Call this function after recv but before Response Code (R.C.) Check >>>>
    'Private Shared Function TransactionActivate_Cbfunc_Postprocessor(ByRef devSerialPort As SerialPort, ByRef o_IDGCmdData As tagIDGCmdData, ByVal bRS232CommOK As Boolean) As Boolean
    '    Try
    '        ' Common Process to receive data 
    '        PreProcessCbRecv(devSerialPort, o_IDGCmdData, bRS232CommOK)
    '        '
    '    Catch ex As Exception
    '        LogLnThreadSafe("[Err] TransactionActivateCbRecv() = " + ex.Message)
    '    End Try
    '    '
    '    Return True
    'End Function

    ''<<<<< Call this function For Response Code (R.C.) Check And Action >>>>
    'Private Shared Function TransactionActivate_Cbfunc_ResponseCodeCheckAndAction(ByRef devSerialPort As SerialPort, ByRef o_IDGCmdData As tagIDGCmdData, ByVal bRS232CommOK As Boolean) As Boolean
    '    Try
    '        ' Common Process to receive data 
    '        PreProcessCbRecv(devSerialPort, o_IDGCmdData, bRS232CommOK)
    '        '
    '    Catch ex As Exception
    '        LogLnThreadSafe("[Err] TransactionActivateCbRecv() = " + ex.Message)
    '    End Try
    '    '
    '    Return True
    'End Function
    Private m_u16CmdAT As UInt16 = &H201

    Property ATCmdCode As UInt16
        Get
            Return m_u16CmdAT
        End Get
        Set(value As UInt16)
            m_u16CmdAT = value
        End Set
    End Property
    '<Note> TransactionActivate's nTimoutSec is time out period in second
    '<Note> OUTPUT : o_arrayByteTxtResponse is Raw Data Only. No ViVO Protocol2 Header(i.e. "ViVOtech2\x0\byte1Len\byte2Len") And No Trail CRC 2 bytes 
    Public Overrides Sub TransactionActivate(ByVal dictTxnSendTLVs As Dictionary(Of String, tagTLV), ByRef o_tagInfoTxnResponse As ClassTransaction.TAG_TXN_PARSE_RESPONSE_INFO, Optional ByVal nTimeout As Integer = ClassDevRS232.m_cnTX_TIMEOUT_IN_SEC)
        Dim u16Cmd As UInt16 = m_u16CmdAT '&H201
        Dim strFuncName As String = "TransactionActivate"
        Dim arrayByteSend As Byte() = Nothing
        Dim arrayByteRecv As Byte() = Nothing

        m_bIsRetStatusOK = False
        If (m_bIDGCmdBusy) Then
            Return
        End If
        m_bIDGCmdBusy = True
        Try
            '-----------------------------------------------------------------[ Send to reader ]-----------------------------------------------------------------
            'Compose The IDG Packet 01-01, Not Command Frame Data, No Response Frame Data
            m_tagIDGCmdDataInfo.Reinit(u16Cmd, m_arraybyteSendBuf, Me)

            'Compose Necessary TLVs
            m_tagIDGCmdDataInfo.ComposerAddData(CType(nTimeout, Byte))
            If dictTxnSendTLVs.Count > 0 Then
                ComposeRestTLVsConfigurations(dictTxnSendTLVs, m_tagIDGCmdDataInfo)
            End If

            PktComposerCommandFrameHeaderCmdSetDataLengthCRCData(m_tagIDGCmdDataInfo)

            m_refdevConn.SendCommandFrameAndWaitForResponseFrame(AddressOf ClassReaderProtocolBasic.PreProcessCbRecv, m_tagIDGCmdDataInfo, m_nIDGCommanderType, True, nTimeout * 1000)

            'Copy Received Data To o_tagInfoTxnResponse's Buffer
            If o_tagInfoTxnResponse.Equals(Nothing) Then
                o_tagInfoTxnResponse = New ClassTransaction.TAG_TXN_PARSE_RESPONSE_INFO(m_tagIDGCmdDataInfo)
            Else
                o_tagInfoTxnResponse.RenewResponseBufferAndUpdateStatusCode(m_tagIDGCmdDataInfo)
            End If

            If m_tagIDGCmdDataInfo.CallByIDGCmdTimeOutChk_IsCommStopped Then
                'Stop By Cancel / Stop / Timeout
                o_tagInfoTxnResponse.TransactionStoppedSet = True
                Exit Sub
            End If

            'Its' response code = 0 => OK
            If (m_tagIDGCmdDataInfo.bIsResponseOK) Then
                'Parse Response  OK Data

                'If Response Code
                m_bIsRetStatusOK = True
            Else
                'Parse Failed Data

            End If
        Catch ex As TimeoutException
            LogLn("[Err][RS232 Timeout] [IDG: " + GetIDGCmdString(u16Cmd) + "] " + strFuncName + "() = " + ex.Message)
        Catch ex As Exception
            LogLn("[Err] [IDG: " + GetIDGCmdString(u16Cmd) + "] " + strFuncName + "() = " + ex.Message)
        Finally
            m_tagIDGCmdDataInfo.Cleanup()
            If (arrayByteSend IsNot Nothing) Then
                Erase arrayByteSend
            End If
            If (arrayByteRecv IsNot Nothing) Then
                Erase arrayByteRecv
            End If
            m_bIDGCmdBusy = False
        End Try

    End Sub

    '
    Public Overrides Sub TransactionActivate(ByVal dictTxnSendTLVs As SortedDictionary(Of String, tagTLV), ByRef o_tagInfoTxnResponse As ClassTransaction.TAG_TXN_PARSE_RESPONSE_INFO, Optional ByVal nTimeout As Integer = ClassDevRS232.m_cnTX_TIMEOUT_IN_SEC)
        Dim u16Cmd As UInt16 = m_u16CmdAT '&H201
        Dim strFuncName As String = "TransactionActivate"
        '
        m_bIsRetStatusOK = False
        If (m_bIDGCmdBusy) Then
            Return
        End If
        m_bIDGCmdBusy = True
        Try
            '-----------------------------------------------------------------[ Send to reader ]-----------------------------------------------------------------
            'Compose The IDG Packet 01-01, Not Command Frame Data, No Response Frame Data
            m_tagIDGCmdDataInfo.Reinit(u16Cmd, m_arraybyteSendBuf, Me)

            'Compose Necessary TLVs
            m_tagIDGCmdDataInfo.ComposerAddData(CType(nTimeout, Byte))
            If dictTxnSendTLVs.Count > 0 Then
                ComposeRestTLVsConfigurations(dictTxnSendTLVs, m_tagIDGCmdDataInfo)
            End If

            PktComposerCommandFrameHeaderCmdSetDataLengthCRCData(m_tagIDGCmdDataInfo)

            m_refdevConn.SendCommandFrameAndWaitForResponseFrame(AddressOf ClassReaderProtocolBasic.PreProcessCbRecv, m_tagIDGCmdDataInfo, m_nIDGCommanderType, True, nTimeout * 1000)

            'Copy Received Data To o_tagInfoTxnResponse's Buffer
            If o_tagInfoTxnResponse.Equals(Nothing) Then
                o_tagInfoTxnResponse = New ClassTransaction.TAG_TXN_PARSE_RESPONSE_INFO(m_tagIDGCmdDataInfo)
            Else
                o_tagInfoTxnResponse.RenewResponseBufferAndUpdateStatusCode(m_tagIDGCmdDataInfo)
            End If

            If m_tagIDGCmdDataInfo.CallByIDGCmdTimeOutChk_IsCommStopped Then
                'Stop By Cancel / Stop / Timeout
                o_tagInfoTxnResponse.TransactionStoppedSet = True
                Exit Sub
            End If

            'Its' response code = 0 => OK
            If (m_tagIDGCmdDataInfo.bIsResponseOK) Then
                'Parse Response  OK Data

                'If Response Code
                m_bIsRetStatusOK = True
            Else
                'Parse Failed Data

            End If
        Catch ex As TimeoutException
            LogLn("[Err][RS232 Timeout] [IDG: " + GetIDGCmdString(u16Cmd) + "] " + strFuncName + "() = " + ex.Message)
        Catch ex As Exception
            LogLn("[Err] [IDG: " + GetIDGCmdString(u16Cmd) + "] " + strFuncName + "() = " + ex.Message)
        Finally
            m_tagIDGCmdDataInfo.Cleanup()
            m_bIDGCmdBusy = False
        End Try

    End Sub

    Public Overrides Sub ExGetRecvStatusCodeByte(ByRef o_byteStatus As Byte)
        o_byteStatus = m_tagIDGCmdDataInfo.byteGetResponseCode()
    End Sub
    Public Overrides Sub ExGetRecvStatusCodeString(ByRef o_strStatus As String)
        o_strStatus = m_tagIDGCmdDataInfo.strGetResponseMsg()
    End Sub
    'IDG CMD:_0300
    Public Overrides Sub TransactionGetResult()

    End Sub

    'IDG CMD:_0501
    Private Shared Sub TransactionCancelCbRecv(ByRef devSerialPort As SerialPort, ByRef o_IDGCmdData As tagIDGCmdData, ByVal bRS232CommOK As Boolean)
        Try
            ' Common Process to receive data 
            PreProcessCbRecv(devSerialPort, o_IDGCmdData, bRS232CommOK)
            '
        Catch ex As Exception
            LogLnThreadSafe("[Err] TransactionCancelCbRecv() = " + ex.Message)
        End Try
    End Sub
    '
    '
    Public Overrides Sub TransactionCancel(Optional ByVal nTimeoutSec As Integer = ClassDevRS232.m_cnTX_TIMEOUT_IN_SEC)
        Dim u16Cmd As UInt16 = &H501
        Dim strFuncName As String = ""
        '
        'Cancel Command use 
        '1.independent tagIDGCmdDataInfo instead of m_tagIDGCmdDataInfo  for thread safe policy 
        'Dim tagIDGCmdDataInfo As tagIDGCmdData
        '2. Independent In/Out Buffer
        Dim arrayByteBuf As Byte() = New Byte(1023) {}

        'Don't Check Critical Section Since Cancel Command is always in Highest Priority
        m_bIsRetStatusOK = False
        If (m_bIDGCmdBusy) Then
            Return
        End If
        m_bIDGCmdBusy = True
        Try
            '-----------------------------------------------------------------[ Send to reader ]-----------------------------------------------------------------
            'If there is ALREADY a IDG Transaction Activate Command Running, we have to Terminate it.
            If (m_bIDGCmdBusy) Then
                m_tagIDGCmdDataInfo.CallByIDGCmdTimeOutChk_SetTimeoutEventIDGCancelStop()
            End If

            'Reset Recv IDGCmdInfo Sync Function to sync m_tagIDGCmdDataInfoTransactionCancel data after recv thread done
            m_devRS232.SetCallbackRecvParameterSync(AddressOf ClassReaderProtocolIDGGR.RecvParameterSyncForTransactionCancelCase)

            'Compose The IDG Packet 01-01, Not Command Frame Data, No Response Frame Data
            m_tagIDGCmdDataInfoTransactionCancel.Reinit(u16Cmd, arrayByteBuf, Me)

            PktComposerCommandFrameHeaderCmdSetDataLengthCRCData(m_tagIDGCmdDataInfoTransactionCancel)

            m_refdevConn.SendCommandFrameAndWaitForResponseFrame(AddressOf ClassReaderProtocolBasic.PreProcessCbRecv, m_tagIDGCmdDataInfoTransactionCancel, m_nIDGCommanderType, True, nTimeoutSec * 1000)

            'Its' response code = 0 => OK
            If (m_tagIDGCmdDataInfoTransactionCancel.bIsResponseOK) Then
                'Parse Response  OK Data

                'If Response Code
                m_bIsRetStatusOK = True
            Else
                'Parse Failed Data

            End If

        Catch ext As TimeoutException
            LogLn("[Err][RS232 Timeout] [IDG: " + GetIDGCmdString(u16Cmd) + "] " + strFuncName + "() = " + ext.Message)
        Catch ex As Exception
            LogLn("[Err] [IDG: " + GetIDGCmdString(u16Cmd) + "] " + strFuncName + "() = " + ex.Message)
        Finally
            m_tagIDGCmdDataInfoTransactionCancel.Cleanup()
            Erase arrayByteBuf
            'Restore To Default Recv IDGCmdInfo Sync Function
            m_devRS232.SetCallbackRecvParameterSync(AddressOf ClassReaderProtocolIDGGR.RecvParameterSync)
            m_bIDGCmdBusy = False
        End Try

    End Sub

    'IDG CMD:_0502
    Public Overrides Sub TransactionStop()

    End Sub

    'IDG CMD:_840F
    Private Shared Sub TransactionMChip30TornTransactionLogCleanCbRecv(ByRef devSerialPort As SerialPort, ByRef o_IDGCmdData As tagIDGCmdData, ByVal bRS232CommOK As Boolean)
        Try
            ' Common Process to receive data 
            PreProcessCbRecv(devSerialPort, o_IDGCmdData, bRS232CommOK)
            '
        Catch ex As Exception
            LogLnThreadSafe("[Err] TransactionMChip30TornTransactionLogCleanCbRecv() = " + ex.Message)
        End Try
    End Sub
    Public Overrides Sub TransactionMChip30TornTransactionLogClean(Optional ByVal nTimeout As Integer = ClassDevRS232.m_cnTX_TIMEOUT_IN_SEC)
        Dim u16Cmd As UInt16 = &H840F
        Dim strFuncName As String = ""
        Dim arrayByteSend As Byte() = Nothing
        Dim arrayByteRecv As Byte() = Nothing

        If (m_bIDGCmdBusy) Then
            Return
        End If
        m_bIDGCmdBusy = True
        Try

            System_Customized_Command(u16Cmd, strFuncName, arrayByteSend, arrayByteRecv, AddressOf ClassReaderProtocolBasic.PreProcessCbRecv, nTimeout)

            'Its' response code = 0 => OK
            If (bIsRetStatusOK) Then
            Else
            End If
        Catch ext As TimeoutException
            LogLn("[Err] [RS232 Timeout] [IDG: " + GetIDGCmdString(u16Cmd) + "] " + strFuncName + "() = " + ext.Message)
        Catch ex As Exception
            LogLn("[Err] [IDG: " + GetIDGCmdString(u16Cmd) + "] " + strFuncName + "() = " + ex.Message)
        Finally
            'Release Resource 
            If (arrayByteSend IsNot Nothing) Then
                Erase arrayByteSend
            End If
            If (arrayByteRecv IsNot Nothing) Then
                Erase arrayByteRecv
            End If
            m_bIDGCmdBusy = False
        End Try
        '
    End Sub

    'IDG CMD:_840E
    Private Shared Sub TransactionMChip30TornTransactionLogResetCbRecv(ByRef devSerialPort As SerialPort, ByRef o_IDGCmdData As tagIDGCmdData, ByVal bRS232CommOK As Boolean)
        Try
            ' Common Process to receive data 
            PreProcessCbRecv(devSerialPort, o_IDGCmdData, bRS232CommOK)
            '
        Catch ex As Exception
            LogLnThreadSafe("[Err] TransactionMChip30TornTransactionLogResetCbRecv() = " + ex.Message)
        End Try
    End Sub
    Public Overrides Sub TransactionMChip30TornTransactionLogReset(Optional ByVal nTimeout As Integer = ClassDevRS232.m_cnTX_TIMEOUT_IN_SEC)
        Dim u16Cmd As UInt16 = &H840E
        Dim strFuncName As String = "TransactionMChip30TornTransactionLogReset"

        Dim arrayByteSend As Byte() = Nothing
        Dim arrayByteRecv As Byte() = Nothing

        If (m_bIDGCmdBusy) Then
            Return
        End If
        m_bIDGCmdBusy = True
        Try

            System_Customized_Command(u16Cmd, strFuncName, arrayByteSend, arrayByteRecv, AddressOf ClassReaderProtocolBasic.PreProcessCbRecv, nTimeout)

            'Its' response code = 0 => OK
            If (bIsRetStatusOK) Then
            Else
            End If
        Catch ext As TimeoutException
            LogLn("[Err] [RS232 Timeout] [IDG: " + GetIDGCmdString(u16Cmd) + "] " + strFuncName + "() = " + ext.Message)
        Catch ex As Exception
            LogLn("[Err] [IDG: " + GetIDGCmdString(u16Cmd) + "] " + strFuncName + "() = " + ex.Message)
        Finally
            If (arrayByteSend IsNot Nothing) Then
                Erase arrayByteSend
            End If
            If (arrayByteRecv IsNot Nothing) Then
                Erase arrayByteRecv
            End If
            m_bIDGCmdBusy = False
        End Try
        '
    End Sub

    '--------------------------------------------------[ IDG CMD: 03-03]--------------------------------------------------
    Private Shared Sub TransactionUpdateBalanceCbRecv(ByRef devSerialPort As SerialPort, ByRef o_IDGCmdData As tagIDGCmdData, ByVal bRS232CommOK As Boolean)
        Try
            ' Common Process to receive data 
            PreProcessCbRecv(devSerialPort, o_IDGCmdData, bRS232CommOK)
            '
        Catch ex As Exception
            LogLnThreadSafe("[Err] TransactionUpdateBalanceCbRecv() = " + ex.Message)
        End Try
    End Sub
    'IDG CMD:_0303
    Public Overrides Sub TransactionUpdateBalance(ByVal byteApprovedDeclined As Byte, ByVal arrayByteAuthCode As Byte(), ByVal dictTLVs As Dictionary(Of String, tagTLV), ByRef o_tagInfoTxnResponse As ClassTransaction.TAG_TXN_PARSE_RESPONSE_INFO, Optional ByVal nTimeoutSec As Integer = ClassDevRS232.m_cnTX_TIMEOUT_IN_SEC)
        Dim u16Cmd As UInt16 = &H303
        Dim strFuncName As String = "TransactionUpdateBalance"

        Dim arrayByteSend As Byte() = Nothing
        Dim arrayByteRecv As Byte() = Nothing

        m_bIsRetStatusOK = False
        If (m_bIDGCmdBusy) Then
            Return
        End If
        m_bIDGCmdBusy = True
        Try
            'Send D0-06 And Get Return Value
            m_tagIDGCmdDataInfo.Reinit(u16Cmd, m_arraybyteSendBuf, Me)

            'Status Code
            '1 byte
            '00: OK 01: NOT OK
            If byteApprovedDeclined > &H1 Then
                Throw New Exception("Invalid Approve Code " & byteApprovedDeclined & ", It should be ('00' = OK, Approved; or '01' = Not OK, Declined)")
            End If
            m_tagIDGCmdDataInfo.ComposerAddData(byteApprovedDeclined)

            '            TLV Auth_Code
            '9 bytes
            'Authorization Code as a TLV object.
            'Tag:        E300 Format : b8()
            m_tagIDGCmdDataInfo.ComposerAddTLV("E300", 6, "123456")

            'TLV Transaction Date
            '9A
            'EMV data element “Transaction Date” as a TLV data object. Local date that the transaction was authorized. If this TLV is not provided, the transaction uses the reader’s current date.
            'Tag: 9A Format: n6 (YYMMDD)
            'Note: The reader does not perform range checking on this value. The POS application should perform range checking on this value to ensure it is within acceptable limits.
            'TLV Transaction Time
            '9F21
            'EMV data element “Transaction Time” as a TLV data object. Local time that the transaction was authorized. If this TLV is not provided, the transaction uses the reader’s current time.
            'Tag: 9F21 Format: n6 (HHMMSS)
            'Note: The reader does not perform range checking on this value. The POS application should perform range checking on this value to ensure it is within acceptable limits.
            If dictTLVs.Count > 0 Then
                ComposeRestTLVsConfigurations(dictTLVs, m_tagIDGCmdDataInfo)
            End If

            '-----------------------------------------------------------------[ Send to reader ]-----------------------------------------------------------------
            'Compose 1)Protocol Header 2) Command Set 3) Length 4) Command Frame CRC Data
            PktComposerCommandFrameHeaderCmdSetDataLengthCRCData(m_tagIDGCmdDataInfo)

            '----------------[ Debug Section ]----------------
            'If (m_bDebug) Then
            '    'Note: After PktComposerCommandFrameHeaderCmdSetDataLengthCRCData(), m_tagIDGCmdDataInfo.m_nDataSizeSend includes m_nPktHeader + m_nPktTrail
            '    'Form01_Main.LogLnDumpData(m_tagIDGCmdDataInfo.m_refarraybyteData, tagIDGCmdData.m_nPktHeader + m_tagIDGCmdDataInfo.m_nDataSizeSend + tagIDGCmdData.m_nPktTrail)
            '    Form01_Main.LogLnDumpData(m_tagIDGCmdDataInfo.m_refarraybyteData, m_tagIDGCmdDataInfo.m_nDataSizeSend)
            'End If

            '----------------[ End Debug Section ]----------------
            'Waiting For Response in SetConfigurationsGroupSingleCbRecv()
            m_refdevConn.SendCommandFrameAndWaitForResponseFrame(AddressOf ClassReaderProtocolBasic.PreProcessCbRecv, m_tagIDGCmdDataInfo, m_nIDGCommanderType, True, nTimeoutSec * 1000)

            'Copy Received Data To o_tagInfoTxnResponse's Buffer
            If o_tagInfoTxnResponse.Equals(Nothing) Then
                o_tagInfoTxnResponse = New ClassTransaction.TAG_TXN_PARSE_RESPONSE_INFO(m_tagIDGCmdDataInfo)
            Else
                o_tagInfoTxnResponse.RenewResponseBufferAndUpdateStatusCode(m_tagIDGCmdDataInfo)
            End If

            If m_tagIDGCmdDataInfo.CallByIDGCmdTimeOutChk_IsCommStopped Then
                'Stop By Cancel / Stop / Timeout
                o_tagInfoTxnResponse.TransactionStoppedSet = True
                Exit Sub
            End If

            'Its' response code = 0 => OK
            If (m_tagIDGCmdDataInfo.bIsResponseOK) Then
                m_bIsRetStatusOK = True
            Else
                m_bIsRetStatusOK = False
            End If
        Catch ext As TimeoutException
            LogLn("[Err] [RS232 Timeout] [IDG: " + GetIDGCmdString(u16Cmd) + "] " + strFuncName + "() = " + ext.Message)
        Catch ex As Exception
            LogLn("[Err] [IDG: " + GetIDGCmdString(u16Cmd) + "] " + strFuncName + "() = " + ex.Message)
        Finally
            m_tagIDGCmdDataInfo.Cleanup()
            If (arrayByteSend IsNot Nothing) Then
                Erase arrayByteSend
            End If
            If (arrayByteRecv IsNot Nothing) Then
                Erase arrayByteRecv
            End If
            m_bIDGCmdBusy = False
        End Try
        '
    End Sub

    '--------------------------------------------------[ IDG CMD: D0-07 ]--------------------------------------------------
    Private Shared Sub GetCAPublicKeyListIDMassCbRecv(ByRef devSerialPort As SerialPort, ByRef o_IDGCmdData As tagIDGCmdData, ByVal bRS232CommOK As Boolean)
        Try
            ' Common Process to receive data 
            PreProcessCbRecv(devSerialPort, o_IDGCmdData, bRS232CommOK)
            '
        Catch ex As Exception
            LogLnThreadSafe("[Err] GetCAPublicKeyListIDMassCbRecv() = " + ex.Message)
        End Try
    End Sub


    '------------------------------------------------------------------------[ IDG CMD: D0-01 ]------------------------------------------------------------------------
    Private Shared Sub GetCAPublicKeyRIDSingleCbRecv(ByRef devSerialPort As SerialPort, ByRef o_IDGCmdData As tagIDGCmdData, ByVal bRS232CommOK As Boolean)
        Try
            ' Common Process to receive data 
            PreProcessCbRecv(devSerialPort, o_IDGCmdData, bRS232CommOK)
            '
        Catch ex As Exception
            LogLnThreadSafe("[Err] GetCAPublicKeyRIDSingleCbRecv() = " + ex.Message)
        End Try
    End Sub


    'IDG CMD:_D005
    '------------------------------------------------------------------------[IDG CMD:_D0-05, Del RIDs All ]------------------------------------------------------------------------
    Private Shared Sub DelCAPublicKeyRIDsAllCbRecv(ByRef devSerialPort As SerialPort, ByRef o_IDGCmdData As tagIDGCmdData, ByVal bRS232CommOK As Boolean)
        Try
            ' Common Process to receive data 
            PreProcessCbRecv(devSerialPort, o_IDGCmdData, bRS232CommOK)
            '
        Catch ex As Exception
            LogLnThreadSafe("[Err] DelCAPublicKeyRIDsAllCbRecv() = " + ex.Message)
        End Try
    End Sub
    Public Overrides Sub DelCAPublicKeyRIDsAll(Optional ByVal nTimeout As Integer = ClassDevRS232.m_cnTX_TIMEOUT_IN_SEC)
        Dim u16Cmd As UInt16 = &HD005
        Dim strFuncName As String = "DelCAPublicKeyRIDsAll"

        Dim arrayByteSend As Byte() = Nothing
        Dim arrayByteRecv As Byte() = Nothing

        '
        If (m_bIDGCmdBusy) Then
            Return
        End If
        m_bIDGCmdBusy = True
        Try
            '
            System_Customized_Command(u16Cmd, strFuncName, arrayByteSend, arrayByteRecv, AddressOf ClassReaderProtocolIDGGR.DelCAPublicKeyRIDsAllCbRecv, nTimeout)

            'Its' response code = 0 => OK
            If (bIsRetStatusOK) Then
            Else
                LogLn("[Err] [IDG: " + GetIDGCmdString(u16Cmd) + "] " + strFuncName + "() =  Response Code (" & m_tagIDGCmdDataInfo.m_nResponse & ") --> " + m_dictStatusCodeCAPKRIDKeyIndex.Item(m_tagIDGCmdDataInfo.m_nResponse))
            End If

        Catch ext As TimeoutException
            LogLn("[Err] [RS232 Timeout] [IDG: " + GetIDGCmdString(u16Cmd) + "] " + strFuncName + "() = " + ext.Message)
        Catch ex As Exception
            LogLn("[Err] [IDG: " + GetIDGCmdString(u16Cmd) + "] " + strFuncName + "() = " + ex.Message)
        Finally
            If (arrayByteSend IsNot Nothing) Then
                Erase arrayByteSend
            End If
            If (arrayByteRecv IsNot Nothing) Then
                Erase arrayByteRecv
            End If
            m_bIDGCmdBusy = False
        End Try
        '
    End Sub

    '------------------------------------------------------------------------[IDG CMD:_D0-04, Del RID + Index  ]------------------------------------------------------------------------
    Private Shared Sub DelCAPublicKeyRIDSingleCbRecv(ByRef devSerialPort As SerialPort, ByRef o_IDGCmdData As tagIDGCmdData, ByVal bRS232CommOK As Boolean)
        Try
            ' Common Process to receive data 
            PreProcessCbRecv(devSerialPort, o_IDGCmdData, bRS232CommOK)
            '
        Catch ex As Exception
            LogLnThreadSafe("[Err] DelCAPublicKeyRIDSingleCbRecv() = " + ex.Message)
        End Try
    End Sub
    'IDG CMD:_D004
    Public Overrides Sub DelCAPublicKeyRIDSingle(ByVal arrayByteRID() As Byte, ByVal byteKeyIndex As Byte, Optional ByVal nTimeout As Integer = ClassDevRS232.m_cnTX_TIMEOUT_IN_SEC)
        Dim nRIDLen As Integer = 5
        Dim u16Cmd As UInt16 = &HD004
        Dim strFuncName As String = "DelCAPublicKeyRIDSingle"
        '
        m_bIsRetStatusOK = False
        If (m_bIDGCmdBusy) Then
            Return
        End If
        m_bIDGCmdBusy = True
        Try
            '
            'Send D0-06 And Get Return Value
            m_tagIDGCmdDataInfo.Reinit(u16Cmd, m_arraybyteSendBuf, Me)

            'Put RID As Input Parameter, 5 bytes in length
            Array.Copy(arrayByteRID, 0, m_arraybyteSendBuf, tagIDGCmdData.m_nPktHeader, nRIDLen)
            m_tagIDGCmdDataInfo.m_nDataSizeSend = m_tagIDGCmdDataInfo.m_nDataSizeSend + nRIDLen

            'Put RID's Key Index (1 Byte)
            m_arraybyteSendBuf(tagIDGCmdData.m_nPktHeader + m_tagIDGCmdDataInfo.m_nDataSizeSend) = byteKeyIndex
            m_tagIDGCmdDataInfo.m_nDataSizeSend = m_tagIDGCmdDataInfo.m_nDataSizeSend + 1
            '-----------------------------------------------------------------[ Send to reader ]-----------------------------------------------------------------
            'Compose 1)Protocol Header 2) Command Set 3) Length 4) Command Frame CRC Data
            PktComposerCommandFrameHeaderCmdSetDataLengthCRCData(m_tagIDGCmdDataInfo)

            '----------------[ Debug Section ]----------------
            If (m_bDebug) Then
                'Note: After PktComposerCommandFrameHeaderCmdSetDataLengthCRCData(), m_tagIDGCmdDataInfo.m_nDataSizeSend includes m_nPktHeader + m_nPktTrail
                'Form01_Main.LogLnDumpData(m_tagIDGCmdDataInfo.m_refarraybyteData, tagIDGCmdData.m_nPktHeader + m_tagIDGCmdDataInfo.m_nDataSizeSend + tagIDGCmdData.m_nPktTrail)
                Form01_Main.LogLnDumpData(m_tagIDGCmdDataInfo.m_refarraybyteData, m_tagIDGCmdDataInfo.m_nDataSizeSend)
            End If

            '----------------[ End Debug Section ]----------------
            'Waiting For Response in SetConfigurationsGroupSingleCbRecv()
            m_refdevConn.SendCommandFrameAndWaitForResponseFrame(AddressOf ClassReaderProtocolBasic.PreProcessCbRecv, m_tagIDGCmdDataInfo, m_nIDGCommanderType, True, nTimeout * 1000)


            'Its' response code = 0 => OK
            If (m_tagIDGCmdDataInfo.bIsResponseOK) Then
                m_bIsRetStatusOK = True
            Else
                m_bIsRetStatusOK = False
                LogLn("[Err] DelCAPublicKeyRIDSingle() = Response Code (" & m_tagIDGCmdDataInfo.m_nResponse & ") --> " + m_dictStatusCodeCAPKRIDKeyIndex.Item(m_tagIDGCmdDataInfo.m_nResponse))
            End If

        Catch ext As TimeoutException
            Dim strRIDKeyIndex As String = ""
            ClassTableListMaster.ConvertFromArrayByte2String(arrayByteRID, strRIDKeyIndex)
            strRIDKeyIndex = strRIDKeyIndex + " " + String.Format("{0,2:X2}", byteKeyIndex)
            LogLn("[Err] [RS232 Timeout] [IDG: " + GetIDGCmdString(u16Cmd) + "] " + strFuncName + "() = " + ext.Message + ",RID+ Key Index = " + strRIDKeyIndex)
        Catch ex As Exception
            LogLn("[Err] [IDG: " + GetIDGCmdString(u16Cmd) + "] " + strFuncName + "() = " + ex.Message)
        Finally
            m_bIDGCmdBusy = False
        End Try
        '
    End Sub



    'Private Shared Function IsOkWhileCRCCheckFrameResponse(ByRef refIDGCmdData As tagIDGCmdData) As Boolean
    '    Dim u16ResponseFrameCRCCompute As UInt16
    '    Dim u16ResponseFrameCRCTrail As UInt16
    '    Dim nCRCPos As Integer
    '    'Dim strMsg As String

    '    With refIDGCmdData
    '        nCRCPos = .m_nDataSizeRecv - tagIDGCmdData.m_nPktTrail

    '        'Calculate CRC of Response Frame (Header + Data)
    '        u16ResponseFrameCRCCompute = ClassReaderCommander.IDGCmdCRCEvaluate(.m_refarraybyteData, nCRCPos)

    '        'strMsg = String.Format("Eval CRC =  {0,4:X4}", u16ResponseFrameCRCCompute)
    '        'LogLnThreadSafe(strMsg)

    '        'Get Response Frame CRC, Big-Endian
    '        u16ResponseFrameCRCTrail = (CType(.m_refarraybyteData(nCRCPos), UInt16) << 8 And &HFF00) + (CType(.m_refarraybyteData(nCRCPos + 1), UInt16) And &HFF)
    '        'strMsg = String.Format("Trail CRC = {0,4:X4}", u16ResponseFrameCRCTrail)
    '        'LogLnThreadSafe(strMsg)

    '        'Compare with each other
    '        If u16ResponseFrameCRCCompute <> u16ResponseFrameCRCTrail Then
    '            Return False
    '        End If

    '        refIDGCmdData.m_bIsCRCChkDone = True

    '        Return True
    '    End With
    'End Function



    '-----------------------------------------------------------------------------------------------------------------
    'Compose The Packet in Final Stage 1)Protocol Header 2) Command Set 3) Length 4) Command Frame CRC Data
    'Command Set Example
    ' If calling ping command(18-01), set u16CmdSub = &H1801
    Private Overloads Sub PktComposerCommandFrameHeaderCmdSetDataLengthCRCData(ByRef refTagIDGCmdData As tagIDGCmdData, Optional ByVal bCRCMSBFirst As Boolean = False)
        Try
            Dim strProtocol2Header As String = ClassReaderProtocolBasic.s_strProtocolV2Header
            '
            Dim nIdx As Integer

            Dim nOffCmdMain As ClassReaderProtocolBasic.EnumIDGCmdOffset = ClassReaderProtocolIDGGR.EnumIDGCmdOffset.Protocol2_OffSet_CmdMain_Len_1B
            Dim nOffCmdSub As ClassReaderProtocolBasic.EnumIDGCmdOffset = ClassReaderProtocolIDGGR.EnumIDGCmdOffset.Protocol2_OffSet_CmdSub_Len_1B
            '
            Dim nOffDataLenB1 As ClassReaderProtocolBasic.EnumIDGCmdOffset = EnumIDGCmdOffset.Protocol2_OffSet_DataLen_MSB_Len_1B
            Dim nOffDataLenB2 As ClassReaderProtocolBasic.EnumIDGCmdOffset = EnumIDGCmdOffset.Protocol2_OffSet_DataLen_LSB_Len_1B

            '
            '2017 Sept 22, Vivo Protcl V3 Support
            If (refTagIDGCmdData.bIsV3Proctl) Then
                nOffCmdMain = ClassReaderProtocolIDGGR.EnumIDGCmdOffset.Protocol3_OffSet_CmdMain_Len_1B
                nOffCmdSub = ClassReaderProtocolIDGGR.EnumIDGCmdOffset.Protocol3_OffSet_CmdSub_Len_1B
                '
                nOffDataLenB1 = EnumIDGCmdOffset.Protocol3_OffSet_DataLen_MSB_Len_1B
                nOffDataLenB2 = EnumIDGCmdOffset.Protocol3_OffSet_DataLen_LSB_Len_1B
            End If
            '
            With refTagIDGCmdData
                'Save Command Set
                '.m_u16CmdSet = u16CmdSub

                '1)Protocol Header
                For nIdx = 0 To strProtocol2Header.Count - 1
                    .m_refarraybyteData(nIdx) = Convert.ToByte(strProtocol2Header(nIdx))
                Next
                .m_refarraybyteData(9) = &H0
#If 1 Then
                '2) Command Set, (10, 11), Big Endian
                .m_refarraybyteData(nOffCmdMain) = CType((.m_u16CmdSet >> 8) And &HFF, Byte)
                .m_refarraybyteData(nOffCmdSub) = CType((.m_u16CmdSet) And &HFF, Byte)

                '3) Data Length, (12, 13), Big Endian
                .m_refarraybyteData(nOffDataLenB1) = CType((.m_nDataSizeSend >> 8) And &HFF, Byte)
                .m_refarraybyteData(nOffDataLenB2) = CType((.m_nDataSizeSend) And &HFF, Byte)

#Else
                '2) Command Set, (10, 11), Big Endian
                .m_refarraybyteData(ClassReaderProtocolIDGGR.EnumIDGCmdOffset.Protocol2_OffSet_CmdMain_Len_1B) = CType((.m_u16CmdSet >> 8) And &HFF, Byte)
                .m_refarraybyteData(ClassReaderProtocolIDGGR.EnumIDGCmdOffset.Protocol2_OffSet_CmdSub_Len_1B) = CType((.m_u16CmdSet) And &HFF, Byte)

                '3) Data Length, (12, 13), Big Endian
                .m_refarraybyteData(ClassReaderProtocolIDGGR.EnumIDGCmdOffset.Protocol2_OffSet_DataLen_MSB_Len_1B) = CType((.m_nDataSizeSend >> 8) And &HFF, Byte)
                .m_refarraybyteData(ClassReaderProtocolIDGGR.EnumIDGCmdOffset.Protocol2_OffSet_DataLen_LSB_Len_1B) = CType((.m_nDataSizeSend) And &HFF, Byte)

#End If
                '4) Command Frame CRC Data
                ClassReaderProtocolBasic.IDGCmdFrameCRCSet(.m_refarraybyteData, .m_nDataSizeSend + tagIDGCmdData.m_nPktHeader, bCRCMSBFirst)

                '5) Set Packet Length (Header + Raw Data + Trail)
                .m_nDataSizeSend = tagIDGCmdData.m_nPktHeader + .m_nDataSizeSend + tagIDGCmdData.m_nPktTrail
            End With
        Catch ex As Exception
            LogLn("[Err] PktComposerCommandFrameHeaderCmdSetDataLengthCRCData() = " + ex.Message)
        End Try
    End Sub
    '
    Public Overrides Sub Cleanup()
        MyBase.Cleanup()
        '
        If m_arraybyteSendBuf IsNot Nothing Then
            Erase m_arraybyteSendBuf
            m_arraybyteSendBuf = Nothing
        End If
    End Sub


    Private Sub ComposeRestTLVsConfigurations(ByRef dictTLV As Dictionary(Of String, tagTLV), ByRef tagIDGCmdDataInfo As tagIDGCmdData)
        Dim nIdx As Integer
        Dim tlvOp As tagTLVNewOp

        'If m_bDebug Then
        '    'FFEE04 ->TextBox14 @ Major Form
        '    nIdxEnd = Convert.ToInt16(My.Forms.m_refwinformMain.TextBox14.Text, 10)
        'End If
        '------------------[ Compose its' remains TLV properties ]------------------
        nIdx = 0
        For Each iteratorTLV In dictTLV
            tlvOp = ClassReaderCommanderParser.m_dictTLVOp_RefList_Major.Item(iteratorTLV.Key)
            tlvOp.PktComposer(iteratorTLV.Value, tagIDGCmdDataInfo)

            '----------------[ Start of Debug Section ]----------------
            'If m_bDebug Then
            '    If iteratorTLV.Value.m_nLen > 0 Then
            '        nIdx = nIdx + 1
            '        If (nIdx >= nIdxEnd) Then
            '            LogLn("[Debug] TLV has been added up to " & nIdx)
            '            Exit For
            '        End If
            '    End If
            'End If
            '----------------[ End of Debug Section ]----------------
        Next
    End Sub
    Private Sub ComposeRestTLVsConfigurations(ByRef dictTLV As SortedDictionary(Of String, tagTLV), ByRef tagIDGCmdDataInfo As tagIDGCmdData)
        Dim nIdx As Integer
        Dim tlvOp As tagTLVNewOp

        'If m_bDebug Then
        '    'FFEE04 ->TextBox14 @ Major Form
        '    nIdxEnd = Convert.ToInt16(My.Forms.m_refwinformMain.TextBox14.Text, 10)
        'End If
        '------------------[ Compose its' remains TLV properties ]------------------
        nIdx = 0
        For Each iteratorTLV In dictTLV
            tlvOp = ClassReaderCommanderParser.m_dictTLVOp_RefList_Major.Item(iteratorTLV.Key)
            tlvOp.PktComposer(iteratorTLV.Value, tagIDGCmdDataInfo)

            '----------------[ Start of Debug Section ]----------------
            'If m_bDebug Then
            '    If iteratorTLV.Value.m_nLen > 0 Then
            '        nIdx = nIdx + 1
            '        If (nIdx >= nIdxEnd) Then
            '            LogLn("[Debug] TLV has been added up to " & nIdx)
            '            Exit For
            '        End If
            '    End If
            'End If
            '----------------[ End of Debug Section ]----------------
        Next
    End Sub

    Private Sub ComposeRestTLVConfigurations(ByRef tlv As tagTLV, ByRef tagIDGCmdDataInfo As tagIDGCmdData)
        Dim tlvOp As tagTLVNewOp

        '------------------[ Compose its' remains TLV properties ]------------------
        tlvOp = ClassReaderCommanderParser.m_dictTLVOp_RefList_Major.Item(tlv.m_strTag)
        tlvOp.PktComposer(tlv, tagIDGCmdDataInfo)
    End Sub

    '
    Public Overrides Sub GRExclusive_SystemHardwareInfo(ByRef listHWInfo As List(Of String), Optional ByVal nTimeout As Integer = ClassDevRS232.m_cnTX_TIMEOUT_IN_SEC)
        '
        Dim u16Cmd As UInt16 = &H914
        Dim strFuncName As String = "GRExclusive_SystemHardwareInfo" 'System.Reflection.MethodBase.GetCurrentMethod().Name
        '
        Dim arrayByteSend As Byte() = Nothing
        Dim arrayByteRecv As Byte() = Nothing

        If (m_bIDGCmdBusy) Then
            Return
        End If
        m_bIDGCmdBusy = True

        '
        Try
            System_Customized_Command(u16Cmd, strFuncName, arrayByteSend, arrayByteRecv, AddressOf ClassReaderProtocolBasic.PreProcessCbRecv, nTimeout)

            'Its' response code = 0 => OK
            If (bIsRetStatusOK) Then
                Dim arrayByteData As Byte() = Nothing

                m_tagIDGCmdDataInfo.ParserDataGetArrayRawData(arrayByteData)
                If (arrayByteData IsNot Nothing) Then
                    ClassTableListMaster.ConvertFromArrayByte2StringsList(arrayByteData, listHWInfo)
                    ' Free Resource
                    Erase arrayByteData
                Else
                    listHWInfo.Clear()
                End If
            Else
                LogLn("[Err] " + strFuncName + "() = Response Code (" & m_tagIDGCmdDataInfo.m_nResponse & ")")
            End If

        Catch ext As TimeoutException
            LogLn("[Err] [RS232 Timeout][IDG: " + GetIDGCmdString(u16Cmd) + "] " + strFuncName + "() = " + ext.Message)
        Catch ex As Exception
            LogLn("[Err][IDG: " + GetIDGCmdString(u16Cmd) + "] " + strFuncName + "() = " + ex.Message)
        Finally
            m_bIDGCmdBusy = False
            If (arrayByteSend IsNot Nothing) Then
                Erase arrayByteSend
            End If
            If (arrayByteRecv IsNot Nothing) Then
                Erase arrayByteRecv
            End If
        End Try
        '

    End Sub
    '
   
    Public Sub GRExclusive_PT_PollInterfacePICC_Activate(ByRef o_byteCardType As Byte, ByRef o_arrayByteSerialNumUIC As Byte(), Optional strMsg1 As String = "", Optional strMsg2 As String = "", Optional ByVal nTimeout As Integer = ClassDevRS232.m_cnTX_TIMEOUT_IN_SEC)
        'x01_ACTIVATE_INTERFACE = &H1
        'x02_DEACTIVATE_INTERFACE = &H2
        'x04_ISSUE_POLL_FOR_TOKEN = &H4
        'x08_USE_INDEPENDENT_LED_INSTEAD = &H8
        'x10_USE_INDEPENDENT_BUZZER_INSTEAD = &H10
        'x20_USE_SPECIFIED_INTERFACE = &H20
        'x40_RFU = &H40
        'x80_RFU = &H80
        SystemEmvL1Cmd_CL_PT_SystemSetEnhancePassthroughMode(ENUM_EPT_SINGLE_SHOT_MODE.x01_ACTIVATE_INTERFACE Or ENUM_EPT_SINGLE_SHOT_MODE.x04_ISSUE_POLL_FOR_TOKEN Or ENUM_EPT_SINGLE_SHOT_MODE.x08_USE_INDEPENDENT_LED_INSTEAD Or ENUM_EPT_SINGLE_SHOT_MODE.x10_USE_INDEPENDENT_BUZZER_INSTEAD, ENUM_EPT_LCD_MSG_IDX.x01_PLEASE_PRESENT_CARD, ENUM_SEPM_B7_BEEP_INDICATOR.x02_BEEP_2B, ENUM_SEPM_LED_NUM.xFF_LALL, ENUM_SEPM_LED_ONOFF.x00_LED_ON, strMsg1, strMsg2, ENUM_SEPM_INTERFACE.x00_CL, o_byteCardType, o_arrayByteSerialNumUIC, nTimeout)
    End Sub


    Public Sub GRExclusive_PT_PollInterfaceSAM_Activate_Only_VP5500(ByRef o_byteCardType As Byte, ByRef o_arrayByteSerialNumUIC As Byte(), Optional strMsg1 As String = "", Optional strMsg2 As String = "", Optional ByVal bSAM1_ONLY_VP5500 As Boolean = True, Optional ByVal nTimeout As Integer = ClassDevRS232.m_cnTX_TIMEOUT_IN_SEC)
        Dim u8InterfaceFlag As ENUM_SEPM_INTERFACE

        If (bSAM1_ONLY_VP5500) Then
            u8InterfaceFlag = ENUM_SEPM_INTERFACE.x21_SAM1_ONLY_VP5500
        Else
            u8InterfaceFlag = ENUM_SEPM_INTERFACE.x22_SAM2_ONLY_VP5500
        End If

        SystemEmvL1Cmd_CL_PT_SystemSetEnhancePassthroughMode(ENUM_EPT_SINGLE_SHOT_MODE.x01_ACTIVATE_INTERFACE Or ENUM_EPT_SINGLE_SHOT_MODE.x20_USE_SPECIFIED_INTERFACE, ENUM_EPT_LCD_MSG_IDX.x0A_TERM_PRESENT_ONE_CARD_ONLY, ENUM_SEPM_B7_BEEP_INDICATOR.x02_BEEP_2B, ENUM_SEPM_LED_NUM.xFF_LALL, ENUM_SEPM_LED_ONOFF.x00_LED_ON, strMsg1, strMsg2, u8InterfaceFlag, o_byteCardType, o_arrayByteSerialNumUIC, nTimeout)
    End Sub

    Public Sub GRExclusive_PT_PollInterfaceSAM_DeActivate_Only_VP5500(ByRef o_byteCardType As Byte, ByRef o_arrayByteSerialNumUIC As Byte(), Optional strMsg1 As String = "", Optional strMsg2 As String = "", Optional ByVal bSAM1_ONLY_VP5500 As Boolean = True, Optional ByVal nTimeout As Integer = ClassDevRS232.m_cnTX_TIMEOUT_IN_SEC)
        Dim u8InterfaceFlag As ENUM_SEPM_INTERFACE

        If (bSAM1_ONLY_VP5500) Then
            u8InterfaceFlag = ENUM_SEPM_INTERFACE.x21_SAM1_ONLY_VP5500
        Else
            u8InterfaceFlag = ENUM_SEPM_INTERFACE.x22_SAM2_ONLY_VP5500
        End If

        SystemEmvL1Cmd_CL_PT_SystemSetEnhancePassthroughMode(ENUM_EPT_SINGLE_SHOT_MODE.x02_DEACTIVATE_INTERFACE Or ENUM_EPT_SINGLE_SHOT_MODE.x20_USE_SPECIFIED_INTERFACE, ENUM_EPT_LCD_MSG_IDX.x0A_TERM_PRESENT_ONE_CARD_ONLY, ENUM_SEPM_B7_BEEP_INDICATOR.x06_BEEP_1LB_400ms, ENUM_SEPM_LED_NUM.xFF_LALL, ENUM_SEPM_LED_ONOFF.x01_LED_OFF, strMsg1, strMsg2, u8InterfaceFlag, o_byteCardType, o_arrayByteSerialNumUIC, nTimeout)
    End Sub

    Public Sub GRExclusive_PT_PollInterface_Token_Activate(ByRef o_byteCardType As Byte, ByRef o_arrayByteSerialNumUIC As Byte(), Optional strMsg1 As String = "", Optional strMsg2 As String = "", Optional ByVal nTimeout As Integer = ClassDevRS232.m_cnTX_TIMEOUT_IN_SEC)
        SystemEmvL1Cmd_CL_PT_SystemSetEnhancePassthroughMode(ENUM_EPT_SINGLE_SHOT_MODE.x01_ACTIVATE_INTERFACE Or ENUM_EPT_SINGLE_SHOT_MODE.x04_ISSUE_POLL_FOR_TOKEN Or ENUM_EPT_SINGLE_SHOT_MODE.x20_USE_SPECIFIED_INTERFACE, ENUM_EPT_LCD_MSG_IDX.x0A_TERM_PRESENT_ONE_CARD_ONLY, ENUM_SEPM_B7_BEEP_INDICATOR.x02_BEEP_2B, ENUM_SEPM_LED_NUM.xFF_LALL, ENUM_SEPM_LED_ONOFF.x00_LED_ON, strMsg1, strMsg2, ENUM_SEPM_INTERFACE.x00_CL, o_byteCardType, o_arrayByteSerialNumUIC, nTimeout)
    End Sub
    Public Sub GRExclusive_PT_PollInterface_Token_DeActivate(ByRef o_byteCardType As Byte, ByRef o_arrayByteSerialNumUIC As Byte(), Optional strMsg1 As String = "", Optional strMsg2 As String = "", Optional ByVal nTimeout As Integer = ClassDevRS232.m_cnTX_TIMEOUT_IN_SEC)
        SystemEmvL1Cmd_CL_PT_SystemSetEnhancePassthroughMode(ENUM_EPT_SINGLE_SHOT_MODE.x02_DEACTIVATE_INTERFACE Or ENUM_EPT_SINGLE_SHOT_MODE.x04_ISSUE_POLL_FOR_TOKEN Or ENUM_EPT_SINGLE_SHOT_MODE.x20_USE_SPECIFIED_INTERFACE, ENUM_EPT_LCD_MSG_IDX.x0A_TERM_PRESENT_ONE_CARD_ONLY, ENUM_SEPM_B7_BEEP_INDICATOR.x06_BEEP_1LB_400ms, ENUM_SEPM_LED_NUM.xFF_LALL, ENUM_SEPM_LED_ONOFF.x01_LED_OFF, strMsg1, strMsg2, ENUM_SEPM_INTERFACE.x00_CL, o_byteCardType, o_arrayByteSerialNumUIC, nTimeout)
    End Sub

    Public Sub GRExclusvie_PT_PollForToken(ByRef o_byteCardType As Byte, ByRef o_arrayByteSerialNumUIC As Byte(), Optional ByVal nTimeout As Integer = ClassDevRS232.m_cnTX_TIMEOUT_IN_SEC)
        '
        Dim u16Cmd As UInt16 = &H2C02
        Dim strFuncName As String = "GRExclusvie_PT_PollForToken" 'System.Reflection.MethodBase.GetCurrentMethod().Name
        '
        Dim arrayByteSend As Byte() = Nothing
        Dim arrayByteRecv As Byte() = Nothing

        If (m_bIDGCmdBusy) Then
            Return
        End If
        m_bIDGCmdBusy = True

        '
        Try
            arrayByteSend = New Byte() {CType(nTimeout, Byte), &H0}


            System_Customized_Command(u16Cmd, strFuncName, arrayByteSend, arrayByteRecv, AddressOf ClassReaderProtocolBasic.PreProcessCbRecv, nTimeout, True)

            'Its' response code = 0 => OK

            If (bIsRetStatusOK) Then
                ' Byte 0 = Card Type
                o_byteCardType = arrayByteRecv(0)
                ' Byte Remained = Serial Num / UID
                o_arrayByteSerialNumUIC = New Byte(arrayByteRecv.Count - 1 - 1) {}
                Array.Copy(arrayByteRecv, 1, o_arrayByteSerialNumUIC, 0, o_arrayByteSerialNumUIC.Count)
            Else
                LogLn("[Err] " + strFuncName + "() = Response Code (" & m_tagIDGCmdDataInfo.m_nResponse & ")")
            End If

        Catch ext As TimeoutException
            LogLn("[Err] [RS232 Timeout][IDG: " + GetIDGCmdString(u16Cmd) + "] " + strFuncName + "() = " + ext.Message)
        Catch ex As Exception
            LogLn("[Err][IDG: " + GetIDGCmdString(u16Cmd) + "] " + strFuncName + "() = " + ex.Message)
        Finally
            m_bIDGCmdBusy = False
            If (arrayByteSend IsNot Nothing) Then
                Erase arrayByteSend
            End If
            If (arrayByteRecv IsNot Nothing) Then
                Erase arrayByteRecv
            End If
        End Try
        '
    End Sub

 
    
    '
    Public Sub SystemLEDControl(ByRef o_byteCardType As Byte, ByRef o_arrayByteSerialNumUIC As Byte(), Optional ByVal nTimeout As Integer = ClassDevRS232.m_cnTX_TIMEOUT_IN_SEC)
        '
        Dim u16Cmd As UInt16 = &H2C02
        Dim strFuncName As String = "GRExclusvie_PT_PollForToken" 'System.Reflection.MethodBase.GetCurrentMethod().Name
        '
        Dim arrayByteSend As Byte() = Nothing
        Dim arrayByteRecv As Byte() = Nothing

        If (m_bIDGCmdBusy) Then
            Return
        End If
        m_bIDGCmdBusy = True

        '
        Try
            arrayByteSend = New Byte() {CType(nTimeout, Byte), &H0}


            System_Customized_Command(u16Cmd, strFuncName, arrayByteSend, arrayByteRecv, AddressOf ClassReaderProtocolBasic.PreProcessCbRecv, nTimeout, True)

            'Its' response code = 0 => OK

            If (bIsRetStatusOK) Then
                ' Byte 0 = Card Type
                o_byteCardType = arrayByteRecv(0)
                ' Byte Remained = Serial Num / UID
                o_arrayByteSerialNumUIC = New Byte(arrayByteRecv.Count - 1 - 1) {}
                Array.Copy(arrayByteRecv, 1, o_arrayByteSerialNumUIC, 0, o_arrayByteSerialNumUIC.Count)
            Else
                LogLn("[Err] " + strFuncName + "() = Response Code (" & m_tagIDGCmdDataInfo.m_nResponse & ")")
            End If

        Catch ext As TimeoutException
            LogLn("[Err] [RS232 Timeout][IDG: " + GetIDGCmdString(u16Cmd) + "] " + strFuncName + "() = " + ext.Message)
        Catch ex As Exception
            LogLn("[Err][IDG: " + GetIDGCmdString(u16Cmd) + "] " + strFuncName + "() = " + ex.Message)
        Finally
            m_bIDGCmdBusy = False
            If (arrayByteSend IsNot Nothing) Then
                Erase arrayByteSend
            End If
            If (arrayByteRecv IsNot Nothing) Then
                Erase arrayByteRecv
            End If
        End Try
        '
    End Sub
    '
    Public Overrides Sub SystemEmvL1Cmd_CL_001_Carrier(Optional ByVal bCarrier As Boolean = True, Optional ByVal nTimeout As Integer = ClassDevRS232.m_cnTX_TIMEOUT_IN_SEC)
        Dim u16Cmd As UInt16 = &HFD01
        Dim strFuncName As String = "SystemEmvL1Cmd_CL_001_Carrier"
        Dim arrayByteSend As Byte() = Nothing
        Dim arrayByteRecv As Byte() = Nothing
        '
        If (m_bIDGCmdBusy) Then
            Return
        End If
        m_bIDGCmdBusy = True

        Try
            If (bCarrier) Then
                arrayByteSend = New Byte() {&H1}
            Else
                arrayByteSend = New Byte() {&H0}
            End If


            System_Customized_Command(u16Cmd, strFuncName, arrayByteSend, arrayByteRecv, AddressOf ClassReaderProtocolBasic.PreProcessCbRecv, nTimeout)
            'Its' response code = 0 => OK
            If (bIsRetStatusOK) Then

            Else
                LogLn("[Err] " + strFuncName + "() = Response Code (" & m_tagIDGCmdDataInfo.m_nResponse & ") --> " + m_dictStatusCode2.Item(m_tagIDGCmdDataInfo.m_nResponse))
            End If

        Catch ext As TimeoutException
            LogLn("[Err][Timeout] [IDG: " + GetIDGCmdString(u16Cmd) + "] " + strFuncName + "() = " + ext.Message)
        Catch ex As Exception
            LogLn("[Err] [IDG: " + GetIDGCmdString(u16Cmd) + "] " + strFuncName + "() = " + ex.Message)
        Finally
            m_bIDGCmdBusy = False
            If (arrayByteSend IsNot Nothing) Then
                Erase arrayByteSend
            End If
            If (arrayByteRecv IsNot Nothing) Then
                Erase arrayByteRecv
            End If
        End Try
        '
    End Sub
    '
#If 0 Then

    Public Shared Sub m_pfuncCbComposer(ByRef objTLV As tagTLV, ByRef idgCmdData As tagIDGCmdData)
        Try
            Dim arrayByteData() As Byte = idgCmdData.m_refarraybyteData
            Dim arrayByteTag() As Byte = Nothing
            ' If TLV Length = 0, In Debug Mode Sending Out Warning Message
            If objTLV.m_nLen = 0 Then
                LogLn("[Warning] Tag = " + objTLV.m_strTag + " , Length = 0 (Unusable)")
                Return
            End If

            'Put Group Number Here
            With idgCmdData
                'Dim nPosStart As Integer = tagIDGCmdData.m_nPktHeader + .m_nDataSendOff
                Dim nPosStart As Integer = tagIDGCmdData.m_nPktHeader + .m_nDataSizeSend

                'Tag
                ClassTableListMaster.TLVauleFromString2ByteArray(objTLV.m_strTag, arrayByteTag)
                If (arrayByteTag Is Nothing) Then
                    Throw New Exception("Can't Translate Tag String " + objTLV.m_strTag + " To Byte Array")
                End If
                arrayByteTag.CopyTo(arrayByteData, nPosStart)
                ' .m_nDataSendOff = .m_nDataSendOff + arrayByteTag.Count
                nPosStart = nPosStart + arrayByteTag.Count
                'Length
                arrayByteData(nPosStart) = CType(objTLV.m_nLen, Byte)
                nPosStart = nPosStart + 1
                'Value / No Value if Length = &H00
                If objTLV.m_arrayTLVal IsNot Nothing Then
                    objTLV.m_arrayTLVal.CopyTo(arrayByteData, nPosStart)
                    nPosStart = nPosStart + objTLV.m_arrayTLVal.Count
                Else
                    nPosStart = nPosStart
                End If

                '.m_nDataSendOff = nPosStart - tagIDGCmdData.m_nPktHeader
                .m_nDataSizeSend = nPosStart - tagIDGCmdData.m_nPktHeader
            End With
        Catch ex As Exception
            LogLn("[Err] Common TLV Callback Packet Composer () = " + ex.Message)
        End Try
    End Sub

      Public Overrides Sub SetConfigurationsGroupMass()
        m_bIsRetStatusOK = False
        If (m_bIDGCmdBusy) Then
            Return
        End If
        m_bIDGCmdBusy = True
        Try

            ' Get All Configurations Text File Group Data and Put it into 
            Dim iteratorGroup As KeyValuePair(Of Integer, tagConfigCmdUnitGroup)
            Dim configGroup As tagConfigCmdUnitGroup = New tagConfigCmdUnitGroup
            Dim idgCmdResp As tagIDGCmdData = New tagIDGCmdData
            Dim dictGroupList As Dictionary(Of Integer, tagConfigCmdUnitGroup) = m_refobjCTLM.m_dictGroupList
            Dim nIdx As Integer = 0
            Dim nIdxEnd As Integer = dictGroupList.Count - 1
            '
            If dictGroupList.Count < 1 Then
                Throw New Exception("[Err] SetConfigurationsGroupMass() = No Group Data Available")
            End If

            '--------------------[ Prepare IDG Command Stuffs ]----------------------------
            idgCmdResp.m_refarraybyteData = m_arraybyteSendBuf
            nIdx = 0
            For Each iteratorGroup In dictGroupList
                configGroup = iteratorGroup.Value
                'Note : While nIdx = nIdxEnd, it's the final group data, so we send it..
                SetConfigurationsGroupSingle(configGroup)
                '
                nIdx = nIdx + 1
            Next

            'Cleanup
            configGroup.Cleanup()

            idgCmdResp.Cleanup()

        Catch ex As Exception
            LogLn("[Err] SetConfigurationsGroupSingle() = " + ex.Message)
        Finally
            m_bIDGCmdBusy = False
        End Try
        '
    End Sub

  '======================================[ IDG CMD:_2C-01, Set Passthrough Mode ]======================================

    'IDG CMD: _0307
    Public Overrides Sub GetConfigurationsGroupMass(ByRef o_configGroupList As Dictionary(Of Integer, tagConfigCmdUnitGroup), Optional ByVal nTimeout As Integer = ClassDevRS232.m_cnTX_TIMEOUT_IN_SEC)
        Dim u16Cmd As UInt16 = &H307
        Dim strFuncName As String = "GetConfigurationsGroupMass"

        m_bIsRetStatusOK = False
        If (m_bIDGCmdBusy) Then
            Return
        End If
        m_bIDGCmdBusy = True
        '
        Try
            '
            m_bIsRetStatusOK = False
            '
            m_tagIDGCmdDataInfo.Reinit(u16Cmd, m_arraybyteSendBuf, Me)
            '-----------------------------------------------------------------[ Send to reader ]-----------------------------------------------------------------
            ''Compose The IDG Packet 03-07, Not Command Frame Data, No Response Frame Data
            'arrayByteData = m_tagIDGCmdDataInfo.m_refarraybyteData
            ''Composer The Packet with Group ID Info
            'tlvOp = m_refdicttlvConfigurationsTLVOp.Item(tagTLV.TAG_FFE4)
            ''Make a TLV with Group ID = 01
            'tlvTmp = New tagTLV(tagTLV.TAG_FFE4, 1, "01")
            'tlvOp.PktComposer(tlvTmp, m_tagIDGCmdDataInfo)

            'Compose 1)Protocol Header 2) Command Set 3) Length 4) Command Frame CRC Data
            PktComposerCommandFrameHeaderCmdSetDataLengthCRCData(m_tagIDGCmdDataInfo)
            'tlvTmp.Cleanup()

            '----------------[ Debug Section ]----------------
            If (m_bDebug) Then
                'Note: After PktComposerCommandFrameHeaderCmdSetDataLengthCRCData(), m_tagIDGCmdDataInfo.m_nDataSizeSend includes m_nPktHeader + m_nPktTrail
                'Form01_Main.LogLnDumpData(m_tagIDGCmdDataInfo.m_refarraybyteData, tagIDGCmdData.m_nPktHeader + m_tagIDGCmdDataInfo.m_nDataSizeSend + tagIDGCmdData.m_nPktTrail)
                Form01_Main.LogLnDumpData(m_tagIDGCmdDataInfo.m_refarraybyteData, m_tagIDGCmdDataInfo.m_nDataSizeSend)
            End If
            '----------------[ End Debug Section ]----------------

            'Waiting For Response in GetConfigurationsGroupSingleCbRecv()
            m_refdevConn.SendCommandFrameAndWaitForResponseFrame(AddressOf ClassReaderProtocolBasic.PreProcessCbRecv, m_tagIDGCmdDataInfo, m_nIDGCommanderType, True, nTimeout * 1000)


            'Its' response code = 0 => OK
            If (m_tagIDGCmdDataInfo.bIsResponseOK) Then
                m_bIsRetStatusOK = True

                'Parser the Group Info
                ClassReaderCommanderParser.Config_ParseFromReaderGroupInfo(m_tagIDGCmdDataInfo, o_configGroupList)
            Else
                m_bIsRetStatusOK = False
            End If
        Catch ext As TimeoutException
            LogLn("[Err][RS232 Timeout] [IDG: " + GetIDGCmdString(u16Cmd) + "] " + strFuncName + "() = " + ext.Message + ",Group List Count= " & o_configGroupList.Count)
        Catch ex As Exception
            LogLn("[Err] [IDG:  " + GetIDGCmdString(u16Cmd) + "] " + strFuncName + "() = " + ex.Message)
        Finally
            If (Not m_bIsRetStatusOK) And (o_configGroupList IsNot Nothing) Then
                'If Failed then clean up the created group list
                ClassTableListMaster.CleanupConfigurationsGroupList(o_configGroupList)
            End If
            m_bIDGCmdBusy = False
        End Try
        '

    End Sub


   '
    Private m_tagCRLInfo As ClassReaderCommanderParser.GetCRLRecNumInfo

    ' OUTPUT: o_dictCRLCmdAddList = ClassTableListMaster.m_dictCARLCmdAddList
    Public Overrides Sub GetCRLMass(ByRef o_dictCRLCmdAddList As Dictionary(Of String, tagCRLCmdUnit), Optional ByVal nTimeout As Integer = ClassDevRS232.m_cnTX_TIMEOUT_IN_SEC)
        Dim u16DataLen As UInt16
        Static Dim u16CRLRetInfoSize As UInt16 = 12 ' = Rec Num : 4 bytes + Rec Num Rest : 4 bytes + Rec Size : 4 bytes
        Static Dim u16SingleCRLRecSize As UInt16 = 9 ' = RID : 4 bytes + Index : 1 byte + Serial No : 3 bytes
        Dim u16NumOfCRLRec As UInt16 = 100 ' For Default Size
        Dim u16CLRIdx As UInt16 = 0

        Dim u16Cmd As UInt16 = &H8407
        Dim strFuncName As String = "GetCRLMass"
        '
        m_bIsRetStatusOK = False
        If (m_bIDGCmdBusy) Then
            Return
        End If
        m_bIDGCmdBusy = True
        '
        Try
            m_tagIDGCmdDataInfo.Reinit(u16Cmd, m_arraybyteSendBuf, Me)

            'Compose Get CRL Command Frame
            ' 1. Acceptable Data Length: 2 bytes, Big Endian = (Single CRL Record Size( RID:5 bytes+Index:1 byte+SerialNo: 3 bytes = 9 bytes)) * (Number of Records)
            u16DataLen = u16SingleCRLRecSize * u16NumOfCRLRec + u16CRLRetInfoSize
            m_tagIDGCmdDataInfo.ComposerAddData(u16DataLen)

            '2. Start Index of CRL Record  0-base Index: 2 bytes, Big Endian
            m_tagIDGCmdDataInfo.ComposerAddData(u16CLRIdx)
            '-----------------------------------------------------------------[ Send to reader ]-----------------------------------------------------------------
            'Compose 1)Protocol Header 2) Command Set 3) Length 4) Command Frame CRC Data
            PktComposerCommandFrameHeaderCmdSetDataLengthCRCData(m_tagIDGCmdDataInfo)

            '----------------[ Debug Section ]----------------
            If (m_bDebug) Then
                'Note: After PktComposerCommandFrameHeaderCmdSetDataLengthCRCData(), m_tagIDGCmdDataInfo.m_nDataSizeSend includes m_nPktHeader + m_nPktTrail
                'Form01_Main.LogLnDumpData(m_tagIDGCmdDataInfo.m_refarraybyteData, tagIDGCmdData.m_nPktHeader + m_tagIDGCmdDataInfo.m_nDataSizeSend + tagIDGCmdData.m_nPktTrail)
                Form01_Main.LogLnDumpData(m_tagIDGCmdDataInfo.m_refarraybyteData, m_tagIDGCmdDataInfo.m_nDataSizeSend)
            End If

            '----------------[ End Debug Section ]----------------
            'Waiting For Response in SetConfigurationsGroupSingleCbRecv()
            m_refdevConn.SendCommandFrameAndWaitForResponseFrame(AddressOf ClassReaderProtocolBasic.PreProcessCbRecv, m_tagIDGCmdDataInfo, m_nIDGCommanderType, True, nTimeout * 1000)


            'Its' response code = 0 => OK
            If (m_tagIDGCmdDataInfo.bIsResponseOK) Then

                '
                ClassReaderCommanderParser.CRL_GetCRLMassFromResponse(m_tagIDGCmdDataInfo, m_refobjCTLM.m_dictCRLCmdAddList, m_tagCRLInfo)
                '
                If m_refobjCTLM.m_dictCRLCmdAddList Is Nothing Then
                Else
                    If (m_refobjCTLM.m_dictCRLCmdAddList.Count > 0) Then
                        m_bIsRetStatusOK = True
                        LogLn("[Info] GetCRLMass() = (Num of CRL Rec, Num of CRL Single Size) = (" & m_tagCRLInfo.u32RecNumRead & ", " & m_tagCRLInfo.u32SingleRecSize & ")")
                    Else
                        LogLn("[Warning] GetCRLMass() =  Num Of CRL Rec = 0, Check out the problem")
                    End If
                End If
            Else
                LogLn("[Warnig] Response Code = " + m_tagIDGCmdDataInfo.strGetResponseMsg + " ( " & m_tagIDGCmdDataInfo.byteGetResponseCode & " )")
            End If
        Catch ext As TimeoutException
            LogLn("[Err][RS232 Timeout] [IDG: " + GetIDGCmdString(u16Cmd) + "] " + strFuncName + "() = " + ext.Message)
        Catch ex As Exception
            LogLn("[Err] [IDG:  " + GetIDGCmdString(u16Cmd) + "] " + strFuncName + "() = " + ex.Message)
        Finally
            If Not m_bIsRetStatusOK Then
                If m_tagIDGCmdDataInfo.m_nDataSizeRecv > 0 Then
                    LogLn("------------[ Recv Packet Dumping ]------------")
                    Form01_Main.LogLnDumpData(m_tagIDGCmdDataInfo.m_refarraybyteData, m_tagIDGCmdDataInfo.m_nDataSizeRecv)
                Else
                    LogLn("------------[ Recv Packet Dumping N/A, Recv Size = 0 ]------------")
                End If
            End If
            m_bIDGCmdBusy = False
        End Try
        '
    End Sub

  'IDG CMD:_03-05
    Public Overrides Sub GetConfigurationsAIDMass(ByRef o_AIDList As Dictionary(Of String, tagConfigCmdUnitAID), Optional ByVal nTimeout As Integer = ClassDevRS232.m_cnTX_TIMEOUT_IN_SEC)
        Dim u16Cmd As UInt16 = &H305
        Dim strFuncName As String = "GetConfigurationsAIDMass"

        m_bIsRetStatusOK = False
        If (m_bIDGCmdBusy) Then
            Return
        End If
        m_bIDGCmdBusy = True
        Try
            '
            m_bIsRetStatusOK = False
            '
            m_tagIDGCmdDataInfo.Reinit(u16Cmd, m_arraybyteSendBuf, Me)
            '-----------------------------------------------------------------[ Send to reader ]-----------------------------------------------------------------
            ''Compose The IDG Packet 03-05, Not Command Frame Data, No Response Frame Data
            'arrayByteData = m_tagIDGCmdDataInfo.m_refarraybyteData
            ''Composer The Packet with Group ID Info
            'tlvOp = m_refdicttlvConfigurationsTLVOp.Item(tagTLV.TAG_FFE4)
            ''Make a TLV with Group ID = 01
            'tlvTmp = New tagTLV(tagTLV.TAG_FFE4, 1, "01")
            'tlvOp.PktComposer(tlvTmp, m_tagIDGCmdDataInfo)

            'Compose 1)Protocol Header 2) Command Set 3) Length 4) Command Frame CRC Data
            PktComposerCommandFrameHeaderCmdSetDataLengthCRCData(m_tagIDGCmdDataInfo)
            'tlvTmp.Cleanup()

            '----------------[ Debug Section ]----------------
            If (m_bDebug) Then
                'Note: After PktComposerCommandFrameHeaderCmdSetDataLengthCRCData(), m_tagIDGCmdDataInfo.m_nDataSizeSend includes m_nPktHeader + m_nPktTrail
                'Form01_Main.LogLnDumpData(m_tagIDGCmdDataInfo.m_refarraybyteData, tagIDGCmdData.m_nPktHeader + m_tagIDGCmdDataInfo.m_nDataSizeSend + tagIDGCmdData.m_nPktTrail)
                Form01_Main.LogLnDumpData(m_tagIDGCmdDataInfo.m_refarraybyteData, m_tagIDGCmdDataInfo.m_nDataSizeSend)
            End If
            '----------------[ End Debug Section ]----------------

            'Waiting For Response in GetConfigurationsGroupSingleCbRecv()
            m_refdevConn.SendCommandFrameAndWaitForResponseFrame(AddressOf ClassReaderProtocolBasic.PreProcessCbRecv, m_tagIDGCmdDataInfo, m_nIDGCommanderType, True, nTimeout * 1000)

            'Its' response code = 0 => OK
            If (m_tagIDGCmdDataInfo.bIsResponseOK) Then
                m_bIsRetStatusOK = True

                'Parser the Group Info
                'Form01_Main.LogLnDumpData(m_tagIDGCmdDataInfo.m_refarraybyteData, m_tagIDGCmdDataInfo.m_nDataSizeRecv)
                ClassReaderCommanderParser.Config_ParseFromReaderAIDInfo(m_tagIDGCmdDataInfo, o_AIDList)
            Else
                m_bIsRetStatusOK = False
            End If
        Catch ext As TimeoutException
            LogLn("[Err][RS232 Timeout] [IDG: " + GetIDGCmdString(u16Cmd) + "] " + strFuncName + "() = " + ext.Message + ",AID List Count= " & o_AIDList.Count)
        Catch ex As Exception
            LogLn("[Err] [IDG:  " + GetIDGCmdString(u16Cmd) + "] " + strFuncName + "() = " + ex.Message)
        Finally
            If (Not m_bIsRetStatusOK) And (o_AIDList IsNot Nothing) Then
                'If Failed then clean up the created group list
                ClassTableListMaster.CleanupConfigurationsAIDList(o_AIDList)
            End If
            m_bIDGCmdBusy = False
        End Try
        '

    End Sub

   '
    Public Overrides Sub SetCAPublicKeySingle(ByRef tagCAPKInfo As ClassCAPKUnit, Optional ByVal nTimeout As Integer = ClassDevRS232.m_cnTX_TIMEOUT_IN_SEC)
        Dim iteratorTLV As KeyValuePair(Of String, tagTLV)
        Dim dictTLV As Dictionary(Of String, tagTLV) = Nothing
        'Dim tlvOp As tagTLVNewOp
        'Dim tlvTmp As tagTLV
        Dim u16Cmd As UInt16 = &HD003
        Dim strFuncName As String = "SetCAPublicKeySingle"
        '
        m_bIsRetStatusOK = False
        If (m_bIDGCmdBusy) Then
            Return
        End If
        m_bIDGCmdBusy = True
        Try
            'Create Set CA Public Key - Single Information
            m_tagIDGCmdDataInfo.Reinit(u16Cmd, m_arraybyteSendBuf, Me)

            '=======================[ Composing Send Out Data for D0-03, Referenced from IDG 1.2.9 ]=======================
            'M 1) RID, 5 bytes
            'M 2) Key Index, 1 byte
            'M 3) Data as the following 0~n bytes
            '0) Hash Algorithm, 1 byte, The only algorithm supported is SHA-1. The value is set to 01h
            '1) Public Key Algorithm, 1 byte, The encryption algorithm in which this key is used. Currently support only one type: RSA. The value is set to 01h
            '3-22) Checksum/Hash, 20 bytes, Checksum which is calculated using SHA-1 over the following fields: RID & KeyIndex & Modulus & Exponent where the exponent is either one byte or 3 bytes (although we store it in a 4 byte field)
            '23-26) Public Key Exponent, 4 bytes, Actually, the real length of the exponent is either one byte or 3 bytes. It can have two values: 3, or 65537.
            '27-28) Modulus Length, 2 bytes, Indicates the length of the next field.
            '29-n) Modulus, Variable bytes, This is the modulus field of the public key. Its length is specified in the field above.
            ComposeRestTLVsCAPK(tagCAPKInfo.m_dictTLV_CAPKProperties, m_tagIDGCmdDataInfo)
            '-----------------------------------------------------------------[ Send to reader ]-----------------------------------------------------------------
            'Compose 1)Protocol Header 2) Command Set 3) Length 4) Command Frame CRC Data
            PktComposerCommandFrameHeaderCmdSetDataLengthCRCData(m_tagIDGCmdDataInfo)

            '----------------[ Debug Section ]----------------
            If (m_bDebug) Then
                Form01_Main.LogLnDumpData(m_tagIDGCmdDataInfo.m_refarraybyteData, m_tagIDGCmdDataInfo.m_nDataSizeSend)
            End If
            '----------------[ End Debug Section ]----------------

            'Waiting For Response in SetConfigurationsGroupSingleCbRecv()
            m_refdevConn.SendCommandFrameAndWaitForResponseFrame(AddressOf ClassReaderProtocolBasic.PreProcessCbRecv, m_tagIDGCmdDataInfo, m_nIDGCommanderType, True, nTimeout * 1000)

            'Its' response code = 0 => OK
            If (m_tagIDGCmdDataInfo.bIsResponseOK) Then
                m_bIsRetStatusOK = True
            Else
                m_bIsRetStatusOK = False
            End If
        Catch ext As TimeoutException
            LogLn("[Err][RS232 Timeout][IDG: " + GetIDGCmdString(u16Cmd) + "] " + strFuncName + "() = " + ext.Message + ",RID + Key Index = " + tagCAPKInfo.strRID + " " + tagCAPKInfo.strIndex)
        Catch ex As Exception
            LogLn("[Err] [IDG: " + GetIDGCmdString(u16Cmd) + "] " + strFuncName + "() = " + ex.Message)
        Finally
            If dictTLV IsNot Nothing Then
                For Each iteratorTLV In dictTLV
                    iteratorTLV.Value.Cleanup()
                Next
                'Clean up stuffs
                dictTLV.Clear()
            End If
            m_bIDGCmdBusy = False
        End Try
        '
    End Sub

  '
    'INPUT: arrayByteRID = { &HA0, &H00, &H00, &H0E, &H60, &H80 }
    'OUTPUT: RID Key Indexes List = { &H00, &H10, &H12, &H0F }
    Public Overrides Sub GetCAPublicKeyListIDMass(ByVal arrayByteRID() As Byte, ByRef o_RIDKeyList() As Byte, Optional ByVal nTimeout As Integer = ClassDevRS232.m_cnTX_TIMEOUT_IN_SEC)
        Dim u16Cmd As UInt16 = &HD007
        Dim strFuncName As String = "GetCAPublicKeyListIDMass"
        '
        m_bIsRetStatusOK = False
        If (m_bIDGCmdBusy) Then
            Return
        End If
        m_bIDGCmdBusy = True
        Try
            'Send D0-06 And Get Return Value
            m_tagIDGCmdDataInfo.Reinit(u16Cmd, m_arraybyteSendBuf, Me)

            'Put RID As Input Parameter, 5 bytes in length
            Array.Copy(arrayByteRID, 0, m_arraybyteSendBuf, tagIDGCmdData.m_nPktHeader, arrayByteRID.Count)
            m_tagIDGCmdDataInfo.m_nDataSizeSend = m_tagIDGCmdDataInfo.m_nDataSizeSend + arrayByteRID.Count

            '-----------------------------------------------------------------[ Send to reader ]-----------------------------------------------------------------
            'Compose 1)Protocol Header 2) Command Set 3) Length 4) Command Frame CRC Data
            PktComposerCommandFrameHeaderCmdSetDataLengthCRCData(m_tagIDGCmdDataInfo)

            '----------------[ Debug Section ]----------------
            If (m_bDebug) Then
                'Note: After PktComposerCommandFrameHeaderCmdSetDataLengthCRCData(), m_tagIDGCmdDataInfo.m_nDataSizeSend includes m_nPktHeader + m_nPktTrail
                'Form01_Main.LogLnDumpData(m_tagIDGCmdDataInfo.m_refarraybyteData, tagIDGCmdData.m_nPktHeader + m_tagIDGCmdDataInfo.m_nDataSizeSend + tagIDGCmdData.m_nPktTrail)
                Form01_Main.LogLnDumpData(m_tagIDGCmdDataInfo.m_refarraybyteData, m_tagIDGCmdDataInfo.m_nDataSizeSend)
            End If

            '----------------[ End Debug Section ]----------------
            'Waiting For Response in SetConfigurationsGroupSingleCbRecv()
            m_refdevConn.SendCommandFrameAndWaitForResponseFrame(AddressOf ClassReaderProtocolBasic.PreProcessCbRecv, m_tagIDGCmdDataInfo, m_nIDGCommanderType, True, nTimeout * 1000)

            'Its' response code = 0 => OK
            If (m_tagIDGCmdDataInfo.bIsResponseOK) Then
                m_bIsRetStatusOK = True
                '
                'Get RID, 5 bytes per RID, Put into o_RIDList
                ClassReaderCommanderParser.CAPK_GetCAPublicKeyListFromResponse(m_tagIDGCmdDataInfo, o_RIDKeyList)

            Else
                m_bIsRetStatusOK = False
            End If
        Catch ext As TimeoutException
            Dim strRID As String = ""
            ClassTableListMaster.ConvertFromArrayByte2String(arrayByteRID, strRID)
            LogLn("[Err] [RS232 Timeout] [IDG: " + GetIDGCmdString(u16Cmd) + "] " + strFuncName + "() = " + ext.Message + ",RID = " + strRID)
        Catch ex As Exception
            LogLn("[Err] [IDG: " + GetIDGCmdString(u16Cmd) + "] " + strFuncName + "() = " + ex.Message)
        Finally
            m_bIDGCmdBusy = False
        End Try
        '
    End Sub


    Public Overrides Sub GetCAPublicKeyRIDSingle(ByVal arrayByteRID() As Byte, ByVal byteKeyIndex As Byte, ByRef o_tagCAPKInfo As ClassCAPKUnit, Optional ByVal nTimeout As Integer = ClassDevRS232.m_cnTX_TIMEOUT_IN_SEC)
        'Put RID+(Key) Index into IDG Packet Data Fields

        'Send  D0-01 And Resolve Response as Following Format
        '        Status = 00 if successful.
        'When the status is successful (0), the data field contains:
        'RID (5 bytes)
        'Key Index (1 byte)
        'Key Hash Algorithm (1 Byte) - 01h = SHA-1
        'Key Encryption Algorithm (1 Byte) – 01h = RSA
        'Checksum – This Checksum is calculated with a concatenation of:
        'RID & KeyIndex & Modulus & Exponent
        'where the exponent is either one byte or 3 bytes
        'Modulus Length (2 bytes)
        'Modulus (varies in length)
        Dim nRIDLen As Integer = 5
        Dim u16Cmd As UInt16 = &HD001
        Dim strFuncName As String = ""

        '
        m_bIsRetStatusOK = False
        If (m_bIDGCmdBusy) Then
            Return
        End If
        m_bIDGCmdBusy = True
        Try
            '
            'Send D0-06 And Get Return Value
            m_tagIDGCmdDataInfo.Reinit(u16Cmd, m_arraybyteSendBuf, Me)

            'Put RID As Input Parameter, 5 bytes in length
            Array.Copy(arrayByteRID, 0, m_arraybyteSendBuf, tagIDGCmdData.m_nPktHeader, nRIDLen)
            m_tagIDGCmdDataInfo.m_nDataSizeSend = m_tagIDGCmdDataInfo.m_nDataSizeSend + nRIDLen

            'Put RID's Key Index (1 Byte)
            m_arraybyteSendBuf(tagIDGCmdData.m_nPktHeader + m_tagIDGCmdDataInfo.m_nDataSizeSend) = byteKeyIndex
            m_tagIDGCmdDataInfo.m_nDataSizeSend = m_tagIDGCmdDataInfo.m_nDataSizeSend + 1
            '-----------------------------------------------------------------[ Send to reader ]-----------------------------------------------------------------
            'Compose 1)Protocol Header 2) Command Set 3) Length 4) Command Frame CRC Data
            PktComposerCommandFrameHeaderCmdSetDataLengthCRCData(m_tagIDGCmdDataInfo)

            '----------------[ Debug Section ]----------------
            If (m_bDebug) Then
                'Note: After PktComposerCommandFrameHeaderCmdSetDataLengthCRCData(), m_tagIDGCmdDataInfo.m_nDataSizeSend includes m_nPktHeader + m_nPktTrail
                'Form01_Main.LogLnDumpData(m_tagIDGCmdDataInfo.m_refarraybyteData, tagIDGCmdData.m_nPktHeader + m_tagIDGCmdDataInfo.m_nDataSizeSend + tagIDGCmdData.m_nPktTrail)
                Form01_Main.LogLnDumpData(m_tagIDGCmdDataInfo.m_refarraybyteData, m_tagIDGCmdDataInfo.m_nDataSizeSend)
            End If

            '----------------[ End Debug Section ]----------------
            'Waiting For Response in SetConfigurationsGroupSingleCbRecv()
            m_refdevConn.SendCommandFrameAndWaitForResponseFrame(AddressOf ClassReaderProtocolBasic.PreProcessCbRecv, m_tagIDGCmdDataInfo, m_nIDGCommanderType, True, nTimeout * 1000)

            'Its' response code = 0 => OK
            If (m_tagIDGCmdDataInfo.bIsResponseOK) Then
                m_bIsRetStatusOK = True
                '
                'Get RID, 5 bytes per RID, Put into o_RIDList
                ClassReaderCommanderParser.CAPK_GetCAPublicKeyRIDSingleDetailFromResponse(m_tagIDGCmdDataInfo, arrayByteRID, byteKeyIndex, o_tagCAPKInfo)
            Else
                m_bIsRetStatusOK = False
                LogLn("[Err] [IDG: " + GetIDGCmdString(u16Cmd) + "] " + strFuncName + "() =  Response Code (" & m_tagIDGCmdDataInfo.m_nResponse & ") --> " + m_dictStatusCodeCAPKRIDKeyIndex.Item(m_tagIDGCmdDataInfo.m_nResponse))
                '
                If o_tagCAPKInfo Is Nothing Then
                    o_tagCAPKInfo = New ClassCAPKUnit
                End If
            End If
            '----------------[ Put RID(5 bytes) & Key Index into CAPK Info ]----------------
            'Dim tlv As tagTLV
            'tlv = New tagTLV(TAG_CAPK_UNIT.m_strCAPK02RID, 5, arrayByteRID)
            'o_tagCAPKInfo.m_dictTLV_CAPKProperties.Add(TAG_CAPK_UNIT.m_strCAPK02RID, tlv)
            'tlv = New tagTLV(TAG_CAPK_UNIT.m_strCAPK03Index, byteKeyIndex)
            'o_tagCAPKInfo.m_dictTLV_CAPKProperties.Add(TAG_CAPK_UNIT.m_strCAPK03Index, tlv)

        Catch ext As TimeoutException
            Dim strRIDKeyIndex As String = ""
            ClassTableListMaster.ConvertFromArrayByte2String(arrayByteRID, strRIDKeyIndex)
            strRIDKeyIndex = strRIDKeyIndex + " " + String.Format("{0,2:X2}", byteKeyIndex)
            LogLn("[Err] [RS232 Timeout] [IDG: " + GetIDGCmdString(u16Cmd) + "] " + strFuncName + "() = " + ext.Message + ",RID+ Key Index = " + strRIDKeyIndex)
        Catch ex As Exception
            LogLn("[Err] [IDG: " + GetIDGCmdString(u16Cmd) + "] " + strFuncName + "() = " + ex.Message)
        Finally
            m_bIDGCmdBusy = False
        End Try
        '
    End Sub


  '------------------------------------------------------------------------[IDG CMD:_D0-06 ]------------------------------------------------------------------------
    Private Shared Sub GetCAPublicKeyGetAllRIDsCbRecv(ByRef devSerialPort As SerialPort, ByRef o_IDGCmdData As tagIDGCmdData, ByVal bRS232CommOK As Boolean)
        Try
            ' Common Process to receive data 
            PreProcessCbRecv(devSerialPort, o_IDGCmdData, bRS232CommOK)
            '
        Catch ex As Exception
            LogLnThreadSafe("[Err] GetCAPublicKeyGetAllRIDsCbRecv() = " + ex.Message)
        End Try
    End Sub

    Public Overrides Sub GetCAPublicKeyGetAllRIDs(ByRef o_RIDList As List(Of Byte()), Optional ByVal nTimeout As Integer = ClassDevRS232.m_cnTX_TIMEOUT_IN_SEC)
        Dim u16Cmd As UInt16 = &HD006
        Dim strFuncName As String = "GetCAPublicKeyGetAllRIDs"
        '
        m_bIsRetStatusOK = False
        If (m_bIDGCmdBusy) Then
            Return
        End If
        m_bIDGCmdBusy = True
        Try
            'Send D0-06 And Get Return Value
            m_tagIDGCmdDataInfo.Reinit(u16Cmd, m_arraybyteSendBuf, Me)

            'Compose Rest TLVs
            'ComposeRestTLVs(dictTLV, m_tagIDGCmdDataInfo)

            '-----------------------------------------------------------------[ Send to reader ]-----------------------------------------------------------------
            'Compose 1)Protocol Header 2) Command Set 3) Length 4) Command Frame CRC Data
            PktComposerCommandFrameHeaderCmdSetDataLengthCRCData(m_tagIDGCmdDataInfo)

            '----------------[ Debug Section ]----------------
            If (m_bDebug) Then
                'Note: After PktComposerCommandFrameHeaderCmdSetDataLengthCRCData(), m_tagIDGCmdDataInfo.m_nDataSizeSend includes m_nPktHeader + m_nPktTrail
                'Form01_Main.LogLnDumpData(m_tagIDGCmdDataInfo.m_refarraybyteData, tagIDGCmdData.m_nPktHeader + m_tagIDGCmdDataInfo.m_nDataSizeSend + tagIDGCmdData.m_nPktTrail)
                Form01_Main.LogLnDumpData(m_tagIDGCmdDataInfo.m_refarraybyteData, m_tagIDGCmdDataInfo.m_nDataSizeSend)
            End If

            '----------------[ End Debug Section ]----------------
            'Waiting For Response in SetConfigurationsGroupSingleCbRecv()
            m_refdevConn.SendCommandFrameAndWaitForResponseFrame(AddressOf ClassReaderProtocolBasic.PreProcessCbRecv, m_tagIDGCmdDataInfo, m_nIDGCommanderType, True, nTimeout * 1000)



            'Its' response code = 0 => OK
            If (m_tagIDGCmdDataInfo.bIsResponseOK) Then
                m_bIsRetStatusOK = True
                '
                'Get RID, 5 bytes per RID, Put into o_RIDList
                ClassReaderCommanderParser.CAPK_GetCAPublicKeyGetAllRIDsFromResponse(m_tagIDGCmdDataInfo, o_RIDList)
            Else
                m_bIsRetStatusOK = False
            End If
        Catch ext As TimeoutException
            LogLn("[Err] [RS232 Timeout] [IDG: " + GetIDGCmdString(u16Cmd) + "] " + strFuncName + "() = " + ext.Message)
        Catch ex As Exception
            LogLn("[Err] [IDG: " + GetIDGCmdString(u16Cmd) + "] " + strFuncName + "() = " + ex.Message)
        Finally
            m_bIDGCmdBusy = False
        End Try
        '
    End Sub

  '
    Public Overrides Sub SetAllCommandsFromTables(ByRef refobjCTLM As ClassTableListMaster, ByVal bPollmodeMode As Byte, Optional ByVal nTimeout As Integer = ClassDevRS232.m_cnTX_TIMEOUT_IN_SEC, Optional ByVal funcInProgressUpdate As Action(Of Integer) = Nothing)
        '============================================[ Alternative Way ]======================================================
        Try
            '--------------------------[ 1. Poll On Mode]--------------------------
            'GR Platform: Support Poll Mode Setting
            SystemSetPollMode(bPollmodeMode)
            If bIsResponseCodeUnknownCommandAR Then
                '01-01 --> Unknown Command --> Suppose In AR Platform. Try to Set Configuration with TLV "DF59" = GR Backward Capability, Disable/Enable it !!
                SetConfigurationsGRBackwardCapability()
            End If
            If Not (bIsRetStatusOK) Then
                LogLn("[Err] No Response, Poll Mode =" + bPollmodeMode.ToString())
            End If
            '----------------[ Update In Progress ]-----------------------
            If (funcInProgressUpdate IsNot Nothing) Then
                funcInProgressUpdate(1)
            End If

            '--------------------------[ 2. Remove AID Page ]--------------------------
            'Just Remove List AID
            'Throw New Exception("No Handled 002")
            'For nIdx = 0 To 10 ' 10 --> Change to Remove AID List Count
            '    DelConfigurationsAIDSingle()
            'Next
            Dim iteratorTLV As KeyValuePair(Of String, tagTLV)
            Dim strAIDKey As String = ""
            For Each iteratorTLV In refobjCTLM.m_dictAIDListDel
                DelConfigurationsAIDSingle(iteratorTLV.Value)
                '----------------[ Update In Progress ]-----------------------
                If (funcInProgressUpdate IsNot Nothing) Then
                    funcInProgressUpdate(1)
                End If
                'Check If Status Is Not Ready
                ClassTableListMaster.ConvertFromArrayByte2String(iteratorTLV.Value.m_arrayTLVal, strAIDKey)
                If Not (bIsRetStatusOK) Then
                    LogLn("[Err] No Response, Remove AID =" + strAIDKey)
                End If
            Next

            '--------------------------[ 3. Group List ]--------------------------
            Dim configGroup As tagConfigCmdUnitGroup

            'Get Group ID String --> then convert it to Integer Type --> Seek out the Group Info(Key = Group ID) From the Group Table  --> Set Command Group ID
            Dim iteratorGroup As KeyValuePair(Of Integer, tagConfigCmdUnitGroup)
            For Each iteratorGroup In refobjCTLM.m_dictGroupList
                configGroup = refobjCTLM.m_dictGroupList.Item(iteratorGroup.Key)
                '----------------[ Update In Progress ]-----------------------
                If (funcInProgressUpdate IsNot Nothing) Then
                    funcInProgressUpdate(1)
                End If

                LogLn("----Setting Single Group Info (ID= " & iteratorGroup.Key & ") Into Reader, Please Wait----")

                SetConfigurationsGroupSingle(configGroup)

                LogLn("----Setting Single Group Info Done----")
                'Check If Status Is Not Ready
                If Not (bIsRetStatusOK) Then
                    LogLn("[Err] No Response, Group Set ID =" & configGroup.m_nGroupID)
                End If
            Next


            '--------------------------[ 4.  AID List ]--------------------------
            'Get Terminal ID + Group ID String --> Seek out the AID Info(Key = Terminal ID) From the AID Table  --> Set Command AID
            Dim iteratorAID As KeyValuePair(Of String, tagConfigCmdUnitAID)

            For Each iteratorAID In refobjCTLM.m_dictAIDList
                Dim tagAIDInfo As tagConfigCmdUnitAID = iteratorAID.Value
                LogLn("----Setting Single AID Info (Terminal ID+ Group ID= " + tagAIDInfo.m_strKeyTagTerminalIDGroupID + ") Into Reader, Please Wait----")
                SetConfigurationsAIDSingle(tagAIDInfo)
                LogLn("----Setting Single AID Info Done----")
                '----------------[ Update In Progress ]-----------------------
                If (funcInProgressUpdate IsNot Nothing) Then
                    funcInProgressUpdate(1)
                End If

                'Check If Status Is Not Ready
                If Not (bIsRetStatusOK) Then
                    LogLn("[Err] No Response, AID Set Key ID =" + tagAIDInfo.m_strKeyTagTerminalIDGroupID)
                End If
            Next
        Catch ex As Exception
            LogLn("[Err] SetAllPagesConfigurations_Click() = " + ex.Message)
        End Try
    End Sub
    '
   Public Sub GRExclusive_PT_SystemSetEnhancePassthroughMode(u8SingleShotMode As ENUM_EPT_SINGLE_SHOT_MODE, u8LCDMsgIdx As ENUM_EPT_LCD_MSG_IDX, u8BeepIndicator As ENUM_SEPM_B7_BEEP_INDICATOR, u8LEDNum As ENUM_SEPM_LED_NUM, u8LEDOnOff As ENUM_SEPM_LED_ONOFF, strMsg1_Or_CustomMsg As String, strMsg2 As String, u8ActivateInterface As ENUM_SEPM_INTERFACE, ByRef o_byteCardType As Byte, ByRef o_arrayByteSerialNumUIC As Byte(), Optional ByVal bAlwaysSpecificUserInterface As Boolean = True, Optional ByVal nTimeout As Integer = ClassDevRS232.m_cnTX_TIMEOUT_IN_SEC)
        '
        Dim nSendDataMaxSize As Integer = 255
        Dim nSendDataSeekIdx As Integer = 0
        Dim arrayByteSendData As Byte() = New Byte(255) {}
        Dim u16Cmd As UInt16 = &H2C0B
        Dim strFuncName As String = "GRExclusive_PT_SystemSetEnhancePassthroughMode"
        Dim strCmd As String = ClassReaderProtocolIDGAR.GetIDGCmdString(m_tagIDGCmdDataInfo.m_u16CmdSet)

        m_bIsRetStatusOK = False
        If (m_bIDGCmdBusy) Then
            Return
        End If
        m_bIDGCmdBusy = True

        Try
            '-----------------------------------------------------------------[ Send to reader ]-----------------------------------------------------------------
            'Compose The IDG Packet 18-01, Not Command Frame Data, No Response Frame Data
            m_tagIDGCmdDataInfo.Reinit(u16Cmd, m_arraybyteSendBuf, Me)

            '[Single-Shot Commands , 1 byte]+ [LCD Message Index (for readers with a display only), 1 byte] + [Beep Indicator, 1 byte]+[LED Number,  1 byte]+ [LED Status, 1 byte]+[Timeout 1, 1 byte]+[Timeout 2, 1 byte = 0]+[LCD String1 Message, don't care]+[LCD String2 Message, don't care]+[Custom LCD Message, don't care]+[Selected Interface if user specific interface bit = 1]
            arrayByteSendData(nSendDataSeekIdx) = u8SingleShotMode
            nSendDataSeekIdx = nSendDataSeekIdx + 1
            arrayByteSendData(nSendDataSeekIdx) = u8LCDMsgIdx
            nSendDataSeekIdx = nSendDataSeekIdx + 1
            arrayByteSendData(nSendDataSeekIdx) = u8BeepIndicator
            nSendDataSeekIdx = nSendDataSeekIdx + 1
            arrayByteSendData(nSendDataSeekIdx) = u8LEDNum
            nSendDataSeekIdx = nSendDataSeekIdx + 1
            arrayByteSendData(nSendDataSeekIdx) = u8LEDOnOff
            nSendDataSeekIdx = nSendDataSeekIdx + 1
            arrayByteSendData(nSendDataSeekIdx) = nTimeout 'Time out in Second
            nSendDataSeekIdx = nSendDataSeekIdx + 1
            arrayByteSendData(nSendDataSeekIdx) = &H0 ' Timeout 2 in multiple of 10 seconds
            nSendDataSeekIdx = nSendDataSeekIdx + 1

            Select Case (u8LCDMsgIdx)
                Case ENUM_EPT_LCD_MSG_IDX.xFE_CUSTOM_USE_MSG
                    GRExclusive_PT_SystemSetEnhancePassthroughMode_LCDMsgs(strMsg1_Or_CustomMsg, arrayByteSendData, nSendDataSeekIdx)
                Case ENUM_EPT_LCD_MSG_IDX.xFF_LED_BUZZER_ONLY_WITHOUT_LCD
                    'Nothing
                Case Else
                    If ((u8LCDMsgIdx And ENUM_EPT_LCD_MSG_IDX.x80_MASK) = ENUM_EPT_LCD_MSG_IDX.x80_MASK) Then
                        GRExclusive_PT_SystemSetEnhancePassthroughMode_LCDMsgs(strMsg1_Or_CustomMsg, arrayByteSendData, nSendDataSeekIdx)
                        GRExclusive_PT_SystemSetEnhancePassthroughMode_LCDMsgs(strMsg2, arrayByteSendData, nSendDataSeekIdx)
                    Else
                    End If
            End Select

            If (bAlwaysSpecificUserInterface) Then
                'KIOSK III always needs the last specific user interface byte
                arrayByteSendData(nSendDataSeekIdx) = u8ActivateInterface
                nSendDataSeekIdx = nSendDataSeekIdx + 1
            Else
                If ((u8SingleShotMode And ENUM_EPT_SINGLE_SHOT_MODE.x20_USE_SPECIFIED_INTERFACE) = ENUM_EPT_SINGLE_SHOT_MODE.x20_USE_SPECIFIED_INTERFACE) Then
                    arrayByteSendData(nSendDataSeekIdx) = u8ActivateInterface
                    nSendDataSeekIdx = nSendDataSeekIdx + 1
                End If
            End If

            'arrayByteSendData = New Byte() {&H0, CType(nTimeout, Byte), u32FlagsB0Transaction, &H0, u32FlagsB2SingleShot, &H0, byteICCType, byteBeepIndicator, byteLEDControl, &H0, &H0}

            'Set Passthrough Mode
            m_tagIDGCmdDataInfo.ComposerAddData(arrayByteSendData, nSendDataSeekIdx)

            PktComposerCommandFrameHeaderCmdSetDataLengthCRCData(m_tagIDGCmdDataInfo, True)


            'Waiting For Response in SystemPingCbRecv()
            m_refdevConn.SendCommandFrameAndWaitForResponseFrame(AddressOf ClassReaderProtocolBasic.PreProcessCbRecv, m_tagIDGCmdDataInfo, m_nIDGCommanderType, True, nTimeout * 1000)


            'Its' response code = 0 => OK
            If (m_tagIDGCmdDataInfo.bIsResponseOK) Then
                Dim arrayByteData As Byte() = Nothing

                m_bIsRetStatusOK = True
                '
                m_tagIDGCmdDataInfo.ParserDataGetArrayRawData(arrayByteData)

                If (arrayByteData IsNot Nothing) Then
                    ' byte 0 = Card Type
                    o_byteCardType = arrayByteData(0)

                    'Remained data is PICC's Serial Number/UID
                    If (o_arrayByteSerialNumUIC IsNot Nothing) Then
                        Erase o_arrayByteSerialNumUIC
                        o_arrayByteSerialNumUIC = Nothing
                    End If
                    '
                    If (arrayByteData.Count > 1) Then
                        'Copy Data (Serial Num Or UID) of the PICC
                        Dim nDataLen As Integer
                        nDataLen = arrayByteData.Count - 1
                        o_arrayByteSerialNumUIC = New Byte(nDataLen - 1) {}
                        Array.Copy(arrayByteData, 1, o_arrayByteSerialNumUIC, 0, nDataLen)
                    End If
                End If
            Else
                m_bIsRetStatusOK = False
            End If

        Catch ext As TimeoutException
            LogLn("[Err][RS232 Timeout] [IDG: " + GetIDGCmdString(u16Cmd) + "] " + strFuncName + "() = " + ext.Message)
        Catch ex As Exception
            LogLn("[Err] [IDG:  " + GetIDGCmdString(u16Cmd) + "] " + strFuncName + "() = " + ex.Message)
        Finally
            m_tagIDGCmdDataInfo.Cleanup()
            m_bIDGCmdBusy = False
            If (arrayByteSendData Is Nothing) Then
                Erase arrayByteSendData
            End If
        End Try

    End Sub
    '
        Private Sub ComposeRestTLVsCAPK(ByRef dictTLV As Dictionary(Of String, tagTLV), ByRef tagIDGCmdDataInfo As tagIDGCmdData)
        Dim nIdx As Integer
        Dim tlvOp As tagTLVNewOpCAPK
        Dim iteratorTLV As KeyValuePair(Of String, tagTLV)

        'If m_bDebug Then
        '    'FFEE04 ->TextBox14 @ Major Form
        '    nIdxEnd = Convert.ToInt16(My.Forms.m_refwinformMain.TextBox14.Text, 10)
        'End If
        '------------------[ Compose its' remains TLV properties ]------------------
        nIdx = 0
        For Each iteratorTLV In dictTLV
            tlvOp = m_refdictTlvNewOpCAPK.Item(iteratorTLV.Key)
            tlvOp.m_pfuncCbComposer(iteratorTLV.Value, tagIDGCmdDataInfo)

            '----------------[ Start of Debug Section ]----------------
            'If m_bDebug Then
            '    If iteratorTLV.Value.m_nLen > 0 Then
            '        nIdx = nIdx + 1
            '        If (nIdx >= nIdxEnd) Then
            '            LogLn("[Debug] TLV has been added up to " & nIdx)
            '            Exit For
            '        End If
            '    End If
            'End If
            '----------------[ End of Debug Section ]----------------
        Next
    End Sub
#End If

End Class
