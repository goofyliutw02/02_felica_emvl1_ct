﻿Imports System.Threading
Imports System.IO
Imports System

Public Class Form01_Main

    '===============[ Developer Configuration ]===============
    '2017 Oct 30, for Developer Configuration
    'Tx / Rx Log Enable/Disable
    Private m_bIsTxRxLog As Boolean = False

    Private m_bIsHWTestOnly As Boolean = True

    '===============[ End of Developer Configuration]===============
    '===============[ Real Time AutoTest ]===============
    Structure TAG_REALTIME_UPDATE_INFO
        Public m_bEnable As Boolean
        Public m_nTestAutoTimeoutMillisec As Integer
        Public m_nTestAutoTimeCount As Integer
        Public m_nTestAutoTimeCountUpto1Sec As Integer

    End Structure
    '
    Public m_tagIDGCmdTimeOutInfo As TAG_REALTIME_UPDATE_INFO
    Private m_nTimeout As Integer
    '
    '===============[ Major Instances / Objects ]===============
    Public m_objTableListMaster As ClassTableListMaster
    Public m_objRdCmderParser As ClassReaderCommanderParser
    '
    Public m_objRdrCmderIDGGR As ClassReaderProtocolIDGGR
    Public m_refObjRdrCmder As ClassReaderProtocolBasic
    Private m_objTransaction As ClassTransaction
    Private m_refObjDevRS232 As ClassDevRS232
    Private m_refObjDevUSBHID As ClassDevUSBHID
    '
    Private m_objReaderInfo As ClassReaderInfo
    '
    Private m_strAppVersion As String = ""
    '
    Private m_bIsTimeLogDumpingRecvDone As Boolean
    Private m_bIsTimeLogDumpingRecvDoneEnable As Boolean
    Private m_strDumpingRecvBuffer As String
    Private m_strDumpingRecvBufferHex As String
    Private m_nTimeoutDumpingRecv As Integer

    Private m_bIsTxRxSimDatMode As Boolean = False
    '
    ReadOnly Property bIsDebug As Boolean
        Get
            Return m_bDebug
        End Get
    End Property


    Private Property bIsTxRxSimDatMode As Boolean
        Get
            'Return m_bIsTxRxSimDatMode
            Return (1 = m_CmbBox_MifareRunMode.SelectedIndex)
        End Get
        Set(value As Boolean)
            m_bIsTxRxSimDatMode = value
            '
            If (value) Then
                'Using customized 
                m_CmbBox_MifareRunMode.SelectedIndex = 1
            Else
                m_CmbBox_MifareRunMode.SelectedIndex = 0
            End If

        End Set
    End Property

    '
    Public Delegate Sub LogLnDelegate(ByVal strInfo As String)
    Public Delegate Sub CalledByPreProcessCbRecv_IBESU(ByRef refIDGCmdData As tagIDGCmdData)
    '
    Private m_refRichTextBox_LogWin As RichTextBox = Nothing
    Private m_bDebug As Boolean = False
    '
    Private Shared m_pInstance As Form01_Main = Nothing
    Shared Function GetInstance() As Object
        If (m_pInstance Is Nothing) Then
            m_pInstance = New Form01_Main()
        End If
        Return m_pInstance
    End Function
    '
    Shared ReadOnly Property TM007_DateTimeCheck_GetReaderMFGTimeZoneHours As Single
        Get
            Return 8.0F
        End Get
    End Property
    '
    Public Shared Function GetTimeStampYear2Millisec() As String
        Static dwLastTicks As Long = Now.Ticks
        Dim strDateTime As String = ""
        '
        Dim myDate As Date = My.Computer.Clock.LocalTime
        Dim nPassCnt As Integer = 0
        Dim nPassCntMax As Integer = 2 'Date + Time Checks
        Dim dateHostLZone, dateHostTZone8 As Date
        Dim timeZoneLocal As TimeZoneInfo = TimeZoneInfo.Local
        'Dim pDFCL As ClassDataFilesCmpLog = ClassDataFilesCmpLog.GetInstance()
        Dim sTZone8Hours As Single = Form01_Main.TM007_DateTimeCheck_GetReaderMFGTimeZoneHours - timeZoneLocal.BaseUtcOffset.Hours  '8.0F
        'Dim nYear, nMonth, nDay As Integer
        'Dim nHour, nMinutes, nSeconds As Integer
        Dim dictTLV As New Dictionary(Of String, tagTLV)
        'Dim strTmp As String
        Dim nTmp As Integer
        Dim strTimeZoneSign As String = "+"
        Dim strDelimiter As String = "" ' vbCrLf
        'Dim nTotalReader, nTotalHost As Integer
        '
        Try
            '------------------------[Start Processing]----------------------
            nTmp = timeZoneLocal.BaseUtcOffset.Hours
            If (nTmp < 0) Then
                strTimeZoneSign = ""
            End If

            ' Get Host's Date Object, -----------------------------------------------------------------------------------------------------------------"dateHostLZone"
            dateHostLZone = myDate
            'Create A Shifted Date Object from Local Time Zone to UTC/GMT +08000 Time Zone Date Object,------------------- "dateHostTZone8"
            dateHostTZone8 = New Date(myDate.Ticks)
            If (timeZoneLocal.IsDaylightSavingTime(dateHostLZone)) Then
                sTZone8Hours = sTZone8Hours - 1.0F
            End If
            dateHostTZone8 = dateHostTZone8.AddHours(sTZone8Hours) ' - timeZoneLocal.BaseUtcOffset.Hours)

            'Standard Date & Time
            ' 
            strDateTime = String.Format("{0,4:D4}-{1,2:D2}-{2,2:D2}", dateHostLZone.Year, dateHostLZone.Month, dateHostLZone.Day)
            strDateTime = strDateTime + strDelimiter + String.Format(" {0,2:D2}:{1,2:D2}:{2,2:D2}:{3,3:D3}", dateHostLZone.Hour, dateHostLZone.Minute, dateHostLZone.Second, dateHostLZone.Millisecond) 'Host's Time
            strDateTime = strDateTime + strDelimiter + " GMT " + strTimeZoneSign + nTmp.ToString() + " hrs" 'Host's Date
            If (timeZoneLocal.IsDaylightSavingTime(dateHostTZone8)) Then
                strDateTime = strDateTime + strDelimiter + "Daylight Saving Is ON"
            End If

            ' Delta Time : Ticks Interval between Last Command and current Command
            strDateTime = strDateTime + ", Delta =" + ((Now.Ticks - dwLastTicks) \ 10000).ToString() + " ms,"
            dwLastTicks = Now.Ticks

        Catch ex As Exception
            Throw
        End Try

        Return strDateTime
    End Function
    '



    '
    Public m_tagRealTimeUpdateInfoDebugOnly As TAG_REALTIME_UPDATE_INFO_DEBUG_ONLY
    Private m_tagRealTimeUpdateInfoDebugOnlyCOMPortOpenClose_RS232 As TAG_REALTIME_UPDATE_INFO_DEBUG_ONLY
    Private m_tagRealTimeUpdateInfoDebugOnlyCOMPortOpenClose_USBHID As TAG_REALTIME_UPDATE_INFO_DEBUG_ONLY
    '
    '========================[ Time Elapsed Display ]=========================
    Private m_nTimeElapsedTimeMS As Integer
    Private m_nTimeElapsedTimeSec As Integer
    Private m_bTimeElapsedTimeEnable As Boolean
    Property TimeElapsedEnable As Boolean
        Get
            Return m_bTimeElapsedTimeEnable
        End Get
        Set(value As Boolean)
            m_bTimeElapsedTimeEnable = value
        End Set
    End Property
    '
    '
    Private Sub TimeElapsedTimeReset(Optional ByVal bTimeUpdate As Boolean = False, Optional ByVal bTimeElapsedShowAfterReset As Boolean = False)

        m_bTimeElapsedTimeEnable = bTimeUpdate
        m_nTimeElapsedTimeMS = 0
        m_nTimeElapsedTimeSec = 0

        If (bTimeElapsedShowAfterReset) Then
            ShowElapsedTIme()
        End If

    End Sub
    Private Sub TimeElapsedTimeStop()
        TimeElapsedEnable = False
    End Sub
    ' period 100 ms
    Private Sub RealTimeUpdateElapsedTime(ByVal nInterval As Integer)
        If (m_bTimeElapsedTimeEnable) Then
            m_nTimeElapsedTimeMS = m_nTimeElapsedTimeMS + nInterval
            If (m_nTimeElapsedTimeMS >= 1000) Then
                m_nTimeElapsedTimeMS = m_nTimeElapsedTimeMS - 1000
                m_nTimeElapsedTimeSec = m_nTimeElapsedTimeSec + 1
                ShowElapsedTIme()
            End If
        End If
    End Sub
    Private Sub ShowElapsedTIme(Optional ByVal nTimeVal As Integer = -1)
#If 0 Then
        Dim nH, nM, nS, nMs As Integer
        nMs = 0
        If (nTimeVal <> -1) Then
            nH = nTimeVal \ 3600
            nM = ((nTimeVal Mod 3600) \ 60)
            nS = (nTimeVal Mod 60)
        Else
            nH = m_nTimeElapsedTimeSec \ 3600
            nM = ((m_nTimeElapsedTimeSec Mod 3600) \ 60)
            nS = (m_nTimeElapsedTimeSec Mod 60)
            nMs = m_nTimeElapsedTimeMS Mod 100
        End If

        'TextBox_TimeElapsed.Text = String.Format("{0,2:d2} : {1,2:d2} : {2,2:d2}. {3,2:d2}", nH, nM, nS, nMs)
        TextBox_TimeElapsed.Text = String.Format("{0,2:d2} : {1,2:d2} : {2,2:d2}", nH, nM, nS)
#End If
    End Sub '
    '

    '===============[ Real Time AutoTest ]===============
    Structure TAG_REALTIME_UPDATE_INFO_DEBUG_ONLY
        Public m_bArrayTestButtonReCreate As Boolean

        Private m_ArrayTestButton() As System.Windows.Forms.Button
        '
        Private m_nTestAutoTimeout As Integer
        Private m_nTestAutoTimeCount As Integer
        Enum EnumXX As Integer
            TIMEOUT_MAX = 10000
        End Enum
        '
        Public m_bCriticalSection As Boolean

        '
        Public Sub Init(Optional ByVal bArrayTestButtonCreated As Boolean = False)
            m_ArrayTestButton = Nothing
            '
            ResetTimeInfo()
            '
            '
            m_bArrayTestButtonReCreate = False
            m_bCriticalSection = False
        End Sub

        Public Sub Cleanup()
            'Free m_ArrayTestButton Resource
            If m_ArrayTestButton IsNot Nothing Then
                Erase m_ArrayTestButton
                m_ArrayTestButton = Nothing
            End If

            'Other Resources Free Steps are Continued Here !!
        End Sub
        '
        Property nTestAutoTimeCount As Integer
            Get
                Return m_nTestAutoTimeCount
            End Get
            Set(value As Integer)
                m_nTestAutoTimeCount = value
            End Set
        End Property
        '
        Property nTestAutoTimeout As Integer
            Get
                Return m_nTestAutoTimeout
            End Get
            Set(value As Integer)
                m_nTestAutoTimeout = value
            End Set
        End Property

        ReadOnly Property IsTimeout As Boolean
            Get
                Return (m_nTestAutoTimeout <= m_nTestAutoTimeCount)
            End Get
        End Property

        Public Sub ResetTimeInfo(Optional ByVal nTimeoutMax As Integer = TAG_REALTIME_UPDATE_INFO_DEBUG_ONLY.EnumXX.TIMEOUT_MAX)
            m_nTestAutoTimeout = nTimeoutMax
            m_nTestAutoTimeCount = 0
        End Sub
    End Structure

    '
#If 0 Then


    Private Sub RealTimeAutoLoadDefault_WhileDisconnectFromUUTForShipping(nInterval As Integer)
        Try
            Dim nCountChk As Integer = 0
            'Self Test
            If Not CheckBox7_Load_Default_Before_Shipping.Checked Then
                Return
            End If

            'In Connected Mode, Open Buttons is Disabled
            If (Not Button_PortOpen_RS232.Enabled) Then
                'If Wait Message doesn't  show up Yet (-->Critical Area Flag), we disconnection it automatically
                If (Not m_winformWaitTillDone.Visible) Then
                    Button_PortClose.PerformClick()
                End If
            End If

        Catch ex As Exception
            LogLn("[Err] RealTimeAutoLoadDefault_WhileDisconnectFromUUTForShipping() = " + ex.Message)
        End Try
    End Sub

    Private Sub RealTimeAutoTestForDebugONLY(ByVal nInterval As Integer)
        'Dim ArrayTestButton() As System.Windows.Forms.Button

        Try
            '    'Self Test
            '    If (m_winformConfigurations Is Nothing) Then
            '        Return
            '    End If
            '    If (Not m_winformConfigurations.ConfigTxtFileAutoLoadChkBox.Checked) Then
            '        Return
            '    End If

            '    If m_winformConfigurations.Visible Then
            '        ' Get Test Buttons of Configurations Form
            '        ArrayTestButton = m_tagRealTimeUpdateInfoDebugOnly.ArrayTestButton
            '        If ArrayTestButton Is Nothing Then
            '            m_tagRealTimeUpdateInfoDebugOnly.ReCreateArrayTestButtons()
            '        End If
            '        '
            '        m_tagRealTimeUpdateInfoDebugOnly.nTestAutoTimeCount = m_tagRealTimeUpdateInfoDebugOnly.nTestAutoTimeCount + nInterval

            '        If m_tagRealTimeUpdateInfoDebugOnly.IsTimeout Then
            '            'Reset Time Info
            '            m_tagRealTimeUpdateInfoDebugOnly.ResetTimeInfo()
            '            'If wanna recreate ArrayTestButtons
            '            If m_tagRealTimeUpdateInfoDebugOnly.m_bArrayTestButtonReCreate Then
            '                m_tagRealTimeUpdateInfoDebugOnly.ReCreateArrayTestButtons()
            '            End If

            '            '*Lite*  OFF the button
            '            ArrayTestButton(m_nTestTxtLoadAutoIdx).BackColor = Color.Gray
            '            'Advanced to next button
            '            m_nTestTxtLoadAutoIdx = m_nTestTxtLoadAutoIdx + 1
            '            If m_nTestTxtLoadAutoIdx >= ArrayTestButton.Count Then
            '                m_nTestTxtLoadAutoIdx = 0
            '            End If
            '            '*Lite*  ON the button
            '            ArrayTestButton(m_nTestTxtLoadAutoIdx).BackColor = Color.Cyan

            '            'Start to load some file
            '            m_winformConfigurations.TestConfigFileLoad(ArrayTestButton(m_nTestTxtLoadAutoIdx), EventArgs.Empty)

            '        End If
            '    Else
            '        m_tagRealTimeUpdateInfoDebugOnly.ResetTimeInfo()
            '    End If

        Catch ex As Exception
            LogLn("[Err] RealTimeAutoTestForDebugONLY() = " + ex.Message)
        End Try

    End Sub
    '
#End If
    '=====[Real Time Update]======
    Structure tagRealTimeInfo
        Public m_nTimeCount As Integer
        Public m_nTImeOut As Integer
        Public m_winobjRTCChkBox As System.Windows.Forms.CheckBox
        '
        Enum EnumComboBoxTImeField As Integer
            CBTF_YEAR = 0
            CBTF_MON
            CBTF_DAY
            CBTF_HOUR
            CBTF_MIN
            CBTF_SEC
        End Enum
    End Structure
    'Private m_tagRealTimeInfo As tagRealTimeInfo
    Private m_tagRealTimeInfoAutoCOMPortOpenClose As tagRealTimeInfo

    'Private m_arrayComboDateTime() As System.Windows.Forms.ComboBox

    Private Sub RealTimeInit()
        'm_tagRealTimeInfo = New tagRealTimeInfo With {.m_nTimeCount = 0, .m_nTImeOut = 1000, .m_winobjRTCChkBox = CheckBox16}
        'm_arrayComboDateTime = New ComboBox() {ComboBox10, ComboBox5, ComboBox6, ComboBox7, ComboBox8, ComboBox9}

        m_tagRealTimeUpdateInfoDebugOnlyCOMPortOpenClose_USBHID.Init()
    End Sub
    '
    Private Sub RealTimeCleanup()
        'If m_arrayComboDateTime IsNot Nothing Then
        '    Erase m_arrayComboDateTime
        'End If
        '
        m_tagRealTimeUpdateInfoDebugOnly.Cleanup()
        m_tagRealTimeUpdateInfoDebugOnlyCOMPortOpenClose_RS232.Cleanup()
        m_tagRealTimeUpdateInfoDebugOnlyCOMPortOpenClose_USBHID.Cleanup()

    End Sub

#If 0 Then
    ' Show Log Msg

    '
    Private Sub RealTimeDateUpdateTLV(nInterval As Integer)
        If m_tagRealTimeInfo.m_winobjRTCChkBox IsNot Nothing Then
            If m_tagRealTimeInfo.m_winobjRTCChkBox.CheckState = CheckState.Checked Then
                m_tagRealTimeInfo.m_nTimeCount = m_tagRealTimeInfo.m_nTimeCount + nInterval
                If m_tagRealTimeInfo.m_nTimeCount >= m_tagRealTimeInfo.m_nTImeOut Then
                    Dim myDate As Date = My.Computer.Clock.LocalTime
                    m_tagRealTimeInfo.m_nTimeCount = 0
                    m_arrayComboDateTime(tagRealTimeInfo.EnumComboBoxTImeField.CBTF_YEAR).Text = myDate.Year.ToString()
                    m_arrayComboDateTime(tagRealTimeInfo.EnumComboBoxTImeField.CBTF_MON).SelectedIndex = myDate.Month - 1 '.ToString()
                    m_arrayComboDateTime(tagRealTimeInfo.EnumComboBoxTImeField.CBTF_DAY).SelectedIndex = myDate.Day - 1 '.ToString()
                    '
                    m_arrayComboDateTime(tagRealTimeInfo.EnumComboBoxTImeField.CBTF_HOUR).SelectedIndex = myDate.Hour '.ToString()
                    m_arrayComboDateTime(tagRealTimeInfo.EnumComboBoxTImeField.CBTF_MIN).SelectedIndex = myDate.Minute '.ToString()
                    m_arrayComboDateTime(tagRealTimeInfo.EnumComboBoxTImeField.CBTF_SEC).SelectedIndex = myDate.Second '.ToString()
                End If
            End If
        End If
    End Sub
#End If
    '
    Public Const MAX_BUF_LEN_64 As UInt32 = 64

    Private Sub RealTimeUpdateDumpingRecvIntfUSBHID()

        If (m_refObjRdrCmder IsNot Nothing) Then
            If (Not m_refObjRdrCmder.bIsBusy) Then
                If (m_refObjRdrCmder.bIsConnectedUSBHID) Then
                    Dim u32RecvDataSize As UInt32 = MAX_BUF_LEN_64
                    Dim strDumpTmp As String = ""
                    Dim arrayByteData As Byte() = New Byte(MAX_BUF_LEN_64 - 1) {}
                    '====[ Debugging ]====
#If 0 Then
                    Static nLogLn As Integer = 0
                    LogLn("RTime Update Count = " + nLogLn.ToString())
                    nLogLn = nLogLn + 1
#End If
                    '====[ End of Debugging ]====
                    Try
                        u32RecvDataSize = m_refObjRdrCmder.RecvDataRealTimeUpdate(arrayByteData, u32RecvDataSize, 100)
                        If (u32RecvDataSize > 0) Then
                            ' Skip Report ID at the beginning
                            u32RecvDataSize = u32RecvDataSize - 1
                            ClassTableListMaster.ConvertFromArrayByte2String(arrayByteData, 1, u32RecvDataSize, strDumpTmp, True)

                            '2016 Sept 07, remove LogLn(), fasten MSR Test
                            'LogLn("[Hex Data]=" + strDumpTmp) 
                            DumpingRecvHexStr(strDumpTmp)

                            ClassTableListMaster.ConvertFromHexToAscii(arrayByteData, 1, u32RecvDataSize, strDumpTmp, False)

                            '2016 Sept 07, remove LogLn(), fasten MSR Test
                            'LogLn("[ASCII Data]=" + strDumpTmp)
                            'LogLn("-----------[End of Dumping Mode]-----------")
                            DumpingRecvAsciiStr(strDumpTmp)
                        End If
                    Catch ex As Exception
                    Finally
                        If (arrayByteData IsNot Nothing) Then
                            Erase arrayByteData
                        End If
                    End Try
                End If
            End If
        End If
    End Sub
    '
    Private Sub RealTimeUpdateDumpingRecv(ByVal nTimeInterval As Integer)
        If (Not m_bIsTimeLogDumpingRecvDoneEnable) Then
            'Dumping  Any Incoming Data Here in USB HID Connection
            'RealTimeUpdateDumpingRecvIntfUSBHID() '2016 Feb 18, removed , goofy_liu@idtechproducts.com.tw
            Return
        End If

        '=========[ For USB HID Interface, No Thread reads incoming data (passive mode). This timer update routine takes care it ]==========
        'Project: Vendi - Test Item [Key Pad], USB-HID Mode, It has some strange status...
        '1. The First Key Down and Data Recved and dumped
        '2. The Second Key Down and Reader seems has delayed response (4.9~5 seconds response)
        If (m_refObjRdrCmder.bIsConnectedUSBHID) Then
            RealTimeUpdateDumpingRecvIntfUSBHID()

            '===[ For RS232 Interface, there is a system thread (SerialPort Instance manages the thread.)to take care Incoming Data ]===
            'USB HID Interface, we have to take care here.
            'If (bIsEngMode_HWTestONLY) Then
            '    ProcessDumpingRecvData_HWMode()
            'End If
        End If

        If (m_strDumpingRecvBuffer.Equals("")) Then
            Return
        End If
        '
        m_nTimeoutDumpingRecv = m_nTimeoutDumpingRecv + nTimeInterval
    End Sub
    '
    '
    Private Sub RealTimeAutoTestForCOMPortOpenClose_USBHID(nInterval As Integer)
        Try
            Dim nCountChk As Integer = 0
            'Self Test
            'If Not CheckBox_PortOpen_Auto_USBHID.Checked Then
            '    Return
            'End If

            '
            With m_tagRealTimeUpdateInfoDebugOnlyCOMPortOpenClose_USBHID
                If (Not .m_bCriticalSection) Then
                    .m_bCriticalSection = True
                    '======================
                    If (True) Or (nCountChk = 2) Then
                        .nTestAutoTimeCount = .nTestAutoTimeCount + nInterval

                        If .IsTimeout Then
                            'TODO Your Code Here For "Trigger Action"
                            'COM Port is Open Now, try to close it
                            If Btn_S00_Stop_Close.Enabled Then

                                m_refObjRdrCmder.IDGCmdInterruptWaitBusyToIdle()

                                Btn_S00_Close_Click(Btn_S00_Stop_Close, EventArgs.Empty)
                            Else
                                'Initial                              
                                Btn_S00_Connect_Click(Btn_S00_Connect_Start, EventArgs.Empty)
                            End If
                        End If
                    Else
                        .ResetTimeInfo()
                    End If
                    '======================
                    .m_bCriticalSection = False
                    'End of Critical Section preventing from Re-entry
                End If

            End With


        Catch ex As Exception
            LogLn("[Err] RealTimeAutoTestForCOMPortOpenClose() = " + ex.Message)
        End Try
    End Sub

    '
    Private Sub RealTimeDateUpdate(ByVal nInterval As Integer)
        Try
            '============[ System Time/Date Real Time Update ]===================
            'RealTimeDateUpdateTLV(nInterval)
            RealTimeUpdateLogMsg(nInterval)
            IDGCmdTimeOutRealTimeUpdate(nInterval)
            RealTimeUpdateDumpingRecv(nInterval)
            '=======================[Debug / Auto Test Area]=====================
            '============[ Test Item  ]===================
            RealTimeUpdateElapsedTime(nInterval)
            'RealTimeAutoTestForDebugONLY(nInterval)
            'RealTimeAutoTestForCOMPortOpenClose(nInterval)
            '---[ Auto Test : Open Device --> Close Device --> in the Loop ]---
            'RealTimeAutoTestForCOMPortOpenClose_USBHID(nInterval) 
            'RealTimeAutoLoadDefault_WhileDisconnectFromUUTForShipping(nInterval)
            '=====================[End of Debug / Auto Test Area]=======================

        Catch ex As Exception
            LogLn("[Err] RealTimeDateUpdate() = " + ex.Message)
        End Try
    End Sub


    '
    Private Sub Timer1_Tick(sender As Object, e As EventArgs) Handles Timer1.Tick
        'UI Driver Update
        'If m_objUIDriver IsNot Nothing Then
        '    Me.TImeUpdateUIDriver(Timer1.Interval)
        'End If

        'Real Time Update
        RealTimeDateUpdate(Timer1.Interval)
        '
    End Sub
    '
    Private m_txtBoxTimeout As TextBox
    Public Function IDGCmdTimeoutGet() As Integer
        Dim nTimeoutSec As Integer

        nTimeoutSec = Convert.ToInt16(m_txtBoxTimeout.Text, 10)
        m_nTimeout = nTimeoutSec
        Return nTimeoutSec
    End Function

    ' bForce2ChangeIfBigger
    Public Sub IDGCmdTimeoutSet(Optional ByVal nTimeOut As Integer = ClassDevRS232.m_cnTX_TIMEOUT_IN_SEC, Optional ByVal bForce2ChangeIfOrgValueIsBigger As Boolean = True)
        Dim nTimeoutOld As Integer = Convert.ToInt32(m_txtBoxTimeout.Text, 10)

        If (Not bForce2ChangeIfOrgValueIsBigger) Then
            If (nTimeoutOld >= nTimeOut) Then
                Return
            End If
        End If

        m_txtBoxTimeout.Text = nTimeOut.ToString()
        If (m_refTSStsLabel_IDGTimer IsNot Nothing) Then
            m_refTSStsLabel_IDGTimer.Text = m_txtBoxTimeout.Text
        End If
    End Sub
    '
    Public Sub IDGInterruptCommand()
        If (m_refObjRdrCmder Is Nothing) Then
            Return
        End If
        'Interrupt Reader Commander Response Waiting if needed
        If (m_refObjRdrCmder.bIsBusy) Then
            ' 2016-Feb-18 Updated, USB Intf Interruption problem
            m_refObjRdrCmder.IDGCmdInterruptNoWait()
        End If
    End Sub
    '=======================================================================
    Private Sub ComPortOpen_InterruptIDGCmdAndClosePort()
        '--------------[Stop All IDG Commander if they are busy now]-----------------
        'Interrupt Reader Commander Response Waiting if needed
        If (m_refObjRdrCmder IsNot Nothing) Then
            If (m_refObjRdrCmder.bIsBusy) Then
                m_refObjRdrCmder.IDGCmdInterruptNoWait()
            End If

            m_refObjRdrCmder.ConnectionClose()
        End If

    End Sub


    Public Sub IDGCmdTimeOutRealTimeStop()
        With m_tagIDGCmdTimeOutInfo

            .m_bEnable = False
            ' Restore Value
            IDGCmdTimeOutTimerDisplaySet(.m_nTestAutoTimeoutMillisec)
        End With
    End Sub
    Public Sub IDGCmdTimeOutRealTimeResetAndStart(Optional ByVal nTimeOutInMillisec As Integer = ClassDevRS232.m_cnRX_TIMEOUT)
        'Dim nTOOffset As Integer = 2000
        With m_tagIDGCmdTimeOutInfo
            'If non-transaction command status, we can't change the Transaction Time Out Value
            If m_refObjRdrCmder.CallByIDGCmdTimeOutChk_IsCommIdle Then
                nTimeOutInMillisec = IDGCmdTimeoutGet() * 1000
                .m_nTestAutoTimeoutMillisec = nTimeOutInMillisec '+ nTOOffset
            Else
                nTimeOutInMillisec = .m_nTestAutoTimeoutMillisec
                IDGCmdTimeOutTimerDisplaySet(.m_nTestAutoTimeoutMillisec)
            End If
            .m_nTestAutoTimeCountUpto1Sec = 0
            .m_nTestAutoTimeCount = nTimeOutInMillisec
            .m_bEnable = True

            Button_Timeout.Visible = True
        End With
    End Sub
    ' This may be invoked in Form04
    Public Sub IDGCmdTimeOutRealTimeUpdate(nIntervalMillisec As Integer)
        If (m_refObjRdrCmder Is Nothing) Then
            Return
        End If
        '
        With m_tagIDGCmdTimeOutInfo
            If (.m_bEnable) Then
                .m_nTestAutoTimeCount = .m_nTestAutoTimeCount - nIntervalMillisec
                If (.m_nTestAutoTimeCount < 0) Then
                    .m_nTestAutoTimeCount = 0
                End If

                ' Update to Timeout Countdown Display per second
                .m_nTestAutoTimeCountUpto1Sec = .m_nTestAutoTimeCountUpto1Sec + nIntervalMillisec
                If (.m_nTestAutoTimeCountUpto1Sec >= 1000) Then
                    IDGCmdTimeOutTimerDisplaySet(.m_nTestAutoTimeCount, 1)
                    .m_nTestAutoTimeCountUpto1Sec = .m_nTestAutoTimeCountUpto1Sec - 1000
                End If

                'Do Timeout Event for IDG Command

                If (.m_nTestAutoTimeCount = 0) Then
                    .m_bEnable = False
                    m_refObjRdrCmder.CallByIDGCmdTimeOutChk_SetTimeoutEvent()

                    ' Restore Display
                    IDGCmdTimeOutTimerDisplaySet(.m_nTestAutoTimeoutMillisec)

                    Button_Timeout.Visible = False
                    'Check if the IDG Command Is Canceled or Stopped by Button "Transaction Stop" or "Transaction Cancel"
                    'ElseIf (m_objRdrCmder.CallByIDGCmdTimeOutChk_IsCommIDGCancelStop Or m_objRdrCmder.CallByIDGCmdTimeOutChk_IsCommStopped Or m_objRdrCmder.CallByIDGCmdTimeOutChk_IsCommIdle Or (Not m_objRdrCmder.bIsBusy)) Then
                ElseIf (Not m_refObjRdrCmder.bIsBusy) Then
                    .m_bEnable = False
                    ' Restore Display
                    IDGCmdTimeOutTimerDisplaySet(.m_nTestAutoTimeoutMillisec)

                    Button_Timeout.Visible = False
                End If
            End If
        End With

    End Sub
    '
    Public Sub DumpingRecvAsciiStr(ByVal strDumpRecvData_ASCII As String)
        If (m_bIsTimeLogDumpingRecvDoneEnable) Then
            If (Not strDumpRecvData_ASCII.Equals("")) Then
                m_strDumpingRecvBuffer = m_strDumpingRecvBuffer + " " + strDumpRecvData_ASCII
                m_nTimeoutDumpingRecv = 0
            End If
        End If
    End Sub
    Public Sub DumpingRecvHexStr(ByVal strDumpRecvDataHex As String)
        If (m_bIsTimeLogDumpingRecvDoneEnable) Then
            If (Not strDumpRecvDataHex.Equals("")) Then
                m_strDumpingRecvBufferHex = m_strDumpingRecvBufferHex + " " + strDumpRecvDataHex
                m_nTimeoutDumpingRecv = 0
            End If
        End If
    End Sub

    Public ReadOnly Property GetDumpingRecvData As String
        Get
            If (m_objReaderInfo.bIsIntfMSR And m_objReaderInfo.bIsMSRParseDone) Then
                Return m_objReaderInfo.GetMSRStr(False)
            End If
            Return m_strDumpingRecvBuffer
        End Get
    End Property
    Public ReadOnly Property GetDumpingRecvDataHexStr As String
        Get
            Return m_strDumpingRecvBufferHex
        End Get
    End Property
    '
    Private m_refTSStsLabel_IDGTimer As ToolStripStatusLabel = Nothing
    Private Sub IDGCmdTimeOutTimerDisplaySet(ByVal nTimeOutValInMillisec As Integer, Optional ByVal nForColorSel As Integer = 0)
        Dim nTmp As Integer
        Dim forColor As Color

        If ((nTimeOutValInMillisec Mod 1000) > 0) Then
            nTmp = 1 ' More 1 sec if any remainder in millisecond
        Else
            nTmp = 0
        End If
        If (nForColorSel = 0) Then
            forColor = Color.Black
        Else
            forColor = Color.Red
        End If

        nTimeOutValInMillisec = (nTimeOutValInMillisec / 1000) + nTmp

        If (nTimeOutValInMillisec = 10) Then
            Dim i As Integer

            i = 0
        End If

        m_txtBoxTimeout.ForeColor = forColor
        m_txtBoxTimeout.Text = nTimeOutValInMillisec.ToString()
        'Sync to outsource IDG Timer Counting Down
        If (m_refTSStsLabel_IDGTimer IsNot Nothing) Then
            m_refTSStsLabel_IDGTimer.Text = m_txtBoxTimeout.Text
        End If
    End Sub
    '
    Public Sub CalledByPreProcessCbRecv_InterruptByExternalStatusUpdate(ByRef refIDGCmdData As tagIDGCmdData)
        refIDGCmdData.setInterruptByExternal = m_refObjRdrCmder.bIsIDGCmdInterruptedByExternal
    End Sub
    '===[ Thread Safe Logging Functions ]===
    Private m_blistLogTimeUpdateCriticalSection As Boolean
    Private m_listLogTimeUpdate As List(Of tagLogTimeUpdateInfo)

    Public Structure tagLogTimeUpdateInfo
        Public m_bIsRemoved As Boolean
        Public m_strLogMsg As String
        '
        Sub New(strLogMsg As String)
            m_strLogMsg = strLogMsg
            m_bIsRemoved = False
        End Sub
    End Structure
    '
    Public Sub AppendText(ByVal strInfo As String)
        'Check Max Lines --> Clear Content

        If (m_logRichText.Lines.Count > _TAG_PRINT_INFO.m_s_nLineMaxToClear) Then
            m_logRichText.Clear()
        End If

        '
        m_logRichText.AppendText(strInfo)
        m_logRichText.ScrollToCaret()
        'Reference outside Rich text controller
        If (m_refRichTextBox_LogWin IsNot Nothing) Then
            If (m_refRichTextBox_LogWin.Lines.Count > _TAG_PRINT_INFO.m_s_nLineMaxToClear) Then
                m_refRichTextBox_LogWin.Clear()
            End If

            m_refRichTextBox_LogWin.AppendText(strInfo)
            m_refRichTextBox_LogWin.ScrollToCaret()
        End If

    End Sub

    ' Show Log Msg
    Private Sub RealTimeUpdateLogMsg(ByVal nInterval As Integer)
        Dim tagLogTime As tagLogTimeUpdateInfo
        '
        If m_blistLogTimeUpdateCriticalSection Then
            Exit Sub
        End If
        '
        If m_listLogTimeUpdate.Count > 0 Then
            '
            For Each tagLogTime In m_listLogTimeUpdate
                LogLn(tagLogTime.m_strLogMsg)
            Next
            '
            m_listLogTimeUpdate.Clear()
            '
        End If
    End Sub
    '
    Private Sub Btn_S00_Connect_Click(sender As Object, e As EventArgs) Handles Btn_S00_Connect_Start.Click
        If (m_bDebug) Then
            LogLn("Do Connection")
        End If
        '
        If (bIsConnSelectedRS232) Then
            ServiceConnectionRS232Init(True)
        Else
            Btn_S00_Connect_Click_USBHID(sender, e)
        End If

        '

        If (m_refObjRdrCmder.bIsConnected) Then
            TestTM002_Init()

            TestTM002_RFIDAntenna(m_nNumOfRounds)
            '2017 Oct 30, update here
            TestTM002_Cleanup()
            '
            PortClose(sender, e)
        End If
    End Sub
    '
    Private Sub PortClose(sender As Object, e As EventArgs)
        Dim strMsg As String = "Please Wait" 'm_pRefLang.GetMsgLangTranslation(ClassServiceLanguages._WTD_STRID.DISCONN_PREPARING)
        Dim nTimeout As Integer = IDGCmdTimeoutGet()
        Dim bTimerEnable As Boolean = Timer1.Enabled
        Try

            UIEnable(False)

            '=================[ Make Sure Timer Must Enabled for Timeout Mechanism @ IDG Command Response Processing]===================
            bTimerEnable = Timer1.Enabled
            Timer1.Enabled = True
            '
            WaitMsgShow(False, strMsg, True)



        Catch ex As Exception
            LogLn("[Err] PortClose() = " + ex.Message)
        Finally

            'm_refObjDevRS232.Close()
            If (m_refObjRdrCmder IsNot Nothing) Then
                m_refObjRdrCmder.ConnectionClose()
            End If

            m_objTransaction.Cleanup()
            WaitMsgHide()
            IDGCmdTimeOutRealTimeStop()
            m_refObjDevRS232 = Nothing
            m_refObjDevUSBHID = Nothing
            m_refObjRdrCmder = Nothing
            ' m_refObjTestScenario = Nothing

            UIEnable(False, 1)

            '=================[ Restore Timer Enabled Status , Specially in the case of  the caller already had disable Timer but still invoke PortClose() function ]===================
            Timer1.Enabled = bTimerEnable

            'This Value must be after m_refObjTestScenario.Cleanup() invocation.
            'm_bIsConnRS232First = False
        End Try
    End Sub
    '
    Private Sub Btn_S00_Close_Click(sender As Object, e As EventArgs) Handles Btn_S00_Stop_Close.Click
        If (m_bDebug) Then
            LogLn("Close Connection")
        End If
        '
        m_bIsBtnStopClicked = True
        '
        'TestTM002_Cleanup()
        ''
        'PortClose(sender, e)
    End Sub

    Private Sub Btn_S999_Clear_Click(sender As Object, e As EventArgs) Handles Btn_S999_Clear.Click
        LogCleanup()
    End Sub



    Public Sub UIEnable(bEnable As Boolean, Optional ByVal nCommMode As Integer = 0)
        Static s_lstBtn As List(Of Control) = New List(Of Control) From {
                    GroupBox_InterfaceMode, GrpBox_02_RndSel, GrpBox_SeekCardType, SplitContainer8
                    }
        Dim pnlRefFuncAll As Panel = SplitContainer4.Panel2

        Select Case (nCommMode)
            Case 0
                'Native, All Enable / Disable
                pnlRefFuncAll.Enabled = bEnable
            Case 1
                'Only Connect Enable
                pnlRefFuncAll.Enabled = True
                For Each itrBtn As Control In s_lstBtn
                    itrBtn.Enabled = True
                Next
                '
                Me.Btn_S00_Connect_Start.Enabled = True
                Me.Btn_S00_Stop_Close.Enabled = False
            Case 2
                'All Enable Except Connect Disable
                pnlRefFuncAll.Enabled = True
                For Each itrBtn As Control In s_lstBtn
                    itrBtn.Enabled = False
                Next
                '
                Me.Btn_S00_Connect_Start.Enabled = False
                Me.Btn_S00_Stop_Close.Enabled = True
        End Select

    End Sub
    '

    Private Sub MainForm_Load_AppVersionInfo_Title_Help_Init_FromExeName()
        ' Only First Time
        Static bIsFirstTime As Boolean = True
        '==========[ Setup Windows Form Title By Assembly Information's Assembly Name+Version ]=============
        'Execution Name Format : TS???_ReaderProductName_XXX
        'where ??? = 145,129, and so on; ReaderProductName, for ex : "Vendi","UnipayIII", "VendIII" and the like
        'Example : AssembleName = USBTool_V1.00.001 --> strAppInfo(0) = "TS145", (1) = "UnipayIII"

        If (bIsFirstTime) Then

            'Dim strAppInfo As String() = Nothing
            ''TODO :  ReaderName --> This will depend on connected device info after connection.
            'strAppInfo = Split(strGetExecutionFileNameNoPath, "_")
            'm_strName001_TS = strAppInfo(0)
            'm_strName002_Reader = strAppInfo(1)
            'm_strName003_ReaderExt = ""
            '
            'If ((strAppInfo.Count > 2) And (Not strAppInfo(2).Equals(Nothing))) Then
            '    'There is extension name for specific product / customer
            '    'ex : TS146_UnipayIII_iBase, For iBase Specific Version.
            '    m_strName003_ReaderExt = strAppInfo(2)
            '    Me.m_strAppVersion = Me.m_strAppVersion + "-" + strAppInfo(2)
            'End If

            Me.m_strAppVersion = ", V" + Form01_Main.strAsmVersion() '+ " For " + m_strName002_Reader
            Me.Text = Me.Text + m_strAppVersion

            'Free Object Resource
            'Erase strAppInfo
            '
            bIsFirstTime = False
        End If
    End Sub
    '
    Private Sub MainForm_Load_AppVersionInfo_Title_Help_Init()
        MainForm_Load_AppVersionInfo_Title_Help_Init_FromExeName()
    End Sub
    '

    '''////////////////////////////////////////////////////////////////////////////////////////////////////
    ''' \fn Private Sub Form01_Main_Load(sender As Object, e As EventArgs) Handles MyBase.Load If (m_pInstance Is Nothing) Then m_pInstance = Me End If ' Me.m_txtBoxTimeout = TextBox4_TxnTimeout ' MainForm_Load_AppVersionInfo_Title_Help_Init() ' m_objReaderInfo = ClassReaderInfo.GetIntance() '=======[Table / List Master Instance]===========
    '''
    ''' \brief  Form 01 main load. Preparing, initialization of Form UI objects.
    '''         Internal/reference Objects, Timer, and so on.
    '''
    ''' \author Goofy Liu
    ''' \date   2018/4/16
    '''
    ''' \param  sender  Source of the event.
    ''' \param  e       Event information.
    '''////////////////////////////////////////////////////////////////////////////////////////////////////

    Private Sub Form01_Main_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        If (m_pInstance Is Nothing) Then
            m_pInstance = Me
        End If
        '
        Me.m_txtBoxTimeout = TextBox4_TxnTimeout
        '
        MainForm_Load_AppVersionInfo_Title_Help_Init()
        '
        m_objReaderInfo = ClassReaderInfo.GetIntance()
        '=======[Table / List Master Instance]===========
        m_objTableListMaster = ClassTableListMaster.GetInstance()
        '
        m_objRdrCmderIDGGR = New ClassReaderProtocolIDGGR(Me, m_objTableListMaster)
        'Turn On Debug Logs.
        'm_objRdrCmderIDGGR.bLogTurnOnOff = True
        '
        m_objRdCmderParser = ClassReaderCommanderParser.GetInstance() 'New ClassReaderCommanderParser

        'RS232 Objects
        m_refwinobjComBoRS232SelectPortName = ComboBox_PortSel_COM
        m_refwinobjComBoRS232SelectBaudRate = ComboBox_PortSel_Baudrate

        '============[ Transaction Stuffs ]============
        m_objTransaction = New ClassTransaction(m_objTableListMaster, Me, m_objRdrCmderIDGGR) ', m_objUIDriver, m_objRdrCmderIDGAR)
        '
        ProgressBarInit()
        '
        MainForm_Load_WaitTillDone_Dlg_Init()

        '=========[Log Stuffs]=========
        m_listLogTimeUpdate = New List(Of tagLogTimeUpdateInfo)
        m_blistLogTimeUpdateCriticalSection = False

        'Initialize Command Lines
        Dim pCmdLine As ClassCmdLineArgumentOptions = ClassCmdLineArgumentOptions.GetInstance()
        pCmdLine.ParseLineTokens(Environment.CommandLine)

        'RS232 Port Scan
        ServiceRS232PortScan(ComboBox_PortSel_COM)
        ServicePortGet() 'Select Item 0 if No Selection
        ServiceBuadrateGet() 'Select Item 0 if No Selection

        '2018 April 18, goofy_liu
        If (pCmdLine.bIsPollForToken_MifareType) Then
            'Hide Card Type Selection
            cbox_SeekCardType.Visible = False
            TabCtrl_EndMode.SelectedTab = TabPg_End_TWait
            TabCtrl_EndMode.TabPages.Remove(TabPg_End_Rounds)
        Else
            TabCtrl_EndMode.SelectedTab = TabPg_End_Rounds
            TabCtrl_EndMode.TabPages.Remove(TabPg_End_TWait)
        End If
        '
        '2018 April 19, goofy_liu, Mifare UI Specialized.
        'SplitContainer5.Visible = pCmdLine.bIsPollForToken_MifareType
        '
        UIEnable(False, 1)
    End Sub

    '''////////////////////////////////////////////////////////////////////////////////////////////////////
    ''' \fn Private Sub Form01_Main_FormClosed(sender As Object, e As FormClosedEventArgs) Handles MyBase.FormClosed Cleanup() End Sub Private Sub Cleanup() Btn_S00_Stop_Close.PerformClick() '"Clean up stuffs after Form Closed m_objRdCmderParser.Cleanup() m_objTransaction.Cleanup() If (m_refObjDevRS232 IsNot Nothing) Then m_refObjDevRS232.Close()
    '''
    ''' \brief  Form 01 main form closed. To clean up Main Form UI Object
    '''
    ''' \author Goofy Liu
    ''' \date   2018/4/16
    '''
    ''' \param  sender  Source of the event.
    ''' \param  e       Form closed event information.
    '''////////////////////////////////////////////////////////////////////////////////////////////////////

    Private Sub Form01_Main_FormClosed(sender As Object, e As FormClosedEventArgs) Handles MyBase.FormClosed
        Cleanup()
    End Sub

    Private Sub Cleanup()
        Btn_S00_Stop_Close.PerformClick()
        '"Clean up stuffs after Form Closed
        m_objRdCmderParser.Cleanup()
        m_objTransaction.Cleanup()
        If (m_refObjDevRS232 IsNot Nothing) Then
            m_refObjDevRS232.Close()
        End If
        '
        If (m_refObjDevUSBHID IsNot Nothing) Then
            m_refObjDevUSBHID.Close()
        End If
        '
        m_listTxRxSimDat.Clear()
    End Sub

    Private Shared Function strAsmVersion() As Object
        'Translate Microsoft Version Format to IDTECH Version format
        ' x.y.z.w --> x.yz.00w
        Dim strTmp As String
        Dim strTmpLst As String()
        strTmp = GetType(Form01_Main).Assembly.GetName().Version.ToString()
        strTmpLst = Split(strTmp, ".")

        strTmp = strTmpLst(0) + "." + strTmpLst(1) + strTmpLst(2)
        If (strTmpLst(3).Length() = 2) Then
            strTmp = strTmp + ".0" + strTmpLst(3)
        Else
            strTmp = strTmp + ".00" + strTmpLst(3)
        End If
        Erase strTmpLst
        ' + ".00" + strTmpLst(3)
        Return strTmp
    End Function

    Sub SystemProductAutoDetectionPicSelect(image As Image)
        If (Not m_bIsHWTestOnly) Then
            LogLn("[Note] the Reader photo is not ready..")
        End If

    End Sub


    Public Sub ReInit()
        m_bIsTimeLogDumpingRecvDone = False
        m_bIsTimeLogDumpingRecvDoneEnable = False
        'm_pCmdLineInst = Nothing ' 2016 June 20, goofy_liu
        m_bWaitMsgDlgON = True
        'm_bServiceMode = False
    End Sub

    Public Sub New()

        ' This call is required by the designer.
        InitializeComponent()

        ' Add any initialization after the InitializeComponent() call.
        ReInit()
        '    Private m_strTSName As String
        'm_strName002_Reader = "[UnknownReader]"
        'm_strName001_TS = "[Not Initial Yet]"
        '=============[ Show Currently PC Installed Dot Net Framework Version in Console Window ]===========
        'Dim strExtBatchFile As String = Directory.GetCurrentDirectory + "\dll.bat"
        'Dim psi As New ProcessStartInfo(strExtBatchFile)
        'psi.RedirectStandardError = True
        'psi.RedirectStandardOutput = True
        'psi.CreateNoWindow = False
        'psi.WindowStyle = ProcessWindowStyle.Hidden
        'psi.UseShellExecute = False

        'Dim process As Process = process.Start(psi)

        GetVersionFromRegistry()

    End Sub
    '

    '
    Private m_strDefUsbHidPid As String = ""
    Private m_strDefUsbHidVid As String = ""

    Private WriteOnly Property npropUsbHidVidFromUI As String
        Set(value As String)
            m_strDefUsbHidVid = value
        End Set
    End Property


    Private WriteOnly Property npropUsbHidPidFromUI As String
        Set(value As String)
            m_strDefUsbHidPid = value
        End Set
    End Property
    '
    ReadOnly Property bIsConnected As Boolean
        Get
            If (m_refObjRdrCmder Is Nothing) Then
                Return False
            End If

            Return m_refObjRdrCmder.bIsConnected
        End Get
    End Property
    '
    ReadOnly Property bIsConnRS232 As Boolean
        Get
            If (m_refObjRdrCmder Is Nothing) Then
                Return False
            End If

            Return m_refObjRdrCmder.bIsConnectedRS232
        End Get
    End Property

    ReadOnly Property bIsConnUSBHID As Boolean
        Get
            If (m_refObjRdrCmder Is Nothing) Then
                Return False
            End If

            Return m_refObjRdrCmder.bIsConnectedUSBHID
        End Get
    End Property


    ReadOnly Property bIsConnSelectedUSBHID As Boolean
        Get
            Return TabCtrl_CommIntf.SelectedTab.Equals(TabPg_Intf_USB)
        End Get
    End Property

    ReadOnly Property bIsConnSelectedRS232 As Boolean
        Get
            Return TabCtrl_CommIntf.SelectedTab.Equals(TabPg_Intf_2_RS232)
        End Get
    End Property

    Private Function Button_OpenPort_USBHID_Click_ProbingReaderCommanderType(ptrCBFProgressBarUpdate As Action(Of Integer), nProgressCnt As Integer, Optional ByVal nIDGRdrCmdrType As ClassReaderInfo.ENU_FW_PLATFORM_TYPE = ClassReaderInfo.ENU_FW_PLATFORM_TYPE.AR) As Boolean
        'Dim bFound As Boolean = False

        Select Case (nIDGRdrCmdrType)
            Case ClassReaderInfo.ENU_FW_PLATFORM_TYPE.AR
                m_refObjRdrCmder = m_objRdrCmderIDGGR

                'm_refObjTestScenario = m_objTestScenarioIDGAR
                If (Not m_bIsHWTestOnly) Then
                    LogLn("[Err] AR Platform Is Not Supported !!")
                End If

                Return False
            Case ClassReaderInfo.ENU_FW_PLATFORM_TYPE.GR, ClassReaderInfo.ENU_FW_PLATFORM_TYPE.FW, ClassReaderInfo.ENU_FW_PLATFORM_TYPE.NEO_IDG
                m_refObjRdrCmder = m_objRdrCmderIDGGR
                'm_refObjTestScenario = m_objTestScenarioIDGGR
            Case Else
                Return False
        End Select
        '----[ 2017 Oct 30, Developer Configuration ]----
        m_refObjRdrCmder.bLogTurnOnOff = m_bIsTxRxLog
        m_refObjRdrCmder.bIsHWTestOnly = m_bIsHWTestOnly
        '----[ End of Developer Configuration ]----
        '
        If (bIsConnSelectedUSBHID) Then
            'USB-HID / USB-KB
            m_refObjRdrCmder.ConnectionOpen(m_strDefUsbHidVid, m_strDefUsbHidPid)

            If (Not m_refObjRdrCmder.bIsConnected) Then
                Return False
            End If

            m_strDefUsbHidVid = m_refObjRdrCmder.m_devUSBHID.strpropGetVIDCurrent
            m_strDefUsbHidPid = m_refObjRdrCmder.m_devUSBHID.strpropGetPIDCurrent
        ElseIf (bIsConnSelectedRS232) Then
            'RS232, UART
        Else
            'Unknown Interface Used
            Return False
        End If


        If (ptrCBFProgressBarUpdate IsNot Nothing) Then
            ptrCBFProgressBarUpdate(nProgressCnt)
        End If

        If (Not m_refObjRdrCmder.bIsConnected()) Then
            Return False
        End If

        'This is used by ServiceGetStrConnectionMode() Routines
        m_refObjDevUSBHID = m_refObjRdrCmder.m_devUSBHID


        If ((Not m_refObjRdrCmder.bIsConnectedUSBHID_KBWedge) And (Not m_refObjRdrCmder.bIsConnectedUSBHID)) Then
            'USB-KB mode we don't set these command
            '---------------------------------[ Trun Off PT & Cust LCD Mode ]---------------------------------------
            'Turn Off Pass through mode
            'm_refObjTestScenario.TestTM_Common_PassthroughMode_And_ThrowErrException(False, False)
            m_refObjRdrCmder.SystemSetPassthroughMode(False)

            'Turn Off Custom Display Interface
            'm_refObjTestScenario.TestTM_Common_UI_LCDCustomDisplayMode_And_ThrowErrExceptinOccured_Deprecated(False, False)
            m_refObjRdrCmder.SystemLCDCustomDisplayModeSetOff()
        End If
      
        '------------[ Ping the Reader & Check Result while in Transaction None  Debug Mode ]----------------
        If (Not m_bDebug) Then
            'GetFirmwareVersionAndFigureOutProductType(m_refObjRdrCmder, m_objReaderInfo)
            'Ping the reader device is alive or not, Note: Please use 5 seconds to ping the device
            IDGCmdTimeoutSet(1)
            m_refObjRdrCmder.SystemPing(IDGCmdTimeoutGet())

            '------------[ Check If Status Is Not Ready ]----------------
            If Not (m_refObjRdrCmder.bIsRetStatusOK) Then
                m_refObjRdrCmder.ConnectionClose()
                Return False
            End If
        End If

        Return True
    End Function
    '
    Private Sub Btn_S00_Connect_Click_USBHID(sender As Object, e As EventArgs)
        Dim nProgressCnt, nProgressCntOffsetInLoop, nProgressMax As Integer
        Dim nTimeout As Integer = IDGCmdTimeoutGet()
        Dim nStepsProductPlatformIdentification As Integer = 0
        Dim bIsConnected As Boolean = False
        Dim nIDGType As ClassReaderInfo.ENU_FW_PLATFORM_TYPE
        'Dim pCDFCL As ClassDataFilesCmpLog = ClassDataFilesCmpLog.GetInstance()
        'Dim refObjTS As ClassTestScenarioBasic = m_refObjTestScenario
        Dim nProdId As ClassReaderInfo.ENUM_PD_IMG_IDX
        Dim strMsgConnErr As String = "[Interface][USB HID] UUT not be able connected"
        '
        'Button_LogSaveFile.PerformClick()
        LogCleanup()

        ReInit()

        nProgressCnt = 1
        'nProgressCntOffsetInLoop value is judged by while loops worst case steps
        nProgressCntOffsetInLoop = 6
        ' Use GR Test Scenario to get number of worst case steps
        'If (refObjTS Is Nothing) Then
        '    refObjTS = m_objTestScenarioIDGGR
        'End If
        UIEnable(False)
        nProgressMax = nProgressCntOffsetInLoop '+ 4 + refObjTS.InitProgressBarPrepare()
        Try
            '=======================[ Start Reader Platform Identification Process ]=========================
            '--------------------[ Start System Checking ]--------------------
            WaitMsgHide()
            'WaitMsgShow(False, m_pRefLang.GetMsgLangTranslation(ClassServiceLanguages._WTD_STRID.CONN_CONNING)) ' "Connecting...")
            WaitMsgShow(False, "Connecting...")

            'bResult = False
            'Config Time out up to 5 seconds
            IDGCmdTimeoutSet(5)

            'Using Default UI Interface Setting of Communication Interface
            Me.ProgressBarStart(nProgressMax)
            '=======================[ To Probe Reader Commander Type : AR or GR ]===========================

            For Each iteratorPlaform In m_objReaderInfo.m_dictPlatformNameStr2EnuVal
                If (Button_OpenPort_USBHID_Click_ProbingReaderCommanderType(AddressOf Me.ProgressBarUpdate, nProgressCnt, iteratorPlaform.Value)) Then
                    nIDGType = iteratorPlaform.Value
                    If ((m_refObjRdrCmder.bIsConnectedUSBHID_KBWedge) Or (m_refObjRdrCmder.bIsConnectedUSBHID)) Then
                        '- [ USB-KB Mode, simply setting is enough ]-
                        m_objReaderInfo.SetProduct(m_refObjRdrCmder.propUSBHIDGetProductNameID)
                        'Set Default Platform - NEO 1.0
                        m_objReaderInfo.bIsPlatform(ClassReaderInfo.ENU_FW_PLATFORM_TYPE.NEO_IDG)
                        'Connected
                        bIsConnected = True

                        Exit For
                    Else
                        Dim lstHWInfo As List(Of String) = New List(Of String)()

                        Dim io_nNativePrdID As ClassReaderInfo.ENUM_PD_IMG_IDX = m_refObjRdrCmder.propUSBHIDGetProductNameID

                        m_refObjRdrCmder.SystemGetHardwareInfo(lstHWInfo)
                        If (Not m_refObjRdrCmder.bIsRetStatusOK) Then
                            '
                            'Continue For 'Dont Care Error Response.
                        End If
                        '- [ New Method ]-
                        'Possible has sub-reader type, TS needs to perform advanced Version /HW Info Checks
                        'For Ex: Unipay III --> Branches to Unipay III, VP4880, VP4880E, and VP4880C.
                        ' All of them have same VID:PID
                        If m_objReaderInfo.SetProduct_LV2(io_nNativePrdID, String.Join(",", lstHWInfo.ToArray())) Then
                            m_refObjRdrCmder.propUSBHIDGetProductNameID = io_nNativePrdID
                        End If
                        '
                        lstHWInfo.Clear()
                    End If
                    '
                    If (Not m_objReaderInfo.bIsPlatform(iteratorPlaform.Value)) Then
                        'In correct Setting between Reader Info Instance And Reader Commander Instance
                        m_refObjRdrCmder.ConnectionClose()
                        Me.ProgressBarUpdate(nProgressCnt)
                        nProgressCntOffsetInLoop = nProgressCntOffsetInLoop - 1
                        Continue For
                    End If
                    bIsConnected = True
                    Exit For
                End If
                Me.ProgressBarUpdate(nProgressCnt)
                nProgressCntOffsetInLoop = nProgressCntOffsetInLoop - 1
            Next
            Me.ProgressBarUpdate(nProgressCntOffsetInLoop)

            '==================[ No Supported Product /UUT found ]=====================
            If (Not bIsConnected) Then
                'Not Found, Return Error
                WaitMsgHide()
                Me.ProgressBarEnd()
                LogLn("[Warning] It  couldn't find Compatible Reader Info @ USB HID Interface")
                'MessageBox.Show("[Interface][USB HID] UUT not found", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error)
                'Return

                'AR Platform, use Vend III as class model
                If (Button_OpenPort_USBHID_Click_ProbingReaderCommanderType(Nothing, 10, ClassReaderInfo.ENU_FW_PLATFORM_TYPE.GR)) Then
                    m_objReaderInfo.SetProduct(ClassReaderInfo.ENUM_PD_IMG_IDX.NEO_UNIPAY_1D5, ClassReaderInfo.ENU_FW_PLATFORM_TYPE.GR)
                Else
                    'If (Not bIsEngMode_HWTestONLY) Then
                    '    MessageBox.Show(strMsgConnErr, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error)
                    'End If

                    LogLn(strMsgConnErr)

                    Return
                End If

                'End If
            End If
            'End If  'If (Not Button_OpenPort_USBHID_Click_DefaultSetting())
            '
            Me.ProgressBarUpdate(nProgressMax)
            Me.ProgressBarEnd()

            'm_refObjRdrCmder.bLogTurnOnOff = CheckBox_Engineer_Debug_On.Checked
            'm_refObjRdrCmder.bIsHWTestOnly = ChkBox_EngMode_HWTestMode.Checked
            '
            ConnectDeviceConnectedDevInit_Common(Nothing, 0)
            '
            WaitMsgHide()
            'WaitMsgShow(False, m_pRefLang.GetMsgLangTranslation(ClassServiceLanguages._WTD_STRID.TEST_PREPARING)) ' "Preparing Test...")
            WaitMsgShow(False, "Preparing Test...")
            '
            'MainForm_Load_CmdLineProcess_After_Connection()

            '------------[ Save Successful Setting ]-------------
            If (m_objReaderInfo Is Nothing) Then
                nProdId = ClassReaderInfo.ENUM_PD_IMG_IDX.UNKNOWN_MAX
            Else
                nProdId = m_objReaderInfo.GetProductId
            End If
            'If (Not pCDFCL.XMLCfgFileSave(ClassReaderProtocolBasic.ENU_DEV_INTERFACE.INTF_USBHID, m_objReaderInfo.GetProductId, Me.npropRS232BaudrateFromUI)) Then
            '    'Do some error handling here if it's required.
            'End If
            If (m_refObjRdrCmder.bIsConnected()) Then
                bIsConnected = True

                LogLn("Connected")
                '2017 Oct 25, Assign V2 Protocol As Default
                m_refObjRdrCmder.SetVivoProctlType = ClassReaderInfo.ENU_PROTCL_VER._V2
            Else
                bIsConnected = False
            End If

        Catch ex As Exception
            LogLn("[Err] Btn_S00_Connect_Click_USBHID() = " + ex.Message)
        Finally
            If (bIsConnected) Then
                UIEnable(False, 2)
                '
                'Update Interface Mode
                If (m_refObjRdrCmder.bIsConnectedUSBHID_KBWedge) Then
                    SetIntfMode_KBWedge()
                Else
                    SetIntfMode_HID()
                End If
            Else
                UIEnable(False, 1)
            End If

            WaitMsgHide()

        End Try
    End Sub

    Private Function ConnectDeviceConnectedDevInit_Common(pfuncUpdate As Action(Of Integer), nProgressCnt As Integer) As Boolean
        m_objTransaction.UpdateOpenConnection(m_refObjRdrCmder)
        If (pfuncUpdate IsNot Nothing) Then
            pfuncUpdate(nProgressCnt)
        End If
        '
        Return True
    End Function
    '

    '==============================================
    ' Ret = True : Normal End
    '       = False : Interrupted by User
    Public Shared Function AppSleep(Optional nLoopPeriodMS As Integer = 1000, Optional ByVal bWaitMsg As Boolean = False) As Boolean
        Dim nLoopPeriodCountMax As Integer = Environment.TickCount() + nLoopPeriodMS
        Dim nLoopPeriodMSInterval As Integer = 10
        Dim pMain As Form01_Main = Form01_Main.GetInstance()
        Dim bHide As Boolean = False
        Dim bRet As Boolean = True

        If (pMain.bIsWaitMsgEnd And bWaitMsg) Then
            pMain.WaitMsgHide()
            pMain.WaitMsgShow(True, "Waiting for next round to re-test")
            bHide = True
        End If
        '
        While (True)
            Thread.Sleep(nLoopPeriodMSInterval)
            If (Environment.TickCount() >= nLoopPeriodCountMax) Then
                'nLoopPeriodMSInterval = 100
                Exit While
            End If
            Application.DoEvents()
            If (pMain.bIsWaitMsgEnd And bWaitMsg) Then
                bRet = False
                Exit While
            End If
        End While

        If (bHide) Then
            pMain.WaitMsgHide()
        End If
        '
        Return bRet
    End Function
    '====[ Function Call ]====
    Private m_bIsServiceMode As Boolean = False
    Private Sub SetIntfMode_KBWedge()
        m_bIsServiceMode = True
        RdoBtn_S00_01_USB_KB_WEDGE.Checked = True
        'm_pRadBtn_Pre = RdoBtn_S00_01_USB_KB_WEDGE '2017 Oct 25, remove it, Felica SDK doesn't own it
        m_bIsServiceMode = False
    End Sub
    Private Sub SetIntfMode_HID()
        m_bIsServiceMode = True
        RdoBtn_S00_01_USB_HID.Checked = True
        'm_pRadBtn_Pre = RdoBtn_S00_01_USB_HID  '2017 Oct 25, remove it, Felica SDK doesn't own it
        m_bIsServiceMode = False
    End Sub
    '


    ' Type 01 - End Rounds Mode
    Private m_nNumOfRounds As Integer = -1
    Private m_strGetNumOfRounds_Manual As String = "MANUAL" 'Upper Case
    ' Type 02 - End Time Limit Mode
    Private m_nTimeLimitSec As Integer = 60 'secs

    Public ReadOnly Property bIsGetNumOfRounds_Manual As Boolean
        Get
            Dim strGet As String

            If (-1 = Rnd_01_Cmbox_Sel_Rnd.SelectedIndex) Then
                'Force to input item 01
                Rnd_01_Cmbox_Sel_Rnd.SelectedIndex = 0
                Return False
            End If

            strGet = Rnd_01_Cmbox_Sel_Rnd.SelectedItem

            strGet = strGet.ToUpper()
            Return m_strGetNumOfRounds_Manual.Equals(strGet)
        End Get
    End Property

    'Return
    '        If = -1 --> Invalid Num of Rounds, caller should stop next steps and does error handling.
    Private Function TestCommon_GetNumOfRounds() As Integer

        'A. If Selected Item != "Manual" -->
        If (Not bIsGetNumOfRounds_Manual) Then
            Dim strGet As String = Rnd_01_Cmbox_Sel_Rnd.SelectedItem

            m_nNumOfRounds = Conversion.Int(strGet)

            Rnd_02_Btn_Get_Rnd.Visible = False
        Else

            'B.If Selected Item = "Manual" -->
            Dim pForm02_GetInput As Form02_NumOfRoundsInput = Form02_NumOfRoundsInput.GetInstance()
            If (pForm02_GetInput.ShowDialog() = System.Windows.Forms.DialogResult.OK) Then
                ' 1. If m_nNumOfRounds = -1 --> Display Input Dialog box to select number
                'Update Round Num
                m_nNumOfRounds = pForm02_GetInput.prop_01_GetNumOfRounds
                ' Show Cust Input Buttons
                Rnd_02_Btn_Get_Rnd.Visible = True
                '
                Rnd_02_Btn_Get_Rnd.Text = m_nNumOfRounds.ToString()
                If (Rnd_02_Btn_Get_Rnd.Text.Equals("0")) Then
                    Rnd_02_Btn_Get_Rnd.Text = "Stop by User"
                End If
            Else
                ' If canceled dialog box --> stop input
                'return -1
                m_nNumOfRounds = -1

                Rnd_02_Btn_Get_Rnd.Visible = False
            End If
        End If

        Return m_nNumOfRounds
    End Function


    Private Sub Rnd_01_Cmbox_Sel_Rnd_SelectedIndexChanged(sender As Object, e As EventArgs) Handles Rnd_01_Cmbox_Sel_Rnd.SelectedIndexChanged
        TestCommon_GetNumOfRounds()
    End Sub

    Private Sub Rnd_02_Btn_Get_Rnd_Click(sender As Object, e As EventArgs) Handles Rnd_02_Btn_Get_Rnd.Click
        Dim pForm02_GetInput As Form02_NumOfRoundsInput = Form02_NumOfRoundsInput.GetInstance()
        If (pForm02_GetInput.ShowDialog() = System.Windows.Forms.DialogResult.OK) Then
            ' 1. If m_nNumOfRounds = -1 --> Display Input Dialog box to select number
            'Update Round Num
            m_nNumOfRounds = pForm02_GetInput.prop_01_GetNumOfRounds
            ' Show Cust Input Buttons
            Rnd_02_Btn_Get_Rnd.Visible = True
            '
            Rnd_02_Btn_Get_Rnd.Text = m_nNumOfRounds.ToString()
            If (Rnd_02_Btn_Get_Rnd.Text.Equals("0")) Then
                Rnd_02_Btn_Get_Rnd.Text = "Stop by User"
            End If
        End If
    End Sub

    '
    Public Function GetPortList_RS323() As SortedList(Of String, String)
        Dim listPort As SortedList(Of String, String) = New SortedList(Of String, String)

#If 0 Then
        'Not Support RS232 port scan list

        Dim nIdx, nIdxMax As Integer
        Dim strComPort As String

        If (m_refwinobjComBoRS232SelectPortName Is Nothing) Then
            MainForm_Load_RS232_PortScan()
        End If

        nIdxMax = m_refwinobjComBoRS232SelectPortName.Items.Count - 1

        For nIdx = 0 To nIdxMax
            strComPort = m_refwinobjComBoRS232SelectPortName.Items(nIdx)
            listPort.Add(strComPort, strComPort)
        Next
#End If
        Return listPort
    End Function

    '
    Public ReadOnly Property bIsEndMode_00_Rounds As Boolean
        Get
            Return TabCtrl_EndMode.SelectedTab.Equals(TabPg_End_Rounds)
        End Get
    End Property
    '
    ' 'Return
    '        If = -1 --> Invalid Num of Rounds, caller should stop next steps and does error handling.
    Private Function TestCommon_GetNumOfTimeWaitSec() As Integer

        Dim strGet As String = m_CBox_End_TWaitTime.SelectedItem
        Dim bRoundMode As Boolean = False
        If (strGet.Contains("R")) Then
            'L1 = 1 Round
            'L100 = 100 Rounds
            strGet = strGet.Replace("R", "")
            bRoundMode = True
        End If
        '
        m_nTimeLimitSec = Conversion.Int(strGet)
        '
        If (bRoundMode) Then
            '1 Rounds = -1
            '100 Rounds = -100
            m_nTimeLimitSec = 0 - m_nTimeLimitSec
        End If

        Return m_nTimeLimitSec
    End Function
    '

    Private Sub m_CBox_End_TWaitTime_SelectedIndexChanged(sender As Object, e As EventArgs) Handles m_CBox_End_TWaitTime.SelectedIndexChanged
        TestCommon_GetNumOfTimeWaitSec()
    End Sub
    '

    'Private m_listTxRxSimDat As List(Of String) = New List(Of String)()
    Private m_listTxRxSimDat As List(Of _TAG_PARSE_INFO_BLK) = New List(Of _TAG_PARSE_INFO_BLK)()



    Private Function TxRxSimDat_01_Load(ByVal i_strTxRxSimDatFilePath As String) As Boolean
        Dim bRet As Boolean = False
        Dim streamReader As StreamReader
        Dim strTmp As String
        Dim strlstTmp() As String = Nothing
        Try
            While (True)

                ' Step 01 - Init
                'm_listTxRxSimDat.Clear()
                TxRxSimDat_Init()

                ' Step 02 - Read  All Text Contents.
                'Open File
                streamReader = File.OpenText(i_strTxRxSimDatFilePath)
                '
                strTmp = streamReader.ReadToEnd()

                ' Step 03 - Splitting text content into text lines
                strlstTmp = Split(strTmp, vbCrLf)
                If (strlstTmp Is Nothing) Then
                    Exit While
                End If
                If (0 = strlstTmp.Length) Then
                    Exit While
                End If

                ' Step 04 - Parse [TX] - Part of Each Line
                bRet = TxRxSimDat_01_Load_LineParse(strlstTmp)
                '
                Exit While
            End While
        Catch ex As Exception
            LogLn(ex.Message)
        Finally
            '
            If (strlstTmp IsNot Nothing) Then
                Erase strlstTmp
                strlstTmp = Nothing
            End If
            '
            ' Update Customized Mode Enable ?
            bIsTxRxSimDatMode = bRet
        End Try

        Return bRet
    End Function


    '====[
    Private m_bIsOpeningFileDlg As Boolean = False
    Private m_strTxRxSimDatFilePath As String = ""
    Private Sub m_CmbBox_MifareRunMode_SelectedIndexChanged(sender As Object, e As EventArgs) Handles m_CmbBox_MifareRunMode.SelectedIndexChanged
        If (1 = m_CmbBox_MifareRunMode.SelectedIndex) Then
            UIEnable(False)
            If (Not m_bIsOpeningFileDlg) Then
                m_bIsOpeningFileDlg = True
                '
                If (OpenFileDialog1.ShowDialog() = System.Windows.Forms.DialogResult.OK) Then
                    'Save text File Path
                    m_strTxRxSimDatFilePath = OpenFileDialog1.FileName
                    If (Not TxRxSimDat_01_Load(m_strTxRxSimDatFilePath)) Then
                        LogLn("Loaded Customized Tx/Rx Data Failed !!")
                        'Restore back to default mode
                        m_CmbBox_MifareRunMode.SelectedIndex = 0
                    Else
                        LogLn("Loaded Customized Tx/Rx Data OKAY !!")
                    End If
                Else
                    m_CmbBox_MifareRunMode.SelectedIndex = 0
                End If
                ' 
                m_bIsOpeningFileDlg = False
            End If
            UIEnable(True)
        End If
    End Sub

    Private Sub NumUpDwn_Timeout_TxRx_ValueChanged(sender As Object, e As EventArgs) Handles NumUpDwn_Timeout_TxRx.ValueChanged

    End Sub

    Private Sub ConnectDeviceRS232BtnClick(sender As Object, e As EventArgs)

    End Sub

    Private Sub Label2_Click(sender As Object, e As MouseEventArgs)

    End Sub

    Private Sub Label1_MouseDoubleClick(sender As Object, e As MouseEventArgs)

    End Sub

    Private m_pRadBtn_Pre As RadioButton = Nothing
    Private Sub RdoBtn_S00_01_USB_KB_WEDGE_CheckedChanged(sender As Object, e As EventArgs) Handles RdoBtn_S00_01_USB_KB_WEDGE.CheckedChanged, RdoBtn_S00_01_USB_HID.CheckedChanged
        Dim radbtn As RadioButton = CType(sender, RadioButton)
        Static s_bCritial As Boolean = False
        Dim bResult As Boolean = False
        Dim bLoop As Boolean = True
        Dim nStg As Integer = 0
        Dim byteMode As Byte = &H0
        Dim nRetry As Integer = 0
        Dim nRetryMax As Integer = 6
        Dim nTimeout As Integer = 2
        'If it's First time, we show a notification message to user.
        Static s_bIsFirstTime As Boolean = True
        '
        If (m_bIsServiceMode) Then
            'Service mode - do nothing
            Exit Sub
        End If

        'If it's not connected mode, we skip this operation.
        If (m_refObjRdrCmder Is Nothing) Then
            Return
        End If
        If (Not m_refObjRdrCmder.bIsConnected) Then
            Return
        End If
        '
        If (s_bCritial) Then
            Exit Sub
        End If
        s_bCritial = True
        '
        Try


            If (s_bIsFirstTime) Then
                MsgBox("Switching to the other mode will possibly take time up to 4 secs+. Please be patient.", MsgBoxStyle.Information Or MsgBoxStyle.OkOnly, "Notification")
                s_bIsFirstTime = False
            End If


            ' Step 00 - Same mode, do nothing
            If (m_pRadBtn_Pre Is radbtn) Then
                bResult = True
                Exit Try
            End If


            ProgressBarStart(4)
            ' Step 01 - Send Switch mode Message
            If (radbtn.Text.Contains("HID")) Then
                'HID
                byteMode = &H0
            Else
                'KB Wedge
                byteMode = &H1
            End If
            m_refObjRdrCmder.System_KBWedge_ModeSwitch(byteMode, nTimeout)
            If (Not m_refObjRdrCmder.bIsRetStatusOK) Then
                Exit Try
            End If

            ' Disable All UI, Show Wait Message
            UIEnable(False)
            WaitMsgShow(True, "Please Wait...", True, 10000)
            '
            While (bLoop)
                If (bIsWaitMsgEnd) Then
                    Exit Try
                End If
                '
                ProgressBarUpdate(1, False)
                '
                Select Case (nStg)
                    Case 0
                        'Step 02 - Close it
                        Btn_S00_Stop_Close.PerformClick()
                        nStg = nStg + 1

                    Case 1
                        'Step 03 - Wait for while, retrying up to as possible as max retry times (interval = 1000 ms).
                        AppSleep(1000)

                        nStg = nStg + 1
                        nRetry = nRetry + 1
                        If (nRetry >= nRetryMax) Then
                            nRetry = 0
                            'Hide Wait Message Box to prevent System Message Box being closed.
                            WaitMsgHide()
                            '2017 Sept 18, Fixed bug, if Retrying Result still failed, we stop the connection.
                            MsgBox("Can't switch to another mode and stop connection", MsgBoxStyle.Critical Or MsgBoxStyle.OkOnly, "Error")
                            Exit While
                        End If
                        '
                    Case 2
                        'Step 04 - Trying connection
                        m_strDefUsbHidVid = ""
                        m_strDefUsbHidPid = ""
                        m_refObjRdrCmder.ConnectionOpen(m_strDefUsbHidVid, m_strDefUsbHidPid)
                        'If the reader can't available, back to wait stage(=1)
                        If ((Not m_refObjRdrCmder.bIsConnectedUSBHID)
                            ) Then
                            nStg = 1
                            ProgressBarUpdate(-1, False)
                            Continue While
                        End If
                        If (
                               ((byteMode = &H1) And (Not m_refObjRdrCmder.bIsConnectedUSBHID_KBWedge)) _
                            Or ((byteMode = &H0) And (m_refObjRdrCmder.bIsConnectedUSBHID_KBWedge))
                            ) Then
                            nStg = 0
                            ProgressBarUpdate(-2, False)
                            Continue While
                        End If
                        '
                        nStg = nStg + 1
                    Case 3
                        'Step 05 -'detect it (open & ping command)
                        m_refObjRdrCmder.SystemPing(nTimeout)
                        'If the reader can't available, back to close stage (=0)
                        If (Not m_refObjRdrCmder.bIsRetStatusOK) Then
                            nStg = 0
                            ProgressBarUpdate(-3, False)
                            Continue While
                        End If
                        '
                        nStg = nStg + 1
                        bLoop = False
                        '
                        bResult = True
                End Select
            End While


        Catch ex As Exception
            LogLn("[Err] " + ex.Message)
        Finally

            If (Not bResult) Then
                'If Result is false, we will close connection.
                'Dont call btn.PerformClick() since here we already disabled the button.
                Btn_S00_Close_Click(Btn_S00_Stop_Close, e)
                ' Set previous Radio button as checked
                If (m_pRadBtn_Pre IsNot Nothing) Then
                    m_pRadBtn_Pre.Checked = True
                End If
                'Enable UI - Skip since the UI has been enable in Close procedure/function.
            Else
                'If it's Good done, will update relative variables.
                m_pRadBtn_Pre = radbtn
                'Enable UI - Connected
                UIEnable(False, 2)
            End If
            '
            'ProgressBarUpdate(5, True)
            ''
            'AppSleep(500)
            '
            ProgressBarEnd()

            WaitMsgHide()
            '
            s_bCritial = False
        End Try
    End Sub
End Class
