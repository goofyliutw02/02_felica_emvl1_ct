﻿'------------------------------------------------------------------------------------------
' File Description: This Class Instance is used for Transaction Action
'
'
'Imports WindowsApplication1
'------------------------------------------------------------------------------------------
Public Class ClassTransaction
    '----------------------------------------------------[Delegate Function type]----------------------------------------------------
    ' Setup Delegate(Callback) Functions
    ' Private Delegate Sub LogLnDelegate(ByRef strInfo As String)
    Private Shared LogLn As Form01_Main.LogLnDelegate = New Form01_Main.LogLnDelegate(AddressOf Form01_Main.GetInstance().LogLn)

    Private Shared m_bDebug As Boolean = False

    '----------------------------------------------------[Private Variables]----------------------------------------------------
    Private m_refwinformMainForm As Form01_Main
    Private m_refobjCTLM As ClassTableListMaster
    'Private m_refobjUIDriver As ClassUIDriver
    Private m_refobjReaderProtocolCmder As ClassReaderProtocolBasic

    Private m_dictTLVsTxnSend As SortedDictionary(Of String, tagTLV)
    Private m_bIsInterruptedByExternal As Boolean
    'Send Transaction Activate Command 02-01. <Note> Transaction Commands (ClassReaderCommander) must be responsible to print out Sent & Received Data Content in Hex Text Format. 
    'Private m_arrayByteTxnResponse As Byte()

    Public Structure TAG_CVM_LIST_DATA_ELEMENT
        'Public m_u32Amount1, m_u32Amount2 As UInt32
        Public m_byteCVMCode, m_byteCVMCoditionCodes As Byte
        '
        Public Sub Cleanup()

        End Sub
    End Structure
    Public Structure TAG_PHONE_MSG_TABLE_ELEMENT
        Public m_arrayBytePCIIMask As Byte()
        Public m_arrayBytePCIIValue As Byte()
        Public m_byteMessageIdentifier As Byte
        Public m_byteStatus As Byte
        '
        Public Sub Cleanup()
            If m_arrayBytePCIIMask IsNot Nothing Then
                Erase m_arrayBytePCIIMask
            End If
            If m_arrayBytePCIIValue IsNot Nothing Then
                Erase m_arrayBytePCIIValue
            End If
        End Sub
    End Structure
    'Info Section of  Parsed Response Data
    Public Structure TAG_TXN_PARSE_RESPONSE_INFO
        '--------------------------[ Array Byte Data for Parser  ]--------------------------
        Public m_arrayByteDataResponse As Byte()
        Public m_nOffset As Integer
        Public m_nIDGStatusCode As ClassReaderProtocolBasic.EnumSTATUSCODE2
        Public m_strIDGStatusCode As String

        Public m_nRFStatusCode As ClassReaderProtocolBasic.EnumSTATECODE_RF
        Public m_u16SW1 As UInt16
        Public m_u16SW2 As UInt16
        Public m_nTransactionErrorCode As ClassReaderProtocolBasic.EnumTXN_ERRCODE

        Public m_byteEncryption_01_AttributeByte As Byte

        '------------------------[ Date Time Info ]------------------------
        ' Time Elapsed In Milliseconds = DirectCast((m_infoDateTimeTxnEnd - m_infoDateTimeTxnBegin).TotalMilliseconds, Int64)
        Public m_infoDateTimeTxnBegin As Double
        Public m_infoDateTimeTxnEnd As Double
        Public m_infoDateTimeTxnEndDateTime As DateTime


        Private m_DF8129_CVM_byte_Val As Byte
        Private m_DF8129_CVM_strMsg As String
        Private m_DF8129_UIRequestOnOutCome_Bool As Boolean
        Private m_DF8129_UIRequestOnRestart_Bool As Boolean
        Private m_DF8129_DataRecord_Bool As Boolean
        Private m_DF8129_Discretionary_Bool As Boolean
        Private m_DF8129_ReceiptPresents_Bool As Boolean
        Private m_bTransactionStopped As Boolean
        Private m_bIsInterrupted As Object
        Public m_bIsEncMSRButNoDUKPTKeyMode As Boolean



        ReadOnly Property bIsProtocolOnlineAuthReq As Boolean
            Get
                Return (m_nIDGStatusCode = ClassReaderProtocolBasic.EnumSTATUSCODE2.x23RequestOnlineAuthorization)
            End Get
        End Property

        ReadOnly Property bIsProtocolUserInterfaceEvent As Boolean
            Get
                Return (m_nIDGStatusCode = ClassReaderProtocolBasic.EnumSTATUSCODE2.x0EUserInterfaceEvent)
            End Get
        End Property

        ReadOnly Property bIsProtocolRetStatusOK As Boolean
            Get
                Return (m_nIDGStatusCode = ClassReaderProtocolBasic.EnumSTATUSCODE2.x00OK)
            End Get
        End Property

        ReadOnly Property bIsProtocolRetStatusFailOrNACK As Boolean
            Get
                Return (m_nIDGStatusCode = ClassReaderProtocolBasic.EnumSTATUSCODE2.x0AFailedNACK)
            End Get
        End Property

        ReadOnly Property bIsProtocolRetStatusFaileWithTransactionStatusOnlineAuthReq As Boolean
            Get
                Return (((m_nIDGStatusCode = ClassReaderProtocolBasic.EnumSTATUSCODE2.x0AFailedNACK) And (m_nTransactionErrorCode = ClassReaderProtocolBasic.EnumTXN_ERRCODE.x23RequestOnlineAuthorization)) Or (m_nIDGStatusCode = ClassReaderProtocolBasic.EnumSTATUSCODE2.x23RequestOnlineAuthorization))
            End Get
        End Property

        WriteOnly Property TransactionStoppedSet As Boolean
            Set(value As Boolean)
                m_bTransactionStopped = True
            End Set
        End Property
        ReadOnly Property TransactionStoppedGet As Boolean
            Get
                Return m_bTransactionStopped
            End Get
        End Property

        ReadOnly Property strGetTransactionResult As String
            Get
                Dim strGTR As String = ""
                ClassReaderProtocolBasic.GetTransactionErrCodeMsg(m_nTransactionErrorCode, strGTR, m_nRFStatusCode)
                Return strGTR
            End Get
        End Property


        ReadOnly Property strGetTransactionResultRFErrCode As String
            Get
                Dim strGTRRFC As String = ""
                ClassReaderProtocolBasic.GetTransactionRFErrCodeMsg(m_nRFStatusCode, strGTRRFC)
                Return strGTRRFC
            End Get
        End Property

        'Reference Doc: IDG GR1.2.9 Rev 1
        ReadOnly Property bIsTransactionRFErrorAllowedRetryTransaction As Boolean
            Get
                If (m_nTransactionErrorCode = ClassReaderProtocolBasic.EnumTXN_ERRCODE.x20CardReturnedErrorStatus) Then
                    If ClassReaderProtocolBasic.m_dictRFErrCodeRetryTransactionAllowedListForTransactionErr20h.Contains(m_nRFStatusCode) Then
                        Return True
                    End If
                End If
                If (m_nTransactionErrorCode = ClassReaderProtocolBasic.EnumTXN_ERRCODE.x30CardDidNotRespond) Then
                    If ClassReaderProtocolBasic.m_dictRFErrCodeRetryTransactionAllowedListForTransactionErr30h.Contains(m_nRFStatusCode) Then
                        Return True
                    End If
                End If
                '
                Return False
            End Get
        End Property

        Public Sub TxnTimeBegin()
            m_infoDateTimeTxnBegin = (DateTime.Now - New DateTime(1970, 1, 1)).TotalMilliseconds
        End Sub
        Public Sub TxnTimeEnd()
            m_infoDateTimeTxnEndDateTime = DateTime.Now
            m_infoDateTimeTxnEnd = (DateTime.Now - New DateTime(1970, 1, 1)).TotalMilliseconds
        End Sub

        Public ReadOnly Property TxnTimeElapasedInSecs As Double
            Get
                Return (m_infoDateTimeTxnEnd - m_infoDateTimeTxnBegin) / 1000
            End Get
        End Property
        Public ReadOnly Property TxnTimeEndWhen As DateTime
            Get
                Return m_infoDateTimeTxnEndDateTime
            End Get
        End Property

        '--------------------------[ Printout Message Collections ]--------------------------
        Enum EnumTxnResult2PrintoutIndent As Integer
            UI_TXN_RESULT_2_PRINTOUT_INDENT_MIN = 0
            UI_TXN_RESULT_2_PRINTOUT_INDENT0 = UI_TXN_RESULT_2_PRINTOUT_INDENT_MIN ' Left alias index = 0
            UI_TXN_RESULT_2_PRINTOUT_INDENT1 ' Left alias index = 1
            UI_TXN_RESULT_2_PRINTOUT_INDENT2  ' Left alias index = 2
            UI_TXN_RESULT_2_PRINTOUT_INDENT3  ' Left alias index = 3
            UI_TXN_RESULT_2_PRINTOUT_INDENT4  ' Left alias index = 4
            UI_TXN_RESULT_2_PRINTOUT_INDENT5  ' Left alias index = 5
            UI_TXN_RESULT_2_PRINTOUT_INDENT6  ' Left alias index = 6
            UI_TXN_RESULT_2_PRINTOUT_INDENT7  ' Left alias index = 7
            UI_TXN_RESULT_2_PRINTOUT_INDENT8  ' Left alias index = 8
            UI_TXN_RESULT_2_PRINTOUT_INDENT_MAX = UI_TXN_RESULT_2_PRINTOUT_INDENT6
        End Enum
        Public Structure TXN_RESPONSE_PARSER_2_PRINTOUT
            Public m_nLeftBoundaryIndx As EnumTxnResult2PrintoutIndent
            Public m_strMsg As String
        End Structure

        ' For Print out Transaction Response 2 Output Window Format Usage
        Public Shared m_dictTxnResponse2PrintoutIndent As Dictionary(Of EnumTxnResult2PrintoutIndent, String) = New Dictionary(Of EnumTxnResult2PrintoutIndent, String) From {
        {EnumTxnResult2PrintoutIndent.UI_TXN_RESULT_2_PRINTOUT_INDENT0, ""},
        {EnumTxnResult2PrintoutIndent.UI_TXN_RESULT_2_PRINTOUT_INDENT1, " "},
        {EnumTxnResult2PrintoutIndent.UI_TXN_RESULT_2_PRINTOUT_INDENT2, "  "},
        {EnumTxnResult2PrintoutIndent.UI_TXN_RESULT_2_PRINTOUT_INDENT3, "   "},
        {EnumTxnResult2PrintoutIndent.UI_TXN_RESULT_2_PRINTOUT_INDENT4, "    "},
        {EnumTxnResult2PrintoutIndent.UI_TXN_RESULT_2_PRINTOUT_INDENT5, "     "},
        {EnumTxnResult2PrintoutIndent.UI_TXN_RESULT_2_PRINTOUT_INDENT6, "      "},
        {EnumTxnResult2PrintoutIndent.UI_TXN_RESULT_2_PRINTOUT_INDENT7, "       "},
        {EnumTxnResult2PrintoutIndent.UI_TXN_RESULT_2_PRINTOUT_INDENT8, "        "}
        }

        '
        Public m_nIndentOp As EnumTxnResult2PrintoutIndent
        Public m_listTxnResponseMsg As List(Of TXN_RESPONSE_PARSER_2_PRINTOUT)
        Public Sub AddIndent(Optional ByVal nAddOff As Integer = 1)
            m_nIndentOp = m_nIndentOp + nAddOff
            If m_nIndentOp > EnumTxnResult2PrintoutIndent.UI_TXN_RESULT_2_PRINTOUT_INDENT_MAX Then
                m_nIndentOp = EnumTxnResult2PrintoutIndent.UI_TXN_RESULT_2_PRINTOUT_INDENT_MAX
            End If
        End Sub
        Public Sub DecIndent(Optional ByVal nDecOff As Integer = 1)
            m_nIndentOp = m_nIndentOp - nDecOff
            If m_nIndentOp < EnumTxnResult2PrintoutIndent.UI_TXN_RESULT_2_PRINTOUT_INDENT_MIN Then
                m_nIndentOp = EnumTxnResult2PrintoutIndent.UI_TXN_RESULT_2_PRINTOUT_INDENT_MIN
            End If
        End Sub

        '--------------------------[ Track1 ]--------------------------
        'Length: 1 byte
        'Data : ASCII
        Public m_byteMSDLen1 As Byte
        Public m_arrayByteMSDData1_Or_OnlineAuthAmountRequested As Byte()

        '--------------------------[ Track2 ]--------------------------
        'Length: 1 byte
        'Data : ASCII
        Public m_byteMSDLen2 As Byte
        Public m_arrayByteMSDData2 As Byte()

        '--------------------------[ Track3 ]--------------------------
        'Length: 1 byte
        'Data : ASCII
        Public m_byteMSDLen3 As Byte
        Public m_arrayByteMSDData3 As Byte()

        '--------------------------[ Tag: E1 = DE 055, Clearing Record Present ]--------------------------
        ' 1 byte = (&H01,Presents); (&H00,Absence) 
        Public m_byteClearingRecordFlags As Byte '&H01 = Presents; &H00 = N/A
        Public m_dictTlvsClearingRecordE1_DE055 As SortedDictionary(Of String, tagTLV)

        '--------------------------[Rest Are TLVs ]--------------------------
        'Public m_dictTlvsTxnResult As Dictionary(Of String, tagTLV)
        Public m_listTlvsTxnResult As List(Of tagTLV)
        Public m_dictTlvsTxnResult As SortedDictionary(Of String, tagTLV)
        Public m_dictTlvsTxnResult_6F_FileControlInformationTemplate As SortedDictionary(Of String, tagTLV)
        Public m_dictTlvsTxnResult_77_ResponseMessageTemplateFormat2 As SortedDictionary(Of String, tagTLV)
        Public m_dictTlvsTxnResult_A5_FileControlInformationProprietaryTemplate As SortedDictionary(Of String, tagTLV)
        Public m_dictTlvsTxnResult_BF0C_FileControlInformationIssureDiscretionaryData As SortedDictionary(Of String, tagTLV)
        Public m_dictTlvsTxnResult_FFEE01 As SortedDictionary(Of String, tagTLV)
        Public m_listTlvsTxnResult_FFEE04 As List(Of tagTLV) 'Since FFEE05 (Signal OUT) could come in multiple tags , use List To management it
        Public m_dictTlvsTxnResult_FFEE05 As SortedDictionary(Of String, tagTLV)
        Public m_dictTlvsTxnResult_FF8106 As SortedDictionary(Of String, tagTLV)
        Public m_dictTlvsTxnResult_FF8105 As SortedDictionary(Of String, tagTLV)
        Public m_dictTlvsTxnResult_FF8103 As SortedDictionary(Of String, tagTLV)
        Public m_dictTlvsTxnResult_FF8102 As SortedDictionary(Of String, tagTLV)

        Public m_CVMList_u32Amount1, m_CVMList_u32Amount2 As UInt32

        Public m_dictCVMList As List(Of TAG_CVM_LIST_DATA_ELEMENT)
        Public m_dictPhoneMessageTable As List(Of TAG_PHONE_MSG_TABLE_ELEMENT)
        '
        Sub New(ByRef tagIDGCmdDataInfo As tagIDGCmdData)
            '
            '
            '
            'm_dictTlvsTxnResult = New Dictionary(Of String, tagTLV)
            m_listTlvsTxnResult = New List(Of tagTLV)
            m_dictTlvsTxnResult = New SortedDictionary(Of String, tagTLV)
            m_dictTlvsTxnResult_6F_FileControlInformationTemplate = New SortedDictionary(Of String, tagTLV)
            m_dictTlvsTxnResult_77_ResponseMessageTemplateFormat2 = New SortedDictionary(Of String, tagTLV)
            m_dictTlvsTxnResult_A5_FileControlInformationProprietaryTemplate = New SortedDictionary(Of String, tagTLV)
            m_dictTlvsTxnResult_BF0C_FileControlInformationIssureDiscretionaryData = New SortedDictionary(Of String, tagTLV)
            m_dictTlvsClearingRecordE1_DE055 = New SortedDictionary(Of String, tagTLV)
            m_dictTlvsTxnResult_FFEE01 = New SortedDictionary(Of String, tagTLV)
            m_listTlvsTxnResult_FFEE04 = New List(Of tagTLV)
            m_dictTlvsTxnResult_FFEE05 = New SortedDictionary(Of String, tagTLV)
            m_dictTlvsTxnResult_FF8102 = New SortedDictionary(Of String, tagTLV)
            m_dictTlvsTxnResult_FF8103 = New SortedDictionary(Of String, tagTLV)
            m_dictTlvsTxnResult_FF8105 = New SortedDictionary(Of String, tagTLV)
            m_dictTlvsTxnResult_FF8106 = New SortedDictionary(Of String, tagTLV)

            m_dictCVMList = New List(Of TAG_CVM_LIST_DATA_ELEMENT)
            m_dictPhoneMessageTable = New List(Of TAG_PHONE_MSG_TABLE_ELEMENT)
            '
            m_nOffset = 0
            m_byteMSDLen1 = 0
            m_byteMSDLen2 = 0
            m_byteClearingRecordFlags = 0
            '
            m_nIndentOp = EnumTxnResult2PrintoutIndent.UI_TXN_RESULT_2_PRINTOUT_INDENT1
            m_listTxnResponseMsg = New List(Of TXN_RESPONSE_PARSER_2_PRINTOUT)
            '
            m_infoDateTimeTxnBegin = 0
            m_infoDateTimeTxnEnd = m_infoDateTimeTxnBegin
            '
            m_DF8129_CVM_byte_Val = 0
            m_DF8129_CVM_strMsg = "N/A"
            m_DF8129_UIRequestOnOutCome_Bool = False
            m_DF8129_UIRequestOnRestart_Bool = False
            m_DF8129_DataRecord_Bool = False
            m_DF8129_Discretionary_Bool = False

            '
            m_bTransactionStopped = False
            m_bIsEncMSRButNoDUKPTKeyMode = False
            '
            m_bIsInterrupted = tagIDGCmdDataInfo.bIsInterruptedByExternal

            '
            If Not tagIDGCmdDataInfo.bIsNotResponse Then
                RenewResponseBufferAndUpdateStatusCode(tagIDGCmdDataInfo)
            End If
        End Sub

        Shared Sub CleanupTransactionList(ByRef dictCVMList As List(Of TAG_CVM_LIST_DATA_ELEMENT))
            If (dictCVMList IsNot Nothing) Then
                For Each iteratorCVM In dictCVMList
                    iteratorCVM.Cleanup()
                Next
            End If
            dictCVMList.Clear()
        End Sub
        Shared Sub CleanupTransactionList(ByRef dictPhoneMessageTable As List(Of TAG_PHONE_MSG_TABLE_ELEMENT))
            If (dictPhoneMessageTable IsNot Nothing) Then
                For Each iteratorPMT In dictPhoneMessageTable
                    iteratorPMT.Cleanup()
                Next
            End If
            dictPhoneMessageTable.Clear()
        End Sub

        '
        Public Sub Cleanup()
            'm_dictTlvsTxnResult,
            Dim dictTLVListCleanupGroup As SortedDictionary(Of String, tagTLV)() = New SortedDictionary(Of String, tagTLV)() {
            m_dictTlvsTxnResult,
            m_dictTlvsTxnResult_6F_FileControlInformationTemplate,
            m_dictTlvsTxnResult_77_ResponseMessageTemplateFormat2,
            m_dictTlvsTxnResult_A5_FileControlInformationProprietaryTemplate,
            m_dictTlvsTxnResult_BF0C_FileControlInformationIssureDiscretionaryData,
            m_dictTlvsTxnResult_FFEE01,
            m_dictTlvsTxnResult_FFEE05,
            m_dictTlvsTxnResult_FF8102,
            m_dictTlvsTxnResult_FF8103,
            m_dictTlvsTxnResult_FF8105,
            m_dictTlvsTxnResult_FF8106
                }
            Dim nIdx, nIdxMax As Integer
            '
            nIdxMax = dictTLVListCleanupGroup.Count - 1
            For nIdx = 0 To nIdxMax
                If dictTLVListCleanupGroup(nIdx) IsNot Nothing Then
                    If dictTLVListCleanupGroup(nIdx).Count > 0 Then
                        ClassTableListMaster.CleanupConfigurationsTLVList(dictTLVListCleanupGroup(nIdx))
                    End If
                End If
            Next
            Erase dictTLVListCleanupGroup
            '
            ClassTableListMaster.CleanupConfigurationsTLVList(m_listTlvsTxnResult)

            If m_byteMSDLen1 > 0 Then
                m_byteMSDLen1 = 0
                Erase m_arrayByteMSDData1_Or_OnlineAuthAmountRequested
                'm_arrayByteMSDData1 = Nothing
            End If
            '
            If m_byteMSDLen2 > 0 Then
                m_byteMSDLen2 = 0
                Erase m_arrayByteMSDData2
                'm_arrayByteMSDData2 = Nothing
            End If
            '
            If m_byteMSDLen3 > 0 Then
                m_byteMSDLen3 = 0
                Erase m_arrayByteMSDData3
                'm_arrayByteMSDData3 = Nothing
            End If
            '
            If m_byteClearingRecordFlags = &H1 Then
                m_byteClearingRecordFlags = 0
                ClassTableListMaster.CleanupConfigurationsTLVList(m_dictTlvsClearingRecordE1_DE055)
            End If

            '
            If m_listTlvsTxnResult_FFEE04 IsNot Nothing Then
                If m_listTlvsTxnResult_FFEE04.Count > 0 Then
                    For Each tlv In m_listTlvsTxnResult_FFEE04
                        tlv.Cleanup()
                    Next
                    m_listTlvsTxnResult_FFEE04.Clear()
                End If
            End If

            TAG_TXN_PARSE_RESPONSE_INFO.CleanupTransactionList(m_dictCVMList)
            TAG_TXN_PARSE_RESPONSE_INFO.CleanupTransactionList(m_dictPhoneMessageTable)
            '
            m_nOffset = 0
            '
            m_nIndentOp = EnumTxnResult2PrintoutIndent.UI_TXN_RESULT_2_PRINTOUT_INDENT1
            m_listTxnResponseMsg.Clear()
            '
            m_infoDateTimeTxnBegin = 0
            m_infoDateTimeTxnEnd = m_infoDateTimeTxnBegin
            '
            m_DF8129_CVM_byte_Val = 0
            m_DF8129_CVM_strMsg = "N/A"
            m_DF8129_UIRequestOnOutCome_Bool = False
            m_DF8129_UIRequestOnRestart_Bool = False
            m_DF8129_DataRecord_Bool = False
            m_DF8129_Discretionary_Bool = False
            '
            m_nIDGStatusCode = tagIDGCmdData.EnumDEFAULTVAL.RESPONSE
            m_strIDGStatusCode = "N/A"

            m_bIsInterrupted = False
            m_bIsEncMSRButNoDUKPTKeyMode = False
        End Sub

        Sub RenewResponseBufferAndUpdateStatusCode(ByRef tagIDGCmdDataInfo As tagIDGCmdData)
            '
            tagIDGCmdDataInfo.ParserDataGetArrayRawData(m_arrayByteDataResponse)
            'No data available
            'If (m_arrayByteDataResponse Is Nothing) Then
            '    Return
            'End If
            '
            m_nIDGStatusCode = tagIDGCmdDataInfo.m_nResponse
            m_strIDGStatusCode = tagIDGCmdDataInfo.strGetResponseMsg()
            '
            m_bTransactionStopped = False
            m_bIsInterrupted = tagIDGCmdDataInfo.bIsInterruptedByExternal
        End Sub

        ReadOnly Property bIsInterruptedByExternal As Boolean
            Get
                Return m_bIsInterrupted
            End Get
        End Property

        Public Sub Add2Printout(strMsg As String, Optional ByVal nIndentOff As Integer = 0)
            If (nIndentOff > 0) Then
                m_nIndentOp = m_nIndentOp + nIndentOff
                If m_nIndentOp > EnumTxnResult2PrintoutIndent.UI_TXN_RESULT_2_PRINTOUT_INDENT_MAX Then
                    m_nIndentOp = EnumTxnResult2PrintoutIndent.UI_TXN_RESULT_2_PRINTOUT_INDENT_MAX
                End If
            End If
            m_listTxnResponseMsg.Add(New TXN_RESPONSE_PARSER_2_PRINTOUT With {.m_nLeftBoundaryIndx = m_nIndentOp, .m_strMsg = strMsg})
            If (nIndentOff > 0) Then
                m_nIndentOp = m_nIndentOp - nIndentOff
                If m_nIndentOp < EnumTxnResult2PrintoutIndent.UI_TXN_RESULT_2_PRINTOUT_INDENT_MIN Then
                    m_nIndentOp = EnumTxnResult2PrintoutIndent.UI_TXN_RESULT_2_PRINTOUT_INDENT_MIN
                End If
            End If
        End Sub
        Public Sub Add2PrintoutAbs(strMsg As String, Optional ByVal nIndentOff As Integer = 0)
            If nIndentOff > EnumTxnResult2PrintoutIndent.UI_TXN_RESULT_2_PRINTOUT_INDENT_MAX Then
                nIndentOff = EnumTxnResult2PrintoutIndent.UI_TXN_RESULT_2_PRINTOUT_INDENT_MAX
            End If
            If nIndentOff < EnumTxnResult2PrintoutIndent.UI_TXN_RESULT_2_PRINTOUT_INDENT_MIN Then
                nIndentOff = EnumTxnResult2PrintoutIndent.UI_TXN_RESULT_2_PRINTOUT_INDENT_MIN
            End If
            m_listTxnResponseMsg.Add(New TXN_RESPONSE_PARSER_2_PRINTOUT With {.m_nLeftBoundaryIndx = nIndentOff, .m_strMsg = strMsg})
        End Sub

        ReadOnly Property Prop_DF8129_UIRequestOnOutcome_Bool As Boolean
            Get
                Return m_DF8129_UIRequestOnOutCome_Bool
            End Get
        End Property
        Sub DF8129_SetOutcomeParameterUIRequestOnOutcome(bVal As Boolean)
            m_DF8129_UIRequestOnOutCome_Bool = bVal
        End Sub

        ReadOnly Property Prop_DF8129_UIRequestOnRestart_Bool As Boolean
            Get
                Return m_DF8129_UIRequestOnRestart_Bool
            End Get
        End Property
        Sub DF8129_SetOutcomeParameterUIRequestOnRestart(bVal As Boolean)
            m_DF8129_UIRequestOnRestart_Bool = bVal
        End Sub

        ReadOnly Property Prop_DF8129_DataRecord_Bool As Boolean
            Get
                Return m_DF8129_DataRecord_Bool
            End Get
        End Property
        Sub DF8129_SetOutcomeParameterDataRecord(bVal As Boolean)
            m_DF8129_DataRecord_Bool = bVal
        End Sub

        ReadOnly Property Prop_DF8129_DiscretionaryData_Bool As Boolean
            Get
                Return m_DF8129_Discretionary_Bool
            End Get
        End Property
        Sub DF8129_SetOutcomeParameterDiscretionaryData(bVal As Boolean)
            m_DF8129_Discretionary_Bool = bVal
        End Sub

        ReadOnly Property Prop_DF8129_CVM_Val As Byte
            Get
                Return m_DF8129_CVM_byte_Val
            End Get
        End Property
        ReadOnly Property Prop_DF8129_CVM_strMsg As String
            Get
                Return m_DF8129_CVM_strMsg
            End Get
        End Property
        Sub SetOutcomeParameterDF8129SetCVM(byteVal As Byte, strTmp As String)
            m_DF8129_CVM_byte_Val = byteVal
            m_DF8129_CVM_strMsg = strTmp
        End Sub

        ReadOnly Property Prop_DF8129_ReceiptPresents_Bool As Boolean
            Get
                Return m_DF8129_ReceiptPresents_Bool
            End Get
        End Property
        Sub DF8129_SetOutcomeParameterReceiptPresents(bVal As Boolean)
            m_DF8129_ReceiptPresents_Bool = bVal
        End Sub

        ReadOnly Property GetStrMSRDataTrack1(bIsFormatted As Boolean) As String
            Get
                'Dim pLang As ClassServiceLanguages = ClassServiceLanguages.GetInstance()
                Dim strTrackTmp As String = "" 'pLang.GetMsgLangTranslation(ClassServiceLanguages._WTD_STRID.COMMON_NA)
                Dim strTrack As String = "" 'pLang.GetMsgLangTranslation(ClassServiceLanguages._WTD_STRID.TM001_MSRTrack1) '"" '"Track 1: "
                'Track 1
                If (m_byteMSDLen1 > 0) Then
                    ClassTableListMaster.ConvertFromHexToAscii(m_arrayByteMSDData1_Or_OnlineAuthAmountRequested, strTrackTmp, True)
                    If (bIsFormatted) Then
                        If (strTrackTmp(strTrackTmp.Length - 1) <> "?"c) Then
                            strTrackTmp = strTrackTmp + "?"
                        End If
                        If (strTrackTmp(0) <> "%"c) Then
                            strTrackTmp = "%" + strTrackTmp
                        End If
                    End If
                End If
                If (Not bIsFormatted) Then
                    strTrack = ""
                End If
                Return (strTrack + strTrackTmp)
            End Get
        End Property
        ReadOnly Property GetStrMSRDataTrack2(bIsFormatted As Boolean) As String
            Get
                'Dim pLang As ClassServiceLanguages = ClassServiceLanguages.GetInstance()
                Dim strTrackTmp As String = "" 'pLang.GetMsgLangTranslation(ClassServiceLanguages._WTD_STRID.COMMON_NA)
                Dim strTrack As String = "" 'pLang.GetMsgLangTranslation(ClassServiceLanguages._WTD_STRID.TM001_MSRTrack2) '"" '"Track 1: "
                'Track 1
                If (m_byteMSDLen2 > 0) Then
                    ClassTableListMaster.ConvertFromHexToAscii(m_arrayByteMSDData2, strTrackTmp, True)
                    If (bIsFormatted) Then
                        If (strTrackTmp(strTrackTmp.Length - 1) <> "?"c) Then
                            strTrackTmp = strTrackTmp + "?"
                        End If
                        If (strTrackTmp(0) <> ";"c) Then
                            strTrackTmp = ";" + strTrackTmp
                        End If
                    End If
                End If
                If (Not bIsFormatted) Then
                    strTrack = ""
                End If
                Return (strTrack + strTrackTmp)
            End Get
        End Property
        ReadOnly Property GetStrMSRDataTrack3(bIsFormatted As Boolean) As String
            Get
                'Dim pLang As ClassServiceLanguages = ClassServiceLanguages.GetInstance()
                Dim strTrackTmp As String = "" 'pLang.GetMsgLangTranslation(ClassServiceLanguages._WTD_STRID.COMMON_NA)
                Dim strTrack As String = "" 'pLang.GetMsgLangTranslation(ClassServiceLanguages._WTD_STRID.TM001_MSRTrack3) '"" '"Track 1: "
                'Track 1
                If (m_byteMSDLen3 > 0) Then
                    ClassTableListMaster.ConvertFromHexToAscii(m_arrayByteMSDData3, strTrackTmp, True)
                    If (bIsFormatted) Then
                        If (strTrackTmp(strTrackTmp.Length - 1) <> "?"c) Then
                            strTrackTmp = strTrackTmp + "?"
                        End If
                        If (strTrackTmp(0) <> ";"c) Then
                            strTrackTmp = ";" + strTrackTmp
                        End If
                    End If
                End If
                If (Not bIsFormatted) Then
                    strTrack = ""
                End If
                Return (strTrack + strTrackTmp)
            End Get
        End Property
        ReadOnly Property GetStrMSRData(Optional ByVal bIsFormatted As Boolean = False) As String
            Get
                Dim strTrackData As String = ""
                Dim strTmp As String

                If (bIsFormatted) Then
                    strTmp = vbCrLf
                Else
                    strTmp = ""
                End If
                '============[ New Method ]==========
                If (m_byteMSDLen1 > 0) Then
                    strTrackData = strTrackData + GetStrMSRDataTrack1(bIsFormatted) + strTmp
                End If
                If (m_byteMSDLen2 > 0) Then
                    strTrackData = strTrackData + GetStrMSRDataTrack2(bIsFormatted) + strTmp
                End If
                If (m_byteMSDLen3 > 0) Then
                    strTrackData = strTrackData + GetStrMSRDataTrack3(bIsFormatted) + strTmp
                End If
                Return strTrackData
            End Get
        End Property

    End Structure
    '
    Private m_tagTxnActivateParseResponseInfo As TAG_TXN_PARSE_RESPONSE_INFO
    Private m_bActive As Boolean

    '===========[ Constructor ]===========
    Sub New(ByRef refobjTableListMaster As ClassTableListMaster, ByRef refwinformMainForm As Form01_Main, ByRef refobjReaderCommander As ClassReaderProtocolBasic) 'ByRef refobjUIDriver As ClassUIDriver, 

        'External Variables
        m_refobjCTLM = refobjTableListMaster

        m_refwinformMainForm = refwinformMainForm

        'm_refobjUIDriver = refobjUIDriver

        m_refobjReaderProtocolCmder = refobjReaderCommander

        '
        m_tagTxnActivateParseResponseInfo = New TAG_TXN_PARSE_RESPONSE_INFO(Nothing)
        '
        m_bActive = False
        '
        m_bIsInterruptedByExternal = False
    End Sub

    ReadOnly Property bIsActive As Boolean
        Get
            Return m_bActive
        End Get
    End Property

    ReadOnly Property bIsInterruptedByExternal As Boolean
        Get
            'Don't use m_tagTxnActivateParseResponseInfo .bIsInterruptedByExternal 
            'since it may be incorrect after Cancel Txn Cmd has been done

            Return m_bIsInterruptedByExternal
        End Get
    End Property
    '
    Public Sub UpdateOpenConnection(ByVal refobjReaderCommander As ClassReaderProtocolBasic)
        m_refobjReaderProtocolCmder = refobjReaderCommander
    End Sub

    Public Sub Cleanup()
        'If (m_arrayByteTxnResponse IsNot Nothing) Then
        '    Erase m_arrayByteTxnResponse
        '    m_arrayByteTxnResponse = Nothing
        'End If
        '
        ClassTableListMaster.CleanupConfigurationsTLVList(m_dictTLVsTxnSend)
        '
        m_bIsInterruptedByExternal = False

        m_tagTxnActivateParseResponseInfo.Cleanup()
    End Sub

    ReadOnly Property bIsOnlineAuthRequiredAfterTransaction As Boolean
        Get
            Return (m_tagTxnActivateParseResponseInfo.bIsProtocolOnlineAuthReq Or m_tagTxnActivateParseResponseInfo.bIsProtocolRetStatusFaileWithTransactionStatusOnlineAuthReq)
        End Get
    End Property
    ReadOnly Property bIsTransactionRFErrorAllowedRetryTransaction As Boolean
        Get
            Return (m_tagTxnActivateParseResponseInfo.bIsTransactionRFErrorAllowedRetryTransaction)
        End Get
    End Property
    '
    ReadOnly Property GetInfoTransactionResponse As TAG_TXN_PARSE_RESPONSE_INFO
        Get
            Return m_tagTxnActivateParseResponseInfo
        End Get
    End Property
    '
    Sub TxnActivateGetTLVs(ByRef o_dictTxnSendTLVs As Dictionary(Of String, tagTLV))
        'Dim nPaymentType As Integer
        'Dim dictTLVsPaymentSelect As Dictionary(Of String, TXN_TLVS_UI_INFO)

        '-----------[ Validation Check ]-----------
        If o_dictTxnSendTLVs Is Nothing Then
            o_dictTxnSendTLVs = New Dictionary(Of String, tagTLV)
        Else
            ClassTableListMaster.CleanupConfigurationsTLVList(o_dictTxnSendTLVs)
        End If

        '-----------[ Payment Independent (Common) TLVs ]-----------
        'TxnActivateGetTLVsPaymentSelect(m_dictWUIOTxnTLVsTablePaymentCommon, o_dictTxnSendTLVs)
        ClassReaderCommanderParser.TLV_ParseFromReaderMultipleTLVs("9C 01 00 5F 2A 02 09 78 9F 02 06 00 00 00 00 15 00 9F 03 06 00 00 00 00 00 00 5F 36 01 02 9F 53 01 01 9F 7C 14 01 02 03 04 05 06 07 08 09 0A 0B 0C 0D 0E 0F 10 11 12 13 14 FF EE 04 00", o_dictTxnSendTLVs)
    End Sub
    '
    Sub TxnActivateGetTLVs(ByRef o_dictTxnSendTLVs As SortedDictionary(Of String, tagTLV))
        'Dim nPaymentType As Integer
        'Dim dictTLVsPaymentSelect As Dictionary(Of String, TXN_TLVS_UI_INFO)

        '-----------[ Validation Check ]-----------
        If o_dictTxnSendTLVs Is Nothing Then
            o_dictTxnSendTLVs = New SortedDictionary(Of String, tagTLV)
        Else
            ClassTableListMaster.CleanupConfigurationsTLVList(o_dictTxnSendTLVs)
        End If

        '-----------[ Payment Independent (Common) TLVs ]-----------
        ' TxnActivateGetTLVsPaymentSelect(m_dictWUIOTxnTLVsTablePaymentCommon, o_dictTxnSendTLVs)
        ClassReaderCommanderParser.TLV_ParseFromReaderMultipleTLVs("9C 01 00 5F 2A 02 09 78 9F 02 06 00 00 00 00 15 00 9F 03 06 00 00 00 00 00 00 5F 36 01 02 9F 53 01 01 9F 7C 14 01 02 03 04 05 06 07 08 09 0A 0B 0C 0D 0E 0F 10 11 12 13 14 FF EE 04 00", o_dictTxnSendTLVs)
    End Sub
    '
    '=========================================[ Printout Transaction Response/Result Relative Functions ]====================================
    Shared Sub PrintoutTransactionResponseTLVs2PrintBuffer(ByRef tagTxnActivateParseResponseInfo As ClassTransaction.TAG_TXN_PARSE_RESPONSE_INFO, ByRef dictTlvs As Dictionary(Of String, tagTLV), ByVal strTLVsHeadline As String)
        Dim tlv As tagTLV
        Dim tlvOp As tagTLVNewOp

        With tagTxnActivateParseResponseInfo
            If dictTlvs.Count > 0 Then
                .Add2PrintoutAbs("####[ " + strTLVsHeadline + " Found, TLVs Num = " & dictTlvs.Count & " ]####")
                For Each iteratorTLV In dictTlvs
                    tlv = iteratorTLV.Value
                    tlvOp = ClassReaderCommanderParser.m_dictTLVOp_RefList_Major.Item(tlv.m_strTag)
                    tlvOp.PktParser2Printout(tlvOp, tlv, tagTxnActivateParseResponseInfo)
                Next
            End If

        End With
    End Sub
    Shared Sub PrintoutTransactionResponseTLVs2PrintBuffer(ByRef tagTxnActivateParseResponseInfo As ClassTransaction.TAG_TXN_PARSE_RESPONSE_INFO, ByRef dictTlvs As SortedDictionary(Of String, tagTLV), ByVal strTLVsHeadline As String)
        Dim tlv As tagTLV
        Dim tlvOp As tagTLVNewOp

        With tagTxnActivateParseResponseInfo
            If dictTlvs.Count > 0 Then
                .Add2PrintoutAbs("####[ " + strTLVsHeadline + " Found, TLVs Num = " & dictTlvs.Count & " ]####")
                For Each iteratorTLV In dictTlvs
                    tlv = iteratorTLV.Value
                    tlvOp = ClassReaderCommanderParser.m_dictTLVOp_RefList_Major.Item(tlv.m_strTag)
                    tlvOp.PktParser2Printout(tlvOp, tlv, tagTxnActivateParseResponseInfo)
                Next
            End If

        End With
    End Sub

    Shared Sub PrintoutTransactionResponseTLVs2PrintBuffer(ByRef tagTxnActivateParseResponseInfo As ClassTransaction.TAG_TXN_PARSE_RESPONSE_INFO, ByRef listTlvs As List(Of tagTLV), ByVal strTLVsHeadline As String)
        Dim tlv As tagTLV
        Dim tlvOp As tagTLVNewOp

        With tagTxnActivateParseResponseInfo
            If listTlvs.Count > 0 Then
                .Add2PrintoutAbs("####[ " + strTLVsHeadline + " Found, TLVs Num = " & listTlvs.Count & " ]####")
                For Each iteratorTLV In listTlvs
                    tlv = iteratorTLV
                    tlvOp = ClassReaderCommanderParser.m_dictTLVOp_RefList_Major.Item(tlv.m_strTag)
                    tlvOp.PktParser2Printout(tlvOp, tlv, tagTxnActivateParseResponseInfo)
                Next
            End If

        End With
    End Sub
    '
    Shared Sub PrintoutTransactionResponseMsgONLY(ByRef tagTxnActivateParseResponseInfo As ClassTransaction.TAG_TXN_PARSE_RESPONSE_INFO)
        '---------------------------[ Printout Transaction All Messages ]---------------------------
        Dim iteratorTxnResponseMsg As ClassTransaction.TAG_TXN_PARSE_RESPONSE_INFO.TXN_RESPONSE_PARSER_2_PRINTOUT

        'My.Forms.MainForm.CleanupOutputWindow_Click(Nothing, EventArgs.Empty)
        For Each iteratorTxnResponseMsg In tagTxnActivateParseResponseInfo.m_listTxnResponseMsg

            LogLn(ClassTransaction.TAG_TXN_PARSE_RESPONSE_INFO.m_dictTxnResponse2PrintoutIndent.Item(iteratorTxnResponseMsg.m_nLeftBoundaryIndx) + iteratorTxnResponseMsg.m_strMsg)
            ' LogLn(ClassUIDriver.m_dictTxnResponse2PrintoutIndent.Item(iteratorTxnResponseMsg.m_nLeftBoundaryIndx) + iteratorTxnResponseMsg.m_strMsg)
        Next

    End Sub

    Shared Sub PrintoutTransactionResponseMsgONLY(ByRef tagTxnActivateParseResponseInfo As ClassTransaction.TAG_TXN_PARSE_RESPONSE_INFO, ByRef o_strOutput As String)
        '---------------------------[ Printout Transaction All Messages ]---------------------------
        Dim iteratorTxnResponseMsg As ClassTransaction.TAG_TXN_PARSE_RESPONSE_INFO.TXN_RESPONSE_PARSER_2_PRINTOUT

        'My.Forms.MainForm.CleanupOutputWindow_Click(Nothing, EventArgs.Empty)
        o_strOutput = ""
        For Each iteratorTxnResponseMsg In tagTxnActivateParseResponseInfo.m_listTxnResponseMsg
            o_strOutput = o_strOutput + vbCrLf _
                + ClassTransaction.TAG_TXN_PARSE_RESPONSE_INFO.m_dictTxnResponse2PrintoutIndent.Item(iteratorTxnResponseMsg.m_nLeftBoundaryIndx) _
                + iteratorTxnResponseMsg.m_strMsg
        Next

    End Sub

    Shared Sub PrintoutTLVsInfo(ByVal dictTLVs As SortedDictionary(Of String, tagTLV))
        Dim tagTxnActivateParseResponseInfo As ClassTransaction.TAG_TXN_PARSE_RESPONSE_INFO


        tagTxnActivateParseResponseInfo = New ClassTransaction.TAG_TXN_PARSE_RESPONSE_INFO(Nothing)
        '
        PrintoutTransactionResponseTLVs2PrintBuffer(tagTxnActivateParseResponseInfo, dictTLVs, "Printout Info")
        PrintoutTransactionResponseMsgONLY(tagTxnActivateParseResponseInfo)
        '
        tagTxnActivateParseResponseInfo.Cleanup()
    End Sub
    '
    'Ex: [IN] dictTLVs = [9A:050102][9F21:120101]
    '[OUT] o_strTLVInfoList = "9A : Time -------- 05 01 02
    '                                                9F21 : Date ------------ 12 01 01"
    Shared Sub PrintoutTLVsInfo(ByVal dictTLVs As SortedDictionary(Of String, tagTLV), ByRef o_strTLVInfoList As String)
        Dim tagTxnActivateParseResponseInfo As ClassTransaction.TAG_TXN_PARSE_RESPONSE_INFO


        tagTxnActivateParseResponseInfo = New ClassTransaction.TAG_TXN_PARSE_RESPONSE_INFO(Nothing)
        '
        PrintoutTransactionResponseTLVs2PrintBuffer(tagTxnActivateParseResponseInfo, dictTLVs, "Printout Info")
        '
        o_strTLVInfoList = ""
        PrintoutTransactionResponseMsgONLY(tagTxnActivateParseResponseInfo, o_strTLVInfoList)
        '
        tagTxnActivateParseResponseInfo.Cleanup()
    End Sub
    '
    Public Sub PrintoutTransactionResponse_CaseOfFailureExceptCaseOfUserInterfaceEvent_Common(ByRef tagTxnActivateParseResponseInfo As ClassTransaction.TAG_TXN_PARSE_RESPONSE_INFO)
        Dim strTmp As String = ""
        With tagTxnActivateParseResponseInfo
            '.m_listTxnResponseMsg.Clear()

            '1, 1 byte, Error Code
            'byteVal = .m_arrayByteDataResponse(.m_nOffset)
            ClassReaderProtocolBasic.GetTransactionErrCodeMsg(.m_nTransactionErrorCode, strTmp)
            .Add2Printout("Error Code = " + String.Format("{0,2:X2}", CType(.m_nTransactionErrorCode, Integer)) + ", " + strTmp)

            '.m_nOffset = .m_nOffset + 1

            '2, 1 byte, SW1  '3, 1 byte, SW2
            .Add2Printout("SW12 = " + String.Format("{0,2:X2}{1,2:X2}", .m_u16SW1, .m_u16SW2))
            '.m_nOffset = .m_nOffset + 2

            '4, 1 byte, RF Status Code
            'byteVal = .m_arrayByteDataResponse(.m_nOffset)
            ClassReaderProtocolBasic.GetTransactionRFErrCodeMsg(.m_nRFStatusCode, strTmp)
            .Add2Printout("RF Status Code = " + String.Format("{0,2:X2}", CType(.m_nRFStatusCode, Integer)) + ", " + strTmp)
            '.m_nOffset = .m_nOffset + 1
        End With

    End Sub
    '
    Public Sub PrintoutTransactionResponse_CaseOfFailure_23h_OnlineAuth(ByRef tagTxnActivateParseResponseInfo As ClassTransaction.TAG_TXN_PARSE_RESPONSE_INFO)
        Dim strTmp As String
        With tagTxnActivateParseResponseInfo
            '.m_listTxnResponseMsg.Clear()
            strTmp = "==[" + tagTxnActivateParseResponseInfo.TxnTimeEndWhen.ToString("R") + " , Elapsed Time : " + tagTxnActivateParseResponseInfo.TxnTimeElapasedInSecs.ToString() + " secs]=="
            .Add2Printout(strTmp)

            'Error Code, SW12, RF Status Code
            PrintoutTransactionResponse_CaseOfFailureExceptCaseOfUserInterfaceEvent_Common(tagTxnActivateParseResponseInfo)

            'TLV Track 2 Equivalent Data (57)
            PrintoutTransactionResponseTLVs2PrintBuffer(tagTxnActivateParseResponseInfo, .m_listTlvsTxnResult, "Rest TLVs Data")

            'Amount Requested Data, 12
            ClassTableListMaster.ConvertFromArrayByte2String(.m_arrayByteMSDData1_Or_OnlineAuthAmountRequested, strTmp, False)
            .Add2Printout("Amount Requested Data = " + strTmp)

            '---------------------------[ Printout Transaction All Messages ]---------------------------
            PrintoutTransactionResponseMsgONLY(tagTxnActivateParseResponseInfo)

            '-------------------------[ Status Code ]-------------------------
            LogLn("================================")
            strTmp = String.Format("{0, 2:X2}", CType(.m_nIDGStatusCode, Integer)) + " = " + .m_strIDGStatusCode
            LogLn("Status Code : " + strTmp)

            'My.Forms.MainForm.SetTransactionResponseMsg(strTmp, Color.Blue)
        End With

    End Sub
    '
    Public Sub PrintoutTransactionResponse_CaseOfFailure_0Eh_UserInterfaceEvent(ByRef tagTxnActivateParseResponseInfo As ClassTransaction.TAG_TXN_PARSE_RESPONSE_INFO)
        Dim strTmp As String

        With tagTxnActivateParseResponseInfo
            '.m_listTxnResponseMsg.Clear()

            '.m_listTxnResponseMsg.Clear()
            strTmp = "==[" + tagTxnActivateParseResponseInfo.TxnTimeEndWhen.ToString("R") + " , Elapsed Time : " + tagTxnActivateParseResponseInfo.TxnTimeElapasedInSecs.ToString() + " secs]=="
            .Add2Printout(strTmp)

            'Error Code, SW12, RF Status Code
            PrintoutTransactionResponse_CaseOfFailureExceptCaseOfUserInterfaceEvent_Common(tagTxnActivateParseResponseInfo)

            'Rest Of TLVs
            PrintoutTransactionResponseTLVs2PrintBuffer(tagTxnActivateParseResponseInfo, .m_listTlvsTxnResult, "Rest TLVs Data")

            '---------------------------[ Printout Transaction All Messages ]---------------------------
            PrintoutTransactionResponseMsgONLY(tagTxnActivateParseResponseInfo)

            '-------------------------[ Status Code ]-------------------------
            LogLn("================================")
            strTmp = String.Format("{0, 2:X2}", CType(.m_nIDGStatusCode, Integer)) + " = " + .m_strIDGStatusCode
            LogLn("Status Code : " + strTmp)

            'My.Forms.MainForm.SetTransactionResponseMsg(strTmp, Color.Red)
        End With

    End Sub
    '
    Public Sub PrintoutTransactionResponse_CaseOfFailure_Not_23h_OnlineAuth_Not_0Eh_UserInterfaceEvent(ByRef tagTxnActivateParseResponseInfo As ClassTransaction.TAG_TXN_PARSE_RESPONSE_INFO)

        Dim strTmp As String
        With tagTxnActivateParseResponseInfo
            '.m_listTxnResponseMsg.Clear()
            strTmp = "==[" + tagTxnActivateParseResponseInfo.TxnTimeEndWhen.ToString("R") + " , Elapsed Time : " + tagTxnActivateParseResponseInfo.TxnTimeElapasedInSecs.ToString() + " secs]=="
            .Add2Printout(strTmp)

            'Error Code, SW12, RF Status Code
            PrintoutTransactionResponse_CaseOfFailureExceptCaseOfUserInterfaceEvent_Common(tagTxnActivateParseResponseInfo)

            '5. Rest Of TLVs
            PrintoutTransactionResponseTLVs2PrintBuffer(tagTxnActivateParseResponseInfo, tagTxnActivateParseResponseInfo.m_listTlvsTxnResult, "Rest TLVs Data")

            '---------------------------[ Printout Transaction All Messages ]---------------------------
            PrintoutTransactionResponseMsgONLY(tagTxnActivateParseResponseInfo)

            '-------------------------[ Status Code ]-------------------------
            LogLn("================================")
            strTmp = String.Format("{0, 2:X2}", CType(.m_nIDGStatusCode, Integer)) + " = " + .m_strIDGStatusCode
            LogLn("Status Code : " + strTmp)

            'My.Forms.MainForm.SetTransactionResponseMsg(strTmp, Color.Red)
        End With

    End Sub
    '
    Public Sub PrintoutTransactionResponse_CaseOfFailure_IDGCmdStatusCodeFailedOthers(ByRef tagTxnActivateParseResponseInfo As ClassTransaction.TAG_TXN_PARSE_RESPONSE_INFO)
        Dim strTmp As String

        With tagTxnActivateParseResponseInfo
            strTmp = "==[" + .TxnTimeEndWhen.ToString("R") + " , Elapsed Time : " + .TxnTimeElapasedInSecs.ToString() + " secs]=="
            LogLn(strTmp)

            '-------------------------[ Status Code ]-------------------------
            LogLn("================================")
            strTmp = "Status Code : " + String.Format("{0, 2:X2}", CType(.m_nIDGStatusCode, Integer))
            strTmp = strTmp + " = " + .m_strIDGStatusCode
            LogLn(strTmp)

            'My.Forms.MainForm.SetTransactionResponseMsg(strTmp, Color.Red)
        End With
    End Sub
    '
    Public Sub PrintoutTransactionResponse_CaseOfOK_00h(ByRef tagTxnActivateParseResponseInfo As ClassTransaction.TAG_TXN_PARSE_RESPONSE_INFO)
        Dim strElapsedTime As String = "N/A"
        'Dim tlv As tagTLV
        Dim n2PrintoutIndentIndex As ClassTransaction.TAG_TXN_PARSE_RESPONSE_INFO.EnumTxnResult2PrintoutIndent = ClassTransaction.TAG_TXN_PARSE_RESPONSE_INFO.EnumTxnResult2PrintoutIndent.UI_TXN_RESULT_2_PRINTOUT_INDENT1
        Dim strTmp As String
        '

        'Transaction Result Data Format is referenced from IDG Commands Document

        With tagTxnActivateParseResponseInfo
            .m_listTxnResponseMsg.Clear()
            '
            strTmp = "==[" + tagTxnActivateParseResponseInfo.TxnTimeEndWhen.ToString("R") + " , Elapsed Time : " + tagTxnActivateParseResponseInfo.TxnTimeElapasedInSecs.ToString() + " secs]=="
            .Add2Printout(strTmp)

            '---------------------------[ Track 1 Data ]---------------------------
            If (.m_byteMSDLen1 > 0) Then
                .Add2Printout(tagTxnActivateParseResponseInfo.GetStrMSRDataTrack1(True))
            End If

            '---------------------------[ Track 2 Data ]---------------------------
            If (.m_byteMSDLen2 > 0) Then
                .Add2Printout(tagTxnActivateParseResponseInfo.GetStrMSRDataTrack2(True))
            End If

            '---------------------------[ Track 3 Data ]---------------------------
            '[Track 3 Length = 1 byte][Track 3 Data]
            If (.m_byteMSDLen3 > 0) Then
                .Add2Printout(tagTxnActivateParseResponseInfo.GetStrMSRDataTrack3(True))
            End If

            '---------------------------[ C.R. "E1" Data ]---------------------------
            If (.m_byteClearingRecordFlags = &H1) Then
                PrintoutTransactionResponseTLVs2PrintBuffer(tagTxnActivateParseResponseInfo, tagTxnActivateParseResponseInfo.m_dictTlvsClearingRecordE1_DE055, "Cleaning Record Data")
                '    .Add2Printout("####[ Cleaning Record Data Found, TLVs Num = " & .m_dictTlvsClearingRecordE1_DE055.Count & " ]####")
                '    For Each iteratorTLV In .m_dictTlvsClearingRecordE1_DE055
                '        tlv = iteratorTLV.Value
                '        tlvOp = ClassReaderCommanderParser.m_dictTLVOp.Item(tlv.m_strTag)
                '        tlvOp.PktParser2Printout(tlvOp, tlv, tagTxnActivateParseResponseInfo)
                '    Next
            End If

            '---------------------------[ All Major TLVs ]---------------------------
            PrintoutTransactionResponseTLVs2PrintBuffer(tagTxnActivateParseResponseInfo, tagTxnActivateParseResponseInfo.m_listTlvsTxnResult, "Rest TLVs Data")
            'If .m_dictTlvsTxnResult.Count > 0 Then
            '    .Add2Printout("####[ Rest TLVs Data Found, TLVs Num = " & .m_dictTlvsTxnResult.Count & " ]####")
            '    For Each iteratorTLV In .m_dictTlvsTxnResult
            '        tlv = iteratorTLV.Value
            '        tlvOp = ClassReaderCommanderParser.m_dictTLVOp.Item(tlv.m_strTag)
            '        tlvOp.PktParser2Printout(tlvOp, tlv, tagTxnActivateParseResponseInfo)
            '    Next
            'End If

            '---------------------------[ Printout Transaction All Messages ]---------------------------
            PrintoutTransactionResponseMsgONLY(tagTxnActivateParseResponseInfo)

            '-------------------------[ Status Code ]-------------------------
            LogLn("================================")
            With tagTxnActivateParseResponseInfo
                strTmp = String.Format("{0, 2:X2}", CType(.m_nIDGStatusCode, Integer)) + " = " + .m_strIDGStatusCode
                LogLn("Status Code : " + strTmp)

                'My.Forms.MainForm.SetTransactionResponseMsg(strTmp, Color.Green)
            End With

        End With
    End Sub

    Public Sub PrintoutTransactionEnhancementResponse_MSR(ByRef tagTxnActivateParseResponseInfo As ClassTransaction.TAG_TXN_PARSE_RESPONSE_INFO)
        Dim strTmp As String

        With tagTxnActivateParseResponseInfo
            If (.bIsProtocolRetStatusOK) Then
                'Response OK then  it contains Track Data if necessary
                '[Track 1 Length = 1 byte][Track 1 Data]
                '[Track 2 Length = 1 byte][Track 2 Data]
                '---------------------------[ Track 1 Data ]---------------------------
                '---------------------------[ Track 1 Data ]---------------------------
                If (.m_byteMSDLen1 > 0) Then
                    .Add2Printout(tagTxnActivateParseResponseInfo.GetStrMSRDataTrack1(True))
                End If

                '---------------------------[ Track 2 Data ]---------------------------
                If (.m_byteMSDLen2 > 0) Then
                    .Add2Printout(tagTxnActivateParseResponseInfo.GetStrMSRDataTrack2(True))
                End If

                '---------------------------[ Track 3 Data ]---------------------------
                '[Track 3 Length = 1 byte][Track 3 Data]
                If (.m_byteMSDLen3 > 0) Then
                    .Add2Printout(tagTxnActivateParseResponseInfo.GetStrMSRDataTrack3(True))
                End If
            End If
        End With

        '---------------------------[ Printout Transaction All Messages ]---------------------------
        PrintoutTransactionResponseMsgONLY(tagTxnActivateParseResponseInfo)

        '-------------------------[ Status Code ]-------------------------
        LogLn("================================")
        With tagTxnActivateParseResponseInfo
            strTmp = String.Format("{0, 2:X2}", CType(.m_nIDGStatusCode, Integer)) + " = " + .m_strIDGStatusCode
            LogLn("Status Code : " + strTmp)

            'My.Forms.MainForm.SetTransactionResponseMsg(strTmp, Color.Green)
        End With
    End Sub

    Sub PrintoutTransactionTLVsBeforeActive(dictTLVsTxnSend As Dictionary(Of String, tagTLV))
        Dim iteratorTLV As KeyValuePair(Of String, tagTLV)
        'Dim strVal As String
        Dim tlvOp As tagTLVNewOp
        Dim arrayByteData As Byte() = New Byte(15) {&H0, &H0, &H0, &H0, &H0, &H0, &H0, &H0, &H0, &H0, &H0, &H0, &H0, &H0, &H0, &H0}
        Dim tagIDGData As tagIDGCmdData = New tagIDGCmdData(&H1801, arrayByteData, Nothing)
        tagIDGData.SetDebugMode_RecvDone()
        Dim txnResult As ClassTransaction.TAG_TXN_PARSE_RESPONSE_INFO = New ClassTransaction.TAG_TXN_PARSE_RESPONSE_INFO(tagIDGData)

        Try
            LogLn("==[ Printout Transaction Activate TLVs Parameters, Num = " & dictTLVsTxnSend.Count & " ]==")
            For Each iteratorTLV In dictTLVsTxnSend

                'ClassTableListMaster.ConvertFromArrayByte2String(iteratorTLV.Value.m_arrayTLVal, strVal)
                tlvOp = ClassReaderCommanderParser.m_dictTLVOp_RefList_Major.Item(iteratorTLV.Key)
                tlvOp.PktParser2Printout(tlvOp, iteratorTLV.Value, txnResult)
                'LogLn(iteratorTLV.Key + " : " + strVal)
                'LogLn(iteratorTLV.Key + "( " + tlvOp.m_strComment + " )  : " + strVal)
            Next
            '
            PrintoutTransactionResponseMsgONLY(txnResult)
            '
            LogLn(" ")
        Catch ex As Exception
            Throw 'ex
        Finally
            txnResult.Cleanup()
            tagIDGData.Cleanup()
            Erase arrayByteData
        End Try

    End Sub

    Sub PrintoutTransactionTLVsBeforeActive(dictTLVsTxnSend As SortedDictionary(Of String, tagTLV))
        Dim iteratorTLV As KeyValuePair(Of String, tagTLV)
        'Dim strVal As String
        Dim tlvOp As tagTLVNewOp
        Dim arrayByteData As Byte() = New Byte(15) {&H0, &H0, &H0, &H0, &H0, &H0, &H0, &H0, &H0, &H0, &H0, &H0, &H0, &H0, &H0, &H0}
        Dim tagIDGData As tagIDGCmdData = New tagIDGCmdData(&H1801, arrayByteData, Nothing)
        tagIDGData.SetDebugMode_RecvDone()
        Dim txnResult As ClassTransaction.TAG_TXN_PARSE_RESPONSE_INFO = New ClassTransaction.TAG_TXN_PARSE_RESPONSE_INFO(tagIDGData)

        Try
            LogLn("==[ Printout Transaction Activate TLVs Parameters, Num = " & dictTLVsTxnSend.Count & " ]==")
            For Each iteratorTLV In dictTLVsTxnSend

                'ClassTableListMaster.ConvertFromArrayByte2String(iteratorTLV.Value.m_arrayTLVal, strVal)
                tlvOp = ClassReaderCommanderParser.m_dictTLVOp_RefList_Major.Item(iteratorTLV.Key)
                tlvOp.PktParser2Printout(tlvOp, iteratorTLV.Value, txnResult)
                'LogLn(iteratorTLV.Key + " : " + strVal)
                'LogLn(iteratorTLV.Key + "( " + tlvOp.m_strComment + " )  : " + strVal)
            Next
            '
            PrintoutTransactionResponseMsgONLY(txnResult)
            '
            LogLn(" ")
        Catch ex As Exception
            Throw 'ex
        Finally
            txnResult.Cleanup()
            tagIDGData.Cleanup()
            Erase arrayByteData
        End Try

    End Sub

    Private Shared Sub PrintoutTransactionResponseWantedTLV(ByVal strTAG As String, ByVal dictTlvsTxnResult As Dictionary(Of String, tagTLV), ByRef tagTxnActivateParseResponseInfo As ClassTransaction.TAG_TXN_PARSE_RESPONSE_INFO)
        Dim tlv As tagTLV = Nothing
        Dim tlvOp As tagTLVNewOp = Nothing

        'Note, This function is not implemented yet since output to main info windows is not ready
        Throw New Exception("[Err][Not Be Implemented] PrintoutTransactionResponseWantedTLV() ")
        tlvOp = ClassReaderCommanderParser.m_dictTLVOp_RefList_Major.Item(strTAG)
        If dictTlvsTxnResult.ContainsKey(strTAG) Then
            tlv = dictTlvsTxnResult.Item(strTAG)
            tlvOp.PktParser2Printout(tlvOp, tlv, tagTxnActivateParseResponseInfo)
        Else
            tagTxnActivateParseResponseInfo.Add2Printout(tlvOp.m_strComment + " ( " + strTAG + " ) : is not found.")
        End If
    End Sub

    Sub PrintoutTxnUpdateBalanceResponse_CaseOfOK(ByRef tagTxnActivateParseResponseInfo As ClassTransaction.TAG_TXN_PARSE_RESPONSE_INFO, bValApproved As Boolean)
        ' Only Couple of Tag 57; E300 
        Dim strTmp As String

        LogLn("================================")

        PrintoutTransactionResponseTLVs2PrintBuffer(tagTxnActivateParseResponseInfo, tagTxnActivateParseResponseInfo.m_listTlvsTxnResult, "Online Authentication Data")

        '-------------------------[ Status Code ]-------------------------
        With tagTxnActivateParseResponseInfo
            strTmp = String.Format("{0, 2:X2}", CType(.m_nIDGStatusCode, Integer)) + " = " + .m_strIDGStatusCode
            LogLn("Status Code : " + strTmp)

            If (bValApproved) Then
                strTmp = strTmp + " , Online Approved"
            Else
                strTmp = strTmp + " , Online Declined"
            End If
            'My.Forms.MainForm.SetTransactionResponseMsg(strTmp, Color.Green)
        End With


    End Sub

    Sub PrintoutTxnUpdateBalanceResponse_CaseOfFailure(ByRef tagTxnActivateParseResponseInfo As ClassTransaction.TAG_TXN_PARSE_RESPONSE_INFO, bValApproved As Boolean)
        Dim strTmp As String

        LogLn("================================")

        PrintoutTransactionResponse_CaseOfFailure_Not_23h_OnlineAuth_Not_0Eh_UserInterfaceEvent(tagTxnActivateParseResponseInfo)

        If (bValApproved) Then
            strTmp = " , Online Approved"
        Else
            strTmp = " , Online Declined"
        End If
        'My.Forms.MainForm.SetTransactionResponseMsg(strTmp, Color.Red, True)
    End Sub
    '====================================[Picture Box = Status Light Indicator]====================================
    '
    Public Sub TxnActivate_NEO_Generic_02_40()

        m_bActive = True

        Try
            'UIDriverEnableDisable(ClassUIDriver.EnumUIDriverEventEnableDisable.UI_ENABLE_TRANSACTION_START)

            '==================[ Perform Some Action Before Transaction Start ]========================

            m_tagTxnActivateParseResponseInfo.Cleanup()
            'Get Send out TLV Lists for Transaction. Payment Type Selection is also involved.
            '[Don't Forget] To Cleanup Dictionary Resources
            TxnActivateGetTLVs(m_dictTLVsTxnSend)

            'Show / Printout to Main Result Window before Activation
            PrintoutTransactionTLVsBeforeActive(m_dictTLVsTxnSend)

            '-------------------[ Remember Start Time ]-------------------
            m_tagTxnActivateParseResponseInfo.TxnTimeBegin()

            'LogLn("Time Begin: " + m_tagTxnActivateParseResponseInfo.m_infoDateTimeTxnBegin.ToString("R"))
            'Send Transaction Activate Command 02-40. <Note> Transaction Commands (ClassReaderCommander) must be responsible to print out Sent & Received Data Content in Hex Text Format. 
            '[Don't Forget] To Release resource m_arrayByteTxnResponse after parsing job done
            '+1 --> Delay One more second to wait response from reader
            '
            Dim prefObjRdrCmder_NEO As ClassReaderProtocolIDGGR = CType(m_refobjReaderProtocolCmder, ClassReaderProtocolIDGGR)
            Dim pRdrInfo As ClassReaderInfo = ClassReaderInfo.GetIntance()

            If (pRdrInfo.bIsPlatformNEO20_2017) Then
                'ToDo - improve it.
                '2017 Aug 03, goofy_liu
                'Beta01, Since 02-40 in Beta01 FW Version missed Tk3 last char "?"
                prefObjRdrCmder_NEO.ATCmdCode = &H201
            Else
                prefObjRdrCmder_NEO.ATCmdCode = &H240
            End If

            m_refobjReaderProtocolCmder.TransactionActivate(m_dictTLVsTxnSend, m_tagTxnActivateParseResponseInfo, m_refwinformMainForm.IDGCmdTimeoutGet() + 1)

            '-------------------[ Remember End Time ]-------------------
            m_tagTxnActivateParseResponseInfo.TxnTimeEnd()

            If (m_refobjReaderProtocolCmder.bIsIDGCmdInterruptedByExternal) Then
                m_refwinformMainForm.IDGCmdTimeoutSet(10)
                'Should Send "Transaction Cancel" Cmd
                m_refobjReaderProtocolCmder.TransactionCancel()
                ' 
                m_bIsInterruptedByExternal = True
            Else
                '==========================[ Transaction Result / Failure Procession ]==========================
                If m_tagTxnActivateParseResponseInfo.bIsProtocolRetStatusOK Or m_tagTxnActivateParseResponseInfo.bIsProtocolOnlineAuthReq Then
                    '-----------------------------------[ Transaction OK ]-----------------------------------
                    'LogLn("Time End: " + m_tagTxnActivateParseResponseInfo.m_infoDateTimeTxnEnd.ToString("R"))
                    'Do Parser Response Data
                    TxnActivateParseResponse_CaseOfOK_00h_NEOGeneric_02_40(m_tagTxnActivateParseResponseInfo)

                    'Show / Printout to Main Result Window After Response Data Has Been Parsed
                    PrintoutTransactionResponse_CaseOfOK_00h(m_tagTxnActivateParseResponseInfo)

                    '[Note] Continue Online Authentication is decided by ClassTransaction Level, ClassTransaction Instance invoker could check it by 
                    'calling ClassTransaction Instance.bIsOnlineAuthRequiredAfterTransaction

                ElseIf m_tagTxnActivateParseResponseInfo.bIsProtocolRetStatusFaileWithTransactionStatusOnlineAuthReq Then
                    '-----------------------------------[ Online Authentication Request ]-----------------------------------
                    'Part It
                    TxnActivateParseResponse_CaseOfFailure_23h_OnlineAuth(m_tagTxnActivateParseResponseInfo)

                    'Show / Printout to Main Result Window After Response Data Has Been Parsed
                    PrintoutTransactionResponse_CaseOfFailure_23h_OnlineAuth(m_tagTxnActivateParseResponseInfo)

                    '[Note] Continue Online Authentication is decided by ClassTransaction Level, ClassTransaction Instance invoker could check it by 
                    'calling ClassTransaction Instance.bIsOnlineAuthRequiredAfterTransaction
                ElseIf m_tagTxnActivateParseResponseInfo.bIsProtocolUserInterfaceEvent Then
                    '-----------------------------------[ User Event Status ]-----------------------------------
                    TxnActivateParseResponse_CaseOfFailure_0Eh_UserInterfaceEvent(m_tagTxnActivateParseResponseInfo)

                    PrintoutTransactionResponse_CaseOfFailure_0Eh_UserInterfaceEvent(m_tagTxnActivateParseResponseInfo)

                ElseIf m_tagTxnActivateParseResponseInfo.bIsProtocolRetStatusFailOrNACK Then
                    'IDG Command Failed Code with Other Transaction Status Code
                    TxnActivateParseResponse_CaseOfFailure_Not_23h_OnlineAuth_Not_0Eh_UserInterfaceEvent(m_tagTxnActivateParseResponseInfo)

                    PrintoutTransactionResponse_CaseOfFailure_Not_23h_OnlineAuth_Not_0Eh_UserInterfaceEvent(m_tagTxnActivateParseResponseInfo)
                    '-----------------------------------[  ]-----------------------------------
                    'Other Failure Case, [Special Note] about Retrying Case in Card Returned Error Status
                    If (m_tagTxnActivateParseResponseInfo.bIsTransactionRFErrorAllowedRetryTransaction) Then
                        LogLn("[Info] RF Err Code - Retry Transaction" + m_tagTxnActivateParseResponseInfo.strGetTransactionResult)
                    End If
                Else
                    'IDG Command Failed Code with Other Transaction Status Code
                    TxnActivateParseResponse_CaseOfFailure_Not_23h_OnlineAuth_Not_0Eh_UserInterfaceEvent(m_tagTxnActivateParseResponseInfo)

                    PrintoutTransactionResponse_CaseOfFailure_Not_23h_OnlineAuth_Not_0Eh_UserInterfaceEvent(m_tagTxnActivateParseResponseInfo)
                    'IDG Command Status Code Exception "OK" And "Fail/NACK"  And "Online Auth" And "User Interface Event" value
                    PrintoutTransactionResponse_CaseOfFailure_IDGCmdStatusCodeFailedOthers(m_tagTxnActivateParseResponseInfo)
                End If
            End If
        Catch ex As Exception
            LogLn("[Err] TxnActivate() = " + ex.Message)
        Finally
            '1. Clean up TLV list
            ClassTableListMaster.CleanupConfigurationsTLVList(m_dictTLVsTxnSend)
            '2. Erase Byte Array for Response
            'Erase m_arrayByteTxnResponse
            '3. Clean up m_tagTxnActivateParsedResponseInfo Structure
            'm_tagTxnActivateParseResponseInfo.Cleanup()
            '
            'UIDriverEnableDisable(m_refwinformMainForm.ModeSelectionGet())
            m_refwinformMainForm.UIEnable(True)
            '
            m_bActive = False
        End Try
        'Finally Clean it up
    End Sub
    '
    Public Sub TxnActivate()

        m_bActive = True

        Try
            'UIDriverEnableDisable(ClassUIDriver.EnumUIDriverEventEnableDisable.UI_ENABLE_TRANSACTION_START)

            '==================[ Perform Some Action Before Transaction Start ]========================

            m_tagTxnActivateParseResponseInfo.Cleanup()
            'Get Send out TLV Lists for Transaction. Payment Type Selection is also involved.
            '[Don't Forget] To Cleanup Dictionary Resources
            TxnActivateGetTLVs(m_dictTLVsTxnSend)

            'Show / Printout to Main Result Window before Activation
            PrintoutTransactionTLVsBeforeActive(m_dictTLVsTxnSend)

            '-------------------[ Remember Start Time ]-------------------
            m_tagTxnActivateParseResponseInfo.TxnTimeBegin()

            'LogLn("Time Begin: " + m_tagTxnActivateParseResponseInfo.m_infoDateTimeTxnBegin.ToString("R"))
            'Send Transaction Activate Command 02-01. <Note> Transaction Commands (ClassReaderCommander) must be responsible to print out Sent & Received Data Content in Hex Text Format. 
            '[Don't Forget] To Release resource m_arrayByteTxnResponse after parsing job done
            '+1 --> Delay One more second to wait response from reader
            m_refobjReaderProtocolCmder.TransactionActivate(m_dictTLVsTxnSend, m_tagTxnActivateParseResponseInfo, m_refwinformMainForm.IDGCmdTimeoutGet() + 1)

            '-------------------[ Remember End Time ]-------------------
            m_tagTxnActivateParseResponseInfo.TxnTimeEnd()

            If (m_refobjReaderProtocolCmder.bIsIDGCmdInterruptedByExternal) Then
                m_refwinformMainForm.IDGCmdTimeoutSet(10)
                'Should Send "Transaction Cancel" Cmd
                m_refobjReaderProtocolCmder.TransactionCancel()
                ' 
                m_bIsInterruptedByExternal = True
            Else
                '==========================[ Transaction Result / Failure Procession ]==========================
                If m_tagTxnActivateParseResponseInfo.bIsProtocolRetStatusOK Or m_tagTxnActivateParseResponseInfo.bIsProtocolOnlineAuthReq Then
                    '-----------------------------------[ Transaction OK ]-----------------------------------
                    'LogLn("Time End: " + m_tagTxnActivateParseResponseInfo.m_infoDateTimeTxnEnd.ToString("R"))
                    'Do Parser Response Data
                    TxnActivateParseResponse_CaseOfOK_00h(m_tagTxnActivateParseResponseInfo)

                    'Show / Printout to Main Result Window After Response Data Has Been Parsed
                    PrintoutTransactionResponse_CaseOfOK_00h(m_tagTxnActivateParseResponseInfo)

                    '[Note] Continue Online Authentication is decided by ClassTransaction Level, ClassTransaction Instance invoker could check it by 
                    'calling ClassTransaction Instance.bIsOnlineAuthRequiredAfterTransaction

                ElseIf m_tagTxnActivateParseResponseInfo.bIsProtocolRetStatusFaileWithTransactionStatusOnlineAuthReq Then
                    '-----------------------------------[ Online Authentication Request ]-----------------------------------
                    'Part It
                    TxnActivateParseResponse_CaseOfFailure_23h_OnlineAuth(m_tagTxnActivateParseResponseInfo)

                    'Show / Printout to Main Result Window After Response Data Has Been Parsed
                    PrintoutTransactionResponse_CaseOfFailure_23h_OnlineAuth(m_tagTxnActivateParseResponseInfo)

                    '[Note] Continue Online Authentication is decided by ClassTransaction Level, ClassTransaction Instance invoker could check it by 
                    'calling ClassTransaction Instance.bIsOnlineAuthRequiredAfterTransaction
                ElseIf m_tagTxnActivateParseResponseInfo.bIsProtocolUserInterfaceEvent Then
                    '-----------------------------------[ User Event Status ]-----------------------------------
                    TxnActivateParseResponse_CaseOfFailure_0Eh_UserInterfaceEvent(m_tagTxnActivateParseResponseInfo)

                    PrintoutTransactionResponse_CaseOfFailure_0Eh_UserInterfaceEvent(m_tagTxnActivateParseResponseInfo)

                ElseIf m_tagTxnActivateParseResponseInfo.bIsProtocolRetStatusFailOrNACK Then
                    'IDG Command Failed Code with Other Transaction Status Code
                    TxnActivateParseResponse_CaseOfFailure_Not_23h_OnlineAuth_Not_0Eh_UserInterfaceEvent(m_tagTxnActivateParseResponseInfo)

                    PrintoutTransactionResponse_CaseOfFailure_Not_23h_OnlineAuth_Not_0Eh_UserInterfaceEvent(m_tagTxnActivateParseResponseInfo)
                    '-----------------------------------[  ]-----------------------------------
                    'Other Failure Case, [Special Note] about Retrying Case in Card Returned Error Status
                    If (m_tagTxnActivateParseResponseInfo.bIsTransactionRFErrorAllowedRetryTransaction) Then
                        LogLn("[Info] RF Err Code - Retry Transaction" + m_tagTxnActivateParseResponseInfo.strGetTransactionResult)
                    End If
                Else
                    'IDG Command Failed Code with Other Transaction Status Code
                    TxnActivateParseResponse_CaseOfFailure_Not_23h_OnlineAuth_Not_0Eh_UserInterfaceEvent(m_tagTxnActivateParseResponseInfo)

                    PrintoutTransactionResponse_CaseOfFailure_Not_23h_OnlineAuth_Not_0Eh_UserInterfaceEvent(m_tagTxnActivateParseResponseInfo)
                    'IDG Command Status Code Exception "OK" And "Fail/NACK"  And "Online Auth" And "User Interface Event" value
                    PrintoutTransactionResponse_CaseOfFailure_IDGCmdStatusCodeFailedOthers(m_tagTxnActivateParseResponseInfo)
                End If
            End If
        Catch ex As Exception
            LogLn("[Err] TxnActivate() = " + ex.Message)
        Finally
            '1. Clean up TLV list
            ClassTableListMaster.CleanupConfigurationsTLVList(m_dictTLVsTxnSend)
            '2. Erase Byte Array for Response
            'Erase m_arrayByteTxnResponse
            '3. Clean up m_tagTxnActivateParsedResponseInfo Structure
            'm_tagTxnActivateParseResponseInfo.Cleanup()
            '
            'UIDriverEnableDisable(m_refwinformMainForm.ModeSelectionGet())
            m_refwinformMainForm.UIEnable(True)
            '
            m_bActive = False
        End Try
        'Finally Clean it up
    End Sub

    '    typedef struct tagDispStr
    '{
    '	int x;
    '	int y;
    '	int fontDesignation;
    '	int fontID;
    '	int dispFlags;
    '	int fgColor;
    '	int bgColor;
    '	char msg[65];
    '}DispStr;
    Public Structure TAG_IDG0220_IDG8303_ENH_TXN_ACTIVATE

        Private m_tagPos As Point
        Private m_nFontDesignation_Or_FontSize As Integer '<10
        Private m_nFontID_Or_FontType As Integer '<100
        Private m_nDispFlags_Or_DispOptions As Integer '<10
        Private m_nFgColor As Integer ' Currently is not used
        Private m_nBgColor As Integer ' Currently is not used
        ' Dont Exceed Length = 45
        Public m_strMsg As String
        Private m_nWidth As Integer
        Private m_nHeight As Integer
        Private m_u32CtrlID As UInt32
        Private m_bIDGCmd8303 As Boolean

        Public Sub New(nPosX As Integer, nPosY As Integer, nFontDesignation_Or_FontSize As Integer, _
                       nFontID_Or_FontType As Integer, nDispFlags_Or_DispOptions As Integer, nFgColor As Integer, _
                       nBgColor As Integer, strMsg As String)
            If (m_tagPos.Equals(Nothing)) Then
                m_tagPos = New Point(nPosX, nPosY)
            Else
                m_tagPos.X = nPosX
                m_tagPos.Y = nPosY
            End If

            m_nFontDesignation_Or_FontSize = nFontDesignation_Or_FontSize
            m_nFontID_Or_FontType = nFontID_Or_FontType
            m_nDispFlags_Or_DispOptions = nDispFlags_Or_DispOptions
            m_nFgColor = nFgColor
            m_nBgColor = nBgColor
            m_strMsg = strMsg
            '
            m_bIDGCmd8303 = False '   = 02-20, Output Enhanced Transaction - MSR,
        End Sub
        '        DeviceProperties DevicePropertiesFactory::GetDeviceProperties(eDeviceType type)
        '{
        '	DeviceProperties props;
        '	switch(type)
        '	{
        '	case VP4800:
        '		props = DeviceProperties(type, 0, 0, 0, 0,
        '			TEST_LED|TEST_BUZZER|TEST_PICC|TEST_SAM);
        '		break;

        '	case VP8100:
        '		props = DeviceProperties(type, 16, 2, 
        '			KEY_1|KEY_2|KEY_3|KEY_4|KEY_5|KEY_6|KEY_7|KEY_8|KEY_9|KEY_0|KEY_CANCEL|KEY_BACKSPACE|KEY_ENTER,
        '			KEY_F1|KEY_F2|KEY_F3,
        '            TEST_LED|TEST_BUZZER|TEST_MSR|TEST_PICC|TEST_ICC|TEST_SAM|TEST_SECURITY|TEST_KEY|TEST_ETH|TEST_LCDCONTRA| TEST_PICC_PCD);
        '		break;

        '	case VP8600:
        '		props = DeviceProperties(type, 240, 160, 
        '			KEY_1|KEY_2|KEY_3|KEY_4|KEY_5|KEY_6|KEY_7|KEY_8|KEY_9|KEY_0|KEY_CANCEL|KEY_BACKSPACE|KEY_ENTER,
        '			0,
        '			TEST_LED|TEST_BUZZER|TEST_MSR|TEST_PICC|TEST_ICC|TEST_SAM|TEST_SECURITY|TEST_LCD|TEST_TOUCH|TEST_KEY|TEST_ETH|TEST_LCDCONTRA);
        '		break;

        '	case VP8800:
        '		props = DeviceProperties(type, 480, 272, 
        '			KEY_1|KEY_2|KEY_3|KEY_4|KEY_5|KEY_6|KEY_7|KEY_8|KEY_9|KEY_0|KEY_CANCEL|KEY_BACKSPACE|KEY_ENTER,
        '			0,
        '			TEST_LED|TEST_BUZZER|TEST_MSR|TEST_PICC|TEST_ICC|TEST_SAM|TEST_SECURITY|TEST_LCD|TEST_TOUCH|TEST_KEY|TEST_ETH);
        '		break;

        '	case VPVEND3:
        '		props = DeviceProperties(type, 128, 64,
        '			0,
        '			KEY_F1|KEY_F2|KEY_F3|KEY_F4,
        '			TEST_LED|TEST_BUZZER|TEST_MSR|TEST_PICC|TEST_ICC|TEST_SAM|TEST_SECURITY|TEST_KEY|TEST_ETH|TEST_LCDCONTRA);
        '		break;
        '	}

        '	return props;
        '}
        Public Sub New(nPosX As Integer, nPosY As Integer, nWidth As Integer, _
                       nHeight As Integer, nFontSize As Integer, nFontType As Integer, _
                       nDispOptions As Integer, strMsg As String, u32ControlID As UInt32)
            If (m_tagPos.Equals(Nothing)) Then
                m_tagPos = New Point(nPosX, nPosY)
            Else
                m_tagPos.X = nPosX
                m_tagPos.Y = nPosY
            End If
            m_nWidth = nWidth
            m_nHeight = nHeight
            m_u32CtrlID = u32ControlID
            '
            '
            m_nFontDesignation_Or_FontSize = nFontSize
            m_nFontID_Or_FontType = nFontType
            m_nDispFlags_Or_DispOptions = nDispOptions
            m_strMsg = strMsg
            '
            m_bIDGCmd8303 = True ' 83-03, Display LCD Message
        End Sub
        '
        Public Sub SetMessage(strMsg As String)
            m_strMsg = strMsg
        End Sub
        '
        Property u32propCtrolID As UInt32
            Get
                Return m_u32CtrlID
            End Get
            Set(u32CtrlID As UInt32)
                m_u32CtrlID = u32CtrlID
            End Set
        End Property

        '
        Public Function ToByteDataInTLV(strTag As String) As tagTLV
            Dim arrayByte As Byte() = ToByteData()
            Dim tlv As tagTLV

            tlv = New tagTLV(strTag, arrayByte)
            Erase arrayByte

            Return tlv
        End Function
        '
        Public Function ToByteData() As Byte()
            If (m_bIDGCmd8303) Then
                Return ToByteDataIDGCmd8303_LCDDisplayText()
            Else
                Return ToByteDataIDGCmd0220_TransactionActivateEnchancement()
            End If
        End Function
        '
        Public Function ToByteDataIDGCmd0220_TransactionActivateEnchancement() As Byte()
            'Normalize some fields at first (can't over max value)
            m_nFontDesignation_Or_FontSize = m_nFontDesignation_Or_FontSize Mod 10
            m_nFontID_Or_FontType = m_nFontID_Or_FontType Mod 100
            m_nDispFlags_Or_DispOptions = m_nDispFlags_Or_DispOptions Mod 10

            'LCD Line1 (Top Line)--> DF70+Len(1)+FontDesignation(2)+FontID(3)+ID(1)+DisplayFlag(2,&H0/&H2)+Color(ARGB,9,"00000000\0")+Message(max=45 bytes,String , "XXX\0")
            Dim arrayByteData As Byte() = Nothing
            Dim nLenTmp As Integer = 0
            Dim nLenTmp2 As Integer = 0
            Dim strTmp As String
            Dim strColor As String = "00000000" ' = ARGB
            Dim dictString As Dictionary(Of Integer, String) = New Dictionary(Of Integer, String) From {{0, m_nFontDesignation_Or_FontSize.ToString()}, {1, String.Format("{0,2:2}", m_nFontID_Or_FontType)}, {2, m_nDispFlags_Or_DispOptions.ToString()}, {3, strColor}, {4, m_strMsg}}
            Dim nIdx, nIdxMax As Integer


            Try
                nLenTmp = 2 + 3 + 2 + 9 + (m_strMsg.Length + 1)
                arrayByteData = New Byte(nLenTmp - 1) {}
                '
                nIdxMax = dictString.Count() - 1
                For nIdx = 0 To nIdxMax
                    strTmp = dictString.Item(nIdx)
                    ClassTableListMaster.ConvertFromAscii2HexByte(strTmp, arrayByteData, nLenTmp2, False)
                    nLenTmp2 = nLenTmp2 + strTmp.Length()
                    arrayByteData(nLenTmp2) = &H0 ' String Zero
                    nLenTmp2 = nLenTmp2 + 1
                Next
                '
            Catch ex As Exception
                Form01_Main.GetInstance().LogLn("[Err] TAG_IDG0220_ENH_TXN_ACTIVATE.Get Data() ")
            Finally
                dictString.Clear()

            End Try

            Return arrayByteData
        End Function
        '
        Public Function ToByteDataIDGCmd8303_LCDDisplayText() As Byte()
            'Normalize some fields at first (can't over max value)
            m_nFontDesignation_Or_FontSize = m_nFontDesignation_Or_FontSize Mod 10
            m_nFontID_Or_FontType = m_nFontID_Or_FontType Mod 100
            m_nDispFlags_Or_DispOptions = m_nDispFlags_Or_DispOptions Mod 10

            'LCD Text Display --> 4(Pos X)+4(Pos Y)+4(Width)+4(Height)+2(font Size)+3(fontType)+2(DispFlags/displayOption)+strMsg.Length+1(Null)
            Dim arrayByteData As Byte() = Nothing
            Dim nLenTmp As Integer = 0
            Dim nPos As Integer = 0
            Dim strTmp As String = ""
            Dim strColor As String = "00000000" ' = ARGB
            Dim strTxtInfo As String = String.Format("{0:000} {1:000} {2:000} {3:000} {4} {5:00} {6} {7} ", m_tagPos.X, m_tagPos.Y, m_nWidth, m_nHeight, m_nFontDesignation_Or_FontSize, m_nFontID_Or_FontType, m_nDispFlags_Or_DispOptions, m_strMsg)
            'Dim nIdx, nIdxMax As Integer
            Dim arrayByteDataNullPos As Byte() = New Byte() {3, 3, 3, 3, 1, 2, 1, m_strMsg.Length}
            '
            Try
                nLenTmp = strTxtInfo.Length() ' 4 + 4 + 4 + 4 + 2 + 3 + 2 + (m_strMsg.Length + 1)
                arrayByteData = System.Text.Encoding.ASCII.GetBytes(strTxtInfo)
                '
                For Each idxByte In arrayByteDataNullPos
                    nPos = nPos + idxByte
                    arrayByteData(nPos) = &H0
                    nPos = nPos + 1 ' Null value
                Next
                '
            Catch ex As Exception
                Form01_Main.GetInstance().LogLn("[Err] TAG_IDG0220_ENH_TXN_ACTIVATE.Get Data() =" + ex.Message)
            Finally
                'dictString.Clear()
                'Erase arrayByteData '<--- don't do this since the buffer is return to caller.
            End Try

            Return arrayByteData
        End Function
        '

        Public Sub Cleanup()
            '
        End Sub

    End Structure
    '
    'Non-Secure Mode --> Using 02-20 in AR
    Public Sub TxnActivateEnhancementMSR(ByVal strLine1 As String, ByVal strLine2 As String, Optional ByVal bSecuredMode_02_05 As Boolean = False, Optional ByVal bytePollCardType As ClassReaderProtocolBasic.ENHANCE_CARD_TYPE = ClassReaderProtocolBasic.ENHANCE_CARD_TYPE.POLL_MSR)
        Dim txnEnhInfo, txnEnhInfo2 As ClassTransaction.TAG_IDG0220_IDG8303_ENH_TXN_ACTIVATE

        '------------------------[Start Porcessing]----------------------
        txnEnhInfo = New ClassTransaction.TAG_IDG0220_IDG8303_ENH_TXN_ACTIVATE(0, 0, 1, 1, 0, 0, 0, strLine1)
        txnEnhInfo2 = New ClassTransaction.TAG_IDG0220_IDG8303_ENH_TXN_ACTIVATE(0, 0, 1, 1, 0, 0, 0, strLine2)
        '
        TxnActivateEnhancementMSR(txnEnhInfo, txnEnhInfo2, bytePollCardType, bSecuredMode_02_05)
        '
        txnEnhInfo.Cleanup()
        txnEnhInfo2.Cleanup()
    End Sub
    '
    'Secure Mode --> Using 02-05 in AR V3.0.0
    Public Sub TxnActivateEnhancementMSR_SecureMode_02_05(ByVal strLine1 As String, ByVal strLine2 As String, Optional ByVal bytePollCardType As ClassReaderProtocolBasic.ENHANCE_CARD_TYPE = ClassReaderProtocolBasic.ENHANCE_CARD_TYPE.POLL_MSR)
        Dim txnEnhInfo, txnEnhInfo2 As ClassTransaction.TAG_IDG0220_IDG8303_ENH_TXN_ACTIVATE

        '------------------------[Start Porcessing]----------------------
        txnEnhInfo = New ClassTransaction.TAG_IDG0220_IDG8303_ENH_TXN_ACTIVATE(0, 0, 1, 1, 0, 0, 0, strLine1)
        txnEnhInfo2 = New ClassTransaction.TAG_IDG0220_IDG8303_ENH_TXN_ACTIVATE(0, 0, 1, 1, 0, 0, 0, strLine2)
        '
        TxnActivateEnhancementMSR(txnEnhInfo, txnEnhInfo2, bytePollCardType)
        '
        txnEnhInfo.Cleanup()
        txnEnhInfo2.Cleanup()
    End Sub
    Public Sub TxnActivateEnhancementMSR_AR2_1_5(ByVal strLine1 As String, ByVal strLine2 As String, Optional ByVal bytePollCardType As ClassReaderProtocolBasic.ENHANCE_CARD_TYPE = ClassReaderProtocolBasic.ENHANCE_CARD_TYPE.POLL_MSR)
        Dim txnEnhInfo, txnEnhInfo2 As ClassTransaction.TAG_IDG0220_IDG8303_ENH_TXN_ACTIVATE

        '------------------------[Start Porcessing]----------------------
        txnEnhInfo = New ClassTransaction.TAG_IDG0220_IDG8303_ENH_TXN_ACTIVATE(0, 0, 1, 1, 0, 0, 0, strLine1)
        txnEnhInfo2 = New ClassTransaction.TAG_IDG0220_IDG8303_ENH_TXN_ACTIVATE(0, 0, 1, 1, 0, 0, 0, strLine2)
        '
        TxnActivateEnhancementMSR(txnEnhInfo, txnEnhInfo2, bytePollCardType)
        '
        txnEnhInfo.Cleanup()
        txnEnhInfo2.Cleanup()
    End Sub
    '
    Public Sub TxnActivateEnhancementMSR(ByVal strLine1 As TAG_IDG0220_IDG8303_ENH_TXN_ACTIVATE, ByVal strLine2 As TAG_IDG0220_IDG8303_ENH_TXN_ACTIVATE, ByVal bytePollCardType As ClassReaderProtocolBasic.ENHANCE_CARD_TYPE, Optional ByVal bSecuredMode_02_05 As Boolean = False)
        Dim dictTLVs As Dictionary(Of String, tagTLV) = Nothing
        Dim tlv1 = Nothing, tlv2 = Nothing, tlv3 As tagTLV = Nothing
        Dim pRdrInfo As ClassReaderInfo = ClassReaderInfo.GetIntance()
        Dim bytUIFlag As Byte = &H0
        '
        If (strLine1.m_strMsg.Length > 0) Then
            tlv1 = strLine1.ToByteDataInTLV("DF70")
        End If

        'LCD Line2 (Bottom Line)--> DF71
        If (strLine2.m_strMsg.Length > 0) Then
            tlv2 = strLine2.ToByteDataInTLV("DF71")
        End If

        'Use tag FFEE01
        tagTLV.Combine(tlv1, tlv2, tagTLV.TAG_FFEE01, tlv3)

        If ((tlv3.Equals(Nothing)) Or (tlv3.m_arrayTLVal IsNot Nothing)) Then
            dictTLVs = New Dictionary(Of String, tagTLV)

            dictTLVs.Add(tlv3.m_strTag, tlv3)
        End If


        'If there is LCD supported Feature+AR Platform, we Set UIFlag.b5 = ON
        'Enhanced Act Txn Command 02-20, 02-05 in AR V3.0.0
        If (pRdrInfo.bIsLCDSupported And pRdrInfo.bIsPlatformAR_Pisces_2017) Then
            bytUIFlag = bytUIFlag Or &H10
        End If
        'Do Enhancement Transaction


        TxnActivateEnhancement(dictTLVs, ClassReaderProtocolBasic.ENHANCE_CARD_TYPE.POLL_MSR, 0, 0, bSecuredMode_02_05)

        ClassTableListMaster.CleanupConfigurationsTLVList(dictTLVs)
    End Sub

    Public Sub TxnActivateEnhancement(ByVal dictTLVs As Dictionary(Of String, tagTLV), ByVal bytePollCardType As ClassReaderProtocolBasic.ENHANCE_CARD_TYPE, ByVal byteUIFlag As Byte, ByVal byteMode As Byte, Optional ByVal bSecuredMode_02_05 As Boolean = False)

        m_bActive = True

        Try
            'UIDriverEnableDisable(ClassUIDriver.EnumUIDriverEventEnableDisable.UI_ENABLE_TRANSACTION_START)

            '==================[ Perform Some Action Before Transaction Start ]========================

            m_tagTxnActivateParseResponseInfo.Cleanup()
            'Get Send out TLV Lists for Transaction. Payment Type Selection is also involved.
            '[Don't Forget] To Cleanup Dictionary Resources
            TxnActivateGetTLVs(m_dictTLVsTxnSend)

            'Show / Printout to Main Result Window before Activation
            If (dictTLVs IsNot Nothing) Then
                If (dictTLVs.Count > 0) Then
                    ClassTableListMaster.TLVListsAddFirstList2SecondList(dictTLVs, m_dictTLVsTxnSend)
                End If
            End If
            If ((m_bDebug) Or (m_refwinformMainForm.bIsDebug)) Then

                PrintoutTransactionTLVsBeforeActive(m_dictTLVsTxnSend)
                'PrintoutTransactionTLVsBeforeActive(dictTLVs)


                '-------------------[ Remember Start Time ]-------------------
                m_tagTxnActivateParseResponseInfo.TxnTimeBegin()
            End If

            If (bSecuredMode_02_05) Then
                '2017 Mar 20, goofy_liu@idtechproducts.com.tw
                'Secure Mode - ToDO
#If 0 Then
                '[ToDo] Not Finished Yet. I found in PISCES r23520, this command will cause Txn Rx with V2 Sts = 0x0A status
                ' Keep investigation in near future.
                m_refobjReaderProtocolCmder.TransactionActivateEnhancement_SecureMode(bytePollCardType, byteUIFlag, byteMode, m_dictTLVsTxnSend, m_tagTxnActivateParseResponseInfo, m_refwinformMainForm.IDGCmdTimeoutGet())
#Else
                '[Legacy], this function for Plaintext mode
                m_refobjReaderProtocolCmder.TransactionActivateEnhancement(bytePollCardType, byteUIFlag, byteMode, m_dictTLVsTxnSend, m_tagTxnActivateParseResponseInfo, m_refwinformMainForm.IDGCmdTimeoutGet())
#End If


            Else
                'Non Secure Mode
                'LogLn("Time Begin: " + m_tagTxnActivateParseResponseInfo.m_infoDateTimeTxnBegin.ToString("R"))
                'Send Transaction Activate Command 02-01. <Note> Transaction Commands (ClassReaderCommander) must be responsible to print out Sent & Received Data Content in Hex Text Format. 
                '[Don't Forget] To Release resource m_arrayByteTxnResponse after parsing job done
                m_refobjReaderProtocolCmder.TransactionActivateEnhancement(bytePollCardType, byteUIFlag, byteMode, m_dictTLVsTxnSend, m_tagTxnActivateParseResponseInfo, m_refwinformMainForm.IDGCmdTimeoutGet())
            End If
            '
            If ((m_bDebug) Or (m_refwinformMainForm.bIsDebug)) Then
                '-------------------[ Remember End Time ]-------------------
                m_tagTxnActivateParseResponseInfo.TxnTimeEnd()
            End If
            '==========================[ Transaction Result / Failure Procession ]==========================
            Select Case bytePollCardType
                Case ClassReaderProtocolBasic.ENHANCE_CARD_TYPE.POLL_CONTACT
                Case ClassReaderProtocolBasic.ENHANCE_CARD_TYPE.POLL_CONTACTLESS
                Case ClassReaderProtocolBasic.ENHANCE_CARD_TYPE.POLL_MSR
                    If m_tagTxnActivateParseResponseInfo.bIsProtocolRetStatusOK Then
                        TxnActivateEnchancementParseResponse_CaseOfOK_00h_MSR(m_tagTxnActivateParseResponseInfo)
                    Else
                        TxnActivateEnchancementParseResponse_CaseOfFailed(m_tagTxnActivateParseResponseInfo)
                    End If
                    PrintoutTransactionEnhancementResponse_MSR(m_tagTxnActivateParseResponseInfo)
            End Select


        Catch ex As Exception
            LogLn("[Err] TxnActivate() = " + ex.Message)
        Finally
            '1. Clean up TLV list
            ClassTableListMaster.CleanupConfigurationsTLVList(m_dictTLVsTxnSend)
            '2. Erase Byte Array for Response
            'Erase m_arrayByteTxnResponse
            '3. Clean up m_tagTxnActivateParsedResponseInfo Structure
            'm_tagTxnActivateParseResponseInfo.Cleanup()
            '
            'UIDriverEnableDisable(m_refwinformMainForm.ModeSelectionGet())
            m_refwinformMainForm.UIEnable(True)
            '
            m_bActive = False
        End Try


        'Finally Clean it up

    End Sub
    '
    '
    Public Sub TxnActivateEnchancementParseResponse_CaseOfFailed(ByRef m_tagTxnActivateParseResponseInfo As TAG_TXN_PARSE_RESPONSE_INFO)
        'No Response Data
    End Sub

    '
    Public Sub TxnActivateEnchancementParseResponse_CaseOfOK_00h_MSR(ByRef m_tagTxnActivateParseResponseInfo As TAG_TXN_PARSE_RESPONSE_INFO)
        Dim nTmp As Integer

        Try
            With m_tagTxnActivateParseResponseInfo
                '------------------[ 1. Get MagStripe Track 1 Data If Available ]------------------
                If .m_arrayByteDataResponse Is Nothing Then
                    Exit Sub
                End If
                '
                nTmp = .m_arrayByteDataResponse(.m_nOffset)
                .m_byteMSDLen1 = nTmp
                If nTmp > 0 Then
                    .m_arrayByteMSDData1_Or_OnlineAuthAmountRequested = New Byte(nTmp) {} ' Including NULL Char at the last element
                    Array.Copy(.m_arrayByteDataResponse, .m_nOffset + 1, .m_arrayByteMSDData1_Or_OnlineAuthAmountRequested, 0, nTmp)
                    'The Last Byte is NULL
                    .m_arrayByteMSDData1_Or_OnlineAuthAmountRequested(nTmp) = &H0
                End If
                .m_nOffset = .m_nOffset + nTmp + 1

                '------------------[ 2. Get MagStripe Track 2 Data If Available ]------------------
                nTmp = .m_arrayByteDataResponse(.m_nOffset)
                .m_byteMSDLen2 = nTmp
                If nTmp > 0 Then
                    .m_arrayByteMSDData2 = New Byte(nTmp) {} ' Including NULL Char at the last element
                    Array.Copy(.m_arrayByteDataResponse, .m_nOffset + 1, .m_arrayByteMSDData2, 0, nTmp)
                End If
                .m_nOffset = .m_nOffset + nTmp + 1

                '------------------[ 3. Get MagStripe Track 3 Data If Available ]------------------
                nTmp = .m_arrayByteDataResponse(.m_nOffset)
                .m_byteMSDLen3 = nTmp
                If nTmp > 0 Then
                    .m_arrayByteMSDData3 = New Byte(nTmp) {} ' Including NULL Char at the last element
                    Array.Copy(.m_arrayByteDataResponse, .m_nOffset + 1, .m_arrayByteMSDData3, 0, nTmp)
                End If
                .m_nOffset = .m_nOffset + nTmp + 1

            End With
        Catch ex As Exception
            LogLn("[Err] TxnActivateEnchancementParseResponse_CaseOfOK_00h() = " + ex.Message)
        End Try

    End Sub
    '
    Public Sub TxnActivateUpdateBalance(bValApproved As Boolean)
        Dim dictTLVsTxnSend2 As Dictionary(Of String, tagTLV) = Nothing
        Try
            m_bActive = True

            dictTLVsTxnSend2 = New Dictionary(Of String, tagTLV)
            Dim strTag As String
            Dim byteApprovedDeclined As Byte

            'UIDriverEnableDisable(ClassUIDriver.EnumUIDriverEventEnableDisable.UI_ENABLE_TRANSACTION_START)

            m_tagTxnActivateParseResponseInfo.Cleanup()
            'Get Send out TLV Lists for Transaction. Payment Type Selection is also involved.
            '[Don't Forget] To Cleanup Dictionary Resources
            TxnActivateGetTLVs(m_dictTLVsTxnSend)

            'Is Auto Approved
            'If m_CBOnlineAuthAutoApproved.Checked Then
            If bValApproved Then
                byteApprovedDeclined = &H0
            Else
                byteApprovedDeclined = &H1
            End If

            'Only 9A and 9F21
            strTag = "9A"
            If m_dictTLVsTxnSend.ContainsKey(strTag) Then
                dictTLVsTxnSend2.Add(strTag, m_dictTLVsTxnSend.Item(strTag))
            End If
            strTag = "9F21"
            If m_dictTLVsTxnSend.ContainsKey(strTag) Then
                dictTLVsTxnSend2.Add(strTag, m_dictTLVsTxnSend.Item(strTag))
            End If

            'Show / Printout to Main Result Window before Activation
            PrintoutTransactionTLVsBeforeActive(dictTLVsTxnSend2)

            '-------------------[ Remember Start Time ]-------------------
            m_tagTxnActivateParseResponseInfo.TxnTimeBegin()

            'LogLn("Time Begin: " + m_tagTxnActivateParseResponseInfo.m_infoDateTimeTxnBegin.ToString("R"))
            'Send Transaction Activate Command 02-01. <Note> Transaction Commands (ClassReaderCommander) must be responsible to print out Sent & Received Data Content in Hex Text Format. 
            '[Don't Forget] To Release resource m_arrayByteTxnResponse after parsing job done
            m_refobjReaderProtocolCmder.TransactionUpdateBalance(byteApprovedDeclined, Nothing, dictTLVsTxnSend2, m_tagTxnActivateParseResponseInfo, m_refwinformMainForm.IDGCmdTimeoutGet())

            '-------------------[ Remember End Time ]-------------------
            m_tagTxnActivateParseResponseInfo.TxnTimeEnd()

            '==========================[ Transaction Result / Failure Procession ]==========================
            If m_tagTxnActivateParseResponseInfo.bIsProtocolRetStatusOK Then
                '-----------------------------------[ Transaction OK ]-----------------------------------
                'LogLn("Time End: " + m_tagTxnActivateParseResponseInfo.m_infoDateTimeTxnEnd.ToString("R"))
                'Do Parser Response Data
                TxnUpdateBalanceParseResponse_CaseOfOK(m_tagTxnActivateParseResponseInfo)

                'Show / Printout to Main Result Window After Response Data Has Been Parsed
                PrintoutTxnUpdateBalanceResponse_CaseOfOK(m_tagTxnActivateParseResponseInfo, bValApproved)

            Else
                TxnUpdateBalanceParseResponse_CaseOfFailure(m_tagTxnActivateParseResponseInfo)

                PrintoutTxnUpdateBalanceResponse_CaseOfFailure(m_tagTxnActivateParseResponseInfo, bValApproved)
                '-----------------------------------[  ]-----------------------------------
                'Other Failure Case, [Special Note] about Retrying Case in Card Returned Error Status
                If (m_tagTxnActivateParseResponseInfo.bIsTransactionRFErrorAllowedRetryTransaction) Then
                    LogLn("[Info] RF Err Code - Retry Transaction" + m_tagTxnActivateParseResponseInfo.strGetTransactionResult)
                End If
            End If

        Catch ex As Exception
            LogLn("[Err] TxnActivateUpdateBalance() = " + ex.Message)
        Finally
            '1. Clean up TLV lists
            ClassTableListMaster.CleanupConfigurationsTLVList(m_dictTLVsTxnSend)
            ClassTableListMaster.CleanupConfigurationsTLVList(dictTLVsTxnSend2)
            '2. Erase Byte Array for Response
            'Erase m_arrayByteTxnResponse
            '3. Clean up m_tagTxnActivateParsedResponseInfo Structure
            'm_tagTxnActivateParseResponseInfo.Cleanup()
            '
            'UIDriverEnableDisable(m_refwinformMainForm.ModeSelectionGet())
            m_refwinformMainForm.UIEnable(True)
            '
            m_bActive = False
        End Try


        'Finally Clean it up

    End Sub

    Public Sub TxnStop()

    End Sub

    Public Sub TxnCancel()
        m_refobjReaderProtocolCmder.TransactionCancel(m_refwinformMainForm.IDGCmdTimeoutGet())
    End Sub

    Public Sub TxnLogClean()

    End Sub

    Public Sub TxnLogReset()

    End Sub
    '
    '==========================[ Transaction Activation - NEO Platform, Generic Txn, 02-40 ]==========================
    Public Sub TxnActivateParseResponse_CaseOfOK_00h_NEOGeneric_02_40_VP6300_Beta01(ByRef m_tagTxnActivateParseResponseInfo As TAG_TXN_PARSE_RESPONSE_INFO)
        '
        'Dim nTmp As Integer
        Dim tlv As tagTLV = Nothing
        Dim strTmp As String = ""
        Dim objRdrInfo As ClassReaderInfo = ClassReaderInfo.GetIntance()
        Dim bResult As Boolean = False

        Try
            ' 1st data = Attribute Byte , 1 byte
            ' 2nd data = TLVs (KSN,MSR,Clearing Data, Outcome, and Other Kind of Txn Resp TLVs)
            With m_tagTxnActivateParseResponseInfo

                If .m_arrayByteDataResponse Is Nothing Then
                    Exit Sub
                End If
                '
#If 1 Then
                'Get Raw Data Directly
                ClassTableListMaster.ConvertFromHexToAscii(.m_arrayByteDataResponse, .m_nOffset, .m_arrayByteDataResponse.Length() - .m_nOffset, strTmp, False)

                objRdrInfo.VerifyMSRTrackIsReady_StartStop() ' Start  MSR Parsing, 2016 Sept 09
                bResult = objRdrInfo.ParseAndVerifyMSRTrackIsReady_AutoPollBurstMode(strTmp)
                objRdrInfo.VerifyMSRTrackIsReady_StartStop(False) 'Stop MSR Parsing, 2016 Sept 09
                '
                If (Not bResult) Then
                    'Can't Parse MSR Data, Show Error
                    Throw New Exception(tagTLV.TAG_DFEE23 + " Invalid MSR Data, " + strTmp)
                End If
                '
                'Get Track  1
                If (objRdrInfo.bIsMSRAvailableTrack1) Then
                    strTmp = objRdrInfo.s_strTrack1
                    .m_byteMSDLen1 = strTmp.Length()
                    ClassTableListMaster.ConvertFromAscii2HexByte(strTmp, .m_arrayByteMSDData1_Or_OnlineAuthAmountRequested)
                End If
                'Get Track  2
                If (objRdrInfo.bIsMSRAvailableTrack2) Then
                    strTmp = objRdrInfo.s_strTrack2
                    .m_byteMSDLen2 = strTmp.Length()
                    ClassTableListMaster.ConvertFromAscii2HexByte(strTmp, .m_arrayByteMSDData2)
                End If
                'Get Track  3
                If (objRdrInfo.bIsMSRAvailableTrack3) Then
                    strTmp = objRdrInfo.s_strTrack3
                    .m_byteMSDLen3 = strTmp.Length()
                    ClassTableListMaster.ConvertFromAscii2HexByte(strTmp, .m_arrayByteMSDData3)
                End If
#Else


                '------------------[ 1. Get Attribute Byte, 1 Byte ]------------------
                .m_byteEncryption_01_AttributeByte = .m_arrayByteDataResponse(.m_nOffset)
                .m_nOffset = .m_nOffset + 1
                '------------------[ 2. Parse TLVs, Size In Bytes ]------------------
                TxnActivateParseResponseDoTLVs(.m_arrayByteDataResponse, .m_nOffset, .m_arrayByteDataResponse.Length() - .m_nOffset, .m_dictTlvsTxnResult)

                '------------------[ 3. Get MSR TLV -  DFEE23, if MSR Data Exists. i.e. MSR Flag B0b3 ON]------------
                ' We don't support cipher text MSR data right now, so kick it off !!
                If ((.m_byteEncryption_01_AttributeByte And &H80) = &H80) Then
                    'B0b7 Encryption Mode
                    'One more check KSN Tag Presents ? i.e. FFEE12
                    If (.m_dictTlvsTxnResult.Count > 0) Then
                        If (.m_dictTlvsTxnResult.ContainsKey(tagTLV.TAG_FFEE12)) Then
                            ' We don't be able to decrypt it here
                            Throw New Exception("Encryption Mode Detected !! Don't Support It !! ")
                        End If
                    End If
                End If
                If ((.m_byteEncryption_01_AttributeByte And &H8) = &H8) Then
                    .m_bIsEncMSRButNoDUKPTKeyMode = True

                    '------------------[ 4. Parse Track 1, 2, 3(optional, depending on reader type ]------------
                    If ((.m_dictTlvsTxnResult IsNot Nothing) And (.m_dictTlvsTxnResult.Count > 0) And (.m_dictTlvsTxnResult.ContainsKey(tagTLV.TAG_DFEE23) Or .m_dictTlvsTxnResult.ContainsKey(tagTLV.TAG_DF4B))) Then


                        If (.m_dictTlvsTxnResult.ContainsKey(tagTLV.TAG_DFEE23)) Then
                            tlv = .m_dictTlvsTxnResult(tagTLV.TAG_DFEE23)
                        Else 'DF4B
                            'Unipay 1.5, TTK Version ONLY
                            'See "Unipay III & Unipay 1D5 Supplement"
                            tlv = .m_dictTlvsTxnResult(tagTLV.TAG_DF4B)
                        End If
                        If (tlv.m_nLen < 1) Then
                            Throw New Exception(tagTLV.TAG_DFEE23 + " contains No MSR Data !!")
                        End If
                        ClassTableListMaster.ConvertFromHexToAscii(tlv.m_arrayTLVal, strTmp, False)
                        '
                        objRdrInfo.VerifyMSRTrackIsReady_StartStop() ' Start  MSR Parsing, 2016 Sept 09
                        bResult = objRdrInfo.ParseAndVerifyMSRTrackIsReady_AutoPollBurstMode(strTmp)
                        objRdrInfo.VerifyMSRTrackIsReady_StartStop(False) 'Stop MSR Parsing, 2016 Sept 09
                        '
                        If (Not bResult) Then
                            'Can't Parse MSR Data, Show Error
                            Throw New Exception(tagTLV.TAG_DFEE23 + " Invalid MSR Data, " + strTmp)
                        End If
                        '
                        'Get Track  1
                        If (objRdrInfo.bIsMSRAvailableTrack1) Then
                            strTmp = objRdrInfo.s_strTrack1
                            .m_byteMSDLen1 = strTmp.Length()
                            ClassTableListMaster.ConvertFromAscii2HexByte(strTmp, .m_arrayByteMSDData1_Or_OnlineAuthAmountRequested)
                        End If
                        'Get Track  2
                        If (objRdrInfo.bIsMSRAvailableTrack2) Then
                            strTmp = objRdrInfo.s_strTrack2
                            .m_byteMSDLen2 = strTmp.Length()
                            ClassTableListMaster.ConvertFromAscii2HexByte(strTmp, .m_arrayByteMSDData2)
                        End If
                        'Get Track  3
                        If (objRdrInfo.bIsMSRAvailableTrack3) Then
                            strTmp = objRdrInfo.s_strTrack3
                            .m_byteMSDLen3 = strTmp.Length()
                            ClassTableListMaster.ConvertFromAscii2HexByte(strTmp, .m_arrayByteMSDData3)
                        End If
                    End If
                Else
                    'No MSR Data
                End If ' End of MSR Parsing
#End If
                'Set Offset Value
                .m_nOffset = .m_arrayByteDataResponse.Length()

            End With
        Catch ex As Exception
            LogLn("[Err] TxnActivateParseResponse_CaseOfOK_00h() = " + ex.Message)
        End Try
        '
    End Sub

    Public Sub TxnActivateParseResponse_CaseOfOK_00h_NEOGeneric_02_40(ByRef m_tagTxnActivateParseResponseInfo As TAG_TXN_PARSE_RESPONSE_INFO)
        Dim pRdrInfo As ClassReaderInfo = ClassReaderInfo.GetIntance()
        If (pRdrInfo.bIsPlatformNEO20_2017) Then
            TxnActivateParseResponse_CaseOfOK_00h_NEOGeneric_02_40_VP6300_Beta01(m_tagTxnActivateParseResponseInfo)
        Else
            TxnActivateParseResponse_CaseOfOK_00h_NEOGeneric_02_40_Native(m_tagTxnActivateParseResponseInfo)
        End If
    End Sub
    '==========================[ Transaction Activation - NEO Platform, Generic Txn, 02-40 ]==========================
    Public Sub TxnActivateParseResponse_CaseOfOK_00h_NEOGeneric_02_40_Native(ByRef m_tagTxnActivateParseResponseInfo As TAG_TXN_PARSE_RESPONSE_INFO)
        'Dim nTmp As Integer
        Dim tlv As tagTLV = Nothing
        Dim strTmp As String = ""
        Dim objRdrInfo As ClassReaderInfo = ClassReaderInfo.GetIntance()
        Dim bResult As Boolean = False

        Try
            ' 1st data = Attribute Byte , 1 byte
            ' 2nd data = TLVs (KSN,MSR,Clearing Data, Outcome, and Other Kind of Txn Resp TLVs)
            With m_tagTxnActivateParseResponseInfo

                If .m_arrayByteDataResponse Is Nothing Then
                    Exit Sub
                End If
                '
                '------------------[ 1. Get Attribute Byte, 1 Byte ]------------------
                .m_byteEncryption_01_AttributeByte = .m_arrayByteDataResponse(.m_nOffset)
                .m_nOffset = .m_nOffset + 1
                '------------------[ 2. Parse TLVs, Size In Bytes ]------------------
                TxnActivateParseResponseDoTLVs(.m_arrayByteDataResponse, .m_nOffset, .m_arrayByteDataResponse.Length() - .m_nOffset, .m_dictTlvsTxnResult)

                '------------------[ 3. Get MSR TLV -  DFEE23, if MSR Data Exists. i.e. MSR Flag B0b3 ON]------------
                ' We don't support cipher text MSR data right now, so kick it off !!
                If ((.m_byteEncryption_01_AttributeByte And &H80) = &H80) Then
                    'B0b7 Encryption Mode
                    'One more check KSN Tag Presents ? i.e. FFEE12
                    If (.m_dictTlvsTxnResult.Count > 0) Then
                        If (.m_dictTlvsTxnResult.ContainsKey(tagTLV.TAG_FFEE12)) Then
                            ' We don't be able to decrypt it here
                            Throw New Exception("Encryption Mode Detected !! Don't Support It !! ")
                        End If
                    End If
                End If
                If ((.m_byteEncryption_01_AttributeByte And &H8) = &H8) Then
                    .m_bIsEncMSRButNoDUKPTKeyMode = True

                    '------------------[ 4. Parse Track 1, 2, 3(optional, depending on reader type ]------------
                    If ((.m_dictTlvsTxnResult IsNot Nothing) And (.m_dictTlvsTxnResult.Count > 0) And (.m_dictTlvsTxnResult.ContainsKey(tagTLV.TAG_DFEE23) Or .m_dictTlvsTxnResult.ContainsKey(tagTLV.TAG_DF4B))) Then

                        If (.m_dictTlvsTxnResult.ContainsKey(tagTLV.TAG_DFEE23)) Then
                            tlv = .m_dictTlvsTxnResult(tagTLV.TAG_DFEE23)
                        Else 'DF4B
                            'Unipay 1.5, TTK Version ONLY
                            'See "Unipay III & Unipay 1D5 Supplement"
                            tlv = .m_dictTlvsTxnResult(tagTLV.TAG_DF4B)
                        End If
                        If (tlv.m_nLen < 1) Then
                            Throw New Exception(tagTLV.TAG_DFEE23 + " contains No MSR Data !!")
                        End If
                        ClassTableListMaster.ConvertFromHexToAscii(tlv.m_arrayTLVal, strTmp, False)
                        '
                        objRdrInfo.VerifyMSRTrackIsReady_StartStop() ' Start  MSR Parsing, 2016 Sept 09
                        bResult = objRdrInfo.ParseAndVerifyMSRTrackIsReady_AutoPollBurstMode(strTmp)
                        objRdrInfo.VerifyMSRTrackIsReady_StartStop(False) 'Stop MSR Parsing, 2016 Sept 09
                        '
                        If (Not bResult) Then
                            'Can't Parse MSR Data, Show Error
                            Throw New Exception(tagTLV.TAG_DFEE23 + " Invalid MSR Data, " + strTmp)
                        End If
                        '
                        'Get Track  1
                        If (objRdrInfo.bIsMSRAvailableTrack1) Then
                            strTmp = objRdrInfo.s_strTrack1
                            .m_byteMSDLen1 = strTmp.Length()
                            ClassTableListMaster.ConvertFromAscii2HexByte(strTmp, .m_arrayByteMSDData1_Or_OnlineAuthAmountRequested)
                        End If
                        'Get Track  2
                        If (objRdrInfo.bIsMSRAvailableTrack2) Then
                            strTmp = objRdrInfo.s_strTrack2
                            .m_byteMSDLen2 = strTmp.Length()
                            ClassTableListMaster.ConvertFromAscii2HexByte(strTmp, .m_arrayByteMSDData2)
                        End If
                        'Get Track  3
                        If (objRdrInfo.bIsMSRAvailableTrack3) Then
                            strTmp = objRdrInfo.s_strTrack3
                            .m_byteMSDLen3 = strTmp.Length()
                            ClassTableListMaster.ConvertFromAscii2HexByte(strTmp, .m_arrayByteMSDData3)
                        End If
                    End If
#If 0 Then
                    .m_byteMSDLen1 = nTmp
                    If nTmp > 0 Then
                        .m_arrayByteMSDData1_Or_OnlineAuthAmountRequested = New Byte(nTmp) {} ' Including NULL Char at the last element
                        Array.Copy(.m_arrayByteDataResponse, .m_nOffset + 1, .m_arrayByteMSDData1_Or_OnlineAuthAmountRequested, 0, nTmp)
                        'The Last Byte is NULL
                        .m_arrayByteMSDData1_Or_OnlineAuthAmountRequested(nTmp) = &H0
                    End If
                    .m_nOffset = .m_nOffset + nTmp + 1

                    '------------------[ 2. Get MagStripe Track 2 Data If Available ]------------------
                    nTmp = .m_arrayByteDataResponse(.m_nOffset)
                    .m_byteMSDLen2 = nTmp
                    If nTmp > 0 Then
                        .m_arrayByteMSDData2 = New Byte(nTmp) {} ' Including NULL Char at the last element
                        Array.Copy(.m_arrayByteDataResponse, .m_nOffset + 1, .m_arrayByteMSDData2, 0, nTmp)
                    End If
                    .m_nOffset = .m_nOffset + nTmp + 1
#End If
                Else
                    'No MSR Data
                End If ' End of MSR Parsing

                'Set Offset Value
                .m_nOffset = .m_arrayByteDataResponse.Length()

            End With
        Catch ex As Exception
            LogLn("[Err] TxnActivateParseResponse_CaseOfOK_00h() = " + ex.Message)
        End Try

    End Sub
    '==========================[ Transaction Activation ]==========================
    Public Sub TxnActivateParseResponse_CaseOfOK_00h(ByRef m_tagTxnActivateParseResponseInfo As TAG_TXN_PARSE_RESPONSE_INFO)
        Dim nTmp As Integer

        Try
            With m_tagTxnActivateParseResponseInfo
                '------------------[ 1. Get MagStripe Track 1 Data If Available ]------------------
                If .m_arrayByteDataResponse Is Nothing Then
                    Exit Sub
                End If
                '
                nTmp = .m_arrayByteDataResponse(.m_nOffset)
                .m_byteMSDLen1 = nTmp
                If nTmp > 0 Then
                    .m_arrayByteMSDData1_Or_OnlineAuthAmountRequested = New Byte(nTmp) {} ' Including NULL Char at the last element
                    Array.Copy(.m_arrayByteDataResponse, .m_nOffset + 1, .m_arrayByteMSDData1_Or_OnlineAuthAmountRequested, 0, nTmp)
                    'The Last Byte is NULL
                    .m_arrayByteMSDData1_Or_OnlineAuthAmountRequested(nTmp) = &H0
                End If
                .m_nOffset = .m_nOffset + nTmp + 1

                '------------------[ 2. Get MagStripe Track 2 Data If Available ]------------------
                nTmp = .m_arrayByteDataResponse(.m_nOffset)
                .m_byteMSDLen2 = nTmp
                If nTmp > 0 Then
                    .m_arrayByteMSDData2 = New Byte(nTmp) {} ' Including NULL Char at the last element
                    Array.Copy(.m_arrayByteDataResponse, .m_nOffset + 1, .m_arrayByteMSDData2, 0, nTmp)
                End If
                .m_nOffset = .m_nOffset + nTmp + 1

                '------------------[ 3. Get Clearing Record (i.e. C.R. , tag = &HE1, i.e. DE055) Data If Available ]------------------
                nTmp = .m_arrayByteDataResponse(.m_nOffset)
                .m_byteClearingRecordFlags = nTmp ' = &H00 or &H01 (C.R. Presents)
                If (nTmp <> &H0) And (.m_arrayByteDataResponse(.m_nOffset + 1) = &HE1) Then
                    'Get C.R. Data Length, 1 byte
                    nTmp = .m_arrayByteDataResponse(.m_nOffset + 2)
                    'TLV Type: <E1>+ <??>(Length)+(1~128 bytes)
                    TxnActivateParseResponseDoTLVs(.m_arrayByteDataResponse, .m_nOffset + 3, nTmp, .m_dictTlvsClearingRecordE1_DE055)
                    .m_nOffset = .m_nOffset + nTmp + 3
                Else
                    .m_nOffset = .m_nOffset + 1
                End If

                '------------------[ 4. Parse Multiple TLVs Data ]------------------
                nTmp = .m_arrayByteDataResponse.Count - .m_nOffset
                If (nTmp < 2) Then
                    Throw New Exception("No TLVs Available")
                End If
                TxnActivateParseResponseDoTLVs(.m_arrayByteDataResponse, .m_nOffset, nTmp, .m_listTlvsTxnResult)
            End With
        Catch ex As Exception
            LogLn("[Err] TxnActivateParseResponse_CaseOfOK_00h() = " + ex.Message)
        End Try

    End Sub
    '
    Public Sub TxnActivateParseResponse_CaseOfFailure_23h_OnlineAuth(ByRef m_tagTxnActivateParseResponseInfo As TAG_TXN_PARSE_RESPONSE_INFO)
        Dim nTmp As Integer
        Try
            With m_tagTxnActivateParseResponseInfo
                '------------------[ 1. Buffer Validation ]------------------
                If .m_arrayByteDataResponse Is Nothing Then
                    Throw New Exception("No Recv Data Buffer")
                End If
                '
                '------------------[ 1. 1 byte, Error Code ]------------------
                .m_nTransactionErrorCode = .m_arrayByteDataResponse(.m_nOffset)
                .m_nOffset = .m_nOffset + nTmp + 1

                '------------------[ 2. 1 byte, SW1 ]------------------
                .m_u16SW1 = .m_arrayByteDataResponse(.m_nOffset)
                .m_nOffset = .m_nOffset + nTmp + 1
                '------------------[ 3, 1 byte, SW2 ]------------------
                .m_u16SW2 = .m_arrayByteDataResponse(.m_nOffset)
                .m_nOffset = .m_nOffset + nTmp + 1

                '------------------[ 4, 1 byte, RF Status Code ]------------------
                .m_nRFStatusCode = .m_arrayByteDataResponse(.m_nOffset)
                .m_nOffset = .m_nOffset + nTmp + 1

                '------------------[ 5. TLV Track 2 Equivalent Data, Tag: 57 ]------------------
                nTmp = .m_arrayByteDataResponse.Count - .m_nOffset - 6
                If (nTmp <> 21) Then
                    Throw New Exception(" Incorrect Format, Track2 Equivalent Data")
                End If
                TxnActivateParseResponseDoTLVs(.m_arrayByteDataResponse, .m_nOffset, nTmp, .m_listTlvsTxnResult)
                .m_nOffset = .m_nOffset + 21

                '------------------[ 6. Amount Requested, 6 byte, n12, Difference between the Maximum Offline Spending Amount And Balance ]------------------
                If ((.m_nOffset + 6) <> .m_arrayByteDataResponse.Count) Then
                    Throw New Exception("Amount Requested Format Error")
                End If
                .m_arrayByteMSDData1_Or_OnlineAuthAmountRequested = New Byte(6 - 1) {} ' Including NULL Char at the last element
                Array.Copy(.m_arrayByteDataResponse, .m_nOffset, .m_arrayByteMSDData1_Or_OnlineAuthAmountRequested, 0, 6)

                ''------------------[ 4. Parse Multiple TLVs Data ]------------------
                'nTmp = .m_arrayByteDataResponse.Count - .m_nOffset
                'TxnActivateParseResponseDoTLVs(.m_arrayByteDataResponse, .m_nOffset, nTmp, .m_dictTlvsTxnResult)
            End With
        Catch ex As Exception
            LogLn("[Err] TxnActivateParseResponse_CaseOfFailure_23h_OnlineAuth () = " + ex.Message)
        End Try

    End Sub

    '
    Public Sub TxnActivateParseResponse_CaseOfFailure_0Eh_UserInterfaceEvent(ByRef m_tagTxnActivateParseResponseInfo As TAG_TXN_PARSE_RESPONSE_INFO)
        Throw New Exception("TxnActivateParseResponse_CaseOfFailure_0Eh_UserInterfaceEvent() = Not Implemented")
        '
        Try
            Dim nTmp As Integer = 0

            With m_tagTxnActivateParseResponseInfo
                If .m_arrayByteDataResponse Is Nothing Then
                    Exit Sub
                End If

                '------------------[ 1. Get MagStripe Track 1 Data If Available ]------------------
                nTmp = .m_arrayByteDataResponse(.m_nOffset)
                .m_byteMSDLen1 = nTmp
                If nTmp > 0 Then
                    .m_arrayByteMSDData1_Or_OnlineAuthAmountRequested = New Byte(nTmp) {} ' Including NULL Char at the last element
                    Array.Copy(.m_arrayByteDataResponse, .m_nOffset + 1, .m_arrayByteMSDData1_Or_OnlineAuthAmountRequested, 0, nTmp)
                    'The Last Byte is NULL
                    .m_arrayByteMSDData1_Or_OnlineAuthAmountRequested(nTmp) = &H0
                End If
                .m_nOffset = .m_nOffset + nTmp + 1

                '------------------[ 2. Get MagStripe Track 2 Data If Available ]------------------
                nTmp = .m_arrayByteDataResponse(.m_nOffset)
                .m_byteMSDLen2 = nTmp
                If nTmp > 0 Then
                    .m_arrayByteMSDData2 = New Byte(nTmp) {} ' Including NULL Char at the last element
                    Array.Copy(.m_arrayByteDataResponse, .m_nOffset + 1, .m_arrayByteMSDData2, 0, nTmp)
                End If
                .m_nOffset = .m_nOffset + nTmp + 1

                '------------------[ 3. Get Clearing Record (i.e. C.R. , tag = &HE1, i.e. DE055) Data If Available ]------------------
                nTmp = .m_arrayByteDataResponse(.m_nOffset)
                .m_byteClearingRecordFlags = nTmp ' = &H00 or &H01 (C.R. Presents)
                If (nTmp <> &H0) And (.m_arrayByteDataResponse(.m_nOffset + 1) = &HE1) Then
                    'Get C.R. Data Length, 1 byte
                    nTmp = .m_arrayByteDataResponse(.m_nOffset + 2)
                    'TLV Type: <E1>+ <??>(Length)+(1~128 bytes)
                    TxnActivateParseResponseDoTLVs(.m_arrayByteDataResponse, .m_nOffset + 3, nTmp, .m_dictTlvsClearingRecordE1_DE055)
                    .m_nOffset = .m_nOffset + nTmp + 3
                Else
                    .m_nOffset = .m_nOffset + 1
                End If

                '------------------[ 4. Parse Multiple TLVs Data ]------------------
                nTmp = .m_arrayByteDataResponse.Count - .m_nOffset
                TxnActivateParseResponseDoTLVs(.m_arrayByteDataResponse, .m_nOffset, nTmp, .m_listTlvsTxnResult)
            End With
        Catch ex As Exception
            LogLn("[Err] " + ex.Message)
        Finally

        End Try
    End Sub
    '
    Public Sub TxnActivateParseResponse_CaseOfFailure_Not_23h_OnlineAuth_Not_0Eh_UserInterfaceEvent(ByRef m_tagTxnActivateParseResponseInfo As TAG_TXN_PARSE_RESPONSE_INFO)
        Dim nTmp As Integer
        Try
            With m_tagTxnActivateParseResponseInfo
                '------------------[ 1. Buffer Validation ]------------------
                If .m_arrayByteDataResponse Is Nothing Then
                    Throw New Exception("[Err] No Recv Data Buffer")
                End If

                '------------------[ 1. 1 byte, Error Code ]------------------
                .m_nTransactionErrorCode = .m_arrayByteDataResponse(.m_nOffset)
                .m_nOffset = .m_nOffset + nTmp + 1

                '------------------[ 2. 1 byte, SW1 ]------------------
                .m_u16SW1 = .m_arrayByteDataResponse(.m_nOffset)
                .m_nOffset = .m_nOffset + nTmp + 1
                '------------------[ 3, 1 byte, SW2 ]------------------
                .m_u16SW2 = .m_arrayByteDataResponse(.m_nOffset)
                .m_nOffset = .m_nOffset + nTmp + 1

                '------------------[ 4, 1 byte, RF Status Code ]------------------
                .m_nRFStatusCode = .m_arrayByteDataResponse(.m_nOffset)
                .m_nOffset = .m_nOffset + nTmp + 1

                ''------------------[ 5. (Optional, if available )Parse Multiple TLVs Data ]-----------------
                nTmp = .m_arrayByteDataResponse.Count - .m_nOffset
                If (nTmp > 0) Then
                    TxnActivateParseResponseDoTLVs(.m_arrayByteDataResponse, .m_nOffset, nTmp, .m_listTlvsTxnResult)
                End If

            End With
        Catch ex As Exception
            LogLn("[Err] TxnActivateParseResponse_CaseOfFailure_Not_23h_OnlineAuth_Not_0Eh_UserInterfaceEvent() =" + ex.Message)
        End Try
    End Sub

    '

    Private Sub TxnActivateParseResponseDoTLVs(ByRef arrayByteDataResponse As Byte(), ByVal nPosStart As Integer, ByVal nDataLen As Integer, ByRef o_dictTlvsParsed As Dictionary(Of String, tagTLV))
        Dim array3BytesTag As Byte() = New Byte(2) {}
        Dim array2BytesTag As Byte() = New Byte(1) {}
        'Dim arrayByteTag As Byte

        Try
            If o_dictTlvsParsed IsNot Nothing Then
                If o_dictTlvsParsed.Count > 0 Then
                    ClassTableListMaster.CleanupConfigurationsTLVList(o_dictTlvsParsed)
                End If
            Else
                o_dictTlvsParsed = New Dictionary(Of String, tagTLV)
            End If

            '4.1.Find 3-byte Tag
            'Array.Copy(m_arrayByteDataResponse, nPosStart, array3BytesTag, 3, )
            '4.2. If Not 1. , Try 2-byte tag
            '4.3. If Not 2., Try 1-byte tag
            '4.4. If Not 3., Show TLV not found error
            ClassReaderCommanderParser.TLV_ParseFromReaderMultipleTLVs(arrayByteDataResponse, nPosStart, nDataLen, o_dictTlvsParsed)

        Catch ex As Exception
            LogLn("[Err] TxnActivateParseResponseDoTLVs() = " + ex.Message)
        Finally
            Erase array3BytesTag
            Erase array2BytesTag
        End Try

    End Sub

    Private Sub TxnActivateParseResponseDoTLVs(ByRef arrayByteDataResponse As Byte(), ByVal nPosStart As Integer, ByVal nDataLen As Integer, ByRef o_dictTlvsParsed As SortedDictionary(Of String, tagTLV))
        Dim array3BytesTag As Byte() = New Byte(2) {}
        Dim array2BytesTag As Byte() = New Byte(1) {}
        'Dim arrayByteTag As Byte

        Try
            If o_dictTlvsParsed IsNot Nothing Then
                If o_dictTlvsParsed.Count > 0 Then
                    ClassTableListMaster.CleanupConfigurationsTLVList(o_dictTlvsParsed)
                End If
            Else
                o_dictTlvsParsed = New SortedDictionary(Of String, tagTLV)
            End If

            '4.1.Find 3-byte Tag
            'Array.Copy(m_arrayByteDataResponse, nPosStart, array3BytesTag, 3, )
            '4.2. If Not 1. , Try 2-byte tag
            '4.3. If Not 2., Try 1-byte tag
            '4.4. If Not 3., Show TLV not found error
            ClassReaderCommanderParser.TLV_ParseFromReaderMultipleTLVs(arrayByteDataResponse, nPosStart, nDataLen, o_dictTlvsParsed)

        Catch ex As Exception
            LogLn("[Err] TxnActivateParseResponseDoTLVs() = " + ex.Message)
        Finally
            Erase array3BytesTag
            Erase array2BytesTag
        End Try

    End Sub

    Private Sub TxnActivateParseResponseDoTLVs(ByRef arrayByteDataResponse As Byte(), ByVal nPosStart As Integer, ByVal nDataLen As Integer, ByRef o_listTlvsParsed As List(Of tagTLV))
        Dim array3BytesTag As Byte() = New Byte(2) {}
        Dim array2BytesTag As Byte() = New Byte(1) {}
        'Dim arrayByteTag As Byte

        Try
            If o_listTlvsParsed IsNot Nothing Then
                ClassTableListMaster.CleanupConfigurationsTLVList(o_listTlvsParsed)
            Else
                o_listTlvsParsed = New List(Of tagTLV)
            End If

            '4.1.Find 3-byte Tag
            'Array.Copy(m_arrayByteDataResponse, nPosStart, array3BytesTag, 3, )
            '4.2. If Not 1. , Try 2-byte tag
            '4.3. If Not 2., Try 1-byte tag
            '4.4. If Not 3., Show TLV not found error
            ClassReaderCommanderParser.TLV_ParseFromReaderMultipleTLVs(arrayByteDataResponse, nPosStart, nDataLen, o_listTlvsParsed)

        Catch ex As Exception
            LogLn("[Err] TxnActivateParseResponseDoTLVs() = " + ex.Message)
        Finally
            Erase array3BytesTag
            Erase array2BytesTag
        End Try

    End Sub

    Private Sub TxnUpdateBalanceParseResponse_CaseOfOK(ByRef tagTxnActivateParseResponseInfo As TAG_TXN_PARSE_RESPONSE_INFO)
        Dim nTmp As Integer
        Try
            With tagTxnActivateParseResponseInfo
                ''------------------[ 1. Parse Multiple TLVs Data, tag: 57; E300 ]------------------
                If (.m_arrayByteDataResponse IsNot Nothing) Then
                    nTmp = .m_arrayByteDataResponse.Count - .m_nOffset
                    TxnActivateParseResponseDoTLVs(.m_arrayByteDataResponse, .m_nOffset, nTmp, .m_listTlvsTxnResult)
                End If
            End With
        Catch ex As Exception
            LogLn("[Err] TxnUpdateBalanceParseResponse() =" + ex.Message)
        End Try
    End Sub

    Private Sub TxnUpdateBalanceParseResponse_CaseOfFailure(ByRef tagTxnActivateParseResponseInfo As TAG_TXN_PARSE_RESPONSE_INFO)
        TxnActivateParseResponse_CaseOfFailure_Not_23h_OnlineAuth_Not_0Eh_UserInterfaceEvent(tagTxnActivateParseResponseInfo)
    End Sub


End Class
