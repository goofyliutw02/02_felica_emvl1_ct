﻿'==================================================================================
' File Name: ClassReaderProtocolBasic.vb
' Description: The ClassReaderProtocolBasic is as Must-be Inherited Base Class of Reader's Packet Parser & Composer.
' Who use it as Base Class: ClassReaderProtocolBasicIDGAR, ClassReaderProtocolBasicIDGAR, and ClassReaderProtocolBasicIDTECH
' Revision:
' -----------------------[ Initial Version ]-----------------------
' 2014 May 30, by goofy_liu@idtechproducts.com.tw

'----[Customized Protocol ]----
'01 -ClassRdrCommNoPrtcl, protocol type(PTLTP): Customized Protocol Handler,preambles + epilogue check.
'Now used for "Enzytek Golden SPS, AT Commands, <CR><LF>,<CR>"
'02 - ClassReaderProtocolIDGAR, PTLTP : IDG AR 3.0.0
'03 - ClassReaderProtocolIDGGR, PTLTP : IDG NEO 1.0.0/GR 2.0.0
'==================================================================================
'
'----------------------[ Importing Stuffs ]----------------------
Imports System.Runtime.Serialization
Imports System.Runtime.Serialization.Formatters.Binary
'Imports DiagTool_HardwareExclusive.ClassReaderProtocolBasicIDGAR
Imports System.IO
Imports System.IO.Ports
Imports System.Threading

'-------------------[ Class Declaration ]-------------------
Public MustInherit Class ClassReaderProtocolBasic
    '
    Inherits Object
    Implements IDisposable

    '
    '--------------------[All major stuffs placeholder]--------------------
    Protected m_refobjCTLM As ClassTableListMaster
    Protected m_refwinformMain As Form01_Main
    Protected m_bIsRetStatusOK As Boolean
    Protected m_bIDGCmdBusy As Boolean
    'Protected m_refdictTlvNewOpConfigurations As Dictionary(Of String, tagTLVNewOp)
    'Protected m_refdictTlvNewOpCAPK As Dictionary(Of String, tagTLVNewOpCAPK)
    Protected m_nIDGCommanderType As ClassReaderInfo.ENU_FW_PLATFORM_TYPE
    'Protected m_nIDGVivoProtcl As ClassReaderInfo.ENU_PROTCL_VER = ClassReaderInfo.ENU_PROTCL_VER._V2  '2017 Sept 21 added
    Protected Shared m_bDebug As Boolean = False
    '=============[ Public Shared Objects Section ]=====================
    Protected m_nDevIntf As ENU_DEV_INTERFACE

    'Public m_devUSBHID As ClassDevHID
    Public m_devRS232 As ClassDevRS232
    'USB HID Packets Style
    Public m_devUSBHID As ClassDevUSBHID
    Public m_refdevConn As ClassDev

    Public Shared s_strProtocolV2Header As String = "ViVOtech2"
    Public Shared s_strProtocolV3Header As String = "ViVOpayV3"
    Public Shared s_arrayByteV2Header As Byte() = New Byte() {&H56, &H69, &H56, &H4F, &H74, &H65, &H63, &H68, &H32}
    'Public Shared s_arrayByteV3Header As Byte() = New Byte() {&H56, &H69, &H56, &H4F, &H70, &H61, &H79, &H56, &H33}
    '======================[ Error Code , Message ]==========================
    '===========================[IDG AR Command Table]===========================
    Public Shared m_dictIDGCommands As SortedDictionary(Of UInt16, String) = New SortedDictionary(Of UInt16, String) From {
        {&H101, "Set Poll Mode"},
        {&H102, "Sys: LED Set"},
        {&H201, "Activate Transaction"},
        {&H240, "Activate Transaction - New"},
        {&H220, "Enhanced Activate Transaction"},
        {&H302, "Get Configuration - Global, Group 0"},
        {&H303, "Update Balance"},
        {&H305, "Get All AIDs"},
        {&H306, "Get Configurable Group"},
        {&H307, "Get All Groups"},
        {&H0, "0,Invalid Cmd"},
        {&H400, "Set Configuration - Global, Group 0"},
        {&H402, "Set AID"},
        {&H403, "Set Configurable Group"},
        {&H404, "Delete AID"},
        {&H409, "Reset to Default, V:S/N, V:DUKPT/Admin Keys"},
        {&H40F, "Reset to Default, V:S/N, X:DUKPT/Admin Keys"},
        {&H501, "Transaction Cancel"},
        {&H900, "Get All Reader Variables"},
        {&H901, "Get Product Type"},
        {&H902, "Get Processor Type"},
        {&H903, "Get Main Firmware Version"},
        {&H904, "Get Firmware Subsystem Suite"},
        {&H906, "Get Serial Protocol Suite"},
        {&H907, "Get Layer1 PayPass Version"},
        {&H908, "Get Layer1 Anti-collision Resolution Version"},
        {&H909, "RFU"},
        {&H90A, "Get Layer2 Card Application Suite"},
        {&H90B, "RFU"},
        {&H90C, "Get User Experience Suite"},
        {&H90E, "Get System Information Suite"},
        {&H90F, "Get Patch Version Number"},
        {&H911, "Get Miscellaneous and Proprietary Version"},
        {&H914, "Get Hardware Info"},
        {&H920, "Get Firmware Version (Major)"},
        {&HB01, "Buzzer Short Beep"},
        {&HB02, "Buzzer Long Beep"},
        {&H1201, "Sys: Get Serial Number"},
        {&H1202, "Sys: Set Serial Number"},
        {&H1801, "Sys: Ping"},
        {&H2505, "RTC Set - PCI, SRED"},
        {&H2506, "RTC Get - PCI, SRED"},
        {&H2900, "Get Version Protocol 2"},
        {&H2904, "Get Boot Loader Version"},
        {&H2C01, "Sys: Set Passthrough Mode"},
        {&H2C02, "CL Poll For Token"},
        {&H2C0B, "Enhanced Passthrough Mode"},
        {&H2C11, "Enhancement Pass ThrougMode Set"},
        {&H2C12, "CT Get ATR"},
        {&H2C18, "CT Card Power Off"},
        {&H6001, "CT Retrieve Application Data"},
        {&H6002, "CT Remove Application Data"},
        {&H6003, "CT Set Application Data"},
        {&H6004, "CT Retrieve Terminal Data"},
        {&H6005, "CT Remove Terminal Data"},
        {&H6006, "CT Set Terminal Data"},
        {&H6007, "CT Retrieve AID List"},
        {&H6008, "CT Retrieve CA Public Key"},
        {&H6014, "CT Get Reader Status"},
        {&H8102, "DUKPT Check All Keys"},
        {&H8104, "DUKPT Check Indicated Key"},
        {&H810A, "DUKPT Get Key Serial Number (KSN)"},
        {&H8300, "DUKPT: Erase All Keys"},
        {&H8303, "Sys: LCD Text Display"},
        {&H830D, "Sys: LCD Clear"},
        {&H8306, "Sys: Get UI Event Input"},
        {&H8308, "Sys: LCD Custom Mode Start"},
        {&H8309, "Sys: LCD Custom Mode Stop"},
        {&H830C, "Sys: UI - Event Queue Cleanup"},
        {&H830E, "Sys: LCD - Image Display"},
        {&H831F, "Sys: Delete Specific File"},
        {&H8322, "Sys: List Files Top Dir Only or Recursively From Top Dir"},
        {&H8324, "Sys: Download File"},
        {&H840F, "MC3  - Torn  Transaction Log Clean"},
        {&H840E, "MC3 - Torn Transaction Log Reset"},
        {&H9113, "UMFG - Peripheral Control, NEO2.0"},
        {&HC73D, "Read Secured Log Data, +TAMPER +UMFG +SMFG +PKI +SMSG, NEO2.0"},
        {&HA02, "LED Control"},
        {&HC716, "Bootloader-->Application"},
        {&HC741, "Applicaton-->Bootloader"},
        {&HD003, "Set CA Public Key"},
        {&HD005, "Delete All CA Public Keys"},
        {&HF000, "PMS Timeout(TO) Configure - LLS TO + Power Off TO"},
        {&HF0F4, "Configure Buttons"},
        {&HF0F5, "Get Buttons Configuration"},
        {&HF0F6, "Blue LED in Sequence - ON"},
        {&HF0F7, "Blue LED in Sequence - OFF"},
        {&HF0F9, "LCD Display Clear"},
        {&HF0FC, "LCD Display Line 1 Message"},
        {&HF0FD, "LCD Display Line 2 Message"},
        {&HF0FB, "MSR (Yellow) LED - ON"},
        {&HF0FA, "MSR (Yellow)  LED - OFF"},
        {&HF0FE, "Buzzer Beep Once Short"}
        }
    '

    Public Shared Function GetCommandDescription(ByVal u16Cmd As UInt16) As String
        If (m_dictIDGCommands IsNot Nothing) Then
            If (m_dictIDGCommands.ContainsKey(u16Cmd)) Then
                Return m_dictIDGCommands.Item(u16Cmd)
            End If
            Dim strMsg As String = String.Format("Unknown IDG GR Cmd = {0,4:X4}", u16Cmd)
            Return strMsg
        Else
            Return "Cmd Description Table Not Ready, Please New ClassReaderCommanderIDAR Instance First"
        End If
    End Function
    '
    '----------[ Check out "Table 2: GR 2.0.0, Protocol 1 Status Codes" ]------------
    Enum EnumProtocolV1FrameType As Byte
        '------[ Command Type]-------------
        _C = &H43 '"C"
        '------[ Response Type]-------------
        _A = &H41 '"A"
        _N = &H4E '"N"
        _S = &H53 '"S"
        _D = &H44 'D'
        '
        _X = &H0 'Unknown
    End Enum
    Public Shared m_dictProtocolV1FrameTypeResponse As Dictionary(Of Byte, String) = New Dictionary(Of Byte, String) From {
        {EnumProtocolV1FrameType._C, "Command"},
        {EnumProtocolV1FrameType._A, "ACK"},
        {EnumProtocolV1FrameType._N, "NAK"},
        {EnumProtocolV1FrameType._S, "Special"},
        {EnumProtocolV1FrameType._D, "Data Frames"}
        }
    'Skip {EnumProtocolV1FrameType._X, "Unknown"}
    '{EnumProtocolV1FrameType._C ,"Command"},

    'Note. Data Frame (Frame Type = "D") length is varied from 1~244
    'Public Shared m_dictProtocolV1FrameTypeResponseLen As Dictionary(Of Byte, Integer) = New Dictionary(Of Byte, Integer) From {
    '    {EnumProtocolV1FrameType._C, 2},
    '    {EnumProtocolV1FrameType._A, 2},
    '    {EnumProtocolV1FrameType._N, 2},
    '    {EnumProtocolV1FrameType._S, 4}
    '    }

    '----------[ Check out "Table 2: GR 2.0.0, Protocol 1 Status Codes" ]------------
    Enum EnumSTATUSCODE1 As Byte
        x00OK = &H0
        x01IncorrectFrameTag
        x02IncorrectFrameType
        x03UnknownFrameType
        x04UnknownCommand
        x05UnknownSubCommand
        x06CRCError
        x07Failed
        x08Timeout
        '
        x0AIncorrectParameter = &HA
        x0BCommandNotSupported
        x0CSubCommandNotSupported
        x0DParameterNotSupported_StatusAbortCommand
        x0ECommandNotAllowed
        x0FSubCommandNotAllowed
    End Enum

    '----------[ Check out "Table 8: Protocol 2 Status Codes" ]------------
    Enum EnumSTATUSCODE2 As Byte
        x00OK = &H0
        x01IncorrectHeaderTag
        x02UnknownCommand
        x03UnknownSubCommand
        x04CRCErrorInFrame
        x05IncorrectParameter
        x06ParameterNotSupported
        x07MalformattedData
        x08Timeout
        '
        x0AFailedNACK = &HA
        x0BCommandNotAllowed
        x0CSubCommandNotAllowed
        x0DBufferOverflow
        x0EUserInterfaceEvent
        '
        x11CommTypeNotSupported = &H11 'VT-1, burst, etc.
        x12SecureInterfaceIsNotFunctionalOrIsInAnIntermediateState
        x13DataFieldIsNotMod8
        x14Pad0x80NotFoundWhereExpected
        x15SpecifiedKeyTypeIsInvalid
        x16SecureComm_Init_CouldNotRetrieveKeyFromTheSAM
        x17HashCodeProblem
        x18SecureComm_InstallKey_CouldNotStoreTheKeyIntoTheSAM
        x19FrameIsTooLarge
        x1ASecureComm_Init_UnitPoweredUpInAuthenticationStateButPOSMustResent
        x1BTheEEPROMMayNotBeInitializedBecauseSecCommInterfaceDoesNotMakeSense
        x1CProblemEncodingAPDU
        '
        x20ILM_UnsupportedIndex = &H20
        x21ILM_UnexpectedSequenceCounterInMultipleFramesForSingleBitmap
        x22ILM_ImproperBitMap
        x23RequestOnlineAuthorization
        x24ViVOCard3RawDataReadSuccessful
        x25ILM_MessageIndexNotAvailable_ViVOcommActivateTransactionCardType
        x26ILM_VersionInformationMismatch
        x27ILM_NotSendingCommandsInCorrectIndexMessageIndex
        x28ILM_TImeoutOrNextExpectedMessageNotReceived
        x29ILM_ILMLanguagesNotAvailableForViewing
        x2AILM_OtherLanguageNotSupported
        x2FSecuIntfKeyCertNotActivate = &H2F
        '
        x45_NEO20_9002_EnableTamperFailed_NoSerialNumber = &H45
        '
        x50AutoSwitchOK = &H50
        x51AutoSwitchFailed
    End Enum

    '----------[ Check out "Table 9: Error Codes" ]------------
    Enum EnumTXN_ERRCODE As Byte
        x00NoError = &H0
        x01OutOfSequence
        x02GoToContactInterface
        x03TransactionAmountIsZero
        '
        x20CardReturnedErrorStatus = &H20
        x21CollisionError
        x22AmountOverMaximumLimit
        x23RequestOnlineAuthorization
        x25CardBlocked
        x26CardExpired
        x27UnsupportedCard
        '
        x30CardDidNotRespond = &H30
        '
        x42CardGeneratedAAC = &H42
        x43CardGeneratedARQC
        x44SDAorDDAFailedSinceNotSupportedByCard
        '
        x50SDAorDDAorCDDAFailed_CAPublicKey = &H50
        x51SDAorDDAorCDDAFailed_IssuerPublicKey
        x52SDAFailed_SSAD
        x53DDAorCDDAFailed_ICCPublicKey
        x54DDAorCDDAFailed_DynamicSignatureVerification
        x55ProcessionRestrictionsFailed
        x56TerminalRiskManagementFailed_TRM
        x57CardholderVerificationFailed
        x58TerminalActionAnalysisFailed_TAA
        '
        x61SDMemoryError = &H61
    End Enum



    Enum EnumCAPKCODE As Byte
        '20h - SAM Transceiver error – problem communicating with the SAM (see note below)
        x20SAMTransceiverError = &H20
        '21h - Length error in data returned from the SAM
        x21LengthErrorInDataReturnedFromTheSAM
        '41h - Unknown Error from SAM
        x41UnknownErrorFromSAM = &H41
        '42h - Invalid data detected by SAM
        x42InvalidDataDetectedBySAM
        '43h - Incomplete data detected by SAM
        x43IncompleteDataDetectedBySAM
        '44h - Reserved
        x44Reserved
        '45h - Invalid key hash algorithm
        x45InvalidKeyHashAlgorithm
        '46h - Invalid key encryption algorithm
        x46InvalidKeyEncryptionAlgorithm
        '47h - Invalid modulus length
        x47InvalidModulusLength
        '48h - Invalid exponent
        x48InvalidExponent
        '49h - Key already exists
        x49KeyAlreadyExists
        '4Ah - No space for new RID
        x4ANoSpaceForNewRID
        '4Bh - Key not found
        x4BKeyNotFound
        '4Ch - Crypto not responding
        x4CCryptoNotResponding
        '4Dh - Crypto communication error
        x4DCryptoCommunicationError
        '4Eh - Key slots full for this RID (maximum number of keys for an RID has been installed)
        x4EhKeySlotsFullForThisRID
        '4Fh - All key slots are full (maximum number of keys has been installed)
        x4FhAllKeySlotsAreFull
    End Enum

    'Public Overrides Sub SystemSetEnhancePassthroughMode(u32FlagsB0Transaction As ENUM_SEPM_B2B5_FLAGS_B0_TRANSACTION, u32FlagsB2SingleShot As ENUM_SEPM_B2B5_FLAGS_B2_SINGLE_SHOT, byteICCType As ENUM_SEPM_B6_ICC_TYPE, byteBeepIndicator As ENUM_SEPM_B7_BEEP_INDICATOR, byteLEDControl As ENUM_SEPM_LED_ONOFF, Optional ByVal nTimeout As Integer = ClassDevRS232.m_cnTX_TIMEOUT_IN_SEC)
    Enum ENUM_EPT_SINGLE_SHOT_MODE As Integer
        x01_ACTIVATE_INTERFACE = &H1
        x02_DEACTIVATE_INTERFACE = &H2
        x04_ISSUE_POLL_FOR_TOKEN = &H4
        x08_USE_INDEPENDENT_LED_INSTEAD = &H8
        x10_USE_INDEPENDENT_BUZZER_INSTEAD = &H10
        x20_USE_SPECIFIED_INTERFACE = &H20
        x40_RFU = &H40
        x80_RFU = &H80
    End Enum
    Enum ENUM_EPT_LCD_MSG_IDX As Integer
        x00_IDLE_MSG_WELCOME = &H0
        x01_PLEASE_PRESENT_CARD
        x02_TIME_OUT_OR_TXN_CANCEL
        x03_TXN_PROCESSING_IN_READER_AND_CARD
        x04_TXN_PASS_THANK_YOU
        x05_TXN_FAILED
        x06_AMOUNT_0DotZero_TAP_CARD
        x07_BALANCE_OR_OFFLINE_FUNDS_0DotZero
        x08_TERM_INSERT_OR_SWIPE_CARD_USE_CHIP_PIN
        x09_TERM_TRY_AGAIN_TAP
        x0A_TERM_PRESENT_ONE_CARD_ONLY
        x0B_TERM_WAIT_AUTHORIZE
        x80_MASK = &H80
        xFE_CUSTOM_USE_MSG = &HFE
        xFF_LED_BUZZER_ONLY_WITHOUT_LCD = &HFF
    End Enum
    Public Enum ENUM_SEPM_LED_NUM
        x00_L0 = &H0
        x01_L1 = &H1
        x02_L2 = &H2
        x03_L3 = &H3
        xFF_LALL = &HFF
    End Enum
    Public Enum ENUM_SEPM_LED_ONOFF
        x00_LED_ON = &H0
        x01_LED_OFF = &H1
    End Enum
    Public Enum ENUM_SEPM_INTERFACE
        x00_CL = &H0
        x20_RFU = &H20
        x21_SAM1_ONLY_VP5500 = &H21
        x22_SAM2_ONLY_VP5500 = &H22
    End Enum
    '----------[ IDG Command Offset Definition ]------------
    Structure _TAG_VIVO_PROCTL
        Public Shared Protocol2_OffSet_Header_Len_10B As Integer = 10 'Length of V2 Header
        Public Shared Protocol2_OffSet_Header_NullByte_Offset As Integer = 9
        Public Shared Protocol2_OffSet_CmdMain_Len_1B As Integer = 10
        Public Shared Protocol2_OffSet_CmdSub_Len_1B As Integer = 11
        Public Shared Protocol2_OffSet_DataLen_MSB_Len_1B As Integer = 12
        Public Shared Protocol2_OffSet_DataLen_LSB_Len_1B As Integer = 13
        Public Shared Protocol2_OffSet_Rx_DataLen_MSB_Len_1B = 12
        Public Shared Protocol2_OffSet_Rx_DataLen_LSB_Len_1B = 13
        Public Shared Protocol2_OffSet_RetStatusCode_LSB_Len_1B As Integer = 11
    End Structure
    '
    Enum EnumIDGCmdOffset As Integer
        'Tx: "ViVOPayV3\0"+"Cmd,Sub,2 Bytes"+"Len, 2 Bs"+"Data, if Len>0"+"CRC,2 Bytes"
        'Rx: "ViVOPayV3\0"+"Cmd,Sub,2 Bytes"+"Sts Code, 1B"+"Len, 2 Bs"+"Data, if Len>0"+"CRC,2 Bytes"
        Protocol3_OffSet_Header_Len_10B = 10 'Length of V3 Header
        Protocol3_OffSet_Header_NullByte_Offset = 9
        Protocol3_OffSet_CmdMain_Len_1B = 10
        Protocol3_OffSet_CmdSub_Len_1B = 11
        Protocol3_OffSet_DataLen_MSB_Len_1B = 12
        Protocol3_OffSet_DataLen_LSB_Len_1B = 13
        Protocol3_OffSet_Rx_DataLen_MSB_Len_1B = 13
        Protocol3_OffSet_Rx_DataLen_LSB_Len_1B = 14
        Protocol3_OffSet_Rx_RetStatusCode_LSB_Len_1B = 12

        'Tx: "ViVOtech2\0"+"Cmd,Sub,2 Bytes"+"Len, 2 Bs"+"Data, if Len>0"+"CRC,2 Bytes"
        'Rx: "ViVOtech2\0"+"Cmd,Sub,2 Bytes"+"Sts Code, 1B"+"Len, 2 Bs"+"Data, if Len>0"+"CRC,2 Bytes"
        Protocol2_OffSet_Header_Len_10B = 10 'Length of V2 Header
        Protocol2_OffSet_Header_NullByte_Offset = 9
        Protocol2_OffSet_CmdMain_Len_1B = 10
        Protocol2_OffSet_CmdSub_Len_1B = 11
        Protocol2_OffSet_DataLen_MSB_Len_1B = 12
        Protocol2_OffSet_DataLen_LSB_Len_1B = 13
        'Since in Protocol V3 Resp Format, add sub-cmd byte before Status Code byte.
        'V2 Resp Format: <Cmd><Sts Code><Len MSB><Len LSB><Data, if Len >0><CRC,2 Bs>
        'V3 Resp Format: <Cmd><*Sub-Cmd*><Sts Code><Len MSB><Len LSB><Data, if Len >0><CRC,2 Bs>
        Protocol2_OffSet_Rx_DataLen_MSB_Len_1B = 12
        Protocol2_OffSet_Rx_DataLen_LSB_Len_1B = 13
        Protocol2_OffSet_Rx_RetStatusCode_LSB_Len_1B = 11
        '
        Protocol1_OffSet_Header_Len_9B = 0
        Protocol1_OffSet_FrameType_Byte9_1B = 9
        Protocol1_OffSet_CmdMain_Len_1B = 10
        Protocol1_OffSet_CmdSub_Len_1B = 11
        Protocol1_OffSet_Cmd_Data1_1B = 12
        Protocol1_OffSet_Cmd_Data2_1B = 13
        Protocol1_OffSet_TypeDS_Data_Byte10_Len_1B = 10
        '
        Protocol1_OffSet_DataLen_Byte13_1B = 13
        '
        Protocol1_OffSet_RetStatusCode_Byte11_1B = 11


    End Enum
    '----------[ Check out "Table 10: RF State Codes" ]------------
    Enum EnumSTATECODE_RF As Byte
        x00None_RFStateCodeNotAvailable = &H0
        x01PPSE_ErrorOccurredDuringPPSECommand
        x02SELECT_ErrorOccurredDuringSELECTCommand
        x03GPO_ErrorOccurredDuringGETPROCESSINGOPTIONSCommand
        x04READRECORD_ErrorOccurredDuringREADRECORDCommand
        x05GENAC_ErrorOccurredDuringGENACCommand
        x06CCC_ErrorOccurredDuringCCCCommand
        x07IA_ErrorOccurredDuringIACommand
        x08SDA_ErrorOccurredDuringSDAProcessing
        x09DDA_ErrorOccurredDuringDDAProcessing
        x0ACDA_ErrorOccurredDuringCDAProcessing
        x0BTAA_ErrorOccurredDuringTAAProcessing
        x0CUPDATERECORD_ErrorOccurredDuringUPDATERECORDCommand
        '
        x10GETDATATicket_ErrorOccurredDuringGETDATACommandToRetrieveTheTicket = &H10
        x11GETDATATicketingProf_ErrorOccurredDuringGETDATACommandToRetrieveTheTicketingProfile
        x12GETDATABalance_ErrorOccurredDuringGETDATACommandToRetrieveTheBalance
        x13GETDATAAll_ErrorOccurredDuringGETDATACommandToRetrieveAllData
        '
        x20PUTDATATicket_ErrorOccurredDuringPUTDATACommandToRetrieveTheTicket = &H20
    End Enum
    '
    '
    Public Enum ENUM_BUAD_RATE As Byte
        _x9600 = &H1
        _x19200
        _x38400
        _x57600
        _x115200
    End Enum

    Public Enum ENUM_SEPM_B6_ICC_TYPE
        TYPE_x00_NEO20_LAST_ATR = &H0
        TYPE_x20_ICC = &H20
        TYPE_x21_SAM1 ' = &H21
        TYPE_x22_SAM2 '= &H22
        TYPE_x23_SAM3 ' = &H23
        TYPE_x24_SAM4 ' = &H24
    End Enum
    '====================================================================================================================================
    Public Shared m_dictStatusCodeCAPKRIDKeyIndex As Dictionary(Of Byte, String) = New Dictionary(Of Byte, String) From {
        {EnumCAPKCODE.x20SAMTransceiverError, "SAM Transceiver Error"}, {EnumCAPKCODE.x21LengthErrorInDataReturnedFromTheSAM, "Length Error In Data Returned From TheSAM"}, {EnumCAPKCODE.x41UnknownErrorFromSAM, "Unknown Error From SAM"}, {EnumCAPKCODE.x42InvalidDataDetectedBySAM, "Invalid Data Detected By SAM"}, {EnumCAPKCODE.x43IncompleteDataDetectedBySAM, "Incomplete Data Detected By SAM"}, {EnumCAPKCODE.x44Reserved, "Reserved"}, {EnumCAPKCODE.x45InvalidKeyHashAlgorithm, "Invalid Key Hash Algorithm"}, {EnumCAPKCODE.x46InvalidKeyEncryptionAlgorithm, "Invalid Key Encryption Algorithm"}, {EnumCAPKCODE.x47InvalidModulusLength, "Invalid Modulus Length"}, {EnumCAPKCODE.x48InvalidExponent, "Invalid Exponent"}, {EnumCAPKCODE.x49KeyAlreadyExists, "Key Already Exists"}, {EnumCAPKCODE.x4ANoSpaceForNewRID, "No Space For New RID"}, {EnumCAPKCODE.x4BKeyNotFound, "Key Not Found"}, {EnumCAPKCODE.x4CCryptoNotResponding, "Crypto Not Responding"}, {EnumCAPKCODE.x4DCryptoCommunicationError, "Crypto Communication Error"}, {EnumCAPKCODE.x4EhKeySlotsFullForThisRID, "Key Slots Full For This RID"}, {EnumCAPKCODE.x4FhAllKeySlotsAreFull, "All Key Slots Are Full"}}
    ',
    ',
    '     ,
    '     , {CAPKCODE.x4ANoSpaceForNewRID, "No Space For New RID"}, {CAPKCODE.x4BKeyNotFound, "Key Not Found"}, 
    '}
    '
    '====================================================================================================================================
    'Public Shared m_dictFrameType As Dictionary(Of Char, String) = New Dictionary(Of Char, String) From {
    '    {EnumCAPKCODE.x20SAMTransceiverError, "SAM Transceiver Error"}, {EnumCAPKCODE.x21LengthErrorInDataReturnedFromTheSAM, "Length Error In Data Returned From TheSAM"}, {EnumCAPKCODE.x41UnknownErrorFromSAM, "Unknown Error From SAM"}, {EnumCAPKCODE.x42InvalidDataDetectedBySAM, "Invalid Data Detected By SAM"}, {EnumCAPKCODE.x43IncompleteDataDetectedBySAM, "Incomplete Data Detected By SAM"}, {EnumCAPKCODE.x44Reserved, "Reserved"}, {EnumCAPKCODE.x45InvalidKeyHashAlgorithm, "Invalid Key Hash Algorithm"}, {EnumCAPKCODE.x46InvalidKeyEncryptionAlgorithm, "Invalid Key Encryption Algorithm"}, {EnumCAPKCODE.x47InvalidModulusLength, "Invalid Modulus Length"}, {EnumCAPKCODE.x48InvalidExponent, "Invalid Exponent"}, {EnumCAPKCODE.x49KeyAlreadyExists, "Key Already Exists"}, {EnumCAPKCODE.x4ANoSpaceForNewRID, "No Space For New RID"}, {EnumCAPKCODE.x4BKeyNotFound, "Key Not Found"}, {EnumCAPKCODE.x4CCryptoNotResponding, "Crypto Not Responding"}, {EnumCAPKCODE.x4DCryptoCommunicationError, "Crypto Communication Error"}, {EnumCAPKCODE.x4EhKeySlotsFullForThisRID, "Key Slots Full For This RID"}, {EnumCAPKCODE.x4FhAllKeySlotsAreFull, "All Key Slots Are Full"}}
    '====================================================================================================================================
    Public Shared m_dictStatusCode1 As Dictionary(Of Byte, String) = New Dictionary(Of Byte, String) From {
        {EnumSTATUSCODE1.x00OK, "OK"}, {EnumSTATUSCODE1.x01IncorrectFrameTag, "Incorrect Frame Tag"},
 {EnumSTATUSCODE1.x02IncorrectFrameType, "Incorrect Frame Type"}, {EnumSTATUSCODE1.x03UnknownFrameType, "Unknown Frame Type"}, {EnumSTATUSCODE1.x04UnknownCommand, "Unknown Command"}, {EnumSTATUSCODE1.x05UnknownSubCommand, "Unknown Sub Command"},
          {EnumSTATUSCODE1.x06CRCError, "CRC Error"}, {EnumSTATUSCODE1.x07Failed, "Failed"}, {EnumSTATUSCODE1.x08Timeout, "Time Out"}, {EnumSTATUSCODE1.x0AIncorrectParameter, "Incorrect Parameter"},
          {EnumSTATUSCODE1.x0BCommandNotSupported, "Command Not Supported"}, {EnumSTATUSCODE1.x0CSubCommandNotSupported, "Sub Command Not Supported"}, {EnumSTATUSCODE1.x0DParameterNotSupported_StatusAbortCommand, "Parameter Not Supported / Status Abort Command"},
                  {EnumSTATUSCODE1.x0ECommandNotAllowed, "Command Not Allowed"}, {EnumSTATUSCODE1.x0FSubCommandNotAllowed, "Sub Command Not Allowed"}}
    '====================================================================================================================================
    Public Shared m_dictStatusCode2 As Dictionary(Of Byte, String) = New Dictionary(Of Byte, String) From {
    {EnumSTATUSCODE2.x00OK, "OK"}, {EnumSTATUSCODE2.x01IncorrectHeaderTag, "Incorrect Header Tag"}, {EnumSTATUSCODE2.x02UnknownCommand, "Unknown Command"},
    {EnumSTATUSCODE2.x03UnknownSubCommand, "Unknown Sub Command"}, {EnumSTATUSCODE2.x04CRCErrorInFrame, "CRC Error In Frame"},
    {EnumSTATUSCODE2.x05IncorrectParameter, "Incorrect Parameter"}, {EnumSTATUSCODE2.x06ParameterNotSupported, "Parameter Not Supported"}, {EnumSTATUSCODE2.x07MalformattedData, "Mal-formatted Data"},
    {EnumSTATUSCODE2.x08Timeout, "Time Out"}, {EnumSTATUSCODE2.x0AFailedNACK, "Failed / NACK"}, {EnumSTATUSCODE2.x0BCommandNotAllowed, "Command Not Allowed"},
    {EnumSTATUSCODE2.x0CSubCommandNotAllowed, "Sub-Command Not Allowed"}, {EnumSTATUSCODE2.x0DBufferOverflow, "Buffer Overflow (Data Length too large for reader buffer"}, {EnumSTATUSCODE2.x0EUserInterfaceEvent, "User Interface Event"},
    {EnumSTATUSCODE2.x11CommTypeNotSupported, "Comm Type Not Supported"}, {EnumSTATUSCODE2.x12SecureInterfaceIsNotFunctionalOrIsInAnIntermediateState, "Secure Interface Is Not Functional  Or Is In An Intermediate State"}, {EnumSTATUSCODE2.x13DataFieldIsNotMod8, "Data Field Is Not Mod 8"},
    {EnumSTATUSCODE2.x14Pad0x80NotFoundWhereExpected, "Pad 0x80 Not Found Where Expected"}, {EnumSTATUSCODE2.x15SpecifiedKeyTypeIsInvalid, "Specified Key Type Is Invalid"}, {EnumSTATUSCODE2.x16SecureComm_Init_CouldNotRetrieveKeyFromTheSAM, "SecureComm:Init =Could Not Retrieve Key From The SAM"},
    {EnumSTATUSCODE2.x17HashCodeProblem, "Hash Code Problem"}, {EnumSTATUSCODE2.x18SecureComm_InstallKey_CouldNotStoreTheKeyIntoTheSAM, "SecureComm:InstallKey = Could Not Store The Key Into The SAM"}, {EnumSTATUSCODE2.x19FrameIsTooLarge, "FrameIs Too Large"},
    {EnumSTATUSCODE2.x1ASecureComm_Init_UnitPoweredUpInAuthenticationStateButPOSMustResent, "SecureComm:Init = Unit Powered Up In Authentication State But POS Must Resent "}, {EnumSTATUSCODE2.x1BTheEEPROMMayNotBeInitializedBecauseSecCommInterfaceDoesNotMakeSense, "The EEPROM May Not Be Initialized Because SecComm Interface Does Not Make Sense"}, {EnumSTATUSCODE2.x1CProblemEncodingAPDU, "Problem Encoding APDU"},
    {EnumSTATUSCODE2.x20ILM_UnsupportedIndex, "ILM : Unsupported Index"}, {EnumSTATUSCODE2.x21ILM_UnexpectedSequenceCounterInMultipleFramesForSingleBitmap, "ILM : Unexpected Sequence Counter In Multiple Frames For Single Bitmap"}, {EnumSTATUSCODE2.x22ILM_ImproperBitMap, "ILM : Improper Bit Map"},
    {EnumSTATUSCODE2.x23RequestOnlineAuthorization, "Request On-line Authorization"}, {EnumSTATUSCODE2.x24ViVOCard3RawDataReadSuccessful, "ViVOCard3 Raw Data Read Successful"}, {EnumSTATUSCODE2.x25ILM_MessageIndexNotAvailable_ViVOcommActivateTransactionCardType, "ILM : Message Index Not Available or ViVOcomm Activate Transaction Card Type"},
    {EnumSTATUSCODE2.x26ILM_VersionInformationMismatch, "ILM : Version Information Mismatch"}, {EnumSTATUSCODE2.x27ILM_NotSendingCommandsInCorrectIndexMessageIndex, "ILM : Not Sending Commands In Correct Index Message Index "}, {EnumSTATUSCODE2.x28ILM_TImeoutOrNextExpectedMessageNotReceived, "ILM : TImeout Or Next Expected Message Not Received"},
    {EnumSTATUSCODE2.x29ILM_ILMLanguagesNotAvailableForViewing, "ILM : ILM Languages Not Available For Viewing"}, {EnumSTATUSCODE2.x2AILM_OtherLanguageNotSupported, "ILM : Other Language Not Supported"}, {EnumSTATUSCODE2.x50AutoSwitchOK, "AutoSwitch = OK"},
    {EnumSTATUSCODE2.x51AutoSwitchFailed, "Auto Switch Failed"}
    }

    ReadOnly Property bIsUSBCablePlugged(Optional ByVal bRenewPort As Boolean = False) As Boolean
        Get
            Return m_devUSBHID.bIsUSBCablePlugged(bRenewPort)
        End Get
    End Property

    '================================================

    Public Structure TAG_GET_INPUT_EVETN
        Public Enum EVT_TYPE As UInt32
            _BUTTON = &H30000UI
            _CHECK_BOX = &H30001UI
            _LINE_ITEM = &H30002UI
            _KEYPAD = &H30003UI
            _TOUCH_SCREEN = &H30004UI
            _SLIDESHOW = &H30005UI
            _TRANSACTION = &H30006UI
            _BUTTON_RADIO = &H30007UI
            _UI_MODULE = &H30008UI
            '
            _UNKNOWN = &HFFFFFFUI
        End Enum

        Public Enum EVT_KEY_ID As UInt16
            KEYPAD_KEY_0 = &H30
            KEYPAD_KEY_1 '0031 h
            KEYPAD_KEY_2 '0032 h
            KEYPAD_KEY_3 '0033 h
            KEYPAD_KEY_4 '0034 h
            KEYPAD_KEY_5 '0035 h
            KEYPAD_KEY_6 '0036 h
            KEYPAD_KEY_7 '0037 h
            KEYPAD_KEY_8 '0038 h
            KEYPAD_KEY_9 '0039 h
            KEYPAD_KEY_ENTER = &HD
            KEYPAD_KEY_CLEAR_BACKSPACE = &H8
            KEYPAD_KEY_CANCEL = &H1B
            ' Vend III
            FUNC_KEY_LEFT_F1 = &H70
            FUNC_KEY_MIDDLE_F2 = &H71
            FUNC_KEY_RIGHT_F3 = &H72
            FUNC_KEY_DOWN_F4 = &H73
            'PISCES
            FUNC_KEY_STAR = &H2A '*
            FUNC_KEY_SHARP = &H23 '#
        End Enum
        '
        Public m_in_byteTimeout As Byte
        '
        Public m_out_EvtType As EVT_TYPE
        Public m_out_GraphicID As UInt32
        Public m_out_arrayByteEvtInfo As Byte()
        Public m_out_byteState As Byte
        Public m_out_u16KeyID As UInt16
        Public Shared m_dictKeyInfoMsg As SortedDictionary(Of TAG_GET_INPUT_EVETN.EVT_KEY_ID, String) = New SortedDictionary(Of TAG_GET_INPUT_EVETN.EVT_KEY_ID, String) From {
            {TAG_GET_INPUT_EVETN.EVT_KEY_ID.KEYPAD_KEY_0, "Key 0"},
            {TAG_GET_INPUT_EVETN.EVT_KEY_ID.KEYPAD_KEY_1, "Key 1"},
            {TAG_GET_INPUT_EVETN.EVT_KEY_ID.KEYPAD_KEY_2, "Key 2"},
            {TAG_GET_INPUT_EVETN.EVT_KEY_ID.KEYPAD_KEY_3, "Key 3"},
            {TAG_GET_INPUT_EVETN.EVT_KEY_ID.KEYPAD_KEY_4, "Key 4"},
            {TAG_GET_INPUT_EVETN.EVT_KEY_ID.KEYPAD_KEY_5, "Key 5"},
            {TAG_GET_INPUT_EVETN.EVT_KEY_ID.KEYPAD_KEY_6, "Key 6"},
            {TAG_GET_INPUT_EVETN.EVT_KEY_ID.KEYPAD_KEY_7, "Key 7"},
            {TAG_GET_INPUT_EVETN.EVT_KEY_ID.KEYPAD_KEY_8, "Key 8"},
            {TAG_GET_INPUT_EVETN.EVT_KEY_ID.KEYPAD_KEY_9, "Key 9"},
            {TAG_GET_INPUT_EVETN.EVT_KEY_ID.KEYPAD_KEY_ENTER, "Key Enter"},
            {TAG_GET_INPUT_EVETN.EVT_KEY_ID.KEYPAD_KEY_CLEAR_BACKSPACE, "Key Clear"},
            {TAG_GET_INPUT_EVETN.EVT_KEY_ID.KEYPAD_KEY_CANCEL, "Key Cancel"},
            {TAG_GET_INPUT_EVETN.EVT_KEY_ID.FUNC_KEY_LEFT_F1, "F1_Left"},
            {TAG_GET_INPUT_EVETN.EVT_KEY_ID.FUNC_KEY_MIDDLE_F2, "F2_Middle"},
            {TAG_GET_INPUT_EVETN.EVT_KEY_ID.FUNC_KEY_RIGHT_F3, "F3_Right"},
            {TAG_GET_INPUT_EVETN.EVT_KEY_ID.FUNC_KEY_DOWN_F4, "F4_Down"}
            }
        '
        Private m_strBtn_Txt As String 'Save GUI Button Event Type - Button Text 
        Public ReadOnly Property GetEvtBtnText As String
            Get
                Return m_strBtn_Txt
            End Get
        End Property

        Public Shared Function GetKeypadIDMsg(KeyPadID As TAG_GET_INPUT_EVETN.EVT_TYPE) As String
            If (m_dictKeyInfoMsg.ContainsKey(KeyPadID)) Then
                Return m_dictKeyInfoMsg.Item(KeyPadID)
            End If

            Return "Key Pad ID - Unknown"
        End Function

        Public Shared m_dictUIEventInfoMsg As SortedDictionary(Of TAG_GET_INPUT_EVETN.EVT_TYPE, String) = New SortedDictionary(Of EVT_TYPE, String) From {
            {TAG_GET_INPUT_EVETN.EVT_TYPE._BUTTON, "UI Type - Button"},
            {TAG_GET_INPUT_EVETN.EVT_TYPE._BUTTON_RADIO, "UI Type - Radio Button"},
            {TAG_GET_INPUT_EVETN.EVT_TYPE._CHECK_BOX, "UI Type - Check Box"},
            {TAG_GET_INPUT_EVETN.EVT_TYPE._KEYPAD, "UI Type - Key Pad"},
            {TAG_GET_INPUT_EVETN.EVT_TYPE._LINE_ITEM, "UI Type - Line Item"},
            {TAG_GET_INPUT_EVETN.EVT_TYPE._SLIDESHOW, "UI Type - SlideShow"},
            {TAG_GET_INPUT_EVETN.EVT_TYPE._TOUCH_SCREEN, "UI Type - Touch Screen"},
            {TAG_GET_INPUT_EVETN.EVT_TYPE._TRANSACTION, "UI Type - Transaction"},
            {TAG_GET_INPUT_EVETN.EVT_TYPE._UI_MODULE, "UI Module Evt"}
        }
        Public Shared Function GetUIMsg(UIID As TAG_GET_INPUT_EVETN.EVT_TYPE) As String
            If (m_dictUIEventInfoMsg.ContainsKey(UIID)) Then
                Return m_dictUIEventInfoMsg.Item(UIID)
            End If

            Return "UI Type - Unknown"
        End Function

        'Public m_dictEventInfo As SortedDictionary(Of UInt16, Byte())

        ReadOnly Property bIsGUIButtonType As Boolean
            Get
                Return (m_out_EvtType = EVT_TYPE._BUTTON)
            End Get
        End Property

        ReadOnly Property bIsKeyPadType As Boolean
            Get
                Return (m_out_EvtType = EVT_TYPE._KEYPAD)
            End Get
        End Property

        ReadOnly Property bIsKeyPadKey(ByVal key As EVT_KEY_ID) As Boolean
            Get
                Return (m_out_u16KeyID = key)
            End Get
        End Property
        '

        Public Sub Init(Optional ByVal byteTimeout As Byte = &H0)
            'm_dictEventInfo = New SortedDictionary(Of UInt16, Byte())
            m_out_EvtType = EVT_TYPE._UNKNOWN
            m_in_byteTimeout = byteTimeout
        End Sub
        '
        Private Sub CleanupDict()
            'Dim arrayByteTmp As Byte()
            'If (m_dictEventInfo IsNot Nothing) Then
            '    For Each iteratorSeek In m_dictEventInfo
            '        arrayByteTmp = iteratorSeek.Value
            '        Erase arrayByteTmp
            '    Next
            '    '
            '    m_dictEventInfo.Clear()
            'End If
        End Sub

        Public Shared Sub CleanupGlobalDict()
            If (m_dictUIEventInfoMsg IsNot Nothing) Then
                m_dictUIEventInfoMsg.Clear()
            End If
            If (m_dictKeyInfoMsg IsNot Nothing) Then
                m_dictKeyInfoMsg.Clear()
            End If
        End Sub
        '
        Public Sub Cleanup()
            CleanupDict()
            '
            Erase m_out_arrayByteEvtInfo
        End Sub

        Sub ParseInputEvent(o_arrayByteData As Byte())
            If (o_arrayByteData Is Nothing) Then
                Return
            End If

            Dim nDataLen As UInt16
            Dim nIdx As Byte = 0

            'Cleanup Previous Stuff
            Cleanup()

            'Event Type  : 4 bytes
            If (o_arrayByteData.Length >= 4) Then
                m_out_EvtType = CType(((CType(o_arrayByteData(nIdx), UInt32) << 24) And &HFF000000UI) Or ((CType(o_arrayByteData(nIdx + 1), UInt32) << 16) And &HFF0000UI) + ((CType(o_arrayByteData(nIdx + 2), UInt32) << 8) And &HFF00) + (o_arrayByteData(nIdx + 3) And &HFF), EVT_TYPE)
            Else
                m_out_EvtType = EVT_TYPE._UNKNOWN
            End If

            If (o_arrayByteData.Length >= 8) Then
                'Graphic ID : 4 bytes
                nIdx = 4
                m_out_GraphicID = CUInt(((CType(o_arrayByteData(nIdx), UInt32) << 24) And &HFF000000UI) Or ((CType(o_arrayByteData(nIdx + 1), UInt32) << 16) And &HFF0000UI) + ((CType(o_arrayByteData(nIdx + 2), UInt32) << 8) And &HFF00) + (o_arrayByteData(nIdx + 3) And &HFF))
            Else
                m_out_GraphicID = 0
            End If

            If (o_arrayByteData.Length > 8) Then
                'Data: array Len - 8
                nDataLen = CUShort(o_arrayByteData.Count - 8)
                If (nDataLen < 1) Then
                    Throw New Exception("ParseInputEvent() = rest data len (" + nDataLen.ToString() + ") < 0")
                End If
                m_out_arrayByteEvtInfo = New Byte(nDataLen - 1) {}
                nIdx = 8
                Array.Copy(o_arrayByteData, nIdx, m_out_arrayByteEvtInfo, 0, nDataLen)
            Else
                m_out_arrayByteEvtInfo = Nothing
            End If
            '
            Select Case (m_out_EvtType)
                Case EVT_TYPE._BUTTON
                    'Status, 1 byte: 01 = Pressed
                    m_out_byteState = m_out_arrayByteEvtInfo(0)
                    'Btn Text, zero-end string
                    ClassTableListMaster.ConvertFromHexToAscii(m_out_arrayByteEvtInfo, 1, m_out_arrayByteEvtInfo.Length - 1, m_strBtn_Txt, True)

                Case EVT_TYPE._BUTTON_RADIO
                    Throw New Exception("Not Implemented Event Type = Radio Button")
                Case EVT_TYPE._CHECK_BOX
                    Throw New Exception("Not Implemented Event Type = Check Box")
                Case EVT_TYPE._KEYPAD
                    m_out_byteState = m_out_arrayByteEvtInfo(0)
                    m_out_u16KeyID = CUShort(((CType(m_out_arrayByteEvtInfo(1), UInt16) << 8) And &HFF00) + (CType(m_out_arrayByteEvtInfo(2), UInt16) And &HFF))
                Case EVT_TYPE._LINE_ITEM
                    Throw New Exception("Not Implemented Event Type = Line Item")
                Case EVT_TYPE._SLIDESHOW
                    Throw New Exception("Not Implemented Event Type = Slide Show")
                Case EVT_TYPE._TOUCH_SCREEN
                    Throw New Exception("Not Implemented Event Type = Touch Screen")
                Case EVT_TYPE._TRANSACTION
                    Throw New Exception("Not Implemented Event Type = Transaction")
                Case EVT_TYPE._UI_MODULE
                    Throw New Exception("Not Implemented Event Type = UI Event")
                Case Else
                    'Unknown Case
                    Throw New Exception("Unknown UI Event = " + String.Format("{0,4:X4}", m_out_EvtType))
                    Return
            End Select
        End Sub

        Sub SetNoEvent()
            If (m_out_arrayByteEvtInfo IsNot Nothing) Then
                Erase m_out_arrayByteEvtInfo
                m_out_arrayByteEvtInfo = Nothing
            End If
        End Sub
        ReadOnly Property bIsUIEvtAvailable As Boolean
            Get
                Return (m_out_arrayByteEvtInfo IsNot Nothing)
            End Get
        End Property

    End Structure

    '=======================[ Packet Composer & Parser Section ]=======================

    'Protected m_bInterruptCommand As Boolean
    '=================[Private Variables Area]===================
    Protected m_arraybyteSendBuf As Byte()
    Protected m_arraybyteSendBufUnknown As Byte()
    '--------------------------[]------------------------------
    Public Enum ENHANCE_CARD_TYPE As Byte
        POLL_CONTACT = &H1
        POLL_CONTACTLESS = &H2
        POLL_MSR = &H4
    End Enum
    Public Enum ENHANCE_UI_FLAGs As Byte
        DISABLE_CL_LOGO = &H8
        DISABLE_BUZZER = &H4
        DISABLE_LED = &H2
        DISABLE_LCD = &H1
    End Enum
    Public Enum ENU_DEV_INTERFACE As Integer
        INTF_RS232
        INTF_USBHID

        INTF_UNKNOWN
    End Enum


    ReadOnly Property bIsBusy As Boolean
        Get
            Return m_bIDGCmdBusy
        End Get
    End Property

    ReadOnly Property bIsConnected As Boolean
        Get
            If (m_refdevConn Is Nothing) Then
                Return False
            End If

            Select Case (m_nDevIntf)
                Case ENU_DEV_INTERFACE.INTF_RS232
                    Return m_devRS232.bIsOpen
                Case ENU_DEV_INTERFACE.INTF_USBHID
                    Return m_devUSBHID.bIsOpen
            End Select

            'Return (m_nDevIntf <> ENU_DEV_INTERFACE.INTF_UNKNOWN)
            Return False
        End Get
    End Property


    'New (Constructor) can't override...
    'Sub New(ByRef refwinformMain As Form01_Main , ByRef refobjCTLB As ClassTableListMaster, ByRef refDictTlvNewOpConfigurations As Dictionary(Of String, tagTLVNewOp), ByRef refDictTlvNewOpCAPK As Dictionary(Of String, tagTLVNewOpCAPK))
    Sub New(ByRef refwinformMain As Form01_Main, ByRef refobjCTLB As ClassTableListMaster)

        m_devRS232 = New ClassDevRS232(refwinformMain)
        m_devUSBHID = New ClassDevUSBHID(refwinformMain)
        '
        m_refwinformMain = Form01_Main.GetInstance()
        '
        'Create Logging Stuffs
        LogLnThreadSafe = New Form01_Main.LogLnDelegate(AddressOf m_refwinformMain.LogThreadSafe)
        DumpingRecv = New Form01_Main.LogLnDelegate(AddressOf m_refwinformMain.DumpingRecvAsciiStr)
        DumpingRecvHexStr = New Form01_Main.LogLnDelegate(AddressOf m_refwinformMain.DumpingRecvHexStr)

        CalledByPreProcessCbRecv_InterruptByExternalStatusUpdate = New Form01_Main.CalledByPreProcessCbRecv_IBESU(AddressOf m_refwinformMain.CalledByPreProcessCbRecv_InterruptByExternalStatusUpdate)
        LogLn = New Form01_Main.LogLnDelegate(AddressOf m_refwinformMain.LogLn)
        '

        m_refobjCTLM = refobjCTLB
        'm_refdictTlvNewOpConfigurations = refDictTlvNewOpConfigurations
        'm_refdictTlvNewOpCAPK = refDictTlvNewOpCAPK
        '
        m_arrayXYEndianIndex = {New Point(1, 0), New Point(0, 1)}
        '
        m_bIsRetStatusOK = False
        m_bIDGCmdBusy = False
        '
        m_nIDGCommanderType = ClassReaderInfo.ENU_FW_PLATFORM_TYPE.UNKNOWN
        '
        m_arraybyteSendBuf = New Byte(ClassDevRS232.m_cnRX_BUF_MAX - 1) {}
        m_arraybyteSendBufUnknown = New Byte(ClassDevRS232.m_cnRX_BUF_MAX - 1) {}

        PT_SystemSetEnhancePassthroughMode_Init()

        ARExclusive_ATRResponseMsg_Init()
    End Sub

    ReadOnly Property bIsRetStatus_x12_SecuIntfNoReadyOrIntrmedt As Boolean
        Get
            Return (ResponseCode = ClassReaderProtocolBasic.EnumSTATUSCODE2.x12SecureInterfaceIsNotFunctionalOrIsInAnIntermediateState)
        End Get
    End Property

    ReadOnly Property bIsRetStatus_x2F_SecuIntfKeyCertNotActivate As Boolean
        Get
            Return (ResponseCode = ClassReaderProtocolBasic.EnumSTATUSCODE2.x2FSecuIntfKeyCertNotActivate)
        End Get
    End Property

    ReadOnly Property propResponseAryByteGet As Byte()
        Get
            If (m_tagIDGCmdDataInfo.bIsRawData) Then
                Dim aryByteRaw As Byte() = Nothing
                m_tagIDGCmdDataInfo.ParserDataGetArrayRawData(aryByteRaw)
                Return aryByteRaw
            End If
            '
            Return Nothing
        End Get
    End Property

    'ReadOnly Property bIsRetStatus_x45_NoSerialNumber As Boolean
    '    Get
    '        Return m_tagIDGCmdDataInfo.bIsResponseFail_x45_NoSerialNumber
    '    End Get
    'End Property

    Public Overridable Sub Init()

    End Sub

    Public ReadOnly Property bIsIDGCmderAR As Boolean
        Get
            Return (m_nIDGCommanderType = ClassReaderInfo.ENU_FW_PLATFORM_TYPE.AR)
        End Get
    End Property

    Public ReadOnly Property bIsIDGCmderGR As Boolean
        Get
            Return (m_nIDGCommanderType = ClassReaderInfo.ENU_FW_PLATFORM_TYPE.GR)
        End Get
    End Property

    Public ReadOnly Property bIsIDGCmderUnknown As Boolean
        Get
            Return (m_nIDGCommanderType = ClassReaderInfo.ENU_FW_PLATFORM_TYPE.UNKNOWN)
        End Get
    End Property

    Public Property bLogTurnOnOff As Boolean
        Get
            Select Case (m_nDevIntf)
                Case ENU_DEV_INTERFACE.INTF_RS232
                    Return m_devRS232.bLogTurnOnOff
                Case ENU_DEV_INTERFACE.INTF_USBHID
                    Return m_devUSBHID.bLogTurnOnOff
            End Select
            Return False
        End Get

        Set(value As Boolean)
            m_devRS232.bLogTurnOnOff = value

            m_devUSBHID.bLogTurnOnOff = value
        End Set
    End Property

    Public WriteOnly Property bIsHWTestOnly As Boolean
        Set(value As Boolean)
            m_devRS232.bIsHWTestOnly = value

            m_devUSBHID.bIsHWTestOnly = value
        End Set
    End Property

    Public Property propUSBHIDGetProductNameID As ClassReaderInfo.ENUM_PD_IMG_IDX
        Set(value As ClassReaderInfo.ENUM_PD_IMG_IDX)
            m_devUSBHID.propGetNameID = value
        End Set
        Get
            If (Not m_devUSBHID.bIsOpen) Then
                Return ClassReaderInfo.ENUM_PD_IMG_IDX.UNKNOWN_MAX
            End If

            Return m_devUSBHID.propGetNameID
        End Get
    End Property

    Public Overridable Sub Cleanup()
        'Cleanup & Dispose RS232
        m_devRS232.Cleanup()
        m_devUSBHID.Cleanup()
        '
        If m_arrayXYEndianIndex IsNot Nothing Then
            Erase m_arrayXYEndianIndex
            m_arrayXYEndianIndex = Nothing
        End If

        '
        If m_arraybyteSendBuf IsNot Nothing Then
            Erase m_arraybyteSendBuf
            m_arraybyteSendBuf = Nothing
        End If

        '
        If m_arraybyteSendBufUnknown IsNot Nothing Then
            Erase m_arraybyteSendBufUnknown
            m_arraybyteSendBufUnknown = Nothing
        End If

        PT_SystemSetEnhancePassthroughMode_Cleanup()

        ARExclusive_ATRResponseMsg_Cleanup()
    End Sub

    ReadOnly Property bIsConnectedRS232 As Boolean
        Get
            Return (m_nDevIntf = ENU_DEV_INTERFACE.INTF_RS232)
        End Get
    End Property
    ReadOnly Property bIsConnectedUSBHID As Boolean
        Get
            Return (m_nDevIntf = ENU_DEV_INTERFACE.INTF_USBHID)
        End Get
    End Property
    Public Sub ConnectionOpen(ByVal strComPort As String, nBuadRate As ClassDevRS232.EnumBUAD_RATE)
        m_devRS232.Config(strComPort, nBuadRate) ' ClassDevRS232.BUAD_RATE.BUADRATE_115200)
        m_devRS232.Open()

        'Need to ping it if okay
        If (m_devRS232.bIsOpen()) Then
            m_nDevIntf = ENU_DEV_INTERFACE.INTF_RS232
            m_refdevConn = m_devRS232
        Else
            m_nDevIntf = ENU_DEV_INTERFACE.INTF_UNKNOWN
            m_refdevConn = Nothing
        End If
    End Sub

    'Since USB HID connection is send depend on USB Device's VID+PID Identification
    Public Sub ConnectionOpen(Optional ByVal strDefCfgXml_VID As String = "", Optional ByVal strDefCfgXml_PID As String = "")
        Dim pCmdLnInst = ClassCmdLineArgumentOptions.GetInstance()
        'Set up new VID , PID if there is customized VID/PID
        'LogLn("usb]--001")
        If (pCmdLnInst IsNot Nothing) Then
            If (pCmdLnInst.bIsUSBHID_PID_VID_Available) Then
                'LogLn("usb]--002")
                m_devUSBHID.SetVidPid(pCmdLnInst.u16USBHID_VID, pCmdLnInst.u16USBHID_PID)
            ElseIf ((strDefCfgXml_PID IsNot Nothing) And (Not strDefCfgXml_PID.Equals(""))) Then
                'LogLn("usb]--003")
                Dim u32Vid As UInt32
                Dim u32Pid As UInt32

                u32Vid = Convert.ToInt32(strDefCfgXml_VID, 16)
                u32Pid = Convert.ToInt32(strDefCfgXml_PID, 16)
                m_devUSBHID.SetVidPid(u32Vid, u32Pid)
            End If
        End If
        'LogLn("usb]--004")

        'Open it
        m_devUSBHID.OpenAndWait()
        'LogLn("usb]--005")

        If (m_devUSBHID.bIsOpen) Then
            'LogLn("usb]--006")
            m_nDevIntf = ENU_DEV_INTERFACE.INTF_USBHID
            m_refdevConn = m_devUSBHID
        Else
            'LogLn("usb]--007")
            m_nDevIntf = ENU_DEV_INTERFACE.INTF_UNKNOWN
            m_refdevConn = Nothing
        End If
        'LogLn("usb]--008")
    End Sub

    Public Function RecvDataRealTimeUpdate(ByRef arrayByte As Byte(), u32RecvLen As UInt32, u32TimeoutMS As UInt32) As UInt32
        Dim u32RecvDataSize As UInt32 = 0

        If (m_bIDGCmdBusy) Then
            Return 0
        End If

        Try
            m_bIDGCmdBusy = True

            'This cleans up interrupt flag to false, may be interrupt by external event.
            m_tagIDGCmdDataInfo.Reinit(CType(&H0, UShort), m_arraybyteSendBuf, Me, , m_arraybyteSendBufUnknown)

            Select Case (m_nDevIntf)
                Case ENU_DEV_INTERFACE.INTF_RS232
                    'Do Nothing Since Serial Port Instance Thread Takes care incoming data
                Case ENU_DEV_INTERFACE.INTF_USBHID
                    '2016 Dec 07, To Prevent from previous External Cancel Behavior
                    m_devUSBHID.InterruptExternalLoop(False)
                    u32RecvDataSize = m_devUSBHID.RecvDataRealTimeUpdate(arrayByte, u32RecvLen, u32TimeoutMS)
            End Select
            '
        Catch ex As Exception
            Throw ex
        Finally
            m_bIDGCmdBusy = False
        End Try
        '
        Return u32RecvDataSize
    End Function



    Public Sub ConnectionClose()
        Dim lStartTicks As Long = Date.Now.Ticks
        Dim lTicksMax As Long = 1 * 1000000 '10 million ticks in a second
        Dim lCurtTicks As Long

        If (m_nDevIntf = ENU_DEV_INTERFACE.INTF_UNKNOWN) Then
            Return
        End If
        '
        'Waiting Till IDG Command Stop/Cancel/Done
        While (m_bIDGCmdBusy)
            m_tagIDGCmdDataInfo.CallByIDGCmdTimeOutChk_SetTimeoutEventIDGCancelStop()
            'Application.DoEvents() '<------------ Don't do this will cause infinite loop in some critical situation

            ' Time out checking
            lCurtTicks = Date.Now.Ticks
            If ((lCurtTicks - lStartTicks) > lTicksMax) Then
                Exit While
            End If
        End While


        'Locked IDG Command Execution
        m_bIDGCmdBusy = True
        'Select Case (m_nDevIntf)
        '    Case ENU_DEV_INTERFACE.INTF_RS232
        '        m_devRS232.Close()
        '    Case ENU_DEV_INTERFACE.INTF_USBHID
        '        m_devUSBHID.Close()
        'End Select
        If (m_refdevConn IsNot Nothing) Then
            m_refdevConn.Close()
        End If
        'Unlocked it
        m_bIDGCmdBusy = False
        m_nDevIntf = ENU_DEV_INTERFACE.INTF_UNKNOWN
    End Sub

    '
    'Public Sub DevInterfaceUseUSBHID()
    '    m_nDevIntf = ENU_DEV_INTERFACE.INTF_USBHID
    'End Sub

    'Public Sub DevInterfaceUseRS232()
    '    m_nDevIntf = ENU_DEV_INTERFACE.INTF_RS232
    'End Sub

    '================================================================================================
    Protected Shared m_tagIDGCmdDataInfo As tagIDGCmdData
    Protected Shared m_tagIDGCmdDataInfoTransactionCancel As tagIDGCmdData

    '
    ReadOnly Property propGetIDGCmdResultDataInfo As tagIDGCmdData
        Get
            Return m_tagIDGCmdDataInfo
        End Get
    End Property

    ReadOnly Property ResponseCode As Integer
        Get
            Return m_tagIDGCmdDataInfo.m_nResponse
        End Get
    End Property

    Protected Shared Function GetIDGCmdString(m_u16CmdSet As UShort) As String
        Dim strCmd As String
        '
        strCmd = String.Format("{0,2:X2}-{1,2:X2}", ((m_u16CmdSet >> 8) And &HFF), (m_u16CmdSet And &HFF))
        '
        Return strCmd

    End Function
    '==================================[ Dictionary Clone Functions ]==================================
    Public Shared Function CloneDict(ByVal inputObj As Dictionary(Of String, tagTLV)) As Dictionary(Of String, tagTLV)
        'creating a Memorystream which works like a temporary storeage '
        'Using memStrm As New MemoryStream()
        '    'Binary Formatter for serializing the object into memory stream '
        '    Dim binFormatter As New BinaryFormatter(Nothing, New StreamingContext(StreamingContextStates.Clone))

        '    'talks for itself '
        '    binFormatter.Serialize(memStrm, inputObj)

        '    'setting the memorystream to the start of it '
        '    memStrm.Seek(0, SeekOrigin.Begin)

        '    'try to cast the serialized item into our Item '
        '    Try
        '        Return DirectCast(binFormatter.Deserialize(memStrm), T)
        '    Catch ex As Exception
        '        Trace.TraceError(ex.Message)
        '        Return Nothing
        '    End Try
        'End Using
        Dim retDict As Dictionary(Of String, tagTLV) = Nothing

        Try
            retDict = New Dictionary(Of String, tagTLV)
            Dim tagtlvNew As tagTLV
            Dim tagtlvOriginal As tagTLV
            Dim iteratorSeek As KeyValuePair(Of String, tagTLV)

            For Each iteratorSeek In inputObj
                tagtlvOriginal = iteratorSeek.Value
                With tagtlvOriginal
                    tagtlvNew = New tagTLV(.m_strTag, .m_nLen, .m_arrayTLVal, .m_strCommentTrail)
                End With

                retDict.Add(iteratorSeek.Key, tagtlvNew)
            Next


        Catch ex As Exception
            LogLn("[Err] Clone() = " + ex.Message)
        End Try

        Return retDict
    End Function
    Public Shared Function CloneDict(ByVal inputObj As SortedDictionary(Of String, tagTLV)) As SortedDictionary(Of String, tagTLV)
        'creating a Memorystream which works like a temporary storeage '
        'Using memStrm As New MemoryStream()
        '    'Binary Formatter for serializing the object into memory stream '
        '    Dim binFormatter As New BinaryFormatter(Nothing, New StreamingContext(StreamingContextStates.Clone))

        '    'talks for itself '
        '    binFormatter.Serialize(memStrm, inputObj)

        '    'setting the memorystream to the start of it '
        '    memStrm.Seek(0, SeekOrigin.Begin)

        '    'try to cast the serialized item into our Item '
        '    Try
        '        Return DirectCast(binFormatter.Deserialize(memStrm), T)
        '    Catch ex As Exception
        '        Trace.TraceError(ex.Message)
        '        Return Nothing
        '    End Try
        'End Using
        Dim retDict As SortedDictionary(Of String, tagTLV) = Nothing

        Try
            retDict = New SortedDictionary(Of String, tagTLV)
            Dim tagtlvNew As tagTLV
            Dim tagtlvOriginal As tagTLV
            Dim iteratorSeek As KeyValuePair(Of String, tagTLV)

            For Each iteratorSeek In inputObj
                tagtlvOriginal = iteratorSeek.Value
                With tagtlvOriginal
                    tagtlvNew = New tagTLV(.m_strTag, .m_nLen, .m_arrayTLVal, .m_strCommentTrail)
                End With

                retDict.Add(iteratorSeek.Key, tagtlvNew)
            Next


        Catch ex As Exception
            LogLn("[Err] Clone() = " + ex.Message)
        End Try

        Return retDict
    End Function

    '=================[Private Sub/Func Area]=================
    'Logging
    'Private m_dbgInfo As StackFrame = New StackFrame
    'Delegate Function type
    'Public Delegate Sub LogLnDelegate(ByRef strInfo As String)
    Public Shared LogLn As Form01_Main.LogLnDelegate
    Protected Shared LogLnThreadSafe As Form01_Main.LogLnDelegate
    Protected Shared DumpingRecv As Form01_Main.LogLnDelegate
    Protected Shared DumpingRecvHexStr As Form01_Main.LogLnDelegate

    Public Shared CalledByPreProcessCbRecv_InterruptByExternalStatusUpdate As Form01_Main.CalledByPreProcessCbRecv_IBESU
    '=================[Private Variables Area]===================

    '=================[Public Variables Area]===================
    ReadOnly Property bIsRetStatusOK As Boolean
        Get
            Return m_bIsRetStatusOK
        End Get
    End Property
    '
    ReadOnly Property bIsRetStatusNotAllowed As Boolean
        Get
            Return m_tagIDGCmdDataInfo.bIsResponseNotAllowed
        End Get
    End Property
    '
    ReadOnly Property bIsRetStatusTimeout As Boolean
        Get
            Return m_refdevConn.bIsStatusTimeout Or m_tagIDGCmdDataInfo.bIsResponseTimeout
        End Get
    End Property

    '===========[Interruption Services]=========
    ReadOnly Property bIsIDGCmdInterruptedByExternal As Boolean
        Get
            Return m_tagIDGCmdDataInfo.bIsInterruptedByExternal
        End Get
    End Property


    Private Sub SystemInterruptCommand()
        'If there is ALREADY a IDG Transaction Activate Command Running, we have to Terminate it.
        If (m_bIDGCmdBusy) Then
            m_tagIDGCmdDataInfo.WaitCommandLoopInterrupt()
        Else
            Exit Sub
        End If
    End Sub

    ' External Calling - No Loop Wait Busy
    Public Sub IDGCmdInterruptNoWait()
        'Waiting Till IDG Command Stop/Cancel/Done
        If (m_bIDGCmdBusy) Then
            Select Case (m_nDevIntf)
                Case ENU_DEV_INTERFACE.INTF_RS232
                    SystemInterruptCommand()
                    'Already done in SystemInterruptCommand()
                    'm_tagIDGCmdDataInfo.setInterruptByExternal = True
                Case ENU_DEV_INTERFACE.INTF_USBHID
                    m_devUSBHID.InterruptExternalLoop(True)
                    'For ProtocolCmder's Caller Layer Check
                    m_tagIDGCmdDataInfo.setInterruptByExternal = True
            End Select
        End If
    End Sub

    ' External Calling - Loop Busy Wait
    Public Sub IDGCmdInterruptWaitBusyToIdle()
        'Waiting Till IDG Command Stop/Cancel/Done
        While (m_bIDGCmdBusy)
            Select Case (m_nDevIntf)
                Case ENU_DEV_INTERFACE.INTF_RS232
                    SystemInterruptCommand()
                    'Already done in SystemInterruptCommand()
                    'm_tagIDGCmdDataInfo.setInterruptByExternal = True
                Case ENU_DEV_INTERFACE.INTF_USBHID
                    m_devUSBHID.InterruptExternalLoop(True)
                    'For ProtocolCmder's Caller Layer Check
                    m_tagIDGCmdDataInfo.setInterruptByExternal = True
            End Select
            '
            Application.DoEvents()
        End While
    End Sub
    '===========[End of Interruption Services Section ]=========
    '
    ReadOnly Property bIsRetStatusFailedNAK As Boolean
        Get
            Return m_tagIDGCmdDataInfo.bIsResponseFailNAK
        End Get
    End Property

    ReadOnly Property bIsResponseCodeUnknownCommand As Boolean
        Get
            Return (ResponseCode = ClassReaderProtocolIDGGR.EnumSTATUSCODE2.x02UnknownCommand)
        End Get
    End Property

    ReadOnly Property bIsResponseCodeImproperBitmap As Boolean
        Get
            Return (ResponseCode = ClassReaderProtocolIDGGR.EnumSTATUSCODE2.x22ILM_ImproperBitMap)
        End Get
    End Property

    '=================[Private Sub/Func Area]=================
    Private Sub TestFuncCRCTableUsedByIDGCmdSet()
        Dim cmdIDG() As Byte
        Dim CrcTest1 As String
        Dim CrcTest11() As Byte
        Dim CrcTest2() As Byte
        Dim CrcTest3() As Byte
        Dim u16Crc As UInt16

        cmdIDG = New Byte() {&H56, &H69, &H56, &H4F, &H74, &H65, &H63, &H68, &H32, &H0,
                             &H18, &H1, &H0, &H0, &HB3, &HCD}
        CrcTest1 = "123456789"

        CrcTest11 = System.Text.Encoding.ASCII.GetBytes(CrcTest1)
        CrcTest2 = New Byte() {1, 2, 3, 4, 5}
        CrcTest3 = New Byte() {&H56, &H69, &H56, &H4F, &H74, &H65, &H63, &H68, &H0, &H43, &H18, &H0, &H0, &H0}

        u16Crc = IDGCmdCRCEvaluate(CrcTest11, CrcTest11.Length) '=&H29B1
        u16Crc = IDGCmdCRCEvaluate(CrcTest2, CrcTest2.Length) '=&H9304
        u16Crc = IDGCmdCRCEvaluate(CrcTest3, CrcTest3.Length) '=&HA1F5

        '---------------[ Cleanup Sections ]---------------
        Erase cmdIDG
        Erase CrcTest2
        Erase CrcTest3
    End Sub


    '==================[Public Sub / Func Area ]=====================
    Private Shared m_arrayu16IDGCmdSetCrcTable() As UInt16 = New UInt16() { _
        &H0, &H1021, &H2042, &H3063, &H4084, &H50A5, &H60C6, &H70E7, &H8108, &H9129,
  &HA14A, &HB16B, &HC18C, &HD1AD, &HE1CE, &HF1EF, &H1231, &H210, &H3273, &H2252,
  &H52B5, &H4294, &H72F7, &H62D6, &H9339, &H8318, &HB37B, &HA35A, &HD3BD, &HC39C,
  &HF3FF, &HE3DE, &H2462, &H3443, &H420, &H1401, &H64E6, &H74C7, &H44A4, &H5485,
  &HA56A, &HB54B, &H8528, &H9509, &HE5EE, &HF5CF, &HC5AC, &HD58D, &H3653, &H2672,
  &H1611, &H630, &H76D7, &H66F6, &H5695, &H46B4, &HB75B, &HA77A, &H9719, &H8738,
  &HF7DF, &HE7FE, &HD79D, &HC7BC, &H48C4, &H58E5, &H6886, &H78A7, &H840, &H1861,
  &H2802, &H3823, &HC9CC, &HD9ED, &HE98E, &HF9AF, &H8948, &H9969, &HA90A, &HB92B,
  &H5AF5, &H4AD4, &H7AB7, &H6A96, &H1A71, &HA50, &H3A33, &H2A12, &HDBFD, &HCBDC,
  &HFBBF, &HEB9E, &H9B79, &H8B58, &HBB3B, &HAB1A, &H6CA6, &H7C87, &H4CE4, &H5CC5,
  &H2C22, &H3C03, &HC60, &H1C41, &HEDAE, &HFD8F, &HCDEC, &HDDCD, &HAD2A, &HBD0B,
  &H8D68, &H9D49, &H7E97, &H6EB6, &H5ED5, &H4EF4, &H3E13, &H2E32, &H1E51, &HE70,
  &HFF9F, &HEFBE, &HDFDD, &HCFFC, &HBF1B, &HAF3A, &H9F59, &H8F78, &H9188, &H81A9,
  &HB1CA, &HA1EB, &HD10C, &HC12D, &HF14E, &HE16F, &H1080, &HA1, &H30C2, &H20E3,
  &H5004, &H4025, &H7046, &H6067, &H83B9, &H9398, &HA3FB, &HB3DA, &HC33D, &HD31C,
  &HE37F, &HF35E, &H2B1, &H1290, &H22F3, &H32D2, &H4235, &H5214, &H6277, &H7256,
  &HB5EA, &HA5CB, &H95A8, &H8589, &HF56E, &HE54F, &HD52C, &HC50D, &H34E2, &H24C3,
  &H14A0, &H481, &H7466, &H6447, &H5424, &H4405, &HA7DB, &HB7FA, &H8799, &H97B8,
  &HE75F, &HF77E, &HC71D, &HD73C, &H26D3, &H36F2, &H691, &H16B0, &H6657, &H7676,
  &H4615, &H5634, &HD94C, &HC96D, &HF90E, &HE92F, &H99C8, &H89E9, &HB98A, &HA9AB,
  &H5844, &H4865, &H7806, &H6827, &H18C0, &H8E1, &H3882, &H28A3, &HCB7D, &HDB5C,
  &HEB3F, &HFB1E, &H8BF9, &H9BD8, &HABBB, &HBB9A, &H4A75, &H5A54, &H6A37, &H7A16,
  &HAF1, &H1AD0, &H2AB3, &H3A92, &HFD2E, &HED0F, &HDD6C, &HCD4D, &HBDAA, &HAD8B,
  &H9DE8, &H8DC9, &H7C26, &H6C07, &H5C64, &H4C45, &H3CA2, &H2C83, &H1CE0, &HCC1,
  &HEF1F, &HFF3E, &HCF5D, &HDF7C, &HAF9B, &HBFBA, &H8FD9, &H9FF8, &H6E17, &H7E36,
  &H4E55, &H5E74, &H2E93, &H3EB2, &HED1, &H1EF0}
    '    static unsigned short CalculateChecksum(unsigned char* data, int length)
    '{
    '	unsigned short crc = 0xffff;

    '	while(length--)
    '	{
    '		 crc = crcTable[ ((crc >> 8) ^ *data++) ] ^ (crc << 8);      
    '	}
    '	return crc;
    '}

    '    Data String (ASCII Text): 123456789
    'CRC: 29B1h
    'Data (hex): [01h] [02h] [03h] [04h] [05h]
    'CRC: 9304h
    'Data (hex): [56] [69] [56] [4F] [74] [65] [63] [68] [00] [43] [18] [00] [00] [00]
    'CRC: A1F5h
    Private Shared m_arrayXYEndianIndex() As Point
    Public Shared Sub IDGCmdFrameCRCSet(ByVal data() As Byte, ByVal nDataLen As Integer, Optional ByVal bBigEndian As Boolean = False)
        Dim u16Crc As UInt16 = &HFFFF
        Dim nIdx As Integer
        'Buffer Accommodation Check
        If data.Count < (nDataLen + 2) Then
            Throw New Exception("Data Buffer Over flow( buff size = " & data.Count & ", needed space = " & (nDataLen + 2))
        End If
        ' Padding CRC to packet trail
        u16Crc = IDGCmdCRCEvaluate(data, nDataLen)

        If bBigEndian Then
            nIdx = 1
        Else
            nIdx = 0
        End If
        data(m_arrayXYEndianIndex(nIdx).X + nDataLen) = CType(((u16Crc >> 8) And &HFF), Byte)
        data(m_arrayXYEndianIndex(nIdx).Y + nDataLen) = CType((u16Crc And &HFF), Byte)

    End Sub

    'Input Data String (ASCII Text): 123456789
    'CRC: 29B1h
    'Input Data (hex): [01h] [02h] [03h] [04h] [05h]
    'CRC: 9304h
    'Input Data (hex): [56] [69] [56] [4F] [74] [65] [63] [68] [00] [43] [18] [00] [00] [00]
    'CRC: A1F5h
    Public Overloads Shared Function IDGCmdCRCEvaluate(ByVal data() As Byte, ByVal nDataLen As Integer) As UInt16
        Dim n16Result As UInt16 = &H0
        Dim u16Crc As UInt16 = &HFFFF
        Dim u16Crc1 As UInt16
        Dim u16Crc2 As UInt16
        Dim i As Integer = 0
        '
        '
        For Each bData As Byte In data
            u16Crc1 = (u16Crc >> 8) Xor data(i)
            u16Crc2 = u16Crc << 8
            u16Crc = m_arrayu16IDGCmdSetCrcTable(u16Crc1) Xor u16Crc2
            i = i + 1
            If (i = nDataLen) Then
                Exit For
            End If
        Next bData
        '
        Return u16Crc
    End Function

    Public Overloads Shared Function IDGCmdCRCEvaluate(ByVal data() As Byte, ByVal nStartOff As Integer, ByVal nDataLen As Integer) As UInt16
        Dim n16Result As UInt16 = &H0
        Dim u16Crc As UInt16 = &HFFFF
        Dim u16Crc1 As UInt16
        Dim u16Crc2 As UInt16
        Dim i As Integer = 0
        Dim iMax As Integer = nDataLen - 1
        '
        If (iMax > data.Length - 1) Then
            iMax = data.Length - 1
        End If
        For i = nStartOff To iMax
            u16Crc1 = (u16Crc >> 8) Xor data(i)
            u16Crc2 = u16Crc << 8
            u16Crc = m_arrayu16IDGCmdSetCrcTable(u16Crc1) Xor u16Crc2
        Next
        '
        Return u16Crc
    End Function
    'RS232 Packets Style

    '=======================[ APIs / SDKs  Section ]=======================
    '
    Public Sub System_Customized_Command_TimeMS(ByVal u16Cmd As UInt16, ByVal strFuncName As String, ByVal arrayByteSendData As Byte(), ByRef o_arrayByteRecv As Byte(), callbackExternalRecvProcedure As ClassDevRS232.DelegatecallbackExternalRecvProcedure, Optional ByVal nTimeoutMS As Integer = 1000, Optional bCRCMSB As Boolean = False, Optional _
                                         bWait As Boolean = True)
        'Dim strFuncName As String = "System_Customized_Command"
        '

        Try
            m_bIsRetStatusOK = False

            m_tagIDGCmdDataInfo.Reinit(u16Cmd, m_arraybyteSendBuf, Me, , m_arraybyteSendBufUnknown)

            'Add Send Data
            If (arrayByteSendData IsNot Nothing) Then
                m_tagIDGCmdDataInfo.ComposerAddData(arrayByteSendData)
            End If

            '-----------------------------------------------------------------[ Send to reader ]-----------------------------------------------------------------
            'Compose 1)Protocol Header 2) Command Set 3) Length 4) Command Frame CRC Data
            PktComposerCommandFrameHeaderCmdSetDataLengthCRCData(m_tagIDGCmdDataInfo, bCRCMSB)

            '----------------[ Debug Section ]----------------
            If (m_bDebug) Then
                'Note: After PktComposerCommandFrameHeaderCmdSetDataLengthCRCData(), m_tagIDGCmdDataInfo.m_nDataSizeSend includes m_nPktHeader + m_nPktTrail
                'm_refwinformMain.LogLnDumpData(m_tagIDGCmdDataInfo.m_refarraybyteData, tagIDGCmdData.m_nPktHeader + m_tagIDGCmdDataInfo.m_nDataSizeSend + tagIDGCmdData.m_nPktTrail)
                Form01_Main.LogLnDumpData(m_tagIDGCmdDataInfo.m_refarraybyteData.Skip(m_tagIDGCmdDataInfo.m_nDataRecvOffSkipBytes).ToArray(), m_tagIDGCmdDataInfo.m_nDataSizeRecv)
            End If

            '----------------[ End Debug Section ]----------------
            'Waiting For Response in SetConfigurationsGroupSingleCbRecv()
            If (callbackExternalRecvProcedure Is Nothing) Then
                callbackExternalRecvProcedure = AddressOf ClassReaderProtocolBasic.PreProcessCbRecv
            End If

            m_refdevConn.SendCommandFrameAndWaitForResponseFrame(callbackExternalRecvProcedure, m_tagIDGCmdDataInfo, m_nIDGCommanderType, bWait, nTimeoutMS)

            'Its' response code = 0 => OK
            If (m_tagIDGCmdDataInfo.bIsResponseOK) Then
                m_bIsRetStatusOK = True
            End If

            '----------------------[ ParserDataGetArrayRawData() Already done for necessary stuffs ]-------------
            'If (o_arrayByteRecv IsNot Nothing) Then
            '    If (m_tagIDGCmdDataInfo.bIsRawData) Then
            '        m_tagIDGCmdDataInfo.ParserDataGetArrayRawData(o_arrayByteRecv)
            '    Else
            '        'Set First byte as 0 
            '        o_arrayByteRecv(0) = &H0
            '    End If
            'Else
            '    m_tagIDGCmdDataInfo.ParserDataGetArrayRawData(o_arrayByteRecv)
            'End If
            m_tagIDGCmdDataInfo.ParserDataGetArrayRawData(o_arrayByteRecv)

        Catch ext As TimeoutException
            LogLn("[Err][Timeout] [IDG: " + GetIDGCmdString(u16Cmd) + "] " + strFuncName + "() = " + ext.Message)
        Catch ex As Exception
            LogLn("[Err] [IDG:  " + GetIDGCmdString(u16Cmd) + "] " + strFuncName + "() = " + ex.Message)
            Throw 'ex
        Finally

        End Try
    End Sub

    Public Sub System_Customized_TxRx_MS(Optional ByVal strCmd As String = "1801", Optional ByVal i_strByteSend As String = "", Optional ByRef o_strRecv As String = "", Optional ByVal nTimeoutMS As Integer = 1000)
        '
        Dim strFuncName As String = "Customized Tx/Rx"
        Dim u16Cmd As UInt16
        Dim arrayByteSend As Byte() = Nothing
        Dim arrayByteRecv As Byte() = Nothing
        Dim aryByteTmp As Byte() = Nothing

        If (m_bIDGCmdBusy) Then
            Return
        End If
        m_bIDGCmdBusy = True
        '
        Try
            o_strRecv = ""
            ClassTableListMaster.TLVauleFromString2ByteArray(strCmd, aryByteTmp)
            ClassTableListMaster.ConvertFromByteArray2UInt16(aryByteTmp, u16Cmd)
            If ((i_strByteSend.Length > 0) And ((i_strByteSend.Length Mod 2) = 0)) Then
                ClassTableListMaster.TLVauleFromString2ByteArray(i_strByteSend, arrayByteSend)
            End If
            '
            'System_Customized_Command_TimeMS(u16Cmd, strFuncName, arrayByteSend, arrayByteRecv, AddressOf ClassReaderProtocolBasic.PreProcessCbRecv, nTimeoutMS, , False)
            System_Customized_Command_TimeMS(u16Cmd, strFuncName, arrayByteSend, arrayByteRecv, AddressOf ClassReaderProtocolBasic.PreProcessCbRecv, nTimeoutMS, , True)

            If (bIsRetStatusOK) Then
                If (arrayByteRecv IsNot Nothing) Then
                    If (arrayByteRecv.Length > 0) Then
                        ClassTableListMaster.ConvertFromArrayByte2String(arrayByteRecv, o_strRecv)
                    End If
                End If
            End If
        Catch ex As Exception
            Throw
        Finally
            m_bIDGCmdBusy = False
            If (arrayByteSend IsNot Nothing) Then
                Erase arrayByteSend
            End If
            If (arrayByteRecv IsNot Nothing) Then
                Erase arrayByteRecv
            End If

            m_bIDGCmdBusy = False
        End Try
    End Sub


    '
    Public Sub SystemResetToDefault_04F0_RevserveSN(Optional ByVal nTimeout As Integer = ClassDevRS232.m_cnTX_TIMEOUT_IN_SEC)
        '
        '0409 = NEO/IDG Standard Cmd
        '04F0 = Undocumented Cmd
        Dim u16Cmd As UInt16 = &H4F0
        Dim strFuncName As String = "ResetToDefault(NEO ONLY)"
        Dim arrayByteSend As Byte() = Nothing
        Dim arrayByteRecv As Byte() = Nothing

        If (m_bIDGCmdBusy) Then
            Return
        End If

        Try
            'ClassTableListMaster.ConvertValue2ArrayByte(byteModeOn, arrayByteSend)

            System_Customized_Command(u16Cmd, strFuncName, arrayByteSend, arrayByteRecv, AddressOf ClassReaderProtocolBasic.PreProcessCbRecv, nTimeout, , False)

            If (bIsRetStatusOK) Then

            End If
        Catch ex As Exception

        Finally
            m_bIDGCmdBusy = False
            If (arrayByteSend IsNot Nothing) Then
                Erase arrayByteSend
            End If
            If (arrayByteRecv IsNot Nothing) Then
                Erase arrayByteRecv
            End If
        End Try
    End Sub
    '
    Public Overridable Sub SystemResetToDefault(Optional ByVal nTimeout As Integer = ClassDevRS232.m_cnTX_TIMEOUT_IN_SEC)
        '
        '0409 = NEO/IDG Standard Cmd
        '04F0 = Undocumented Cmd
        Dim u16Cmd As UInt16 = &H409
        Dim strFuncName As String = "ResetToDefault"
        Dim arrayByteSend As Byte() = Nothing
        Dim arrayByteRecv As Byte() = Nothing

        If (m_bIDGCmdBusy) Then
            Return
        End If
        m_bIDGCmdBusy = True

        Try
            'ClassTableListMaster.ConvertValue2ArrayByte(byteModeOn, arrayByteSend)

            System_Customized_Command(u16Cmd, strFuncName, arrayByteSend, arrayByteRecv, AddressOf ClassReaderProtocolBasic.PreProcessCbRecv, nTimeout)

            If (bIsRetStatusOK) Then

            Else

            End If
        Catch ex As Exception

        Finally
            m_bIDGCmdBusy = False
            If (arrayByteSend IsNot Nothing) Then
                Erase arrayByteSend
            End If
            If (arrayByteRecv IsNot Nothing) Then
                Erase arrayByteRecv
            End If
        End Try
        '
    End Sub

    Public Overridable Sub SystemResetToDefault_040A_Deprecated(Optional ByVal nTimeout As Integer = ClassDevRS232.m_cnTX_TIMEOUT_IN_SEC)
        '
        '0409 = NEO/IDG Standard Cmd
        '04F0 = Undocumented Cmd
        '040A = Reset Default: EMV + Global / System Settings, except certs+keys
        Dim u16Cmd As UInt16 = &H40A
        Dim strFuncName As String = "SystemResetToDefault_040A"
        Dim arrayByteSend As Byte() = Nothing
        Dim arrayByteRecv As Byte() = Nothing


        If (m_bIDGCmdBusy) Then
            Return
        End If
        m_bIDGCmdBusy = True

        Try
            'ClassTableListMaster.ConvertValue2ArrayByte(byteModeOn, arrayByteSend)

            System_Customized_Command(u16Cmd, strFuncName, arrayByteSend, arrayByteRecv, AddressOf ClassReaderProtocolBasic.PreProcessCbRecv, nTimeout)

            If (bIsRetStatusOK) Then

            Else

            End If
        Catch ex As Exception

        Finally
            m_bIDGCmdBusy = False
            If (arrayByteSend IsNot Nothing) Then
                Erase arrayByteSend
            End If
            If (arrayByteRecv IsNot Nothing) Then
                Erase arrayByteRecv
            End If
        End Try
    End Sub
    '

    '
    Public Overridable Sub SystemGetUUID_AppMode(ByRef o_arrayBytesUUID As Byte(), Optional ByVal nTimeout As Integer = ClassDevRS232.m_cnTX_TIMEOUT_IN_SEC)
        Dim strFuncName As String = "SystemGetUUID_AppMode"
        Dim arrayByteSend As Byte() = Nothing
        Dim arrayByteRecv As Byte() = Nothing
        Dim u16Cmd As UInt16 = &H317
        Try

            System_Customized_Command(u16Cmd, strFuncName, arrayByteSend, arrayByteRecv, AddressOf PreProcessCbRecv, nTimeout, True, False)

            m_bIsRetStatusOK = True

            o_arrayBytesUUID = Nothing

            If (bIsRetStatusOK) Then
                If (m_tagIDGCmdDataInfo.bIsRawData) Then
                    m_tagIDGCmdDataInfo.ParserDataGetArrayRawData(o_arrayBytesUUID)
                End If
            End If
        Catch ex As Exception
            LogLn("[Err] " + strFuncName + " () = " + ex.Message)
        Finally
            m_bIDGCmdBusy = False
            If (arrayByteSend IsNot Nothing) Then
                Erase arrayByteSend
            End If
            If (arrayByteRecv IsNot Nothing) Then
                Erase arrayByteRecv
            End If
        End Try
    End Sub
    '
    Public Overridable Sub SystemGetUUID_BLMode(ByRef o_arrayBytesUUID As Byte(), Optional ByVal nTimeout As Integer = ClassDevRS232.m_cnTX_TIMEOUT_IN_SEC)
        Dim strFuncName As String = "SystemGetUUID_BLMode"
        Dim arrayByteSend As Byte() = Nothing
        Dim arrayByteRecv As Byte() = Nothing
        Dim u16Cmd As UInt16 = &HC717
        Try

            System_Customized_Command(u16Cmd, strFuncName, arrayByteSend, arrayByteRecv, AddressOf PreProcessCbRecv, nTimeout, True, False)

            m_bIsRetStatusOK = True

            o_arrayBytesUUID = Nothing

            If (bIsRetStatusOK) Then
                If (m_tagIDGCmdDataInfo.bIsRawData) Then
                    m_tagIDGCmdDataInfo.ParserDataGetArrayRawData(o_arrayBytesUUID)
                End If
            End If
        Catch ex As Exception
            LogLn("[Err] " + strFuncName + " () = " + ex.Message)
        Finally
            m_bIDGCmdBusy = False
            If (arrayByteSend IsNot Nothing) Then
                Erase arrayByteSend
            End If
            If (arrayByteRecv IsNot Nothing) Then
                Erase arrayByteRecv
            End If
        End Try
    End Sub
    ' Don't Wait Response --> bWait = False
    'Enter Bootloader  C7-41, NEO/IDG GR Command, Also same to AR ?
    '5669564F746563683200C7410000ACF3
    Public Overridable Sub SystemSetToBootloaderMode(Optional ByVal nTimeout As Integer = ClassDevRS232.m_cnTX_TIMEOUT_IN_SEC)
        Dim strFuncName As String = "SystemSetToBootloaderMode"
        Dim arrayByteSend As Byte() = Nothing
        Dim arrayByteRecv As Byte() = Nothing
        Dim u16Cmd As UInt16 = &HC741
        Try

            System_Customized_Command(u16Cmd, strFuncName, arrayByteSend, arrayByteRecv, AddressOf PreProcessCbRecv, nTimeout, True, False)

            m_bIsRetStatusOK = True
            'If (bIsRetStatusOK) Then

            'Else

            'End If
        Catch ex As Exception
            LogLn("[Err] " + strFuncName + " () = " + ex.Message)
        Finally
            m_bIDGCmdBusy = False
            If (arrayByteSend IsNot Nothing) Then
                Erase arrayByteSend
            End If
            If (arrayByteRecv IsNot Nothing) Then
                Erase arrayByteRecv
            End If
        End Try
    End Sub
    '
    ' Wait Response --> bWait = True
    'Enter Bootloader  C7-16, NEO/IDG GR Command, Also same to AR ?
    'Tx: 5669564F746563683200C716000077AD
    'Rx: 5669564F746563683200C7000000866E
    Public Overridable Sub SystemBootloaderToAppMode(Optional ByVal nTimeout As Integer = ClassDevRS232.m_cnTX_TIMEOUT_IN_SEC)
        Dim strFuncName As String = "SystemBootloaderToAppMode"
        Dim arrayByteSend As Byte() = Nothing
        Dim arrayByteRecv As Byte() = Nothing
        Dim u16Cmd As UInt16 = &HC716
        Try

            System_Customized_Command(u16Cmd, strFuncName, arrayByteSend, arrayByteRecv, AddressOf PreProcessCbRecv, nTimeout, True, False)

            If (bIsRetStatusOK) Then

            Else

            End If
        Catch ex As Exception
            LogLn("[Err] " + strFuncName + " () = " + ex.Message)
        Finally
            m_bIDGCmdBusy = False
            If (arrayByteSend IsNot Nothing) Then
                Erase arrayByteSend
            End If
            If (arrayByteRecv IsNot Nothing) Then
                Erase arrayByteRecv
            End If
        End Try
    End Sub
    '
    '
    'Purpose:  C7-2F Get Admin Key Status = 0x00 : None; 0x01 : Exist; 0xFF; Exhausted
    Public Overridable Sub SystemSREDGetAdminKeyStatus(ByRef byteSts As Byte, Optional ByVal nTimeout As Integer = ClassDevRS232.m_cnTX_TIMEOUT_IN_SEC)
        Dim strFuncName As String = "SystemSREDGetAdminKeyStatus"
        Dim arrayByteSend As Byte() = Nothing
        Dim arrayByteRecv As Byte() = {byteSts}
        Dim u16Cmd As UInt16 = &HC72F
        Try

            System_Customized_Command(u16Cmd, strFuncName, arrayByteSend, arrayByteRecv, AddressOf PreProcessCbRecv, nTimeout, True, False)

            If (bIsRetStatusOK) Then
                If (m_tagIDGCmdDataInfo.bIsRawData) Then
                    m_tagIDGCmdDataInfo.ParserDataGetByte(byteSts, 0)
                End If
            Else
                'Error Handling for NEO Protocol Level
            End If
        Catch ex As Exception
            LogLn("[Err] " + strFuncName + " () = " + ex.Message)
        Finally
            m_bIDGCmdBusy = False
            If (arrayByteSend IsNot Nothing) Then
                Erase arrayByteSend
            End If
            If (arrayByteRecv IsNot Nothing) Then
                Erase arrayByteRecv
            End If
        End Try
    End Sub
    '

    '==============================================================
    Public Enum ENUM_SEPM_B7_BEEP_INDICATOR
        x00_BEEP_NO = &H0
        x01_BEEP_1B ' = &H21
        x02_BEEP_2B '= &H22
        x03_BEEP_3SB ' = &H23
        x04_BEEP_4SB ' = &H24
        x05_BEEP_1LB_200ms
        x06_BEEP_1LB_400ms
        x07_BEEP_1LB_600ms
        x08_BEEP_1LB_800ms
    End Enum
    '
    Public Enum ENUM_CARD_TYPE As Byte
        CARDx00_NOT_DETECT = &H0
        CARDx01_14443_TYPE_A_SUPPORT_14443_4 '=1
        CARDx02_14443_TYPE_B_SUPPORT_14443_4 '=2
        CARDx03_MiFARE_TYPE_A '=3
        CARDx04_MiFARE_TYPE_B '=4
        CARDx05_14443_TYPE_A_NOT_SUPPORT_14443_4 '=5
        CARDx06_14443_TYPE_B_NOT_SUPPORT_14443_4 '=6
        CARDx07_14443_TYPE_A_AND_MiFARE_NFC_PHONE '=7
        CARDx08_RCTIF_BPRIME
        CARDx09_RCTIF_A
        CARDx0A_RCTIF_B_OR_NFC_PROTOCOL_TYPE_A
        CARDx0B_NFC_PROTOCOL_TYPE_B
        CARDx0C_NFC_PROTOCOL_TYPE_C
        CARDx0D_NFC_PROTOCOL_TYPE_D

        CARDx20_Contact_ICC_Card = &H20
        CARDx21_SAM1 '=&H21
        CARDx22_SAM2 '=&H22
        CARDx23_SAM3_IF_SUPPORTED ' =&H23
        CARDx24_SAM4_IF_SUPPORTED ' =&H24
    End Enum
    '
    Protected m_listCardType As Dictionary(Of ENUM_CARD_TYPE, String)
    '
    Protected Sub PT_SystemSetEnhancePassthroughMode_Init()
        If (m_listCardType IsNot Nothing) Then
            m_listCardType.Clear()
        End If
        '
        'Table 53: Enhanced Poll for Token Data Field for Response Frame, p174, GR 2.0.0
        m_listCardType = New Dictionary(Of ENUM_CARD_TYPE, String) From {
                 {ENUM_CARD_TYPE.CARDx00_NOT_DETECT, "Card not"},
                {ENUM_CARD_TYPE.CARDx01_14443_TYPE_A_SUPPORT_14443_4, "ISO 14443 Type A (Supports ISO 14443-4 Protocol) Card"},
                {ENUM_CARD_TYPE.CARDx02_14443_TYPE_B_SUPPORT_14443_4, "ISO 14443 Type B (Supports ISO 14443-4 Protocol) Card"},
                {ENUM_CARD_TYPE.CARDx03_MiFARE_TYPE_A, "Mifare Type A (Standard) Card"},
                {ENUM_CARD_TYPE.CARDx04_MiFARE_TYPE_B, "Mifare Type A (Ultralight) Card"},
                {ENUM_CARD_TYPE.CARDx05_14443_TYPE_A_NOT_SUPPORT_14443_4, "ISO 14443 Type A (Does not support ISO 14443-4 Protocol) Card"},
                {ENUM_CARD_TYPE.CARDx06_14443_TYPE_B_NOT_SUPPORT_14443_4, "ISO 14443 Type B (Does not support ISO 14443-4 Protocol) Card"},
                {ENUM_CARD_TYPE.CARDx07_14443_TYPE_A_AND_MiFARE_NFC_PHONE, "ISO 14443 Type A and Mifare (NFC phone) Card"},
                {ENUM_CARD_TYPE.CARDx20_Contact_ICC_Card, "Contact (ICC) Card"},
                {ENUM_CARD_TYPE.CARDx21_SAM1, "SAM 1"},
                {ENUM_CARD_TYPE.CARDx22_SAM2, "SAM 2"},
                {ENUM_CARD_TYPE.CARDx23_SAM3_IF_SUPPORTED, "SAM 3"},
                {ENUM_CARD_TYPE.CARDx24_SAM4_IF_SUPPORTED, "SAM 4"}
                }
        '
    End Sub
    '
    Public ReadOnly Property propGetCC_CL_CardType(byteCardType As Byte) As String
        Get
            If (Not m_listCardType.ContainsKey(byteCardType)) Then
                Return ("N/A , Card Type Value= " + byteCardType.ToString())
            End If
            '
            Return m_listCardType.Item(byteCardType) + " detected"
        End Get
    End Property
    '
    Protected Sub PT_SystemSetEnhancePassthroughMode_Cleanup()
        m_listCardType.Clear()
    End Sub
    '
    Public Overridable Sub SystemSetPassthroughMode(Optional ByVal byteModeOn As Boolean = True, Optional ByVal nTimeout As Integer = ClassDevRS232.m_cnTX_TIMEOUT_IN_SEC)
        '
        Dim u16Cmd As UInt16 = &H2C01
        Dim strFuncName As String = "SystemSetPassthroughMode"
        Dim arrayByteSend As Byte() = Nothing
        Dim arrayByteRecv As Byte() = Nothing

        If (m_bIDGCmdBusy) Then
            Return
        End If
        m_bIDGCmdBusy = True

        Try
            ClassTableListMaster.ConvertValue2ArrayByte(byteModeOn, arrayByteSend)

            System_Customized_Command(u16Cmd, strFuncName, arrayByteSend, arrayByteRecv, AddressOf PreProcessCbRecv, nTimeout, True)

            If (bIsRetStatusOK) Then

            Else

            End If
        Catch ex As Exception

        Finally
            m_bIDGCmdBusy = False
            If (arrayByteSend IsNot Nothing) Then
                Erase arrayByteSend
            End If
            If (arrayByteRecv IsNot Nothing) Then
                Erase arrayByteRecv
            End If
        End Try
    End Sub

    '

    '
    Public Overridable Sub GRExclusive_SystemHardwareInfo(ByRef listHWInfo As List(Of String), Optional ByVal nTimeout As Integer = ClassDevRS232.m_cnTX_TIMEOUT_IN_SEC)
        Try

        Catch ex As Exception
            LogLn("[Err] () = " + ex.Message)
        Finally

        End Try
    End Sub
    '
    Public Overridable Sub SystemPing(Optional ByVal nTimeout As Integer = ClassDevRS232.m_cnTX_TIMEOUT_IN_SEC)
        '
        Dim u16Cmd As UInt16 = &H1801
        Dim strFuncName As String = "SystemPing"
        Dim arrayByteSend As Byte() = Nothing
        Dim arrayByteRecv As Byte() = Nothing

        If (m_bIDGCmdBusy) Then
            Return
        End If
        m_bIDGCmdBusy = True

        Try
            'ClassTableListMaster.ConvertValue2ArrayByte(byteModeOn, arrayByteSend)

            System_Customized_Command(u16Cmd, strFuncName, arrayByteSend, arrayByteRecv, AddressOf PreProcessCbRecv, nTimeout)

            If (bIsRetStatusOK) Then

            Else

            End If
        Catch ex As Exception

        Finally
            If (arrayByteSend IsNot Nothing) Then
                Erase arrayByteSend
            End If
            If (arrayByteRecv IsNot Nothing) Then
                Erase arrayByteRecv
            End If

            m_bIDGCmdBusy = False
        End Try
    End Sub
    '
    '
    Public Overridable Sub SystemBLModeGetVersion(ByRef o_strMsg As String, Optional ByVal nTimeout As Integer = ClassDevRS232.m_cnTX_TIMEOUT_IN_SEC)
        '
        Dim u16Cmd As UInt16 = &HC710
        Dim strFuncName As String = "SystemBLModeGetVersion"
        Dim arrayByteSend As Byte() = Nothing
        Dim arrayByteRecv As Byte() = Nothing
        '
        o_strMsg = "N/A"
        '
        If (m_bIDGCmdBusy) Then
            Return
        End If
        m_bIDGCmdBusy = True

        Try
            'ClassTableListMaster.ConvertValue2ArrayByte(byteModeOn, arrayByteSend)

            System_Customized_Command(u16Cmd, strFuncName, arrayByteSend, arrayByteRecv, AddressOf PreProcessCbRecv, nTimeout)

            If (bIsRetStatusOK) Then
                If (m_tagIDGCmdDataInfo.bIsRawData) Then

                    m_tagIDGCmdDataInfo.ParserDataGetArrayRawData(arrayByteRecv)
                    '
                    ClassTableListMaster.ConvertFromHexToAscii(arrayByteRecv, o_strMsg, True)
                End If
            End If

            If (o_strMsg.Equals("")) Then
                o_strMsg = "[Err][BL Mode] Get Bootloader Version Error"
            End If
        Catch ex As Exception

        Finally
            If (arrayByteSend IsNot Nothing) Then
                Erase arrayByteSend
            End If
            If (arrayByteRecv IsNot Nothing) Then
                Erase arrayByteRecv
            End If

            m_bIDGCmdBusy = False
        End Try
    End Sub

    '
    Public Overridable Sub SystemSetPollMode(ByVal bPollMode As Boolean, Optional ByVal nTimeout As Integer = ClassDevRS232.m_cnTX_TIMEOUT_IN_SEC)
        Try

        Catch ex As Exception
            LogLn("[Err] SystemSetPollMode() = " + ex.Message)
        Finally

        End Try
    End Sub

    '
    Public Enum ENUM_BUZZER_TYPE As Byte
        SHORT_BEEP = &H1
        LONG_BEEP = &H2
    End Enum
    Public Enum ENUM_SB_TYPE As Byte
        NUM1 = &H1
        NUM2 'H2
        NUM3 'H3
        NUM4 'H4
    End Enum
    Public Enum ENUM_LB_TYPE As Byte
        MS200 = &H0 ' duration = 200ms
        MS400 'H1 ' duration = 400ms
        MS600 'H2 ' duration =  600ms
        MS800 'H3 'duration =  800ms
    End Enum



    Public Sub System_Customized_Command(ByVal u16Cmd As UInt16, ByVal strFuncName As String, ByVal arrayByteSendData As Byte(), ByRef o_arrayByteRecv As Byte(), callbackExternalRecvProcedure As ClassDevRS232.DelegatecallbackExternalRecvProcedure, Optional ByVal nTimeout As Integer = ClassDevRS232.m_cnTX_TIMEOUT_IN_SEC, Optional bCRCMSB As Boolean = False, Optional _
                                         bWait As Boolean = True)
        'Dim strFuncName As String = "System_Customized_Command"
        '

        Try
            m_bIsRetStatusOK = False

            m_tagIDGCmdDataInfo.Reinit(u16Cmd, m_arraybyteSendBuf, Me, , m_arraybyteSendBufUnknown)

            'Add Send Data
            If (arrayByteSendData IsNot Nothing) Then
                m_tagIDGCmdDataInfo.ComposerAddData(arrayByteSendData)
            End If

            '-----------------------------------------------------------------[ Send to reader ]-----------------------------------------------------------------
            'Compose 1)Protocol Header 2) Command Set 3) Length 4) Command Frame CRC Data
            PktComposerCommandFrameHeaderCmdSetDataLengthCRCData(m_tagIDGCmdDataInfo, bCRCMSB)

            '----------------[ Debug Section ]----------------
            If (m_bDebug) Then
                'Note: After PktComposerCommandFrameHeaderCmdSetDataLengthCRCData(), m_tagIDGCmdDataInfo.m_nDataSizeSend includes m_nPktHeader + m_nPktTrail
                'm_refwinformMain.LogLnDumpData(m_tagIDGCmdDataInfo.m_refarraybyteData, tagIDGCmdData.m_nPktHeader + m_tagIDGCmdDataInfo.m_nDataSizeSend + tagIDGCmdData.m_nPktTrail)
                Form01_Main.LogLnDumpData(m_tagIDGCmdDataInfo.m_refarraybyteData.Skip(m_tagIDGCmdDataInfo.m_nDataRecvOffSkipBytes).ToArray(), m_tagIDGCmdDataInfo.m_nDataSizeRecv)
            End If

            '----------------[ End Debug Section ]----------------
            'Waiting For Response in SetConfigurationsGroupSingleCbRecv()
            If (callbackExternalRecvProcedure Is Nothing) Then
                callbackExternalRecvProcedure = AddressOf ClassReaderProtocolBasic.PreProcessCbRecv
            End If

            m_refdevConn.SendCommandFrameAndWaitForResponseFrame(callbackExternalRecvProcedure, m_tagIDGCmdDataInfo, m_nIDGCommanderType, bWait, nTimeout * 1000)

            'Its' response code = 0 => OK
            If (m_tagIDGCmdDataInfo.bIsResponseOK) Then
                m_bIsRetStatusOK = True
            End If

            '----------------------[ ParserDataGetArrayRawData() Already done for necessary stuffs ]-------------
            'If (o_arrayByteRecv IsNot Nothing) Then
            '    If (m_tagIDGCmdDataInfo.bIsRawData) Then
            '        m_tagIDGCmdDataInfo.ParserDataGetArrayRawData(o_arrayByteRecv)
            '    Else
            '        'Set First byte as 0 
            '        o_arrayByteRecv(0) = &H0
            '    End If
            'Else
            '    m_tagIDGCmdDataInfo.ParserDataGetArrayRawData(o_arrayByteRecv)
            'End If

            m_tagIDGCmdDataInfo.ParserDataGetArrayRawData(o_arrayByteRecv)

        Catch ext As TimeoutException
            LogLn("[Err][Timeout] [IDG: " + GetIDGCmdString(u16Cmd) + "] " + strFuncName + "() = " + ext.Message)
        Catch ex As Exception
            LogLn("[Err] [IDG:  " + GetIDGCmdString(u16Cmd) + "] " + strFuncName + "() = " + ex.Message)
            Throw 'ex
        Finally

        End Try
    End Sub
    '
    Public Sub System_Customized_Command_DirectSend(ByVal strFuncName As String, ByVal arrayByteSendData As Byte(), ByRef o_arrayByteRecv As Byte(), ByRef o_u32RxLen As UInt32, callbackExternalRecvProcedure As ClassDevRS232.DelegatecallbackExternalRecvProcedure, Optional ByVal nTimeout As Integer = ClassDevRS232.m_cnTX_TIMEOUT_IN_SEC, Optional bCRCMSB As Boolean = False, Optional _
                                         bWait As Boolean = True)
        'Dim strFuncName As String = "System_Customized_Command"
        '
        'Dim u32RxLen As UInt32 = 0
        Try
            m_bIsRetStatusOK = False

            '----------------[ Debug - Printout Tx Data ]----------------
            If (m_bDebug) Then
                Form01_Main.LogLnDumpData(arrayByteSendData, arrayByteSendData.Length)
            End If

            'If (o_arrayByteRecv IsNot Nothing) Then
            '    u32RxLen = o_arrayByteRecv.Length
            'End If
            m_refdevConn.SendCommandFrameAndWaitForResponseFrame_Pure(callbackExternalRecvProcedure, arrayByteSendData, o_arrayByteRecv, o_u32RxLen, bWait, nTimeout * 1000)

            'Its' response code = 0 => OK
            If (o_u32RxLen > 0) Then
                m_bIsRetStatusOK = True
            End If

        Catch ext As TimeoutException
            LogLn("[Err][Timeout] DirectSend " + strFuncName + "() = " + ext.Message)
        Catch ex As Exception
            LogLn("[Err]  DirectSend " + +strFuncName + "() = " + ex.Message)
            Throw 'ex
        Finally

        End Try
    End Sub



    '
    Public Overridable Sub ExGetRecvStatusCodeByte(ByRef o_byteStatus As Byte)
        o_byteStatus = m_tagIDGCmdDataInfo.byteGetResponseCode()
    End Sub
    Public Overridable Sub ExGetRecvStatusCodeString(ByRef o_strStatus As String)
        o_strStatus = m_tagIDGCmdDataInfo.strGetResponseMsg()
    End Sub

    Enum enumModuleVersionMode As Byte
        ENUM_GET_00_ALL_READER_VARIABLES = 0
        ENUM_GET_01_PRODUCT_TYPE
        ENUM_GET_02_PROCESSOR_TYPE
        ENUM_GET_03_MAIN_FIRMWARE_VERSION
        ENUM_GET_04_FIRMWARE_SUBSYSTEM_SUITE
        ENUM_GET_06_SERIAL_PROTOCOL_SUITE = 6
        ENUM_GET_07_LAYER_1_PAYPASS_VERSION
        ENUM_GET_08_LAYER_1_ANTI_COLLISION_RESOLUTION_VERSION
        ENUM_GET_09_RFU
        ENUM_GET_0A_GET_LAYER_2_CARD_APPLICATION_SUITE
        ENUM_GET_0B_RFU
        ENUM_GET_0C_GET_USER_EXPERIENCE_SUITE
        ENUM_GET_0E_GET_SYSTEM_INFORMATION_SUITE = &HE
        ENUM_GET_0F_GET_PATCH_VERSION_NUMBER
        ENUM_GET_11_GET_MISCELLANEOUS_AND_PROPRIETARY_VERSION_STRINGS = &H11
    End Enum

    '----------------------------[This for AR currently, 2014 - Oct - 3, IDG AR V2.1.3 ]--------------------
    Public Overridable Sub SystemGetFirmwareModuleVersion(ByVal nMVM As enumModuleVersionMode, ByRef o_strFwVersionList As List(Of String), Optional ByVal nTimeout As Integer = ClassDevRS232.m_cnTX_TIMEOUT_IN_SEC)
        Dim strFuncName As String = ""

        LogLn("[Warning] Not Implement Yet " + strFuncName)
    End Sub
    '2017 Sept 21, Support ViVOpayV3 protocol
    Public Shared Function IsOkWhileCRCCheckFrameResponseV3(ByRef refIDGCmdData As tagIDGCmdData) As Boolean
        Dim u16ResponseFrameCRCCompute As UInt16
        Dim u16ResponseFrameCRCTrail As UInt16
        Dim nCRCPos As Integer
        Dim strMsg As String = ""

        With refIDGCmdData
            nCRCPos = .m_nDataRecvOffSkipBytes + .m_nDataSizeRecv - tagIDGCmdData.m_nPktTrail

            If (nCRCPos < tagIDGCmdData.m_nPktHeader) Then
                Return False
            End If
            'Calculate CRC of Response Frame (Header + Data)
            u16ResponseFrameCRCCompute = ClassReaderProtocolBasic.IDGCmdCRCEvaluate(.m_refarraybyteData, .m_nDataRecvOffSkipBytes, nCRCPos)

            'strMsg = String.Format("Eval CRC =  {0,4:X4}", u16ResponseFrameCRCCompute)
            'LogLnThreadSafe(strMsg)
            If ((nCRCPos + 1) >= .m_refarraybyteData.Length) Then
                'Out of Range --> Possible DUT Off / Unplugged
                Return False
            End If
            'Get Response Frame CRC, Big-Endian
            u16ResponseFrameCRCTrail = CUShort((CType(.m_refarraybyteData(nCRCPos), UInt16) << 8 And &HFF00) + (CType(.m_refarraybyteData(nCRCPos + 1), UInt16) And &HFF))
            'strMsg = String.Format("Trail CRC = {0,4:X4}", u16ResponseFrameCRCTrail)
            'LogLnThreadSafe(strMsg)

            'Compare with each other
            If u16ResponseFrameCRCCompute <> u16ResponseFrameCRCTrail Then
                Return False
            End If

            'LogLnThreadSafe("PreProcessCbRecv() Read 6")
            '
            .m_nResponse = .m_refarraybyteData(.m_nDataRecvOffSkipBytes + ClassReaderProtocolBasic.EnumIDGCmdOffset.Protocol3_OffSet_Rx_RetStatusCode_LSB_Len_1B)

            'Before advancing .m_nDataRecvOffSkipBytes2 + .m_nDataRecvOffSkipBytes2_PrefixGarbage , 
            'we should dump something if possible.
            If (.m_nDataRecvOffSkipBytes2_PrefixGarbage < .m_nDataRecvOffSkipBytes) Then

                ClassTableListMaster.ConvertFromHexToAscii(.m_refarraybyteData,
   .m_nDataRecvOffSkipBytes2_PrefixGarbage, .m_nDataRecvOffSkipBytes - .m_nDataRecvOffSkipBytes2_PrefixGarbage, strMsg)

                'Don't use Form01_Main .LogLn() since UI Control in Main Thread , this funciton be invoked by RS232 event thread
                Form01_Main.GetInstance().DumpingRecvAsciiStr(strMsg)
            End If

            'Advanced RecvOffSkipBytes to the pos after the found packet
            .m_nDataRecvOffSkipBytes = nCRCPos + 2
            .m_nDataRecvOffSkipBytes2_PrefixGarbage = .m_nDataRecvOffSkipBytes

            '
            .m_bIsCRCChkDone = True
        End With
        '
        Return True
    End Function
    '
    '
    Public Shared Function IsOkWhileCRCCheckFrameResponse_CRCIgnored(ByRef refIDGCmdData As tagIDGCmdData) As Boolean
        Dim u16ResponseFrameCRCCompute As UInt16
        Dim u16ResponseFrameCRCTrail As UInt16
        Dim nCRCPos As Integer
        Dim strMsg As String = ""

        With refIDGCmdData
            nCRCPos = .m_nDataRecvOffSkipBytes + .m_nDataSizeRecv - tagIDGCmdData.m_nPktTrail

            If (False) Then
                If (nCRCPos < tagIDGCmdData.m_nPktHeader) Then
                    'Response No data / timeout --> nCRCPos < 0.
                    Return False
                End If
                'Calculate CRC of Response Frame (Header + Data)
                u16ResponseFrameCRCCompute = ClassReaderProtocolBasic.IDGCmdCRCEvaluate(.m_refarraybyteData, .m_nDataRecvOffSkipBytes, nCRCPos)

                'strMsg = String.Format("Eval CRC =  {0,4:X4}", u16ResponseFrameCRCCompute)
                'LogLnThreadSafe(strMsg)
                If ((nCRCPos + 1) >= .m_refarraybyteData.Length) Then
                    'Out of Range --> Possible DUT Off / Unplugged
                    Return False
                End If
                'Get Response Frame CRC, Big-Endian
                u16ResponseFrameCRCTrail = CUShort((CType(.m_refarraybyteData(nCRCPos), UInt16) << 8 And &HFF00) + (CType(.m_refarraybyteData(nCRCPos + 1), UInt16) And &HFF))
                'strMsg = String.Format("Trail CRC = {0,4:X4}", u16ResponseFrameCRCTrail)
                'LogLnThreadSafe(strMsg)

                'Compare with each other
                If u16ResponseFrameCRCCompute <> u16ResponseFrameCRCTrail Then
                    Return False
                End If
            Else

            End If
            'LogLnThreadSafe("PreProcessCbRecv() Read 6")
            '
            .m_nResponse = .m_refarraybyteData(.m_nDataRecvOffSkipBytes + ClassReaderProtocolBasic.EnumIDGCmdOffset.Protocol2_OffSet_Rx_RetStatusCode_LSB_Len_1B)

            'Before advancing .m_nDataRecvOffSkipBytes2 + .m_nDataRecvOffSkipBytes2_PrefixGarbage , 
            'we should dump something if possible.
            If (.m_nDataRecvOffSkipBytes2_PrefixGarbage < .m_nDataRecvOffSkipBytes) Then

                ClassTableListMaster.ConvertFromHexToAscii(.m_refarraybyteData,
   .m_nDataRecvOffSkipBytes2_PrefixGarbage, .m_nDataRecvOffSkipBytes - .m_nDataRecvOffSkipBytes2_PrefixGarbage, strMsg)

                'Don't use Form01_Main .LogLn() since UI Control in Main Thread , this funciton be invoked by RS232 event thread
                Form01_Main.GetInstance().DumpingRecvAsciiStr(strMsg)
            End If

            'Advanced RecvOffSkipBytes to the pos after the found packet
            .m_nDataRecvOffSkipBytes = nCRCPos + 2
            .m_nDataRecvOffSkipBytes2_PrefixGarbage = .m_nDataRecvOffSkipBytes

            '
            .m_bIsCRCChkDone = True
        End With
        '
        Return True
    End Function
    '
    Public Shared Function IsOkWhileCRCCheckFrameResponseV2(ByRef refIDGCmdData As tagIDGCmdData) As Boolean
        Dim u16ResponseFrameCRCCompute As UInt16
        Dim u16ResponseFrameCRCTrail As UInt16
        Dim nCRCPos As Integer
        Dim strMsg As String = ""

        With refIDGCmdData
            nCRCPos = .m_nDataRecvOffSkipBytes + .m_nDataSizeRecv - tagIDGCmdData.m_nPktTrail

            If (nCRCPos < tagIDGCmdData.m_nPktHeader) Then
                'Response No data / timeout --> nCRCPos < 0.
                Return False
            End If
            'Calculate CRC of Response Frame (Header + Data)
            u16ResponseFrameCRCCompute = ClassReaderProtocolBasic.IDGCmdCRCEvaluate(.m_refarraybyteData, .m_nDataRecvOffSkipBytes, nCRCPos)

            'strMsg = String.Format("Eval CRC =  {0,4:X4}", u16ResponseFrameCRCCompute)
            'LogLnThreadSafe(strMsg)
            If ((nCRCPos + 1) >= .m_refarraybyteData.Length) Then
                'Out of Range --> Possible DUT Off / Unplugged
                Return False
            End If
            'Get Response Frame CRC, Big-Endian
            u16ResponseFrameCRCTrail = CUShort((CType(.m_refarraybyteData(nCRCPos), UInt16) << 8 And &HFF00) + (CType(.m_refarraybyteData(nCRCPos + 1), UInt16) And &HFF))
            'strMsg = String.Format("Trail CRC = {0,4:X4}", u16ResponseFrameCRCTrail)
            'LogLnThreadSafe(strMsg)

            'Compare with each other
            If u16ResponseFrameCRCCompute <> u16ResponseFrameCRCTrail Then
                Return False
            End If

            'LogLnThreadSafe("PreProcessCbRecv() Read 6")
            '
            .m_nResponse = .m_refarraybyteData(.m_nDataRecvOffSkipBytes + ClassReaderProtocolBasic.EnumIDGCmdOffset.Protocol2_OffSet_Rx_RetStatusCode_LSB_Len_1B)

            'Before advancing .m_nDataRecvOffSkipBytes2 + .m_nDataRecvOffSkipBytes2_PrefixGarbage , 
            'we should dump something if possible.
            If (.m_nDataRecvOffSkipBytes2_PrefixGarbage < .m_nDataRecvOffSkipBytes) Then

                ClassTableListMaster.ConvertFromHexToAscii(.m_refarraybyteData,
   .m_nDataRecvOffSkipBytes2_PrefixGarbage, .m_nDataRecvOffSkipBytes - .m_nDataRecvOffSkipBytes2_PrefixGarbage, strMsg)

                'Don't use Form01_Main .LogLn() since UI Control in Main Thread , this funciton be invoked by RS232 event thread
                Form01_Main.GetInstance().DumpingRecvAsciiStr(strMsg)
            End If

            'Advanced RecvOffSkipBytes to the pos after the found packet
            .m_nDataRecvOffSkipBytes = nCRCPos + 2
            .m_nDataRecvOffSkipBytes2_PrefixGarbage = .m_nDataRecvOffSkipBytes

            '
            .m_bIsCRCChkDone = True
        End With
        '
        Return True
    End Function


    Private Shared m_s_bIsIDGCRCSkipped As Boolean = False
    '
    Public Shared Property bIsIDGCmdCrcChk_Skipped As Boolean
        Set(value As Boolean)
            m_s_bIsIDGCRCSkipped = value
        End Set
        Get
            Return m_s_bIsIDGCRCSkipped
        End Get
    End Property
    '
    Public Shared Function IsOkWhileCRCCheckFrameResponse(ByRef refIDGCmdData As tagIDGCmdData) As Boolean

        If (m_s_bIsIDGCRCSkipped) Then
            IsOkWhileCRCCheckFrameResponse_CRCIgnored(refIDGCmdData)
            Return True
        End If

        If (refIDGCmdData.bIsV2Proctl) Then
            Return IsOkWhileCRCCheckFrameResponseV2(refIDGCmdData)
        End If
        '
        If (refIDGCmdData.bIsV3Proctl) Then
            Return IsOkWhileCRCCheckFrameResponseV3(refIDGCmdData)
        End If
        '
        Return False
    End Function

    '
    'This is use Protocol2 @ GR to receive
    '[Note][Params]
    ' bRS232CommOK = True --> RS232 Communication Good
    '                                 = False --> RS232 Communication Failed
    '[Description]
    'Command --> To Reader; Response --> To Terminal
    'Command Frame: "ViVOtech2" &H00 <Cmd:1 byte,10><Sub-Cmd: 1, 11><DataLen MSB: 1, 12><DataLen LSB: 1, 13><Data: 14~14+n-1><CRC LSB: 1, 14+n><CRC MSB: 1, 14+n+1>
    'Response Frame: "ViVOtech2" &H00 <Cmd:1 byte,10><StatusCode: 1, 11><DataLen MSB: 1, 12><DataLen LSB: 1, 13><Data: 14~14+n-1><CRC MSB: 1, 14+n><CRC LSB: 1, 14+n+1>

    Private Shared Sub PreProcessCbRecv_NormalMode(ByRef devSerialPort As SerialPort, ByRef refIDGCmdData As tagIDGCmdData, ByVal bRS232CommOK As Boolean)
        'method V02, -) Audio Jack Garbage check, +) Garbage collection.
            PreProcessCbRecv_NormalMode_V2(devSerialPort, refIDGCmdData, bRS232CommOK)
    End Sub
    '
    Private Shared m_s_tagIDGCmdData_Previous As tagIDGCmdData = New tagIDGCmdData(&H0, Nothing, Nothing)
    '
    Private Shared Sub PreProcessCbRecv_NormalMode_V2(ByRef devSerialPort As SerialPort, ByRef refIDGCmdData As tagIDGCmdData, ByVal bRS232CommOK As Boolean)
        Dim nDataLen As Integer
        Dim nDataLen2 As Integer
        Dim proto2ResponseStatusCode As Byte

        Try

            'LogLnThreadSafe("PPCB:001")
            nDataLen = devSerialPort.BytesToRead 'find number of bytes in buf

            'LogLnThreadSafe("PPCB:002")

            ' Boundary Checking to prevent from buffer overflow
            If (nDataLen > (refIDGCmdData.m_refarraybyteData.Count - refIDGCmdData.m_nDataRecvOff)) Then
                nDataLen = refIDGCmdData.m_refarraybyteData.Count - refIDGCmdData.m_nDataRecvOff
            End If

            '2017 July 21, goofy_liu
            'To prevent Protocol's Tail Unknown Data , here we only receive
            'Ex: 
            'Waiting to read data: <CRC,2B>#*(@(#...
            'Previous in-buf data: ViVOtech2<NULL>18010000, 
            'Found: appending "<CRC,2B>#*(@(#..." to in-buf data is not good, ONLY <CRC,2B> missed in Rx Frame
            'by check refIDGCmdData.m_nDataSizeRecv value - refIDGCmdData.m_nDataRecvOff

            'Prevent from null length read
            If (nDataLen = 0) Then
                Return
            End If

            nDataLen2 = devSerialPort.Read(refIDGCmdData.m_refarraybyteData, refIDGCmdData.m_nDataRecvOff, nDataLen)
            If (nDataLen < nDataLen2) Then
                If (nDataLen2 < 0) Then
                    'LogLnThreadSafe("[Error] SystemPingCbRecv() = Invalid Port.Read Bytes ( " & nDataLen2 & " )")
                    Return
                Else
                    'LogLnThreadSafe("[Warning] SystemPingCbRecv() = BytesToRead( " & nDataLen & ") < Port.Read Bytes ( " & nDataLen2 & " ) => Risk Point : recv buffer overflow.")
                End If
            End If
            'LogLnThreadSafe("PPCB:003")

            'Compute Read Data Accumulation Length
            refIDGCmdData.m_nDataRecvOff = refIDGCmdData.m_nDataRecvOff + nDataLen2
            'LogLnThreadSafe("PPCB:004")

            'LogLnThreadSafe("PPCB:006")

            If (Not refIDGCmdData.bIsVHdrReadyRawSizeReady) Then
                'TODO or Deprecated, 2017 July 26, No need to move out garbage.
                'If (Not IsProtoclHdrRemoveAheadGarbage(refIDGCmdData)) Then
                Return
                'End If
            End If


            refIDGCmdData.m_byteCmdRx = refIDGCmdData.m_refarraybyteData(refIDGCmdData.m_nDataRecvOffSkipBytes + ClassReaderProtocolBasic.EnumIDGCmdOffset.Protocol2_OffSet_CmdMain_Len_1B)

            'If Command not ready
            If (Not refIDGCmdData.bIsTxRxCmdEqualed) Then
                refIDGCmdData.Reinit_Common_MemValsOnly()

                'If (Not refIDGCmdData.Truncate1stPkt()) Then
                Return
                'End If
                'Continue While
            End If

            'Check FULL Recv Packet is available then read it
            'For Ex:
            ' ViVOtech2<NULL><CmdSub,2B><DataLen,2B,MSB><Data1>..,missed part of them
            ' --> return directly since not full of packet
            If refIDGCmdData.m_nDataRecvOff < refIDGCmdData.m_nDataSizeRecv Then
                'LogLnThreadSafe("[Warning] SystemPingCbRecv() =  Recv Packet Length is available ( " & refIDGCmdData.m_nDataSizeRecv & ") BUT not Full ( Bytes Read =  " & refIDGCmdData.m_nDataRecvOff)
                Return
            End If

            'LogLnThreadSafe("PPCB:007")

            ' Now We Check Response CRC Validation
            With refIDGCmdData
                If .bIsRecvDone Then
                    If Not IsOkWhileCRCCheckFrameResponse(refIDGCmdData) Then
                        LogLnThreadSafe("[Err][IDG Cmd CRC] PreProcessCbRecv() = Response Frame CRC Failed")
                        Return
                    End If

                    'Check Interrupt Command present
                    CalledByPreProcessCbRecv_InterruptByExternalStatusUpdate(refIDGCmdData)

                    'LogLnThreadSafe("PPCB:008")
                    '-------------------[ Get Response Status Code ]-------------------'


                    'LogLnThreadSafe("PPCB:0081")
                    proto2ResponseStatusCode = CType(.m_nResponse, Byte)
                    'LogLnThreadSafe("PPCB:0082")
                    If Not m_dictStatusCode2.ContainsKey(proto2ResponseStatusCode) Then
                        'LogLnThreadSafe("PPCB:0083")
                        'm_refwinformMain.LogLnDumpData(.m_refarraybyteData, .m_nDataRecvOffSkipBytes + .m_nDataSizeRecv) '<-- Don't Do Dump Function in Recv Thread since Dump Function is not thread Safe Function
                        'LogLnThreadSafe("PPCB:0084")
                        Dim strCMDId As String = String.Format("{0,4:X4}", refIDGCmdData.m_u16CmdSet)
                        LogLnThreadSafe("[Waring] PreProcessCbRecv() = Unknown Response (Cmd : " + strCMDId + ") Status Code = " & .m_nResponse)
                        Return
                    End If
                    'LogLnThreadSafe("PPCB:009")


                    ' Printout Response Message
                    'LogLnThreadSafe(m_dictStatusCode2.Item(proto2ResponseStatusCode))
                    'Done
                    'LogLnThreadSafe("PreProcessCbRecv() Read 7")
                Else
                    Dim pMainInst As Form01_Main = Form01_Main.GetInstance()
                    If (pMainInst.bIsDebug) Then
                        LogLnThreadSafe("[Warning] Receiving Data is continued, Wanted Rx Data Len = " & refIDGCmdData.m_nDataSizeRecv & ",Totally Rx Off Len = " & refIDGCmdData.m_nDataRecvOff)
                    End If
                End If
            End With
            'LogLnThreadSafe("PPCB:0010")
        Catch ex As Exception
            LogLnThreadSafe("[Recv Normal Mode - Err] [IDG:  " + GetIDGCmdString(refIDGCmdData.m_u16CmdSet) + "] " + ex.Message)
        Finally
            refIDGCmdData.ParserDataUpdateBeforeRetrieve()
        End Try
    End Sub
  
    '=================[ For Stress Test ]=================
    '[IN] refIDGCmdData.m_refarraybyteData  must Already contains Completely Rx Data
    Public Shared Sub PreProcessCbRecv_NormalMode_StressTest(ByRef refIDGCmdData As tagIDGCmdData)
        Dim nDataLen As Integer
        'Dim nDataLen2 As Integer
        Dim proto2ResponseStatusCode As Byte
        Dim nOffRetSts As ClassReaderProtocolBasic.EnumIDGCmdOffset = ClassReaderProtocolBasic.EnumIDGCmdOffset.Protocol2_OffSet_Rx_RetStatusCode_LSB_Len_1B
        Dim nOffMain As ClassReaderProtocolBasic.EnumIDGCmdOffset = ClassReaderProtocolBasic.EnumIDGCmdOffset.Protocol2_OffSet_CmdMain_Len_1B

        '2017 Sept 22, for ViVOpayV3 protocol
        If (refIDGCmdData.bIsV3Proctl) Then
            nOffRetSts = ClassReaderProtocolBasic.EnumIDGCmdOffset.Protocol3_OffSet_Rx_RetStatusCode_LSB_Len_1B
            nOffMain = ClassReaderProtocolBasic.EnumIDGCmdOffset.Protocol3_OffSet_CmdMain_Len_1B
        End If

        Try

            'LogLnThreadSafe("PPCB:001")
            'nDataLen = devSerialPort.BytesToRead 'find number of bytes in buf
            nDataLen = refIDGCmdData.m_nDataSizeRecv
            'Prevent from garbage call
            If (nDataLen = 0) Then
                Return
            End If
            refIDGCmdData.m_nDataSizeRecv = 0
            'LogLnThreadSafe("PPCB:002")


            With refIDGCmdData
                'Compute Read Data Accumulation Length
                .m_nDataRecvOff = nDataLen
                'LogLnThreadSafe("PPCB:004")

                While (True)
                    'For Special case 20160308, add this loop to process 1st packet is extra garbage 
                    ' --> Remove 1st packet --> Process again by this while loop
                    '==================[ In Case Mobile Reader  Audio Jack Tx Garbage]=================
                    If (Not PreProcessCbRecv_NormalMode_Filter_PrefixGarbage_AudioJack_2_RS232(refIDGCmdData)) Then
                        '============= [ TODO ] ================
                        'Error , 
                        Dim bErr As Boolean
                        bErr = False
                        Return
                    End If
                    '

                    If (Not .bIsVHdrReadyRawSizeReady) Then
                        Exit While
                    End If
                    .m_nDataSizeRecv = .m_nDataRecvOff ' Make Sure Data Size

                    .m_nResponse = .m_refarraybyteData(.m_nDataRecvOffSkipBytes + nOffRetSts) 'ClassReaderProtocolBasic.EnumIDGCmdOffset.Protocol2_OffSet_RetStatusCode_LSB_Len_1B)

                    proto2ResponseStatusCode = CType(.m_nResponse, Byte)

                    'Check Recv Raw Data Length is available then read it
                    If .m_nDataRecvOff < tagIDGCmdData.m_nPktHeader Then
                        'LogLnThreadSafe("[Warning] SystemPingCbRecv() = Recv Raw Data Length not read, read OFF Len = " & refIDGCmdData.m_nDataRecvOff & " < " & tagIDGCmdData.m_nPktHeader)
                        Return
                    End If
                    'LogLnThreadSafe("PPCB:005")

                    '2016-03-08 added. RecvOffSkip Bytes for Skipping Preamble of Audio Jack Requirement
                    .m_byteCmdRx = .m_refarraybyteData(nOffMain + .m_nDataRecvOffSkipBytes) 'ClassReaderProtocolIDGAR.EnumIDGCmdOffset.Protocol2_OffSet_CmdMain_Len_1B + .m_nDataRecvOffSkipBytes)

                    If (Not .bIsVHdrReadyRawSizeReady) Then
                        Return
                    End If
                    '>>>>>>>>>>>>>[ 2016-03-08 ]<<<<<<<<<<<<<
                    ' For Vendi V1.00.040 Failed Case
                    ' Test Item "Keypad Test"
                    ' Get Button Config ,  Bushund Log Info.
                    'C >> 56 69 56 4f  74 65 63 68  32 00 f0 f5  00 00 08 f
                    ' R << 56 69 56 4f  74 65 63 68  32 00 02 08  00 00
                    ' R << 56 69 56 4f  74 65 63 68  32 00 f0 00  00 03
                    ' R << 01 01 0f 8f  24 20 01
                    ' To Make Sure the we have received the correct response packet
                    If (Not .bIsTxRxCmdEqualed) Then
                        If (Not .Truncate1stPkt()) Then
                            Return
                        End If
                        Continue While
                    End If
                    '
                    Exit While
                End While

                'LogLnThreadSafe("PPCB:006")

                'Check FULL Recv Packet is available then read it
                If .m_nDataRecvOff < .m_nDataSizeRecv Then
                    'LogLnThreadSafe("[Warning] SystemPingCbRecv() =  Recv Packet Length is available ( " & refIDGCmdData.m_nDataSizeRecv & ") BUT not Full ( Bytes Read =  " & refIDGCmdData.m_nDataRecvOff)
                    Return
                End If
            End With 'End With of refIDGCmdData


            'LogLnThreadSafe("PPCB:007")

            ' Now We Check Response CRC Validation
            With refIDGCmdData
                If .bIsRecvDone Then
                    If Not IsOkWhileCRCCheckFrameResponse(refIDGCmdData) Then
                        LogLnThreadSafe("[Err][IDG Cmd CRC] PreProcessCbRecv() = Response Frame CRC Failed")
                        Return
                    End If

                    'Check Interrupt Command present
                    CalledByPreProcessCbRecv_InterruptByExternalStatusUpdate(refIDGCmdData)

                    'LogLnThreadSafe("PPCB:008")
                    '-------------------[ Get Response Status Code ]-------------------'
                    'LogLnThreadSafe("PreProcessCbRecv() Read 6")
                    .m_nResponse = .m_refarraybyteData(.m_nDataRecvOffSkipBytes + nOffRetSts) 'ClassReaderProtocolBasic.EnumIDGCmdOffset.Protocol2_OffSet_RetStatusCode_LSB_Len_1B)
                    'LogLnThreadSafe("PPCB:0081")
                    proto2ResponseStatusCode = CType(.m_nResponse, Byte)
                    'LogLnThreadSafe("PPCB:0082")
                    If Not m_dictStatusCode2.ContainsKey(proto2ResponseStatusCode) Then
                        'LogLnThreadSafe("PPCB:0083")
                        'm_refwinformMain.LogLnDumpData(.m_refarraybyteData, .m_nDataRecvOffSkipBytes + .m_nDataSizeRecv) '<-- Don't Do Dump Function in Recv Thread since Dump Function is not thread Safe Function
                        'LogLnThreadSafe("PPCB:0084")
                        Dim strCMDId As String = String.Format("{0,4:X4}", refIDGCmdData.m_u16CmdSet)
                        LogLnThreadSafe("[Waring] PreProcessCbRecv() = Unknown Response (Cmd : " + strCMDId + ") Status Code = " & .m_nResponse)
                        Return
                    End If
                    'LogLnThreadSafe("PPCB:009")

                    ' Printout Response Message
                    'LogLnThreadSafe(m_dictStatusCode2.Item(proto2ResponseStatusCode))
                    'Done
                    'LogLnThreadSafe("PreProcessCbRecv() Read 7")
                Else
                    LogLnThreadSafe("[Warning] Receiving Data is continued, Recv Data Len = " & refIDGCmdData.m_nDataSizeRecv & ",Recv Off Len = " & refIDGCmdData.m_nDataRecvOff)
                End If
            End With
            'LogLnThreadSafe("PPCB:0010")
        Catch ex As Exception
            LogLnThreadSafe("[Recv Normal Mode - Err] [IDG:  " + GetIDGCmdString(refIDGCmdData.m_u16CmdSet) + "] " + ex.Message)
        End Try
    End Sub
    '=================[ End of  Stress Test ]=================
    '
    Private Shared Sub PreProcessCbRecv_DumpingMode(ByRef devSerialPort As SerialPort, ByRef refIDGCmdData As tagIDGCmdData, ByVal bRS232CommOK As Boolean)
        Dim nDataLen As Integer
        Dim nDataLen2 As Integer
        Dim strDumpTmp As String = ""
        Dim pMainInst As Form01_Main = Form01_Main.GetInstance()

        '
        Try
            nDataLen = devSerialPort.BytesToRead 'find number of bytes in buf
            'Prevent from garbage call
            If (nDataLen = 0) Then
                Return

            End If
            ' Boundary Checking to prevent from buffer overflow
            If (nDataLen > refIDGCmdData.m_refarraybyteData.Count) Then
                nDataLen = refIDGCmdData.m_refarraybyteData.Count
            End If

            nDataLen2 = devSerialPort.Read(refIDGCmdData.m_refarraybyteData, 0, nDataLen)
            If (nDataLen < nDataLen2) Then
                If (nDataLen2 < 0) Then
                    'LogLnThreadSafe("[Error] SystemPingCbRecv() = Invalid Port.Read Bytes ( " & nDataLen2 & " )")
                    Return
                Else
                    'LogLnThreadSafe("[Warning] SystemPingCbRecv() = BytesToRead( " & nDataLen & ") < Port.Read Bytes ( " & nDataLen2 & " ) => Risk Point : recv buffer overflow.")
                End If
            End If
            '
            'Dumping It Now
            ClassTableListMaster.ConvertFromArrayByte2String(refIDGCmdData.m_refarraybyteData, 0, nDataLen2, strDumpTmp)
            '2016 April 24(Mon) - Remove Auto Poll+Burst Mode Dumpping Message to haste TS
            'LogLnThreadSafe("[Hex Data]=" + strDumpTmp)                                                                                             
            DumpingRecvHexStr(strDumpTmp)


            ClassTableListMaster.ConvertFromHexToAscii(refIDGCmdData.m_refarraybyteData, 0, nDataLen2, strDumpTmp)
            'LogLnThreadSafe("[ASCII Data]=" + strDumpTmp)
            'LogLnThreadSafe("-----------[End of Dumping Mode]-----------")
            DumpingRecv(strDumpTmp)

        Catch ex As Exception
            LogLnThreadSafe("[Recv Dumping Mode ] [Err] [IDG:  " + GetIDGCmdString(refIDGCmdData.m_u16CmdSet) + "] " + ex.Message)
        End Try
    End Sub

    '
    Protected Shared Sub PreProcessCbRecv(ByRef devSerialPort As SerialPort, ByRef refIDGCmdData As tagIDGCmdData, ByVal bRS232CommOK As Boolean)

        'Dim devInterface As Object = Nothing
        'Select Case (m_nDevIntf)
        '    Case ENU_DEV_INTERFACE.INTF_RS232
        '        devInterface = CType(devSerialPort, SerialPort)
        '    Case ENU_DEV_INTERFACE.INTF_USBHID
        '        'Since ClassDevUSBHID.Send() function already received all data, No need to do this

        '        Return
        'End Select
        If (refIDGCmdData.bIsIDGCmderBusy) Then
            'In IDG Command Active Mode, Do Normal IDG Processing
            PreProcessCbRecv_NormalMode(devSerialPort, refIDGCmdData, bRS232CommOK)
        Else
            'Not in IDG Command Active Mode, Do Data Dumping Mode
            PreProcessCbRecv_DumpingMode(devSerialPort, refIDGCmdData, bRS232CommOK)
        End If
    End Sub
    '
    Protected Shared Sub PreProcessCbRecvV1(ByRef devSerialPort As SerialPort, ByRef refIDGCmdData As tagIDGCmdData, ByVal bRS232CommOK As Boolean)

        'Dim devInterface As Object = Nothing
        'Select Case (m_nDevIntf)
        '    Case ENU_DEV_INTERFACE.INTF_RS232
        '        devInterface = CType(devSerialPort, SerialPort)
        '    Case ENU_DEV_INTERFACE.INTF_USBHID
        '        'Since ClassDevUSBHID.Send() function already received all data, No need to do this

        '        Return
        'End Select
        If (refIDGCmdData.bIsIDGCmderBusy) Then
            'In IDG Command Active Mode, Do Normal IDG Processing

            PreProcessCbRecv_NormalModeV1(devSerialPort, refIDGCmdData, bRS232CommOK)

        Else
            'Not in IDG Command Active Mode, Do Data Dumping Mode
            PreProcessCbRecv_DumpingModeV1(devSerialPort, refIDGCmdData, bRS232CommOK)
        End If
    End Sub
    '
    'TODO: V1 Protocol Response Frame Type 'A' (i.e. ACK) and Data Frame Received Procedure
    'Is Not Added. It must be implemented for future requirement
    Private Shared Sub PreProcessCbRecv_NormalModeV1(ByRef devSerialPort As SerialPort, ByRef refIDGCmdData As tagIDGCmdData, ByVal bRS232CommOK As Boolean)
        Dim nDataLen As Integer
        Dim nDataLen2 As Integer
        Dim bLoop As Boolean = True 'First Time set it as True to enter the loop

        Dim proto1ResponseStatusCode As Byte

        Try

            'LogLnThreadSafe("PPCB:001")
            nDataLen = devSerialPort.BytesToRead 'find number of bytes in buf
            'Prevent from garbage call
            If (nDataLen = 0) Then
                Return
            End If
            'LogLnThreadSafe("PPCB:002")

            ' Boundary Checking to prevent from buffer overflow
            If (nDataLen > (refIDGCmdData.m_refarraybyteData.Count - refIDGCmdData.m_nDataRecvOff)) Then
                nDataLen = refIDGCmdData.m_refarraybyteData.Count
            End If


            nDataLen2 = devSerialPort.Read(refIDGCmdData.m_refarraybyteData, refIDGCmdData.m_nDataRecvOff, nDataLen)
            If (nDataLen < nDataLen2) Then
                If (nDataLen2 < 0) Then
                    'LogLnThreadSafe("[Error] SystemPingCbRecv() = Invalid Port.Read Bytes ( " & nDataLen2 & " )")
                    Return
                Else
                    'LogLnThreadSafe("[Warning] SystemPingCbRecv() = BytesToRead( " & nDataLen & ") < Port.Read Bytes ( " & nDataLen2 & " ) => Risk Point : recv buffer overflow.")
                End If
            End If
            'LogLnThreadSafe("PPCB:003")
            While (bLoop)
                bLoop = False
                With refIDGCmdData

                    'Compute Read Data Accumulation Length
                    .m_nDataRecvOff = .m_nDataRecvOff + nDataLen2
                    'LogLnThreadSafe("PPCB:004")

                    'Check Response Header Length 
                    If .m_nDataRecvOff < tagIDGCmdData.m_nPktHeader Then
                        'LogLnThreadSafe("[Warning] SystemPingCbRecv() = Recv Raw Data Length not read, read OFF Len = " & refIDGCmdData.m_nDataRecvOff & " < " & tagIDGCmdData.m_nPktHeader)
                        Return
                    End If
                    'LogLnThreadSafe("PPCB:005")

                    'Check & Get Frame Type
                    Select Case refIDGCmdData.m_byteV1FrameType
                        Case EnumProtocolV1FrameType._A
                        Case EnumProtocolV1FrameType._D
                        Case EnumProtocolV1FrameType._N
                        Case EnumProtocolV1FrameType._S
                        Case EnumProtocolV1FrameType._X
                            'Protocol 1 Frame Type
                            If ((.m_byteV1FrameType = EnumProtocolV1FrameType._X) And (ClassReaderProtocolBasic.EnumIDGCmdOffset.Protocol1_OffSet_FrameType_Byte9_1B <= .m_nDataRecvOff)) Then
                                .m_byteV1FrameType = .m_refarraybyteData(ClassReaderProtocolBasic.EnumIDGCmdOffset.Protocol1_OffSet_FrameType_Byte9_1B)
                            Else
                                Return
                            End If
                    End Select

                    'Get Response Data Length
                    If .m_nDataSizeRecv = 0 Then
                        Select Case .m_byteV1FrameType
                            Case EnumProtocolV1FrameType._X
                                'Default is 2 bytes
                                .m_nDataSizeRecvRaw = 2 'Supposed
                                .m_nDataRecvCmdRespLenInBytes = 2 'Supposed
                            Case EnumProtocolV1FrameType._D
                                .m_nDataSizeRecvRaw = .m_byteV1FrameDataLen
                                .m_nDataRecvCmdRespLenInBytes = 0 ' No Cmd & Status Code in D Type
                                ' Clear it to indicate starting to receive Response Frame Type D Data, bIsV1ResponseOK will check this value as > 0
                                .m_byteV1FrameDataLen = 0
                            Case EnumProtocolV1FrameType._S
                                .m_nDataSizeRecvRaw = 4
                                .m_nDataRecvCmdRespLenInBytes = 0 ' No Cmd & Status Code in S Type
                            Case Else '_A, _N
                                'Protocol 1 Frame Type
                                .m_nDataSizeRecvRaw = 2 '2 'Protocol 1 Only has 2 bytes
                                .m_nDataRecvCmdRespLenInBytes = 2
                        End Select
                        .m_nDataSizeRecv = tagIDGCmdData.m_nPktHeaderV1 + .m_nDataRecvCmdRespLenInBytes + .m_nDataSizeRecvRaw + tagIDGCmdData.m_nPktTrail
                    End If

                    'LogLnThreadSafe("PPCB:006")

                    'Check FULL Recv Packet is available then read it
                    If .m_nDataRecvOff < .m_nDataSizeRecv Then
                        'LogLnThreadSafe("[Warning] SystemPingCbRecv() =  Recv Packet Length is available ( " & .m_nDataSizeRecv & ") BUT not Full ( Bytes Read =  " & .m_nDataRecvOff)
                        Return
                    End If

                    'LogLnThreadSafe("PPCB:007")

                    ' Now We Check Response CRC Validation
                    'With refIDGCmdData
                    If .bIsV1RecvDone Then
                        If Not IsOkWhileCRCCheckFrameResponse(refIDGCmdData) Then
                            LogLnThreadSafe("[Err][IDG Cmd CRC] PreProcessCbRecv() = Response Frame CRC Failed")
                            Return
                        End If

                        'Check Interrupt Command present
                        CalledByPreProcessCbRecv_InterruptByExternalStatusUpdate(refIDGCmdData)

                        'LogLnThreadSafe("PPCB:008")
                        '-------------------[ Get Response Status Code ]-------------------'
                        'LogLnThreadSafe("PreProcessCbRecv() Read 6")
                        Select Case .m_byteV1FrameType
                            Case EnumProtocolV1FrameType._S, EnumProtocolV1FrameType._D
                                ' Frame Type _S and _D has no response code but here we supposed that it inherits from previous frame's Response code
                                '.m_nResponse = EnumSTATUSCODE1.x00OK
                            Case Else '_A, _N, and supposed that Frame Type _X response has response code
                                'Protocol 1 Frame Type
                                proto1ResponseStatusCode = .m_refarraybyteData(ClassReaderProtocolBasic.EnumIDGCmdOffset.Protocol1_OffSet_RetStatusCode_Byte11_1B)
                                'LogLnThreadSafe("PPCB:0081")

                                'LogLnThreadSafe("PPCB:0082")
                                If Not m_dictStatusCode1.ContainsKey(proto1ResponseStatusCode) Then
                                    'LogLnThreadSafe("PPCB:0083")
                                    'm_refwinformMain.LogLnDumpData(.m_refarraybyteData, .m_nDataSizeRecv) '<-- Don't Do Dump Function in Recv Thread since Dump Function is not thread Safe Function
                                    'LogLnThreadSafe("PPCB:0084")
                                    Dim strCMDId As String = String.Format("{0,4:X4}", .m_u16CmdSet)
                                    LogLnThreadSafe("[Waring] PreProcessCbRecv() = Unknown Response (Cmd : " + strCMDId + ") Status Code = " & .m_nResponse)
                                End If

                                'While Frame Type A is replied, we have to do advanced verification
                                If (.m_byteV1FrameType = EnumProtocolV1FrameType._A) Then
                                    'Check whether there is followed data frame in A Type Frame
                                    If ((proto1ResponseStatusCode = EnumSTATUSCODE1.x00OK) And (.m_bFrameDataFollowed)) Then
                                        'Dim nOffsetFrom, nOffsetTo, nOffsetLen As Integer

                                        'Get Next Data Frame Length
                                        .m_byteV1FrameDataLen = .m_refarraybyteData(ClassReaderProtocolBasic.EnumIDGCmdOffset.Protocol1_OffSet_DataLen_Byte13_1B)

                                        'If Data Frame Response has partial received data, then move it to beginning of the received buffer
                                        If (.m_byteV1FrameDataLen > 0) Then
                                            .ReinitForV1DataFrameFollowedMode(.m_nDataSizeRecv, 0, .m_nDataRecvOff - .m_nDataSizeRecv)
                                            If (.m_nDataRecvOff > 0) Then
                                                '==================[ Note ]===================
                                                'If there are more bytes coming after 1st Response Frame, let's try to resolve it again !!
                                                'Reset received data length
                                                nDataLen = 0
                                                nDataLen2 = 0
                                                bLoop = True ' Parse _D or _S Frame Type Response Packet Again
                                            End If
                                        End If
                                    End If
                                End If
                                '==============[ Note: Don't move the following statement to upper / forward to cause unknown branching error. ]==========
                                .m_nResponse = proto1ResponseStatusCode
                        End Select
                        'LogLnThreadSafe("PPCB:009")

                        ' Printout Response Message
                        'LogLnThreadSafe(m_dictStatusCode2.Item(proto2ResponseStatusCode))
                        'Done
                        'LogLnThreadSafe("PreProcessCbRecv() Read 7")
                    Else
                        LogLnThreadSafe("[Warning] Receiving Data is continued, Recv Data Len = " & .m_nDataSizeRecv & ",Recv Off Len = " & .m_nDataRecvOff)
                    End If
                End With 'With refIDGCmdData

            End While

            'LogLnThreadSafe("PPCB:0010")
        Catch ex As Exception
            LogLnThreadSafe("[Recv Normal Mode - Err] [IDG:  " + GetIDGCmdString(refIDGCmdData.m_u16CmdSet) + "] " + ex.Message)
        End Try
    End Sub
    '
    '
    Private Shared Sub PreProcessCbRecv_DumpingModeV1(ByRef devSerialPort As SerialPort, ByRef refIDGCmdData As tagIDGCmdData, ByVal bRS232CommOK As Boolean)
        Dim nDataLen As Integer
        Dim nDataLen2 As Integer
        Dim strDumpTmp As String = ""
        '
        Try
            nDataLen = devSerialPort.BytesToRead 'find number of bytes in buf
            'Prevent from garbage call
            If (nDataLen = 0) Then
                Return

            End If
            ' Boundary Checking to prevent from buffer overflow
            If (nDataLen > refIDGCmdData.m_refarraybyteData.Count) Then
                nDataLen = refIDGCmdData.m_refarraybyteData.Count
            End If

            nDataLen2 = devSerialPort.Read(refIDGCmdData.m_refarraybyteData, 0, nDataLen)
            If (nDataLen < nDataLen2) Then
                If (nDataLen2 < 0) Then
                    'LogLnThreadSafe("[Error] SystemPingCbRecv() = Invalid Port.Read Bytes ( " & nDataLen2 & " )")
                    Return
                Else
                    'LogLnThreadSafe("[Warning] SystemPingCbRecv() = BytesToRead( " & nDataLen & ") < Port.Read Bytes ( " & nDataLen2 & " ) => Risk Point : recv buffer overflow.")
                End If
            End If
            '
            'Dumping It Now
            ClassTableListMaster.ConvertFromArrayByte2String(refIDGCmdData.m_refarraybyteData, 0, nDataLen2, strDumpTmp)
            LogLnThreadSafe("[Hex Data]=" + strDumpTmp)
            DumpingRecvHexStr(strDumpTmp)

            ClassTableListMaster.ConvertFromHexToAscii(refIDGCmdData.m_refarraybyteData, 0, nDataLen2, strDumpTmp)
            LogLnThreadSafe("[ASCII Data]=" + strDumpTmp)
            LogLnThreadSafe("-----------[End of Dumping Mode]-----------")
            '
            DumpingRecv(strDumpTmp)
        Catch ex As Exception
            LogLnThreadSafe("[Recv Dumping Mode ] [Err] [IDG:  " + GetIDGCmdString(refIDGCmdData.m_u16CmdSet) + "] " + ex.Message)
        End Try
    End Sub
    '========================================[Get System Info Area]==========================================
    '2018 Oct 02, goofy_liu
    ' remove ViVO Legacy Products
    '2017 Oct 03, based on NEO 2.0 , Rev 111 version
    'goofy_liu@idtechproducts.com.tw

    Protected Shared m_s_dictProductTypeName As Dictionary(Of UInt32, String) = New Dictionary(Of UInt32, String) From {
        {&H433600, "Vendi (NEO)"},
        {&H433900, "Kiosk3 (NEO)"},
        {&H553100, "UniPay 1.5 (NEO)"},
        {&H553300, "UniPay III (NEO) "},
        {&H553331, "VP4880, VP4880 OEM (NEO) (iBase/Cake same code)"},
        {&H553332, "VP4880E(NEO)"},
        {&H553333, "VP4880C (NEO)"},
        {&H563100, "VP3600"},
        {&H563200, "VP5200"},
        {&H563300, "VP5300"},
        {&H563400, "VP6300"},
        {&H563500, "VP6800"},
        {&H563600, "VP8300"},
        {&H563700, "VP8310"},
        {&H563800, "VP8800"},
        {&H563900, "VP8810"},
        {&H564000, "VP9000"},
        {&H564100, "VP3310"}
        }
    '2017 Oct 26,
    'Note - if o_strProductType = "Unknown Product Type" --> Incorrect String
    Public Overridable Sub SystemProductTypeGet(ByRef o_strProductType As String, Optional ByVal i_strTLVProductType As String = "", Optional ByVal nTimeout As Integer = ClassDevRS232.m_cnTX_TIMEOUT_IN_SEC)      '
        Dim u16Cmd As UInt16 = &H901
        Dim strFuncName As String = "SystemProductTypeGet"
        Dim arrayByteDataResponse As Byte() = Nothing
        Dim arrayByteSend As Byte() = Nothing
        Dim arrayByteRecv As Byte() = Nothing
        '
        If (i_strTLVProductType.Equals("")) Then
            i_strTLVProductType = tagTLV.TAG_DF60_ProductType
        End If


        If (m_bIDGCmdBusy) Then
            Return
        End If
        m_bIDGCmdBusy = True
        '
        o_strProductType = "Unknown Product Type"
        Try
            '-----------------------------------------------------------------[ Send to reader ]-----------------------------------------------------------------
            'm_tagIDGCmdDataInfo.Reinit(u16Cmd, m_arraybyteSendBuf, Me, , m_arraybyteSendBufUnknown)

            'PktComposerCommandFrameHeaderCmdSetDataLengthCRCData(m_tagIDGCmdDataInfo)

            'm_devRS232.SendCommandFrameAndWaitForResponseFrame(AddressOf ClassReaderProtocolBasic.PreProcessCbRecv, m_tagIDGCmdDataInfo, m_nIDGCommanderType, True, nTimeout * 1000)

            System_Customized_Command(u16Cmd, strFuncName, arrayByteSend, arrayByteRecv, AddressOf ClassReaderProtocolBasic.PreProcessCbRecv, nTimeout)

            If (bIsRetStatusOK) Then
                Dim dictTLVs As Dictionary(Of String, tagTLV) = New Dictionary(Of String, tagTLV)
                'Get Serial Num String
                m_tagIDGCmdDataInfo.ParserDataGetArrayRawData(arrayByteDataResponse)
                '
                If (arrayByteDataResponse IsNot Nothing) Then
                    Dim strTmp As String = ""
                    'Dim strTmp As String = tagTLV.TAG_DF61 '- 2-byte Tag, Deprecated
                    'Dim strTmp2 As String = tagTLV.TAG_DFEE61 ' New 3-byte '"DFEE61", PISCES ONLY

                    ClassReaderCommanderParser.TLV_ParseFromReaderMultipleTLVs(arrayByteDataResponse, 0, arrayByteDataResponse.Count, dictTLVs)
                    'If ((dictTLVs.Count > 0) And (dictTLVs.ContainsKey(strTmp) Or dictTLVs.ContainsKey(strTmp2))) Then
                    If ((dictTLVs.Count > 0) And dictTLVs.ContainsKey(i_strTLVProductType)) Then
                        Dim tlv As tagTLV
                        Dim u32ValProdType As UInt32
                        'If (dictTLVs.ContainsKey(strTmp2)) Then
                        '    tlv = dictTLVs.Item(strTmp2)
                        'Else
                        '    tlv = dictTLVs.Item(strTmp)
                        'End If

#If 1 Then
                        tlv = dictTLVs.Item(i_strTLVProductType)
                        u32ValProdType = tlv.u32Val
                        If (m_s_dictProductTypeName.ContainsKey(u32ValProdType)) Then
                            o_strProductType = m_s_dictProductTypeName(u32ValProdType)
                        Else
                            ClassTableListMaster.ConvertFromHexToAscii(tlv.m_arrayTLVal, strTmp, True)
                            o_strProductType = "Product Type N/A : " + o_strProductType + " Val = " + strTmp
                        End If
#Else
                        ClassTableListMaster.ConvertFromHexToAscii(tlv.m_arrayTLVal, 0, 1, strTmp, True)
                        Select Case (strTmp)
                            'Case "?"
                            Case "H" 'ZA9L0 - AR
                                o_strProductType = "Zatara?ASSP (ZA9L0)"
                            Case "J" 'ZA9L1 - AR
                                o_strProductType = "Zatara?ASSP (ZA9L1)"
                            Case "E" 'ARM 7 LPC Processors - GR
                                o_strProductType = "ARM 7 LPC Processors/ LPC21xx"
                            Case "M" 'ARM 7 LPC Processors - GR
                                o_strProductType = "ARM Cortex-M4/ K21 Family"
                            Case "K" '4B, Freescale iMX6UL, PISCES
                                o_strProductType = "ARM® Cortex®-A7 core / iMX6UL"
                            Case Else
                                'o_strProcessorType = "Unknown Processor Type"
                                o_strProductType = o_strProductType + " = " + strTmp + " (hex=" + String.Format("{0,2:X2}", tlv.m_arrayTLVal(0)) + ")"
                        End Select
#End If
                    End If

                    Erase arrayByteDataResponse
                    ClassTableListMaster.CleanupConfigurationsTLVList(dictTLVs)
                Else
                End If
            Else
            End If
        Catch ext As TimeoutException
            LogLn("[Err][Timeout] [IDG: " + GetIDGCmdString(u16Cmd) + "] " + strFuncName + "() = " + ext.Message)
        Catch ex As Exception
            LogLn("[Err] [IDG:  " + GetIDGCmdString(u16Cmd) + "] " + strFuncName + "() = " + ex.Message)
        Finally
            m_tagIDGCmdDataInfo.Cleanup()
            m_bIDGCmdBusy = False
            If (arrayByteSend IsNot Nothing) Then
                Erase arrayByteSend
            End If
            If (arrayByteRecv IsNot Nothing) Then
                Erase arrayByteRecv
            End If
        End Try

    End Sub

    '2017 Oct 26, Notes
    '1. Check o_strProcessorType = "Unknown Processor Type" for un-identify processor type.
    '
    Public Overridable Sub SystemProcessorTypeGet(ByRef o_strProcessorType As String, ByVal i_strTLVProcessor As String, Optional ByVal nTimeout As Integer = ClassDevRS232.m_cnTX_TIMEOUT_IN_SEC)      '
        Dim u16Cmd As UInt16 = &H902
        Dim strFuncName As String = "SystemProcessorTypeGet"
        Dim arrayByteDataResponse As Byte() = Nothing
        Dim arrayByteSend As Byte() = Nothing
        Dim arrayByteRecv As Byte() = Nothing


        If (m_bIDGCmdBusy) Then
            Return
        End If
        m_bIDGCmdBusy = True
        '
        o_strProcessorType = "Unknown Processor Type"
        Try
            '-----------------------------------------------------------------[ Send to reader ]-----------------------------------------------------------------
            'm_tagIDGCmdDataInfo.Reinit(u16Cmd, m_arraybyteSendBuf, Me, , m_arraybyteSendBufUnknown)

            'PktComposerCommandFrameHeaderCmdSetDataLengthCRCData(m_tagIDGCmdDataInfo)

            'm_devRS232.SendCommandFrameAndWaitForResponseFrame(AddressOf ClassReaderProtocolBasic.PreProcessCbRecv, m_tagIDGCmdDataInfo, m_nIDGCommanderType, True, nTimeout * 1000)

            System_Customized_Command(u16Cmd, strFuncName, arrayByteSend, arrayByteRecv, AddressOf ClassReaderProtocolBasic.PreProcessCbRecv, nTimeout)

            If (bIsRetStatusOK) Then
                Dim dictTLVs As Dictionary(Of String, tagTLV) = New Dictionary(Of String, tagTLV)
                Static s_strlstTag_ProcessorType() = New String() {"DF61", "DFEE61"}
                'Get Serial Num String
                m_tagIDGCmdDataInfo.ParserDataGetArrayRawData(arrayByteDataResponse)
                '
                If (arrayByteDataResponse IsNot Nothing) Then
                    Dim strTmp As String = ""
                    'Dim strTmp As String = tagTLV.TAG_DF61 '- 2-byte Tag, Deprecated
                    'Dim strTmp2 As String = tagTLV.TAG_DFEE61 ' New 3-byte '"DFEE61", PISCES ONLY


                    ClassReaderCommanderParser.TLV_ParseFromReaderMultipleTLVs(arrayByteDataResponse, 0, arrayByteDataResponse.Count, dictTLVs)

                    For Each strTagName As String In s_strlstTag_ProcessorType
                        'If ((dictTLVs.Count > 0) And (dictTLVs.ContainsKey(strTmp) Or dictTLVs.ContainsKey(strTmp2))) Then
#If 1 Then
                        If ((dictTLVs.Count > 0) And dictTLVs.ContainsKey(strTagName)) Then
                            Dim tlv As tagTLV

                            'If (dictTLVs.ContainsKey(strTmp2)) Then
                            '    tlv = dictTLVs.Item(strTmp2)
                            'Else
                            '    tlv = dictTLVs.Item(strTmp)
                            'End If
                            tlv = dictTLVs.Item(strTagName)

                            ClassTableListMaster.ConvertFromHexToAscii(tlv.m_arrayTLVal, 0, 1, strTmp, True)
                            Select Case (strTmp)
                                'Case "?"
                                Case "H" 'ZA9L0 - AR
                                    o_strProcessorType = "Zatara?ASSP (ZA9L0)"
                                Case "J" 'ZA9L1 - AR
                                    o_strProcessorType = "Zatara?ASSP (ZA9L1)"
                                Case "E" 'ARM 7 LPC Processors - GR
                                    o_strProcessorType = "ARM 7 LPC Processors/ LPC21xx"
                                Case "M" 'ARM 7 LPC Processors - GR
                                    o_strProcessorType = "ARM Cortex-M4/ K21 Family"
                                Case "K" '4B, Freescale iMX6UL, PISCES
                                    o_strProcessorType = "ARM® Cortex®-A7 core / iMX6UL"
                                Case Else
                                    'o_strProcessorType = "Unknown Processor Type"
                                    o_strProcessorType = o_strProcessorType + " = " + strTmp + " (hex=" + String.Format("{0,2:X2}", tlv.m_arrayTLVal(0)) + ")"
                            End Select
                        Else

                        End If
#Else
                        If ((dictTLVs.Count > 0) And dictTLVs.ContainsKey(i_strTLVProcessor)) Then
                            Dim tlv As tagTLV

                            'If (dictTLVs.ContainsKey(strTmp2)) Then
                            '    tlv = dictTLVs.Item(strTmp2)
                            'Else
                            '    tlv = dictTLVs.Item(strTmp)
                            'End If
                            tlv = dictTLVs.Item(i_strTLVProcessor)

                            ClassTableListMaster.ConvertFromHexToAscii(tlv.m_arrayTLVal, 0, 1, strTmp, True)
                            Select Case (strTmp)
                                'Case "?"
                                Case "H" 'ZA9L0 - AR
                                    o_strProcessorType = "Zatara?ASSP (ZA9L0)"
                                Case "J" 'ZA9L1 - AR
                                    o_strProcessorType = "Zatara?ASSP (ZA9L1)"
                                Case "E" 'ARM 7 LPC Processors - GR
                                    o_strProcessorType = "ARM 7 LPC Processors/ LPC21xx"
                                Case "M" 'ARM 7 LPC Processors - GR
                                    o_strProcessorType = "ARM Cortex-M4/ K21 Family"
                                Case "K" '4B, Freescale iMX6UL, PISCES
                                    o_strProcessorType = "ARM® Cortex®-A7 core / iMX6UL"
                                Case Else
                                    'o_strProcessorType = "Unknown Processor Type"
                                    o_strProcessorType = o_strProcessorType + " = " + strTmp + " (hex=" + String.Format("{0,2:X2}", tlv.m_arrayTLVal(0)) + ")"
                            End Select
                        Else

                        End If
#End If

                    Next
                    Erase arrayByteDataResponse
                    ClassTableListMaster.CleanupConfigurationsTLVList(dictTLVs)
                Else
                End If
            Else
            End If
        Catch ext As TimeoutException
            LogLn("[Err][Timeout] [IDG: " + GetIDGCmdString(u16Cmd) + "] " + strFuncName + "() = " + ext.Message)
        Catch ex As Exception
            LogLn("[Err] [IDG:  " + GetIDGCmdString(u16Cmd) + "] " + strFuncName + "() = " + ex.Message)
        Finally
            m_tagIDGCmdDataInfo.Cleanup()
            m_bIDGCmdBusy = False
            If (arrayByteSend IsNot Nothing) Then
                Erase arrayByteSend
            End If
            If (arrayByteRecv IsNot Nothing) Then
                Erase arrayByteRecv
            End If
        End Try

    End Sub
    '
    '-----------------------------------------------------------------------------------------------------------------
    'Compose The Packet in Final Stage 1)Protocol Header 2) Command Set 3) Length 4) Command Frame CRC Data
    'Command Set Example
    ' If calling ping command(18-01), set u16CmdSub = &H1801
    Protected Sub PktComposerCommandFrameHeaderCmdSetDataLengthCRCData(ByRef refIDGCmdData As tagIDGCmdData, Optional ByVal bCRCMSBFirst As Boolean = False)
        'Select Case (m_nIDGCommanderType)
        '    Case ClassReaderInfo.ENU_FW_PLATFORM_TYPE.NEO_IDG
        'End Select

        If (refIDGCmdData.bIsV3Proctl) Then
            PktComposerV3CommandFrameHeaderCmdSetDataLengthCRCData(refIDGCmdData, refIDGCmdData.m_u16CmdSet, bCRCMSBFirst)
            Return
        End If

        PktComposerV2CommandFrameHeaderCmdSetDataLengthCRCData(refIDGCmdData, refIDGCmdData.m_u16CmdSet, bCRCMSBFirst)
    End Sub


    Protected Sub PktComposerV2CommandFrameHeaderCmdSetDataLengthCRCData(ByRef refIDGCmdData As tagIDGCmdData, ByVal u16CmdSub As UInt16, Optional ByVal bCRCMSBFirst As Boolean = False)
        Try
            Dim strV2Hdr As String = ClassReaderProtocolBasic.s_strProtocolV2Header
            Dim nIdx As Integer
            With refIDGCmdData
                'Save Command Set
                .m_u16CmdSet = u16CmdSub

                '1)Protocol Header
                For nIdx = 0 To strV2Hdr.Count - 1
                    .m_refarraybyteData(nIdx) = Convert.ToByte(strV2Hdr(nIdx))
                Next
                .m_refarraybyteData(ClassReaderProtocolBasic.EnumIDGCmdOffset.Protocol2_OffSet_Header_NullByte_Offset) = &H0

                '2) Command Set, (10, 11), Big Endian
                .m_refarraybyteData(ClassReaderProtocolBasic.EnumIDGCmdOffset.Protocol2_OffSet_CmdMain_Len_1B) = CType((u16CmdSub >> 8) And &HFF, Byte)
                .m_refarraybyteData(ClassReaderProtocolBasic.EnumIDGCmdOffset.Protocol2_OffSet_CmdSub_Len_1B) = CType((u16CmdSub) And &HFF, Byte)

                '3) Data Length, (12, 13), Big Endian
                .m_refarraybyteData(ClassReaderProtocolBasic.EnumIDGCmdOffset.Protocol2_OffSet_DataLen_MSB_Len_1B) = CType((.m_nDataSizeSend >> 8) And &HFF, Byte)
                .m_refarraybyteData(ClassReaderProtocolBasic.EnumIDGCmdOffset.Protocol2_OffSet_DataLen_LSB_Len_1B) = CType((.m_nDataSizeSend) And &HFF, Byte)

                '4) Command Frame CRC Data
                'ClassReaderProtocolBasic.IDGCmdFrameCRCSet(.m_refarraybyteData, .m_nDataSizeSend + tagIDGCmdData.m_nPktHeader, False)
                ClassReaderProtocolBasic.IDGCmdFrameCRCSet(.m_refarraybyteData, .m_nDataSizeSend + tagIDGCmdData.m_nPktHeader, bCRCMSBFirst)

                '5) Set Packet Length (Header + Raw Data + Trail)
                .m_nDataSizeSend = tagIDGCmdData.m_nPktHeader + .m_nDataSizeSend + tagIDGCmdData.m_nPktTrail
            End With
        Catch ex As Exception
            LogLn("[Err] PktComposerV2CommandFrameHeaderCmdSetDataLengthCRCData() = " + ex.Message)
        End Try
    End Sub
    '2017 Sept 21,
    Protected Sub PktComposerV3CommandFrameHeaderCmdSetDataLengthCRCData(ByRef refIDGCmdData As tagIDGCmdData, ByVal u16CmdSub As UInt16, Optional ByVal bCRCMSBFirst As Boolean = False)
        Try
            Dim strVHdr As String = ClassReaderProtocolBasic.s_strProtocolV3Header
            Dim nIdx As Integer
            With refIDGCmdData
                'Save Command Set
                .m_u16CmdSet = u16CmdSub

                '1)Protocol Header
                For nIdx = 0 To strVHdr.Count - 1
                    .m_refarraybyteData(nIdx) = Convert.ToByte(strVHdr(nIdx))
                Next
                .m_refarraybyteData(ClassReaderProtocolBasic.EnumIDGCmdOffset.Protocol3_OffSet_Header_NullByte_Offset) = &H0

                '2) Command Set, (10, 11), Big Endian
                .m_refarraybyteData(ClassReaderProtocolBasic.EnumIDGCmdOffset.Protocol3_OffSet_Header_Len_10B) = CType((u16CmdSub >> 8) And &HFF, Byte)
                .m_refarraybyteData(ClassReaderProtocolBasic.EnumIDGCmdOffset.Protocol3_OffSet_CmdSub_Len_1B) = CType((u16CmdSub) And &HFF, Byte)

                '3) Data Length, (12, 13), Big Endian
                .m_refarraybyteData(ClassReaderProtocolBasic.EnumIDGCmdOffset.Protocol3_OffSet_DataLen_MSB_Len_1B) = CType((.m_nDataSizeSend >> 8) And &HFF, Byte)
                .m_refarraybyteData(ClassReaderProtocolBasic.EnumIDGCmdOffset.Protocol3_OffSet_DataLen_LSB_Len_1B) = CType((.m_nDataSizeSend) And &HFF, Byte)

                '4) Command Frame CRC Data
                'ClassReaderProtocolBasic.IDGCmdFrameCRCSet(.m_refarraybyteData, .m_nDataSizeSend + tagIDGCmdData.m_nPktHeader, False)
                ClassReaderProtocolBasic.IDGCmdFrameCRCSet(.m_refarraybyteData, .m_nDataSizeSend + tagIDGCmdData.m_nPktHeader, bCRCMSBFirst)

                '5) Set Packet Length (Header + Raw Data + Trail)
                .m_nDataSizeSend = tagIDGCmdData.m_nPktHeader + .m_nDataSizeSend + tagIDGCmdData.m_nPktTrail
            End With
        Catch ex As Exception
            LogLn("[Err] PktComposerV3CommandFrameHeaderCmdSetDataLengthCRCData() = " + ex.Message)
        End Try
    End Sub
    '

    '
    Public Overridable Sub SystemGetHardwareInfo(ByRef o_strFwVersionList As List(Of String), Optional ByVal nTimeout As Integer = ClassDevRS232.m_cnTX_TIMEOUT_IN_SEC)
        Dim u16Cmd As UInt16 = &H914
        Dim strFuncName As String = "SystemGetHardwareInfo"
        Dim arrayByteSend As Byte() = Nothing
        Dim arrayByteRecv As Byte() = Nothing

        If (m_bIDGCmdBusy) Then
            Return
        End If
        m_bIDGCmdBusy = True

        '
        Try
            ''Send 09-20 And Get Return Value
            'm_tagIDGCmdDataInfo.Reinit(u16Cmd, m_arraybyteSendBuf, Me, , m_arraybyteSendBufUnknown)

            ''-----------------------------------------------------------------[ Send to reader ]-----------------------------------------------------------------
            ''Compose 1)Protocol Header 2) Command Set 3) Length 4) Command Frame CRC Data
            'PktComposerCommandFrameHeaderCmdSetDataLengthCRCData(m_tagIDGCmdDataInfo, m_tagIDGCmdDataInfo.m_u16CmdSet)

            ''----------------[ Debug Section ]----------------
            'If (m_bDebug) Then
            '    'Note: After PktComposerCommandFrameHeaderCmdSetDataLengthCRCData(), m_tagIDGCmdDataInfo.m_nDataSizeSend includes m_nPktHeader + m_nPktTrail
            '    'm_refwinformMain.LogLnDumpData(m_tagIDGCmdDataInfo.m_refarraybyteData, tagIDGCmdData.m_nPktHeader + m_tagIDGCmdDataInfo.m_nDataSizeSend + tagIDGCmdData.m_nPktTrail)
            '    m_refwinformMain.LogLnDumpData(m_tagIDGCmdDataInfo.m_refarraybyteData, m_tagIDGCmdDataInfo.m_nDataSizeSend)
            'End If

            ''----------------[ End Debug Section ]----------------
            ''Waiting For Response in SetConfigurationsGroupSingleCbRecv()
            'm_devRS232.SendCommandFrameAndWaitForResponseFrame(AddressOf ClassReaderProtocolBasic.CommonCbRecv, m_tagIDGCmdDataInfo, True, nTimeout * 1000)
            System_Customized_Command(u16Cmd, strFuncName, arrayByteSend, arrayByteRecv, AddressOf ClassReaderProtocolBasic.PreProcessCbRecv, nTimeout)

            'Its' response code = 0 => OK
            If (bIsRetStatusOK) Then
                Dim arrayByteData As Byte() = Nothing


                m_tagIDGCmdDataInfo.ParserDataGetArrayRawData(arrayByteData)
                If (arrayByteData IsNot Nothing) Then

                    ClassTableListMaster.ConvertFromArrayByte2StringsList(arrayByteData, o_strFwVersionList)

                    ' Free Resource
                    Erase arrayByteData
                Else
                    o_strFwVersionList.Clear()
                End If
            Else
                LogLn("[Err] " + strFuncName + "() = Response Code (" & m_tagIDGCmdDataInfo.m_nResponse & ") --> " + m_dictStatusCode2.Item(m_tagIDGCmdDataInfo.m_nResponse))
            End If

        Catch ext As TimeoutException
            LogLn("[Err] [Timeout][IDG: " + GetIDGCmdString(u16Cmd) + "] " + strFuncName + "() = " + ext.Message)
        Catch ex As Exception
            LogLn("[Err][IDG: " + GetIDGCmdString(u16Cmd) + "] " + strFuncName + "() = " + ex.Message)
        Finally
            m_bIDGCmdBusy = False
            If (arrayByteSend IsNot Nothing) Then
                Erase arrayByteSend
            End If
            If (arrayByteRecv IsNot Nothing) Then
                Erase arrayByteRecv
            End If
        End Try
        '

    End Sub

    Private Sub SystemGetFirmwareModuleVersionSimpleTextString_Common(ByVal u16Cmd As UInt16, ByRef o_strFwVersionList As List(Of String), Optional ByVal nTimeout As Integer = ClassDevRS232.m_cnTX_TIMEOUT_IN_SEC)
        'Dim u16Cmd As UInt16 = &H920
        'Dim u16Cmd As UInt16 = &H903
        Dim strFuncName As String = "SystemGetFirmwareModuleVersionSimpleTextString"

        Dim arrayByteSend As Byte() = Nothing
        Dim arrayByteRecv As Byte() = Nothing

        If (m_bIDGCmdBusy) Then
            Return
        End If
        m_bIDGCmdBusy = True

        '
        Try
            ''Send 09-20 And Get Return Value
            'm_tagIDGCmdDataInfo.Reinit(u16Cmd, m_arraybyteSendBuf, Me, , m_arraybyteSendBufUnknown)

            ''-----------------------------------------------------------------[ Send to reader ]-----------------------------------------------------------------
            ''Compose 1)Protocol Header 2) Command Set 3) Length 4) Command Frame CRC Data
            'PktComposerCommandFrameHeaderCmdSetDataLengthCRCData(m_tagIDGCmdDataInfo, m_tagIDGCmdDataInfo.m_u16CmdSet)

            ''----------------[ Debug Section ]----------------
            'If (m_bDebug) Then
            '    'Note: After PktComposerCommandFrameHeaderCmdSetDataLengthCRCData(), m_tagIDGCmdDataInfo.m_nDataSizeSend includes m_nPktHeader + m_nPktTrail
            '    'm_refwinformMain.LogLnDumpData(m_tagIDGCmdDataInfo.m_refarraybyteData, tagIDGCmdData.m_nPktHeader + m_tagIDGCmdDataInfo.m_nDataSizeSend + tagIDGCmdData.m_nPktTrail)
            '    m_refwinformMain.LogLnDumpData(m_tagIDGCmdDataInfo.m_refarraybyteData, m_tagIDGCmdDataInfo.m_nDataSizeSend)
            'End If

            ''----------------[ End Debug Section ]----------------
            ''Waiting For Response in SetConfigurationsGroupSingleCbRecv()
            'm_devRS232.SendCommandFrameAndWaitForResponseFrame(AddressOf ClassReaderProtocolBasic.CommonCbRecv, m_tagIDGCmdDataInfo, True, nTimeout * 1000)

            System_Customized_Command(u16Cmd, strFuncName, arrayByteSend, arrayByteRecv, AddressOf ClassReaderProtocolBasic.PreProcessCbRecv, nTimeout)

            'Its' response code = 0 => OK
            If (bIsRetStatusOK) Then
                Dim arrayByteData As Byte() = Nothing


                m_tagIDGCmdDataInfo.ParserDataGetArrayRawData(arrayByteData)
                If (arrayByteData IsNot Nothing) Then

                    ClassTableListMaster.ConvertFromArrayByte2StringsList(arrayByteData, o_strFwVersionList)

                    ' Free Resource
                    Erase arrayByteData
                Else
                    o_strFwVersionList.Clear()
                End If
            Else
                LogLn("[Err] " + strFuncName + "() = Response Code (" & m_tagIDGCmdDataInfo.m_nResponse & ") --> " + m_dictStatusCode2.Item(m_tagIDGCmdDataInfo.m_nResponse))
            End If

        Catch ext As TimeoutException
            LogLn("[Err] [Timeout][IDG: " + GetIDGCmdString(u16Cmd) + "] " + strFuncName + "() = " + ext.Message)
        Catch ex As Exception
            LogLn("[Err][IDG: " + GetIDGCmdString(u16Cmd) + "] " + strFuncName + "() = " + ex.Message)
        Finally
            m_bIDGCmdBusy = False
            If (arrayByteSend IsNot Nothing) Then
                Erase arrayByteSend
            End If
            If (arrayByteRecv IsNot Nothing) Then
                Erase arrayByteRecv
            End If
        End Try
        '
    End Sub

    Private Sub SystemGetFirmwareModuleVersionSimpleTextString_Deprecated_0903(ByRef o_strFwVersionList As List(Of String), Optional ByVal nTimeout As Integer = ClassDevRS232.m_cnTX_TIMEOUT_IN_SEC)
        SystemGetFirmwareModuleVersionSimpleTextString_Common(&H903, o_strFwVersionList, nTimeout)
    End Sub
    '
    Private Sub SystemGetFirmwareModuleVersionSimpleTextString_0920(ByRef o_strFwVersionList As List(Of String), Optional ByVal nTimeout As Integer = ClassDevRS232.m_cnTX_TIMEOUT_IN_SEC)
        SystemGetFirmwareModuleVersionSimpleTextString_Common(&H920, o_strFwVersionList, nTimeout)
    End Sub
    '
    Public Overridable Sub SystemGetFirmwareModuleVersionSimpleTextString(ByRef o_strFwVersionList As List(Of String), Optional ByVal nTimeout As Integer = ClassDevRS232.m_cnTX_TIMEOUT_IN_SEC)

        SystemGetFirmwareModuleVersionSimpleTextString_0920(o_strFwVersionList, nTimeout)
        If (Not bIsRetStatusOK) Then
            SystemGetFirmwareModuleVersionSimpleTextString_Deprecated_0903(o_strFwVersionList, nTimeout)
        End If
        'Dim u16Cmd As UInt16 = &H920
        'Dim u16Cmd As UInt16 = &H903
        'Dim strFuncName As String = "SystemGetFirmwareModuleVersionSimpleTextString"

        'Dim arrayByteSend As Byte() = Nothing
        'Dim arrayByteRecv As Byte()

        ''
        'If (m_bIDGCmdBusy) Then
        '    Return
        'End If
        'm_bIDGCmdBusy = True

        ''
        'Try
        ''Send 09-20 And Get Return Value
        'm_tagIDGCmdDataInfo.Reinit(u16Cmd, m_arraybyteSendBuf, Me, , m_arraybyteSendBufUnknown)

        ''-----------------------------------------------------------------[ Send to reader ]-----------------------------------------------------------------
        ''Compose 1)Protocol Header 2) Command Set 3) Length 4) Command Frame CRC Data
        'PktComposerCommandFrameHeaderCmdSetDataLengthCRCData(m_tagIDGCmdDataInfo, m_tagIDGCmdDataInfo.m_u16CmdSet)

        ''----------------[ Debug Section ]----------------
        'If (m_bDebug) Then
        '    'Note: After PktComposerCommandFrameHeaderCmdSetDataLengthCRCData(), m_tagIDGCmdDataInfo.m_nDataSizeSend includes m_nPktHeader + m_nPktTrail
        '    'm_refwinformMain.LogLnDumpData(m_tagIDGCmdDataInfo.m_refarraybyteData, tagIDGCmdData.m_nPktHeader + m_tagIDGCmdDataInfo.m_nDataSizeSend + tagIDGCmdData.m_nPktTrail)
        '    m_refwinformMain.LogLnDumpData(m_tagIDGCmdDataInfo.m_refarraybyteData, m_tagIDGCmdDataInfo.m_nDataSizeSend)
        'End If

        ''----------------[ End Debug Section ]----------------
        ''Waiting For Response in SetConfigurationsGroupSingleCbRecv()
        'm_devRS232.SendCommandFrameAndWaitForResponseFrame(AddressOf ClassReaderProtocolBasic.CommonCbRecv, m_tagIDGCmdDataInfo, True, nTimeout * 1000)

        'System_Customized_Command(u16Cmd, strFuncName, arrayByteSend, arrayByteRecv, AddressOf ClassReaderProtocolBasic.PreProcessCbRecv, nTimeout)

        ''Its' response code = 0 => OK
        'If (bIsRetStatusOK) Then
        '    Dim arrayByteData As Byte() = Nothing


        '    m_tagIDGCmdDataInfo.ParserDataGetArrayRawData(arrayByteData)
        '    If (arrayByteData IsNot Nothing) Then

        '        ClassTableListMaster.ConvertFromArrayByte2StringsList(arrayByteData, o_strFwVersionList)

        '        ' Free Resource
        '        Erase arrayByteData
        '    Else
        '        o_strFwVersionList.Clear()
        '    End If
        'Else
        '    LogLn("[Err] " + strFuncName + "() = Response Code (" & m_tagIDGCmdDataInfo.m_nResponse & ") --> " + m_dictStatusCode2.Item(m_tagIDGCmdDataInfo.m_nResponse))
        'End If

        'Catch ext As TimeoutException
        '    LogLn("[Err] [Timeout][IDG: " + GetIDGCmdString(u16Cmd) + "] " + strFuncName + "() = " + ext.Message)
        'Catch ex As Exception
        '    LogLn("[Err][IDG: " + GetIDGCmdString(u16Cmd) + "] " + strFuncName + "() = " + ex.Message)
        'Finally
        '    m_bIDGCmdBusy = False
        '    If (arrayByteSend IsNot Nothing) Then
        '        Erase arrayByteSend
        '    End If
        '    If (arrayByteRecv IsNot Nothing) Then
        '        Erase arrayByteRecv
        '    End If
        'End Try
        ''

    End Sub
    '
    'IDG CMD:_2900
    Private Shared Sub CommonCbRecv(ByRef devSerialPort As SerialPort, ByRef o_IDGCmdData As tagIDGCmdData, ByVal bRS232CommOK As Boolean)
        Try
            ' Common Process to receive data 
            PreProcessCbRecv(devSerialPort, o_IDGCmdData, bRS232CommOK)
            '
        Catch ex As Exception
            LogLnThreadSafe("[Err] SystemGetFirmwareVersionCbRecv() = " + ex.Message)
        End Try
    End Sub


    '
    Public Overridable Sub SystemGetFirmwareVersion(ByRef o_strFwVersionList As List(Of String), Optional ByVal nTimeout As Integer = ClassDevRS232.m_cnTX_TIMEOUT_IN_SEC)
        Dim u16Cmd As UInt16 = &H2900
        Dim strFuncName As String = "SystemGetFirmwareVersion"
        Dim arrayByteSend As Byte() = Nothing
        Dim arrayByteRecv As Byte() = Nothing
        '
        If (m_bIDGCmdBusy) Then
            Return
        End If
        m_bIDGCmdBusy = True

        Try
            '
            ''Send D0-06 And Get Return Value
            'm_tagIDGCmdDataInfo.Reinit(u16Cmd, m_arraybyteSendBuf, Me, , m_arraybyteSendBufUnknown)

            ''-----------------------------------------------------------------[ Send to reader ]-----------------------------------------------------------------
            ''Compose 1)Protocol Header 2) Command Set 3) Length 4) Command Frame CRC Data
            'PktComposerCommandFrameHeaderCmdSetDataLengthCRCData(m_tagIDGCmdDataInfo, m_tagIDGCmdDataInfo.m_u16CmdSet)

            ''----------------[ Debug Section ]----------------
            'If (m_bDebug) Then
            '    m_refwinformMain.LogLnDumpData(m_tagIDGCmdDataInfo.m_refarraybyteData, m_tagIDGCmdDataInfo.m_nDataSizeSend)
            'End If

            ''----------------[ End Debug Section ]----------------
            ''Waiting For Response in SetConfigurationsGroupSingleCbRecv()
            'm_devRS232.SendCommandFrameAndWaitForResponseFrame(AddressOf ClassReaderProtocolBasic.CommonCbRecv, m_tagIDGCmdDataInfo, True, nTimeout * 1000)

            System_Customized_Command(u16Cmd, strFuncName, arrayByteSend, arrayByteRecv, AddressOf ClassReaderProtocolBasic.PreProcessCbRecv, nTimeout)
            'Its' response code = 0 => OK
            If (bIsRetStatusOK) Then
                Dim arrayByteData As Byte() = Nothing

                m_tagIDGCmdDataInfo.ParserDataGetArrayRawData(arrayByteData)
                '
                If (arrayByteData IsNot Nothing) Then
                    ClassTableListMaster.ConvertFromArrayByte2StringsList(arrayByteData, o_strFwVersionList)
                    ' Free Resource
                    Erase arrayByteData
                Else
                    o_strFwVersionList.Clear()
                End If
            Else
                LogLn("[Err] SystemGetFirmwareVersion() = Response Code (" & m_tagIDGCmdDataInfo.m_nResponse & ") --> " + m_dictStatusCode2.Item(m_tagIDGCmdDataInfo.m_nResponse))
            End If

        Catch ext As TimeoutException
            LogLn("[Err][Timeout] [IDG: " + GetIDGCmdString(u16Cmd) + "] " + strFuncName + "() = " + ext.Message)
        Catch ex As Exception
            LogLn("[Err] [IDG: " + GetIDGCmdString(u16Cmd) + "] " + strFuncName + "() = " + ex.Message)
        Finally
            m_bIDGCmdBusy = False
            If (arrayByteSend IsNot Nothing) Then
                Erase arrayByteSend
            End If
            If (arrayByteRecv IsNot Nothing) Then
                Erase arrayByteRecv
            End If
        End Try
        '

    End Sub

    '======================================================
    Public Overridable Sub SystemGetBootloaderVersion(ByRef o_strFwVersionList As List(Of String), Optional ByVal nTimeout As Integer = ClassDevRS232.m_cnTX_TIMEOUT_IN_SEC, Optional ByVal bListClr As Boolean = False)
        Dim u16Cmd As UInt16 = &H2904
        Dim strFuncName As String = "SystemGetBootloaderVersion"
        Dim arrayByteSend As Byte() = Nothing
        Dim arrayByteRecv As Byte() = Nothing
        '
        If (m_bIDGCmdBusy) Then
            Return
        End If
        m_bIDGCmdBusy = True

        Try
            '
            System_Customized_Command(u16Cmd, strFuncName, arrayByteSend, arrayByteRecv, AddressOf ClassReaderProtocolBasic.PreProcessCbRecv, nTimeout)
            'Its' response code = 0 => OK
            If (bIsRetStatusOK) Then
                Dim arrayByteData As Byte() = Nothing


                m_tagIDGCmdDataInfo.ParserDataGetArrayRawData(arrayByteData)
                '
                If (arrayByteData IsNot Nothing) Then
                    ClassTableListMaster.ConvertFromArrayByte2StringsList(arrayByteData, o_strFwVersionList, bListClr)
                    ' Free Resource
                    Erase arrayByteData
                Else
                    If (bListClr) Then
                        o_strFwVersionList.Clear()
                    End If
                End If
            Else
                LogLn("[Err] SystemGetFirmwareVersion() = Response Code (" & m_tagIDGCmdDataInfo.m_nResponse & ") --> " + m_dictStatusCode2.Item(m_tagIDGCmdDataInfo.m_nResponse))
            End If

        Catch ext As TimeoutException
            LogLn("[Err][Timeout] [IDG: " + GetIDGCmdString(u16Cmd) + "] " + strFuncName + "() = " + ext.Message)
        Catch ex As Exception
            LogLn("[Err] [IDG: " + GetIDGCmdString(u16Cmd) + "] " + strFuncName + "() = " + ex.Message)
        Finally
            m_bIDGCmdBusy = False
            If (arrayByteSend IsNot Nothing) Then
                Erase arrayByteSend
            End If
            If (arrayByteRecv IsNot Nothing) Then
                Erase arrayByteRecv
            End If
        End Try
    End Sub
    '
    '======================================================
    Public Overridable Sub SystemSerialNumberErase(ByRef o_strSerialNum As String, Optional ByVal nTimeout As Integer = ClassDevRS232.m_cnTX_TIMEOUT_IN_SEC)
        Dim u16Cmd As UInt16 = &H120F
        Dim strFuncName As String = "SystemSerialNumberErase"

        Dim arrayByteSend As Byte() = Nothing
        Dim arrayByteRecv As Byte() = Nothing

        If (m_bIDGCmdBusy) Then
            Return
        End If
        m_bIDGCmdBusy = True

        Try
            System_Customized_Command(u16Cmd, strFuncName, arrayByteSend, arrayByteRecv, AddressOf ClassReaderProtocolBasic.PreProcessCbRecv, nTimeout)

            If (bIsRetStatusOK) Then

            Else
                If (arrayByteSend IsNot Nothing) Then
                    Erase arrayByteSend
                End If
                If (arrayByteRecv IsNot Nothing) Then
                    Erase arrayByteRecv
                End If
            End If

        Catch ext As TimeoutException
            LogLn("[Err][Timeout] [IDG: " + GetIDGCmdString(u16Cmd) + "] " + strFuncName + "() = " + ext.Message)
        Catch ex As Exception
            LogLn("[Err] [IDG:  " + GetIDGCmdString(u16Cmd) + "] " + strFuncName + "() = " + ex.Message)
        Finally
            m_tagIDGCmdDataInfo.Cleanup()
            m_bIDGCmdBusy = False
        End Try
    End Sub

    Public Sub SystemGetInfo_Generic(ByVal u16Cmd As UInt16, ByRef o_strSerialNum As String, Optional ByVal nTimeout As Integer = ClassDevRS232.m_cnTX_TIMEOUT_IN_SEC)
        Dim strFuncName As String = "SystemGetInfo_Generic"

        Dim arrayByteSend As Byte() = Nothing
        Dim arrayByteRecv As Byte() = Nothing

        If (m_bIDGCmdBusy) Then
            Return
        End If
        m_bIDGCmdBusy = True

        Try
            '-----------------------------------------------------------------[ Send to reader ]-----------------------------------------------------------------
            'm_tagIDGCmdDataInfo.Reinit(u16Cmd, m_arraybyteSendBuf, Me, , m_arraybyteSendBufUnknown)

            'PktComposerCommandFrameHeaderCmdSetDataLengthCRCData(m_tagIDGCmdDataInfo)

            'm_devRS232.SendCommandFrameAndWaitForResponseFrame(AddressOf ClassReaderProtocolBasic.CommonCbRecv, m_tagIDGCmdDataInfo, True, nTimeout * 1000)

            System_Customized_Command(u16Cmd, strFuncName, arrayByteSend, arrayByteRecv, AddressOf ClassReaderProtocolBasic.PreProcessCbRecv, nTimeout)

            If (bIsRetStatusOK) Then
                'Get Serial Num String
                m_tagIDGCmdDataInfo.ParserDataGet(o_strSerialNum)
            Else
                If (arrayByteSend IsNot Nothing) Then
                    Erase arrayByteSend
                End If
                If (arrayByteRecv IsNot Nothing) Then
                    Erase arrayByteRecv
                End If
            End If

        Catch ext As TimeoutException
            LogLn("[Err][Timeout] [IDG: " + GetIDGCmdString(u16Cmd) + "] " + strFuncName + "() = " + ext.Message)
        Catch ex As Exception
            LogLn("[Err] [IDG:  " + GetIDGCmdString(u16Cmd) + "] " + strFuncName + "() = " + ex.Message)
        Finally
            m_tagIDGCmdDataInfo.Cleanup()
            m_bIDGCmdBusy = False
        End Try
    End Sub
    '

    '======================================================
    Public Overridable Sub SystemSerialNumberGet(ByRef o_strSerialNum As String, Optional ByVal nTimeout As Integer = ClassDevRS232.m_cnTX_TIMEOUT_IN_SEC)
        Dim u16Cmd As UInt16 = &H1201
        Dim strFuncName As String = "SystemSerialNumberGet"

        Dim arrayByteSend As Byte() = Nothing
        Dim arrayByteRecv As Byte() = Nothing

        If (m_bIDGCmdBusy) Then
            Return
        End If
        m_bIDGCmdBusy = True

        Try
            o_strSerialNum = ""
            '-----------------------------------------------------------------[ Send to reader ]-----------------------------------------------------------------
            'm_tagIDGCmdDataInfo.Reinit(u16Cmd, m_arraybyteSendBuf, Me, , m_arraybyteSendBufUnknown)

            'PktComposerCommandFrameHeaderCmdSetDataLengthCRCData(m_tagIDGCmdDataInfo)

            'm_devRS232.SendCommandFrameAndWaitForResponseFrame(AddressOf ClassReaderProtocolBasic.CommonCbRecv, m_tagIDGCmdDataInfo, True, nTimeout * 1000)

            System_Customized_Command(u16Cmd, strFuncName, arrayByteSend, arrayByteRecv, AddressOf ClassReaderProtocolBasic.PreProcessCbRecv, nTimeout)

            If (bIsRetStatusOK) Then
                'Get Serial Num String
                m_tagIDGCmdDataInfo.ParserDataGet(o_strSerialNum)
            Else
                If (arrayByteSend IsNot Nothing) Then
                    Erase arrayByteSend
                End If
                If (arrayByteRecv IsNot Nothing) Then
                    Erase arrayByteRecv
                End If
            End If

        Catch ext As TimeoutException
            LogLn("[Err][Timeout] [IDG: " + GetIDGCmdString(u16Cmd) + "] " + strFuncName + "() = " + ext.Message)
        Catch ex As Exception
            LogLn("[Err] [IDG:  " + GetIDGCmdString(u16Cmd) + "] " + strFuncName + "() = " + ex.Message)
        Finally
            m_tagIDGCmdDataInfo.Cleanup()
            m_bIDGCmdBusy = False
        End Try
    End Sub
    '
    Public Overridable Sub SystemSerialNumberReset(Optional ByVal nTimeout As Integer = ClassDevRS232.m_cnTX_TIMEOUT_IN_SEC)
        Dim u16Cmd As UInt16 = &H120F
        Dim strFuncName As String = "SystemSerialNumberReset"

        Dim arrayByteSend As Byte() = Nothing
        Dim arrayByteRecv As Byte() = Nothing

        If (m_bIDGCmdBusy) Then
            Return
        End If
        m_bIDGCmdBusy = True

        Try
            '-----------------------------------------------------------------[ Send to reader ]-----------------------------------------------------------------
            System_Customized_Command(u16Cmd, strFuncName, arrayByteSend, arrayByteRecv, AddressOf ClassReaderProtocolBasic.PreProcessCbRecv, nTimeout)

            If (bIsRetStatusOK) Then
                '
            Else
                If (arrayByteSend IsNot Nothing) Then
                    Erase arrayByteSend
                End If
                If (arrayByteRecv IsNot Nothing) Then
                    Erase arrayByteRecv
                End If
            End If

        Catch ext As TimeoutException
            LogLn("[Err][Timeout] [IDG: " + GetIDGCmdString(u16Cmd) + "] " + strFuncName + "() = " + ext.Message)
        Catch ex As Exception
            LogLn("[Err] [IDG:  " + GetIDGCmdString(u16Cmd) + "] " + strFuncName + "() = " + ex.Message)
        Finally
            m_tagIDGCmdDataInfo.Cleanup()
            m_bIDGCmdBusy = False
        End Try
    End Sub


    'S/N : 10~15 bytes
    '2018 May 23: update to fixed 10 bytes, for NEO 2.0
    Public Overridable Sub SystemSerialNumberSet(strSN As String, Optional ByVal nTimeout As Integer = ClassDevRS232.m_cnTX_TIMEOUT_IN_SEC)
        Dim u16Cmd As UInt16 = &H1202
        Dim strFuncName As String = "SystemSerialNumberSet"
        SystemSerialNumberSet_Generic(u16Cmd, strFuncName, strSN, nTimeout)
    End Sub
    '
    '
    Protected Sub SystemSerialNumberSet_Generic(ByVal u16Cmd As UInt16, strFuncName As String, strSN As String, Optional ByVal nTimeout As Integer = ClassDevRS232.m_cnTX_TIMEOUT_IN_SEC)

        'Set Serial Number Length = 10~15 bytes
        '2018 May 23: update to fixed 10 bytes, for NEO 2.0
        Dim arrayByteSend As Byte() = Nothing
        Dim arrayByteRecv As Byte() = Nothing


        If (m_bIDGCmdBusy) Then
            Return
        End If
        m_bIDGCmdBusy = True

        Try
            arrayByteSend = New Byte(strSN.Length - 1) {}
            ClassTableListMaster.ConvertFromAscii2HexByte(strSN, arrayByteSend, 0, False)
            '-----------------------------------------------------------------[ Send to reader ]-----------------------------------------------------------------
            System_Customized_Command(u16Cmd, strFuncName, arrayByteSend, arrayByteRecv, AddressOf ClassReaderProtocolBasic.PreProcessCbRecv, nTimeout)

            If (bIsRetStatusOK) Then
            Else
            End If

        Catch ext As TimeoutException
            LogLn("[Err][Timeout] [IDG: " + GetIDGCmdString(u16Cmd) + "] " + strFuncName + "() = " + ext.Message)
        Catch ex As Exception
            LogLn("[Err] [IDG:  " + GetIDGCmdString(u16Cmd) + "] " + strFuncName + "() = " + ex.Message)
        Finally
            If (arrayByteSend IsNot Nothing) Then
                Erase arrayByteSend
            End If
            If (arrayByteRecv IsNot Nothing) Then
                Erase arrayByteRecv
            End If
            m_tagIDGCmdDataInfo.Cleanup()
            m_bIDGCmdBusy = False
        End Try
    End Sub


    '======================================================
    Sub CallByIDGCmdTimeOutChk_SetTimeoutEvent()
        If (m_bIDGCmdBusy) Then
            m_tagIDGCmdDataInfo.CallByIDGCmdTimeOutChk_SetTimeoutEvent()
        Else
            LogLn("[Warning] CallByIDGCmdTimeOutChk_SetTimeoutEvent() = Currently No IDG Command Recv Waiting")
        End If
    End Sub

    ReadOnly Property CallByIDGCmdTimeOutChk_IsCommIDGCancelStop As Boolean
        Get
            If (m_bIDGCmdBusy) Then
                Return m_tagIDGCmdDataInfo.CallByIDGCmdTimeOutChk_IsCommIDGCancelStop
            End If
            ' LogLn("[Warning] CallByIDGCmdTimeOutChk_IsCommIDGCancelStop Property = Currently No IDG Command Running")

            Return False
        End Get
    End Property

    ReadOnly Property CallByIDGCmdTimeOutChk_IsCommStopped As Boolean
        Get
            If (m_bIDGCmdBusy) Then
                Return m_tagIDGCmdDataInfo.CallByIDGCmdTimeOutChk_IsCommStopped
            End If
            'LogLn("[Warning] CallByIDGCmdTimeOutChk_IsCommStopped Property = Currently No IDG Command Running")

            Return False
        End Get
    End Property

    ReadOnly Property CallByIDGCmdTimeOutChk_IsCommIdle As Boolean
        Get
            If (m_bIDGCmdBusy) Then
                Return m_tagIDGCmdDataInfo.CallByIDGCmdTimeOutChk_IsCommIdle
            End If
            'LogLn("[Warning] CallByIDGCmdTimeOutChk_IsCommIdle Property = Currently No IDG Command Running")

            Return False
        End Get
    End Property
    '
    ReadOnly Property CallByIDGCmdTimeOutChk_IsSendAndWait As Boolean
        Get
            If (m_bIDGCmdBusy) Then
                Return m_tagIDGCmdDataInfo.CallByIDGCmdTimeOutChk_IsCommAct
            End If
            'LogLn("[Warning] CallByIDGCmdTimeOutChk_IsCommIdle Property = Currently No IDG Command Running")

            Return False
        End Get
    End Property    '


    '[Note] if Status Code = OK (&H00) --> Check out o_arrayByteATR
    '            if Status Code <> OK (&H00) --> Check out o_strErrStatusCode
    Public Sub SystemSetEhnacePassthroughMode_CC_ATR_After_PollForTokenCC(byteCardType As ClassReaderProtocolBasic.ENUM_SEPM_B6_ICC_TYPE, ByRef o_arrayByteATR As Byte(), ByRef o_strErrStatusCode As String, Optional ByVal nTimeout As Integer = ClassDevRS232.m_cnTX_TIMEOUT_IN_SEC)
        Dim u32ErrStatusCode As UInt32 = 0
        '[Diff between "CC_ATR...()" and "ICC_ATR...()"
        ' 1. Param IN/OUT : o_strErrStatusCode <---> o_u32ErrStatusCode
        SystemSetEhnacePassthroughMode_ICC_ATR_After_PollForTokenICC(byteCardType, o_arrayByteATR, u32ErrStatusCode, nTimeout)
        If (Not bIsRetStatusOK) Then
            If (u32ErrStatusCode > 0) Then
                If (m_dictARExclusive_ContactCardATRResponseMsg.ContainsKey(u32ErrStatusCode)) Then
                    o_strErrStatusCode = m_dictARExclusive_ContactCardATRResponseMsg.Item(u32ErrStatusCode)
                Else
                    o_strErrStatusCode = "Err, Invalid ICC ATR Response Code =" + String.Format("{0,4:X4}", u32ErrStatusCode)
                End If
            Else
                o_strErrStatusCode = ""
            End If
        End If

    End Sub

    Public Overridable Sub SystemSetEhnacePassthroughMode_ICC_ATR_After_PollForTokenICC(byteCardType As ClassReaderProtocolBasic.ENUM_SEPM_B6_ICC_TYPE, ByRef o_arrayByteATR As Byte(), ByRef o_u32ErrStatusCode As UInt32, Optional ByVal nTimeout As Integer = ClassDevRS232.m_cnTX_TIMEOUT_IN_SEC)
        Dim u16Cmd As UInt16 = &H2C12
        Dim strFuncName As String = "SystemSetEhnacePassthroughMode_ICC_ATR_After_PollForTokenICC"

        'Set Serial Number Length = 15 bytes
        Dim arrayByteSend As Byte() = New Byte(0) {CType(byteCardType, Byte)}
        Dim arrayByteRecv As Byte() = Nothing


        If (m_bIDGCmdBusy) Then
            Return
        End If
        m_bIDGCmdBusy = True

        Try
            '-----------------------------------------------------------------[ Send to reader ]-----------------------------------------------------------------
            System_Customized_Command(u16Cmd, strFuncName, arrayByteSend, arrayByteRecv, AddressOf ClassReaderProtocolBasic.PreProcessCbRecv, nTimeout)

            '2017 Aug 03
            'Don't care Response is OKAY or Failed, we think it there is a ICC Card's S/N
            If (m_tagIDGCmdDataInfo.bIsRawData) Then
                m_tagIDGCmdDataInfo.ParserDataGetArrayRawData(o_arrayByteATR)
            End If

            If (bIsRetStatusOK) Then
                'Do Nothing
            Else
                'Response Data may contains 4-byte error-code
                If (m_tagIDGCmdDataInfo.bIsRawData) Then
                    Dim arrayByteData As Byte() = Nothing
                    m_tagIDGCmdDataInfo.ParserDataGetArrayRawData(arrayByteData)
                    ClassTableListMaster.ConvertFromArrayByte2UInt32(arrayByteData, o_u32ErrStatusCode)
                End If
            End If

        Catch ext As TimeoutException
            LogLn("[Err][Timeout] [IDG: " + GetIDGCmdString(u16Cmd) + "] " + strFuncName + "() = " + ext.Message)
        Catch ex As Exception
            LogLn("[Err] [IDG:  " + GetIDGCmdString(u16Cmd) + "] " + strFuncName + "() = " + ex.Message)
        Finally
            If (arrayByteSend IsNot Nothing) Then
                Erase arrayByteSend
            End If
            If (arrayByteRecv IsNot Nothing) Then
                Erase arrayByteRecv
            End If
            m_tagIDGCmdDataInfo.Cleanup()
            m_bIDGCmdBusy = False
        End Try
    End Sub
    '
    Private Shared s_dict9600to1Map As SortedDictionary(Of ClassDevRS232.EnumBUAD_RATE, ClassReaderProtocolBasic.ENUM_BUAD_RATE) = New SortedDictionary(Of ClassDevRS232.EnumBUAD_RATE, ENUM_BUAD_RATE) From {{ClassDevRS232.EnumBUAD_RATE._9600, ClassReaderProtocolBasic.ENUM_BUAD_RATE._x9600}, {ClassDevRS232.EnumBUAD_RATE._57600, ClassReaderProtocolBasic.ENUM_BUAD_RATE._x57600}, {ClassDevRS232.EnumBUAD_RATE._38400, ClassReaderProtocolBasic.ENUM_BUAD_RATE._x38400}, {ClassDevRS232.EnumBUAD_RATE._19200, ClassReaderProtocolBasic.ENUM_BUAD_RATE._x19200}, {ClassDevRS232.EnumBUAD_RATE._115200, ClassReaderProtocolBasic.ENUM_BUAD_RATE._x115200}}
    Public Overridable Sub SystemSetBuadRate(byteBuadrate2 As ClassDevRS232.EnumBUAD_RATE, Optional ByVal nTimeout As Integer = ClassDevRS232.m_cnTX_TIMEOUT_IN_SEC)
        Dim u16Cmd As UInt16 = &H3001
        Dim strFuncName As String = "SystemSetBuadRate"
        Dim byteBuadrate As ClassReaderProtocolBasic.ENUM_BUAD_RATE = s_dict9600to1Map(byteBuadrate2)
        'ClassReaderProtocolBasic.ENUM_BUAD_RATE
        'Set Serial Number Length = 15 bytes
        Dim arrayByteSend As Byte() = New Byte(0) {CType(byteBuadrate, Byte)}
        Dim arrayByteRecv As Byte() = Nothing


        If (m_bIDGCmdBusy) Then
            Return
        End If
        m_bIDGCmdBusy = True

        Try
            '-----------------------------------------------------------------[ Send to reader ]-----------------------------------------------------------------
            System_Customized_Command(u16Cmd, strFuncName, arrayByteSend, arrayByteRecv, AddressOf ClassReaderProtocolBasic.PreProcessCbRecv, nTimeout)

            If (bIsRetStatusOK) Then

            End If

        Catch ext As TimeoutException
            LogLn("[Err][Timeout] [IDG: " + GetIDGCmdString(u16Cmd) + "] " + strFuncName + "() = " + ext.Message)
        Catch ex As Exception
            LogLn("[Err] [IDG:  " + GetIDGCmdString(u16Cmd) + "] " + strFuncName + "() = " + ex.Message)
        Finally
            If (arrayByteSend IsNot Nothing) Then
                Erase arrayByteSend
            End If
            If (arrayByteRecv IsNot Nothing) Then
                Erase arrayByteRecv
            End If
            m_tagIDGCmdDataInfo.Cleanup()
            m_bIDGCmdBusy = False
        End Try
    End Sub
    '
    Public Overridable Sub SystemSetBuadRate(byteBuadrate As ClassReaderProtocolBasic.ENUM_BUAD_RATE, Optional ByVal nTimeout As Integer = ClassDevRS232.m_cnTX_TIMEOUT_IN_SEC)
        Dim u16Cmd As UInt16 = &H3001
        Dim strFuncName As String = "SystemSetBuadRate"

        'Set Serial Number Length = 15 bytes
        Dim arrayByteSend As Byte() = New Byte(0) {CType(byteBuadrate, Byte)}
        Dim arrayByteRecv As Byte() = Nothing


        If (m_bIDGCmdBusy) Then
            Return
        End If
        m_bIDGCmdBusy = True

        Try
            '-----------------------------------------------------------------[ Send to reader ]-----------------------------------------------------------------
            System_Customized_Command(u16Cmd, strFuncName, arrayByteSend, arrayByteRecv, AddressOf ClassReaderProtocolBasic.PreProcessCbRecv, nTimeout)

            If (bIsRetStatusOK) Then

            End If

        Catch ext As TimeoutException
            LogLn("[Err][Timeout] [IDG: " + GetIDGCmdString(u16Cmd) + "] " + strFuncName + "() = " + ext.Message)
        Catch ex As Exception
            LogLn("[Err] [IDG:  " + GetIDGCmdString(u16Cmd) + "] " + strFuncName + "() = " + ex.Message)
        Finally
            If (arrayByteSend IsNot Nothing) Then
                Erase arrayByteSend
            End If
            If (arrayByteRecv IsNot Nothing) Then
                Erase arrayByteRecv
            End If
            m_tagIDGCmdDataInfo.Cleanup()
            m_bIDGCmdBusy = False
        End Try
    End Sub

#Region "IDisposable Support"
    Private disposedValue As Boolean ' To detect redundant calls

    ' IDisposable
    Protected Overridable Sub Dispose(disposing As Boolean)
        If Not Me.disposedValue Then
            If disposing Then
                ' TODO: dispose managed state (managed objects).
                If (m_devRS232 IsNot Nothing) Then
                    m_devRS232.Dispose()
                End If
            End If

            ' TODO: free unmanaged resources (unmanaged objects) and override Finalize() below.
            ' TODO: set large fields to null.
        End If
        Me.disposedValue = True
    End Sub

    ' TODO: override Finalize() only if Dispose(ByVal disposing As Boolean) above has code to free unmanaged resources.
    'Protected Overrides Sub Finalize()
    '    ' Do not change this code.  Put cleanup code in Dispose(ByVal disposing As Boolean) above.
    '    Dispose(False)
    '    MyBase.Finalize()
    'End Sub

    ' This code added by Visual Basic to correctly implement the disposable pattern.
    Public Sub Dispose() Implements IDisposable.Dispose
        ' Do not change this code.  Put cleanup code in Dispose(disposing As Boolean) above.
        Dispose(True)
        GC.SuppressFinalize(Me)
    End Sub
#End Region

    'Ret = False --> Error
    Private Shared Function PreProcessCbRecv_NormalMode_Filter_PrefixGarbage_AudioJack_2_RS232(ByRef refIDGCmdData As tagIDGCmdData) As Boolean
        'Dim strTmp As String
        Dim nPos As Integer
        If refIDGCmdData.Equals(Nothing) Then
            Return False
        End If
        If (refIDGCmdData.m_refarraybyteData.Length < 1) Then
            Return False
        End If

        nPos = ClassTableListMaster.ArrayByteSubPatternIndexOf(refIDGCmdData.m_refarraybyteData, 0, ClassReaderProtocolBasic.s_arrayByteV2Header, 0, refIDGCmdData.m_nDataRecvOff)
        If (nPos < 0) Then
            Return False
        End If

        If (nPos > 0) Then
            refIDGCmdData.m_nDataRecvOff = refIDGCmdData.m_nDataRecvOff - nPos
            Array.Copy(refIDGCmdData.m_refarraybyteData, nPos, refIDGCmdData.m_refarraybyteData, 0, refIDGCmdData.m_nDataRecvOff)
        End If

        Return True
    End Function

    Public Overridable Sub SystemSetEhnacePassthroughMode_ICC_ATR_GetStatus_x60_14_Powered_Seated(byteCardType As ClassReaderProtocolBasic.ENUM_CARD_TYPE, ByRef o_byteCCStatus As Byte, Optional ByVal nTimeout As Integer = ClassDevRS232.m_cnTX_TIMEOUT_IN_SEC)
        '1.23 [NEO, ICC Functions]	Get Contact Reader Status -
        ' VP6300
        ' Cmd =  &H60-&H14, Len = 01,Type = ICC = &H20
        ' Resp = Len : 01; 
        ' Data And &H1 = &H1 ? ICC Powered : ICC Not Ready
        '      And &H2 = &H2 ? Card Seated : Card Not Seated

        'VP5300
        '2017 Sept 22 - Resp Data (0) = &H04:Found ; &H00 : N/A, Not Standard
        Dim u16Cmd As UInt16 = &H6014
        Dim strFuncName As String = "SystemSetEhnacePassthroughMode_ICC_ATR_GetStatus_x60_14_Powered_Seated"

        'Set Serial Number Length = 15 bytes
        Dim arrayByteSend As Byte() = New Byte(0) {CType(byteCardType, Byte)}
        Dim arrayByteRecv As Byte() = Nothing

        o_byteCCStatus = 0

        If (m_bIDGCmdBusy) Then
            Return
        End If
        m_bIDGCmdBusy = True

        Try
            '-----------------------------------------------------------------[ Send to reader ]-----------------------------------------------------------------
            System_Customized_Command(u16Cmd, strFuncName, arrayByteSend, arrayByteRecv, AddressOf ClassReaderProtocolBasic.PreProcessCbRecv, nTimeout)
            '
            m_bIsRetStatusOK = m_tagIDGCmdDataInfo.bIsResponseOK
            '
            If (bIsRetStatusOK) Then
                If (m_tagIDGCmdDataInfo.bIsRawData) Then
                    m_tagIDGCmdDataInfo.ParserDataGetByte(o_byteCCStatus, 0)
                End If
            End If

        Catch ext As TimeoutException
            LogLn("[Err][Timeout] [IDG: " + GetIDGCmdString(u16Cmd) + "] " + strFuncName + "() = " + ext.Message)
        Catch ex As Exception
            LogLn("[Err] [IDG:  " + GetIDGCmdString(u16Cmd) + "] " + strFuncName + "() = " + ex.Message)
        Finally
            If (arrayByteSend IsNot Nothing) Then
                Erase arrayByteSend
            End If
            If (arrayByteRecv IsNot Nothing) Then
                Erase arrayByteRecv
            End If
            m_tagIDGCmdDataInfo.Cleanup()
            m_bIDGCmdBusy = False
        End Try
    End Sub

    Sub SystemSetEhnacePassthroughMode_CC_ATR_PowerDown_x2C_18(Optional nIntfContact As ENUM_SEPM_B6_ICC_TYPE = ENUM_SEPM_B6_ICC_TYPE.TYPE_x20_ICC, Optional ByVal nTimeout As Integer = ClassDevRS232.m_cnTX_TIMEOUT_IN_SEC)
        '1.23 [NEO, ICC Functions]	Get Contact Reader Status -
        ' Cmd =  &H60-&H14, Len = 01,Type = ICC = &H20
        ' Resp = Len : 01; 
        'Data And &H1 = &H1 ? ICC Powered : ICC Not Ready
        '          And &H2 = &H2 ? Card Seated : Card Not Seated
        Dim u16Cmd As UInt16 = &H2C18
        Dim strFuncName As String = "SystemSetEhnacePassthroughMode_CC_ATR_PowerDown_x2C_18"

        'Set Serial Number Length = 15 bytes
        Dim arrayByteSend As Byte() = New Byte() {CType(nIntfContact, Byte)}
        Dim arrayByteRecv As Byte() = Nothing

        '-------[ Critical Section begin ]
        m_bIsRetStatusOK = False
        If (m_bIDGCmdBusy) Then
            Return
        End If
        m_bIDGCmdBusy = True
        '[ Critical Section begin ]---------

        Try
            '-----------------------------------------------------------------[ Send to reader ]-----------------------------------------------------------------
            System_Customized_Command(u16Cmd, strFuncName, arrayByteSend, arrayByteRecv, AddressOf ClassReaderProtocolBasic.PreProcessCbRecv, nTimeout)

        Catch ext As TimeoutException
            LogLn("[Err][Timeout] [IDG: " + GetIDGCmdString(u16Cmd) + "] " + strFuncName + "() = " + ext.Message)
        Catch ex As Exception
            LogLn("[Err] [IDG:  " + GetIDGCmdString(u16Cmd) + "] " + strFuncName + "() = " + ex.Message)
        Finally
            If (arrayByteSend IsNot Nothing) Then
                Erase arrayByteSend
            End If
            If (arrayByteRecv IsNot Nothing) Then
                Erase arrayByteRecv
            End If
            m_tagIDGCmdDataInfo.Cleanup()
            m_bIDGCmdBusy = False
        End Try
    End Sub

    '
    ' Supported Reader Type : Mobile Reader, Unipay III, Unipay 1.5
    Public Overridable Sub SystemPowerOff_Exclusive_MobileReader(Optional ByVal nTimeout As Integer = ClassDevRS232.m_cnTX_TIMEOUT_IN_SEC)
        Dim strFuncName As String = "SystemPowerOff_Exclusive_MobileReader"
        Dim arrayByteSend As Byte() = Nothing
        Dim arrayByteRecv As Byte() = Nothing
        Dim u16Cmd As UInt16 = &HF00F

        '-------[ Critical Section begin ]
        m_bIsRetStatusOK = False
        If (m_bIDGCmdBusy) Then
            Return
        End If
        m_bIDGCmdBusy = True
        '[ Critical Section begin ]---------

        Try

            System_Customized_Command(u16Cmd, strFuncName, arrayByteSend, arrayByteRecv, AddressOf PreProcessCbRecv, nTimeout, True, False)

            If (bIsRetStatusOK) Then

            Else

            End If
        Catch ex As Exception
            LogLn("[Err] " + strFuncName + " () = " + ex.Message)
        Finally

            If (arrayByteSend IsNot Nothing) Then
                Erase arrayByteSend
            End If
            If (arrayByteRecv IsNot Nothing) Then
                Erase arrayByteRecv
            End If

            m_bIDGCmdBusy = False
        End Try
    End Sub
    '
    Public Overridable Sub SystemPowerOff_Delay_PMC_Exclusive_MobileReader(Optional ByVal byte01WaitTimeSec As Byte = &H5, Optional ByVal byte02SleepTimeSec As Byte = &H14, Optional ByVal nTimeout As Integer = ClassDevRS232.m_cnTX_TIMEOUT_IN_SEC)
        Dim strFuncName As String = "SystemPowerOff_Delay_PMC_Exclusive_MobileReader"
        Dim arrayByteSend As Byte() = Nothing
        Dim arrayByteRecv As Byte() = Nothing
        Dim u16Cmd As UInt16 = &HF000

        '-------[ Critical Section begin ]
        m_bIsRetStatusOK = False
        If (m_bIDGCmdBusy) Then
            Return
        End If
        m_bIDGCmdBusy = True
        '[ Critical Section begin ]---------

        Try
            arrayByteSend = New Byte() {byte01WaitTimeSec, byte02SleepTimeSec}
            '
            System_Customized_Command(u16Cmd, strFuncName, arrayByteSend, arrayByteRecv, AddressOf PreProcessCbRecv, nTimeout, True, True)
            '
            If (bIsRetStatusOK) Then

            Else

            End If
        Catch ex As Exception
            LogLn("[Err] " + strFuncName + " () = " + ex.Message)
        Finally

            If (arrayByteSend IsNot Nothing) Then
                Erase arrayByteSend
            End If
            If (arrayByteRecv IsNot Nothing) Then
                Erase arrayByteRecv
            End If

            m_bIDGCmdBusy = False
        End Try
    End Sub
    '
    Public Overridable Sub SystemEncryptionDUKPTAdminKeyStatusGet_Silk20(ByRef o_bPresent As Byte, Optional ByVal nTimeout As Integer = ClassDevRS232.m_cnTX_TIMEOUT_IN_SEC)
        '
        Dim u16Cmd As UInt16 = &HC72F
        Dim strFuncName As String = "SystemEncryptionDUKPTAdminKeyStatusGet_Silk20"
        Dim arrayByteSend As Byte() = Nothing
        Dim arrayByteRecv As Byte() = Nothing

        'Default status :  Admin Key is Absent
        o_bPresent = &HF0

        If (m_bIDGCmdBusy) Then
            Return
        End If
        m_bIDGCmdBusy = True

        Try
            System_Customized_Command(u16Cmd, strFuncName, arrayByteSend, arrayByteRecv, AddressOf PreProcessCbRecv, nTimeout)

            If (bIsRetStatusOK) Then
                If (m_tagIDGCmdDataInfo.bIsRawData) Then
                    m_tagIDGCmdDataInfo.ParserDataGetArrayRawData(arrayByteRecv)
                    o_bPresent = arrayByteRecv(0)
                End If
            Else

            End If
        Catch ex As Exception

        Finally
            If (arrayByteSend IsNot Nothing) Then
                Erase arrayByteSend
            End If
            If (arrayByteRecv IsNot Nothing) Then
                Erase arrayByteRecv
            End If

            m_bIDGCmdBusy = False
        End Try
    End Sub
    '
    'o_bPresent_Slot2_Admin_DUKPT_Key As Byte, ByRef o_bPresent_Slot3_MAC_DUKPT_Key As Byte, ByRef o_bPresent_Slot5_Acct_DUKPT_Key
    Public Overridable Sub SystemEncryptionDUKPTAdminKeyStatusGet_Vivopay(ByRef o_bPresent_Slot2_Admin_DUKPT_Key As Byte, ByRef o_bPresent_Slot3_MAC_DUKPT_Key As Byte, ByRef o_bPresent_Slot5_Acct_DUKPT_Key As Byte, Optional ByVal nTimeout As Integer = ClassDevRS232.m_cnTX_TIMEOUT_IN_SEC)
        '
        Dim u16Cmd As UInt16 = &H8102
        Dim strFuncName As String = "SystemEncryptionDUKPTAdminKeyStatusGet_Vivopay"
        Dim arrayByteSend As Byte() = Nothing
        Dim arrayByteRecv As Byte() = Nothing

        'Default status :  Admin Key is Absent
        o_bPresent_Slot2_Admin_DUKPT_Key = &HF0

        If (m_bIDGCmdBusy) Then
            Return
        End If
        m_bIDGCmdBusy = True

        Try
            System_Customized_Command(u16Cmd, strFuncName, arrayByteSend, arrayByteRecv, AddressOf PreProcessCbRecv, nTimeout)

            If (bIsRetStatusOK) Then
                If (m_tagIDGCmdDataInfo.bIsRawData) Then
                    m_tagIDGCmdDataInfo.ParserDataGetArrayRawData(arrayByteRecv)
                    ' From NEO v1.00 Rev 90

                    ' Admin Key Slot = 2, Valid Sts = &H1
                    o_bPresent_Slot2_Admin_DUKPT_Key = arrayByteRecv(2)
                    ' Admin Key Slot = 3, Valid Sts = &H1
                    o_bPresent_Slot3_MAC_DUKPT_Key = arrayByteRecv(3)
                    ' Admin Key Slot = 5, Valid Sts = &H1
                    o_bPresent_Slot5_Acct_DUKPT_Key = arrayByteRecv(5)
                End If
            Else

            End If
        Catch ex As Exception

        Finally
            If (arrayByteSend IsNot Nothing) Then
                Erase arrayByteSend
            End If
            If (arrayByteRecv IsNot Nothing) Then
                Erase arrayByteRecv
            End If

            m_bIDGCmdBusy = False
        End Try
    End Sub
    '
    '
    Public Overridable Sub SystemEncryptionDUKPTAdminKeyEraseAll_Vivopay(Optional ByVal nTimeout As Integer = ClassDevRS232.m_cnTX_TIMEOUT_IN_SEC)
        '
        Dim u16Cmd As UInt16 = &H8300
        Dim strFuncName As String = "SystemEncryptionDUKPTAdminKeyEraseAll_Vivopay"
        Dim arrayByteSend As Byte() = Nothing
        Dim arrayByteRecv As Byte() = Nothing

        If (m_bIDGCmdBusy) Then
            Return
        End If
        m_bIDGCmdBusy = True

        Try
            System_Customized_Command(u16Cmd, strFuncName, arrayByteSend, arrayByteRecv, AddressOf PreProcessCbRecv, nTimeout)

            If (bIsRetStatusOK) Then

            Else

            End If
        Catch ex As Exception

        Finally
            If (arrayByteSend IsNot Nothing) Then
                Erase arrayByteSend
            End If
            If (arrayByteRecv IsNot Nothing) Then
                Erase arrayByteRecv
            End If

            m_bIDGCmdBusy = False
        End Try
    End Sub
    '
    ' [OUT] o_bPresent : &H0 = Admin Key is Absent; &H1 = Presents; &HFF = Exhausted
    Public Overridable Sub SystemEncryptionDUKPTAdminKeyStatusGet(ByRef o_bPresent_Slot2_Admin_DUKPT_Key As Byte, ByRef o_bPresent_Slot3_MAC_DUKPT_Key As Byte, ByRef o_bPresent_Slot5_Acct_DUKPT_Key As Byte, Optional ByVal nTimeout As Integer = ClassDevRS232.m_cnTX_TIMEOUT_IN_SEC)
        Dim bVivopay As Boolean = True
        If (bVivopay) Then
            SystemEncryptionDUKPTAdminKeyStatusGet_Vivopay(o_bPresent_Slot2_Admin_DUKPT_Key, o_bPresent_Slot3_MAC_DUKPT_Key, o_bPresent_Slot5_Acct_DUKPT_Key, nTimeout)
        Else
            SystemEncryptionDUKPTAdminKeyStatusGet_Silk20(o_bPresent_Slot2_Admin_DUKPT_Key, nTimeout)
        End If
    End Sub
    '
    Public Overridable Sub SystemEncryptionAlgoSet(byteGetShippingCfgEncAlgo As Byte, Optional ByVal nTimeout As Integer = ClassDevRS232.m_cnTX_TIMEOUT_IN_SEC)
        '
        Dim u16Cmd As UInt16 = &HC732
        Dim strFuncName As String = "SystemEncryptionAlgoSet"
        Dim arrayByteSend As Byte() = New Byte() {byteGetShippingCfgEncAlgo}
        Dim arrayByteRecv As Byte() = Nothing

        If (m_bIDGCmdBusy) Then
            Return
        End If
        m_bIDGCmdBusy = True

        Try
            System_Customized_Command(u16Cmd, strFuncName, arrayByteSend, arrayByteRecv, AddressOf PreProcessCbRecv, nTimeout)

            If (bIsRetStatusOK) Then

            Else

            End If
        Catch ex As Exception

        Finally
            If (arrayByteSend IsNot Nothing) Then
                Erase arrayByteSend
            End If
            If (arrayByteRecv IsNot Nothing) Then
                Erase arrayByteRecv
            End If

            m_bIDGCmdBusy = False
        End Try
    End Sub
    '
    Public Overridable Sub SystemEncryptionAlgoGet(ByRef o_byteGetShippingCfgEncAlgo As Byte, Optional ByVal nTimeout As Integer = ClassDevRS232.m_cnTX_TIMEOUT_IN_SEC)
        '
        Dim u16Cmd As UInt16 = &HC733
        Dim strFuncName As String = "SystemEncryptionAlgoGet"
        Dim arrayByteSend As Byte() = Nothing
        Dim arrayByteRecv As Byte() = Nothing

        'Unknown Encryption Algorithm as Default
        o_byteGetShippingCfgEncAlgo = &HFF

        If (m_bIDGCmdBusy) Then
            Return
        End If
        m_bIDGCmdBusy = True

        Try
            System_Customized_Command(u16Cmd, strFuncName, arrayByteSend, arrayByteRecv, AddressOf PreProcessCbRecv, nTimeout)

            If (bIsRetStatusOK) Then
                If (m_tagIDGCmdDataInfo.bIsRawData) Then
                    m_tagIDGCmdDataInfo.ParserDataGetArrayRawData(arrayByteRecv)
                    o_byteGetShippingCfgEncAlgo = arrayByteRecv(0)
                End If
            Else

            End If
        Catch ex As Exception

        Finally
            If (arrayByteSend IsNot Nothing) Then
                Erase arrayByteSend
            End If
            If (arrayByteRecv IsNot Nothing) Then
                Erase arrayByteRecv
            End If

            m_bIDGCmdBusy = False
        End Try
    End Sub
    '
    Public Overridable Sub SystemEncryptionEnableSet(byteEnable As Byte, Optional ByVal nTimeout As Integer = ClassDevRS232.m_cnTX_TIMEOUT_IN_SEC)
        '
        Dim u16Cmd As UInt16 = &HC736
        Dim strFuncName As String = "SystemEncryptionEnable"
        Dim arrayByteSend As Byte() = New Byte() {byteEnable}
        Dim arrayByteRecv As Byte() = Nothing

        If (m_bIDGCmdBusy) Then
            Return
        End If
        m_bIDGCmdBusy = True

        Try
            System_Customized_Command(u16Cmd, strFuncName, arrayByteSend, arrayByteRecv, AddressOf PreProcessCbRecv, nTimeout)

            If (bIsRetStatusOK) Then

            Else

            End If
        Catch ex As Exception

        Finally
            If (arrayByteSend IsNot Nothing) Then
                Erase arrayByteSend
            End If
            If (arrayByteRecv IsNot Nothing) Then
                Erase arrayByteRecv
            End If

            m_bIDGCmdBusy = False
        End Try
    End Sub
    '
    Public Overridable Sub SystemEncryptionEnableGet(ByRef o_byteEnable As Byte, Optional ByVal nTimeout As Integer = ClassDevRS232.m_cnTX_TIMEOUT_IN_SEC)
        '
        Dim u16Cmd As UInt16 = &HC737
        Dim strFuncName As String = "SystemEncryptionEnableGet"
        Dim arrayByteSend As Byte() = Nothing
        Dim arrayByteRecv As Byte() = Nothing

        If (m_bIDGCmdBusy) Then
            Return
        End If
        m_bIDGCmdBusy = True
        o_byteEnable = &H0
        '
        Try
            System_Customized_Command(u16Cmd, strFuncName, arrayByteSend, arrayByteRecv, AddressOf PreProcessCbRecv, nTimeout)

            If (bIsRetStatusOK) Then
                If (m_tagIDGCmdDataInfo.bIsRawData) Then
                    m_tagIDGCmdDataInfo.ParserDataGetArrayRawData(arrayByteRecv)
                    o_byteEnable = arrayByteRecv(0)
                End If

            Else

            End If
        Catch ex As Exception

        Finally
            If (arrayByteSend IsNot Nothing) Then
                Erase arrayByteSend
            End If
            If (arrayByteRecv IsNot Nothing) Then
                Erase arrayByteRecv
            End If

            m_bIDGCmdBusy = False
        End Try
    End Sub
    '

    Sub IDGCmdInterruptLoop(bDoLoop As Boolean)
        'Waiting Till IDG Command Stop/Cancel/Done
        While (m_bIDGCmdBusy)
            Select Case (m_nDevIntf)
                Case ENU_DEV_INTERFACE.INTF_RS232
                    'm_tagIDGCmdDataInfo.CallByIDGCmdTimeOutChk_SetTimeoutEventIDGCancelStop()
                    SystemInterruptCommand()
                Case ENU_DEV_INTERFACE.INTF_USBHID
                    m_devUSBHID.InterruptExternalLoop(True)
                    Return
            End Select

            If (Not bDoLoop) Then
                Return
            End If
            Application.DoEvents()
        End While
    End Sub

    Public Enum ENUM_NETWORK_MODE As Byte
        _x00_DHCP = &H0
        _x01_STATIC_IP
    End Enum
    '


    Public Overridable Sub SystemBeeper(Optional ByVal nTimeout As Integer = ClassDevRS232.m_cnTX_TIMEOUT_IN_SEC)
        Dim u16Cmd As UInt16 = &HF0FE
        Dim strFuncName As String = "SystemBeeper"

        If (m_bIDGCmdBusy) Then
            Return
        End If
        m_bIDGCmdBusy = True

        Try
            '-----------------------------------------------------------------[ Send to reader ]-----------------------------------------------------------------
            System_Customized_Command(u16Cmd, strFuncName, Nothing, Nothing, AddressOf ClassReaderProtocolBasic.PreProcessCbRecv, nTimeout)

            If (bIsRetStatusOK) Then

            Else

            End If

        Catch ext As TimeoutException
            LogLn("[Err][Timeout] [IDG: " + GetIDGCmdString(u16Cmd) + "] " + strFuncName + "() = " + ext.Message)
        Catch ex As Exception
            LogLn("[Err] [IDG:  " + GetIDGCmdString(u16Cmd) + "] " + strFuncName + "() = " + ex.Message)
        Finally
            m_tagIDGCmdDataInfo.Cleanup()
            m_bIDGCmdBusy = False
        End Try

    End Sub
    '
    Public Overridable Sub SystemUI_LCDPixels_TurnOn_ExclusiveVP6300(Optional ByVal nTimeout As Integer = ClassDevRS232.m_cnTX_TIMEOUT_IN_SEC)
        Dim u16Cmd As UInt16 = &HF803
        Dim strFuncName As String = "SystemUI_LCDPixels_TurnOn_ExclusiveVP6300"

        If (m_bIDGCmdBusy) Then
            Return
        End If
        m_bIDGCmdBusy = True

        Try
            '-----------------------------------------------------------------[ Send to reader ]-----------------------------------------------------------------
            System_Customized_Command(u16Cmd, strFuncName, Nothing, Nothing, AddressOf ClassReaderProtocolBasic.PreProcessCbRecv, nTimeout)

            If (bIsRetStatusOK) Then

            Else

            End If

        Catch ext As TimeoutException
            LogLn("[Err][Timeout] [IDG: " + GetIDGCmdString(u16Cmd) + "] " + strFuncName + "() = " + ext.Message)
        Catch ex As Exception
            LogLn("[Err] [IDG:  " + GetIDGCmdString(u16Cmd) + "] " + strFuncName + "() = " + ex.Message)
        Finally
            m_tagIDGCmdDataInfo.Cleanup()
            m_bIDGCmdBusy = False
        End Try

    End Sub
    '

    '
    ' Tx Packet Ex:
    '56 69 56 4F 74 65 63 68 32 00 83 05 00 18 31 00 32 30 31 00 31 32 30 00 31 33 30 00 31 00 39 00 36 36 00 4C 54 00 00 00
    Public Overridable Sub System_LCD_GUIBtnDisp(nXPos As Integer, nYPos As Integer, nW As Integer, nH As Integer, strBtnTxt As String, ByRef o_n32GraphID As UInt32, Optional ByVal nTimeout As Integer = ClassDevRS232.m_cnTX_TIMEOUT_IN_SEC)
        '
        Dim u16Cmd As UInt16 = &H8305
        Dim strFuncName As String = "ARExclusive_System_LCDCustMode_Stop, V2"
        Dim aryTxData() As Byte = Nothing
        Dim aryRxData() As Byte = Nothing
        Dim aryTmp() As Char = Nothing
        Dim strTx As String
        If (m_bIDGCmdBusy) Then
            Return
        End If
        m_bIDGCmdBusy = True

        Try
            '-----------------------------------------------------------------[ Send to reader ]-----------------------------------------------------------------
            'V2 Tx Format : X_Pos\00Y_Pos\00Width\00Height\00\31\00\36\36\00
            strTx = String.Format("{0} 00 {1} 00 {2} 00 {3} 00 31 00 39 00 36 36 00 {4} 00",
                                  ClassTableListMaster.ConvertFromAscii2HexString(nXPos.ToString()),
                                  ClassTableListMaster.ConvertFromAscii2HexString(nYPos.ToString()),
                                  ClassTableListMaster.ConvertFromAscii2HexString(nW.ToString()),
                                  ClassTableListMaster.ConvertFromAscii2HexString(nH.ToString()),
                                  ClassTableListMaster.ConvertFromAscii2HexString(strBtnTxt))

            ClassTableListMaster.TLVauleFromString2ByteArray(strTx, aryTxData)

            ' No Wait since waiting for
            System_Customized_Command(u16Cmd, strFuncName, aryTxData, aryRxData, AddressOf ClassReaderProtocolBasic.PreProcessCbRecv, nTimeout)

            o_n32GraphID = 0
            If (bIsRetStatusOK) Then
                'Get Graphic ID, 4 bytes
                If (m_tagIDGCmdDataInfo.bIsRawData) Then
                    m_tagIDGCmdDataInfo.ParserDataGet(o_n32GraphID)
                End If
            End If

        Catch ext As TimeoutException
            LogLn("[Err][Timeout] [IDG: " + GetIDGCmdString(u16Cmd) + "] " + strFuncName + "() = " + ext.Message)
        Catch ex As Exception
            LogLn("[Err] [IDG:  " + GetIDGCmdString(u16Cmd) + "] " + strFuncName + "() = " + ex.Message)
        Finally
            If (aryTxData IsNot Nothing) Then
                Erase aryTxData
            End If
            '
            If (aryRxData IsNot Nothing) Then
                Erase aryRxData
            End If
            m_tagIDGCmdDataInfo.Cleanup()
            m_bIDGCmdBusy = False
        End Try
        '
    End Sub
    '
    Public Overridable Sub SystemGetHWSPIInfo(ByRef o_SPIVerG53 As String, Optional ByVal nTimeout As Integer = ClassDevRS232.m_cnTX_TIMEOUT_IN_SEC)
        Dim u16Cmd As UInt16 = &H2906
        Dim strFuncName As String = "SystemGetHWSPIInfo"
        Dim aryTxData() As Byte = Nothing
        Dim aryRxData() As Byte = Nothing
        If (m_bIDGCmdBusy) Then
            Return
        End If
        m_bIDGCmdBusy = True

        Try
            '-----------------------------------------------------------------[ Send to reader ]-----------------------------------------------------------------
            ' No Wait since waiting for
            System_Customized_Command(u16Cmd, strFuncName, aryTxData, aryRxData, AddressOf ClassReaderProtocolBasic.PreProcessCbRecv, nTimeout)

            o_SPIVerG53 = "N/A"
            If (bIsRetStatusOK) Then
                If (m_tagIDGCmdDataInfo.bIsRawData) Then
                    m_tagIDGCmdDataInfo.ParserDataGet(o_SPIVerG53)
                End If
            End If

        Catch ext As TimeoutException
            LogLn("[Err][Timeout] [IDG: " + GetIDGCmdString(u16Cmd) + "] " + strFuncName + "() = " + ext.Message)
        Catch ex As Exception
            LogLn("[Err] [IDG:  " + GetIDGCmdString(u16Cmd) + "] " + strFuncName + "() = " + ex.Message)
        Finally
            If (aryTxData IsNot Nothing) Then
                Erase aryTxData
            End If
            '
            If (aryRxData IsNot Nothing) Then
                Erase aryRxData
            End If
            '
            m_tagIDGCmdDataInfo.Cleanup()
            m_bIDGCmdBusy = False
        End Try
    End Sub
    '


    '-------[ 2017 Jan 16, System Time Set, For PCI Design Purpose ]---------
    'Ex: mrefRdrProtocol.ARExclusive_PCI_SystemTime_Set("161231","235948","")
    'If (mrefRdrProtocol.bIsOK)
    ' Do OK Handling Here
    'Else
    ' Do Err Handling Here
    Public Overridable Sub SystemTime_Set_PCI_SRED(ByVal i_strDate_YYMMDD As String, ByVal i_strTime_HHMMSS As String, i_strMAC_Temp As String, ByRef o_byteRetCode As Byte, Optional ByVal nTimeout As Integer = ClassDevRS232.m_cnTX_TIMEOUT_IN_SEC)
        'Please read "Handling system time and RTC time Rev0.2.docx"
        '29-05 ,Non-PIC, len = 0x07, 
        'i_strDate_YYMMDD, Data in BCD = "161231" --> 0x16 0x12 0x31 = 2016 December 31;
        'i_strTime_HHMMSS, Time in BCD = "235959" --> 0x23 0x59 0x59 = 23h 59m 59s;
        'i_strMAC_Temp, MAC in hex string or Empty String (Non-PCI),
        'Using AR @ PISCES platform.

        Dim u16Cmd As UInt16 = &H2505
        Dim strFuncName As String = "SystemTime_Set_PCI_SRED"
        Dim aryDatTx As Byte() = Nothing
        Dim aryDatRx As Byte() = Nothing
        Dim aryDatTmp As Byte() = Nothing
        Dim aryDatTmp2 As Byte() = Nothing
        Dim aryDatMACLenData As Byte() = Nothing ' Len(B1)+MACData(If B1>0 & i_strMAC_Temp has MAC Data) or Len = &H0, 1 Byte
        Dim nOffset As Integer = 0

        If (m_bIDGCmdBusy) Then
            Return
        End If
        m_bIDGCmdBusy = True

        Try
            'Translate to Set Time Format
            ClassTableListMaster.TLVauleFromString2ByteArray(i_strDate_YYMMDD, aryDatTmp) 'part I
            ClassTableListMaster.TLVauleFromString2ByteArray(i_strTime_HHMMSS, aryDatRx) 'part II
            If (i_strMAC_Temp.Equals("")) Then 'Part III
                aryDatMACLenData = New Byte() {&H0}
            Else
                ClassTableListMaster.TLVauleFromString2ByteArray(i_strMAC_Temp, aryDatTmp2)
                aryDatMACLenData = New Byte(aryDatTmp2.Length) {}
                aryDatMACLenData(0) = aryDatTmp2.Length
            End If
            aryDatTx = New Byte(aryDatTmp.Length + aryDatRx.Length + aryDatMACLenData.Length - 1) {}

            Array.Copy(aryDatTmp, 0, aryDatTx, nOffset, aryDatTmp.Length) 'Part I moving
            nOffset = nOffset + aryDatTmp.Length
            Array.Copy(aryDatRx, 0, aryDatTx, nOffset, aryDatRx.Length) 'Part II moving
            nOffset = nOffset + aryDatRx.Length
            Array.Copy(aryDatMACLenData, 0, aryDatTx, nOffset, aryDatMACLenData.Length) 'Part III moving
            nOffset = nOffset + aryDatMACLenData.Length

            '---[Free Resource if possible]---
            Erase aryDatRx
            Erase aryDatTmp
            Erase aryDatTmp2
            Erase aryDatMACLenData
            '
            aryDatRx = Nothing
            aryDatTmp = Nothing
            aryDatTmp2 = Nothing
            aryDatMACLenData = Nothing
            '
            '-----------------------------------------------------------------[ Send to reader ]-----------------------------------------------------------------
            System_Customized_Command(u16Cmd, strFuncName, aryDatTx, aryDatRx, AddressOf ClassReaderProtocolBasic.PreProcessCbRecv, nTimeout)

            If (bIsRetStatusOK) Then
                'Always Return 1 byte Ret Code
                ' 0 —-- Success
                ' 1 --- MAC verify fail, MAC Key is absent
                ' 2 --- MAC verify fail, KSN error
                ' 3 --- MAC verify fail, MAC value mismatch
                ' 4 --- Set Date and Time fail
                ' 5 --- Invalid date and time format
                If (m_tagIDGCmdDataInfo.bIsRawData) Then
                    o_byteRetCode = aryDatRx(0)
                Else
                    'Default = Success
                    o_byteRetCode = &H0
                End If

            Else

            End If

        Catch ext As TimeoutException
            LogLn("[Err][Tx/Rx Timeout] [IDG: " + GetIDGCmdString(u16Cmd) + "] " + strFuncName + "() = " + ext.Message)
        Catch ex As Exception
            LogLn("[Err] [IDG:  " + GetIDGCmdString(u16Cmd) + "] " + strFuncName + "() = " + ex.Message)
        Finally
            m_tagIDGCmdDataInfo.Cleanup()
            m_bIDGCmdBusy = False
            Erase aryDatTx
            '
        End Try

    End Sub
    '
    Public Overridable Sub SystemTime_Get_PCI_SRED(ByRef o_strDate_YYMMDD As String, ByRef o_strTime_HHMMSS As String, Optional ByVal nTimeout As Integer = ClassDevRS232.m_cnTX_TIMEOUT_IN_SEC)
        'Please read "Handling system time and RTC time Rev0.2.docx"
        '29-06 ,Non-PIC, len = 0x07 if Non-PCI, = 0x23 if PCI
        'o_strDate_YYMMDD, Data in BCD = "161231" --> 0x16 0x12 0x31 = 2016 December 31;
        'o_strTime_HHMMSS, Time in BCD = "235959" --> 0x23 0x59 0x59 = 23h 59m 59s;
        'o_strMAC_Temp, MAC in hex string or Empty String (Non-PCI),
        'Using AR @ PISCES platform.

        Dim u16Cmd As UInt16 = &H2506
        Dim strFuncName As String = "SystemTime_Get_PCI_SRED"
        Dim aryDatTx As Byte() = Nothing
        Dim aryDatRx As Byte() = Nothing
        Dim aryDatTmp As Byte() = New Byte(2) {} '3 bytes for YYMMDD
        Dim aryDatTmp2 As Byte() = New Byte(2) {} '3 bytes for HHMMSS
        Dim nOffset As Integer = 0

        If (m_bIDGCmdBusy) Then
            Return
        End If
        m_bIDGCmdBusy = True

        Try
            '-------[ Send to reader ]-------
            System_Customized_Command(u16Cmd, strFuncName, aryDatTx, aryDatRx, AddressOf ClassReaderProtocolBasic.PreProcessCbRecv, nTimeout)

            If (bIsRetStatusOK) Then
                'Translate to Get Time Format
                Array.Copy(aryDatRx, nOffset, aryDatTmp, 0, aryDatTmp.Length)
                nOffset = aryDatTmp.Length
                Array.Copy(aryDatRx, nOffset, aryDatTmp2, 0, aryDatTmp2.Length)
                '
                ClassTableListMaster.ConvertFromArrayByte2String(aryDatTmp, o_strDate_YYMMDD, False)
                ClassTableListMaster.ConvertFromArrayByte2String(aryDatTmp2, o_strTime_HHMMSS, False)
            Else

            End If

        Catch ext As TimeoutException
            LogLn("[Err][Tx/Rx Timeout] [IDG: " + GetIDGCmdString(u16Cmd) + "] " + strFuncName + "() = " + ext.Message)
        Catch ex As Exception
            LogLn("[Err] [IDG:  " + GetIDGCmdString(u16Cmd) + "] " + strFuncName + "() = " + ex.Message)
        Finally
            m_tagIDGCmdDataInfo.Cleanup()
            m_bIDGCmdBusy = False
            If (aryDatRx IsNot Nothing) Then
                Erase aryDatRx
            End If

            If (aryDatTmp IsNot Nothing) Then
                Erase aryDatTmp
            End If

            If (aryDatTmp2 IsNot Nothing) Then
                Erase aryDatTmp2
            End If
            '
        End Try

    End Sub
    '
    'Out MAC Address Format "001122DDEEFF"
    Public Overridable Sub SystemEthMACAddressGet(ByRef o_strEthMACAddr As String, Optional ByVal nTimeout As Integer = ClassDevRS232.m_cnRX_TIMEOUT_IN_SEC)
        Dim u16Cmd As UInt16 = &HD111 'AR 3.0.0 Draft 3.
        Dim strFuncName As String = "SystemEthMACAddressGet"

        Dim arrayByteSend As Byte() = New Byte() {&H0}
        Dim arrayByteRecv As Byte() = Nothing
        Dim arrayByteEthMACAddr As Byte() = Nothing
        If (m_bIDGCmdBusy) Then
            Return
        End If
        m_bIDGCmdBusy = True

        Try
            '-----------------------------------------------------------------[ Send to reader ]-----------------------------------------------------------------
            'm_tagIDGCmdDataInfo.Reinit(u16Cmd, m_arraybyteSendBuf, Me, , m_arraybyteSendBufUnknown)

            'PktComposerCommandFrameHeaderCmdSetDataLengthCRCData(m_tagIDGCmdDataInfo)

            'm_devRS232.SendCommandFrameAndWaitForResponseFrame(AddressOf ClassReaderProtocolBasic.CommonCbRecv, m_tagIDGCmdDataInfo, True, nTimeout * 1000)

            System_Customized_Command(u16Cmd, strFuncName, arrayByteSend, arrayByteRecv, AddressOf ClassReaderProtocolBasic.PreProcessCbRecv, nTimeout)

            If (bIsRetStatusOK) Then
                'Get Eth MAC Address String
                ClassTableListMaster.ConvertFromArrayByte2String(arrayByteRecv, o_strEthMACAddr, False)
                o_strEthMACAddr = o_strEthMACAddr.Replace(":", "")
                o_strEthMACAddr = o_strEthMACAddr.Replace("-", "")
            Else
                If (arrayByteSend IsNot Nothing) Then
                    Erase arrayByteSend
                End If
                If (arrayByteRecv IsNot Nothing) Then
                    Erase arrayByteRecv
                End If
            End If

        Catch ext As TimeoutException
            LogLn("[Err][Timeout] [IDG: " + GetIDGCmdString(u16Cmd) + "] " + strFuncName + "() = " + ext.Message)
        Catch ex As Exception
            LogLn("[Err] [IDG:  " + GetIDGCmdString(u16Cmd) + "] " + strFuncName + "() = " + ex.Message)
        Finally
            m_tagIDGCmdDataInfo.Cleanup()
            m_bIDGCmdBusy = False
        End Try
    End Sub
    '
    'Note: MAC Address String Type 01 : strEthMACAddress = "00:11:22:DD:EE:FF"
    '                         Type 02 : if in the form "001122DDEEFF", ":" will be added
    Public Overridable Sub SystemEthMACAddressSet(strEthMACAddress As String, Optional ByVal nTimeout As Integer = ClassDevRS232.m_cnTX_TIMEOUT_IN_SEC)
        Dim u16Cmd As UInt16 = &HD116
        Dim strFuncName As String = "SystemEthMACAddressSet"
        Dim strEthMacAddr2 As String = ""
        'Set Serial Number Length = 15 bytes
        Dim arrayByteSend As Byte() = Nothing
        Dim arrayByteRecv As Byte() = Nothing
        Dim nCnt As Integer = 0

        If (m_bIDGCmdBusy) Then
            Return
        End If
        m_bIDGCmdBusy = True

        Try
            'Formalize First
            strEthMACAddress = strEthMACAddress.Replace(" ", "")
            strEthMACAddress = strEthMACAddress.Replace("-", "")
            strEthMACAddress = strEthMACAddress.Replace(":", "")
            strEthMACAddress = strEthMACAddress.ToUpper()
            'if in the form "001122DDEEFF", ":" will be added
            If (strEthMACAddress.Length < 17) Then
                For Each ch As Char In strEthMACAddress
                    strEthMacAddr2 = strEthMacAddr2 + ch
                    If (ch = ":"c) Then
                        nCnt = 0
                        Continue For
                    End If

                    nCnt += 1
                    If ((nCnt >= 2) And (strEthMacAddr2.Length < 17)) Then
                        strEthMacAddr2 = strEthMacAddr2 + ":"
                        nCnt = 0
                    End If
                Next
            Else
                strEthMacAddr2 = strEthMACAddress
            End If
            'To Hex Binary
            ClassTableListMaster.ConvertFromAscii2HexByte(strEthMacAddr2, arrayByteSend)
            '-----------------------------------------------------------------[ Send to reader ]-----------------------------------------------------------------
            System_Customized_Command(u16Cmd, strFuncName, arrayByteSend, arrayByteRecv, AddressOf ClassReaderProtocolBasic.PreProcessCbRecv, nTimeout)

            If (bIsRetStatusOK) Then
            Else
            End If

        Catch ext As TimeoutException
            LogLn("[Err][Timeout] [IDG: " + GetIDGCmdString(u16Cmd) + "] " + strFuncName + "() = " + ext.Message)
        Catch ex As Exception
            LogLn("[Err] [IDG:  " + GetIDGCmdString(u16Cmd) + "] " + strFuncName + "() = " + ex.Message)
        Finally
            If (arrayByteSend IsNot Nothing) Then
                Erase arrayByteSend
            End If
            If (arrayByteRecv IsNot Nothing) Then
                Erase arrayByteRecv
            End If
            m_tagIDGCmdDataInfo.Cleanup()
            m_bIDGCmdBusy = False
        End Try
    End Sub




    'Return = True :
    '       = False:
    Private Shared Function IsProtoclHdrRemoveAheadGarbage(ByRef io_refIDGCmdData As tagIDGCmdData) As Boolean
        Dim bFound As Boolean = False
        ' 
        ' ClassReaderProtocolBasic.s_strProtocolV2Header;s_arrayByteV2Header
        '
        ' Ex : In buf = "56 53 62 62 56 69 56 4F 74 65 63 68 32 00 ..."
        '      Do LogLn(CharString(56 53 62 62))
        '      return buf = "56 69 56 4F 74 65 63 68 32 00 .." 
        '
        Dim byteIdxChar As Byte = ClassReaderProtocolBasic.s_arrayByteV2Header(0)
        Dim nLenExtraByteMove As Integer = io_refIDGCmdData.m_nDataRecvOffSkipBytes  'Data Move Length, start position = skipped offset
        Dim nLenExtraByteMoveTmp As Integer = 0 'Data Move Length
        Dim byteArrayTmp As Byte() = Nothing
        'If Remains Sub String is part of Header(start at [0]), we keep the remains and skip log it as unknown / garbage
        Dim bRemainsIsMatchedSubstring As Boolean = False
        'Data Source Offset
        Dim nOffsetSrc As Integer = 0

        Try
            'Data Source Array = .m_refarraybyteData
            'Data Dest Array = .m_refarraybyteDataExtraUnknown (if not null/nothing)
            With io_refIDGCmdData
                While (True)
                    'Step 01 - Check Current Rx Data Len >= Header Length
                    '   --> No, return
                    nLenExtraByteMoveTmp = System.Array.IndexOf(Of Byte)(.m_refarraybyteData, byteIdxChar, nLenExtraByteMove)

                    If ((nLenExtraByteMoveTmp = -1) Or (.m_nDataRecvOff <= nLenExtraByteMoveTmp)) Then
                        'Step 02 - Not found First Char,
                        'All Rx In Data are unknown data
                        nLenExtraByteMove = .m_nDataRecvOff - .m_nDataRecvOffSkipBytes
                        'End searching
                        Exit While
                    Else
                        'Step 03 - Found!
                        ' Subtract Remain Part
                        If (byteArrayTmp IsNot Nothing) Then
                            Erase byteArrayTmp
                        End If
                        '+1 --> since nLenExtraByteMove also involved.
                        nLenExtraByteMoveTmp = .m_refarraybyteData.Count - nLenExtraByteMove + 1
                        byteArrayTmp = New Byte(nLenExtraByteMoveTmp) {}
                        Array.Copy(.m_refarraybyteData, nLenExtraByteMove, byteArrayTmp, 0, nLenExtraByteMoveTmp)

                        '03.a - Check Remains Length with Protocol Header, RemLen >= Header Length, 
                        If (byteArrayTmp.Count >= ClassReaderProtocolBasic.s_arrayByteV2Header.Count) Then
                            '03.b - If RemLen >= HdrLen, use Header Pattern as seeking substring
                            '---[ Check Remains as part of header ]---
                            'If bRemainsIsMatchedSubstring = false, then all
                            bRemainsIsMatchedSubstring = (ClassTableListMaster.ArrayByteSubPatternIndexOf(byteArrayTmp, 0, ClassReaderProtocolBasic.s_arrayByteV2Header, 0, ClassReaderProtocolBasic.s_arrayByteV2Header.Count) = 0)

                            If (Not bRemainsIsMatchedSubstring) Then
                                'Not Found -> Try to seek next 
                                nLenExtraByteMove = nLenExtraByteMoveTmp + 1
                                Continue While
                            End If

                            'In case of this
                            'Ex: .m_refarraybyteData = asd00<NULL><NULL>#*(ViVOtech2....<CRC,2bytes>:*(*<NULL> <NULL>...
                            ' where 
                            'byteArrayTmp() = ViVOtech2....<CRC,2bytes>:*(*<NULL> <NULL>...
                            'nLenExtraByteMove = 0B ,11th byte

                        Else
                            '03.c - If RemLen < HdrLen, use Remains String as seeking substring
                            'In case of this
                            'Ex: m_refarraybyteData =  asd00<NULL><NULL>#*(ViVOte
                            ' where 
                            'byteArrayTmp() = ViVOte, part of std header ClassReaderProtocolBasic.s_arrayByteV2Header
                            '
                            bRemainsIsMatchedSubstring = (ClassTableListMaster.ArrayByteSubPatternIndexOf(ClassReaderProtocolBasic.s_arrayByteV2Header, 0, byteArrayTmp, 0, byteArrayTmp.Count) = 0)
                            '
                            '
                        End If

                        'Update Seek Index
                        nLenExtraByteMove = nLenExtraByteMoveTmp

                        If (Not bRemainsIsMatchedSubstring) Then
                            'All Rx In Data are unknown data
                            nLenExtraByteMove = .m_nDataSizeRecv
                        End If


                    End If

                    bFound = True
                    Exit While
                End While

                'Step 04 -
                If (nLenExtraByteMove > 0) Then
                    'Move all of them to garbage / unknown data pool (m_refarraybyteDataExtraUnknown) if 
                    'the pool is not NULL (i.e. Nothing)
                    .RxBufMoveUnknownDataOut(nLenExtraByteMove)

                End If
            End With
            '

        Catch ex As Exception
            LogLn("[Err] " + ex.Message)
        Finally
            If (byteArrayTmp IsNot Nothing) Then
                Erase byteArrayTmp
            End If
        End Try
        '
        Return bFound
    End Function
    '
    Public Overridable Sub System_External_SDRAM(Optional ByVal nTimeout As Integer = ClassDevRS232.m_cnTX_TIMEOUT_IN_SEC)
        Try
            Throw New Exception("System_External_SDRAM(), basic , Not Ready")
        Catch ex As Exception
            LogLn("[Err] () = " + ex.Message)
        Finally

        End Try
    End Sub

    '
    Public Overridable Sub System_ExtDev_PingL100(ByRef o_strL100_Info As String, Optional ByVal nTimeout As Integer = ClassDevRS232.m_cnTX_TIMEOUT_IN_SEC)
        Try
            Throw New Exception("System_ExtDev_PingL100(), basic , Not Ready")
        Catch ex As Exception
            LogLn("[Err] () = " + ex.Message)
        Finally

        End Try
    End Sub

    '2017 Sept 18, starts from project VP5300, NEO 2.0 platform.
    Public Overridable Sub System_PwrSave_Sleep_MsrRfid(byteVal00 As Byte, byteVal01 As Byte, nTimeout As Integer)
        Dim u16Cmd As UInt16 = &HF003
        Dim strFuncName As String = "System_PwrSave_Sleep_MsrRfid"
        Dim bytAryTx As Byte() = New Byte(1) {byteVal00, byteVal01}

        If (m_bIDGCmdBusy) Then
            Return
        End If
        m_bIDGCmdBusy = True

        Try
            '----------------------[ Send to reader ]---------------------
            System_Customized_Command(u16Cmd, strFuncName, bytAryTx, Nothing, AddressOf ClassReaderProtocolBasic.PreProcessCbRecv, nTimeout)

            If (bIsRetStatusOK) Then

            Else

            End If

        Catch ext As TimeoutException
            LogLn("[Err][Timeout] [IDG: " + GetIDGCmdString(u16Cmd) + "] " + strFuncName + "() = " + ext.Message)
        Catch ex As Exception
            LogLn("[Err] [IDG:  " + GetIDGCmdString(u16Cmd) + "] " + strFuncName + "() = " + ex.Message)
        Finally
            If (bytAryTx IsNot Nothing) Then
                Erase bytAryTx
            End If
            '
            m_tagIDGCmdDataInfo.Cleanup()
            m_bIDGCmdBusy = False
        End Try
    End Sub
    '
    Public WriteOnly Property SetVivoProctlType As ClassReaderInfo.ENU_PROTCL_VER
        Set(valueType As ClassReaderInfo.ENU_PROTCL_VER)
            m_tagIDGCmdDataInfo.SetVivoProctlType = valueType
            m_tagIDGCmdDataInfoTransactionCancel.SetVivoProctlType = valueType
        End Set
    End Property

    '2017 Sept 27, Antenna Control - 01h (ON)/ 00h (OFF)
    Public Overridable Sub SystemPT_AntennaCtrl(ByVal byteVal As Byte, Optional ByVal nTimeout As Integer = ClassDevRS232.m_cnTX_TIMEOUT_IN_SEC)
        Dim u16Cmd As UInt16 = &H2801
        Dim strFuncName As String = "SystemPT_AntennaCtrl"
        Dim bytAryTx As Byte() = New Byte(0) {byteVal}

        If (m_bIDGCmdBusy) Then
            Return
        End If
        m_bIDGCmdBusy = True

        Try
            '----------------------[ Send to reader ]---------------------
            System_Customized_Command(u16Cmd, strFuncName, bytAryTx, Nothing, AddressOf ClassReaderProtocolBasic.PreProcessCbRecv, nTimeout)

            If (bIsRetStatusOK) Then

            Else

            End If

        Catch ext As TimeoutException
            LogLn("[Err][Timeout] [IDG: " + GetIDGCmdString(u16Cmd) + "] " + strFuncName + "() = " + ext.Message)
        Catch ex As Exception
            LogLn("[Err] [IDG:  " + GetIDGCmdString(u16Cmd) + "] " + strFuncName + "() = " + ex.Message)
        Finally
            If (bytAryTx IsNot Nothing) Then
                Erase bytAryTx
            End If
            '
            m_tagIDGCmdDataInfo.Cleanup()
            m_bIDGCmdBusy = False
        End Try
    End Sub

    '
    Public Function System_Customized_Command_CfgShip_Chk(strCmd As String, strTxDat As String, ByRef io_strRxDat As String, Optional ByVal nTimeout As Integer = ClassDevRS232.m_cnTX_TIMEOUT_IN_SEC) As Boolean
        Dim u16Cmd As UInt16 = Convert.ToUInt16(strCmd, 16)
        Dim strFuncName As String = "System_Customized_Command_CfgShip_Chk"
        Dim arrayByteSend As Byte() = Nothing
        Dim arrayByteRecv As Byte() = Nothing
        Dim bRet As Boolean = True
        Dim strRxData_Rdr As String = ""

        If (m_bIDGCmdBusy) Then
            Return False
        End If
        m_bIDGCmdBusy = True

        Try
            'Ex: strCmd = "D003"


            'ClassTableListMaster.ConvertValue2ArrayByte(byteModeOn, arrayByteSend)
            If (Not strTxDat.Equals("NA")) Then
                ClassTableListMaster.TLVauleFromString2ByteArray(strTxDat, arrayByteSend)
            End If

            System_Customized_Command(u16Cmd, strFuncName, arrayByteSend, arrayByteRecv, AddressOf PreProcessCbRecv, nTimeout)

            If (bIsRetStatusOK) Then
                If (Not io_strRxDat.Equals("NA")) Then
                    'Compare it.
                    ClassTableListMaster.ConvertFromArrayByte2String(arrayByteRecv, strRxData_Rdr, False)
                    bRet = io_strRxDat.Equals(strRxData_Rdr)
                    If (Not bRet) Then
                        io_strRxDat = "Err : " + io_strRxDat + " : " + strRxData_Rdr
                    End If
                End If
            Else

            End If
        Catch ex As Exception

        Finally
            If (arrayByteSend IsNot Nothing) Then
                Erase arrayByteSend
            End If
            If (arrayByteRecv IsNot Nothing) Then
                Erase arrayByteRecv
            End If

            m_bIDGCmdBusy = False
        End Try
        '
        Return bRet
    End Function

    Public Overridable Sub NEO20_SystemReboot(Optional ByVal nTimeout As Integer = ClassDevRS232.m_cnTX_TIMEOUT_IN_SEC)
        '
        Dim u16Cmd As UInt16 = &H7705
        Dim strFuncName As String = "NEO20_SystemReboot"
        Dim arrayByteSend As Byte() = Nothing
        Dim arrayByteRecv As Byte() = Nothing

        If (m_bIDGCmdBusy) Then
            Return
        End If
        m_bIDGCmdBusy = True

        Try
            'ClassTableListMaster.ConvertValue2ArrayByte(byteModeOn, arrayByteSend)

            System_Customized_Command(u16Cmd, strFuncName, arrayByteSend, arrayByteRecv, AddressOf PreProcessCbRecv, nTimeout)

            If (bIsRetStatusOK) Then

            Else

            End If
        Catch ex As Exception

        Finally
            If (arrayByteSend IsNot Nothing) Then
                Erase arrayByteSend
            End If
            If (arrayByteRecv IsNot Nothing) Then
                Erase arrayByteRecv
            End If

            m_bIDGCmdBusy = False
        End Try
    End Sub


    Public Overridable Sub System_KBWedge_ModeSwitch(ByVal byteMode As Byte, Optional ByVal nTimeoutSec As Integer = ClassDevRS232.m_cnTX_TIMEOUT_IN_SEC)
        '2017 Sept 12, goofy_liu@idtechproductsc.om.tw
        Dim u16Cmd As UInt16 = &H10A
        Dim strFuncName As String = "System_KBWedge_ModeSwitch"
        Dim byteAryTx As Byte() = New Byte(1) {&H0, byteMode}

        If (m_bIDGCmdBusy) Then
            Return
        End If
        m_bIDGCmdBusy = True

        Try
            '----------------------[ Send to reader ]---------------------
            System_Customized_Command(u16Cmd, strFuncName, byteAryTx, Nothing, AddressOf ClassReaderProtocolBasic.PreProcessCbRecv, nTimeoutSec)

            If (bIsRetStatusOK) Then

            Else

            End If

        Catch ext As TimeoutException
            LogLn("[Err][Timeout] [IDG: " + GetIDGCmdString(u16Cmd) + "] " + strFuncName + "() = " + ext.Message)
        Catch ex As Exception
            LogLn("[Err] [IDG:  " + GetIDGCmdString(u16Cmd) + "] " + strFuncName + "() = " + ex.Message)
        Finally
            '----------[Note, BTPay Mini]----------
            '2017 Mar 02
            'Enzytek BLE Module & MUC communication has slow response.
            'Tx/Rx response time is around up to 200 ms.
            'So here we have to delay 100ms before Tx and delay 100ms after Rx.
            'Point (B)
            Thread.Sleep(100)
            If (byteAryTx IsNot Nothing) Then
                Erase byteAryTx
            End If
            m_tagIDGCmdDataInfo.Cleanup()
            m_bIDGCmdBusy = False
        End Try

    End Sub
    '

    ReadOnly Property bIsConnectedUSBHID_KBWedge As Boolean
        Get
            Return m_devUSBHID.bIsOpen And m_devUSBHID.bIsKBWedge
        End Get
    End Property


End Class
'

