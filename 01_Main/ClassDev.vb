﻿Imports System.IO.Ports

'==================================================================================
' File Name: ClassDev.vb
' Description: The ClassDev is as Must Inherited Class of Communication( RS232 / USB HID ) object
' Who use it as component: ClassReaderCommander
' Similar Class: ClassDevUsbHID
'
' Revision:
' -----------------------[ Initial Version ]-----------------------
' 2014 June 4, by goofy_liu@idtechproducts.com.tw
'==================================================================================
Public MustInherit Class ClassDev
    Inherits Object

    '
    '=================[Private Sub/Func Area]=================
    'Logging
    'Private m_dbgInfo As StackFrame = New StackFrame
    'Delegate Function type
    'Private Delegate Sub LogLnDelegate(ByRef strInfo As String)
    Protected Shared LogLn As Form01_Main.LogLnDelegate
    Protected Shared LogLnThreadSafe As Form01_Main.LogLnDelegate

    'False = Log On Mode
    'True = Log Off Mode
    Protected m_bLogTurnOnOff As Boolean = False
    Protected m_bIsEngMode_HWTest As Boolean = False
    Protected m_pMF As Form01_Main = Nothing
    '
    'Public Delegate Sub DelegatecallbackExternalRecvProcedure(ByRef devSerialPort As SerialPort, ByRef callbackParameters As tagIDGCmdData, ByVal bRS232CommOK As Boolean)
    Public Delegate Sub DelegatecallbackExternalRecvProcedure(ByRef devPort As SerialPort, ByRef callbackParameters As tagIDGCmdData, ByVal bCommOK As Boolean)
    Public Delegate Sub DelegatecallbackExternalRecvParameterSynctagIDGCmdData(ByRef tagIDGDataInfo As tagIDGCmdData)
    Protected m_callbackExternalRecvProcedureSyncParameter As DelegatecallbackExternalRecvParameterSynctagIDGCmdData
    Protected m_callbackExternalRecvProcedure As DelegatecallbackExternalRecvProcedure
    Protected m_callbackParametersTagIDGCmdData As tagIDGCmdData
    '
    Public Property bIsHWTestOnly As Boolean
        Set(value As Boolean)
            m_bIsEngMode_HWTest = value
        End Set
        Get
            Return m_bIsEngMode_HWTest
        End Get
    End Property
    '
    Public Property bLogTurnOnOff As Boolean
        Get
            Return m_bLogTurnOnOff
        End Get
        Set(value As Boolean)
            m_bLogTurnOnOff = value
        End Set
    End Property
    '
    Protected m_refwinformMain As Form01_Main
    Protected m_bIsStressTest As Boolean = False
    'This is very useful feature for debugging will constant frame data as received packet if there is no reader could be tested.
    Protected m_bDebugRecvMode As Boolean
    Protected m_arrayByteDRMBuffer As Byte()
    Protected m_arrayByteBufferRxDirect As Byte()
    Sub New(refwinformMain As Form01_Main)
        '
        m_pMF = Form01_Main.GetInstance()
        '
        m_bDebugRecvMode = False
        m_refwinformMain = refwinformMain

        m_callbackExternalRecvProcedure = Nothing

        'Create Logging Stuffs
        LogLn = New Form01_Main.LogLnDelegate(AddressOf m_pMF.LogLn)
        LogLnThreadSafe = New Form01_Main.LogLnDelegate(AddressOf m_pMF.LogThreadSafe)
        '
        m_bLogTurnOnOff = False
        m_arrayByteBufferRxDirect = New Byte(ClassDevRS232.m_cnRX_BUF_MAX - 1) {}
    End Sub
    '
    Public Sub SetDebugRecvMode(arrayByteDRMBuffer As Byte(), Optional ByVal bDRM As Boolean = True)
        m_arrayByteDRMBuffer = arrayByteDRMBuffer
        m_bDebugRecvMode = bDRM
    End Sub
    '
    Public MustOverride Sub Open()
    Public MustOverride Sub Close()

    Protected m_bTimeout As Boolean
    Property bIsStatusTimeout As Boolean
        Set(value As Boolean)
            m_bTimeout = value
        End Set
        Get
            Return m_bTimeout
        End Get
    End Property
    '
    Public Overridable Sub SendCommandAndResponse(ByRef arraybyte As Byte(), ByVal u32DataSize As UInt32, ByVal u32TimeoutMS As UInt32)
        ' Throw New Exception("[TODO] ClassDev::Send()")
    End Sub

    ' Param :
    ' [IN]
    ' io_u32DataSizeRx = Input Buffer Size
    '[OUT]
    ' io_u32DataSizeRx = Read Data Size
    Public Overridable Sub SendCommandAndResponse(ByRef arraybyteTx As Byte(), ByVal u32DataSizeTx As UInt32, ByRef o_arraybyteRx As Byte(), ByRef io_u32DataSizeRx As UInt32, ByVal u32TimeoutMS As UInt32, Optional ByVal bIsDiscardedTxRx As Boolean = True)

    End Sub
    'For RS232 Recv Handler
    Public Overridable Sub Recv(ByVal sender As Object, ByVal e As SerialDataReceivedEventArgs)
        ' Throw New Exception("[TODO] ClassDev::Recv RS232()")
    End Sub

    'For USB HID Handler
    Public Overridable Sub Recv(ByRef refCmdHIDInfo As Object)
        ' Throw New Exception("[TODO] ClassDev::Recv USBHID()")
    End Sub
    '
    '[Note] Inherited Class Must Call Base Class's Cleanup() to free mem resource
    Public Overridable Sub Cleanup()
        'Throw New Exception("[TODO] ClassDev::Cleanup()")
        If (m_arrayByteBufferRxDirect IsNot Nothing) Then
            Erase m_arrayByteBufferRxDirect
        End If
        If (m_arrayByteDRMBuffer IsNot Nothing) Then
            Erase m_arrayByteDRMBuffer
        End If

    End Sub

    Public Function IsReportIDUSBHID(byteVal As Byte) As Boolean
        If ((byteVal > &H0) And (byteVal < &H5)) Then
            Return True
        End If
        'If (byteVal = &H1) Then
        '    Return True
        'End If
        'If (byteVal = &H2) Then
        '    Return True
        'End If
        'If (byteVal = &H3) Then
        '    Return True
        'End If
        'If (byteVal = &H4) Then
        '    Return True
        'End If
        Return False
    End Function
    '
    Protected Sub ConfigCallbackRecv(ByRef callbackExternalRecvProcedure As DelegatecallbackExternalRecvProcedure, Optional ByRef callbackParameters As tagIDGCmdData = Nothing)
        '----------------------------------------------------
        ' Setup Callback function & Parameter
        m_callbackExternalRecvProcedure = callbackExternalRecvProcedure
        m_callbackParametersTagIDGCmdData = callbackParameters
    End Sub
    '
    Protected Sub SendCommandFrameAndWaitForResponseFrameDebugRecvMode(ByRef callbackExternalRecvProcedure As DelegatecallbackExternalRecvProcedure, ByRef refIDGCmdData As tagIDGCmdData, Optional ByVal bWait As Boolean = True, Optional ByVal u32TimeoutMS As UInt32 = ClassDevRS232.m_cnTX_TIMEOUT, Optional ByVal bDebug As Boolean = False)
        Try
            Dim nTicksCountDelay As Integer = 0
            Dim nTicksCountStart As Integer
            Dim nTicksCountStartSuitableDeplay As Integer
            Dim bTimeout As Boolean = False
            'Dim nTicksCountCurrent As Integer
            'Dim strMsgList() As String
            '
            Dim strMsgCmdSub As String = refIDGCmdData.strCmdNEOFormat
            Dim strWaitStatus As String
            If bWait Then
                strWaitStatus = "Wait Response"
            Else
                strWaitStatus = "Exit Without Wait"
            End If
            'LogLn("[Note] ClassDevRS232.SendCommandFrameAndWaitForResponseFrame() , Cmd = " + strMsg)
            If (m_bLogTurnOnOff) Then
                LogLn("IDG Cmd [ " + strMsgCmdSub + " ],  " + strWaitStatus)
            End If

            '
            refIDGCmdData.DefaultResponse()


            ' Set to Command Activate Mode
            refIDGCmdData.CommInterruptFSMAct()

            'Send(refIDGCmdData.m_refarraybyteData, refIDGCmdData.m_nDataSizeSend)

            'LogLn("SendCommandFrameAndWaitForResponseFrame() 4")
            refIDGCmdData.SetDebugMode_RecvDone(m_arrayByteDRMBuffer)

            '----------------------[ Waiting for Response Frame ]----------------------
            If bWait Then
                'Get Start System Ticks
                nTicksCountStart = Environment.TickCount
                nTicksCountStartSuitableDeplay = nTicksCountStart
                '
                While Not IsResponseFrameReady(refIDGCmdData)
                End While
                '
                If refIDGCmdData.CallByIDGCmdTimeOutChk_IsCommIDGCancelStop Then
                    Throw New Exception("[Warning] Stopping RS232 Recv Handler")
                End If
                '
                If bTimeout Then
                    'strMsg = String.Format("[Err][Timeout] Cmd Frame, Cmd ={0,4:X4}", refIDGCmdData.m_u16CmdSet)
                    Throw New TimeoutException("[Err][Timeout] Cmd Frame, Cmd =" + strMsgCmdSub)
                End If

                If (bDebug) Or (Not refIDGCmdData.bIsResponseOK) Then
                    LogLn(">>>>>>>>>> Rx Data <<<<<<<<<<")
                    Form01_Main.LogLnDumpData(refIDGCmdData.m_refarraybyteData.Skip(refIDGCmdData.m_nDataRecvOffSkipBytes).ToArray(), refIDGCmdData.m_nDataSizeRecv)
                    LogLn(" ")
                End If
                '
                If (refIDGCmdData.bIsResponseOK) Then
                    If (m_bLogTurnOnOff) Then
                        LogLn(strMsgCmdSub + " << RxSts = " + refIDGCmdData.strGetResponseMsg + "( " & refIDGCmdData.byteGetResponseCode & " )")
                    End If
                End If
                '
                refIDGCmdData.CommInterruptFSMIdle()
                '
            Else
                ' Do Nothing
            End If
        Catch ex As Exception
            Throw 'ex
        Finally
            If ((refIDGCmdData.CallByIDGCmdTimeOutChk_IsCommIDGCancelStop) Or (refIDGCmdData.CallByIDGCmdTimeOutChk_IsCommTimeOutCancelStop)) Then
                refIDGCmdData.CallByIDGCmdTimeOutChk_SetTimeoutEvent()
            Else
                If ((Not refIDGCmdData.CallByIDGCmdTimeOutChk_IsCommIdle) And (m_bLogTurnOnOff)) Then
                    ' LogLn("[Dev RS232][Warning] Not in Stopping Status or No instance stop Receiving Procedure, must have some exception, FSM = " & refIDGCmdData.CallByIDGCmdTimeOutChk_GetStrCommFSM) ' 2016 July 06, remove this msg
                End If
            End If
        End Try
    End Sub
    '
    Protected Sub SendCommandFrameAndWaitForResponseFrameDebugRecvMode_Pure(ByRef callbackExternalRecvProcedure As DelegatecallbackExternalRecvProcedure, ByVal arrayByteTx As Byte(), ByRef o_arrayByteRx As Byte(), ByRef o_u32RxLen As UInt32, Optional ByVal bWait As Boolean = True, Optional ByVal u32TimeoutMS As UInt32 = ClassDevRS232.m_cnTX_TIMEOUT, Optional ByVal bDebug As Boolean = False)
        Try
            Dim nTicksCountDelay As Integer = 0
            Dim nTicksCountStart As Integer
            Dim nTicksCountStartSuitableDeplay As Integer
            Dim bTimeout As Boolean = False
            'Dim nTicksCountCurrent As Integer
            'Dim strMsgList() As String
            '
            Dim strMsg As String = "Direct Tx/Rx, Deubg Mode "
            Dim strWaitStatus As String
            If bWait Then
                strWaitStatus = "Wait Response"
            Else
                strWaitStatus = "Exit Without Wait"
            End If

            'LogLn("[Note] ClassDevRS232.SendCommandFrameAndWaitForResponseFrame() , Cmd = " + strMsg)
            If (m_bLogTurnOnOff) Then
                LogLn("IDG Cmd [ " + strMsg + " ],  " + strWaitStatus)
            End If

            '----------------------[ Waiting for Response Frame ]----------------------
            If bWait Then
                'Get Start System Ticks
                nTicksCountStart = Environment.TickCount
                nTicksCountStartSuitableDeplay = nTicksCountStart

                '
                If bTimeout Then
                    'strMsg = String.Format("[Err][Timeout] Cmd Frame, Cmd ={0,4:X4}", refIDGCmdData.m_u16CmdSet)
                    Throw New TimeoutException("[Err][Timeout] Cmd Frame")
                End If

                If (bDebug) Or (Not o_arrayByteRx.Equals(Nothing)) Then
                    LogLn(">>>>>>>>>> Recv Data <<<<<<<<<<")
                    Form01_Main.LogLnDumpData(o_arrayByteRx, o_arrayByteRx.Length)
                    LogLn(" ")
                End If
                '
            Else
                ' Do Nothing
            End If
            '[TBD] In Debug Mode we make Output(Return) Rx Len = Input Length 
            o_u32RxLen = arrayByteTx.Length
        Catch ex As Exception
            Throw 'ex
        Finally

        End Try
    End Sub
    '
    '[Return]
    ' = True , Cmd And Response Matched
    ' = False, Cmd And Response Mismatched.
    Public Overridable Sub SendCommandFrameAndWaitForResponseFrame(ByRef callbackExternalRecvProcedure As DelegatecallbackExternalRecvProcedure, ByRef refIDGCmdData As tagIDGCmdData, ByVal nPlatform As ClassReaderInfo.ENU_FW_PLATFORM_TYPE, Optional ByVal bWait As Boolean = True, Optional ByVal u32TimeoutInMS As UInt32 = ClassDevRS232.m_cnTX_TIMEOUT)
        Try

        Catch ex As Exception
            Throw 'ex
        Finally
            'm_refwinformMain.IDGCmdTimeOutRealTimeStop()
        End Try

    End Sub
    '
    Public Overridable Sub SendCommandFrameAndWaitForResponseFrame_Pure(ByRef callbackExternalRecvProcedure As DelegatecallbackExternalRecvProcedure, ByVal arrayDataTx As Byte(), ByRef o_arrayDataRx As Byte(), ByRef o_u32RxLen As UInt32, Optional ByVal bWait As Boolean = True, Optional ByVal u32TimeoutInMS As UInt32 = ClassDevRS232.m_cnTX_TIMEOUT)
        Try
            Throw New Exception("Not Implement")
        Catch ex As Exception
            Throw 'ex
        Finally
            'm_refwinformMain.IDGCmdTimeOutRealTimeStop()
        End Try

    End Sub

    Public Overridable Sub SendCommandFrameAndWaitForResponseFrame_Pure2(ByRef callbackExternalRecvProcedure As DelegatecallbackExternalRecvProcedure, ByVal arrayDataTx As Byte(), ByRef o_arrayDataRx As Byte(), ByRef o_u32RxLen As UInt32, Optional ByVal bWait As Boolean = True, Optional ByVal u32TimeoutInMS As UInt32 = ClassDevRS232.m_cnTX_TIMEOUT)
        Try
            Throw New Exception("Not Implement")
        Catch ex As Exception
            Throw 'ex
        Finally
            'm_refwinformMain.IDGCmdTimeOutRealTimeStop()
        End Try

    End Sub

    '
    Protected Function IsResponseFrameReady(ByRef refIDGCmdData As tagIDGCmdData) As Boolean
        If (Not refIDGCmdData.bIsV1RecvDone) Then
            Return False
        End If

        If refIDGCmdData.bIsNotResponse Then
            Return False
        End If

        Return True
    End Function
End Class
