﻿Public Class FormWaitTillDone
    Enum _ANS As Integer
        _PASS_FAIL
        _SEEN_NOCHANGE
        _HEAR_NOSOUND

        _MAX
    End Enum
    '
    'Private m_pLang As ClassServiceLanguages

    Private m_nIdx As Integer
    Private m_bPass As Boolean
    Private m_bNoInterruptCmd As Boolean
    Private m_strStandardMsg As String '= "Please Wait...."
    Private m_listLabelViewUpdate As List(Of KeyValuePair(Of Font, Label))
    Private m_nAnswerMode As _ANS
    Private m_bServiceMode As Boolean
    Private m_bCanceled As Boolean

    Private m_pParentControlServiceMode As Control = Nothing

    Public Property AnsMode As _ANS
        Set(value As _ANS)
            m_nAnswerMode = value
        End Set
        Get
            Return m_nAnswerMode
        End Get
    End Property

    '============================================================
    Private m_formOriginalLocation As Point
    Private m_formStartMode As FormStartPosition
    Private m_szForm As Size
    WriteOnly Property ExternalModeConfigParent As Control
        Set(value As Control)
            m_pParentControlServiceMode = value
        End Set
    End Property
    Property bServiceMode As Boolean
        Get
            Return m_bServiceMode
        End Get
        Set(value As Boolean)
            If (value) Then
                If (m_pParentControlServiceMode IsNot Nothing) Then
                    'Save Original Left-Top Location & start mode
                    m_formOriginalLocation = Location
                    m_formStartMode = StartPosition
                    m_szForm = Size
                    ' Move to new point
                    StartPosition = FormStartPosition.Manual
                    Location = m_pParentControlServiceMode.PointToScreen(New Point(m_pParentControlServiceMode.ClientSize.Width - Width, 0))
                    Size = New Size(370, 215) ',199)
                End If
            Else
                If (m_pParentControlServiceMode IsNot Nothing) Then
                    ' Move to original point & start position mode
                    StartPosition = m_formStartMode
                    Location = m_formOriginalLocation
                    Size = m_szForm
                End If
            End If
            '
            m_bServiceMode = value
        End Set
    End Property
    '===========================================================
    Private m_tagTimeOutInfo As Form01_Main.TAG_REALTIME_UPDATE_INFO

    ReadOnly Property bIsCanceled As Boolean
        Get
            Return m_bCanceled
        End Get
    End Property

    Public Sub TimerUpdateTimeoutStop()
        With m_tagTimeOutInfo
            .m_bEnable = False
        End With
        '
    End Sub
    Public Sub TimerUpdateTimeoutReset(Optional ByVal nTimeOutInMillisec As Integer = ClassDevRS232.m_cnRX_TIMEOUT)
        With m_tagTimeOutInfo
            .m_nTestAutoTimeCountUpto1Sec = 0
            .m_nTestAutoTimeCount = nTimeOutInMillisec
            .m_bEnable = (nTimeOutInMillisec > 0)
        End With
        '
        If (m_tagTimeOutInfo.m_bEnable) Then
            Me.Text = "Remain Seconds : " + (m_tagTimeOutInfo.m_nTestAutoTimeCount / 1000).ToString()
        Else
            Me.Text = "Waiting..."
        End If

    End Sub
    Private Sub TimerUpdateTimeoutUpdate(nIntervalMillisec As Integer)
        Dim bStop As Boolean = False
        Dim bUpdatePerSec As Boolean = False


        '---------[ Stress Test - Auto End ]----------
#If 0 Then
        Dim pForm02 As Form02_StartupSelection = Form02_StartupSelection.GetInstance()
        If (pForm02.bIsStressTest) Then
            Button2_PASS_Click(Button_PASS, EventArgs.Empty)
        End If
#End If
        '---------[ End of Stress Test - Auto End ]----------

        With m_tagTimeOutInfo
            If (.m_bEnable) Then
                .m_nTestAutoTimeCount = .m_nTestAutoTimeCount - nIntervalMillisec
                If (.m_nTestAutoTimeCount < 0) Then
                    .m_nTestAutoTimeCount = 0
                    .m_bEnable = False
                    bStop = True
                End If

                ' Update to Timeout Countdown Display per second
                .m_nTestAutoTimeCountUpto1Sec = .m_nTestAutoTimeCountUpto1Sec + nIntervalMillisec
                If (.m_nTestAutoTimeCountUpto1Sec >= 1000) Then
                    .m_nTestAutoTimeCountUpto1Sec = .m_nTestAutoTimeCountUpto1Sec - 1000
                    bUpdatePerSec = True
                End If
            End If
        End With
        '
        If (bStop) Then
            Dispose()
        End If
        If (bUpdatePerSec) Then
            Me.Text = "Remain Seconds : " + (m_tagTimeOutInfo.m_nTestAutoTimeCount / 1000).ToString()
        End If
    End Sub
    '
    Private Sub Timer1_Tick(sender As Object, e As EventArgs) Handles Timer1.Tick
        'If ((Me.Visible) And (m_dictMsgAnimation IsNot Nothing)) Then
        '    m_dictMsgAnimation(m_nIdx).Visible = False
        '    m_nIdx = m_nIdx + 1
        '    If (m_nIdx >= m_dictMsgAnimation.Count) Then
        '        m_nIdx = 0
        '        SetAllMsgsVisible()
        '    End If
        '    m_dictMsgAnimation(m_nIdx).Visible = True
        'Else
        '    If (m_dictMsgAnimation IsNot Nothing) Then
        '        SetAllMsgsVisible()
        '        m_nIdx = 0
        '    End If
        'End If

        TimerUpdateTimeoutUpdate(Timer1.Interval)
    End Sub

    Private Sub FormWaitTillDone_Disposed(sender As Object, e As EventArgs) Handles Me.Disposed
        TimerUpdateTimeoutStop()
    End Sub

    Private Sub FormWaitTillDone_FormClosed(sender As Object, e As FormClosedEventArgs) Handles MyBase.FormClosed
        'Stop Waiting Command, Don't do this --> It make
        'Form01_Main.IDGInterruptCommand()
        Form01_Main.GetInstance().Activate()
        TimerUpdateTimeoutStop()
        m_listLabelViewUpdate.Clear()
    End Sub

    Private Sub FormWaitTillDone_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        m_nIdx = 0
        m_bPass = False
        m_bCanceled = False
        Timer1.Enabled = True
        m_nAnswerMode = _ANS._PASS_FAIL

        'm_pLang = ClassServiceLanguages.GetInstance()
        'm_pLang.DoTranslateLanguage_FormWaitTillDone(Me)

        '=======[ Refresh To Topmost ]=========
        TopMost = False
        TopMost = True

    End Sub

    Private Sub SetAllMsgsVisible(Optional bVisible As Boolean = False)
        For Each iteratorSeek In m_listLabelViewUpdate
            iteratorSeek.Value.Visible = True
        Next
    End Sub

    Private Sub Button1_Click(sender As Object, e As EventArgs) Handles Button_Cancel.Click
        'Record Cancel Action Are performed
        m_bCanceled = True

        'Stop Waiting Command
        If (Not m_bNoInterruptCmd) Then
            Form01_Main.GetInstance().IDGInterruptCommand()
            Dispose()
        Else
            Button_Cancel.Visible = False
            'Wait Till External Window Close
            SetLabels("Wait Till Done")
            'SetLabels(m_pLang.GetMsgLangTranslation(ClassServiceLanguages._WTD_STRID.COMMON_WAIT_TILL_DONE))
        End If
        '
        'Form01_Main.GetInstance().bEngMode_TM_ItemSingleLoop = False

    End Sub

    Private Sub SetLabels(strMsg As String)
        Dim pRefMF As Form01_Main = Form01_Main.GetInstance()
        Dim fontNew As Font = Nothing
        Dim refTextBox_Result As Label

        For Each iteratorSeek In m_listLabelViewUpdate

            If (iteratorSeek.Key Is Nothing) Then
                'iteratorSeek .Key  = m_refTextBox_ResultMajor.Font
                Continue For
            End If
            refTextBox_Result = iteratorSeek.Value
            refTextBox_Result.Font = iteratorSeek.Key

            If (pRefMF.LogFindTextFontMatchSizeDependingOnSelectedFont(strMsg, iteratorSeek.Key, refTextBox_Result.Width, refTextBox_Result.Height, fontNew)) Then
                refTextBox_Result.Font = fontNew
            End If

            refTextBox_Result.Text = strMsg
            refTextBox_Result.Visible = True
        Next

    End Sub
    '
    Public Sub SetCancelMode(Optional ByVal bMode As Boolean = False, Optional ByVal strMsgCust As String = "", Optional ByVal bNoInterruptCmd As Boolean = False)
        Me.ControlBox = bMode
        Button_Cancel.Visible = bMode
        Button_PASS.Visible = False
        Button_FAIL.Visible = False
        m_bPass = False
        m_bNoInterruptCmd = bNoInterruptCmd

        If (strMsgCust.Length > 0) Then
            SetLabels(strMsgCust)
        Else
            SetLabels(m_strStandardMsg)
        End If
    End Sub
  
    '
    Public Sub Init()
        'Dim pLang As ClassServiceLanguages = ClassServiceLanguages.GetInstance()
        Button_PASS.Text = "PASSED" 'pLang.GetMsgLangTranslation(ClassServiceLanguages._WTD_STRID.COMMON_PASSED)
        Button_FAIL.Text = "FAILED" 'pLang.GetMsgLangTranslation(ClassServiceLanguages._WTD_STRID.COMMON_FAILED)
        Button_Cancel.Text = "CANCEL" 'pLang.GetMsgLangTranslation(ClassServiceLanguages._WTD_STRID.COMMON_CANCEL)
        'Must do this again since Load Event is gone without form startup again
        'm_pLang = pLang

        m_strStandardMsg = "WAIT" 'm_pLang.GetMsgLangTranslation(ClassServiceLanguages._WTD_STRID.COMMON_WAIT)
    End Sub
    '
    Public Sub SetPASSFAILMode(Optional ByVal bMode As Boolean = False, Optional ByVal strMsgCust As String = "")
        Me.ControlBox = False
        Button_Cancel.Visible = False
        Button_PASS.Visible = bMode
        Button_FAIL.Visible = bMode
        m_bPass = False
        '
        If (bMode) Then
            Select Case m_nAnswerMode
                Case _ANS._PASS_FAIL
                    Button_PASS.Text = "PASS" 'm_pLang.GetMsgLangTranslation(ClassServiceLanguages._WTD_STRID.COMMON_PASSED)
                    Button_FAIL.Text = "FAIL" 'm_pLang.GetMsgLangTranslation(ClassServiceLanguages._WTD_STRID.COMMON_FAILED)
                Case _ANS._SEEN_NOCHANGE
                    Button_PASS.Text = "SEEN" 'm_pLang.GetMsgLangTranslation(ClassServiceLanguages._WTD_STRID.COMMON_SEEN)
                    Button_FAIL.Text = "UNSEEN" 'm_pLang.GetMsgLangTranslation(ClassServiceLanguages._WTD_STRID.COMMON_UNSEEN)
                Case _ANS._HEAR_NOSOUND
                    Button_PASS.Text = "YES" 'm_pLang.GetMsgLangTranslation(ClassServiceLanguages._WTD_STRID.COMMON_SOUND_YES)
                    Button_FAIL.Text = "NO" 'm_pLang.GetMsgLangTranslation(ClassServiceLanguages._WTD_STRID.COMMON_SOUND_NO)
            End Select
        End If
        '
        If (strMsgCust.Length > 0) Then
            SetLabels(strMsgCust)
        Else
            SetLabels(m_strStandardMsg)
        End If
    End Sub

    Private Sub Button2_PASS_Click(sender As Object, e As EventArgs) Handles Button_PASS.Click
        m_bPass = True
        Dispose()
    End Sub

    Private Sub Button3_FAIL_Click(sender As Object, e As EventArgs) Handles Button_FAIL.Click
        m_bPass = False
        Dispose()
    End Sub

    ReadOnly Property bIsPass As Boolean
        Get
            Return m_bPass
        End Get
    End Property

    Public Sub New()

        ' This call is required by the designer.
        InitializeComponent()

        ' Add any initialization after the InitializeComponent() call.
        'm_listLabelViewUpdate = New List(Of Label) From {
        '    Label1,
        '    Label2,
        '    Label3,
        '    Label4,
        '    Label5
        '    }
        m_listLabelViewUpdate = New List(Of KeyValuePair(Of Font, Label)) From {
        New KeyValuePair(Of Font, Label)(Label5.Font, Label5)
        }
        '
        m_bServiceMode = False
    End Sub

#If 0 Then
         '
    Public Sub SetCancelModeLang(Optional ByVal bMode As Boolean = False, Optional ByVal nMsgIDCust As ClassServiceLanguages._WTD_STRID = ClassServiceLanguages._WTD_STRID._XX, Optional ByVal bNoInterruptCmd As Boolean = False)
        Dim pLang As ClassServiceLanguages = ClassServiceLanguages.GetInstance()
        Me.ControlBox = bMode
        Button_Cancel.Visible = bMode
        Button_PASS.Visible = False
        Button_FAIL.Visible = False
        m_bPass = False
        m_bNoInterruptCmd = bNoInterruptCmd


        If (nMsgIDCust = ClassServiceLanguages._WTD_STRID._XX) Then
            nMsgIDCust = ClassServiceLanguages._WTD_STRID.COMMON_WAIT
        End If
        SetLabels(pLang.GetMsgLangTranslation(nMsgIDCust))
    End Sub

    Private Sub FormWaitTillDone_VisibleChanged(sender As Object, e As EventArgs) Handles MyBase.VisibleChanged
        Dim btn As Button

        If (Button_PASS.Visible) Then
            btn = Button_PASS
        Else
            btn = Button_Cancel
        End If


        If (btn.Visible) Then
            Dim pt As Point
            Static s_pMF As Form01_Main = Form01_Main.GetInstance()
            pt = btn.PointToScreen(New Point(btn.Width \ 2, btn.Height \ 2))
            pt.X = pt.X + s_pMF.LocXOff
            pt.Y = pt.Y + s_pMF.LocYOff
            'Form01_Main.GetInstance().LogLn("Form X, Y = " + Location.X.ToString() + "," + Location.Y.ToString())
            's_pMF.LogLn("Wait Form X, Y = " + Location.X.ToString() + "," + Location.Y.ToString())

            Cursor.Position = pt
        End If
    End Sub
#End If

End Class