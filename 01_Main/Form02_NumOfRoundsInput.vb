﻿Public Class Form02_NumOfRoundsInput

    Private m_nNumOfRounds As Integer

    '
    Private Shared m_pInst As Form02_NumOfRoundsInput = Nothing
    Public ReadOnly Property prop_01_GetNumOfRounds As Integer
        Get
            Return m_nNumOfRounds
        End Get
    End Property
    Public Shared Function GetInstance() As Form02_NumOfRoundsInput
        If (m_pInst Is Nothing) Then
            m_pInst = New Form02_NumOfRoundsInput()
        End If
        If (m_pInst.IsDisposed) Then
            m_pInst = New Form02_NumOfRoundsInput()
        End If
        '
        Return m_pInst
    End Function


    Private Sub Btn_Done_Click(sender As Object, e As EventArgs) Handles Btn_Done.Click
        'Assign Value First
        m_nNumOfRounds = CType(NumUpDn_Rnd.Value, Integer)
        Hide()
        Me.DialogResult = System.Windows.Forms.DialogResult.OK
    End Sub

    Private Sub Btn_Cancel_Click(sender As Object, e As EventArgs) Handles Btn_Cancel.Click
        Hide()
        Me.DialogResult = System.Windows.Forms.DialogResult.Cancel
    End Sub

    Private Sub Form02_NumOfRoundsInput_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        m_nNumOfRounds = 100
    End Sub
End Class