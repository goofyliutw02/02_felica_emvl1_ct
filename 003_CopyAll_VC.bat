@echo off
REM usage ex: CopyAll.bat ..\SVNLocal\trunk 00_idt_usb_hid_dll_tw idt_usb_hid_dll_tw
REM CopyAll.bat [DestDir-Uppder] [Sub-ProjectFolder] [VC.ProjectName]
setlocal EnableDelayedExpansion

REM *******[VC Porject Folder]*******

set "dir00=%2"
set "vardst=%1\%dir00%"
echo dir00=%dir00%
echo vardst=%vardst%

REM xcopy "\*.cpp" "%1\%2" /Y /C

xcopy %dir00%\*.vcxproj %vardst% /Y /C
xcopy %dir00%\*.vcxproj.* %vardst% /Y /C
xcopy %dir00%\*.cpp %vardst% /Y /C
xcopy %dir00%\*.h %vardst% /Y /C
xcopy %dir00%\*.def %vardst% /Y /C
xcopy %dir00%\*.lib %vardst% /Y /C

REM the following is copy vc solution/project files, 
REM VC2012 generates 2 hidden files then use /H
xcopy %dir00%\%3.* %vardst% /Y /H /C

xcopy %dir00%\*.rc %vardst% /Y /C
xcopy %dir00%\*.bat %vardst% /Y /C
xcopy %dir00%\res\*.* %vardst%\res\ /Y /C

REM Special For Excel Datbase - Admin Key Injection
if exist %dir00%\msado15.tlh (
xcopy %dir00%\msado15.*  %vardst% /Y /C
) 

REM Special For [01_Main] "Main TS"
echo ***[ Special For %dir00% ]***
if "%dir00%" == "01_Main" (
xcopy "%dir00%\My Project"  "%vardst%\My Project\" /Y /E /C
xcopy "%dir00%\Resources"  "%vardst%\Resources\" /Y /E /C
xcopy "%dir00%\Service References"  "%vardst%\Service References\" /Y /E /C
)

REM Special For [00_idt_usb_hid_dll_tw] "IDT USB HID Lib"
echo ***[ Special For %dir00% ]***
if "%dir00%" == "00_idt_usb_hid_dll_tw" (
REM xcopy %dir00%\Settings  %vardst%\Settings\ /Y /E /C
REM xcopy %dir00%\multilang  %vardst%\multilang\ /Y /E /C
)

Special For [03_AdminInj] "Admin Key Injection"
echo ***[ Special For %dir00% ]***
if "%dir00%" == "03_AdminInj" (
xcopy %dir00%\Settings  %vardst%\Settings\ /Y /E /C
xcopy %dir00%\multilang  %vardst%\multilang\ /Y /E /C
)

REM *******[VC Porject Folder]*******

@echo on