@echo off
REM usage ex: CopyAll.bat ..\SVNLocal\trunk 00_idt_usb_hid_dll_tw idt_usb_hid_dll_tw
REM CopyAll.bat [DestDir-Uppder] [Sub-ProjectFolder] [VC.ProjectName]
setlocal EnableDelayedExpansion

REM *******[Global Vars]*******
set "dir00=%2"
set "vardst=%1\%dir00%"

echo dir00=%dir00%
echo vardst=%vardst%

REM *******[VB Porject Folder]*******
xcopy %dir00%\*.vb %vardst% /Y
xcopy %dir00%\*.resx %vardst% /Y
xcopy %dir00%\Resource\*.* %vardst%\Resource /Y
xcopy %dir00%\*.lib %vardst% /Y
xcopy %dir00%\%3.* %vardst% /Y /H
xcopy %dir00%\*.config %vardst% /Y

REM Special For [01_Main] "Main TS"
echo ***[ Special For %dir00% ]***
if "%dir00%" == "01_Main" (
xcopy "%dir00%\My Project"  "%vardst%\My Project\" /Y /E
xcopy "%dir00%\Resources"  "%vardst%\Resources\" /Y /E
xcopy "%dir00%\Service References"  "%vardst%\Service References\" /Y /E
)

REM *******[VB Porject Folder]*******

@echo on