@echo off
REM usage ex: call CopyAll_InnoSetup.bat ..\SVNLocal\trunk InnoSetup\TS128\Src
REM CopyAll.bat [%1=DestDir-Uppder] [%2=Sub-ProjectFolder]
setlocal EnableDelayedExpansion

REM *******[InnoSetup Porject Folder]*******

set "dir00=%2"
set "vardst=%1\%dir00%"
set "curdir=%cd%"
set "setupdirSrc=%dir00%\..\setup"
set "setupdirDst=%vardst%\..\setup"

echo dir00=%dir00%
echo vardst=%vardst%
echo curdir
echo Src = %setupdirSrc%
echo Dst = %setupdirDst%
xcopy %dir00%\*.* %vardst% /Y /C
xcopy %dir00%\Images %vardst%\Images\ /Y /E /C

REM [Dir = "Logs"]
cd %dir00%\Logs
call cleanup.bat
cd %curdir%
xcopy %dir00%\Logs %vardst%\Logs\ /Y /E /C

REM [Dir = "PartNumber"]
REM set "curdir=%cd%"
cd %dir00%\PartNumber
call cleanup.bat
cd %curdir%
xcopy %dir00%\PartNumber %vardst%\PartNumber\ /Y /E /C

REM [Dir = "Transaction" Config]
xcopy %dir00%\Transaction %vardst%\Transaction\ /Y /E /C

REM [Dir = "KI"]
cd %dir00%\KI
REM Since Admin Key Injection Log File = *.txt.
REM To Remove them before being copied to somewhere
del *.txt
cd %curdir%
xcopy %dir00%\KI %vardst%\KI\ /Y /E /C

REM [Dir = "setup"]
cd %curdir%
md %setupdirDst%
xcopy %setupdirSrc%\*.bat %setupdirDst%\*.bat /Y /C
xcopy %setupdirSrc%\Rename*.exe %setupdirDst%\Rename*.exe /Y /C
xcopy %setupdirSrc%\*.doc* %setupdirDst%\*.doc* /Y /C
REM xcopy %setupdirSrc%\Setup*.exe %setupdirDst%\Setup*.exe /Y /C
REM *******[InnoSetup Porject Folder]*******

@echo on