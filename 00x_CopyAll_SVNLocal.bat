REM ********[ Global Vars ]********
set "curdir=%cd%"
set "svnpathsub=..\SVNLocal\trunk"
set "idtprj=FelicaSDK_EMV_L1"
set "idtprjDir999_Generic=01_Main"
set "idtprjDirExt01=idt_usb_hid_dll_tw"
set "idtprjDirInnoSrc=InnoSetup"
set "idtprjDirInnoSrcMifare=InnoSetup-Mifare"
set "cmptoolExeFull=C:\Program Files\KDiff3\kdiff3.exe"
set "cmptoolExeFullx86=C:\Program Files (x86)\KDiff3\kdiff3.exe"


REM goto Test
REM goto SkipKDiff3

REM ********[Diff Code to shows up the difference between 
REM [_Current] and [SVNLocal\trunk] for User Notification]*******
pause
"%cmptoolExeFull%" "%curdir%" "%curdir%\%svnpathsub%"
pause
echo Continue to copy necessary files and folders to [SVNLocal]...
:SkipKDiff3

REM ********[Start to Copy to SVNLocal Directory]*******
REM - Main Directory -
xcopy *.bat %svnpathsub% /Y
xcopy %idtprj%.* %svnpathsub% /Y /H
REM xcopy Doc "%svnpathsub%"\Doc\ /E /Y

REM - Sub Directory -

call 002_CopyAll_VB.bat "%svnpathsub%" %idtprjDir999_Generic% %idtprj%.vbproj
xcopy %idtprjDir999_Generic%\Main.* %svnpathsub%\%idtprjDir999_Generic%\ /Y /H
xcopy %idtprjDir999_Generic%\Logo_Icon.* %svnpathsub%\%idtprjDir999_Generic%\ /Y /H
REM Main.* files

call 003_CopyAll_VC.bat "%svnpathsub%" 00_%idtprjDirExt01% %idtprjDirExt01%.vcxproj
REM call 003_CopyAll_VC.bat "%svnpathsub%" 03_AdminInj NTS.vcxproj
REM 003_CopyAll_VC.bat "%svnpathsub%" 02_CallDLL_Example_Mfc CallDLL_Example_Mfc.vcxproj
REM
call 001_CopyAll_InnoSetup.bat "%svnpathsub%" "..\%idtprjDirInnoSrc%\Src"
call 001_CopyAll_InnoSetup.bat "%svnpathsub%" "..\%idtprjDirInnoSrcMifare%\Src"
REM
call 004_CopyAll_DebugRelease.bat "%svnpathsub%" Debug
:Test
call 004_CopyAll_DebugRelease.bat "%svnpathsub%" Release

REM - Open SVNLocal Folder and ReleaseNote.txt for SVN Update Log
echo %svnpathsub%
explorer "%svnpathsub%"
REM
start "" "..\%idtprjDirInnoSrc%\Src\ReleaseNote.txt"
start "" "..\%idtprjDirInnoSrcMifare%\Src\ReleaseNote.txt"