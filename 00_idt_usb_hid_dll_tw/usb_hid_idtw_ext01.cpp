/*
    2017 May 26, goofy_liu@idtechproductsions.com.tw
    Note: Development of these DLL export "C" functions for Java LabTool External DLL 
          Invocation (JNative Interface).
*/
#include "stdafx.h"
#include "idt_usb_hid.h"
#include "usb_hid_idtw.h"


#include <string.h>
#include <iostream>

using namespace std;

#ifdef _DEBUG
#define new DEBUG_NEW
#endif

//
//TODO: If this DLL is dynamically linked against the MFC DLLs,
//		any functions exported from this DLL which call into
//		MFC must have the AFX_MANAGE_STATE macro added at the
//		very beginning of the function.
//
//		For example:
//
//		extern "C" BOOL PASCAL EXPORT ExportedFunction()
//		{
//			AFX_MANAGE_STATE(AfxGetStaticModuleState());
//			// normal function body here
//		}
//
//		It is very important that this macro appear in each
//		function, prior to any calls into MFC.  This means that
//		it must appear as the first statement within the 
//		function, even before any object variable declarations
//		as their constructors may generate calls into the MFC
//		DLL.
//
//		Please see MFC Technical Notes 33 and 58 for additional
//		details.
//

// When Open S
static DWORD s_dwWaitLoop = 0;

extern "C" {
     /*
    */
    void __stdcall USBHIDSetWaitLoopStart(){
        s_dwWaitLoop = 0;
    }

    void __stdcall USBHIDSetWaitLoopEnd(){
        s_dwWaitLoop = 1;
    }

    // Default 
    DWORD pfuncCBRxWait(){
        DWORD dwTmp = s_dwWaitLoop;
        s_dwWaitLoop = 0; // Always Auto Reset in Wait Loop for Rx
        return dwTmp;
    }
        //
    int __stdcall USBHIDOpen_JLT_Default_u8() //int nVID,int nPID)
    {
        int nVID = 0x0ACD;
        int nPID = 0x4210;
        /*CString strTmp = "";
        strTmp.Format ("%04X-%04X",nVID,nPID);
        ::AfxMessageBox(strTmp,MB_OK | MB_ICONWARNING);*/
        return USBHIDOpen_JLT_u8(nVID,nPID);
    }

    DWORD __stdcall USBHIDSendAndResponse_DefCallbackWait_u32  (DWORD u32VID,DWORD u32PID, LPBYTE pSendData, DWORD u32SendSize, LPBYTE pRecvData, DWORD u32RecvDataBufSize,DWORD u32TimeoutMS)
    {
        // Clear Wait Flag first
        USBHIDSetWaitLoopStart();
        return USBHIDSendAndResponse_u32(u32VID, u32PID, pSendData, u32SendSize, pRecvData, u32RecvDataBufSize, u32TimeoutMS, pfuncCBRxWait);
    }
    //

     DWORD __stdcall USBHIDRecvData_DefCallbackWait_u32(DWORD u32VID,DWORD u32PID, LPBYTE pbyteRecvOut, DWORD u32RecvOutSize, DWORD u32WaitMs)
    {
        // Clear Wait Flag first
        USBHIDSetWaitLoopStart();
        return USBHIDRecvData_u32(u32VID, u32PID,  pbyteRecvOut,  u32RecvOutSize, u32WaitMs, pfuncCBRxWait);
    }

     //
    DWORD __stdcall USBHIDRecvDataIDGCmd_DefCallbackWait_u32(DWORD u32VID,DWORD u32PID, LPBYTE pbyteRecvOut, DWORD u32RecvOutSize, DWORD u32WaitMs)
    {
        // Clear Wait Flag first
        USBHIDSetWaitLoopStart();
        return USBHIDRecvDataIDGCmd_u32(u32VID, u32PID,  pbyteRecvOut, u32RecvOutSize, u32WaitMs, pfuncCBRxWait);
    }

}// extern "C"