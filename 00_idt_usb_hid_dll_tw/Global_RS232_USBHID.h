/*
    Description:
        
*/

#if !defined(__GLOBAL_RS232_USBHID_H)
#define __GLOBAL_RS232_USBHID_H
//

#define MAX_BUF_SIZE 512
#define MAX_BUF_SIZE_4K 4096

//
#define TMP_BUF_LEN 512

#define TIMEOUT_RS232_TXRX_MS       65000
#define TIMEOUT_USBHID_TXRX_MS       65000


#define TIME_CMD_INTERVAL   300 // milli-seconds

#define PORT_RS232 0
#define PORT_USB 1
#define PORT_UNKNOWN 99

//
typedef  DWORD (* pFUNC_APP_SLEEP)(void);
typedef  void (* pFUNC_APP_SLEEP2)(DWORD dwTimeMS);

//
#endif // __GLOBAL_RS232_USBHID_H