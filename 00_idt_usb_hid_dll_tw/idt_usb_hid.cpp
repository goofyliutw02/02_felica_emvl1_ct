// idt_usb_hid.cpp : implementation file
//

#include "stdafx.h"
#include "idt_usb_hid.h"
#include "TConvert.h"
#include <afx.h>
#include <iostream>
//
#pragma comment(lib,"hid.lib")
#pragma comment(lib,"Setupapi.lib")
//
#define MAX_FRAME_SINGLE_SIZE 63
#define MAX_FRAME_TOTAL_SIZE 4096

#define TIME_OUT


using namespace std;

typedef enum {
    FSM_RD,
    FSM_IO_ERR,
    FSM_IO_PENDING
} ENU_FSM;

#define BUF_MAX_SIZE 128
//#define LOG_INFO(szMsg) {cout << szMsg << endl;}
#define LOG_INFO(szMsg) {TRACE("%s\n",szMsg );}
#if !defined(LOG_INFO)
#define LOG_INFO(szMsg) {}
#endif
//
#define LOG_INFO_BREAK() LOG_INFO("==============================================")

//
//static HANDLE m_hidDevice = INVALID_HANDLE_VALUE;

//
CIdtUsbHid::CIdtUsbHid():
    m_bIsDebug(false),
    m_u32VID(0),  m_u32PID (0),
    m_nOffsetV3Flag(9), // V2 or V3 identifier,where 9 = 1(RID)+8 '2'(0x32) or '3'(0x33)
    m_hidDevice (INVALID_HANDLE_VALUE),
    m_ReadEvent ( NULL),
    m_WriteEvent(NULL),
    m_nUsbDevMode(-1), // -1:Unknown; 0:USBHID; 1:USBKB
    m_nOffsetCmd(11)
{
    LOG_INFO("DLL Loaded and Start")
    //::AfxMessageBox(_T("DLL Loaded and Start"),MB_OK|MB_ICONASTERISK);
}

CIdtUsbHid::~CIdtUsbHid()
{
    LOG_INFO("DLL End and Unloaded")
        Close ();
}

BOOL  CIdtUsbHid::bIsOpen()
{
    return (m_hidDevice != INVALID_HANDLE_VALUE);
}

BOOL  CIdtUsbHid::bIsNotSameDev(DWORD u32VID,DWORD u32PID)
{
    if (m_u32VID != u32VID )
        return TRUE;
    if (m_u32PID != u32PID )
        return TRUE;

    return FALSE;
}


/*
  Description: LookDevice(...)
        Searching Wanted Device by [uVID,uPID] in System Seek Loop

  Parameters:
	[IN]
        uVID,uPID
	[OUT]
        N/A
  Return:
    = 1 : Found
    = 0 : Error / Not Found
*/
DWORD CIdtUsbHid::LookDevice_V17May23(UINT uVID, UINT uPID) 
{
  // debug version
    GUID			HidGuid;	    //UID of HID device class
    HDEVINFO		hDevInfoSet = INVALID_HANDLE_VALUE;	//handle to all hid device's info set.
    SP_DEVICE_INTERFACE_DATA	devIfData;  //All HID devices interface data.
    SP_DEVICE_INTERFACE_DETAIL_DATA *pIfDetail = NULL; //interface detail
    HIDD_ATTRIBUTES		attrs;		//Structrue with VID and PID
    ULONG			len, required;	//length of interface datail structure
    PHIDP_PREPARSED_DATA	preparsedData;
    //char szDevicePath[1025] = {""};
    CString strDbgMsg;

    if (bIsOpen())
    {
        if ((m_u32VID==uVID) && (m_u32PID==uPID))
            return 1;

        Close();
        //return 0;
    }

    // reset to Unknown USB Device Mode
    //
    m_nUsbDevMode = -1;
    HidD_GetHidGuid(&HidGuid);


    if( (hDevInfoSet=SetupDiGetClassDevs(&HidGuid, NULL, NULL,
        DIGCF_PRESENT|DIGCF_INTERFACEDEVICE)) == INVALID_HANDLE_VALUE)
    {
        return 0;
    }

    devIfData.cbSize = sizeof(devIfData);
    int index=0;
    while( SetupDiEnumDeviceInterfaces(hDevInfoSet, 0, 
        &HidGuid, index++, &devIfData) != 0)
    { 
        //this call should fail, just get the detail structure length
        SetupDiGetDeviceInterfaceDetail( hDevInfoSet, &devIfData, NULL, 0, &len, NULL);	

        pIfDetail = (SP_DEVICE_INTERFACE_DETAIL_DATA*)malloc(len);
        if (NULL == pIfDetail){
            SetupDiDestroyDeviceInfoList(hDevInfoSet); hDevInfoSet = INVALID_HANDLE_VALUE;
            return 0;
        }
        pIfDetail->cbSize = sizeof(SP_DEVICE_INTERFACE_DETAIL_DATA);
        if( ! SetupDiGetDeviceInterfaceDetail(hDevInfoSet, &devIfData, pIfDetail, len, &required, NULL))
        {
            SetupDiDestroyDeviceInfoList(hDevInfoSet); hDevInfoSet = INVALID_HANDLE_VALUE;
            free(pIfDetail); pIfDetail = NULL;
            return 0;
        }
        //m_hidDevice=CreateFile(pIfDetail->DevicePath, GENERIC_WRITE|GENERIC_READ, 
        //    FILE_SHARE_READ|FILE_SHARE_WRITE,(LPSECURITY_ATTRIBUTES) NULL,
        //    OPEN_EXISTING, FILE_FLAG_OVERLAPPED, NULL);
        m_hidDevice=CreateFile(pIfDetail->DevicePath, GENERIC_WRITE|GENERIC_READ, 
            FILE_SHARE_READ|FILE_SHARE_WRITE,(LPSECURITY_ATTRIBUTES) NULL,
            OPEN_EXISTING, FILE_FLAG_OVERLAPPED, NULL);
        if(m_hidDevice==INVALID_HANDLE_VALUE)
        {
            //			SetupDiDestroyDeviceInfoList(hDevInfoSet);
            free(pIfDetail); pIfDetail = NULL;
            continue;//return 0;
        }


        ::memset(m_devPathName, 0 , 1024);
#if defined(UNICODE)
        _tochar tot( pIfDetail->DevicePath );
        strncpy_s(m_devPathName, 1024,tot, strlen(tot));
#else
        strncpy_s(m_devPathName, 1024,pIfDetail->DevicePath, strlen(pIfDetail->DevicePath));
#endif
        free(pIfDetail); pIfDetail = NULL;
        if( !HidD_GetAttributes(m_hidDevice, &attrs))
        {
            Close();
            continue;
        }

        if( attrs.VendorID!=uVID || attrs.ProductID!=uPID)
        {
            Close();
            continue;
        }

        if(!HidD_GetPreparsedData(m_hidDevice, &preparsedData))
        {
            Close();
            SetupDiDestroyDeviceInfoList(hDevInfoSet); hDevInfoSet = INVALID_HANDLE_VALUE;	
            return 0;
        }

        if(!HidP_GetCaps(preparsedData, &m_caps))
        {
            Close();
            HidD_FreePreparsedData(preparsedData);
            SetupDiDestroyDeviceInfoList(hDevInfoSet); hDevInfoSet = INVALID_HANDLE_VALUE;		
            return 0;
        }
        HidD_FreePreparsedData(preparsedData);
        index-=1;
        break;	//OK, to here, we find one right device, and get it's caps.
    }

    // Make the interface seeking loop finish as possible.
    while(SetupDiEnumDeviceInterfaces(hDevInfoSet, 0, 
            &HidGuid, index++, &devIfData)!=0);

    if (NULL != pIfDetail){
        free(pIfDetail); pIfDetail = NULL;
    }
    //
    if (hDevInfoSet){
        
        SetupDiDestroyDeviceInfoList(hDevInfoSet); hDevInfoSet = INVALID_HANDLE_VALUE;	
    }

    if (INVALID_HANDLE_VALUE == m_hidDevice)
        return 0;

    //=====[ 2017 Sept 11, check USB Device Type. ]======
    if ((m_caps.FeatureReportByteLength == 0x08) &&
       (m_caps.InputReportByteLength == 0x40) &&
       (m_caps.FeatureReportByteLength == 0x40)){
           // m_nUsbDevMode = 0 --> USB-HID
           // m_caps.OutputReportByteLength = 0x40;
           // m_caps.InputReportByteLength = 0x40;
           // m_caps.FeatureReportByteLength = 0x08;
           m_nUsbDevMode = 0;
           if(m_ReadEvent == 0)
           {
               m_ReadEvent = CreateEvent(NULL, TRUE, TRUE, NULL);
           }
           if(m_WriteEvent == 0)
           {
               m_WriteEvent = CreateEvent(NULL, TRUE, FALSE, NULL);
           }

           //Set the members of the overlapped structure.
           m_overlapped.hEvent = m_ReadEvent;
           m_overlapped.Offset = 0;
           m_overlapped.OffsetHigh = 0;
    } else
    if ((m_caps.FeatureReportByteLength == 0x08) &&
       (m_caps.InputReportByteLength == 0x00) &&
       (m_caps.FeatureReportByteLength == 0x00)){
           // m_nUsbDevMode = 1 --> USB-KB, KB Wedge
           // m_caps.OutputReportByteLength = 0x00;
           // m_caps.InputReportByteLength = 0x00;
           // m_caps.FeatureReportByteLength = 0x08;
           m_nUsbDevMode = 1;
    }
  
    m_u32VID =uVID;
    m_u32PID =uPID;
    return 1;	
}

DWORD CIdtUsbHid::LookDevice(UINT uVID, UINT uPID) 
{
    // debug version
    GUID			HidGuid;	    //UID of HID device class
    HDEVINFO		hDevInfoSet = INVALID_HANDLE_VALUE;	//handle to all hid device's info set.
#define CB_SIZE2 1024
    BYTE            aryByteBuf[CB_SIZE2];
    SP_DEVICE_INTERFACE_DATA	devIfData;  //All HID devices interface data.
    SP_DEVICE_INTERFACE_DETAIL_DATA *pIfDetail = (PSP_DEVICE_INTERFACE_DETAIL_DATA)aryByteBuf; //interface detail
    HIDD_ATTRIBUTES		attrs;		//Structrue with VID and PID
    ULONG			len, required;	//length of interface datail structure
    PHIDP_PREPARSED_DATA	preparsedData;
    //char szDevicePath[1025] = {""};
    CString strDbgMsg;
    /*
    2017 May 25, Corruption of DLL function for multi-thread Java Program.
    ====[ Note for Multi-thread environment, ex: JAVA ]=====
    1.Why not direct usage of m_hidDevice ?
    Ans: We hope every thing / info / stuff is ready then assign the handle value to m_hidDevice.
    */
    HANDLE hidDevice = INVALID_HANDLE_VALUE;

    if (bIsOpen())
    {
        if ((m_u32VID==uVID) && (m_u32PID==uPID))
            return 1;

        Close();
        //return 0;
    }

#if (0)
#define MSG_END(cstrXXXMsg) { CString strDbgMsg = cstrXXXMsg; \
    strDbgMsg.Format(_T("HldrID=%08X,%s"),m_hidDevice,strDbgMsg); \
    int nMsgID = ::AfxMessageBox (strDbgMsg+_T(" [CHK]"),MB_YESNO|MB_ICONWARNING); \
    if (nMsgID==IDNO){ \
        if (hDevInfoSet!=INVALID_HANDLE_VALUE) { \
            ::SetupDiDestroyDeviceInfoList(hDevInfoSet); \
            hDevInfoSet = INVALID_HANDLE_VALUE; \
        } \
        Close(); \
        return 0; \
    } \
}

#define MSG_END_LoopBrk(cstrXXXMsg) { CString strDbgMsg = cstrXXXMsg; \
    strDbgMsg.Format(_T("HldrID=%08X,%s"),m_hidDevice,strDbgMsg); \
    int nMsgID = ::AfxMessageBox (strDbgMsg+_T(" [CHK]"),MB_YESNO|MB_ICONWARNING); \
    if (nMsgID==IDNO){ \
        if (hDevInfoSet!=INVALID_HANDLE_VALUE) { \
            ::SetupDiDestroyDeviceInfoList(hDevInfoSet); \
            hDevInfoSet = INVALID_HANDLE_VALUE; \
        } \
       Close(); \
        return 0; \
    } \
}
#else
#define MSG_END(cstrXXXMsg)
#define MSG_END_LoopBrk(cstrXXXMsg)
#endif

    MSG_END("000-");

    HidD_GetHidGuid(&HidGuid);

    MSG_END("000-1");

    if( (hDevInfoSet=SetupDiGetClassDevs(&HidGuid, NULL, NULL,
        DIGCF_PRESENT|DIGCF_INTERFACEDEVICE)) == INVALID_HANDLE_VALUE)
    {
        return 0;
    }

    MSG_END("000-2");

    devIfData.cbSize = sizeof(devIfData);
    int index=0;
    while( SetupDiEnumDeviceInterfaces(hDevInfoSet, 0, 
        &HidGuid, index++, &devIfData) != 0)
    { 
        //this call should fail, just get the detail structure length
        SetupDiGetDeviceInterfaceDetail( hDevInfoSet, &devIfData, NULL, 0, &len, NULL);	

        /*pIfDetail = (SP_DEVICE_INTERFACE_DETAIL_DATA*)malloc(len);
        pIfDetail->cbSize = sizeof(SP_DEVICE_INTERFACE_DETAIL_DATA);*/
        pIfDetail->cbSize = sizeof(SP_DEVICE_INTERFACE_DETAIL_DATA);
        if( ! SetupDiGetDeviceInterfaceDetail(hDevInfoSet, &devIfData, pIfDetail, len, &required, NULL))
        {
            SetupDiDestroyDeviceInfoList(hDevInfoSet); hDevInfoSet = INVALID_HANDLE_VALUE;
            //free(pIfDetail); pIfDetail = NULL;
            return 0;
        }
        //m_hidDevice=CreateFile(pIfDetail->DevicePath, GENERIC_WRITE|GENERIC_READ, 
        //    FILE_SHARE_READ|FILE_SHARE_WRITE,(LPSECURITY_ATTRIBUTES) NULL,
        //    OPEN_EXISTING, FILE_FLAG_OVERLAPPED, NULL);
        hidDevice=::CreateFile(pIfDetail->DevicePath, GENERIC_WRITE|GENERIC_READ, 
            0,(LPSECURITY_ATTRIBUTES) NULL,
            OPEN_EXISTING, FILE_FLAG_OVERLAPPED, NULL);
        if(hidDevice==INVALID_HANDLE_VALUE)
        {
            //			SetupDiDestroyDeviceInfoList(hDevInfoSet);
            //free(pIfDetail); pIfDetail = NULL;
            continue;//return 0;
        }

        MSG_END_LoopBrk("001-");

        ::memset(m_devPathName, 0 , 1024);
        // Note, must reserve String NULL char at the 
#if defined(UNICODE)
        _tochar tot( pIfDetail->DevicePath );
        strncpy_s(m_devPathName, 1023,tot, strlen(tot));
#else
        strncpy_s(m_devPathName, 1023,pIfDetail->DevicePath, strlen(pIfDetail->DevicePath));
#endif


        //free(pIfDetail); pIfDetail = NULL;
        if( !HidD_GetAttributes(hidDevice, &attrs))
        {
            continue;//return 0;
        }

        MSG_END_LoopBrk(m_devPathName);

        if( attrs.VendorID!=uVID || attrs.ProductID!=uPID)
        {
            CloseHandle(hidDevice);
            hidDevice = INVALID_HANDLE_VALUE;		
            continue;
        }

        MSG_END_LoopBrk("002-2");

        if(!HidD_GetPreparsedData(hidDevice, &preparsedData))
        {
            CloseHandle(hidDevice);
            hidDevice = INVALID_HANDLE_VALUE;
            SetupDiDestroyDeviceInfoList(hDevInfoSet);			
            return 0;
        }

        MSG_END_LoopBrk("003-");

        if( ! HidP_GetCaps(preparsedData, &m_caps))
        {
            CloseHandle(hidDevice);
            hidDevice = INVALID_HANDLE_VALUE;
            HidD_FreePreparsedData(preparsedData);
            SetupDiDestroyDeviceInfoList(hDevInfoSet);	hDevInfoSet = INVALID_HANDLE_VALUE;			
            return 0;
        }
        HidD_FreePreparsedData(preparsedData);
        index-=1;
        break;	//OK, to here, we find one right device, and get it's caps.
    }

    // Make the interface seeking loop finish as possible.
    while(SetupDiEnumDeviceInterfaces(hDevInfoSet, 0, 
            &HidGuid, index++, &devIfData)!=0);

    MSG_END("004-");

    /*if (NULL != pIfDetail){
        free(pIfDetail); pIfDetail = NULL;
    }*/
    //
    if (INVALID_HANDLE_VALUE != hDevInfoSet){
        
        if (TRUE != SetupDiDestroyDeviceInfoList(hDevInfoSet)){
          /* strDbgMsg = "004-2";
           strDbgMsg.Format(_T("004-2,Last Err = %d"),GetLastError());
           MSG_END(strDbgMsg);*/
        }
        hDevInfoSet = INVALID_HANDLE_VALUE;
    }


    if (INVALID_HANDLE_VALUE == hidDevice)
        return 0;

      //=====[ 2017 Sept 11, check USB Device Type. ]======
    if ((m_caps.FeatureReportByteLength == 0x08) &&
        (
       ((m_caps.InputReportByteLength == 0x40) &&
       (m_caps.FeatureReportByteLength == 0x40)) ||
       ((m_caps.InputReportByteLength == 0x41) &&
       (m_caps.FeatureReportByteLength == 0x41))
       )){
           // m_nUsbDevMode = 0 --> USB-HID
           // m_caps.OutputReportByteLength = 0x40;
           // m_caps.InputReportByteLength = 0x40;
           // m_caps.FeatureReportByteLength = 0x08;
           m_nUsbDevMode = 0;
           //
           if(m_ReadEvent == 0)
           {
               m_ReadEvent = CreateEvent(NULL, TRUE, TRUE, NULL);
           }
           if(m_WriteEvent == 0)
           {
               m_WriteEvent = CreateEvent(NULL, TRUE, FALSE, NULL);
           }

           //Set the members of the overlapped structure.
           m_overlapped.hEvent = m_ReadEvent;
           m_overlapped.Offset = 0;
           m_overlapped.OffsetHigh = 0;
    } else
    if ((m_caps.FeatureReportByteLength == 0x08) &&
       (m_caps.InputReportByteLength == 0x00) &&
       (m_caps.FeatureReportByteLength == 0x00)){
           // m_nUsbDevMode = 1 --> USB-KB, KB Wedge
           // m_caps.OutputReportByteLength = 0x00;
           // m_caps.InputReportByteLength = 0x00;
           // m_caps.FeatureReportByteLength = 0x08;
           m_nUsbDevMode = 1;
    }

    m_u32VID =uVID;
    m_u32PID =uPID;

    // finally we give value here
    m_hidDevice = hidDevice;

    return 1;	
}
   


//
void CIdtUsbHid::LogInfoHexFomratedLine(LPBYTE pbyteData, DWORD dwDataSize,BOOL bSpaceDelimiter)
{
    DWORD dwIdx, dwSizeWrite = 0;
    TCHAR tszLine[BUF_MAX_SIZE+50]; //Keep Save
    TCHAR * ptszLineWrite = tszLine;

    if (BUF_MAX_SIZE < dwDataSize*3)
        return;
    for (dwIdx = 0;dwIdx < dwDataSize;dwIdx ++)
    {
        if (bSpaceDelimiter)
            _stprintf_s(ptszLineWrite,BUF_MAX_SIZE-dwIdx*3,_T(" %02X"),pbyteData[dwIdx]);
        else
            _stprintf_s(ptszLineWrite,BUF_MAX_SIZE-dwIdx*3,_T("%02X"),pbyteData[dwIdx]);
        dwSizeWrite +=_tcsclen(ptszLineWrite);
        if (dwSizeWrite >= BUF_MAX_SIZE )
            break;
        ptszLineWrite += _tcsclen(ptszLineWrite);
    }
    LOG_INFO(tszLine);
}
//


void CIdtUsbHid::LogInfoHexFomrated(LPBYTE pbyteData, DWORD dwDataSize, DWORD dwBytesPerLine,BOOL bSpaceDelimiter)
{
    DWORD dwDataRest = dwDataSize;

    LOG_INFO_BREAK();
    //
    // Calculate Lines Num
    while(dwDataRest > 0)
    {
        // Boundary Check
        if (dwDataRest < dwBytesPerLine)
            dwBytesPerLine = dwDataRest;

        //
        LogInfoHexFomratedLine(pbyteData, dwBytesPerLine, bSpaceDelimiter);
        pbyteData += dwBytesPerLine;
        dwDataRest -= dwBytesPerLine;
    }
}
   //
unsigned int CIdtUsbHid::Write_Byte(unsigned char* aucmpData, unsigned int auiDataLen) 
{
    unsigned char lucmCommand[100];
    unsigned int  luiCommandLen =auiDataLen;
    unsigned int  luiCommandCount =1 + auiDataLen/63;
    if((auiDataLen % 63) ==0x00) luiCommandCount --;
    unsigned int  luii;
    DWORD dwBytesWritten;
    lucmCommand[0] =0x00;

    do{
        for(luii=0;luii<68;luii++)
        {
            lucmCommand[luii] =0x00;
        }

        if(luiCommandCount >1)
        {
            luiCommandLen -=63;
            lucmCommand[1] =0xBF;//63;
            for(luii=2;luii<65;luii++)
            {
                lucmCommand[luii] =*aucmpData++;
            }
        }
        else
        {
            lucmCommand[1] =luiCommandLen;
            for(luii=2;luii<luiCommandLen+2;luii++)
            {
                lucmCommand[luii] =*aucmpData++;
            }
        }

        m_overlapped.Offset =0;
        m_overlapped.OffsetHigh =0;
        if(!WriteFile(m_hidDevice, lucmCommand, m_caps.OutputReportByteLength, &dwBytesWritten, &m_overlapped))
        {
            DWORD dwErrorCode =GetLastError();
            if(dwErrorCode == ERROR_IO_PENDING)
            {
                if(WAIT_OBJECT_0 ==WaitForSingleObject(m_ReadEvent, 100))
                {		
                    ResetEvent(m_ReadEvent);
                }
                else
                {
                    return 0;
                }
            }
            else
            {
                return 0;
            }
        }
        luiCommandCount--;
    }while(luiCommandCount);

    return 1;
}

/*
Ret : 
= 0 --> time out / other kind of I/O Error
> 0 --> Written bytes --> if < nSendSize --> Partial sent then I/O failed
*/
DWORD CIdtUsbHid::Write_Byte_IDGCmd_FrameSize_64bytes(LPBYTE arrayByteSend, DWORD u32SendSize, DWORD u32TimeoutMS,DWORD (* pfuncCB)(void)) 
{

    DWORD dwBytesWritten;
    DWORD dwBytesWrittenCnt = 0;
    DWORD dwWaitUnitMS = 100;
    DWORD dwTimeoutCount = 0;
    bool bTimeout = false;
    bool bLoop = true;
    bool bEventSignaled = false;
    BOOL bResult = FALSE;
    DWORD dwTickStart;
    DWORD u32RetWaitSignal = WAIT_TIMEOUT;
    ENU_FSM nFSM = FSM_RD ;
    DWORD dwErrorCode;
    int nRetry = 0;
    USHORT u16InputReportLength;

    ASSERT(arrayByteSend);

    ::memset(&m_overlapped ,0x00,sizeof(OVERLAPPED));
    m_overlapped.hEvent = m_WriteEvent ;
    ::ResetEvent (m_WriteEvent );

    if ( m_caps.InputReportByteLength > u32SendSize)
    {
        char szMsg[BUF_MAX_SIZE ];
        sprintf_s(szMsg,BUF_MAX_SIZE ,"-------[Warning]---------, m_caps.InputReportByteLength ( %ul )  is bigger than  in buf size ( %ul )",m_caps.InputReportByteLength,u32SendSize);

        LOG_INFO(szMsg)

            u16InputReportLength = u32SendSize;
    }
    else
        u16InputReportLength = m_caps.InputReportByteLength;

    if (u32SendSize == 64)
    {
        while (bLoop)
        {
            // Start Tick Count
            dwTickStart = ::GetTickCount ();

            if (pfuncCB)
            {
                if ( (*pfuncCB)() > 0)
                { bLoop = false; continue;}
            }
            switch(nFSM)
            {
            case FSM_RD: 
                {
                    if(WriteFile(m_hidDevice, arrayByteSend, u16InputReportLength, &dwBytesWritten, &m_overlapped ))
                    {
                        // Good Read, Check Recv Byte Completely or not
                        //dwBytesWrittenCnt += dwBytesWritten;
                        //// Check Sending Data is completed.
                        //if (dwBytesWrittenCnt >= u32SendSize)
                        //{
                        //    bLoop = false;
                        //    bResult = TRUE;
                        //    break;
                        //}

                        m_overlapped.Offset += dwBytesWritten;
                        // Check Sending Data is completed.
                        if (m_overlapped.Offset >= u32SendSize)
                        {
                            bLoop = false;
                            bResult = TRUE;
                            break;
                        }

                        nRetry = 0;

                        // Manual-reset event should be reset since it is now signaled.
                        ::ResetEvent(m_overlapped.hEvent);
                    }
                    else
                    { 
                        dwErrorCode =GetLastError();
                        switch(dwErrorCode )
                        {
                        case ERROR_IO_PENDING :
                            nFSM = FSM_IO_PENDING ;
                            break;

                        case ERROR_INVALID_USER_BUFFER:
                        case ERROR_NOT_ENOUGH_MEMORY:
                            // Too many  asynchronous I/O requests, Cancel those requests from other threads.
                            //::CancelIoEx(m_hidDevice , &m_overlapped );
                            ::CancelIo(m_hidDevice  );
                             TRACE("ERROR_INVALID_USER_BUFFER or ERROR_NOT_ENOUGH_MEMORY");
                            break;

                        case ERROR_OPERATION_ABORTED:
                            // aborted by other thread, try again
                            nRetry ++;
                            TRACE("ERROR_OPERATION_ABORTED");
                            break;

                        default:
                            // other I/O Error, stop it.
                            nFSM = FSM_IO_ERR ;
                            break;
                        }
                    }
                }
                break;
            case FSM_IO_PENDING:
                {
                    dwBytesWritten = 0;
                    if (::GetOverlappedResult( m_hidDevice,
                        &m_overlapped,
                        &dwBytesWritten,
                        FALSE))
                    {
                        //dwBytesWrittenCnt += dwBytesWritten;

                        //// Check Sending Data is completed.
                        //if (dwBytesWrittenCnt >= u32SendSize)
                        //{
                        //    bLoop = false;
                        //    bResult = TRUE;
                        //    break;
                        //}

                        m_overlapped.Offset += dwBytesWritten;
                        // Check Sending Data is completed.
                        if (m_overlapped.Offset >= u32SendSize)
                        {
                            bLoop = false;
                            bResult = TRUE;
                            break;
                        }
                        nRetry = 0;
                        nFSM = FSM_RD ;
                        TRACE("FSM_IO_PENDING : GetOve3rlappedResult()");
                        // Manual-reset event should be reset since it is now signaled.
                        ::ResetEvent(m_overlapped.hEvent);
                    }
                    else
                    {
                        dwErrorCode =GetLastError();
                        //if(dwErrorCode == ERROR_IO_INCOMPLETE)
                        //{
                        //    nFSM = FSM_IO_PENDING ;
                        //}
                        //else
                        //    // other I/O Error, stop it.
                        //    nFSM = FSM_IO_ERR ;
                        
                        switch (dwErrorCode )
                        {
                        case ERROR_IO_INCOMPLETE :
                            //nFSM = FSM_IO_PENDING ;
                            nRetry ++;
                            if (nRetry>9)
                            {
                                //nFSM = FSM_IO_ERR ;
                            }
                            TRACE("FSM_IO_PENDING : ERROR_IO_INCOMPLETE\n");
                            break;
                        default:
                            // other I/O Error, stop it.
                            nFSM = FSM_IO_ERR ;
                            TRACE("FSM_IO_PENDING : Other I/O Err\n");
                            break;
                        }
                    }
                }
                break;
            case FSM_IO_ERR:
                bLoop = false;
                _stprintf_s(tszMsg,TMP_BUF_LEN,_T("Write I/O Error %x-%x"), arrayByteSend[0], arrayByteSend[1]);
                //MessageBox(NULL,tszMsg,_T("Error"),MB_OK|MB_ICONERROR )             ;
                TRACE("FSM IO ERR = %s\n", tszMsg);
                break;
            }


            // Timeout Check
#if defined(TIME_OUT)
            // Time out counting
            dwTickStart =::GetTickCount ()-dwTickStart;
            if ( u32TimeoutMS > dwTickStart )
                u32TimeoutMS -= dwTickStart ;
            else
                u32TimeoutMS = 0;

            if (1>=u32TimeoutMS)
            {
                bTimeout = true;
                bLoop = false;
                _stprintf_s(tszMsg,TMP_BUF_LEN,_T("Write Timeout %x-%x"), arrayByteSend[0], arrayByteSend[1]);
                //MessageBox(NULL,tszMsg,_T("Error"),MB_OK|MB_ICONERROR );
                TRACE("u32TimeoutMS (%d) <= 1\n", u32TimeoutMS);
            }
#endif
        } // End of while (bKeepWrite)
    }

    if (bTimeout )
    {

    }
    //Timeout --> Return 0
    //return dwBytesWrittenCnt;
    return m_overlapped.Offset ;
}

/*
Comment: Compose Frame Format
byteRID (1 byte) + arrayByteSend (dwCommand Len in Byte) + padding (0x00, up to 64 bytes)
*/
DWORD  CIdtUsbHid::Write_Byte_IDGCmd_Compose_SendData_RID_Data_Padding_Upto_64Bytes_u32(BYTE * arrayByteDataSendFrameSingle, BYTE byteRID,BYTE * arrayByteSend,DWORD dwCommandLen)
{
    DWORD dwArrayByteSendFillLen;

    ASSERT( arrayByteDataSendFrameSingle);
    ASSERT(arrayByteSend);
    ASSERT(dwCommandLen>0);

    // 1st byte = Report ID
    *arrayByteDataSendFrameSingle = byteRID;
    if (dwCommandLen > MAX_FRAME_SINGLE_SIZE)
        dwArrayByteSendFillLen = MAX_FRAME_SINGLE_SIZE;
    else
        dwArrayByteSendFillLen = dwCommandLen;

    // 2. Next fill out data
    ::memcpy(arrayByteDataSendFrameSingle+1,arrayByteSend, dwArrayByteSendFillLen);

    // Padding up if it's MUST
    if (dwArrayByteSendFillLen < MAX_FRAME_SINGLE_SIZE)
    {
        ::memset(arrayByteDataSendFrameSingle+1+dwArrayByteSendFillLen,0x00,MAX_FRAME_SINGLE_SIZE-dwArrayByteSendFillLen);
    }

    return dwArrayByteSendFillLen;
}
/*
Comment: 
Ret : 
= 0 --> time out / Other kind of I/O errors
> 0 --> Written bytes, 
if < nSendSize --> Partial Send then Some kind of I/O Error
*/
DWORD CIdtUsbHid::Write_Byte_IDGCmd(LPBYTE pByteSend, DWORD u32SendSize,DWORD u32TimeoutMS,DWORD (* pfuncCB)(void)) 
{
    unsigned char byteDataSendFrameSingle[MAX_FRAME_SINGLE_SIZE+1];
    DWORD  dwCommandLen = u32SendSize;
    DWORD dwBytesWritten, dwBytesWrittenSingleFrame, u32BytesFilledInSingleFrame;
    BYTE byteRID;

    ASSERT(pByteSend);

    // Compute number of Frames
    unsigned int  nCommandCount =1 + u32SendSize/MAX_FRAME_SINGLE_SIZE;
    if((u32SendSize % MAX_FRAME_SINGLE_SIZE) ==0x00) 
        nCommandCount --;
    unsigned int nSendFrameCnt = 0;

    // Set up first frame RID
    if (nCommandCount > 1)
        byteRID = 0x02;
    else
        byteRID = 0x01;
    dwBytesWritten = 0;

    do{
        u32BytesFilledInSingleFrame =  Write_Byte_IDGCmd_Compose_SendData_RID_Data_Padding_Upto_64Bytes_u32(
            byteDataSendFrameSingle, byteRID,pByteSend+dwBytesWritten,dwCommandLen);

        dwBytesWrittenSingleFrame = Write_Byte_IDGCmd_FrameSize_64bytes(byteDataSendFrameSingle,(MAX_FRAME_SINGLE_SIZE+1),u32TimeoutMS,pfuncCB);
        if (dwBytesWrittenSingleFrame < (MAX_FRAME_SINGLE_SIZE+1))
        {
            // Error then stop
            break;
        }
        else
        {
            dwBytesWritten += u32BytesFilledInSingleFrame;
            dwCommandLen -= u32BytesFilledInSingleFrame;
            nCommandCount--;
        }
        // Set up the middle / last frame RID
        if (nCommandCount > 1)
            byteRID = 0x03;
        else
            byteRID = 0x04;
    } while(nCommandCount);

    return dwBytesWritten;

}

DWORD CIdtUsbHid::Write_Byte_IDGCmd(LPBYTE pByteSend, DWORD u32SendSize,DWORD u32TimeoutMS) 
{
    unsigned char byteDataSendFrameSingle[MAX_FRAME_SINGLE_SIZE+1];
    DWORD  dwCommandLen = u32SendSize;
    DWORD dwBytesWritten, dwBytesWrittenSingleFrame, u32BytesFilledInSingleFrame;
    BYTE byteRID;

    ASSERT(pByteSend);

    // Compute number of Frames
    unsigned int  nCommandCount =1 + u32SendSize/MAX_FRAME_SINGLE_SIZE;
    if((u32SendSize % MAX_FRAME_SINGLE_SIZE) ==0x00) 
        nCommandCount --;
    unsigned int nSendFrameCnt = 0;

    // Set up first frame RID
    if (nCommandCount > 1)
        byteRID = 0x02;
    else
        byteRID = 0x01;
    dwBytesWritten = 0;

    do{
        u32BytesFilledInSingleFrame =  Write_Byte_IDGCmd_Compose_SendData_RID_Data_Padding_Upto_64Bytes_u32(
            byteDataSendFrameSingle, byteRID,pByteSend+dwBytesWritten,dwCommandLen);

        dwBytesWrittenSingleFrame = Write_Byte_IDGCmd_FrameSize_64bytes(byteDataSendFrameSingle,(MAX_FRAME_SINGLE_SIZE+1),u32TimeoutMS);
        if (dwBytesWrittenSingleFrame < (MAX_FRAME_SINGLE_SIZE+1))
        {
            // Error then stop
            break;
        }
        else
        {
            dwBytesWritten += u32BytesFilledInSingleFrame;
            dwCommandLen -= u32BytesFilledInSingleFrame;
            nCommandCount--;
        }

        // Set up the middle / last frame RID
        if (nCommandCount > 1)
            byteRID = 0x03;
        else
            byteRID = 0x04;
    } while(nCommandCount);

    return dwBytesWritten;
}

/*
[IN] 
pnRecvOutSize = output buffer size
arrayByteRecvOut = output buffer
[OUT]
pnRecvOutSize = received data size, if error = 0 --> MUST 64 bytes
arrayByteRecvOut = received data resident output buffer with IDG Command ONLY
*/
BOOL CIdtUsbHid::Read_Byte_IDGCmd_SingleFrame_64bytes_Old(LPBYTE pbyteRecvOut, LPDWORD pu32RecvOutSize, DWORD u32TimeoutMS, DWORD (* pfuncCB)(void)) 
{
    //DWORD dwRecvCnt = MAX_FRAME_SINGLE_SIZE+1;
    //DWORD dwRecvCnt = 0;
    DWORD dwRecvInc = 0;
    DWORD dwBytesRead;
    BOOL bResult = FALSE;
    DWORD dwWait = 100;
    DWORD dwErrorCode;
    DWORD dwTickStart;
    ENU_FSM nFSM = FSM_RD;
    bool bLoop = true;
    bool bTimeout = false;
    int nRetry = 0;
    USHORT u16InputReportLength;
    char szMsg [BUF_MAX_SIZE] = {""};
    BOOL bOverlapWait = FALSE;
    HANDLE hidDevice;
    hidDevice = m_hidDevice;

    ASSERT(pbyteRecvOut);
    ASSERT(pu32RecvOutSize);
    ASSERT((*pu32RecvOutSize)>= (MAX_FRAME_SINGLE_SIZE+1));

    ::memset(&m_overlapped ,0x00,sizeof(OVERLAPPED));
    m_overlapped.hEvent = m_ReadEvent ;
    ::ResetEvent (m_overlapped.hEvent);

    if ( m_caps.InputReportByteLength > *pu32RecvOutSize)
    {
        sprintf_s(szMsg,BUF_MAX_SIZE ,"-------[Warning]---------, m_caps.InputReportByteLength ( %d  ) is bigger than  in buf size ( %ul )" , m_caps.InputReportByteLength , *pu32RecvOutSize);
        LOG_INFO (szMsg);
        u16InputReportLength = *pu32RecvOutSize;
    }
    else
        u16InputReportLength = m_caps.InputReportByteLength;

    while (bLoop)
    {
        // Start Tick Count
        dwTickStart = ::GetTickCount ();

        if (pfuncCB)
        {
            if ( 0 < (*pfuncCB)())
            {
                _stprintf_s(tszMsg,BUF_MAX_SIZE,_T("991: External Call back Function Return Value < 1"));
                LOG_INFO(tszMsg)
                bLoop=false; 
                continue;
            }
        }



        switch(nFSM)
        {
        case FSM_RD: 
            {
                dwBytesRead = 0;
                if(ReadFile(hidDevice , pbyteRecvOut, u16InputReportLength, &dwBytesRead, &m_overlapped))
                {
                    // Good Read, Check Recv Byte Completely or not
                    //dwRecvCnt += dwBytesRead;

                    //// Check Sending Data is completed.
                    //if (dwRecvCnt >= u16InputReportLength)
                    //{
                    //    bLoop = false;
                    //    bResult = TRUE;
                    //    break;
                    //}

                    m_overlapped.Offset += dwBytesRead ;
                    if (m_overlapped.Offset  >= u16InputReportLength)
                    {

                        bLoop = false;
                        bResult = TRUE;
                        LOG_INFO("891:Recv End since Size 64 meet ");
                        break;
                    }
                    nRetry = 0;

                    // Manual-reset event should be reset since it is now signaled.
                    ::ResetEvent(m_overlapped.hEvent); // <-- don't do it ? need a try.

                } // End of if ( ReadFile())
                else
                { 
                    dwErrorCode =GetLastError();
                    switch (dwErrorCode )
                    {
                    case ERROR_IO_PENDING :
                        nFSM = FSM_IO_PENDING ;
                        break;
                    case ERROR_HANDLE_EOF:
                        // Reach end of File, end it
                        bLoop = false;
                        //bResult = FALSE; 
                        _stprintf_s(tszMsg,BUF_MAX_SIZE,_T("993: Meet ERROR_HANDLE_EOF then stop recv while loop !!"));
                        LOG_INFO(tszMsg)
                        break;
                    default:
                        // other I/O Error, stop it.
                        nFSM = FSM_IO_ERR ;
                        break;
                    }
                }// End of if ( ReadFile())
            }
            break;
        case FSM_IO_PENDING:
            {
                //dwErrorCode = ::WaitForSingleObject (m_overlapped .hEvent ,dwWait );

                //switch(dwErrorCode )
                //{
                //case WAIT_OBJECT_0 :
                dwBytesRead = 0;
                if (::GetOverlappedResult( hidDevice ,
                    &m_overlapped,
                    &dwBytesRead,
                    bOverlapWait ))
                {
                    //dwRecvCnt += dwBytesRead;
                    //// Check Sending Data is completed.
                    //if (dwRecvCnt >= u16InputReportLength)
                    //{
                    //    bLoop = false;
                    //    bResult = TRUE;
                    //    break;
                    //}
                    m_overlapped.Offset += dwBytesRead ;
                    if (m_overlapped.Offset  >= u16InputReportLength)
                    {
                        bLoop = false;
                        bResult = TRUE;
                        LOG_INFO("892:Recv End since Size 64 meet ");
                        break;
                    }

                    nRetry = 0;
                    nFSM = FSM_RD ;

                    // Manual-reset event should be reset since it is now signaled.
                    ::ResetEvent(m_overlapped.hEvent);
                }
                else
                {
                    dwErrorCode =GetLastError();
                    switch (dwErrorCode )
                    {
                    case ERROR_IO_INCOMPLETE :
                        //nFSM = FSM_IO_PENDING ;
                        if (nRetry>9)
                        {
                            //nFSM = FSM_IO_ERR ;
                            //::CancelIoEx( hidDevice ,&m_overlapped);
                            ::CancelIo( hidDevice );
                        }
                        else
                        {
                            nRetry ++;
                        }
                        break;

                   case ERROR_OPERATION_ABORTED:
                        nRetry = 0;
                        nFSM = FSM_RD ;
                        ::memset(&m_overlapped ,0x00,sizeof(OVERLAPPED));
                        m_overlapped.hEvent = m_ReadEvent ;
                        ::ResetEvent (m_overlapped.hEvent);
                        break;

                    case ERROR_HANDLE_EOF:
                        // Reach end of File, end it
                        bLoop = false;
                        //bResult = FALSE; 
                        _stprintf_s(tszMsg,BUF_MAX_SIZE,_T("ERROR_HANDLE_EOF"));
                        LOG_INFO(tszMsg)
                        break;

                    default:
                        // other I/O Error, stop it.
                        nFSM = FSM_IO_ERR ;
                        break;
                    }
                }
            }
            break;
        case FSM_IO_ERR:
            bLoop = false;
            _stprintf_s(tszMsg,TMP_BUF_LEN,_T("Read I/O Recv Error, dwErrorCode = %08X"),dwErrorCode);
            LOG_INFO(tszMsg);
            //MessageBox(NULL,tszMsg,_T("Error"),MB_OK|MB_ICONERROR );
            break;
        }


#if defined(TIME_OUT)
        // Time out counting
        dwTickStart =::GetTickCount ()-dwTickStart;
        if ( u32TimeoutMS > dwTickStart )
            u32TimeoutMS -= dwTickStart ;
        else
            u32TimeoutMS = 0;

        // Timeout Check
        if (1>=u32TimeoutMS)
        {
            if (nRetry < 20)
            {
                // Give Final Chance
                //bOverlapWait = TRUE;
                nRetry = 20;
                //continue;
            }

            bTimeout = true;
            bLoop = false;

            // Cancel I/O Request
             ::CancelIo( hidDevice );

            LOG_INFO("===========[ Timeout ]==================");
            _stprintf_s(tszMsg,TMP_BUF_LEN,_T("Read Timeout - Recv"));
            //MessageBox(NULL,tszMsg,_T("Error"),MB_OK|MB_ICONERROR );
        }
#endif
    } // End of while (bLoop)

    if (bTimeout )
    {

    }

    sprintf_s(szMsg,BUF_MAX_SIZE ,"u16InputReportLength = %lu;  dwRecvCnt = %lu",u16InputReportLength,m_overlapped.Offset);
    LOG_INFO(szMsg);
    (*pu32RecvOutSize) =  m_overlapped.Offset ; //dwRecvCnt;

    return bResult;
}
//
LPCTSTR ErrorMessage( DWORD error ) 

// Routine Description:
//      Retrieve the system error message for the last-error code
{ 

    LPVOID lpMsgBuf;

    FormatMessage(
        FORMAT_MESSAGE_ALLOCATE_BUFFER | 
        FORMAT_MESSAGE_FROM_SYSTEM |
        FORMAT_MESSAGE_IGNORE_INSERTS,
        NULL,
        error,
        MAKELANGID(LANG_NEUTRAL, SUBLANG_DEFAULT),
        (LPTSTR) &lpMsgBuf,
        0, NULL );

    return((LPCTSTR)lpMsgBuf);
}
//
BOOL CIdtUsbHid::Read_Byte_IDGCmd_SingleFrame_64bytes_New(LPBYTE pbyteRecvOut, LPDWORD pu32RecvOutSize, DWORD u32TimeoutMS, DWORD (* pfuncCB)(void)) 
{
    //DWORD dwRecvCnt = MAX_FRAME_SINGLE_SIZE+1;
    //DWORD dwRecvCnt = 0;
    DWORD dwRecvInc = 0;
    DWORD dwBytesRead;
    BOOL bResult = FALSE;
    DWORD dwWait = 100;
    //DWORD dwErrorCode;
    DWORD dwTickStart;
    ENU_FSM nFSM = FSM_RD;
    bool bLoop = true;
    bool bTimeout = false;
    int nRetry = 0;
    USHORT u16InputReportLength;
    char szMsg [BUF_MAX_SIZE] = {""};
    BOOL bOverlapWait = FALSE;
    HANDLE hidDevice;
    hidDevice = m_hidDevice;

    ASSERT(pbyteRecvOut);
    ASSERT(pu32RecvOutSize);
    ASSERT((*pu32RecvOutSize)>= (MAX_FRAME_SINGLE_SIZE+1));

    ::memset(&m_overlapped ,0x00,sizeof(OVERLAPPED));
    m_overlapped.hEvent = m_ReadEvent ;
    ::ResetEvent (m_overlapped.hEvent);

    if ( m_caps.InputReportByteLength > *pu32RecvOutSize)
    {
        sprintf_s(szMsg,BUF_MAX_SIZE ,"-------[Warning]---------, m_caps.InputReportByteLength ( %d  ) is bigger than  in buf size ( %ul )" , m_caps.InputReportByteLength , *pu32RecvOutSize);
        LOG_INFO (szMsg);
        u16InputReportLength = *pu32RecvOutSize;
    }
    else
        u16InputReportLength = m_caps.InputReportByteLength;

    while (bLoop)
    {
        // Start Tick Count
        dwTickStart = ::GetTickCount ();

        if (pfuncCB)
        {
            if ( 0 < (*pfuncCB)())
            {
                _stprintf_s(tszMsg,BUF_MAX_SIZE,_T("991: External Call back Function Return Value < 1"));
                LOG_INFO(tszMsg)
                bLoop=false; continue;

            }
        }
        //
        {
            /*char inBuffer[BUF_MAX_SIZE];
            DWORD nBytesToRead      = BUF_SIZE;
            DWORD dwBytesRead       = 0;*/
            //DWORD dwFileSize        = GetFileSize(hFile, NULL);
            //OVERLAPPED stOverlapped = {0};

            DWORD dwError  = 0;
            LPCTSTR errMsg = NULL;

            BOOL bResult   = FALSE;
            BOOL bContinue = TRUE;


            // Set up overlapped structure event. Other members are already 
            // initialized to zero.
            m_overlapped.hEvent = m_ReadEvent; 
            

            // This is an intentionally brute-force loop to force the EOF trigger.
            // A properly designed loop for this simple file read would use the
            // GetFileSize API to regulate execution. However, the purpose here
            // is to demonstrate how to trigger the EOF error and handle it.
            LOG_INFO("New Recv Proc()")
            while(bContinue)
            {
                // Default to ending the loop.
                bContinue = FALSE;

                // Attempt an asynchronous read operation.
                bResult = ReadFile(hidDevice,pbyteRecvOut, u16InputReportLength, &dwBytesRead, &m_overlapped); 

                dwError = GetLastError();

                // Check for a problem or pending operation. 
                if (!bResult) 
                { 
                    switch (dwError) 
                    { 

                    case ERROR_HANDLE_EOF:
                        {
                             _stprintf_s(tszMsg,BUF_MAX_SIZE,_T("ReadFile returned FALSE and EOF condition, async EOF not triggered."));
                             LOG_INFO(tszMsg)
                            break;
                        }
                    case ERROR_IO_PENDING: 
                        { 
                            BOOL bPending=TRUE;

                            // Loop until the I/O is complete, that is: the overlapped 
                            // event is signaled.

                            while( bPending )
                            {
                                bPending = FALSE;

                                // Pending asynchronous I/O, do something else
                                // and re-check overlapped structure.
                                 _stprintf_s(tszMsg,BUF_MAX_SIZE,_T("ReadFile operation is pending"));
                                 LOG_INFO(tszMsg)

                                // Do something else then come back to check. 
                                //GoDoSomethingElse(); 
                                 if ( 0 < (*pfuncCB)())
                                 {
                                     // TODO
                                     _stprintf_s(tszMsg,BUF_MAX_SIZE,_T("791: External Call back Function Return Value < 1"));
                                     LOG_INFO(tszMsg)
                                      bLoop=false; 
                                      continue;
                                 }

                                // Check the result of the asynchronous read
                                // without waiting (forth parameter FALSE). 
                                 bResult = GetOverlappedResult(hidDevice, &m_overlapped, &dwBytesRead, bOverlapWait ) ; 

                                if (!bResult) 
                                { 
                                    switch (dwError = GetLastError()) 
                                    { 
                                    case ERROR_HANDLE_EOF: 
                                        { 
                                            // Handle an end of file
                                            _stprintf_s(tszMsg,BUF_MAX_SIZE,_T("792: GetOverlappedResult found EOF !!"));
                                            LOG_INFO(tszMsg)
                                            break;
                                        } 

                                    case ERROR_IO_INCOMPLETE:
                                        {
                                            // Operation is still pending, allow while loop
                                            // to loop again after printing a little progress.
                                             _stprintf_s(tszMsg,BUF_MAX_SIZE,_T("793:ERROR_IO_INCOMPLETE!!"));
                                            LOG_INFO(tszMsg)
                                            bPending = TRUE;
                                            bContinue = TRUE;
                                            break;
                                        }

                                    default:
                                        {
                                            // Decode any other errors codes.
                                            errMsg = ErrorMessage(dwError);
                                            _stprintf_s(tszMsg,BUF_MAX_SIZE,_T("794:GetOverlappedResult failed (%d): %s\n"),  dwError, errMsg);
                                            LOG_INFO(tszMsg)
                                            LocalFree((LPVOID)errMsg);
                                        }
                                    }
                                } 
                                else
                                {
                                    _stprintf_s(tszMsg,BUF_MAX_SIZE,_T("795:ReadFile operation completed"));
                                    LOG_INFO(tszMsg)
                                    // Manual-reset event should be reset since it is now signaled.
                                    ResetEvent(m_overlapped.hEvent);
                                }
                            }
                            break;
                        }

                    default:
                        {
                            // Decode any other errors codes.
                            errMsg = ErrorMessage(dwError);
                             _stprintf_s(tszMsg,TMP_BUF_LEN,_T("ReadFile GLE unhandled (%d): %s"), dwError, errMsg); 
                            LOG_INFO(tszMsg)
                            LocalFree((LPVOID)errMsg);
                            break;
                        }
                    }
                }
                else
                {
                    // EOF demo did not trigger for the given file.
                    // Note that system caching may cause this condition on most files
                    // after the first read. CreateFile can be called using the
                    // FILE_FLAG_NOBUFFERING parameter but it would require reads are
                    // always aligned to the volume's sector boundary. This is beyond
                    // the scope of this example. See comments in the main() function.

                    LOG_INFO("ReadFile completed synchronously");
                }

                // The following operation assumes the file is not extremely large, otherwise 
                // logic would need to be included to adequately account for very large
                // files and manipulate the OffsetHigh member of the OVERLAPPED structure.
                m_overlapped.Offset += dwBytesRead;
                if ( m_overlapped.Offset >= u16InputReportLength )             
                    bLoop = FALSE;
            }

            //return stOverlapped.Offset;
        } // End of While( bContinue )
        //
#if defined(TIME_OUT)
        // Time out counting
        dwTickStart =::GetTickCount ()-dwTickStart;
        if ( u32TimeoutMS > dwTickStart )
            u32TimeoutMS -= dwTickStart ;
        else
            u32TimeoutMS = 0;

        // Timeout Check
        if (1>=u32TimeoutMS)
        {
            if (nRetry < 20)
            {
                // Give Final Chance
                //bOverlapWait = TRUE;
                nRetry = 20;
                //continue;
            }

            bTimeout = true;
            bLoop = false;

            // Cancel I/O Request
             ::CancelIo(hidDevice);

            LOG_INFO("===========[ Timeout ]==================");
            _stprintf_s(tszMsg,TMP_BUF_LEN,_T("Read Timeout - Recv"));
            LOG_INFO(tszMsg)
        }
#endif
    } // End of while (bLoop)

    if (bTimeout )
    {

    }

    sprintf_s(szMsg,BUF_MAX_SIZE ,"u16InputReportLength = %lu;  dwRecvCnt = %lu",u16InputReportLength,m_overlapped.Offset);
    LOG_INFO(szMsg);
    (*pu32RecvOutSize) =  m_overlapped.Offset ; //dwRecvCnt;

    return bResult;
}
//
BOOL CIdtUsbHid::Read_Byte_IDGCmd_SingleFrame_64bytes(LPBYTE pbyteRecvOut, LPDWORD pu32RecvOutSize, DWORD u32TimeoutMS, DWORD (* pfuncCB)(void)) 
{
   if (false)
   {
       // New Method is copied from MSDN https://msdn.microsoft.com/en-us/library/windows/desktop/aa365690%28v=vs.85%29.aspx
       // But it causes USB HID recv IO_ERROR_INCOMPLETE flooding. 
       // This method now is deprecated.
       return Read_Byte_IDGCmd_SingleFrame_64bytes_New(pbyteRecvOut ,pu32RecvOutSize ,u32TimeoutMS ,pfuncCB );
   }
   return Read_Byte_IDGCmd_SingleFrame_64bytes_Old(pbyteRecvOut ,pu32RecvOutSize ,u32TimeoutMS ,pfuncCB );
}

/*
Comment: Stripe off Report Frame  and get IDG Frame
[IN]
1.pbyteFrameRecv = USB HID Frame buffer, starting at byte RID
2.dwBytesRecvCnt = Frame Size, always = 64 bytes
3.pbyteDestDataRecvIDGFrame = IDG Frame receiving buffer
4.pdwBytesRecvedSingleFrame = IDG Frame buffer remained size
5.pdwRID1_2_GetIDGCmdLength = IDG Command Frame Total Size, ONLY Available when RID = 1 or = 2

[OUT]
3.pbyteDestDataRecvIDGFrame = IDG Frame receiving buffer
4.pdwBytesRecvedSingleFrame = IDG Frame received size
5.pdwRID1_2_GetIDGCmdLength = IDG Command Frame Total Size, ONLY Available when RID = 1 or = 2

Return Value:
*/
BOOL CIdtUsbHid::Read_Byte_IDGCmd_Deassemble_RIDFrame_To_IDGFrame(LPBYTE pbyteSrcUSBHIDFrameRecv,DWORD dwBytesRecvCnt,LPBYTE pbyteDestDataRecvIDGFrame,LPDWORD pdwBytesRecvedSingleFrame, DWORD * pdwRID1_2_GetIDGCmdLength)
{
    BOOL bResult = FALSE;
    BYTE byteRID;
    BOOL bDeassembleIDGRawLength = FALSE;
    BOOL bTruncateTrail = FALSE;
    WORD wRawLen;

    ASSERT(pbyteSrcUSBHIDFrameRecv);
    ASSERT(dwBytesRecvCnt >= (MAX_FRAME_SINGLE_SIZE+1));
    ASSERT(pbyteDestDataRecvIDGFrame);
    ASSERT(pdwBytesRecvedSingleFrame);
    // Get RID
    byteRID = *pbyteSrcUSBHIDFrameRecv;

    switch (byteRID)
    {
    case 0x01:
        ASSERT(pdwRID1_2_GetIDGCmdLength);
        //=====[ Special Note ]=====
        // Rx Frame, Prefix Pattern, Cmd Resp, UI Event, Auto Poll+Burst Mode, and the like
#if 0
        // Paste from JLT's VivopayReaderWriter_RS232.SerialMessageProc.run()
       final static byte[][] headers = new byte[][] {
			{ 0x56, 0x69, 0x56, 0x4F, 0x74, 0x65, 0x63, 0x68, 0x32, 0x00 }, // ViVOtech2\0, version 2 protocol
			// V3, 2016 Sept 23 added, [resp hdr][cmd][sub-cmd][sts code][Len,2 bytes][raw data][crc, 2 bytes]
			{ 0x56, 0x69, 0x56, 0x4F, 0x70, 0x61, 0x79, 0x56, 0x33, 0x00 }, // ViVOpayV3\0, version 3 protocol
			{ 0x45, 0x00, 0x55 }, // UI Event
			{ 0x02 }, 
			{ 0x01, 0x00 }, // Payload Frame (On Successful Read), 0x01, 0x00, 1B_AppType,?B_TrackData,2B_CRC
			{ 0x56, 0x69, 0x56, 0x4F, 0x74, 0x65, 0x63, 0x68, 0x00 }, // ViVOtech\0, version 1 protocol
			{ 0x03, 0x00 } };
	// @formatter:on

	final static int[] lengthIndex = new int[] { 13, 14, 4, 8, -1, 13, -1 };
#endif
        /*
        [ToDo] [Special Case]
        In Auto Poll+Burst Mode, the response data format is 01 00 0c
        */
        //
        if (IsV3Protocol(pbyteSrcUSBHIDFrameRecv))
            SetViVOProtocl_V3();
        else
            SetViVOProtocl_V2();
        //
        wRawLen = ((((DWORD)pbyteSrcUSBHIDFrameRecv[m_nOffsetLenH]) << 8) & 0xFF00) | pbyteSrcUSBHIDFrameRecv[m_nOffsetLenL];
        // Get Total Frame Length, Header = 14/15, Trail (CRC, 2 bytes) = 2
        *pdwRID1_2_GetIDGCmdLength = wRawLen + m_nPkgMinLen;

        // Boundary Check
        if (*pdwBytesRecvedSingleFrame <  *pdwRID1_2_GetIDGCmdLength)
        {
            // Error, out of boundary/buffer
            *pdwBytesRecvedSingleFrame = 0;
            break;
        }
        // Get Single Frame Length
        *pdwBytesRecvedSingleFrame = *pdwRID1_2_GetIDGCmdLength;
        bResult = TRUE;
        break;
    case 0x02:
        ASSERT(pdwRID1_2_GetIDGCmdLength);
         /*
        [ToDo] [Special Case]
        In Auto Poll+Burst Mode, the response data format is 01 00 0c
        */
        //
        if (IsV3Protocol(pbyteSrcUSBHIDFrameRecv))
            SetViVOProtocl_V3();
        else
            SetViVOProtocl_V2();
        //
        wRawLen = ((((DWORD)pbyteSrcUSBHIDFrameRecv[m_nOffsetLenH]) << 8) & 0xFF00) | pbyteSrcUSBHIDFrameRecv[m_nOffsetLenL];
        // Get Total Frame Length, Header = 14 / 15, Trail (CRC, 2 bytes) = 2
        *pdwRID1_2_GetIDGCmdLength = wRawLen + m_nPkgMinLen;

        // Boundary Check
        if (*pdwBytesRecvedSingleFrame <  *pdwRID1_2_GetIDGCmdLength)
        {
            // Error, out of boundary/buffer
            *pdwBytesRecvedSingleFrame = 0;
            break;
        }

        // Get Single Frame Length, must 63 bytes
        *pdwBytesRecvedSingleFrame = dwBytesRecvCnt-1 ;

        m_dwIDGCmdFrameSizeRest = *pdwRID1_2_GetIDGCmdLength - *pdwBytesRecvedSingleFrame;
        break;
    case 0x03:
        // Get Single Frame Length, must 63 bytes
        *pdwBytesRecvedSingleFrame = dwBytesRecvCnt-1 ;
        if (m_dwIDGCmdFrameSizeRest < *pdwBytesRecvedSingleFrame)
        {
            int i;
            i=0;
        }
        else
        m_dwIDGCmdFrameSizeRest -= *pdwBytesRecvedSingleFrame;
        break;
    case 0x04:
        *pdwBytesRecvedSingleFrame = m_dwIDGCmdFrameSizeRest;
        bResult = TRUE;
        break;
    default:
        // Unknown RID, return FALSE, set Recved Single Frame = 0
        *pdwBytesRecvedSingleFrame = 0;
        return bResult;
        break;
    }
    ::memcpy(pbyteDestDataRecvIDGFrame, pbyteSrcUSBHIDFrameRecv+1, *pdwBytesRecvedSingleFrame );
    //

    return bResult;
}

/*
[IN] 
pnRecvOutSize = output buffer size
arrayByteRecvOut = output buffer
[OUT]
pnRecvOutSize = received data size, if error = 0
arrayByteRecvOut = received data resident output buffer with IDG Command ONLY
*/
void CIdtUsbHid::Read_Byte_IDGCmd(LPBYTE arrayByteRecvOut, DWORD * pdwRecvOutSize,DWORD dwTimeoutMS,  DWORD (* pfuncCB)(void)) 
{
    BOOL bResult = FALSE;
    BYTE arrayByteDataRecvFrameSingle[MAX_FRAME_SINGLE_SIZE+1];
    //BYTE arrayByteRecvBuff[MAX_FRAME_TOTAL_SIZE];
    BYTE * pByteRecv = arrayByteRecvOut;
    DWORD dwBytesRecvCnt;
    DWORD dwBytesRecvRest;
    DWORD dwBytesRecvedSingleFrame;
    DWORD dwBytesRecvedSingleFrameIDG;
    DWORD dwRID1_2_GetIDGCmdLength;
    BYTE byteRID, byteRIDPre;

    ASSERT(pdwRecvOutSize);
    unsigned int nRecvBufSize = *pdwRecvOutSize;

    ASSERT(arrayByteRecvOut);
    ASSERT(pdwRecvOutSize);

    dwBytesRecvRest = *pdwRecvOutSize;
    // Clean up to Return Recv Data Size = 0 as Error (default)
    *pdwRecvOutSize = 0;

    byteRIDPre = 0x00; // None RID
    dwBytesRecvCnt = 0; // Initialize Recv Data Size = ZERO
    while (dwBytesRecvCnt < MAX_FRAME_TOTAL_SIZE)
    {
        dwBytesRecvedSingleFrame = MAX_FRAME_SINGLE_SIZE+1;
        Read_Byte_IDGCmd_SingleFrame_64bytes(arrayByteDataRecvFrameSingle, &dwBytesRecvedSingleFrame, dwTimeoutMS,pfuncCB);
        if (dwBytesRecvedSingleFrame != (MAX_FRAME_SINGLE_SIZE+1))
        {
            // Recv I/O Error
             _stprintf_s(tszMsg,BUF_MAX_SIZE,_T("001: dwBytesRecvedSingleFrame (%d) <>  %d, Recv Frame Size Incorrect"), dwBytesRecvedSingleFrame , (MAX_FRAME_SINGLE_SIZE +1));
            LOG_INFO(tszMsg)
            break;
        }


        // According to ViVO IDG Command Specification GR V2.0.0
        // Check this is valid frame or not
        byteRID = *arrayByteDataRecvFrameSingle;
        if ((byteRID == 0x01) || (byteRID == 0x02))
        {
            // Drop this data frame (report) since Report ID (RID) is invalid.
            if (byteRIDPre != 0x00)
                continue;
        } else
            if ((byteRID == 0x03) || (byteRID == 0x04))
            {
                // Drop this data
                if ((byteRIDPre != 0x02) && (byteRIDPre != 0x03))
                    continue;
            }
            // Update buffer size/RID information
            byteRIDPre = byteRID;

            //-----[Stripe off USB HID RID Frame to break down to IDG Frame]-----
            //if (nBytesRecvedSingleFrame == 0)pdwRID1_2_GetIDGCmdLength
            dwBytesRecvedSingleFrameIDG = dwBytesRecvRest;
            if (!Read_Byte_IDGCmd_Deassemble_RIDFrame_To_IDGFrame(arrayByteDataRecvFrameSingle,dwBytesRecvedSingleFrame, pByteRecv, &dwBytesRecvedSingleFrameIDG, &dwRID1_2_GetIDGCmdLength))
            {
                // Not Recv Done yet
            }
            if (dwBytesRecvedSingleFrame == 0)
            {
                // Error , possible be out of buffer / boundary;  Invalid RID.
                // Disassemble Error
                continue;
            }

            pByteRecv += dwBytesRecvedSingleFrameIDG;
            dwBytesRecvCnt += dwBytesRecvedSingleFrameIDG;
            dwBytesRecvRest -=dwBytesRecvedSingleFrameIDG;
            // Put data together

            // Check End of Data Recving condition
            if ((byteRID == 0x01) || (byteRID == 0x04))
            {
                bResult = TRUE;
                *pdwRecvOutSize = dwBytesRecvCnt;
                break;
            }
    } // End of while()
}

/*
[IN] 
pnRecvOutSize = output buffer size
arrayByteRecvOut = output buffer
[OUT]
pnRecvOutSize = received data size, if error = 0
arrayByteRecvOut = received data resident output buffer with IDG Command ONLY
*/
void CIdtUsbHid::Read_Byte_IDGCmd(LPBYTE arrayByteRecvOut, DWORD * pdwRecvOutSize,DWORD dwTimeoutMS) 
{
    BOOL bResult = FALSE;
    BYTE arrayByteDataRecvFrameSingle[MAX_FRAME_SINGLE_SIZE+1];
    //BYTE arrayByteRecvBuff[MAX_FRAME_TOTAL_SIZE];
    BYTE * pByteRecv = arrayByteRecvOut;
    DWORD dwBytesRecvCnt;
    DWORD dwBytesRecvRest;
    DWORD dwBytesRecvedSingleFrame;
    DWORD dwBytesRecvedSingleFrameIDG;
    DWORD dwRID1_2_GetIDGCmdLength;
    BYTE byteRID, byteRIDPre;

    ASSERT(pdwRecvOutSize);
    unsigned int nRecvBufSize = *pdwRecvOutSize;

    ASSERT(arrayByteRecvOut);
    ASSERT(pdwRecvOutSize);

    dwBytesRecvRest = *pdwRecvOutSize;
    // Clean up to Return Recv Data Size = 0 as Error (default)
    *pdwRecvOutSize = 0;

    byteRIDPre = 0x00; // None RID
    dwBytesRecvCnt = 0; // Initialize Recv Data Size = ZERO
    while (dwBytesRecvCnt < MAX_FRAME_TOTAL_SIZE)
    {
        dwBytesRecvedSingleFrame = MAX_FRAME_SINGLE_SIZE+1;
        Read_Byte_IDGCmd_SingleFrame_64bytes(arrayByteDataRecvFrameSingle, &dwBytesRecvedSingleFrame, dwTimeoutMS);
        if (dwBytesRecvedSingleFrame != (MAX_FRAME_SINGLE_SIZE+1))
        {
            // Recv I/O Error
            break;
        }

        // According to ViVO IDG Command Specification GR V2.0.0
        // Check this is valid frame or not
        byteRID = *arrayByteDataRecvFrameSingle;
        if ((byteRID == 0x01) || (byteRID == 0x02))
        {
            // Drop this data frame (report) since Report ID (RID) is invalid.
            if (byteRIDPre != 0x00)
                continue;
        } else if ((byteRID == 0x03) || (byteRID == 0x04))
        {
            // Drop this data
            if ((byteRIDPre != 0x02) && (byteRIDPre != 0x03))
                continue;
        }
        // Update buffer size/RID information
        byteRIDPre = byteRID;

        //-----[Stripe off USB HID RID Frame to break down to IDG Frame]-----
        //if (nBytesRecvedSingleFrame == 0)
        dwBytesRecvedSingleFrameIDG = dwBytesRecvRest;
        if (!Read_Byte_IDGCmd_Deassemble_RIDFrame_To_IDGFrame(arrayByteDataRecvFrameSingle,dwBytesRecvedSingleFrame, pByteRecv, &dwBytesRecvedSingleFrameIDG, &dwRID1_2_GetIDGCmdLength))
        {
            // Not Recv Done yet
        }
        if (dwBytesRecvedSingleFrame == 0)
        {
            // Error , possible be out of buffer / boundary;  Invalid RID.
            // Disassemble Error
            continue;
        }

        pByteRecv += dwBytesRecvedSingleFrameIDG;
        dwBytesRecvCnt += dwBytesRecvedSingleFrameIDG;
        dwBytesRecvRest -=dwBytesRecvedSingleFrameIDG;
        // Put data together

        // Check End of Data Recving condition
        if ((byteRID == 0x01) || (byteRID == 0x04))
        {
            bResult = TRUE;
            *pdwRecvOutSize = dwBytesRecvCnt;
            break;
        }
    } // End of while()
}




DWORD CIdtUsbHid::SendAndGetResponseUSBHID(char * szIDGCmd, LPBYTE pSendData, DWORD dwSendDataSize, LPBYTE pResponseData,DWORD dwResponseDataSize,DWORD u32TimeoutMS, DWORD (* pfuncCB)(void))
{
    int nTmp;

    LOG_INFO("==============[ 00001 ]=================" );
    if (pSendData == NULL)
    {
        _stprintf_s(tszMsg,TMP_BUF_LEN,_T("SendAndGetResponseUSBHID(): pSendData = NULL"));
        //MessageBox(NULL,tszMsg,_T("Error"),MB_OK|MB_ICONERROR );
        return 0;
    }
    if (pResponseData == NULL)
    {
        _stprintf_s(tszMsg,TMP_BUF_LEN,_T("SendAndGetResponseUSBHID(): pResponseData = NULL"));
        //MessageBox(NULL,tszMsg,_T("Error"),MB_OK|MB_ICONERROR );
        return 0;
    }

    if (dwSendDataSize < 1)
    {
        _stprintf_s(tszMsg,TMP_BUF_LEN,_T("SendAndGetResponseUSBHID(): dwSendDataSize = %d"), dwSendDataSize );
        //MessageBox(NULL,tszMsg,_T("Error"),MB_OK|MB_ICONERROR );
        return 0;
    }
    // TODO: Add your control notification handler code here
    LOG_INFO("00002");
    if (!bIsOpen())
    {
        LOG_INFO("Device isn't Open yet");
        return 0;
    }

    // Send Data
    LOG_INFO("00003");

    if (bIsUSB_KBWedge()){
            // USB-KB Wedge
        nTmp = Write_KBWedge(pSendData,dwSendDataSize,u32TimeoutMS, pfuncCB);
    }else
    if (m_caps.OutputReportByteLength==0x41){
        // NGA USB Frame protocol
        nTmp = Write(pSendData,dwSendDataSize);//,u32TimeoutMS);
    }else{
        //USB-HID
        nTmp = Write_Byte_IDGCmd(pSendData,dwSendDataSize,u32TimeoutMS, pfuncCB);
    }
    // Recv Data
    if (nTmp > 0)
    {
        LOG_INFO("00004");
        // Give Error Status Value  in response frame, must Consider RID 1st byte presents status @ USB HID Frame
        pResponseData[m_nOffsetRespSts] = 0xFF; pResponseData[m_nOffsetRespSts+1] = 0xFF;
        if (bIsUSB_KBWedge()){
            // USB-KB Wedge
            dwResponseDataSize = Read_KBWedge(pResponseData,dwResponseDataSize,u32TimeoutMS, pfuncCB);
        }else
        if (m_caps.InputReportByteLength==0x41){
            dwResponseDataSize = Read(pResponseData,dwResponseDataSize, u32TimeoutMS);
        }else{
            Read_Byte_IDGCmd(pResponseData,&dwResponseDataSize,u32TimeoutMS, pfuncCB);
        }
         _stprintf_s(tszMsg,BUF_MAX_SIZE,_T("00005: resp data size  = %d, Response from Reader"), dwResponseDataSize);
        LOG_INFO(tszMsg);
        //
        //
         //LogInfoHexFomrated(pResponseData, dwResponseDataSize);
        if (dwResponseDataSize > 12)
        {
            _stprintf_s(tszMsg,BUF_MAX_SIZE,_T("%s , NEO/AR Status = 0x%02x "),szIDGCmd,pResponseData[m_nOffsetRespSts]);
        }
    }
    else
    {
        _stprintf_s(tszMsg,BUF_MAX_SIZE,_T("%s, Can't Get Response from Reader"), szIDGCmd);
    }

    LOG_INFO(tszMsg)

    return dwResponseDataSize ;
}

DWORD CIdtUsbHid::SendAndGetResponseUSBHID(char * szIDGCmd, LPBYTE pSendData, DWORD dwSendDataSize, LPBYTE pResponseData,DWORD dwResponseDataSize,DWORD u32TimeoutMS)
{
    int nTmp;

    ASSERT(pSendData);
    ASSERT(pResponseData);
    ASSERT(dwSendDataSize > 0);

    // TODO: Add your control notification handler code here
    if (!bIsOpen())
    {
        LOG_INFO("Device isn't Open yet");
        return 0;
    }

    if (m_caps.OutputReportByteLength==0x41){
        // NGA USB Frame protocol
        nTmp = Write(pSendData,dwSendDataSize);//,u32TimeoutMS);
    }else{
        // Send Data
        nTmp = Write_Byte_IDGCmd(pSendData,dwSendDataSize,u32TimeoutMS);
    }
    // Recv Data
    if (nTmp > 0)
    {
        if (bIsUSB_KBWedge()){
            // USB-KB Wedge
            dwResponseDataSize = Read_KBWedge(pResponseData,dwResponseDataSize,u32TimeoutMS, NULL);
        }else
        if (m_caps.InputReportByteLength==0x41){
            dwResponseDataSize = Read(pResponseData,dwResponseDataSize, u32TimeoutMS);
        }else{
            Read_Byte_IDGCmd(pResponseData,&dwResponseDataSize,u32TimeoutMS);
        }
        //sprintf_s(tszMsg,BUF_MAX_SIZE,"%s , NEO/AR Status = 0x%02x ",szIDGCmd,pResponseData[m_nOffsetRespSts]);
    }
    else
    {
        //sprintf_s(tszMsg,BUF_MAX_SIZE,"%s, Can't Get Response from Reader", szIDGCmd);
    }

    //LOG_INFO(tszMsg);

    return dwResponseDataSize ;
}
//
DWORD CIdtUsbHid::SendDataUSBHID(char * szIDGCmd, LPBYTE pSendData, DWORD dwSendDataSize, DWORD u32TimeoutMS)
{
    DWORD dwTmp;

    ASSERT(pSendData);
    ASSERT(dwSendDataSize > 0);

    // TODO: Add your control notification handler code here
    if (!bIsOpen())
    {
        LOG_INFO("Device isn't Open yet");
        return 0;
    }
    // Send Data
    dwTmp = Write_Byte_IDGCmd(pSendData,dwSendDataSize,u32TimeoutMS);
    // Recv Data

    return dwTmp;
}
//
unsigned int CIdtUsbHid::GetReport(unsigned char* aucmpData, unsigned int auiWaitMs)
{
    unsigned char lucmResponse[68];
    unsigned int  luiResponseLen =0;
    DWORD dwBytesRead;
    //	if(LookDevice(0x0ACD, 0X1310))
    //	{
    aucmpData[0] =0x00;
    HANDLE hidDevice;
    hidDevice = m_hidDevice;
    //
    ResetEvent(m_ReadEvent);
    if(!ReadFile(hidDevice, lucmResponse, m_caps.InputReportByteLength, &dwBytesRead, &m_overlapped))
    {
        DWORD hres;
        DWORD errCode = GetLastError();

        switch(errCode)
        {
        case ERROR_IO_PENDING:			
            hres=WaitForSingleObject(m_ReadEvent, auiWaitMs);
            if(WAIT_OBJECT_0 == hres)
            {		
                if(GetOverlappedResult(hidDevice, &m_overlapped, &dwBytesRead, FALSE))
                {
                    ResetEvent(m_ReadEvent);
                }
            }
            else
            {
                //					//Time out........
                if(!CancelIo(hidDevice))
                {
                    ResetEvent(m_ReadEvent);
                    //	CloseHandle(m_hidDevice);
                    //	m_hidDevice =INVALID_HANDLE_VALUE;
                    return 0;
                }
            }
            break;

        case ERROR_DEVICE_NOT_CONNECTED:
            //	CloseHandle(m_hidDevice);
            //	m_hidDevice =INVALID_HANDLE_VALUE;
            return 0;

        default:
            //	CloseHandle(m_hidDevice);
            //	m_hidDevice =INVALID_HANDLE_VALUE;
            return 0;
        }
    }

#if 0
    if(dwBytesRead && lucmResponse[1])
    {
        for(unsigned int luii=0; luii< 64; luii++)
        {
            aucmpData[luii] = lucmResponse[luii+1];
        }
    }
    else
    {
        aucmpData[0] =0x00;
    }
#else
    if(dwBytesRead>0)
    {
       /* for(unsigned int luii=0; luii < (MAX_FRAME_SINGLE_SIZE+1); luii++)
        {
            aucmpData[luii] = lucmResponse[luii];
        }*/
        ::memcpy(aucmpData,lucmResponse+1,dwBytesRead-1);
    }
    else
    {
        aucmpData[0] =0x00;
    }
#endif

    return dwBytesRead;	

}

unsigned int CIdtUsbHid::GetResponse(unsigned char* aucmpData, unsigned int auiWaitMs) 
{
    unsigned char lucmPartResponse[4096];
    unsigned int  luiResponseLen =0;
    unsigned int  luii;
    unsigned int  nOffLen = 0;
    int nRetry = 0;
    int nRetryMax = 5;
    DWORD dwBytesRead=0;
    
    while(1)
    {
        GetReport(lucmPartResponse, auiWaitMs);

        if(lucmPartResponse[0] ==0x00){
            nRetry++;
            if (nRetry<nRetryMax){
                //Waiting for short time and retry
                ::Sleep(50);
                continue;
            }

            break;
        }
        nRetry = 0;

        dwBytesRead =lucmPartResponse[0] & 0x7F;

       /* for(luii =0; luii< dwBytesRead; luii++)
        {
            aucmpData[luiResponseLen++] =lucmPartResponse[luii+1];
        }*/
        ::memcpy(aucmpData+luiResponseLen,lucmPartResponse+1,dwBytesRead);
        luiResponseLen += dwBytesRead;

        // No more frame, stop
        if(lucmPartResponse[0] < 0x80) break;
    }

    //	CloseHandle(m_hidDevice);
    //	m_hidDevice =INVALID_HANDLE_VALUE;
    return luiResponseLen;
}

unsigned int CIdtUsbHid::Write(unsigned char *pbuf, unsigned int iLen, long lTimeout)
{
    if(LookDevice(m_u32VID,m_u32PID))
    {	
        if(Write_Byte(pbuf, iLen))
        {
            //	Close();
            return iLen;
        }

        //	Close();
    }

    return 0;
}


unsigned int CIdtUsbHid::Write(unsigned char* aucmpData, unsigned int auiDataLen) 
{
    unsigned char lucmCommand[68];
    unsigned int  luiCommandLen =auiDataLen;
    unsigned int  luiCommandCount =1 + auiDataLen/MAX_FRAME_SINGLE_SIZE; // MAX_FRAME_SINGLE_SIZE=63
    if((auiDataLen % MAX_FRAME_SINGLE_SIZE) ==0x00) luiCommandCount --;
    unsigned int  luii;
    DWORD dwBytesWritten;
    lucmCommand[0] =0x00;
    
    do{
        /*for(luii=0;luii<68;luii++)
        {
            lucmCommand[luii] =0x00;
        }*/
        ::memset(lucmCommand,0x00,sizeof(lucmCommand));

        if(luiCommandCount >1)
        {
            luiCommandLen -= MAX_FRAME_SINGLE_SIZE; //63;
            lucmCommand[1] =0xBF;//63;
            for(luii=2;luii<65;luii++)
            {
                lucmCommand[luii] =*aucmpData++;
            }
        }
        else
        {
            int nMax = 2+luiCommandLen;
            lucmCommand[1] =luiCommandLen;
            for(luii=2;luii<nMax;luii++) // 65, max value
            {
                lucmCommand[luii] =*aucmpData++;
            }
        }

        m_overlapped.Offset =0;
        m_overlapped.OffsetHigh =0;
        //	LookDevice(0x1D5F,0x0100);
        if(!WriteFile(m_hidDevice, lucmCommand, m_caps.OutputReportByteLength, &dwBytesWritten, &m_overlapped))
        {
            DWORD dwErrorCode =GetLastError();
            if(dwErrorCode == ERROR_IO_PENDING)
            {
                if(WAIT_OBJECT_0 ==WaitForSingleObject(m_ReadEvent, 1000))
                {		
                    ResetEvent(m_ReadEvent);
                }
                else
                {
                    CloseHandle(m_hidDevice);
                    m_hidDevice =INVALID_HANDLE_VALUE;
                    return 0;
                }
            }
            else
            {
                CloseHandle(m_hidDevice);
                m_hidDevice =INVALID_HANDLE_VALUE;
                return 0;
            }
        }
        luiCommandCount--;
    }while(luiCommandCount);

    return 1;
}


unsigned int CIdtUsbHid::Read(unsigned char * pBuf, unsigned int iLim, long lTimeout)
{
    //	if(LookDevice(0x0ACD,0x1520))
    //	{		
    return GetResponse(pBuf, iLim);
    //	}
    return 0;
}

unsigned int CIdtUsbHid::SendCommand(UINT auiVid, UINT auiPid, unsigned char* aucmpSource, unsigned int auiSourceLen, unsigned char* aucmpResp, unsigned int auiWaitMs)
{
    if(LookDevice(auiVid, auiPid))
    {
        if(auiSourceLen)
        {
            if(Write_Byte(aucmpSource, auiSourceLen))
            {
                if(aucmpResp) return GetResponse(aucmpResp, auiWaitMs);
            }
        }
        else
        {
            if(aucmpResp) return GetResponse(aucmpResp, auiWaitMs);
        }

    }

    return 0;
}

//End

void CIdtUsbHid::Close()
{
    if (m_hidDevice != INVALID_HANDLE_VALUE)
    {
        LOG_INFO("Closing Handles");

        CloseHandle(m_hidDevice);
        //
        m_hidDevice =INVALID_HANDLE_VALUE;
    }
    //
    m_u32VID =0;
    m_u32PID =0;

    if (m_ReadEvent)
        CloseHandle(m_ReadEvent);
    if (m_WriteEvent)
        CloseHandle(m_WriteEvent);
    m_ReadEvent = NULL;
    m_WriteEvent = NULL;

    // Reset to Unknown Mode
    m_nUsbDevMode = -1;

}
//============[ 2015 Sept 30 Sync from EMV L1 SDK Tool "PayPass Reader "V3.01.001" ]================
/*
Description: LookDevicePIDList(...)
     Seek out the IDTECH USBHID Reader Devices Info [PID + Device Path] and available number of them.

Parameters: 
  [IN]
        uiVid : VID is not required since (IDTECH's VID = 0x0ACD) is FIXED
        puiPidLst : A pointer to save PID List for Multiple IDTECH USBHID Devices
        ppDevPathLst : A String container Pointer to save Available/Visible IDTECH USBHID Devices Paths
        nBufSizeMax : max element number of puPidLst[] & ppDevPathLst[]
  [OUT]
        puiPidLst : Found PID List for Multiple IDTECH USBHID Devices
        ppDevPathLst : Found IDTECH USBHID Devices Paths
Return:
        number of found IDTECH USBHID devices
*/
unsigned int CIdtUsbHid::LookDevicePIDList(UINT uiVid, UINT * puiPidLst, LPSTR * ppDevPathLst  ,int nBufSizeMax) // return value is number of Pid List
{
    GUID			HidGuid;	    //UID of HID device class
    HDEVINFO		hDevInfoSet = INVALID_HANDLE_VALUE;	//handle to all hid device's info set.
    SP_DEVICE_INTERFACE_DATA	devIfData;  //All HID devices interface data.
    SP_DEVICE_INTERFACE_DETAIL_DATA *pIfDetail = NULL; //interface detail
    HIDD_ATTRIBUTES		attrs;		//Structrue with VID and PID
    ULONG			len, required;	//length of interface datail structure
    PHIDP_PREPARSED_DATA	preparsedData = NULL;
    HANDLE hidDevice = INVALID_HANDLE_VALUE;
    LPTSTR * ppDevPathSeek=ppDevPathLst;

    int nNumOfPair = 0;
    // Params ChkLst
    if (NULL == ppDevPathLst)
        return 0;
    if (NULL == puiPidLst)
        return 0;
    if (nBufSizeMax < 1)
        return 0;
    //
    HidD_GetHidGuid(&HidGuid);
    if( (hDevInfoSet=SetupDiGetClassDevs(&HidGuid, NULL, NULL,
        DIGCF_PRESENT|DIGCF_INTERFACEDEVICE)) == INVALID_HANDLE_VALUE)
    {
        return 0;
    }
    //
    devIfData.cbSize = sizeof(devIfData);
    int index=0;
    while( (SetupDiEnumDeviceInterfaces(hDevInfoSet, 0, 
        &HidGuid, index++, &devIfData) != 0) && (nNumOfPair<nBufSizeMax))
    {   
        if(INVALID_HANDLE_VALUE != hidDevice)
        {
            ::CloseHandle(hidDevice);
            hidDevice = NULL;
        }
        if (pIfDetail)
        {
            free(pIfDetail);
            pIfDetail = NULL;
        }
        //this call should fail, just get the detail structure length
        SetupDiGetDeviceInterfaceDetail( hDevInfoSet, &devIfData, NULL, 0, &len, NULL);	

        pIfDetail = (SP_DEVICE_INTERFACE_DETAIL_DATA*)malloc(len);
        if (NULL == pIfDetail)
            continue;
        pIfDetail->cbSize = sizeof(SP_DEVICE_INTERFACE_DETAIL_DATA);
        if( ! SetupDiGetDeviceInterfaceDetail(hDevInfoSet, &devIfData, pIfDetail, len, &required, NULL))
        {
            continue;
        }
        //
        // Remove FILE_SHARE_XXXXX for standalone usage...
        hidDevice=CreateFile(
            pIfDetail->DevicePath
            ,GENERIC_WRITE|GENERIC_READ
            ,m_dwOpenDevModeShare
            , (LPSECURITY_ATTRIBUTES) NULL
            ,OPEN_EXISTING
            , m_dwOpenDevModeFlagAndAttr
            , NULL); 
        if(hidDevice==INVALID_HANDLE_VALUE)
        {
            DWORD luiError =GetLastError();
            continue;
        }

        //
        if( !HidD_GetAttributes(hidDevice, &attrs))
        {
            continue;
        }
        // If VID is same, then we save this info.
        if( attrs.VendorID!=uiVid)// || attrs.ProductID!=auiPid)
        {
            continue;
        }
        // Cool, now we save necessary dev info to output arrary
        //========[ End of Save Process ]==========
        //====[ Copy Dev Path String ]====
        if (NULL != *ppDevPathSeek)
        {
            free(*ppDevPathSeek);
            *ppDevPathSeek = NULL;
        }
        len -= (sizeof(DWORD)-1); // reserve 1 byte for \x0
        *ppDevPathSeek = (LPSTR)::malloc(len);
        if (NULL == *ppDevPathSeek)
            break;
        memset(*ppDevPathSeek, 0 , len);
        strcpy_s( *ppDevPathSeek,len, pIfDetail->DevicePath);
        //======[ Save PID ]=======
        puiPidLst[nNumOfPair] = attrs.ProductID;
        // Advancing Pointer
        ppDevPathSeek++; nNumOfPair++;
        //========[ End of Save Process ]==========
    } // while(...)

    // =======[ Cleanup & Free Resource ]========
    if (pIfDetail)
    {
        free(pIfDetail);
        pIfDetail = NULL;
    }
    //
    if(INVALID_HANDLE_VALUE != hidDevice)
    {
        ::CloseHandle(hidDevice);
        hidDevice = NULL;
    }
    SetupDiDestroyDeviceInfoList(hDevInfoSet);    
    //
    return (unsigned int)nNumOfPair;
}

/*
Description: OpenDeviceNoSeek(...)
    Open Indicated Device by given device Path without system seek loop.

Params:
	[IN]
        auiVid, auiPid = VID,PID
        ptszDevPath = Device Path
	[OUT]
        N/A
Return:
    = 0 : Device Path is Invalid / Not Found
    = -1 : Device Path and [VID, PID] mismatched
    = -2 : Can't Parse USB HID Data
    = -3 :
Ret:
> 0 --> Found & Open
<= 0 --> Not Found / Invliad Device Path / Get Info Failed
*/
int CIdtUsbHid::OpenDeviceNoSeek(UINT auiVid, UINT auiPid,LPTSTR ptszDevPath) 
{
    HIDD_ATTRIBUTES		attrs;
    PHIDP_PREPARSED_DATA	preparsedData = NULL;


    int nRet = 0;

    if (NULL == ptszDevPath)
        return 0;

    while (true)
    {
        // Remove FILE_SHARE_XXXXX for standalone usage...
        m_hidDevice=CreateFile(
            ptszDevPath //pIfDetail->DevicePath
            , GENERIC_WRITE|GENERIC_READ
            ,m_dwOpenDevModeShare
            , (LPSECURITY_ATTRIBUTES) NULL
            ,OPEN_EXISTING
            , m_dwOpenDevModeFlagAndAttr
            , NULL); // FILE_FLAG_OVERLAPPED causes asynchronous I/O handling, this may
        if(m_hidDevice==INVALID_HANDLE_VALUE)
        {
            DWORD luiError =GetLastError();
            //free(pIfDetail);
            return nRet;
        }
        //::memset(m_devPathName, 0 , 1024);
        //strcpy(m_devPathName, pIfDetail->DevicePath);
        //free(pIfDetail);
        if( !HidD_GetAttributes(m_hidDevice, &attrs))
        {
            break;
        }

        if( attrs.VendorID!=auiVid || attrs.ProductID!=auiPid)
        {
            nRet = -1;
            break;
        }		

        if( ! HidD_GetPreparsedData(m_hidDevice, &preparsedData))
        {
            nRet = -2;
            break;
        } 

        if( ! HidP_GetCaps(preparsedData, &m_caps))
        {
            nRet = -3;
            break;
        }
        if(0 == m_ReadEvent)
        {
            m_ReadEvent = CreateEvent(NULL, TRUE, TRUE, NULL);
        }
        else
            ResetEvent(m_ReadEvent);
        //
        if(m_WriteEvent == 0)
        {
            m_WriteEvent = CreateEvent(NULL, TRUE, FALSE, NULL);
        }
        else
            ResetEvent(m_WriteEvent);

        nRet = 1;
        break;
    } // while (true)
    //
    if (NULL != preparsedData)
    {
        HidD_FreePreparsedData(preparsedData);
    }
    if (0<nRet)
    {
        m_overlapped.hEvent = m_ReadEvent;
        m_overlapped.Offset = 0;
        m_overlapped.OffsetHigh = 0;
        m_u32VID =auiVid;
        m_u32PID =auiPid;
    }
    else
        Close();
    //
    return nRet;
}

//
void CIdtUsbHid::SetVidPid(UINT uiVID,UINT uiPID)
{
    m_u32VID = uiVID;
    m_u32PID = uiPID;
}

//
/*
  Description:
  Parameters:
	[IN]
	[OUT]
  Return:
*/
unsigned int CIdtUsbHid::InterruptSendUSBFrame(unsigned char * pSendData, unsigned int nSendDataLen)
{
    unsigned char arrSendCommand[512] = {0};
    unsigned int  nCmdLen = nSendDataLen;
    unsigned int  nCmdCnt = 1 + nCmdLen / 63;
    if((nCmdLen % 63) == 0x00)  nCmdCnt--;

    unsigned int nPrimCmdCnt = nCmdCnt;
    unsigned int nIndex = 0;
    unsigned int nFirstPachage = 1;
    unsigned long nByteWritten = 0;
    HANDLE hidDevice;
    hidDevice = m_hidDevice;
    do 
    {
        if(nPrimCmdCnt > 2)
        {
            if(nFirstPachage)
            {
                nCmdLen -= 63;
                arrSendCommand[0] = 2;
                for(nIndex = 1; nIndex < 64; nIndex++)
                {
                    arrSendCommand[nIndex] = *pSendData++;
                }
                nFirstPachage = 0;
                goto BeginSend;
            }
            if( nCmdCnt > 1 && !nFirstPachage )
            {
                nCmdLen -= 63;
                arrSendCommand[0] = 3;
                for(nIndex = 1; nIndex < 64; nIndex++)
                {
                    arrSendCommand[nIndex] = *pSendData++;
                }
                nFirstPachage = 0;
                goto BeginSend;
            }
            if( nCmdCnt == 1)
            {
                arrSendCommand[0] = 4;
                for(nIndex = 1; nIndex < nCmdLen + 1; nIndex++)
                {
                    arrSendCommand[nIndex] = *pSendData++;
                }
                goto BeginSend;
            }
        }
        else if(nPrimCmdCnt == 2)
        {
            if(nCmdCnt == 1)
            {
                arrSendCommand[0] = 4;
                for(nIndex = 1; nIndex < nCmdLen + 1; nIndex++)
                {
                    arrSendCommand[nIndex] = *pSendData++;
                }
            }
            else
            {
                nCmdLen -= 63;
                arrSendCommand[0] = 2;
                for(nIndex = 1; nIndex < 64; nIndex++)
                {
                    arrSendCommand[nIndex] = *pSendData++;
                }
            }
        }
        else 
        {
            arrSendCommand[0] = 1;
            for(nIndex = 1; nIndex < nCmdLen + 1; nIndex++)
            {
                arrSendCommand[nIndex] = *pSendData++;
            }
        }


BeginSend:
        this->m_overlapped.Offset = 0;
        this->m_overlapped.OffsetHigh = 0;

        /* Padding Zero If length is not up to 64 bytes */
        if (nIndex < 64)
        {
            int nPaddingLen = 64-nIndex;
            ::memset(&arrSendCommand[nIndex],0x00,nPaddingLen);
        }

        if( !WriteFile(hidDevice, arrSendCommand,this->m_caps.OutputReportByteLength, &nByteWritten, &this->m_overlapped) )
        {
            DWORD dwErrorCode = GetLastError();
            if(dwErrorCode == ERROR_IO_PENDING)
            {
                if(WAIT_OBJECT_0 == WaitForSingleObject(this->m_ReadEvent, 100))
                {
                    ResetEvent(this->m_ReadEvent);
                }
                else
                    return 0;
            }
        }
        nCmdCnt--;
    }
    while(nCmdCnt);   

    return 1;
}

unsigned int CIdtUsbHid::InterruptSend(unsigned char * pSendData, unsigned int nSendDataLen)
{
    if (m_bIsUSBFrame)
        return InterruptSendUSBFrame(pSendData, nSendDataLen);

    return InterruptDirectSend(pSendData,nSendDataLen);
}


unsigned int CIdtUsbHid::InterruptDirectSend(unsigned char * pSendData, unsigned int nSendDataLen)
{
    //unsigned char pSendBuf = new unsigned char [1024 * 8];
    unsigned char arrSendCommand[512] = {0};
    unsigned int nSendLen = nSendDataLen;
    //memset(pSendBuf, 0, 1024 * 8);
    unsigned long nByteWritten = 0;
    unsigned int nCmdCnt = 1 + (nSendLen - 1) / 64;
    unsigned int nIndex = 0;

    HANDLE hidDevice;
    hidDevice = m_hidDevice;
    do {
        memcpy(arrSendCommand, pSendData + nIndex, 64);
        nIndex += 64;
        //BeginSend:
        this->m_overlapped.Offset = 0;
        this->m_overlapped.OffsetHigh = 0;
        if( !WriteFile(hidDevice, arrSendCommand,this->m_caps.OutputReportByteLength, &nByteWritten, &this->m_overlapped) )
        {
            DWORD dwErrorCode = GetLastError();
            if(dwErrorCode == ERROR_IO_PENDING)
            {
                if(WAIT_OBJECT_0 == WaitForSingleObject(this->m_ReadEvent, 100))
                {
                    ResetEvent(this->m_ReadEvent);
                }
                else
                    return 0;
            }
        }
        nCmdCnt--;


    } while(nCmdCnt);

    return 1;
}

unsigned int CIdtUsbHid::InterruptRecv(unsigned char * pRecvData, unsigned int & nRecvDataLen)
{
    BYTE arrRecv[1024] = {0};
    UINT nRecvLen = 0;
    UINT nIndex = 0;
    DWORD dwByteRead = 0;
    BYTE arrRecvPackage[64] = {0};
    UINT nRptCnt = 0;
    UINT nRptLen = 0;
    BYTE nPackageHeadByte = 0x00;

    while(1)
    {
        memset(arrRecvPackage,0,sizeof(arrRecvPackage));
        nRptLen = GetReport(arrRecvPackage, 500);
        if(nRptLen) nRptCnt++;
        nPackageHeadByte = arrRecvPackage[0];

        if( nPackageHeadByte == 0x00 )   
        {
            if( 1 == nRptCnt )  nRptCnt = 0;
            break;
        }

        //dwByteRead = nRptLen;

        switch(nPackageHeadByte)
        {
        case 0x01:
            {
                if(0x00 == arrRecvPackage[m_nOffsetCmd])
                    dwByteRead = (((WORD)(arrRecvPackage[m_nOffsetLenH])) << 8) + arrRecvPackage[m_nOffsetLenL] + m_nPkgMinLen + 1;
                else
                    dwByteRead = 0x10;
            }

            break;
        case 0x02:
            dwByteRead = 0x40;
            break;
        case 0x03:
            dwByteRead = 0x40;
            break;
        case 0x04:
            {
                // Bug, BUG !!
                // This is error parsing format
                dwByteRead = (((WORD)(arrRecv[12])) << 8) + arrRecv[m_nOffsetLenH] + m_nPkgMinLen + nRptCnt - (nRptCnt - 1) * 64; 
            }
            break;
        }

        for(nIndex = 0; nIndex < dwByteRead - 1; nIndex++)
        {
            arrRecv[nRecvLen++] = arrRecvPackage[nIndex + 1];
        }
        if(nPackageHeadByte == 0x01 || nPackageHeadByte == 0x04)    break;

    }	
    if( nRptCnt > 1)
        nRecvLen = 63 * (nRptCnt - 1) + dwByteRead - 1;
    else if( nRptCnt == 1)
        nRecvLen = dwByteRead - 1;
    //if( nRptCnt == 0)
    else
        nRecvLen = 0;
    nRecvDataLen = nRecvLen;
    memcpy(pRecvData, arrRecv, nRecvLen);
    return nRecvLen;
}

//
DWORD CIdtUsbHid::SendAndGetResponseUSBHID_SendFrame64bytes(LPBYTE pSendData, DWORD dwSendDataSize, LPBYTE pResponseData,DWORD dwResponseDataSize,DWORD u32TimeoutMS, pFUNC_APP_SLEEP pfuncCB)
{
    int nTmp;

    LOG_INFO("==============[ 00001 ]=================" );
    if (pSendData == NULL)
    {
        _stprintf_s(tszMsg,TMP_BUF_LEN,_T("SendAndGetResponseUSBHID_SendFrame64bytes(): pSendData = NULL"));
        //MessageBox(NULL,tszMsg,_T("Error"),MB_OK|MB_ICONERROR );
        return 0;
    }
    if (pResponseData == NULL)
    {
        _stprintf_s(tszMsg,TMP_BUF_LEN,_T("SendAndGetResponseUSBHID_SendFrame64bytes(): pResponseData = NULL"));
        //MessageBox(NULL,tszMsg,_T("Error"),MB_OK|MB_ICONERROR );
        return 0;
    }

    if (dwSendDataSize < 1)
    {
        _stprintf_s(tszMsg,TMP_BUF_LEN,_T("SendAndGetResponseUSBHID_SendFrame64bytes(): dwSendDataSize = %d"), dwSendDataSize );
        //MessageBox(NULL,tszMsg,_T("Error"),MB_OK|MB_ICONERROR );
        return 0;
    }
    // TODO: Add your control notification handler code here
    LOG_INFO("00002");
    if (!bIsOpen())
    {
        LOG_INFO("Device isn't Open yet");
        return 0;
    }

    // Send Data
    LOG_INFO("00003");

    //if (m_bIsUSBFrame)
    //   nTmp = Write_Byte_IDGCmd(pSendData,dwSendDataSize,u32TimeoutMS, pfuncCB);
    //else
    nTmp =  InterruptDirectSend(pSendData, dwSendDataSize); // nTmp = 0 (FAIL) or 1 (SUCESS)
    // Recv Data
    if (nTmp > 0)
    {
        LOG_INFO("00004");
        // Give Error Status Value  in response frame, must Consider RID 1st byte presents status @ USB HID Frame
        pResponseData[m_nOffsetRespSts] = 0xFF; pResponseData[m_nOffsetRespSts+1] = 0xFF;

        Read_Byte_IDGCmd(pResponseData,&dwResponseDataSize,u32TimeoutMS, pfuncCB);
        _stprintf_s(tszMsg,BUF_MAX_SIZE,_T("00005: resp data size  = %d, Response from Reader"), dwResponseDataSize);
        LOG_INFO(tszMsg);
        //
        //
        //LogInfoHexFomrated(pResponseData, dwResponseDataSize);
        if (dwResponseDataSize > 12)
        {
            _stprintf_s(tszMsg,BUF_MAX_SIZE,_T("NEO/AR Status = 0x%02x "),pResponseData[m_nOffsetRespSts]);
        }
    }
    else
    {
        _stprintf_s(tszMsg,BUF_MAX_SIZE,_T("Can't Get Response from Reader"));
    }

    LOG_INFO(tszMsg)

    return dwResponseDataSize ;
}

void CIdtUsbHid::SetViVOProtocl_V2()
{
    // check Rx[8] :
    // V2 = '2'; 0x32
    // V3 = '3'; 0x33
    m_nOffsetCmd = 11; // Rx, V2 = 10; V3 = 10, then + Since RID --> 11; 12
    if (1 == m_nUsbDevMode ){
        // If USB-KB mode, RID has been removed
        m_nOffsetCmd --;
    }
    m_nOffsetCmdSub = m_nOffsetCmd+1; // Rx, V2 = N/A; V3 = 11;
    m_nOffsetRespSts = m_nOffsetCmdSub; // Rx, V2 = 11; V3 = 12;
    m_nOffsetLenH = m_nOffsetRespSts+1; // Rx, V2 = 12; V3 = 13
    m_nOffsetLenL = m_nOffsetLenH+1; // Rx, V2 = 13; V3 = 14
    // No Data Portion
    // Rx Pkg = <Hdr,9B><0x00,1B><Cmd,1B><Resp Sts,1B><Data Len,H,L,2B><Data...><CRC,2B>
    m_nPkgMinLen = 16;  // 14 + 2 (CRC)
}

void CIdtUsbHid::SetViVOProtocl_V3()
{
    m_nOffsetCmd = 11; // Rx, V2 = 10; V3 = 10, then + Since RID --> 11; 12
    if (1 == m_nUsbDevMode ){
        // If USB-KB mode, RID has been removed
        m_nOffsetCmd --;
    }
    m_nOffsetCmdSub = m_nOffsetCmd+1; // Rx, V2 = N/A; V3 = 11;
    m_nOffsetRespSts = m_nOffsetCmdSub+1; // Rx, V2 = 11; V3 = 12;
    m_nOffsetLenH = m_nOffsetRespSts+1; // Rx, V2 = 12; V3 = 13
    m_nOffsetLenL = m_nOffsetLenH+1; // Rx, V2 = 13; V3 = 14
    // No Data Portion
    // Rx Pkg = <Hdr,9B><0x00,1B><Cmd,Sub,2B><Resp Sts,1B><Data Len,H,L,2B><Data...><CRC,2B>
    m_nPkgMinLen = 17; // 15 + 2 (CRC)
}

int CIdtUsbHid::ViVOProtocol_GetLength_Pkg(LPBYTE pData)
{
    return (14 + ViVOProtocol_GetLength(pData)+2);
}

int CIdtUsbHid::ViVOProtocol_GetLength(LPBYTE pData)
{
   return (int)(((((DWORD)pData[m_nOffsetLenH]) << 8) & 0xFF00) | pData[m_nOffsetLenL]);
}

#if __USB_KEYB_WEDGE_H_
//
bool CIdtUsbHid::bIsUSB_KBWedge()
{
    return (m_nUsbDevMode == 1);
}

// default param: accessMode = 0
BOOL CIdtUsbHid::Open_KBWedge(UINT vid, UINT pid, UINT accessMode)
{
	GUID			HidGuid;	    /*GUID of HID device class              */
	HDEVINFO		hDevInfoSet;	/*handle to all hid device's info set.  */
	SP_DEVICE_INTERFACE_DATA	devIfData;  /*All HID devices interface data.*/
	SP_DEVICE_INTERFACE_DETAIL_DATA *pIfDetail; /*interface detail          */
	HIDD_ATTRIBUTES		attrs;		/*Structrue with VID and PID            */
	ULONG			len, required;	/*length of interface datail structure  */
	PHIDP_PREPARSED_DATA	preparsedData;

	HidD_GetHidGuid(&HidGuid);
	if ((hDevInfoSet = SetupDiGetClassDevs(&HidGuid, NULL, NULL,
		DIGCF_PRESENT | DIGCF_INTERFACEDEVICE)) == INVALID_HANDLE_VALUE)
	{
		return FALSE;
	}
	devIfData.cbSize = sizeof(devIfData);
	UINT index = 0;
	while (SetupDiEnumDeviceInterfaces(hDevInfoSet, 0, &HidGuid, index++, &devIfData) != 0)
	{

		/*this call should fail, just get the detail structrue length*/
		SetupDiGetDeviceInterfaceDetail(hDevInfoSet, &devIfData, NULL, 0, &len, NULL);

		pIfDetail = (SP_DEVICE_INTERFACE_DETAIL_DATA*)malloc(len);
		pIfDetail->cbSize = sizeof(SP_DEVICE_INTERFACE_DETAIL_DATA);
		if (!SetupDiGetDeviceInterfaceDetail(hDevInfoSet, &devIfData, pIfDetail, len, &required, NULL))
		{
			SetupDiDestroyDeviceInfoList(hDevInfoSet);
			free(pIfDetail);
			return FALSE;
		}
		DWORD access = 0;//GENERIC_WRITE|GENERIC_READ; //|FILE_WRITE_DATA;//0;
		DWORD attributes = 0;
		if (accessMode == 1)
		{
			access = GENERIC_WRITE | GENERIC_READ;
			attributes = FILE_FLAG_OVERLAPPED;
		}
		if ((m_hidDevice = CreateFile(pIfDetail->DevicePath,
			access, FILE_SHARE_READ | FILE_SHARE_WRITE,
			(LPSECURITY_ATTRIBUTES)NULL,
			OPEN_EXISTING, attributes, NULL)) == INVALID_HANDLE_VALUE)
		{
			free(pIfDetail);
			continue;
		}
		memset(m_devPathName, 0, 1024);
		strcpy_s(m_devPathName, pIfDetail->DevicePath);
		free(pIfDetail);
		if (!HidD_GetAttributes(m_hidDevice, &attrs))
		{
			CloseHandle(m_hidDevice);
			m_hidDevice = INVALID_HANDLE_VALUE;
			continue;
		}
		if (attrs.VendorID != vid || attrs.ProductID != pid)
		{
			CloseHandle(m_hidDevice);
			m_hidDevice = INVALID_HANDLE_VALUE;
			continue;
		}

		if (!HidD_GetPreparsedData(m_hidDevice, &preparsedData))
		{
			CloseHandle(m_hidDevice);
			m_hidDevice = INVALID_HANDLE_VALUE;
			SetupDiDestroyDeviceInfoList(hDevInfoSet);
			return FALSE;
		}

		if (!HidP_GetCaps(preparsedData, &m_caps)) {
			CloseHandle(m_hidDevice);
			m_hidDevice = INVALID_HANDLE_VALUE;
			HidD_FreePreparsedData(preparsedData);
			SetupDiDestroyDeviceInfoList(hDevInfoSet);
			return FALSE;
		}
		HidD_FreePreparsedData(preparsedData);

		break;	/*OK, to here, we find one right device, and get it's caps. */
	}
	SetupDiDestroyDeviceInfoList(hDevInfoSet);

#ifdef HID_ONLY
	if (m_hidDevice != INVALID_HANDLE_VALUE){
		hdev2 = CreateFile(m_devPathName,
			GENERIC_WRITE | GENERIC_READ,
			FILE_SHARE_READ | FILE_SHARE_WRITE,
			(LPSECURITY_ATTRIBUTES)NULL,
			OPEN_EXISTING,
			FILE_FLAG_NO_BUFFERING,
			NULL);
	}
	return (m_hidDevice != INVALID_HANDLE_VALUE) && (hdev2 != INVALID_HANDLE_VALUE);
#endif 

    if (m_hidDevice != INVALID_HANDLE_VALUE){
        m_nUsbDevMode = 1;
    }

	return (m_hidDevice != INVALID_HANDLE_VALUE);
}

unsigned int CIdtUsbHid::Write_KBWedge(BYTE * data, UINT len,DWORD u32TimeoutMS, DWORD (* pfuncCB)(void))
{
	UINT sl = m_caps.FeatureReportByteLength;
	if (!(sl >0))
		return 0;

	unsigned char *ph, *ps = (unsigned char*)malloc(sl);
	UINT cyplen = 0;
	for (ph = data; ph < (data + len); ph += sl - 1)
	{
		cyplen = (ph + sl - 1) <= (data + len) ? sl - 1 : data + len - ph;
        // Note:If No Report ID, first byte must be zero
		memset(ps, 0, sl);
		memcpy(ps + 1, ph, cyplen);
		if (HidD_SetFeature(m_hidDevice, ps, sl) < 0)
		{
			free(ps);
			return 0;
		}
		Sleep(5);
        if (pfuncCB != NULL)
            pfuncCB();
	}
	free(ps);

	/* wait the device to do response, then furthmore act can do.*/
	Sleep(25);   /* MODIFY: 10 --> 25, wait a 25 ms time, TC 2005.8.24 */
	return ph - (sl - 1) + cyplen - data;

}
//
unsigned int CIdtUsbHid::Read_KBWedge(BYTE * data, UINT io_unlen,DWORD u32TimeoutMS, DWORD (* pfuncCB)(void))
{
	UINT rl = m_caps.FeatureReportByteLength;		/* receiving buffer lenght      */
	unsigned char * r = (unsigned char*)malloc(rl);	/* receiving buffer         */
	unsigned char * h = data;			/* startt position to store the receiveed data.*/
	UINT allzerocount = 0;
	memset(data, 0, io_unlen);
    DWORD dwErr = 0xFFFFFFFF;
    int nStgChk = 0;
    int nRxLen = 0;
    int nRxPkgLen = 0;
    bool bNotDone = true;
    int nLoopCnt = 0;
    char szBuf[128];

	do{
        // Note:If No Report ID, first byte must be zero
		memset(r, 0, rl);
		if (HidD_GetFeature(m_hidDevice, r, rl) < 0)
		{
			//free(r);
			//return 0;
            dwErr = ::GetLastError ();
            break;
		}

        if (pfuncCB != NULL)
            pfuncCB();
		/* copy from the 2nd byte to the end byte. Report ID is 1st byte.*/
		memcpy(h, r + 1, rl - 1);

        //=======[ Parsing Check ]======
        // Add Rx Len
        nRxLen += (rl-1);
    
        if (m_bIsDebug)
        {
            sprintf(szBuf,"[Dbg001] Rx Len = %d\n",nRxLen);
            MessageBox(NULL,szBuf,_T("Info"),MB_OK | MB_ICONHAND );
        }

        for(;;){
            switch(nStgChk){
            case 0:
                // Check Header "ViVOtechV2<00><Cmd,Sts><Len,2B><CRC,2B>" or "ViVOPayV3<00><Cmd,SubCmd><Sts><Len,2B><CRC,2B>",
                if (nRxLen>=(m_nOffsetCmd+4)){
                    if (m_bIsDebug){
                        sprintf(szBuf,"[Dbg002] Rx Stg 0\n");
                        MessageBox(NULL,szBuf,_T("Info"),MB_OK | MB_ICONHAND );
                    }
                    nStgChk++;
                    continue;
                }
                break;
            case 1:
                // Check V2 / V3 protocol type
                if (IsV3Protocol(data))
                {
                     if (m_bIsDebug)
                     {
                         sprintf(szBuf,"[lp %d][Dbg003] Rx Stg 1, V3 type detected\n",nLoopCnt);
                         MessageBox(NULL,szBuf,_T("Info"),MB_OK | MB_ICONHAND );
                     }
                     
                    SetViVOProtocl_V3();
                }
                else{
                     if (m_bIsDebug)
                     {
                         sprintf(szBuf,"[lp %d][Dbg003] Rx Stg 1, V2 type detected\n",nLoopCnt);
                         MessageBox(NULL,szBuf,_T("Info"),MB_OK | MB_ICONHAND );
                     }
                    SetViVOProtocl_V2();
                }

                // Check Full package ready
                // ViVO V2 or V3 format protocol 
                //         <Header><2 bytes, Length><Raw Data><2 bytes, CRC>
                nRxPkgLen =  ViVOProtocol_GetLength_Pkg(data); //

                if (m_bIsDebug)
                {
                    sprintf(szBuf,"[lp %d][Dbg003] Rx Stg 1, nRxPkgLen = %d\n",nLoopCnt,nRxPkgLen);
                    MessageBox(NULL,szBuf,_T("Info"),MB_OK | MB_ICONHAND );
                }
                if (nRxLen>=nRxPkgLen)
                    // Full Package is ready.
                        bNotDone = false;
                // 
                break;
            }
            //
            break;
        }// for(;;)

        nLoopCnt ++;
	} while (bNotDone &&((h += rl - 1) < (data + io_unlen)));

	free(r);
    

#if 1
    if (IsV3Protocol(data))
        SetViVOProtocl_V3();
    else
        SetViVOProtocl_V2();
   
    // Truncate 00-padding part
    io_unlen = nRxPkgLen; // m_nOffsetLenL +ViVOProtocol_GetLength(data) + 2;

    // return correct data length.
    return io_unlen;
#else
    // NGA Format
	unsigned char tmp[2048] = { 0 };
	for (UINT i = 0; i<io_unlen; i++)
	{
		if (data[i] == 0x00)
		{
			continue;
		}
		else
		{
			memcpy(tmp, data + i, io_unlen - i);
			break;
		}
	}
	memcpy(data, tmp, io_unlen);
	/* something wrong                  */
	if (h - data < rl - 1)
		return 0;
	/* trim the zero end                    */
	/* 1. NAK block only, return 0          */
	if (checkNak(data, h - data))
	{
		unsigned char* p;
		for (p = h - (rl - 1) - 1; p> data; p--)
		if ((*p) != 0)
			break;
		return p - data + 1;

	}
	//return 1;
	/* 2. ACK without data and NAK          */
	if ((h - data) == (rl - 1) * 2 &&
		checkAck(data, rl - 1) &&
		checkNak(data + rl - 1, rl - 1))
		return 1;
	/* 3. UNKNOWN ID command response */
	if (checkUnknownID(data, rl - 1))
		return 1;
	/* 4. WARN without data and NAK			*/
	if ((h - data) == (rl - 1) * 2 &&
		checkWarn(data, rl - 1) &&
		checkNak(data + rl - 1, rl - 1))
		return 1;
	/* 5. ACK start with data block and NAK             */
	if (ACK == data[0] || ACK == data[3] || ACK == data[5]){//&& 

		//	checkNak(h-(rl-1), rl-1) ){/*comfirm  NAK ended*/
		unsigned char* p;
		for (p = h - (rl - 1) - 1; p> data; p--)
		if ((*p) != 0)
			break;
		return p - data + 1;
	}
	if (data[0] == 0x02)
	{

		unsigned char* p;
		for (p = h - (rl - 1) - 1; p> data; p--)
		if ((*p) != 0)
			break;
		return p - data + 1;
	}
	/*
	if (ACK == data[0] || (0x02 == data[0]))
	{
	unsigned char* p;
	for (p = h - (rl - 1) - 1; p> data; p--)
	if ((*p) != 0)
	break;
	return p - data + 1;
	}
	*/
	return 0;
#endif
}

#endif