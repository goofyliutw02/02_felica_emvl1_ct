// MFCLibraryWinobjs.cpp : Defines the initialization routines for the DLL.
//

#include "stdafx.h"
#include "idt_usb_hid.h"
#include "usb_hid_idtw.h"


#include <string.h>
#include <iostream>

using namespace std;

#ifdef _DEBUG
#define new DEBUG_NEW
#endif

//
//TODO: If this DLL is dynamically linked against the MFC DLLs,
//		any functions exported from this DLL which call into
//		MFC must have the AFX_MANAGE_STATE macro added at the
//		very beginning of the function.
//
//		For example:
//
//		extern "C" BOOL PASCAL EXPORT ExportedFunction()
//		{
//			AFX_MANAGE_STATE(AfxGetStaticModuleState());
//			// normal function body here
//		}
//
//		It is very important that this macro appear in each
//		function, prior to any calls into MFC.  This means that
//		it must appear as the first statement within the 
//		function, even before any object variable declarations
//		as their constructors may generate calls into the MFC
//		DLL.
//
//		Please see MFC Technical Notes 33 and 58 for additional
//		details.
//

// CMFCLibraryWinobjsApp

//BEGIN_MESSAGE_MAP(CMFCLibraryWinobjsApp, CWinApp)
//END_MESSAGE_MAP()

//static CIdtUsbHid s_DevUSBHIDReader;
static CIdtUsbHid  s_DevUSBHIDReader;



extern "C" {
    CIdtUsbHid * getUsbHidDevObj(){
        return &s_DevUSBHIDReader;
    }
    //
    void __stdcall testfunc()
    {
        //AFX_MANAGE_STATE(AfxGetStaticModuleState());
        // normal function body here
        ::MessageBeep(1000);
    }

    DWORD __stdcall getDLLVersion(char * szVersion)
    {
        if (szVersion == NULL )
        {
            return 0;
        }

        if (szVersion) 
        {
            ::strcpy_s( szVersion,(size_t)20, "V1.0.0");
        }

        return ::strlen(szVersion);
    }

     BYTE __stdcall USBKB_Wedge_Open_u8(DWORD u32VID,DWORD u32PID)
    {
        if (s_DevUSBHIDReader.bIsNotSameDev(u32VID,u32PID))
        {
            if (s_DevUSBHIDReader.bIsOpen() )
            {
                s_DevUSBHIDReader.Close ();
            }
            s_DevUSBHIDReader.Open_KBWedge(u32VID,u32PID );
        }

        return (BYTE)s_DevUSBHIDReader.bIsOpen ();
     }

    BYTE __stdcall USBHIDOpen_u8(DWORD u32VID,DWORD u32PID)
    {
        if (s_DevUSBHIDReader.bIsNotSameDev(u32VID,u32PID))
        {
            if (s_DevUSBHIDReader.bIsOpen() )
            {
                s_DevUSBHIDReader.Close ();
            }
            s_DevUSBHIDReader.LookDevice (u32VID,u32PID );
        }

        return (BYTE)s_DevUSBHIDReader.bIsOpen ();
    }

    int __stdcall USBHIDOpen_JLT_u8(int nVID,int nPID)
    {
        DWORD u32VID,u32PID;
        u32VID = (DWORD)nVID;
        u32PID = (DWORD)nPID;
        UINT uVID = (UINT)nVID;
        UINT uPID = (UINT)nPID;
        if (s_DevUSBHIDReader.bIsNotSameDev(u32VID,u32PID))
        {
            if (s_DevUSBHIDReader.bIsOpen() )
            {
                s_DevUSBHIDReader.Close ();
            }
            s_DevUSBHIDReader.LookDevice (uVID,uPID);
        }

        return (int)s_DevUSBHIDReader.bIsOpen ();
    }

    //
    void __stdcall USBHIDClose()
    {
        //if (s_DevUSBHIDReader.bIsOpen() )
        //{
            s_DevUSBHIDReader.Close ();
        //}
    }
    //
    static bool s_bCriticalSection = false;


    DWORD __stdcall USBHIDSendAndResponse_u32(DWORD u32VID,DWORD u32PID, LPBYTE pSendData, DWORD u32SendSize, LPBYTE pRecvData, DWORD u32RecvDataBufSize,DWORD u32TimeoutMS, DWORD (* pfuncCB)(void))
    {
        DWORD u32RecvDataSize =0;
        // Make sure USB HID device is found
        if (!s_DevUSBHIDReader.bIsOpen()){
            if (USBHIDOpen_u8(u32VID,u32PID) == 0)
            {
                return 0xFFFFFFFF;
            }
        }
        if (pSendData == NULL)
        {
            return 0xFFFFFFFE;
        }

        if (pRecvData == NULL)
        {
            return 0xFFFFFFFD;
        }

        if (u32SendSize < 1)
        {
            return 0xFFFFFFFC;
        }
        if (u32RecvDataBufSize < 1)
        {
            return 0xFFFFFFFB;
        }

        //printf("\n************ Cmd : %02X-%02X\n",pSendData [10],pSendData [11]);
        if (!s_bCriticalSection )
        {
            s_bCriticalSection = true;
            // To Send Data and Get Response
            u32RecvDataSize = s_DevUSBHIDReader.SendAndGetResponseUSBHID ("",pSendData,u32SendSize,pRecvData,u32RecvDataBufSize ,u32TimeoutMS,pfuncCB );

            s_bCriticalSection = false;
        }
        else
        {
            //printf("[Warning][DLL][USB_HID] Reentry DLL functions, possible in multi-thread status\n");
        }

        // Return Response Size
        return u32RecvDataSize ;
    }
    //

    DWORD __stdcall USBHIDSendDataIDGCmd_u32  (DWORD u32VID,DWORD u32PID, LPBYTE pSendData, DWORD u32SendSize, DWORD u32TimeoutMS)
    {
        DWORD u32TxDataSize =0;
        if (!s_DevUSBHIDReader.bIsOpen()){
            // Make sure USB HID device is found
            if (USBHIDOpen_u8(u32VID,u32PID) == 0)
            {
                return 0xFFFFFFFF;
            }
        }
        if (pSendData == NULL)
        {
            return 0xFFFFFFFE;
        }

        if (u32SendSize < 1)
        {
            return 0xFFFFFFFD;
        }

        //printf("\n************ Cmd : %02X-%02X\n",pSendData [10],pSendData [11]);
        if (!s_bCriticalSection )
        {
            s_bCriticalSection = true;
            // To Send Data and Get Response
            u32TxDataSize = s_DevUSBHIDReader.SendDataUSBHID ("",pSendData,u32SendSize,u32TimeoutMS );

            s_bCriticalSection = false;
        }

        return u32TxDataSize ;
    }

    //
    DWORD __stdcall USBHIDRecvDataIDGCmd_u32(DWORD u32VID,DWORD u32PID, LPBYTE pbyteRecvOut, DWORD u32RecvOutSize, DWORD u32WaitMs,DWORD (* pfuncCB)(void))
    {
        DWORD u32Ret = 0;
        if (!s_DevUSBHIDReader.bIsOpen()){
            if (USBHIDOpen_u8(u32VID,u32PID) == 0)
            {
                return u32Ret;
            }
        }

        if (!s_bCriticalSection )
        {
            s_bCriticalSection = true;

            s_DevUSBHIDReader. Read_Byte_IDGCmd (pbyteRecvOut, &u32RecvOutSize, u32WaitMs, pfuncCB);

            if (u32RecvOutSize > 0 )
            {
                u32Ret = u32RecvOutSize;
            }

            s_bCriticalSection = false;
        }
        else
        {

        }

        return u32Ret ;
    }



    DWORD __stdcall USBHIDRecvData_u32(DWORD u32VID,DWORD u32PID, LPBYTE pbyteRecvOut, DWORD u32RecvOutSize, DWORD u32WaitMs ,DWORD (* pfuncCB)(void))
    {
        DWORD u32Ret = 0;

        if (!s_DevUSBHIDReader.bIsOpen()){
            if (USBHIDOpen_u8(u32VID,u32PID) == 0)
            {
                return u32Ret;
            }
        }

        
        if (!s_bCriticalSection )
        {
            s_bCriticalSection = true;

            u32Ret = (s_DevUSBHIDReader. Read_Byte_IDGCmd_SingleFrame_64bytes (pbyteRecvOut, &u32RecvOutSize, u32WaitMs, pfuncCB) == TRUE) ;

            if (u32Ret )
            {
                u32Ret = u32RecvOutSize;
            }

            s_bCriticalSection = false;
        }
        else
        {

        }

        return u32Ret ;
    }
   
} // End of extern "C"

//BOOL WINAPI InitializeCriticalSectionEx(
//  _Out_  LPCRITICAL_SECTION lpCriticalSection,
//  _In_   DWORD dwSpinCount,
//  _In_   DWORD Flags
//)
//{
//    return TRUE;
//}

//BOOL WINAPI CancelIoEx(
//  _In_      HANDLE hFile,
//  _In_opt_  LPOVERLAPPED lpOverlapped
//)
//{
//    return TRUE;
//}
// CMFCLibraryWinobjsApp construction

//CMFCLibraryWinobjsApp::CMFCLibraryWinobjsApp()
//{
//    // TODO: add construction code here,
//    // Place all significant initialization in InitInstance
//}


// The one and only CMFCLibraryWinobjsApp object

//CMFCLibraryWinobjsApp theApp;
//
//
//// CMFCLibraryWinobjsApp initialization
//
//BOOL CMFCLibraryWinobjsApp::InitInstance()
//{
//    CWinApp::InitInstance();
//
//    return TRUE;
//}
