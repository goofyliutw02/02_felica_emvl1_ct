// MFCLibraryWinobjs.h : main header file for the MFCLibraryWinobjs DLL
//

#pragma once

#ifndef __AFXWIN_H__
	#error "include 'stdafx.h' before including this file for PCH"
#endif

#include "resource.h"		// main symbols

extern "C" {
    CIdtUsbHid * getUsbHidDevObj();
     //
    void __stdcall testfunc();

    DWORD __stdcall getDLLVersion(char * szVersion);

    
    BYTE __stdcall USBHIDOpen_u8(DWORD u32VID,DWORD u32PID);

    int __stdcall USBHIDOpen_JLT_u8(int nVID,int nPID);

    //
    void __stdcall USBHIDClose();

    DWORD __stdcall USBHIDSendAndResponse_u32(DWORD u32VID,DWORD u32PID, LPBYTE pSendData, DWORD u32SendSize, LPBYTE pRecvData, DWORD u32RecvDataBufSize,DWORD u32TimeoutMS, DWORD (* pfuncCB)(void));
    //

      DWORD __stdcall USBHIDSendDataIDGCmd_u32  (DWORD u32VID,DWORD u32PID, LPBYTE pSendData, DWORD u32SendSize,  DWORD u32TimeoutMS);

    //
    DWORD __stdcall USBHIDRecvDataIDGCmd_u32(DWORD u32VID,DWORD u32PID, LPBYTE pbyteRecvOut, DWORD u32RecvOutSize, DWORD u32WaitMs,DWORD (* pfuncCB)(void));


    DWORD __stdcall USBHIDRecvData_u32(DWORD u32VID,DWORD u32PID, LPBYTE pbyteRecvOut, DWORD u32RecvOutSize, DWORD u32WaitMs ,DWORD (* pfuncCB)(void));
    
    //=================================================
    void __stdcall USBHIDSetWaitLoopStart();

    void __stdcall USBHIDSetWaitLoopEnd();

        //
    int __stdcall USBHIDOpen_JLT_Default_u8();

    DWORD __stdcall USBHIDSendAndResponse_DefCallbackWait_u32  (DWORD u32VID,DWORD u32PID, LPBYTE pSendData, DWORD u32SendSize, LPBYTE pRecvData, DWORD u32RecvDataBufSize,DWORD u32TimeoutMS);
    //

     DWORD __stdcall USBHIDRecvData_DefCallbackWait_u32(DWORD u32VID,DWORD u32PID, LPBYTE pbyteRecvOut, DWORD u32RecvOutSize, DWORD u32WaitMs);

     //
    DWORD __stdcall USBHIDRecvDataIDGCmd_DefCallbackWait_u32(DWORD u32VID,DWORD u32PID, LPBYTE pbyteRecvOut, DWORD u32RecvOutSize, DWORD u32WaitMs);

}// extern "C"