/*#############################################################################
# TCONVERT.H
#
# SCA Software International S.A.
# http://www.scasoftware.com
# scaadmin@scasoftware.com
#
# Copyright (c) 2000 SCA Software International S.A.
#
# Date: 01.05.2000
# Author: Zoran M.Todorovic
#
# This software is provided "AS IS", without a warranty of any kind.
# You are free to use/modify this code but leave this header intact.
#
#############################################################################*/

#ifndef __TCONVERT_H__
#define __TCONVERT_H__

#ifndef _INC_TCHAR
# include <tchar.h>
#endif
#ifndef _INC_CRTDBG
//# include <crtdbg.h>
#endif
#ifndef _WINDOWS_
# include <windows.h>
#endif

/*****************[ Basic Conversion Function ]***********************/
int ToWideString( WCHAR* &pwStr, const char* pStr, int len, BOOL IsEnd=TRUE);
int ToMultiBytes( char* &pStr, const WCHAR* pwStr, int len, BOOL IsEnd=TRUE);

//=============================================================================
// class _tochar
// This class converts either WCHAR or CHAR string to a new CHAR string.
// Memory is allocated/deallocated using new/delete
//=============================================================================

class _tochar {
private:
  BOOL m_bAutoDelete;
  LPSTR m_szBuffer;

public:
  _tochar(LPCWSTR wszText, BOOL bAutoDelete = TRUE);
  ~_tochar();
  operator LPSTR();
  operator LPCSTR();
};

//=============================================================================
// class _towchar
// This class converts either WCHAR or CHAR string to a new WCHAR string.
// Memory is allocated/deallocated using new/delete
//=============================================================================
class _towchar {
private:
  BOOL m_bAutoDelete;
  LPWSTR m_wszBuffer;

public:
  _towchar(LPCSTR szText, BOOL bAutoDelete = TRUE);
  ~_towchar();
  operator LPWSTR();
  operator LPCWSTR();
};

//=============================================================================
// class _totchar
// This class converts a TCHAR string to a new TCHAR string.
// Memory is allocated/deallocated using new/delete
//=============================================================================
#if defined(UNICODE)
class _totchar {
private:
  BOOL m_bAutoDelete;
  LPTSTR m_tszBuffer;

public:
  _totchar(LPCSTR szText, BOOL bAutoDelete = TRUE);
  ~_totchar();
  operator LPTSTR();
  operator LPCTSTR();
  _totchar & operator = (LPCTSTR ctszSr);
};
#endif

#if 0
//=============================================================================
// class _cochar
// This class converts either WCHAR or CHAR string to a new CHAR string.
// Memory is allocated/deallocated using CoTaskMemAlloc/CoTaskMemFree.
//=============================================================================

class _cochar {
private:
  BOOL m_bAutoDelete;
  LPSTR m_szBuffer;

public:
  _cochar(LPCWSTR wszText, BOOL bAutoDelete = TRUE)
  {
    m_bAutoDelete = bAutoDelete;
    _ASSERTE(wszText);
    int nLen = wcslen(wszText)+1;
    m_szBuffer = (LPSTR)::CoTaskMemAlloc(nLen * sizeof(CHAR));
    wcstombs(m_szBuffer, wszText, nLen);
  }
  ~_cochar();
  operator LPSTR();
  operator LPCSTR();
};

//=============================================================================
// class _towchar
// This class converts either WCHAR or CHAR string to a new WCHAR string.
// Memory is allocated/deallocated using CoTaskMemAlloc/CoTaskMemFree
//=============================================================================

class _cowchar {
private:
  BOOL m_bAutoDelete;
  LPWSTR m_wszBuffer;

public:
  _cowchar(LPCWSTR wszText, BOOL bAutoDelete = TRUE);
  _cowchar(LPCSTR szText, BOOL bAutoDelete = TRUE);
  ~_cowchar();
  operator LPWSTR();
  operator LPCWSTR();
};

//=============================================================================
// class _cotchar
// This class converts a TCHAR string to a new TCHAR string.
// Memory is allocated/deallocated using CoTaskMemAlloc/CoTaskMemFree
//=============================================================================

class _cotchar {
private:
  BOOL m_bAutoDelete;
  LPTSTR m_tszBuffer;

public:
  _cotchar(LPCSTR szText, BOOL bAutoDelete = TRUE);
  _cotchar(LPCWSTR wszText, BOOL bAutoDelete = TRUE);
  ~_cotchar();
  operator LPTSTR();
  operator LPCTSTR();
};
#endif
#endif

/*#############################################################################
# End of file
#############################################################################*/