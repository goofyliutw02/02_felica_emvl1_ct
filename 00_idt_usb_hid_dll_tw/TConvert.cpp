/*
	This module is used for ASCII code & Unicode (Wide/Tchar)
	coversion rountines.
*/
#pragma once



#include "stdafx.h"
#include "TConvert.h"

/******************************************************
*function:  convert multibyte character set to wide-character set
*param:      pwStr--[out] Points to a buffer that receives the translated buffers.
*            pStr--[in] Points to the multibyte character set(or string) to be converted.
*            len --[in] Specify the size in bytes of the string pointed to by the pStr parameter,
*                       or it can be -1 if the string is null terminated.
*            IsEnd--[in]Specify whether you add '\0' to the end of converted array or not.
*return: the length of converted set (or string )
*******************************************************/
int ToWideString( WCHAR* &pwStr, const char* pStr, int len, BOOL IsEnd)
{
    ASSERT_POINTER(pStr, char);
    ASSERT(len != 0 || len == -1);
    int nWideLen = MultiByteToWideChar(CP_ACP, 0, pStr, len, NULL, 0);
    if (len == -1)
    {
        --nWideLen;
    }
    if (nWideLen == 0)
    {
        return 0;
    }
    if (IsEnd)
    {
        pwStr = new WCHAR[(nWideLen+1)*sizeof(WCHAR)];
        ZeroMemory(pwStr, (nWideLen+1)*sizeof(WCHAR));
    }
    else
    {
        pwStr = new WCHAR[nWideLen*sizeof(WCHAR)];
        ZeroMemory(pwStr, nWideLen*sizeof(WCHAR));
    }
    MultiByteToWideChar(CP_ACP, 0, pStr, len, pwStr, nWideLen);
    return nWideLen;
}
/******************************************************
*function:   convert wide-character  set to multibyte character set
*param:      pStr--[in] Points to a buffer that receives the translated buffer.
*            pwStr--[out] Points to the wide character set ( or string ) to be converted.
*            len --[in] Specify the size in bytes of the string pointed to by the pwStr parameter,
*                       or it can be -1 if the string is null terminated.
*            IsEnd--[in]Specify whether you add '\0' to the end of converted array or not.
*return:     the length of converted set (or string )
*******************************************************/
int ToMultiBytes( char* &pStr, const WCHAR* pwStr, int len, BOOL IsEnd)
{
    ASSERT_POINTER(pwStr, WCHAR) ;
    ASSERT( len != 0 || len == -1 ) ;
    int nChars = WideCharToMultiByte(CP_ACP, 0, pwStr, len, NULL, 0, NULL, NULL);
    if (len == -1)
    {
        --nChars;
    }
    if (nChars == 0)
    {
        return 0;
    }
    if(IsEnd)
    {
        pStr = new char[nChars+1];
        ZeroMemory(pStr, nChars+1);
    }
    else
    {
        pStr = new char[nChars];
        ZeroMemory(pStr, nChars);
    }
    WideCharToMultiByte(CP_ACP, 0, pwStr, len, pStr, nChars, NULL, NULL);
    return nChars;
}
/*
for example........
char *pStr = "test";
WCHAR* pwStr;
int nWideLen = ToWideString(pwStr, pStr, -1, TRUE);
.........
.........
delete pwStr; pwStr = NULL; 

WCHAR* pwStr = _T("test");
char *pStr;
int nWideLen = ToMultiBytes(pStr, pwStr, -1, TRUE);
..........
.........
delete pStr; pStr = NULL;
*/

//=============================================================================
// class _tochar
// This class converts either WCHAR or CHAR string to a new CHAR string.
// Memory is allocated/deallocated using new/delete
//=============================================================================
_tochar::_tochar(LPCWSTR wszText, BOOL bAutoDelete):m_szBuffer(NULL)
{
	/*
	m_bAutoDelete = bAutoDelete;
	_ASSERTE(szText);
	int nLen = strlen(szText) + 1;
	m_szBuffer = new CHAR [nLen];
	strcpy(m_szBuffer, szText);
	*/
	_ASSERTE(wszText);
	m_bAutoDelete = bAutoDelete;
	::ToMultiBytes(m_szBuffer, wszText, -1);
}
_tochar::~_tochar()
{
	if (m_bAutoDelete) {
	  _ASSERTE(m_szBuffer);
	  delete [] m_szBuffer;
	  m_szBuffer = NULL;
	}
}
_tochar::operator LPSTR()
{
	_ASSERTE(m_szBuffer);
	return (LPSTR)m_szBuffer;
}
_tochar::operator LPCSTR()
{
	_ASSERTE(m_szBuffer);
	return (LPCSTR)m_szBuffer;
}
//=============================================================================
// class _towchar
// This class converts either WCHAR or CHAR string to a new WCHAR string.
// Memory is allocated/deallocated using new/delete
//=============================================================================

_towchar::_towchar(LPCSTR cszText, BOOL bAutoDelete):m_wszBuffer(NULL)
{
	/*
	m_bAutoDelete = bAutoDelete;
	_ASSERTE(szText);
	int nLen = strlen(szText) + 1;
	m_wszBuffer = new WCHAR [nLen];
	mbstowcs(m_wszBuffer, szText, nLen);
	*/

	_ASSERTE(cszText);
	m_bAutoDelete = bAutoDelete;
	::ToWideString(m_wszBuffer, cszText, -1);
}
_towchar::~_towchar()
{
	if (m_bAutoDelete) {
	  _ASSERTE(m_wszBuffer);
	  delete [] m_wszBuffer;
	}
}
_towchar::operator LPWSTR()
{
	_ASSERTE(m_wszBuffer);
	return (LPWSTR)m_wszBuffer;
}
_towchar::operator LPCWSTR()
{
	_ASSERTE(m_wszBuffer);
	return (LPCWSTR)m_wszBuffer;
}


#if defined(UNICODE)
//=============================================================================
// class _totchar
// This class converts a TCHAR string to a new TCHAR string.
// Memory is allocated/deallocated using new/delete
//=============================================================================
_totchar::_totchar(LPCSTR cszText, BOOL bAutoDelete):m_tszBuffer(NULL)
{
/*
	m_bAutoDelete = bAutoDelete;
	_ASSERTE(szText);
	int nLen = strlen(szText) + 1;
	m_tszBuffer = new TCHAR [nLen];
	#if defined(UNICODE) || defined(_UNICODE)
	mbstowcs(m_tszBuffer, szText, nLen);
	#else
	strcpy(m_tszBuffer, szText);
	#endif
*/
	_ASSERTE(cszText);
	m_bAutoDelete = bAutoDelete;
	::ToWideString(m_tszBuffer, cszText, -1);
}

_totchar::~_totchar()
{
	if (m_bAutoDelete) {
	  _ASSERTE(m_tszBuffer);
	  delete [] m_tszBuffer;
	}
}
_totchar::operator LPTSTR()
{
	_ASSERTE(m_tszBuffer);
	return (LPTSTR) m_tszBuffer;
}
_totchar::operator LPCTSTR()
{
	_ASSERTE(m_tszBuffer);
	return (LPCTSTR) m_tszBuffer;
}

_totchar & _totchar::operator = (LPCTSTR ctszSrc)
{
	WORD wSrcLen;
	_ASSERTE(ctszSrc);
	wSrcLen=::wcslen(ctszSrc);
	if (::wcslen(m_tszBuffer) < (wSrcLen))
	{
		delete m_tszBuffer;
		m_tszBuffer = new TCHAR [wSrcLen+1];
	}
	::wcscpy_s(m_tszBuffer, wSrcLen+1, ctszSrc);

	return *this;
}
#endif

#if 0
//=============================================================================
// class _cochar
// This class converts either WCHAR or CHAR string to a new CHAR string.
// Memory is allocated/deallocated using CoTaskMemAlloc/CoTaskMemFree.
//=============================================================================
_cochar::_cochar(LPCWSTR wszText, BOOL bAutoDelete)
{
	m_bAutoDelete = bAutoDelete;
	_ASSERTE(wszText);
	int nLen = wcslen(wszText)+1;
	m_szBuffer = (LPSTR)::CoTaskMemAlloc(nLen * sizeof(CHAR));
	wcstombs(m_szBuffer, wszText, nLen);
}
_cochar::_cochar(LPCSTR szText, BOOL bAutoDelete)
{
	m_bAutoDelete = bAutoDelete;
	_ASSERTE(szText);
	int nLen = strlen(szText) + 1;
	m_szBuffer = (LPSTR)::CoTaskMemAlloc(nLen * sizeof(CHAR));
	strcpy(m_szBuffer, szText);
}
_cochar::~_cochar()
{
	if (m_bAutoDelete)
	  ::CoTaskMemFree(m_szBuffer);
}
_cochar::operator LPSTR()
{
	return (LPSTR)m_szBuffer;
}
_cochar::operator LPCSTR()
{
	return (LPCSTR)m_szBuffer;
}


//=============================================================================
// class _towchar
// This class converts either WCHAR or CHAR string to a new WCHAR string.
// Memory is allocated/deallocated using CoTaskMemAlloc/CoTaskMemFree
//=============================================================================
_cowchar::_cowchar(LPCWSTR wszText, BOOL bAutoDelete)
{
	m_bAutoDelete = bAutoDelete;
	_ASSERTE(wszText);
	int nLen = wcslen(wszText)+1;
	m_wszBuffer = (LPWSTR)::CoTaskMemAlloc(nLen * sizeof(WCHAR));
	wcscpy(m_wszBuffer, wszText);
}
_cowchar::_cowchar(LPCSTR szText, BOOL bAutoDelete)
{
	m_bAutoDelete = bAutoDelete;
	_ASSERTE(szText);
	int nLen = strlen(szText) + 1;
	m_wszBuffer = (LPWSTR)::CoTaskMemAlloc(nLen * sizeof (WCHAR));
	mbstowcs(m_wszBuffer, szText, nLen);
}
_cowchar::~_cowchar()
{
	if (m_bAutoDelete)
	  ::CoTaskMemFree(m_wszBuffer);
}
_cowchar::operator LPWSTR()
{
	return (LPWSTR)m_wszBuffer;
}
_cowchar::operator LPCWSTR()
{
	return (LPCWSTR)m_wszBuffer;
}


//=============================================================================
// class _cotchar
// This class converts a TCHAR string to a new TCHAR string.
// Memory is allocated/deallocated using CoTaskMemAlloc/CoTaskMemFree
//=============================================================================
_cotchar::_cotchar(LPCSTR szText, BOOL bAutoDelete)
{
	m_bAutoDelete = bAutoDelete;
	_ASSERTE(szText);
	int nLen = strlen(szText) + 1;
	m_tszBuffer = (LPTSTR)::CoTaskMemAlloc(nLen * sizeof(TCHAR));
	#if defined(UNICODE) || defined(_UNICODE)
	mbstowcs(m_tszBuffer, szText, nLen);
	#else
	strcpy(m_tszBuffer, szText);
	#endif
}
_cotchar::_cotchar(LPCWSTR wszText, BOOL bAutoDelete)
{
	m_bAutoDelete = bAutoDelete;
	_ASSERTE(wszText);
	int nLen = wcslen(wszText) + 1;
	m_tszBuffer = (LPTSTR)::CoTaskMemAlloc(nLen * sizeof(TCHAR));
	#if defined(UNICODE) || defined(_UNICODE)
	wcscpy(m_tszBuffer, wszText);
	#else
	wcstombs(m_tszBuffer, wszText, nLen);
	#endif
}
_cotchar::~_cotchar()
{
	if (m_bAutoDelete)
	  ::CoTaskMemFree(m_tszBuffer);
}
_cotchar::operator LPTSTR()
{
	return (LPTSTR) m_tszBuffer;
}
_cotchar::operator LPCTSTR()
{
	return (LPCTSTR) m_tszBuffer;
}
#endif