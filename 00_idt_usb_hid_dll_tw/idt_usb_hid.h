// idt_usb_hid.h

//#include "stdafx.h"
#include <setupapi.h>
#include <tchar.h>
extern "C"
{
#include "hidsdi.h"
}
//
#include "Global_RS232_USBHID.h"
//
#define __USB_KEYB_WEDGE_H_ 1
// USB
#define MAX_FRAME_SINGLE_SIZE 63
#define MAX_FRAME_TOTAL_SIZE 4096
//
class CIdtUsbHid
{
    DWORD  m_u32VID, m_u32PID;
    OVERLAPPED m_overlapped;
    HANDLE m_ReadEvent;
    HANDLE m_WriteEvent;
    HIDP_CAPS m_caps;
    HANDLE m_hidDevice;
    char m_devPathName[1024];
    DWORD m_dwIDGCmdFrameSizeRest;
    TCHAR tszMsg[TMP_BUF_LEN];

    //=====[ Integrate from EMV L1 Tool Part ]=============
    bool m_bIsUSBFrame;
    //
    LPBYTE    m_pBufTx;
    UINT m_u32BufLenTx;
    DWORD m_dwTxTOMS;
    LPBYTE    m_pBufRx;
    UINT m_u32BufLenRx;
    DWORD m_dwRxTOMS;
    BYTE m_arrTxBuf[MAX_BUF_SIZE_4K];
    BYTE m_arrRxBuf[MAX_BUF_SIZE_4K];

    DWORD m_dwOpenDevModeFlagAndAttr;
    DWORD m_dwOpenDevModeShare;

    //===[ ViVOPay Protocol - V2, V3]====
    int m_nPkgMinLen; // V2 = 16, V3 = 17
    int m_nOffsetV3Flag; // '3', 0x33, offset = 8
    int m_nOffsetCmd; // Rx, V2 = 10; V3 = 10;
    int m_nOffsetCmdSub; // Rx, V2 = N/A; V3 = 11;
    int m_nOffsetRespSts; // Rx, V2 = 11; V3 = 12;
    int m_nOffsetLenH; // Rx, V2 = 12; V3 = 13
    int m_nOffsetLenL; // Rx, V2 = 13; V3 = 14
    //
    int m_nUsbDevMode; // 0 : USB-HID;   1 : USB-KB Wedge;   -1 : Unknown
    //
    bool m_bIsDebug;
    //
private:
    unsigned int InterruptSendUSBFrame(unsigned char * pSendData, unsigned int nSendDataLen);
    unsigned int InterruptDirectSend(unsigned char * pSendData, unsigned int nSendDataLen);
    unsigned int InterruptSend(unsigned char * pSendData, unsigned int nSendDataLen);
    unsigned int InterruptRecv(unsigned char * pRecvData, unsigned int & nRecvDataLen);


    void SetVidPid(UINT uiVID,UINT uiPID);
	//unsigned int LookDevice(UINT auiVid, UINT auiPid);
    unsigned int LookDevicePIDList(UINT uiVid, UINT * puiPidLst, LPSTR * ppDevPathLst, int nBufSize); // return value is number of Pid List
    int OpenDeviceNoSeek(UINT auiVid, UINT auiPid,LPTSTR ptszDevPath);

public:
    void Close();
    int count;
    CIdtUsbHid();
    ~CIdtUsbHid();

    BOOL            bIsOpen();
    BOOL            bIsNotSameDev(DWORD u32PID,DWORD u32VID);

#if __USB_KEYB_WEDGE_H_
    // 2017 Sept 11, For Keyboard Wedge Mode. 
    bool            bIsUSB_KBWedge();
    BOOL            Open_KBWedge(UINT vid, UINT pid, UINT accessMode = 0);
    unsigned int    Write_KBWedge(BYTE * data, UINT len,DWORD u32TimeoutMS, DWORD (* pfuncCB)(void));
    unsigned int    Read_KBWedge(BYTE * data, UINT io_unlen,DWORD u32TimeoutMS, DWORD (* pfuncCB)(void));
#endif
    DWORD           LookDevice(UINT auiVid, UINT auiPid);
    DWORD           LookDevice_V17May23(UINT auiVid, UINT auiPid);

    void LogInfoHexFomratedLine(LPBYTE pbyteData, DWORD dwDataSize,BOOL bSpaceDelimiter);
    //
    void LogInfoHexFomrated(LPBYTE pbyteData, DWORD dwDataSize, DWORD dwBytesPerLine = 16,BOOL bSpaceDelimiter = TRUE );

    unsigned int Read(unsigned char * pBuf, unsigned int iLim, long lTimeout); // maximum 1024 bytes

    unsigned int Write(unsigned char* aucmpData, unsigned int auiDataLen);
    unsigned int Write(unsigned char *pbuf, unsigned int iLen, long lTimeout);

    unsigned int Write_Byte(unsigned char* aucmpData, unsigned int auiDataLen) ;

    //------------------[ USB HID IDG Command Send ]---------------------------
    DWORD Write_Byte_IDGCmd_FrameSize_64bytes(LPBYTE arrayByteSend, DWORD u32SendSize=64, DWORD u32TimeoutMS=500,DWORD (* pfuncCB)(void)=NULL) ;
    //DWORD Write_Byte_IDGCmd_FrameSize_64bytes(LPBYTE arrayByteSend, DWORD u32SendSize=64, DWORD u32TimeoutMS=500) ;
    DWORD  Write_Byte_IDGCmd_Compose_SendData_RID_Data_Padding_Upto_64Bytes_u32(LPBYTE arrayByteDataSendFrameSingle, BYTE byteRID,LPBYTE  arrayByteSend,DWORD dwCommandLen);
    DWORD Write_Byte_IDGCmd(LPBYTE pByteSend, DWORD u32SendSize,DWORD u32TimeoutMS) ;
    DWORD Write_Byte_IDGCmd(LPBYTE pByteSend, DWORD u32SendSize,DWORD u32TimeoutMS,DWORD (* pfuncCB)(void)) ;

    //------------------[ USB HID IDG Command Receive ]---------------------------
    void Read_Byte_IDGCmd                                                                        (LPBYTE arrayByteRecvOut, DWORD * pdwRecvOutSize,DWORD dwTimeoutMS);
    void Read_Byte_IDGCmd                                                                        (LPBYTE arrayByteRecvOut, DWORD * pdwRecvOutSize,DWORD dwTimeoutMS,DWORD (* pfuncCB)(void));
    BOOL Read_Byte_IDGCmd_Deassemble_RIDFrame_To_IDGFrame(
        LPBYTE pbyteSrcUSBHIDFrameRecv,DWORD dwBytesRecvCnt,LPBYTE pbyteDestDataRecvIDGFrame,LPDWORD pdwBytesRecvedSingleFrame, DWORD * pdwRID1_2_GetIDGCmdLength);
    //BOOL Read_Byte_IDGCmd_SingleFrame_64bytes                             (LPBYTE pbyteRecvOut, LPDWORD pdwRecvOutSize, DWORD u32WaitMs = 500) ;
    BOOL Read_Byte_IDGCmd_SingleFrame_64bytes                             (LPBYTE pbyteRecvOut, LPDWORD pdwRecvOutSize, DWORD u32WaitMs = 500,DWORD (* pfuncCB)(void) = NULL) ;
    BOOL Read_Byte_IDGCmd_SingleFrame_64bytes_New                   (LPBYTE pbyteRecvOut, LPDWORD pdwRecvOutSize, DWORD u32WaitMs = 500,DWORD (* pfuncCB)(void) = NULL) ;
    BOOL Read_Byte_IDGCmd_SingleFrame_64bytes_Old                     (LPBYTE pbyteRecvOut, LPDWORD pdwRecvOutSize, DWORD u32WaitMs = 500,DWORD (* pfuncCB)(void) = NULL) ;
    //
    DWORD SendAndGetResponseUSBHID(char * szIDGCmd, LPBYTE pSendData, DWORD dwSendDataSize, LPBYTE pResponseData,DWORD dwResponseDataSize,DWORD u32TimeoutMS);
    DWORD SendDataUSBHID(char * szIDGCmd, LPBYTE pSendData, DWORD dwSendDataSize, DWORD u32TimeoutMS);
    DWORD SendAndGetResponseUSBHID(char * szIDGCmd, LPBYTE pSendData, DWORD dwSendDataSize, LPBYTE pResponseData,DWORD dwResponseDataSize,DWORD u32TimeoutMS,DWORD (* pfuncCB)(void));

    //
    DWORD SendAndGetResponseUSBHID_SendFrame64bytes(LPBYTE pSendData, DWORD dwSendDataSize, LPBYTE pResponseData,DWORD dwResponseDataSize,DWORD u32TimeoutMS, pFUNC_APP_SLEEP pfuncCB= NULL);

    int NeoCmdSend(LPBYTE  pSendData,DWORD dwSendDataSize,DWORD u32TimeoutMS, pFUNC_APP_SLEEP pfuncCB=NULL);
    int NeoCmdRecv(LPBYTE pResponseData,DWORD dwResponseDataSize,DWORD u32TimeoutMS, pFUNC_APP_SLEEP pfuncCB=NULL);
    //
    unsigned int GetReport(unsigned char* aucmpData, unsigned int auiWaitMs);
    unsigned int GetResponse(unsigned char* aucmpData, unsigned int auiWaitMs);

    unsigned int SendCommand(UINT auiVid, UINT auiPid, unsigned char* aucmpSource, unsigned int auiSourceLen, unsigned char* aucmpResp, unsigned int auiWaitMs);

    // 2017 May 27, Added to support V2 & V3 protocol concurrently
    // Automatically to check V2 & V3 while Rx process - YES
    void SetViVOProtocl_V2();
    void SetViVOProtocl_V3();
    int ViVOProtocol_GetLength(LPBYTE pData);
    // V2 = 0x32, '2'
    // V3 = 0x33, '3'
    bool IsV3Protocol(LPBYTE pRxData_Len_9OrMore){ return (pRxData_Len_9OrMore[m_nOffsetV3Flag]==0x33); }
    int ViVOProtocol_GetLength_Pkg(LPBYTE pData);
};
